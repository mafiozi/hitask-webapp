/**
 * Specifies tool tip on calendar drop target
 */
//dojo.provide("hi.dnd.target.calendar");
define(function () {
	dojo.declare("dojo.dnd.HiDragTargetCalendar", [dojo.dnd.Source], {
		isSource: false,
		selfAccept: false,
		accept: ["dragged_task"],
		dropTarget: null,
			
		permissionsAllowed: [Hi_Permissions.MODIFY, Hi_Permissions.EVERYTHING],

		action: 'modify',

		onMouseOut: function (e) {
			dojo.dnd.HiDragTargetCalendar.superclass.onMouseMove.call(this, e);
			
			//JDT var m = dojo.dnd.Manager();
			var m = dojo.dnd.Manager.manager();

			if (m.source instanceof dojo.dnd.HiDragSource) {
				m.avatar.updateText('');
				this.dropTarget = null;
			}
		},

		onMouseMove: function (e) {
			dojo.dnd.HiDragTargetCalendar.superclass.onMouseMove.call(this, e);
			
			if (this.isDragging) {
				//JDT var m = dojo.dnd.Manager();
				var m = dojo.dnd.Manager.manager();
				if (m.source instanceof dojo.dnd.HiDragSource) {
					//Files don't have date or time
					var id = m.source.dragNode.dragId;
					var type = hi.items.manager.get(m.source.dragNode.dragId, 'category');
					if (!id || !type) return;
					/* #3059 Note can be d&d to the calendar and set the time*/
					if (type == hi.widget.TaskItem.CATEGORY_FILE || type == hi.widget.TaskItem.CATEGORY_NOTE) return;

					var txt = this.getDropText(e);
					if (type === hi.widget.TaskItem.CATEGORY_TASK) {
						txt = this.getTaskDropText(e, id);
					}
					m.avatar.updateText(txt);
				}
			}
		},
		
		getDropText: function (e) {
			var _e = this.getDropSpot(e);
			if (_e) {
				return this.text + _e.getElementsByTagName('INPUT')[0].value;
			}
			return '';
		},

		getTaskDropText: function (e, itemId) {
			var item = him.get(itemId);
			var _e = this.getDropSpot(e);
			var text = '';

			if (_e) {
				// return this.text + _e.getElementsByTagName('INPUT')[0].value;
				if (item.start_date) {
					text = hi.i18n.getLocalization('center.start_on');
				} else {
					text = hi.i18n.getLocalization('center.due_on');
				}
			}
			return text + ' ' + _e.getElementsByTagName('INPUT')[0].value;
		},
		
		getDropSpot: function (e) {
			var _e = e.target;
			for(; _e && !dojo.hasClass(_e, 'dojoDndTarget'); _e = _e.parentNode) {
				if (dojo.hasClass(_e, 'dropSpot')) {
					this.dropTarget = _e;
					return _e;
				}
			}
			
			this.dropTarget = null;
			return null;
		}
		
	});
	
	return dojo.dnd.HiDragTargetCalendar;

});