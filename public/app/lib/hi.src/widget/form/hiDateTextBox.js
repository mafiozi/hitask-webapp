/* 
 * Extension of dijit/form/DateTextBox that highlights the current date via a CSS class named 'todayDate'
 */
define([
	"dojo/_base/declare",
	'dijit/form/DateTextBox'
], function(declare,dateTextBox) {
	return declare("hi.widget.form.hiDateTextBox", [dateTextBox],{

		openDropDown:function() {
			this.inherited(arguments);
			
			this.setTodayDate();
			this.connect(this.dropDown,'_populateGrid',function() {
				this.setTodayDate();
			});
		},
		
		setTodayDate: function() {
			var mycal = this.dropDown,
			myD = mycal._patchDate(new Date()),
			newCell = mycal._getNodeByDate(myD);
			if (newCell && !dojo.hasClass(newCell,'todayDate')) dojo.addClass(newCell,'todayDate');
		}
	});
});