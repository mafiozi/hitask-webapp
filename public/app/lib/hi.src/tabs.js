define([
	'dojo/on',
	'dojo/dom-construct',
	'dijit/CheckedMenuItem',
	'dojo/_base/declare'
], function (on, domConstruct, CheckedMenuItem, declare) {
	var Tabs = declare('hi.tabs', [], {
		/**
		 * Default options for tabs
		 * @type {Object}
		 * @private
		 */
		defaultOptions: {
			selectedClass: false,	// class appended to selected tab
			defaultTab: 0,	// index of default tab
			start: 0,
			onChange: function(k){},
			names: {},
			hashInterval: 0,
			openFirst: true,
			check: false,
			elements: false
		},

		id: '',

		count: 0,

		/*
		 * Options for each tab instance
		 * @type {Object}
		 * @private
		 */
		options: {},
		
		/**
		 * Active tabs
		 * @type {Object}
		 * @private
		 */
		active: {},
		
		constructor: function(id, options) {
			this.options = options;
			this.id = id;
			this.build();
		},
		/**
		 * Set options
		 * 
		 * @param {String} tab_id Tab ID
		 * @param {Object} options Options
		 */
		setOptions: function(tab_id, options) {
			var i;
			Tabs.options[tab_id] = {};
			for (i in options) {
				Tabs.options[tab_id][i] = options[i];
			}
		},
		
		/**
		 * Returns ID
		 * 
		 * @param {Object} i
		 * @param {Object} elements
		 */
		getId: function(i, elements) {
			if (elements !== false) {
				if (i in elements) {
					return elements[i];
				}
			} else {
				return i;
			}
			return false;
		},

		canOpenPrevious: function() {
			var active = this.active;
			var bool = false;
			var tabsPref = this.getTabsPreference();
			var keys = Object.keys(this.options.config);
			var index = keys.indexOf(active);

			for (var i = index - 1; i >= 0; i--) {
				if (tabsPref[keys[i]] !== false) {
					bool = keys[i];
					break;
				}
			}

			return bool;
		},

		canOpenNext: function() {
			var active = this.active;
			var bool = false;
			var tabsPref = this.getTabsPreference();
			var keys = Object.keys(this.options.config);
			var index = keys.indexOf(active);

			for (var i = index + 1; i < keys.length; i++) {
				if (tabsPref[keys[i]] !== false) {
					bool = keys[i];
					break;
				}
			}

			return bool;
		},
		
		/**
		 * Initialize tab instance
		 * 
		 * @param {String} tab_id Tab object ID
		 * @param {Object} options Options
		 */
		build: function() {
			var options = dojo.mixin(this.defaultOptions, this.options || {});
			var tabsConfig = options.config;
			var tabsContainer = document.querySelector('#tabs-container');
			tabsContainer = tabsContainer.querySelector('tr');
			var i, k;
			var t = this;
			
			if (!options.openFirst) {
				options.defaultTab = -1;
			}
			if (location.hash) {
				for (i in options.names) {
					if (location.hash == "#" + options.names[i]) {
						options.defaultTab = i;
					}
				}
			}
			if (parseInt(options.hashInterval) > 0) {
				window.setInterval(function(){
					this.checkHash();
				}.bind(this), Math.max(options.hashInterval, 100));
			}
			if (options.elements !== false) {
				options.elements = [];
				options.start = 0;
			}
			// Tabs.setOptions(tab_id, options);
			this.options = options;
			var tab, div;
			i = options.start;
			if (options.defaultTab && options.defaultTab.constructor == Function) {
				options.defaultTab = options.defaultTab(options);
			}

			// create tabs on the fly
			var temp, template;
			for (k in tabsConfig) {
				temp = tabsConfig[k];
				template = [
					'<td id="' + this.id + '_' + k + '" class="' + temp.title + '">',
						'<div class="tab">',
							'<a href="#" onclick="return false;" >',
								'<span class="title">' + hi.i18n.getLocalization(temp.label) + '</span>',
							'</a>',
						'</div>',
					'</td>'
				].join('');
				tab = domConstruct.toDom(template);
				tab.name = k;
				tabsContainer.appendChild(tab);
				this.count++;
				tab.style.cursor = "pointer";
				options.elements.push(k);

				if (k == options.defaultTab) {
					this.select(k);
					if (div) {
						supra.common.showElement(div);
					}
					if (options.selectedClass) {
						dojo.addClass(dojo.byId(tab), options.selectedClass);
					}
				} else {
					if (div) {
						supra.common.hideElement(div);
					}
				}
				on(tab, 'click', function() {
					t.open(this.name);
				}); 
			}

			var tabsMenu = dijit.byId('tabs-menu');
			var tabsPref = this.getTabsPreference();
			// get preferences
			// if (!tabsPref) {
			// 	tabsPref = {};
			// 	for (var k in tabsConfig) {
			// 		tabsPref[k] = true;
			// 	}
			// } else {
			// 	tabsPref = JSON.parse(tabsPref);
			// }
			// console.log(tabsPref);

			if (tabsMenu) {
				var count = 0;
				var k;

				for (k in tabsConfig) {
					if (tabsPref[k]) {
						count++;
					}
				}
				for (k in tabsConfig) {
					// create tabs-menu
					var disabled = false;
					if (count === 1 && tabsPref[k]) {
						disabled = true;
					}
					var menu = new CheckedMenuItem({
						checked: tabsPref[k] || false,
						key: k,
						disabled: disabled,
						label: hi.i18n.getLocalization(tabsConfig[k].label)
					});
					on(menu, 'change', function(checked) {
						var k = this.key;
						var id = tabsConfig[k].id;
						var tab = dojo.byId(t.id + '_' + k);
						if (checked) {
							supra.common.showElement(tab);
							t.onAddTab();
						} else {
							supra.common.hideElement(tab);
							t.onRemoveTab();
							if (k === t.active) {
								var nk;
								if (nk = t.canOpenPrevious()) {
									t.open(nk);
								} else {
									if (nk = t.canOpenNext()) {
										t.open(nk);
									}
								}
							}
						}

						var menus = tabsMenu.getChildren();
						var checkedItems = menus.filter(function(item) {
							return item.checked;
						});
						if (checkedItems.length === 1) {
							checkedItems[0].set('disabled', true);
						} else {
							if (checkedItems.length === 2) {
								checkedItems.forEach(function(item) {
									item.set('disabled', false);
								});
							}
						}
						Tooltip.close('tabs-menu'); //force to close menu when clicked
						console.log('in onchange event');
						console.log(checked);
						tabsPref[k] = checked;
						Hi_Preferences.set('tabs_display', JSON.stringify(tabsPref));
						// Hi_Preferences.set('tabs_display', '');
						Hi_Preferences.send();
					});
					tabsMenu.addChild(menu);

					// hide tabs accordingly
					if (tabsPref[k] !== true) {
						var tab = dojo.byId(t.id + '_' + k);
						if (tab) {
							supra.common.hideElement(tab);
							this.count--;
						}
					}
				}
			}
			var tabsMenuLink = dojo.byId('more-tabs');
			if (tabsMenuLink) {
				on(tabsMenuLink, 'click', function(e) {
					res = Tooltip.open('tabs-menu', null, this, (typeof e != 'undefined'? e : null), 'below');
					Click.setClickFunction(function() {}, this, 'tabs-menu'); return false;
				});
			}
			// while ((k = Tabs.getId(i, options.elements)) !== false) {
			// 	i++;
			// 	tab = dojo.byId(tab_id + "_" + k);
			// 	div = dojo.byId(options.content + "_" + k);
			// 	if (!tab) break;
			// }
		},
		
		/**
		 * Returns tab instance if it's active, otherwise false
		 * 
		 * @param {String} tab_id Tab object ID
		 * @return Tab instance of false
		 * @type {Object}
		 */
		selected: function () {
			return this.active;
		},

		onAddTab: function() {
		},

		onRemoveTab: function() {
		},
		
		/**
		 * Select tab
		 * 
		 * @param {String} tab_id Tab object ID
		 * @param {Number} i Tab index which will be selected
		 */
		select: function(id) {
			this.active = id;
		},

		getTabsPreference: function() {
			var p = Hi_Preferences.get('tabs_display');
			var config = this.options.config;

			if (p && typeof p === 'string') {
				p = JSON.parse(p);
			} else {
				p = {};
				for (var k in config) {
					p[k] = config[k].defaultShow;
				}
			}

			return p;
		},

		canOpen: function(i) {
			var isHidden = false;
			var config = this.options.tabsConfig, temp;
			var tabsPref = this.getTabsPreference();

			for (var k in config) {
				temp = config[k];
				if (temp.id === i) {
					isHidden = tabsPref[k];
					break;
				}
			}

			return (!(this.id === 'grouping' && !Project.email_confirmed && parseInt(i, 10) === 5)) && !isHidden;
		},
		/**
		 * Open tab
		 * 
		 * @param {String} tab_id Tab object ID
		 * @param {Number} i Tab index which will be opened
		 * @param {Boolean} force Open even if validation fails
		 * @param {Boolean} nohash Don't add tab hash to the browser location
		 */
		open: function (id, force, nohash) {
			/* Bug #2001 - block if there is a new task opened */
			if (hi.items.manager.openedId() && hi.items.manager.openedId() < 0) return false;
			if (!Project.email_confirmed && id === 'team') return;

			nohash = nohash || false;
			var options = this.options, tab, div, j = options.start, k;
			if (!force && options.check.constructor == Function) {
				if (!options.check()) return;
			}
			if (options.onChange(id) === false) return;
			FileUpload.destroyAllWidgets();
			while ((k = this.options.elements[j]) !== undefined) {
				j++;
				tab = dojo.byId(this.id + "_" + k);
				if (!tab) break;
				div = dojo.byId(options.content + "_" + k);
				supra.common.hideElement(div);
				if (options.selectedClass) {
					dojo.removeClass(dojo.byId(tab), options.selectedClass);
				}
			}
			tab = dojo.byId(this.id + "_" + id);
			div = dojo.byId(options.content + "_" + id);
			if (options.selectedClass) {
				dojo.addClass(dojo.byId(tab), options.selectedClass);
			}
			if (div) {
				supra.common.showElement(div);
			}
			if (!nohash && (id in options.names)) {
				location.hash = "#" + options.names[id];
			}
			this.select(id);
		},

		openNext: function() {
			var options = this.options,
				elements = options && options.elements,
				currentIndex = 0,
				index = 0,
				active = this.active;

			if(elements && active !== undefined && active !== null){
				currentIndex = elements.indexOf(active);
				if(currentIndex < elements.length - 1){
					currentIndex++;
				}else{
					currentIndex = 0;
				}
			}

			if (this.canOpen(elements[currentIndex])) {
				this.open(elements[currentIndex]);
			} else {
				this.select(elements[currentIndex]);
				this.openNext();
			}
			this.open(elements[currentIndex]);
		},

		openPrevious: function() {
			var options = this.options,
				elements = options && options.elements,
				currentIndex = 0,
				index = 0,
				active = this.active;

			if (elements && active !== undefined && active !== null) {
				currentIndex = elements.indexOf(active);
				if(currentIndex > 0){
					currentIndex--;
				}else{
					currentIndex = elements.length - 1;
				}
			}

			if (this.canOpen(elements[currentIndex])) {
				this.open(elements[currentIndex]);
			} else {
				this.select(elements[currentIndex]);
				this.openPrevious();
			}
		},
		
		/**
		 * Check tab instance if its hash matches document hash and open tab if
		 * needed
		 * 
		 * @param {String} tab_id Tab object ID
		 */
		checkHash: function() {
			var options = this.options, i;
			var hash = location.hash;
			if (hash != "" && hash != "#") {
				for (i in options.names) {
					if (hash == "#" + options.names[i]) {
						if (i != this.selected()) {
							this.open(i);
						}
						break;
					}
				}
			} else if (options.openFirst) {
				this.open(options.start, false, true);
			} else {
				this.open(false);
			}
		}
	});

	return Tabs;
});
