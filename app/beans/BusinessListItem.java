package beans;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class BusinessListItem implements Serializable {
	private String domainName;
	private String logoHash;
	private String title;
	
	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getLogoHash() {
		return logoHash;
	}

	public void setLogoHash(String logoHash) {
		this.logoHash = logoHash;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getLogoPath() {
		if (StringUtils.isNotBlank(logoHash)) {
			return new StringBuilder().append("/avatar/business/").append(logoHash).append(".80.gif").toString();
		}
		
		return null;
	}
}
