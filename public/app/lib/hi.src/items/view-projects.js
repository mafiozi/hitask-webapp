/**
 * Instantiation of task lists (and the groups that contain them) on Project tab:
 * hi.items.ProjectListProjects.projects[]
 * hi.items.TaskListProjectUngrouped
 */
define([
	"hi/store",
	"hi/items/base",
	'dojo/aspect'
], function (hi_store, hi_items, dojo_aspect) {

	//After hi.store.init create project groups
	//dojo.connect(hi_store, "init", hi_store, function () {
	dojo.global.viewProjectTab = function(q) {
		// Create task groups (each color + default list) and set filters
		//return;
		var readStore = hi_store.ProjectCache,
			writeStore = hi_store.Project,
			query = q,//readStore.query(),

			
			groupSelf = 'project',
			groupCurrent = Hi_Preferences.get('currentTab', 'my', "string"),
			
			group = null,
			taskList  = null,
			projectList = null,

			container = dojo.byId(Project.getContainer("tasksList")),
			sortNode = dojo.query('#main .submenu .toolbar td.sorting')[0],
			// sortProjectNode = dojo.query('#main .submenu .toolbar td.sorting.projectSorting')[0];
			// sortProjectColumn = dojo.query('#main .submenu .toolbar col.projectSorting')[0];
			// sortProjectNode = dojo.byId('sorting');
			sortProjectNode = dojo.byId('projectSorting');

		if (groupCurrent == groupSelf) {
			var myP = dojo.connect(hi_store,'afterLoad',function() {
				if (groupCurrent == groupSelf ) {
					var groups = hi_items.ProjectListProjects.projects;
					/* #2509 initiateTab(groups,[hi_items.TaskListProjectUngrouped]); */
					groups.forEach(function(project,i) {
						project.lists[0].set('rendered',true);
					});
					dojo.global.loadTaskItemsState();
				}
				dojo.disconnect(myP);
			});
			dojo.global.showElement(sortProjectNode);
			// dojo.global.showElement(sortProjectColumn);
			// dojo.global.hideElement(sortNode);
		} else {
			dojo.global.hideElement(sortProjectNode);
			// dojo.global.hideElement(sortProjectColumn);
			// dojo.global.showElement(sortNode);
		}
		/* #2568 - filter projects on search query */
		dojo.global.filterProject = function(item) {
			if (Hi_Search.isFormVisible()) {
				//console.log("ITEM",item.title);
				var regex = Hi_Search.getSearchRegEx(),
					matches = false;
				/*#2331 */if (!regex) return true;
				if (item.title.match(regex)) {
					matches = true;
				} else if (item.message && item.message.match(regex)) {
					matches = true;
				} 
				
				if (!matches) {
					// search is active, but didn't matched
					return false;
				}
			}	
			return true;
		};
		
		/* #2568 - an item in the project matches search criteria */
		dojo.global.filterProjectList = function(project) {
			if (Hi_Search.isFormVisible()) {
				var list = project.lists[0],
					items = list.itemWidgets;

				var regex = Hi_Search.getSearchRegEx(),
				matches = false;
				/*#2331 */if (!regex) return true;
				for (var i in items) {
					if (items[i].title.match(regex)) {
						return true;
					} else if (items[i].message && items[i].message.match(regex)) {
						return true;
					}
				}
				return false;
			} 
			return true;
		};
		
		// On data change update project list
		q.observe(function (/*Object*/object, /*Number*/removed, /*Number*/inserted, /*Object*/diff) {
			if (object.category == hi.widget.TaskItem.CATEGORY_PROJECT) {
				if (removed == -1 && inserted > -1) {
					//Added
					/* #2571 - project may already be rendered */
					if (!hi.items.ProjectListProjects.get(object.id))
					{
						var newProject = createProject(object, inserted);
						/* #3052 - render list if this tab is active */
						if (!newProject.lists[0].get('disabled') && !newProject.lists[0].get('rendered')) newProject.lists[0].set('rendered',true);
					}
				} else if (removed > -1 && inserted == -1) {
					//Removed
					if (hi.items.ProjectListProjects.get(object.id)) projectList.remove(object.id);
				} else if (removed > -1 && inserted > -1) {
					//Changed
					//console.log("CHANGED",[object.title,object.id,diff]);
					var project = projectList.get(object.id);
					if (project) {
						/* #3052 - comment this out, otherwise we need more than one refresh before changes are registered if (!diff || diff['changed'] == false) return;*/
						for (var key in diff) {
							project.set(key, object[key]);
						}
						/* #2283 - reorder if necessary */
						projectList.remove(project,true);
						projectList.domNode.removeChild(project.domNode);
						projectList.addNew(project);
					}
				}
				
			}
		}, 'deep');
		//};

		function createProject (item, index) {
			// summary:
			//		Create new project in list
			
			var projectId = item.id;
			var data = dojo.mixin({
				"projectId": item.id,
				"emptyVisible": true,	// shown if there are no tasks in it
				"disabled": groupSelf != groupCurrent,
				"preferencesGroup": groupSelf,
				"sortable": Project.getSorting() == Project.ORDER_PRIORITY,
				"isInProjectList": true,
				'colorValue': item.color_value,
				'colorValuePrev': item.color_value,
				// "permission": item.permission
			}, item);

			if(!data.hasOwnProperty('permissions')){
				data.permissions = [];
			}

			delete(data.id); // used by dijit
			
			// #1953: for some reason IE8 does not like 'children' attribute and fails if it presents.
			if (dojo.isIE == 8) {
				delete(data.children);
			}
			
			var group = new hi.widget.Project(data);
			
			var taskList = new hi.widget.TaskList({
				"readStore": readStore,
				"writeStore": writeStore,
				"query":q,//groupSelf == groupCurrent?dojo.global.sharedQuery:null,
				
				"parentId": projectId,
				"disabled": groupSelf != groupCurrent,
				
				"filterType": "any", // if most outer parent matched then this item should be in the list
				"filter": function (item, strict) {
					// summary:
					//		Filter items which should be in the list. Returns true on success,
					//		false on failure
					// item:
					//		Item data
					/* #3229 - check there are any instances left */
					if (item.recurring) {
						if (!this.calculateInstanceDate(item)) return false;
					}
					if (item.parent == projectId) return true;
					if (!strict && this.filterMatches(item.parent)) return true;
					return false;
				}
			});
			
			taskList.placeAt(group);
			projectList.addNew(group);
			return group;
		}
		
		projectList = hi_items.ProjectListProjects = new hi.widget.ProjectList({
			"allowSorting": true
		});
		dojo.addClass(projectList.domNode, "grouped_" + groupSelf);

		// Ungrouped items
		hi_items.TaskListProjectUngrouped = new hi.widget.TaskList({
			"readStore": readStore,
			"writeStore": writeStore,
			"query": q,//groupSelf == groupCurrent?dojo.global.sharedQuery:null,
			mainList:true,
			"disabled": groupSelf != groupCurrent,
			
			"initialyCollapsed": true, // on load list should be collapsed "xxx items not assigned"
			"localeItemCollapsed": hi.i18n.getLocalization("center.item_not_in_project"),
			"localeItemsCollapsed": hi.i18n.getLocalization("center.items_not_in_project"),
			/* Bug #2075 - change filtertype to 'top', top-ancestor of sub item determines if in project-ungrouped */
			"filterType": "top", // if most outer parent matched then this item should be in the list
			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				// strict:
				//		If true then don't check ancestors
				/* #3229 - check there are any instances left */
				if (item.recurring) {
					if (!this.calculateInstanceDate(item)) return false;
				}
				if (!item.parent) return true;
				if (hi.items.manager.get(item.parent, "category") == 0) return false;
				
				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			}
		});
		
		hi_items.ProjectListProjects.placeAt(container);
		hi_items.TaskListProjectUngrouped.placeAt(container);
		
		dojo.connect(hi.items.manager, "groupingChange", hi.items.manager, function (group, groupPrev) {
			// summary:
			//		On group change enable/disable lists
			var groups;

			if (group == groupSelf && groupPrev != groupSelf) {
				dojo.global.showElement(sortProjectNode);
				// dojo.global.showElement(sortProjectColumn);
				// dojo.global.hideElement(sortNode);
				groups = hi_items.ProjectListProjects.projects;
				refreshTab(groups,[hi_items.TaskListProjectUngrouped],true);
				hi.items.ProjectListProjects.reset();
			} else if (group != groupSelf && groupPrev == groupSelf) {
				// dojo.global.showElement(sortNode);
				dojo.global.hideElement(sortProjectNode);
				// dojo.global.hideElement(sortProjectColumn);
				// domClass.remove(sortNode, 'inProject');
				groups = hi_items.ProjectListProjects.projects;
				for (var i = 0, count = groups.length; i < count; i++) {
					groups[i].set("disabled", true);
				}
				hi_items.TaskListProjectUngrouped.set("disabled", true);
			}
			
			groupCurrent = group;
		});
		
		dojo.connect(Project, "changeSorting", Project, function () {
			// summary:
			//		On sorting change empty lists
			
			if (groupCurrent == groupSelf) {
				var groups = hi_items.ProjectListProjects.projects,
					lists = null, k = 0, kk = 0;
				
				for (var i=0, count=groups.length; i<count; i++) {
					lists = groups[i].lists;
					k = 0;
					kk = lists.length;
					
					for (; k<kk; k++) {
						lists[k].reset({"fill": false});
					}
					
					//Disable, because after sorting we need to fill all lists - it is then enabled on redraw()
					groups[i].set("disabled", true);
				}
				hi_items.TaskListProjectUngrouped.reset({"fill": false});
			}
		});
		
		dojo.connect(Project, "changeSorting", Project, function () {
			// summary:
			//		After sorting update all lists
			
			if (groupCurrent == groupSelf) {
				var groups = hi_items.ProjectListProjects.projects;
				
				for (var i=0, count=groups.length; i<count; i++) {
					groups[i].set("disabled", true);
				}
				hi_items.TaskListProjectUngrouped.set("disabled", true);
			}
		});
		
		dojo.connect(hi.items.manager,'filterChange',function() {
			if (groupCurrent == groupSelf) {
				hi_items.ProjectListProjects.removeAll();
				var groups = hi_items.ProjectListProjects.projects;
			
				for (var i=0, count=groups.length; i<count; i++) {
					groups[i].set("disabled", true);
				}
				/* #2571 *///hi_items.ProjectListProjects.removeAll();
				hi_items.TaskListProjectUngrouped.set("disabled", true);
			}
		});

		/* This is invoked after refresh data store on filter selection change 
		 * This should just initiate the query observers on the lists and enable the lists
		 */
		dojo.connect(hi.items.manager, "redraw", hi.items.manager, function (filterChange) {
			// summary:
			//		Some major change happened, redraw lists
			var groups = hi_items.ProjectListProjects.projects;
			/* #2509 
			 * filter change flushes data store and thus project (because although they are containers for items, like a list,
			 * they are items themselves).  So observer automatically adds them back on
			 */
			
			if (groupCurrent == groupSelf) {
				
				if (filterChange) {
					groups.forEach(function(p) {
						p.lists[0].set('rendered',true);
					});
					refreshTab([],[hi_items.TaskListProjectUngrouped]);
					return;
				}
				//console.log('redraw',groups.length);
				/* #2331 - rereshTab on searchChange */
				refreshTab(groups,[hi_items.TaskListProjectUngrouped]);
				hi.items.ProjectListProjects.reset();
			}
		});
		
		hi.items.manager.getTaskLists[groupSelf] = function () {
			// summary:
			//		Return all task lists for this group
			
			var list = [hi_items.TaskListProjectUngrouped],
				projects = hi_items.ProjectListProjects.projects,
				i = 0,
				imax = projects.length;
			
			for (; i<imax; i++) list = list.concat(projects[i].lists);
			
			return list;
		};
	
	};

});