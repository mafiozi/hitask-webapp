package utils;

import org.apache.commons.lang.StringUtils;

import play.Play;

public class Constants {
	public static final String API_URL = new StringBuilder().append(Play.configuration.getProperty("api.schemeName"))
		.append("://").append(Play.configuration.getProperty("api.host"))
		.append(Play.configuration.getProperty("api.baseUrl")).toString();
	
	public static final String APP_BASE_URL = Play.configuration.getProperty("application.baseUrl", "");
	public static final String APP_BASE_URL_SECURE = StringUtils.replace(APP_BASE_URL, "http://", "https://");
	public static final String APP_BASE_URL_STRIPPED = StringUtils.strip(APP_BASE_URL, "/");
	
	public static final String API_SESSION_ID_ARG = "session_id";
	public static final String API_KEY_ARG = "api_key";
	public static final String API_INTERNAL_ARG = "internal_api";
	public static final String API_INTERNAL_RANDOM_ARG = "internal_api_random";
	
	public static final String SESSION_ID_KEY = "session_id";
	public static final String SESSION_USER_ID_KEY = "user_id";
	public static final String SESSION_LOGIN_ID_KEY = "login_id";
	public static final String SESSION_LOGIN_KEY = "login";
	public static final String SESSION_USERNAME_KEY = "username";
	public static final String SESSION_USER_LEVEL_KEY = "level";
	public static final String SESSION_ACCOUNT_TYPE_KEY = "account_type";
	public static final String SESSION_EXTERNAL_USER_ID_KEY = "external_user_id";
	public static final String SESSION_FULLNAME_KEY = "fullname";
	
	public static final String BUSINESS_LIST_CACHE_KEY = "business_list";
	public static final String BUSINESS_LIST_CACHE_EXPIRE = "1min";
	
	public static final String[] RESERVED_DOMAINS = StringUtils.stripAll(StringUtils.split(Play.configuration.getProperty("hitask.reservedDomains", ""), ","));
	
	public static final String DEFAULT_COOKIE_DOMAIN = Play.configuration.getProperty("application.defaultCookieDomain", "");
	public static final String DEFAULT_DOMAIN = StringUtils.strip(DEFAULT_COOKIE_DOMAIN.trim(), ".");
	public static final String DEFAULT_SECURE_DOMAIN = "secure." + DEFAULT_DOMAIN;
	
	public static String getMixpanelId() {
    	return Play.configuration.getProperty("mixpanel.id");
    }
}
