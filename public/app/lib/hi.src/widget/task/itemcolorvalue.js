define([
	"dojo/_base/array", // array.forEach array.map
	"dojo/_base/declare", // declare
	"dojo/cldr/supplemental", // cldrSupplemental.getFirstDayOfWeek
	"dojo/date", // date
	"dojo/date/locale",
	"dojo/html",
	"dojo/on",
	"dojo/dom", // dom.setSelectable
	"dojo/dom-class", // domClass.contains
	"dojo/_base/event", // event.stop
	"dojo/_base/lang", // lang.getObject, lang.hitch
	"dojo/_base/sniff", // has("ie") has("webkit")
	"dojo/string", // string.substitute
	"dojo/_base/window", // win.doc.createTextNode
	"dijit/_Widget",
	"dijit/_Templated",
	"dojo/Evented",
	"dojo/text!./itemcolorvalue.html"
], function(array, declare, cldrSupplemental, date, local, dojoHtml, dojoOn, dom, domClass, event, lang, has, string, win,
			_WidgetBase, _TemplatedMixin, _Evented, template){
	
	//Replace localization string in template instead of using attributes
	template = template.replace(/\{#([^#]+)#\}/g, function (all, str) {
		var localized = hi.i18n.getLocalization(str.trim()) || '';
		if (!localized) console.warn('Missing localization for "' + str + '"');
		return localized;
	});
	
	// module:
	//		hi/widget/ItemColorValue
	// summary:
	//		Item color value picker
	
	var ItemColorValue = declare("hi.widget.ItemColorValue", [_WidgetBase, _TemplatedMixin, _Evented], {
		templateString: template,
		// Default color value will be prepended in 'postMixInProperties' method.
		//			Red, 	Orange,		 Yellow, 	green, 		purple, 	brown (blue #2aaef5,) 
//		colors: ['#fc2f6a', '#fd9426', '#fecb2e', '#68d844', '#cb77df', '#a18460'], //original 
		colors: ['#fc2f6a', '#fd9426', '#fecb2e', '#55CE2E', '#cb77df', '#a18460'],
		cellTemplate: 
			'<td class="dijitPaletteCell" tabindex="-1" data-color="{color}" role="gridcell">\
				<span class="dijitInline dijitPaletteImg">\
					<img src="{blankGif}" width="15px" height="15px" alt="{color}" \
						class="dijitColorPaletteSwatch" style="background-color: {color}" />\
				</span>\
			</td>',
		activeColorClassName: 'dijitPaletteCellSelected',
		
		color: null,
		_setColorAttr: function(value) {
			if (testHexColor(value)) {
				var valueToLower = value.toLowerCase();
				
				for (var i = 0; i < this.colors.length; i++) {
					if (this.colors[i].toLowerCase() == valueToLower) {
						this._set('color', value);
						var self = this;
						dojo.forEach(dojo.query('td.dijitPaletteCell', this.domNode), function(td) {
							dojo.removeClass(td, self.activeColorClassName);
							if (dojo.getAttr(td, 'data-color').toLowerCase() == valueToLower) {
								dojo.addClass(td, self.activeColorClassName);
							}
						});
						this.emit('change', this.get('color'));
					}
				}
			}
		},
		
		postMixInProperties: function() {
			this.inherited(arguments);
			this.colors = [CONSTANTS.defaultColorValue].concat(this.colors);
		},
		
		buildRendering: function() {
			var self = this;
			this.inherited(arguments);
			
			var tbody = dojo.query('tr', this.domNode);
			tbody = tbody[0];
			var innerHtml = '';
			
			for (var i = 0; i < this.colors.length; i++) {
				innerHtml += this.cellTemplate.replace(/\{color\}/g, this.colors[i]).replace(/\{blankGif\}/g, this._blankGif);
			}
			
			dojoHtml.set(tbody, innerHtml, {parseContent: true});
		},

		handleColorCellClick: function(e) {
			if (e.target && e.target.tagName && e.target.tagName.toLowerCase() == 'img') {
				this.set('color', e.target.alt);
			}
		}
	});
	
	ItemColorValue.colors = ['#fc2f6a', '#fd9426', '#fecb2e', '#55CE2E', '#cb77df', '#a18460'];
	return ItemColorValue;
});