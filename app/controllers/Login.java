package controllers;

import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;

import api.ApiCall;
import api.ApiCallException;
import beans.ApiConfig;
import beans.BusinessListItem;
import beans.UserSession;
import oauth.OAuth2Client;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.libs.Crypto;
import play.mvc.Before;
import play.mvc.Http.Cookie;
import play.mvc.Util;
import play.mvc.results.Result;
import utils.Constants;
import utils.OAuth2Endpoints;

public class Login extends ApplicationSecured {
	private static final String COMMUNITY_SSO_NONCE_COOKIE_NAME = "hcsn";
	private static final String COMMUNITY_SSO_SSO_ARG = "sso";
	private static final String COMMUNITY_SSO_SIG_ARG = "sig";
	private static final String OAUTH_QUERY_STRING_ARG = "oauthqs";
	private static final String OAUTH_APPROVAL_TOKEN_ARG = "oat";
	private static final String OAUTH_APPROVAL_APP_NAME_ARG = "oaan";
	
	@Before
	static void putApiConfig() {
		renderArgs.put("apiConfig", new ApiConfig());
	}

	public static void index() {
		processCommunitySsoIfRequired();
		processOauthQsIfRequired();
		if (isLoggedIn() || rememberMeCookieSent()) {
			redirectToApp();
		}
		
		BusinessListItem businessItem = findAppropriateBusinessItem();
		OAuth2Client[] oauth2clients = OAuth2Endpoints.getClients();
		if (params.get(OAUTH_QUERY_STRING_ARG) != null) {
			// https://community.hitask.com/t/fb-google-hitask-login-issues/69/7
			oauth2clients = new OAuth2Client[] {};
		}
		render(businessItem, oauth2clients);
	}

	public static void out() {
		doApiLogout();
		doLogout();
		
		// FIXME: Temp fix.
		//index();
		render();
	}
	
	public static void oauthApproval() {
		String approvalToken = session.get(OAUTH_APPROVAL_TOKEN_ARG);
		String appName = session.get(OAUTH_APPROVAL_APP_NAME_ARG);
		String qs = params.get(OAUTH_QUERY_STRING_ARG);
		String action = ApiCall.API_OAUTH_APPROVE + "?" + OAUTH_QUERY_STRING_ARG + "=" + qs;
		String r = Crypto.encryptAES(ApiCall.API_OAUTH_AUTH + "?" + Crypto.decryptAES(qs));
		
		session.remove(OAUTH_APPROVAL_TOKEN_ARG);
		session.remove(OAUTH_APPROVAL_APP_NAME_ARG);
		
		if (StringUtils.isBlank(appName) || StringUtils.isBlank(approvalToken) || StringUtils.isBlank(qs)) {
			badRequest("You should start OAuth flow from the first step.");
		} else {
			render(approvalToken, action, appName, r);
		}
	}
	
	public static void oauthApprovalDenied() {
		render();
	}
	
	private static void processOauthQsIfRequired() {
		try {
			String qs = params.get(OAUTH_QUERY_STRING_ARG);
			if (StringUtils.isNotBlank(qs)) {
				qs = Crypto.decryptAES(qs);
				sendRedirectToAfterLoginCookie(ApiCall.API_OAUTH_AUTH + "?" + qs);
			}
		} catch (Exception e) {
			Logger.error(e, "error occurred while processing oauth query string");
		}
	}
	
	/**
	 * https://meta.discourse.org/t/official-single-sign-on-for-discourse/13045
	 * https://meta.discourse.org/t/sso-example-for-jsp/22786
	 */
	private static void processCommunitySsoIfRequired() {
		try {
			String sig = params.get(COMMUNITY_SSO_SIG_ARG);
			String payload = params.get(COMMUNITY_SSO_SSO_ARG);
			String key = Play.configuration.getProperty("community.sso.secret");
			
			if (sig != null && payload != null) {
				if (key == null) {
					Logger.error("requested sso but secret key not found!");
				} else {
					String calc = "";
					try {
						calc = calcCommunitySsoChecksum(key, payload);
					} catch (Exception e) {
						Logger.error(e, "error occurred while calculating sso hmac");
					}
					if (!calc.equals(sig)) {
						Logger.error("received sso [%s] and sig [%s], calculated sig [%s] using key [%s]", payload, sig, calc, key);
					} else {
						String urlDecode = URLDecoder.decode(payload, "UTF-8");
						String nonce = new String(Base64.decodeBase64(urlDecode));
						
						response.setCookie(COMMUNITY_SSO_NONCE_COOKIE_NAME, Crypto.encryptAES(nonce));
					}
				}
			} else {
				communitySsoForgetNonce();
			}
		} catch (Exception e) {
			Logger.error(e, "unexpected error occurred while processing sso");
		}
	}
	
	@Util
	public static String getCommunitySsoNonce() {
		String nonce = null;
		try {
			Cookie c = request.cookies.get(COMMUNITY_SSO_NONCE_COOKIE_NAME);
			if (c != null && StringUtils.isNotBlank(c.value)) {
				nonce = Crypto.decryptAES(c.value);
			}
		} catch (Exception e) {
			Logger.error(e, "error occurred while getting nonce from sso cookie");
		}
		return nonce;
	}
	
	@Util
	public static void communitySsoProcessRedirectIfRequired(UserSession userBean) {
		String nonce = getCommunitySsoNonce();
		String key = Play.configuration.getProperty("community.sso.secret");
		String redirectUrl = Play.configuration.getProperty("community.sso.redirect_url");
		
		if (nonce != null) {
			communitySsoForgetNonce();
			
			if (userBean == null) {
				Logger.error("redirect sso but passed user bean is null!");
			} else if (key == null) {
				Logger.error("redirect sso but secret key not found!");
			} else if (redirectUrl == null) {
				Logger.error("redirect sso but redirect url not found!");
			} else {
				try {
					String urlEncode = nonce
						+ "&name=" + URLEncoder.encode(userBean.getFullName(), "UTF-8")
						+ "&username=" + URLEncoder.encode(userBean.getLogin(), "UTF-8")
						+ "&email=" + URLEncoder.encode(userBean.getAnyEmail(), "UTF-8")
						+ "&external_id=" + URLEncoder.encode(userBean.getId() + "", "UTF-8");
					if (StringUtils.isBlank(userBean.getEmailConfirmed())) {
						urlEncode += "&require_activation=true";
					}
					String avatar = userBean.getAvatar();
					if (avatar != null) {
						urlEncode += "&avatar_force_update=true&avatar_url=" + URLEncoder.encode(avatar, "UTF-8");
					}
					String urlBase64 = new String(Base64.encodeBase64(urlEncode.getBytes("UTF-8")));
					int length = 0;
					int maxLength = urlBase64.length();
					final int step = 60;
					String urlBase64Encode = "";
					while (length < maxLength) {
						urlBase64Encode += urlBase64.substring(length, length + step < maxLength ? length + step : maxLength) + "\n";
						length += step;
					}
					
					redirectUrl += "?sso=" + URLEncoder.encode(urlBase64Encode, "UTF-8") + "&sig=" + calcCommunitySsoChecksum(key, urlBase64Encode);
					redirect(redirectUrl);
				} catch (Exception e) {
					if (e instanceof Result) {
						throw (Result)e;
					} else {
						Logger.error(e, "error occurred while processing sso redirect");
					}
				}
			}
		}
	}
	
	@Util
	public static String calcCommunitySsoChecksum(String macKey, String macData) throws Exception {
		Mac mac = Mac.getInstance("HmacSHA256");
        byte[] keyBytes = macKey.getBytes("UTF-8");
        byte[] dataBytes = macData.getBytes("UTF-8");
        SecretKeySpec secretKey = new SecretKeySpec(keyBytes, "HmacSHA256");
        mac.init(secretKey);
        byte[] doFinal = mac.doFinal(dataBytes);
        byte[] hexBytes = new Hex().encode(doFinal);
        return new String(hexBytes);
	}
	
	@Util
	public static void communitySsoForgetNonce() {
		response.removeCookie(COMMUNITY_SSO_NONCE_COOKIE_NAME);
	}
	
	private static void doApiLogout() {
		String sessionId = getSessionId();
		if (StringUtils.isNotBlank(sessionId)) {
			new ApiCall(sessionId).logout();
		}
	}
	
	private static void redirectToApp() {
		BusinessListItem businessItem = findAppropriateBusinessItem();
		String url = businessItem != null ? constructAppUrl(businessItem.getDomainName()) : null;
		
		if (StringUtils.isNotBlank(url)) {
			redirect(url);
		} else {
			Application.index();
		}
	}
	
	private static String constructAppUrl(String subDomain) {
		String url = null;
		
		try {
			URI uri = new URI(Constants.APP_BASE_URL);
			String scheme = uri.getScheme();
			final String https = "https";
			
			if (!scheme.toLowerCase().equals(https)) {
				scheme = https;
			}
			
			return new StringBuilder().append(scheme).append("://").append(subDomain).append(".").append(uri.getHost()).append("/app/").toString();
		} catch (Exception e) {
			Logger.warn(e, "Could not parse application.baseUrl from configuration");
		}
		
		return url;
	}
	
	private static BusinessListItem findAppropriateBusinessItem() {
		String subDomain = extractSubDomain(request.host);
		
		if (StringUtils.isNotBlank(subDomain) && !isReservedDomain(subDomain)) {
			BusinessListItem[] bizs = getBusinessList();
			
			if (bizs == null) {
				return null;
			}
			
			for (BusinessListItem biz : bizs) {
				if (biz.getDomainName() != null && subDomain.equalsIgnoreCase(biz.getDomainName())) {
					return biz;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Checks if specified domain is reserved by application.
	 * @param domain
	 * @return
	 */
	private static boolean isReservedDomain(String domain) {
		if (Constants.RESERVED_DOMAINS != null) {
			for (String reserved : Constants.RESERVED_DOMAINS) {
				if (StringUtils.isNotBlank(reserved) && reserved.equalsIgnoreCase(domain)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Extracts top level domain from Host value provided by HTTP request.
	 * @param host
	 * @return
	 */
	private static String extractSubDomain(String host) {
		if (StringUtils.isNotBlank(host)) {
			String[] parts = StringUtils.split(host, '.');
			if (parts != null && parts.length > 0) {
				return parts[0];
			}
		}
		
		return null;
	}
	
	/**
	 * Fetches all available business accounts from API.
	 * @return
	 */
	private static BusinessListItem[] getBusinessListFromApi() {
		ApiCall api = new ApiCall();
		
		try {
			return api.getBusinessList();
		} catch (ApiCallException apie) {
			logDetailedError(apie, "while performing internal API request to get business list");
		}
		
		return null;
	}
	
	/**
	 * Extracts all available business accounts from Cache.
	 * @return
	 */
	private static BusinessListItem[] getBusinessListFromCache() {
		return (BusinessListItem[])Cache.get(Constants.BUSINESS_LIST_CACHE_KEY);
	}
	
	/**
	 * Returns all available business accounts.
	 * @return
	 */
	private static BusinessListItem[] getBusinessList() {
		BusinessListItem[] list = getBusinessListFromCache();
		
		if (list == null && (list = getBusinessListFromApi()) != null) {
			Cache.set(Constants.BUSINESS_LIST_CACHE_KEY, list, Constants.BUSINESS_LIST_CACHE_EXPIRE);
		}
		
		return list;
	}
}