/*
 * This file is packed into /lib/dojo.1.5.0.hitask/arena/arena-all.js
 */

/**
 * Create a namespace
 */
dojo.ns = function (ns) {
	ns = ns.split(/[\/\.]/);
	
	var context = dojo.global;
	for(var i=0,ii=ns.length; i<ii; i++) {
		if (!context[ns[i]]) context[ns[i]] = {};
		context = context[ns[i]];
	}
};

define(function () {
	
	dojo.ns("arena.nls.arena-all_en-us");
	return {};
	
});

/*
dojo.provide("arena.arena-all");

//Include here to prevent from loading separately
dojo.provide("arena.nls.arena-all_en-us");

dojo.provide("dijit.nls.loading");
dijit.nls.loading._built=true;

dojo.provide("dijit.nls.loading.en_us");
dijit.nls.loading.en_us={"loadingState":"Loading...","errorState":"Sorry, an error occurred"};

dojo.provide("dijit.nls.common");
dijit.nls.common._built=true;

dojo.provide("dijit.nls.common.en_us");
dijit.nls.common.en_us={"buttonOk":"OK","buttonCancel":"Cancel","buttonSave":"Save","itemClose":"Close"};
*/