/**
 * The task list that contains a tooltip item.  It is a slightly modified (extended) version of hi/widget/task/list
 */
define([
	"dojo/_base/array", // array.forEach array.map
	"dojo/_base/declare", // declare
	"dojo/cldr/supplemental", // cldrSupplemental.getFirstDayOfWeek
	"dojo/date", // date
	"dojo/aspect", // aspect
	"dojo/Deferred", // Deferred
	"dojo/date/locale",
	"dojo/dom", // dom.setSelectable
	"dojo/dom-class", // domClass.contains
	"dojo/_base/event", // event.stop
	"dojo/_base/lang", // lang.getObject, lang.hitch
	"dojo/_base/sniff", // has("ie") has("webkit")
	"dojo/string", // string.substitute
	"dojo/_base/window", // win.doc.createTextNode
	"dijit/_Widget",
	"dijit/_Templated",
	"dojo/text!./listtooltip.html",
	"hi/widget/task/item",
	"hi/widget/task/list",
	'hi/widget/task/item_DOM'
], function(array, declare, cldrSupplemental, date, aspect, Deferred, local, dom, domClass, event, lang, has, string, win,
			_WidgetBase, _TemplatedMixin, template, TaskItem, TaskList,item_DOM){
	// module:
	//		hi/widget/List
	// summary:
	//		Task list which updates itself on store QueryResults data change
	
	
	var VALIDATION_SUCCESS = function () { return true; };
	
	
	var TaskListTooltip = declare("hi.widget.TaskListTooltip", [TaskList], {
		
		// Template for the list
		templateString: template,
		
		// listName: String
		//		Default task name sufix for hi.items.manager
		listName: "tooltip",

		addNew: function (properties, parentTaskItem) {
			// summary:
			//		Add new item form to the list
			// properties: Object
			//		New item properties
			// parentTaskItem: Object
			//		Optional, parent task item
			//		Needed to correctly find newly created item
			
			/* Bug #1945 - A negative ID implies a new item to edit is already opened*/
			/* Bug #1869 - we also need to check in all projects and check for opened items*/
			if (hi.items.manager.openedId() && hi.items.manager.openedId() < 0) return false;
			// hi.items.manager.collapseOpenedItems(true,true);

			properties = properties || {};
			// If this is a new item, can't add another one under it - Bug #1945 - do not know if this condition is ever met
			if (properties.parent && properties.parent < 0) return false;
			
			var parent_data = properties.parent ? hi.items.manager.get(properties.parent) : null;
			/*
			 * #3305
			 * use newItemParentId to determine whether or not this item is initially completed, if, for example,
			 * it is corrected as a sub item in a completed list
			 */
			var parentTaskId = parentTaskItem.task && parentTaskItem.task.taskId?parentTaskItem.task.taskId : null;
			var newItemParentId = properties.parent || parentTaskId || null;
			var data = dojo.mixin({}, {
				"id": --hi.widget.TaskItem.ID_COUNTER,
				"parent": this.parentId || 0,
				completed:newItemParentId&&hi.items.manager.getIsCompleted(newItemParentId,this.calculateInstanceDate(newItemParentId))?1:0,
				"time_last_update": time2strAPI(new Date(0)), // start of the epoch
				"time_create": time2strAPI(new Date(0)), // start of the epoch
				
				"starred": false,
				"title": "",
				"message": "",
				"participants": [],
				"tags": [],
				"category": hi.widget.TaskItem.CATEGORY_TASK,
				"color": 0,
				//"completed": false,
				"reminder_time": 0,
				"reminder_time_type": "m",
				"reminder_enabled": false,
				"time_track": Hi_Preferences.get('enable_time_tracking', false, 'bool'),
				"time_est": 0,
				"recurring": 0,
				'recurring_interval':1,
				"priority": 20000,
				"is_all_day": true,
				"assignee": 0,
				"shared": Hi_Preferences.get('default_shared', false, 'bool'),
				"my": true,
				"user_id": Project.user_id
			}, properties);
			
			hi.items.manager.set(data, null, true);
			
			// Collapse parent item and render children
			if (data.parent) {
				/* 
				 * #3362 - pull item by instance if it is recurring
				 */
				// var name = this.listName === 'tooltip' ? 'tooltip' : null;
				var name = 'tooltip';
				var parent_item = hi.items.manager.item(data.parent, parentTaskItem.task ? (parentTaskItem.task.eventdate || name) : name);
				if (parent_item) {
					item_DOM.collapse(parent_item);
					item_DOM.expandChildren(parent_item);
				}
			}
			var itterations = 50; // 3 seconds
			var expandItem = function() {
				var item = parentTaskItem && parentTaskItem.has ? parentTaskItem.has(data.id, true) : hi.items.manager.item(data.id);
				if (item) {
					item_DOM.expand(item);
					item_DOM.openEdit(item);
				} else {
					itterations--;
					if (itterations) {
						// #3608:
						setTimeout(function() {expandItem();}, dojo.isSafari && dojo.isSafari >= 6.1 ? 1 : 60);
					} else {
						//Something went terribly wrong, remove item
						// hi.items.manager.remove(data.id);
					}
				}
			};
			expandItem();
		},
		
		add: function (item, index, instance_id,/*#2556 */inTaskEditTooltip) {
			// summary:
			//		Add item to the list and expand it immediatelly
			// item:
			//		Item to add
			// index:
			//		Index where to insert item
			// instance_id:
			//		Task event instance date 
			this.rendered = true;
			var widget = TaskList.prototype.add.apply(this, arguments),
				def = new Deferred();
			
			if (widget) {
				//Overwrite item expand/collapse
				widget._setOpenedAttr = this.taskSetOpenedAttr;

				//Handle future clicks
				//widget.handleItemClick();
				/* This ultimately force hi.items.manager.collapseOpenItems() to be called twice -
				 * which means after a tooltip on the calendar has been opened, no other item can be expanded
				 item_DOM.handleItemClick(widget);*/
				var self = this;
				//Open immediatelly
				setTimeout(function () {
					//widget.set("opened", true);
					self.taskSetOpenedAttr(widget, true);
					def.resolve(widget);
				}, 1);
			}
			
			return def;
		},
		
		taskSetOpenedAttr: function (widget, opened) {
			//Don't open if some other item is being edited
			if (opened && hi.items.manager.editing()) {
				return;
			}
			var wp;
			var domNode = dojo.byId('itemNode_' + widget.widgetId);
			if (!domNode) return;

			widget.opened = opened;
			/* This DOM node may already be removed, so we may not need to apply CSS to it */
			if (domNode) {
				dojo.toggleClass(domNode, "highlight", opened);
			}
			//item_DOM._setOpenedAttr(widget,opened);//this._set("opened", opened);
			if (opened) {
				item_DOM.renderExpandedView(widget);
				
				//Close all other tasks
				//hi.items.manager.collapseOpenedItems();
				
				hi.items.manager.opened(widget.taskId || widget.id, true); // TaskId will be empty for new item
				wp = widget.propertiesView;
				if (wp) {
					wp.expand(true); // Expand instantly without animation
					aspect.after(wp, 'handleModifyClick', function() {
						dijit.popup.reposition('tooltip_taskedit_TooltipDialog');
					});
					if (wp.newItemLinkNode) {
						dojo.global.hideElement(wp.newItemLinkNode.domNode);
					}
				}
				// #2049
				dojo.addClass(widget.propertiesView.newItemLinkNode, 'hidden');
				dojo.addClass(widget.propertiesView.deleteLinkNode, 'timetable');
				
					
			} else {
				//Don't collapse item UI, because tooltip is using fade out animation
				hi.items.manager.removeItem(widget.taskId,'tooltip');
				hi.items.manager.opened(widget.taskId || widget.id, false); // TaskId will be empty for new item
			}
		}
		
	});
	
	return TaskListTooltip;

});