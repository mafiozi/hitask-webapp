define([
	"hi/store"
], function (hi_store) {
	//hi_store === hi.store
	
	hi_store.initStore = function () {
		
		// Create project dojo store
		hi_store.create("Project", {
			target: "item",
			idAttribute: "id"
		});
		
		/* #2173 - observers can use this function to determine whether or not their items 
		 * need to be re-sorted
		 */
		hi_store.Project.attributeRequiresSort = function(attribute) {
			var currentSortAttribute = hi_store.Project.getOrderConfiguration(true),
				sortAttribute = null;
				
			for (var i = 0; i < currentSortAttribute.length; i++) {
				sortAttribute = currentSortAttribute[i].attribute;
				if (sortAttribute == attribute) return true;
			}
			return false;
		};
		
		hi_store.Project.getOrder = function (type) {
			var sorting = Project.getSorting(type);

			if(sorting == Project.ORDER_SUBJECT) {
				return "title";
			}else if(sorting == Project.ORDER_START_DATE) {
				return "startDate,startTime";
			}else if(sorting == Project.ORDER_LAST_MODIFIED) {
				return "-timeLastUpdate";
			}else if(sorting == Project.ORDER_PRIORITY) {
				return "priority";
			}else if(sorting == Project.ORDER_DUE_DATE) {
				if (type) return "dueDate";
				return "endDate,endTime";
			} else {
				return "";
			}
		};
		hi_store.Project.getOrderConfiguration = function(asArray) {
			var sorting = Project.getSorting();
			if(sorting == Project.ORDER_SUBJECT) {
				if (asArray) {
					return [{attribute: "title", descending: false, caseInsensitive: false}];
				} else {
					return function(a, b) {
						var aValue = typeof(a.title) == 'string' ? a.title.toUpperCase() : a.title;
						var bValue = typeof(b.title) == 'string' ? b.title.toUpperCase() : b.title;
						var descending = false;
						
						if (aValue != bValue) {
							return !!descending == (aValue == null || aValue > bValue) ? -1 : 1;
						}
						
						return 0;
					};
				}
			} else if (sorting == Project.ORDER_START_DATE) {
				return [{attribute: "start_date", descending: false}, {attribute: "start_time", descending: false},{attribute:'priority',descending:true}];
			} else if (sorting == Project.ORDER_LAST_MODIFIED) {
				return [{attribute: "time_last_update", descending: true}];
			} else if (sorting == Project.ORDER_PRIORITY) {
				return [{attribute: "priority", descending: true}];
			} else if (sorting == Project.ORDER_DUE_DATE) {
				return [{attribute: "end_date", descending: false}, {attribute: "end_time", descending: false},{attribute:'priority',descending:true}];
			} else {
				return null;
			}
		};

		hi_store.Project.getProjectOrderConfiguration = function(asArray) {
			var sorting = Project.getSorting(2);
			if(sorting == Project.ORDER_SUBJECT) {
				if (asArray) {
					return [{attribute: "title", descending: false, caseInsensitive: true}];
				} else {
					return function(a, b) {
						var aValue = typeof(a.title) == 'string' ? a.title.toUpperCase() : a.title;
						var bValue = typeof(b.title) == 'string' ? b.title.toUpperCase() : b.title;
						var descending = false;
						
						if (aValue != bValue) {
							return !!descending == (aValue == null || aValue > bValue) ? -1 : 1;
						}
						
						return 0;
					};
				}
			} else if (sorting == Project.ORDER_START_DATE) {
				return [{attribute: "start_date", descending: false}, {attribute: "start_time", descending: false},{attribute:'priority',descending:true}];
			} else if (sorting == Project.ORDER_LAST_MODIFIED) {
				return [{attribute: "time_last_update", descending: true}];
			} else if (sorting == Project.ORDER_PRIORITY) {
				return [{attribute: "priority", descending: true}];
			} else if (sorting == Project.ORDER_DUE_DATE) {
				return [{attribute: "due_date", descending: false}, {attribute: "end_time", descending: false},{attribute:'priority',descending:true}];
			} else {
				return null;
			}
		};
		
		hi_store.Project.refreshForceIds = []; // #5201
		
		hi_store.Project.refresh = function(clean) {
			// summary:
			//		Sync with server, only items which has changed since last update
			//		are requested
			// clean: Boolean
			//		Reload all data instead of changes
			// Stop timer
			/* Bug #1823 this.refreshStopTimer();*/
			/* Bug #1937 - If clean, then clear cache */
			// Get last update time
			var params = null,
				props = {};
			
			if (!clean ) {
				var latest_time = "",
					items = hi_store.ProjectCache.query(),
					i = 0,
					ii = items.length;
				
				for (; i<ii; i++) {
					if (items[i].time_last_update > latest_time) {
						latest_time = items[i].time_last_update;
					}
				}
				
				params = {"time": latest_time};
				if (hi_store.Project.refreshForceIds.length) {
					params.forceIds = hi_store.Project.refreshForceIds;
					hi_store.Project.refreshForceIds = [];
				}
			} else {
				hi.store.ProjectCache.query().forEach(function(item) {
					if (item.recurring)
					{
						for (var i in hi.items.manager._items[item.id]) {
							hi.items.manager.removeItem(item.id,i);
						}
					} else
					{
						hi.items.manager.removeItem(item.id);
					}
					hi.store.ProjectCache.remove(item.id);
				});
			}
			this.targetParams.orderby = this.getOrder();
			this.targetParams.filter = Hi_TaskFilter.getFilter();
			
			var defer = this.query(props, params);
			defer.then(function(items) {
				if (typeof(items) == 'object') {
					var len = items.length || 0;
					for (var i = 0; i < len; i++) {
						var it = items[i];
						if (it.id && !it.changed && !hi.items.manager.get(it.id)) {
							hi_store.Project.refreshForceIds.push(it.id);
						}
					}
				}
			});
			return defer;//.then(function () {
				// Start timer
				/* Bug #1823 hi_store.Project.refreshStartTimer();*/
			//});
		};
		
		hi_store.Project.refreshStopTimer = function () {
			// summary:
			//		Stop timer which synced dojo store
			/*
			if (hi_store.Project.refreshTimeout) {
				dojo.global.clearTimeout(hi_store.Project.refreshTimeout);
				hi_store.Project.refreshTimeout = null;
			}
			*/
			dojo.global.clearInterval(hi_store.Project.intervalTimer);
			hi_store.Project.refreshTimerOn = 1;
		};
		/* Bug #1823 */
		hi_store.Project.intervalTimer = null;
		hi_store.Project.deferred = null;
		hi_store.Project.requestStartTime = 0;
		hi_store.Project.counter = 1;
		hi_store.Project.bogusInterval = 3;
		hi_store.Project.bogusRequest = function() {
			return dojo.xhr("GET", {
				url: Project.api2_base_url + '/nowhere/',
				handleAs: "json"
			});
		};
		/* Bug #2167 */
		hi_store.Project.refreshTimerOn = null;
		hi_store.Project.refreshStartTimer = function () {
			// summary:
			//		Set timer to sync dojo store after a specific time
			console.log("START TIMER1");
			hi_store.Project.refreshTimerOn = 1;
			var s = parseInt(CONSTANTS.taskRefreshInterval);
			if (!isFinite(s) || s <= 0) s = 60;
			/* Bug #1823 
			hi_store.Project.refreshStopTimer();
			
			hi_store.Project.refreshTimeout = dojo.global.setTimeout(function () { 
				hi_store.Project.refresh();
			}, s * 1000);
			*/
			
			if (hi_store.Project.intervalTimer)
			{
				dojo.global.clearTimeout(hi_store.Project.intervalTimer);
			} 
			hi_store.Project.intervalTimer = dojo.global.setInterval(function() {
				var reqObj;
				
				if (hi_store.Project.counter % hi_store.Project.bogusInterval != 0) 
					reqObj = {request:hi_store.Project.deferred,doRequest:'refresh',requestStartTime:hi_store.Project.requestStartTime,scope:hi_store.Project};
				else
					reqObj = {request:hi_store.Project.deferred,doRequest:'bogusRequest',requestStartTime:hi_store.Project.requestStartTime,scope:hi_store.Project};
				
				dojo.global.fireRequest(reqObj);
				
				hi_store.Project.requestStartTime = reqObj.requestStartTime;
				
				hi_store.Project.deferred = reqObj.request.then(function() {
					hi_store.Project.deferred = null;
					
				},function(err) {
					hi_store.Project.deferred = null;
				});
				
				/* Bug #1823 - To test bad URL Requests hi_store.Project.counter++;*/
			},s*1000);
		};
		
		// Load data
		hi_store.Project.targetParams.orderby = hi_store.Project.getOrder();
		var filterFromUserPrefs = parseInt(Hi_Preferences.get('taskfilter'));
		hi_store.Project.targetParams.filter = filterFromUserPrefs >= 0 ? filterFromUserPrefs : Hi_TaskFilter.getFilter();
		var contactsLoaded = null;
		console.log('boot up');
		
		// #2551
		var checkCompletedItems = function() {
			var completedItems = hi.store.ProjectCache.query(function(item) {
				return item.completed == 1 && item.permission >= Hi_Permissions.ARCHIVE;
			});
			var completedCount = completedItems.length;
			if (completedCount > CONSTANTS.completedItemsWarningThreshold) {
				Project.alertElement.showItem(17, {count: completedCount});
			}
		};
		
		var contactsLoadCallback = function() {
			hi_store.Project.query({}).then(function() {
				// Once finished loading show content
				hi_store.afterLoad();
				var container = dojo.byId(Project.getContainer('tasksList'));
				dojo.removeClass(container, "loading");
				checkCompletedItems();
			});
		};
		
		Team.contactsLoad().then(function() {
			contactsLoaded = true;
			contactsLoadCallback();
		}, function() {
			contactsLoaded = true;
			contactsLoadCallback();
		});
		
		var myContactsLoadTries = setInterval(function() {
			console.log('boot up');
			if (contactsLoaded) {
				clearInterval(myContactsLoadTries);
			} else {
				console.log('try again');
				Team.contactsLoad().then(function() {
					clearInterval(myContactsLoadTries);
					contactsLoadCallback();
				}, function() {
					clearInterval(myContactsLoadTries);
					contactsLoadCallback();
				});
			}
		}, 30 * 1000);

		
		// Start timer
		hi_store.ProjectCache.sort = hi_store.Project.getOrderConfiguration();
		/* #2167 hi_store.Project.refreshStartTimer(); */
		
		Hi_Search.init();
	};
	
	return hi_store;
	
});
