define([
	"dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/_base/event",
	"dojo/_base/array",
	'dojo/sniff',
	'dojo/aspect',
	'dojo/on',
	'dojo/dom-construct',
	'dojo/keys',
	'dojo/string',
	"dojo/dom-geometry",

	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dijit/_WidgetsInTemplateMixin',
	'dijit/form/ComboBox',
	'dijit/form/_TextBoxMixin',
	'dijit/_HasDropDown' ,

	'hi/widget/form/hiSharing/hiSharingDropDown',
	'hi/shortcuts'
], function(declare, lang, event, array, sniff, aspect, on, domConstruct, keys, string, domGeo,
			_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, ComboBox, _TextBoxMixin,
			_HasDropDown){


	if (dojo.isIE) {
		var _TextBoxMinxinPostcreate = _TextBoxMixin.prototype.postCreate;

		dojo.safeMixin(_TextBoxMixin.prototype, {
			postCreate: function() {
				// setting the value here is needed since value="" in the template causes "undefined"
				// and setting in the DOM (instead of the JS object) helps with form reset actions
				this.textbox.setAttribute("value", this.textbox.value); // DOM and JS values should be the same

				this.inherited(arguments);

				// normalize input events to reduce spurious event processing
				//	onkeydown: do not forward modifier keys
				//		       set charOrCode to numeric keycode
				//	onkeypress: do not forward numeric charOrCode keys (already sent through onkeydown)
				//	onpaste & oncut: set charOrCode to 229 (IME)
				//	oninput: if primary event not already processed, set charOrCode to 229 (IME), else do not forward
				function handleEvent(e){
					var charOrCode;
					if(e.type == "keydown"){
						charOrCode = e.keyCode;
						switch(charOrCode){ // ignore state keys
							case keys.SHIFT:
							case keys.ALT:
							case keys.CTRL:
							case keys.META:
							case keys.CAPS_LOCK:
							case keys.NUM_LOCK:
							case keys.SCROLL_LOCK:
								return;
						}
						if(!e.ctrlKey && !e.metaKey && !e.altKey){ // no modifiers
							switch(charOrCode){ // ignore location keys
								case keys.NUMPAD_0:
								case keys.NUMPAD_1:
								case keys.NUMPAD_2:
								case keys.NUMPAD_3:
								case keys.NUMPAD_4:
								case keys.NUMPAD_5:
								case keys.NUMPAD_6:
								case keys.NUMPAD_7:
								case keys.NUMPAD_8:
								case keys.NUMPAD_9:
								case keys.NUMPAD_MULTIPLY:
								case keys.NUMPAD_PLUS:
								case keys.NUMPAD_ENTER:
								case keys.NUMPAD_MINUS:
								case keys.NUMPAD_PERIOD:
								case keys.NUMPAD_DIVIDE:
									return;
							}
							if((charOrCode >= 65 && charOrCode <= 90) || (charOrCode >= 48 && charOrCode <= 57) || charOrCode == keys.SPACE){
								return; // keypress will handle simple non-modified printable keys
							}
							var named = false;
							for(var i in keys){
								if(keys[i] === e.keyCode){
									named = true;
									break;
								}
							}
							if(!named){
								return;
							} // only allow named ones through
						}
					}
					charOrCode = e.charCode >= 32 ? String.fromCharCode(e.charCode) : e.charCode;
					if(!charOrCode){
						charOrCode = (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 48 && e.keyCode <= 57) || e.keyCode == keys.SPACE ? String.fromCharCode(e.keyCode) : e.keyCode;
					}
					if(!charOrCode){
						charOrCode = 229; // IME
					}
					if(e.type == "keypress"){
						if(typeof charOrCode != "string"){
							return;
						}
						if((charOrCode >= 'a' && charOrCode <= 'z') || (charOrCode >= 'A' && charOrCode <= 'Z') || (charOrCode >= '0' && charOrCode <= '9') || (charOrCode === ' ')){
							if(e.ctrlKey || e.metaKey || e.altKey){
								return;
							} // can only be stopped reliably in keydown
						}
					}
					if(e.type == "input"){
						if(this.__skipInputEvent){ // duplicate event
							this.__skipInputEvent = false;
							return;
						}
					}else{
						this.__skipInputEvent = true;
					}
					// create fake event to set charOrCode and to know if preventDefault() was called
					var faux = { faux: true }, attr;
					for(attr in e){
						if(!/^(layer[XY]|returnValue|keyLocation)$/.test(attr)){ // prevent WebKit warnings
							var v = e[attr];
							if(typeof v != "function" && typeof v != "undefined"){
								faux[attr] = v;
							}
						}
					}
					lang.mixin(faux, {
						charOrCode: charOrCode,
						_wasConsumed: false,
						preventDefault: function(){
							faux._wasConsumed = true;
							e.preventDefault();
						},
						stopPropagation: function(){
							e.stopPropagation();
						}
					});
					// give web page author a chance to consume the event
					//console.log(faux.type + ', charOrCode = (' + (typeof charOrCode) + ') ' + charOrCode + ', ctrl ' + !!faux.ctrlKey + ', alt ' + !!faux.altKey + ', meta ' + !!faux.metaKey + ', shift ' + !!faux.shiftKey);
					if(this.onInput(faux) === false){ // return false means stop
						faux.preventDefault();
						faux.stopPropagation();
					}
					if(faux._wasConsumed){
						return;
					} // if preventDefault was called
					this.defer(function(){
						this._onInput(faux);
					}); // widget notification after key has posted
				}
				if (this.declaredClass === 'hi.widget.Tag') {
					this.own(
						on(this.textbox, "keydown, keypress, paste, cut, compositionend", lang.hitch(this, handleEvent)),

						// Allow keypress to bubble to this.domNode, so that TextBox.on("keypress", ...) works,
						// but prevent it from further propagating, so that typing into a TextBox inside a Toolbar doesn't
						// trigger the Toolbar's letter key navigation.
						on(this.domNode, "keypress", function(e){ e.stopPropagation(); })
					);
				} else {
					this.own(
						on(this.textbox, "keydown, keypress, paste, cut, input, compositionend", lang.hitch(this, handleEvent)),

						// Allow keypress to bubble to this.domNode, so that TextBox.on("keypress", ...) works,
						// but prevent it from further propagating, so that typing into a TextBox inside a Toolbar doesn't
						// trigger the Toolbar's letter key navigation.
						on(this.domNode, "keypress", function(e){ e.stopPropagation(); })
					);
				}
			}
		});
	}

	var hiTag = declare("hi.widget.Tag", [ComboBox], {
		//Fire the change event for every text change
		intermediateChanges: true,

		// templateString: template,
		baseClass: 'hiTags dijitTextBox dijitComboBox',

		taskId: 0,

		// labelAttr: 'label',
		
		tags: {},

		_textboxMinWidth: 50,

		_setTagsAttr: function(tags){
			this._set('tags', tags);
		},

		_getTagsAttr: function(){
			return Object.keys(this.tags);
		},

		nls: hi.i18n.getLocalization('task'),

		buildRendering: function(){
			this.inherited(arguments);
			this.itemContainer = dojo.query('.dijitInputContainer', this.domNode)[0];
		},

		postCreate: function(){
			var t = this;

			this.own(
				// on(this.textbox, 'input', lang.hitch(this, function(e) {
				// 	console.log(e.type);
				// 	console.log('my input event');
				// 	e.preventDefault();
				// 	e.stopPropagation();
				// })),
				on(this.textbox, 'keypress', lang.hitch(this, '_onKeyPress')),
				on(this.textbox, 'keydown', lang.hitch(this, '_onKeyDown')),
				aspect.after(this, '_selectOption', function() {
					t._addTag();
				})
			);

			this.inherited(arguments);

			// set placeholder on textbox will trigger input event in IE which is a bug.
			// http://www.carsonshold.com/2013/12/js-bug-ie-10-and-11-oninput-event-with-placeholder-set/
			if (dojo.isIE) {
				var _onInput = this._onInput;
				this._onInput = null;
				this.textbox.setAttribute('placeholder', 'Add a tag');
				this._onInput = _onInput;
			} else {
				this.textbox.setAttribute('placeholder', 'Add a tag');
			}
			if(hi.store.dataLoaded){
				hi.store.dataLoaded.then(function(){
					t._fillStore();
				});
			}
		},

		loadDropDown: function() {
			this._fillStore();
			this.inherited(arguments);
		},

		add: function(tag) {
			var t = this;
			tag = string.trim(tag);
			// var tag = this.tagbox.value;
			if(!tag || this.tags[(tag + '').toLowerCase()]) return;

			this.tags[(tag + '').toLowerCase()] = tag;

			var div = document.createElement('div');
			dojo.addClass(div, 'tag');
			div.id = this.id + '-' + tag;

			var divv = document.createElement('div');
			divv.style.display = 'table';
			divv.style.height = '100%';
			div.appendChild(divv);

			var desc = document.createElement('p');
			dojo.addClass(desc, 'desc');
			desc.innerHTML = tag;

			var closeBtn = document.createElement('span');
			closeBtn.innerHTML = '×';		// use "×" here to replace "close" icon
											// https://red.hitask.net/issues/4723#note-41

			divv.appendChild(desc);
			divv.appendChild(closeBtn);

			div.handlers = [];
			div.handlers.push(on(closeBtn, 'click', lang.hitch(this, '_remove')));

			domConstruct.place(div, this.textbox, 'before');
			this.textbox.value = '';
			this._updateUI();

			if (dojo.isIE) {
				var _onInput = this._onInput;
				this._onInput = null;
				if (Object.keys(this.tags).length) this.textbox.removeAttribute('placeholder');
				this._onInput = _onInput;
			} else {
				if (Object.keys(this.tags).length) this.textbox.removeAttribute('placeholder');
			}
			// Hi_Tag.fillMoreTags();
		},

		remove: function(tag, dontFillMore){
			var id = this.id + '-' + tag, t = this;
			node = dojo.byId(id);

			if(!node) return;

			array.forEach(node.handlers, function(handler){
				handler.remove();
			});
			domConstruct.destroy(node);
			this._updateUI();
			delete this.tags[tag];

			// if (!Object.keys(this.tags).length) this.textbox.setAttribute('placeholder', 'Add a tag');
			if (dojo.isIE) {
				var _onInput = this._onInput;
				this._onInput = null;
				if (!Object.keys(this.tags).length) this.textbox.setAttribute('placeholder', 'Add a tag');
				this._onInput = _onInput;
			} else {
				if (!Object.keys(this.tags).length) this.textbox.setAttribute('placeholder', 'Add a tag');
			}
			// if(!dontFillMore){
			// 	Hi_Tag.fillMoreTags();
			// }
		},

		removeAll: function(){
			for(var k in this.tags){
				this.remove(this.tags[k], true);
			}
			// Hi_Tag.fillMoreTags();
			this.tags = {};
			this.textbox.value = '';
			// this._updateUI();
		},

		_isEmpty: function() {
			return Object.keys(this.tags).length === 0;
		},

		_fillStore: function(){
			var store = this.get('store'),
				tags = Hi_Tag.listArray(),
				data = [];

			array.forEach(tags, function(tag){
				data.push({id: tag, name: tag, label: tag});
			});

			store.setData(data);
		},

		_addTag: function() {
			this.add(this.textbox.value);
		},

		_remove: function(e) {
			var target = e.target;
			if(!target) return;

			var tag = dojo.query(target).closest('.tag')[0];
			if(!tag) return;

			var index = this.id.length + 1;
			var id = tag.id.substr(index);
			this.remove(id);
		},

		_updateUI: function() {
			this._updateInputWidth();
			var padBox = domGeo.getPadBorderExtents(this._buttonNode);
			this._buttonNode.style.height = this.itemContainer.clientHeight - padBox.t - padBox.b + 'px';
		},

		_updateInputWidth: function() {
			var input = this.textbox,
				width,
				tags,
				lastTag;

			if (this.itemContainer.childNodes.length > 1) {
				tags = dojo.query('.tag', this.itemContainer);
				if (tags.length) {
					lastTag = tags.pop();
					width = this.itemContainer.clientWidth - lastTag.offsetLeft - lastTag.offsetWidth - 6;
					
					if (width >= this._textboxMinWidth) {	//Text input is in the same line as the last tag.
						input.style.setProperty('width', width + 'px', 'important');
					} else {	//When not in the same line as the last tag, remove self-defined width CSS.
						input.style.setProperty('width', '');
					}
				}
			} else {
				input.style.setProperty('width', '');
			}
		},
		// value: [],
		// _getValueAttr: function(){
		// 	return this.value;
		// },

		// _onKey: function(/*Event*/ evt){
		// 	// this.inherited(arguments);
		// 	console.log('key event');
		// 	evt.stopPropagation();
		// },

		_onKeyPress: function(evt) {
			var key = evt.charCode || evt.keyCode, lastTag;

			if (key === keys.ENTER || key === 44/*comma*/) {
				evt.preventDefault();
				this._addTag();
			}
			evt.stopPropagation();
		},

		_onKeyDown: function(evt) {
			var key = evt.charCode || evt.keyCode, lastTag,
				t = this;

			if (key === keys.BACKSPACE) {
				if (!this._isEmpty() && this.textbox.value === '') {
					lastTag = Object.keys(this.tags).pop();
					this.remove(lastTag);
					setTimeout(function() {
						t.closeDropDown();
					}, 1);
				}
			}
			evt.stopPropagation();
		},

		_blurTimeout: null,

		_onBlur: function(evt) {
			this.inherited(arguments);
			this._addTag();
		}
	});

	return hiTag;
});