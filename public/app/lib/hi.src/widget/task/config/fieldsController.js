define(['hi/widget/task/item'], function(taskItem) {
	// TaskItem.CATEGORY_PROJECT = 0;
	// TaskItem.CATEGORY_TASK = 1;
	// TaskItem.CATEGORY_EVENT = 2;
	// TaskItem.CATEGORY_NOTE = 4;
	// TaskItem.CATEGORY_FILE = 5;
	var config = {};

	config.showFields = function (widget, category, type) {
		var fc = this[category];
		var k, vis, node;
		type = type || 'item';

		for (k in fc) {
			visible = fc[k];
			node = widget[this.fields[type][k]];

			// if (node.hasAttribute('widgetid')) {
			if (node.domNode) {		// is widget
				node = node.domNode;
			}

			if (node) {
				if (visible) {
					dojo.removeClass(node, 'hidden');
				} else {
					dojo.addClass(node, 'hidden');
				}
			}
		}
	};

	config.fields = {
		'item': {
			startTime: 'startTimeWidget',
			startDate: 'startDateWidget',
			startTimeContainer: 'startTimeContainer',
			endTime: 'endTimeWidget',
			endDate: 'endDateWidget',
			endTimeContainer: 'endTimeContainer',
			dueDate: 'dueDateContainer',
			allday: 'isAllDayWidget' 
		},

		'project': {

		}
	};

	config[taskItem.CATEGORY_TASK] = {	// 1
		startTime: true,
		startDate: true,
		startTimeContainer: true,

		endTime: true,
		endDate: true,
		endTimeContainer: true,

		dueDate: false,
		allday: true
	};

	config[taskItem.CATEGORY_EVENT] = {	// 2
		startTime: true,
		startDate: true,
		startTimeContainer: true,

		endTime: true,
		endDate: true,
		endTimeContainer: true,

		dueDate: false,
		allday: true
	};

	config[taskItem.CATEGORY_NOTE] =  {	// 4
		startTime: false,
		startDate: false,
		startTimeContainer: false,

		endTime: false,
		endDate: false,
		endTimeContainer: false,

		dueDate: false,
		allday: false
	};

	config[taskItem.CATEGORY_FILE] =  {	// 5
		startTime: true,
		startDate: true,
		startTimeContainer: true,

		endTime: true,
		endDate: true,
		endTimeContainer: true,

		dueDate: false,
		allday: false
	};

	config[taskItem.CATEGORY_PROJECT] = {		// 0
		startTime: false,
		startDate: true,
		endTime: false,
		endDate: false,
		dueDate: true,
	};

	return config;
});