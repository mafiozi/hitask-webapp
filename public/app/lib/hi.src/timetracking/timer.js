//dojo.provide("hi.timetracking.timer");
define(['hi/widget/task/item_DOM',
	'dojo/_base/lang',
	'dojo/_base/array',
	'dojo/cookie',
	'dojo/_base/unload'],
	function (item_DOM,
		lang,
		array,
		cookie,
		baseUnload) {
	
	dojo.global.Hi_Timer = {
		
		/**
		 * Timed task ID
		 * @type {Number}
		 */
		timed_task: null,
		
		/**
		 * event date, when this timed task is recurring
		 */
		eventdate: null,
		
		/**
		 * recurring attribute on item
		 */
		recurring: null,
		
		/**
		 * Timer start time, timestamp
		 * @type {Number}
		 */
		start_time: null,
		
		/**
		 * Total spent time 
		 * @type {Number}
		 */
		total_time: 0,
		
		/**
		 * Interval for sending data to server
		 * @type {Number}
		 */
		interval: null,

		startup: function() {
			var cookieValue = cookie("hiTaskTimer");
			if (cookieValue !== "") {
				try {
					cookieValue = JSON.parse(cookieValue);
					this.resumeAfterReload(cookieValue);
				}
				catch (err) {
					console.log("Could not parse hiTaskTimer cookie");
				}
			}
		},
		
		/**
		 * Returns task ID by node
		 * 
		 * @param {Object} node HTMLElement or ID
		 * @return Task ID
		 * @type {Number}
		 */
		_node2id: function (node) {
			if (node.tagName) {
				node = dojo.dom.getAncestorsByTag(node, 'LI', true);
				return dijit.byNode(dojo.byId('hi_widget_TaskItem_0')).taskId;
			}
			return node;
		},
		
		/**
		 * Returns if timer is running
		 * 
		 * @return True if timer is running, otherwise false
		 * @type {Boolean}
		 */
		isRunning: function () {
			return this.timed_task ? true : false;
		},
		
		/**
		 * Returns if timer is paused
		 * 
		 * @return True if timer is paused, otherwise false
		 * @type {Boolean}
		 */
		isPaused: function () {
			return this.start_time ? false : (this.timed_task ? true : false);
		},
		
		/**
		 * Returns true if 
		 */
		isTimedTask: function (id) {
			id = this._node2id(id);
			return this.timed_task == id ? true : false;
		},
		
		/**
		 * Returns formated spent time
		 * 
		 * @return Formatted time as "HH:mm"
		 */
		getTime: function (special) {
			var spent_abs = this.start_time ? (new Date()).getTime() - this.start_time : 0;
			var spent = Math.floor((this.total_time + spent_abs) / 60000);	//in minutes
			var hours = strPadLeft(~~(spent / 60), 2, '0');
			var minutes = strPadLeft(spent % 60, 2, '0');
			
			return hours + (special ? '<i>:</i>' : ':') + minutes; 
		},
		
		/**
		 * Send data to server
		 */
		send: function (callback, fromInterval) {
			if (typeof callback != 'function') callback  = function () {};
			if (!this.timed_task || !this.start_time) return callback();
			
			var item_id = this.timed_task;
			var spent_abs = (new Date()).getTime() - this.start_time;
			var spent = Math.floor(spent_abs / 60000);	//in minutes
			if (spent <= 0) {
				if (!fromInterval) return callback();
				spent = 1;
			}

			var date = time2str(HiTaskCalendar.currentDate, "y-m-d");
			
			var postData = {
				'id': this.timed_task,
				'date': date,
				'description': '',
				'time_spent': spent,
				'user_id': Project.user_id
			};

			Project.ajax('timelog', postData, function (dataResponse) {
				callback();
				
				if (!dataResponse.isStatusOk()) {
					Hi_Timer.stop();
				}
			});
			
			this.total_time += spent * 60000;

			this.start_time = (new Date()).getTime();
			
			//Update UI
			this.view.update();
			
			//Update data
			var time_spent = hi.items.manager.get(this.timed_task, "time_spent");
			time_spent = (parseInt(time_spent, 10) || 0) + spent;

			console.log("time_spent: " + time_spent);

			hi.items.manager.set({
				"id": this.timed_task,
				"time_spent": time_spent
			});
		},
		
		/**
		 * Start timer
		 * 
		 * @param {Number} timed_task Task ID
		 */
		start: function (timed_task, task) {
			timed_task = this._node2id(timed_task) || this.timed_task;
			
			if (this.timed_task) {
				if (this.timed_task == timed_task) {
					if (this.isPaused()) this.resume();
					return;
				}
				this.stop();
			}
			
			this.timed_task = timed_task;
			this.eventdate = task.eventdate || null;
			this.recurring = task.recurring || 0;
			this.total_time = 0;
			
			this.resume();
			
			//Update UI
			this.view.show();
			
			//UI is shown, close task
			var item = hi.items.manager.opened(Hi_Timer.timed_task);
			if (item) {
				//After event has propagated
				setTimeout(function () {
					//if (!item.editing) item.collapse();
				}, 60);
			}
		},
		
		/**
		 * Timer tick function
		 */
		tick: function () {
			Hi_Timer.send(null, true);
			
			clearTimeout(Hi_Timer.interval);
			Hi_Timer.interval = setTimeout(Hi_Timer.tick, 60000);
		},

		/**
		 * Resume timer after reload
		 */
		resumeAfterReload: function(data) {
			if (data.timerStatus == "active") {
				this.timed_task = data.item_id;
				var currentTime = (new Date()).getTime();
				var timeLapse = currentTime - data.lastUpdatedTimeStamp;
				timeLapse = Math.floor(timeLapse / 60000) * 60000;
				this.total_time = data.minutesTrackedAtLastUpdatedTimeStamp;
				setTimeout(lang.hitch(this, function(){
					this.resume();
					this.start_time = data.start_time;
					this.send();
				}), 5000);
			}
		},
		
		/**
		 * Resume timer
		 */
		resume: function () {
			if (!this.timed_task || !this.isPaused()) return;

			baseUnload.addOnUnload(lang.hitch(this, function(){
				var value = "";
				if(cookie("hiTaskTimer") != "loggedOut") {
					if (this.isRunning()) {
						value = {
							timerStatus: "active",
							item_id: this.timed_task,
							lastUpdatedTimeStamp: (new Date()).getTime(),
							minutesTrackedAtLastUpdatedTimeStamp: this.total_time,
							start_time: this.start_time
						};
						value = JSON.stringify(value);
					}
				}
				cookie("hiTaskTimer", value, { expires: 5 });
			}));
			
			this.start_time = (new Date()).getTime();
			
			//Set interval
			var interval = 60000; // 1 minute
			
			clearTimeout(this.interval);
			
			//Wait 2 more seconds to make sure we don't get same minute twice
			this.interval = setTimeout(this.tick, interval + 2000);
			
			//Update UI
			this.view.update();
			this.view.startBlink();
		},
		
		/**
		 * Stop timer
		 */
		stop: function () {
			if (!this.timed_task) return;
			
			this.pause();
			this.timed_task = null;
			this.eventdate = null;
			this.recurring = null;
			this.total_time = 0;
			
			//Update UI
			this.view.hide();
			this.view.stopBlink();
			
			// #2996: need to update tracking list if item opened.
			var item = hi.items.manager.opened();
			var itemTrackingList = dojo.byId('propertiesViewNode_' + (item ? item.widgetId : null));
			if (item && Hi_TimeTracking.list.drawn) {
				// Reload data and redraw if tracking list already drawn.
				Hi_TimeTracking.list.reloadData(function() {
					Hi_TimeTracking.list.redrawList();
				});
			} else if (item && itemTrackingList) {
				var li = dojo.query('.time-tracking-container', itemTrackingList);
				// Pre init tracking list and draw first time if it is a fist log record.
				if (li && li.length == 1) {
					Hi_TimeTracking.list.preInit(false, function() {
						Hi_TimeTracking.list.show(li[0], true);
					});
				}
			}
		},
		
		/**
		 * Pause timer
		 */
		pause: function () {
			if (!this.timed_task) return;
			
			this.send();
			this.start_time = null;
			
			clearTimeout(this.interval);
			this.interval = null;
			
			//Update UI
			this.view.update();
			this.view.stopBlink();
		},
		
		/**
		 * Stop timer + complete task
		 */
		complete: function () {
			if (!this.timed_task) return;
			var task_id = this.timed_task;
			/*
			 * #3367 - timer needs to know how to handle completing a recurring instance
			 */
			var eventdate = this.eventdate||null;
			
			var saveObj = {
				id:task_id,
				completed:true,
			};
			
			if (this.recurring)
			{
				saveObj['recurring'] = this.recurring;
				saveObj['eventDateAttr'] = this.eventdate?time2strAPI(str2timeAPI(this.eventdate)):null;
			}
			
			//Stop timer
			this.stop();
			
			//Complete task
			if (!hi.items.manager.get(task_id, "completed")) {
				hi.items.manager.setSave(task_id,saveObj).then(function(id) {
					/*
					 * #3367 - close task, otherwise it persists in hi.items.manager._opened
					 */
					item_DOM.closeEdit(task_id, eventdate);
					
					// #3470: need to redraw calendar if item has start date.
					if (hi.items.manager.get(task_id, 'start_date')) {
						HiTaskCalendar.draw();
					}
				});
			}
		},
		
		/*
		 * UI
		 */
		view: {
			/**
			 * Blink interval
			 */
			interval: null,
			blink_state: true,
			
			/**
			 * Start blinking
			 */
			startBlink: function () {
				if (this.interval) return;
				var self = this;
				
				this.interval = setInterval(function () {
					var node1 = dojo.byId('timer_tooltip_time').getElementsByTagName('I'); node1 = node1.length ? node1[0] : null;
					var node2 = self.getButton().getElementsByTagName('I'); node2 = node2.length ? node2[0] : null;
					
					self.blink_state = !self.blink_state;
					
					if (self.blink_state) {
						if (node1) node1.style.visibility = 'visible';
						if (node2) node2.style.visibility = 'visible';
					} else {
						if (node1) node1.style.visibility = 'hidden';
						if (node2) node2.style.visibility = 'hidden';
					}
				}, 1000);
			},
			
			/**
			 * Stop blinking
			 */
			stopBlink: function () {
				clearInterval(this.interval);
				this.interval = null;
				
				var node1 = dojo.byId('timer_tooltip_time').getElementsByTagName('I'); node1 = node1.length ? node1[0] : null;
				var node2 = this.getButton().getElementsByTagName('I'); node2 = node2.length ? node2[0] : null;
					
				if (node1) node1.style.visibility = 'visible';
				if (node2) node2.style.visibility = 'visible';
			},
			
			/**
			 * Show/hide button and set classes
			 */
			updateButton: function () {
				var running = Hi_Timer.isRunning(), paused = Hi_Timer.isPaused();
				var btn = this.getButton();
				
				if (running || paused) {
					dojo.removeClass(btn, 'hidden');
					
					if (!paused) {
						dojo.addClass(btn, 'timer-running');
					} else {
						dojo.removeClass(btn, 'timer-running');
					}
					
					var time = Hi_Timer.getTime(true);
					var element = btn.getElementsByTagName('SPAN')[0];
						element.innerHTML = time;
				} else {
					dojo.addClass(btn, 'hidden');
				}
			},
			
			/**
			 * Returns header button element
			 * 
			 * @return Button
			 * @type {HTMLElement}
			 */
			getButton: function () {
				return dojo.byId('timerLink');
			},
			
			/**
			 * Update popup content
			 */
			update: function () {
				var task_id = Hi_Timer.timed_task;
				if (!task_id) return;
				
				//Set title
				var node_title = dojo.query('#timer_tooltip p')[0];
				var title = hi.items.manager.get(task_id, 'title');
				if (title) {
					node_title.innerHTML = htmlspecialchars(title);
				}
				
				//Update time
				var node_time = dojo.byId('timer_tooltip_time');
				var time = Hi_Timer.getTime(true);
				node_time.innerHTML = time;
				
				if (Hi_Timer.isPaused()) {
					dojo.addClass(node_time, 'paused');
				} else {
					dojo.removeClass(node_time, 'paused');
				}
				
				//Buttons
				var button_pause = dojo.byId('timer_tooltip_pause');
				var button_start = dojo.byId('timer_tooltip_start');
				
				if (Hi_Timer.isPaused()) {
					dojo.addClass(button_pause, 'hidden');
					dojo.removeClass(button_start, 'hidden');
				} else {
					dojo.addClass(button_start, 'hidden');
					dojo.removeClass(button_pause, 'hidden');
				}
				
				//Task view
				this.updateTaskView();
				
				//Update button
				this.updateButton();
			},
			
			updateTaskView: function () {
				var item = hi.items.manager.opened(Hi_Timer.timed_task) || hi.items.manager.opened();
				
				//View
				if (item && item.propertiesView) {
					var link_start = item.propertiesView.timeTrackingStartLink ? item.propertiesView.timeTrackingStartLink.domNode : null;
					var link_show = item.propertiesView.timeTrackingShowLink ? item.propertiesView.timeTrackingShowLink.domNode : null;
					
					if (Hi_Timer.timed_task && Hi_Timer.timed_task == item.taskId) {
						if (link_start) {
							dojo.addClass(link_start, 'hidden');
						}
						if (link_show) {
							dojo.removeClass(link_show, 'hidden');
							dojo.query('.timeValue', link_show)[0].innerHTML = Hi_Timer.getTime();
						}
					} else {
						item = hi.items.manager.item(Hi_Timer.timed_task) || hi.items.manager.opened();
						
						if (item && item.propertiesView) {
							link_start = item.propertiesView.timeTrackingStartLink ? item.propertiesView.timeTrackingStartLink.domNode : null;
							link_show = item.propertiesView.timeTrackingShowLink ? item.propertiesView.timeTrackingShowLink.domNode : null;
							
							if (link_show) {
								dojo.addClass(link_show, 'hidden');
							}
							if (link_start) {
								dojo.removeClass(link_start, 'hidden');
							}
						}
					}
				}
			},
			
			/**
			 * Show button and popup
			 */
			show: function () {
				this.update();
				this.updateTaskView();
				
				if (dojo.isIE && dojo.isIE <= 7) {
					setTimeout(function () {
						Hi_Timer.view.getButton().onclick();
					}, 1000);
				} else {
					Hi_Timer.view.getButton().onclick();
				}
				
				// #4391 Put behind timetracking tooltip
				var tooltip_dialog_node = dojo.byId('tooltip_timer').parentNode.parentNode.parentNode.parentNode;
				dojo.setStyle(tooltip_dialog_node, "z-index", 10000);
			},
			
			/**
			 * Hide button and popup
			 */
			hide: function () {
				//Fade out and drop down
				Tooltip.close('timer');
				// #2324
				//dojo.anim(dojo.byId('tooltip_timer'), {top: 350}, 500);
				
				this.updateButton();
				this.updateTaskView();
			}
		}
	};
	
	return dojo.global.Hi_Timer;

});