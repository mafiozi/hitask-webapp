define(['dojo/date/locale', 'dojo/_base/lang', 'dojo/dom-style'], function(locale, lang, domStyle) {
	var __curY = 0;
	var __curColumnViewScrollTop = 0;
	var beforePrint = function() {
		if (!bigCalendar) return;
		// var __resizeHandler = bigCalendar.widget.matrixView._resizeHandler;
		// bigCalendar.widget.matrixView._resizeHandler = null;
		// dijit.byId('appContainer').resize();
		console.log('beforeprint');
		bigCalendar.inPrint = true;
		bigCalendar.resize();
		var w = bigCalendar.widget, cv;

		if (!w) return;

		if (w.get('dateInterval') !== 'month') {
			if (w.columnView.sheetContainer) {
				cv = w.columnView;

				if (!cv) return false;

				var scrollContainerHeight = cv.scrollContainer.clientHeight;
				var sheetContainerHeight = cv.sheetContainer.clientHeight;
				var maxY = scrollContainerHeight - sheetContainerHeight

				// var curY = domStyle.get(cv.sheetContainer, this._cssPrefix + "transform");
				var curY = cv.sheetContainer.style[cv._cssPrefix + 'transform'];
				curY = curY && (curY.match(/^translateY\((-?\d*)px\)$/));
				
				if (curY && curY.length) {
					curY = parseInt(curY[1], 10);
				} 
				if (curY <= maxY) {
					domStyle.set(cv.sheetContainer, cv._cssPrefix + "transform", "translateY(" + maxY + "px)");
					__curY = curY;
				}
			}
		}

		var cp = dojo.byId('centerPaneContent');
		if (cp) cp.style.overflow = 'hidden';
		var mv = bigCalendar.widget.matrixView;
		if (mv) {
			var resizeMatrixView = lang.hitch(mv, function() {
				delete this._resizeTimer;
				this._resizeRowsImpl(this.itemContainer, "tr");
				this._layoutRenderers(this.renderData);
				this._layoutDecorationRenderers(this.renderData);
				if (this.resizeAnimationDuration == 0) {
					domStyle.set(this.itemContainer, "opacity", 1);
				} else {
					fx.fadeIn({node:this.itemContainer, curve:[0, 1]}).play(this.resizeAnimationDuration);
				}
			});
			resizeMatrixView();
		}
		// bigCalendar.widget.matrixView._resizeHandler = __resizeHandler;
		// return;
		var title = dojo.byId('bigCalendarPrintTitle'),
			calendarEvents = dojo.query('.dojoxCalendarEvent'), parent, left,
			bw = bigCalendar.widget;

		if (title) { 
			dojo.removeClass(title, 'hidden');
			switch (bw.dateInterval) {
				case "week":
				case "month":
					title.innerHTML = locale.format(bw.date, {selector: 'date', datePattern: 'MMM, yyyy'});
					break;
				case "day" :
					title.innerHTML = locale.format(bw.date, {selector: 'date', datePattern: 'MMM d, yyyy'});
					break;
			}
		}
		if (bigCalendar) bigCalendar._canUpdate = false;

		if (calendarEvents.length) {
			calendarEvents.forEach(function(node) {
				if (!node.clientHeight || !node.clientWidth) return;

				if (node._printLeft) node.style.left = node._printLeft + '%';
				if (node._printWidth) node.style.width = node._printWidth + '%';
			});
		}
	};

	var afterPrint = function() {
		if (!bigCalendar || !bigCalendar.widget) return;

		bigCalendar.inPrint = false;
		console.log('afterprint');
		
		var title = dojo.byId('bigCalendarPrintTitle'),
			cv = bigCalendar.widget.columnView;

		if (!cv) return;
		
		if (title) dojo.addClass(title, 'hidden');

		var cp = dojo.byId('centerPaneContent');
		if (cp) cp.style.overflow = 'auto';

		if (bigCalendar) {
			bigCalendar._canUpdate = true;
			bigCalendar.resize();
			bigCalendar.widget.columnView.scrollBar.domNode.scrollTop = bigCalendar.widget.columnView.scrollBar._curScrollTop;
		}

		return;
		if (__curY) {
			// bigCalendar.widget.columnView
			domStyle.set(cv.sheetContainer, cv._cssPrefix + "transform", "translateY(" + __curY + "px)");
			__curY = 0;
		}
	};

	dojo.global.beforePrint = beforePrint;

	if (window.matchMedia) {
		var mediaQueryList = window.matchMedia('print');
			mediaQueryList.addListener(function(mql) {
			if (mql.matches) {
				beforePrint();
			} else {
				afterPrint();
			}
		});
	}

	window.onbeforeprint = beforePrint;
	window.onafterprint = afterPrint;
});