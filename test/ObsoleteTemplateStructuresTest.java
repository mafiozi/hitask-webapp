import java.io.File;
import java.util.Iterator;

import org.junit.Test;



public class ObsoleteTemplateStructuresTest extends GroovyViewsBaseTest {
	private static final String[] OLD_STRUCTURES = {
		"{ldelim}", "{rdelim}", "{literal}", "{/literal}", "{/foreach}"
	};
	
	/**
	 * Test searches for old Smarty structures in templates.
	 */
	@Test
	public void searchForOldStructures() {
		Iterator<File> fileIterator = getAllViews();
		
		while (fileIterator.hasNext()) {
			File view = fileIterator.next();
			String content = getFileAsString(view, true);
			
			for (String smartyStr : OLD_STRUCTURES) {
				if (content.contains(smartyStr)) {
					fail(String.format("Found %s in %s", smartyStr, view.getAbsolutePath()));
				}
			}
		}
	}
}
