define([
	"dojo/_base/declare",
	"dojo/store/Memory",
	"dojo/store/util/QueryResults",
	"dojo/store/util/SimpleQueryEngine"
], function(declare, Memory, QueryResults, SimpleQueryEngine) {
  //  module:
  //    dojo/store/Memory
  //  summary:
  //    The module defines an in-memory object store.


return declare("hi.store.Memory", Memory, {
	// summary:
	//		This is a basic in-memory object store with predefined sorting option. It implements dojo.store.api.Store.
	
	constructor: function(/*dojo.store.Memory*/ options){
		// summary:
		//		Creates a memory object store.
		// options:
		//		This provides any configuration information that will be mixed into the store.
		// 		This should generally include the data property to provide the starting set of data.
		
	},
	
	// sort: Array
	//		Sort options
	sort: null,
	
	query: function(query, options){
		// 	summary:
		//		Queries the store for objects.
		// 	query: Object
		//		The query to use for retrieving objects from the store.
		//	options: dojo.store.api.Store.QueryOptions?
		//		The optional arguments to apply to the resultset.
		//	returns: dojo.store.api.Store.QueryResults
		//		The results of the query, extended with iterative methods.
		//
		// 	example:
		// 		Given the following store:
		//
		// 	|	var store = new dojo.store.Memory({
		// 	|		data: [
		// 	|			{id: 1, name: "one", prime: false },
		//	|			{id: 2, name: "two", even: true, prime: true},
		//	|			{id: 3, name: "three", prime: true},
		//	|			{id: 4, name: "four", even: true, prime: false},
		//	|			{id: 5, name: "five", prime: true}
		//	|		]
		//	|	});
		//
		//	...find all items where "prime" is true:
		//
		//	|	var results = store.query({ prime: true });
		//
		//	...or find all items where "even" is true:
		//
		//	|	var results = store.query({ even: true });
		
		if (this.sort) {
			options = dojo.mixin({'sort': this.sort}, options);
		}
		
		return QueryResults(this.queryEngine(query, options)(this.data));
	}
});

});
