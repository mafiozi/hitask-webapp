define([
	
	"hi/global",
	"hi/i18n",
	"hi/api",
	"hi/preferences",
	"hi/calendar",
	"hi/taskcalendar",
	"hi/googlecal"
	
], function () {
	
	return {};
	
});
