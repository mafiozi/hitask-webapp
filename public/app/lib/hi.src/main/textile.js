//dojo.provide("hi.main.textile");
	
/**
 * Format message,
 * Converts textile into HTML
 */
define(function () {
	dojo.global.formatMessage = function(message, unescape) {
		if (!message) return '';
		var r = message, i;
		
		//if (unescape) r = unhtmlspecialchars(r);
		if (!unescape) r = htmlspecialchars(r);
		
		//Unescape quoteblock
		r = r.replace(/(^|\n)&gt;/g, '$1>');
		
		// quick tags first
		qtags = [['\\*', 'strong'],
				 ['\\?\\?', 'cite'],
				 ['\\+', 'ins'],  //fixed
				 ['~', 'sub'],   
				 ['\\^', 'sup'], // me
				 ['@', 'code']];
		
		for (i = 0; i < qtags.length; i++) {
			ttag = qtags[i][0]; htag = qtags[i][1];
			re = new RegExp(ttag+'(.+?)'+ttag,'g');
			r = r.replace(re,'<'+htag+'>'+'$1'+'</'+htag+'>');
		}
		
		// preformatted code
		var lines = r.split('\n'), out = [], prev = null, line;
		
		for(i = 0,ii = lines.length; i < ii; i++) {
			line = lines[i];
			var cur = null;
			
			if (line.indexOf('    ') == 0) cur = 'pre';
			if (line.indexOf('> ') == 0) cur = 'blockquote';
			
			if (prev && (!cur || cur != prev)) {
				out[out.length-1] += '</' + prev + '>';
			}
			
			if (cur && (!prev || prev != cur)) {
				out[out.length] = '<' + cur + '>' + line.substr((cur == 'pre' ? 4 : 2));
			} else if (cur && cur == prev) {
				if (cur == 'pre') {
					out[out.length-1] += "__PRE_BREAK__" + line.substr(4);
				} else if (cur == 'blockquote') {
					out[out.length] = line.substr(2);
				}
			} else {
				out[out.length] = line;
			}
			
			prev = cur;
		}
		
		r = out.join('\n');
		
		// h1-h6
		r = r.replace(/(^|\n)h([1-6])\.(.*)?/g, '$1<h$2>$3</h$2>');
		
		// fix </block><br />
		r = r.replace(/(<\/(h1|h2|h3|h4|h5|h6|pre|blockquote)>)\n/g, '$1');
		
		// underscores count as part of a word, so do them separately (make sure not to replace __PRE_BREAK__)
		re = new RegExp('\\b_([^_].*?)_\\b','g');
		r = r.replace(re,'<em>$1</em>');
		
		//jeff: so do dashes
		re = new RegExp('[\s\n]-(.+?)-[\s\n]','g');
		r = r.replace(re,'<del>$1</del>');
	
		// links
		var urlRegExp = /((ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?)/ig;
		r = r.replace(urlRegExp, '<a target="_blank" data-hi-stop="event" onclick="javascript:event.cancelBubble=true" title="Go to $1" href="$1">$1</a>');
		// r = r.replace(/(^|[\s\(])([a-z]+\:\/\/([\.,;\-!\?]?[^\s\.,;\-!\?\)\(])+)(([\.,;\-!\?]*[\s\(\)])|([\.,;\-!\?\)\(]*$))/ig, '$1<a target="_blank" data-hi-stop="event" onclick="javascript:event.cancelBubble=true" title="Go to $2" href="$2">$2</a>$4');
		r = r.replace(/(^|[\s\(])(www\.([\.,;\-!\?]?[^\s\.,;\-!\?\)\(])+)(([\.,;\-!\?]*[\s\(\)])|([\.,;\-!\?\)\(]*$))/ig, '$1<a target="_blank" data-hi-stop="event" onclick="javascript:event.cancelBubble=true" title="Go to $2" href="http://$2">$2</a>$4');
		r = r.replace(/(\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)/ig, '<a target="_blank" onclick="javascript:event.cancelBubble=true" data-hi-stop="event" title="Send email message to $1" href="mailto:$1">$1</a>');
		
		// long link titles
		r = r.replace(/(<a[^>]*>)(.*?)(<\/a>)/g, function (all, pre, title, post) {
			//Remove all tags
			title = title.replace(/<[^>]+>/g, '');
			if (title.length < 50) return pre + title + post;
			return pre + title + post;
		});
		
		// replace hitask with spans
		r = r.replace(/(^|^[^<>]*|>[^<]*)(hi)(task)/ig, '$1<span class="hi">$2</span><span class="task">$3</span>');
		
		re = new RegExp('"\\b(.+?)\\(\\b(.+?)\\b\\)":([^\\s]+)','g');
		r = r.replace(re,'<a target="_blank" data-hi-stop="event" onclick="javascript:event.cancelBubble=true" href="$3" title="$2">$1</a>');
		re = new RegExp('"\\b(.+?)\\b":([^\\s]+)','g');
		r = r.replace(re,'<a target="_blank" data-hi-stop="event" onclick="javascript:event.cancelBubble=true" href="$2">$1</a>');
	
		// images
		re = new RegExp('!\\b(.+?)\\(\\b(.+?)\\b\\)!','g');
		r = r.replace(re,'<img src="$1" alt="$2">');
		re = new RegExp('!\\b(.+?)\\b!','g');
		r = r.replace(re,'<img src="$1">');
		
		// lists
		lines = r.split('\n');
		out = '';
		nr = '';
		for (i = 0; i < lines.length; i++) {
			line = lines[i].replace(/\s*$/,'');
	
			if (line.search(/^\s*\*\s+/) != -1) { line = line.replace(/^\s*\*\s+/,'\t<liu>') + '</liu>'; } // * for bullet list; make up an liu tag to be fixed later
			if (line.search(/^\s*#\s+/) != -1) { line = line.replace(/^\s*#\s+/,'\t<lio>') + '</lio>'; } // # for numeric list; make up an lio tag to be fixed later
			lines[i] = line;
		}
		
		// Second pass to do lists
		inlist = 0; 
		listtype = '';
		for (i = 0;i < lines.length; i++) {
			line = lines[i];
			if (inlist && listtype == 'ul' && !line.match(/^\t<liu/)) { line = '</ul>' + line; inlist = 0; }
			if (inlist && listtype == 'ol' && !line.match(/^\t<lio/)) { line = '</ol>' + line; inlist = 0; }
			if (!inlist && line.match(/\t<liu/)) { line = line.replace(/\t<liu/, '<ul><liu'); inlist = 1; listtype = 'ul'; }
			if (!inlist && line.match(/\t<lio/)) { line = line.replace(/\t<lio/, '<ol><lio'); inlist = 1; listtype = 'ol'; }
			lines[i] = line;
		}
		
		if (inlist) {
			if (listtype == 'ul') lines.push('</ul>');
			if (listtype == 'ol') lines.push('</ol>');
		}
	
		r = lines.join('\n');
		r = r.replace(/li[o|u]>/g,'li>');
		r = r.replace(/(<\/(li|ul|ol)>)\n/g, '$1');
		
		//Line breaks
		re = new RegExp('\n','g');
		r = r.replace(re,"<br />");
		
		//Pre new lines shouldn't be converted to <br />
		r = r.replace(/__PRE_BREAK__/g, "\n");
		
		return r;
	};
	
	return {
		'formatMessage': formatMessage
	};
	
});