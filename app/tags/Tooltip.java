package tags;

import groovy.lang.Closure;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;
import play.templates.JavaExtensions;

@FastTags.Namespace("app.hi")
public class Tooltip extends BaseTag {
	private static final String TRIANGLE_POSITION_RIGHT = "right";
	private static final String TRIANGLE_POSITION_BOTTOM = "bottom";
	private static final String TRIANGLE_POSITION_LEFT = "left";
	private static final String TRIANGLE_POSITION_TOP = "top";
	private static final Map<String, Integer> TRIANGLE_PX = new HashMap<String, Integer>();
	
	static {
		TRIANGLE_PX.put(TRIANGLE_POSITION_RIGHT, 24);
		TRIANGLE_PX.put(TRIANGLE_POSITION_BOTTOM, 44);
		TRIANGLE_PX.put(TRIANGLE_POSITION_LEFT, 24);
		TRIANGLE_PX.put(TRIANGLE_POSITION_TOP, 44);
	}
	
	public static void _tooltip(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String data = getArgAsJson("data", args);
		String validate = getArgAsString("validate", args);
		String name = getArgAsString("name", args);
		String target = getArgAsString("target", args);
		String position = getArgAsString("position", args);
		String start = getArgAsString("start", args);
		String function = getArgAsString("function", args);
		String onClick = getArgAsString("onClick", args);
		
		StringBuilder sb = new StringBuilder();
		
		if (validate != null) {
			sb.append(String.format("if (!%s(this, (typeof event != \"undefined\" ? event : null))) return;", validate));
		}
		
		sb.append(String.format(
			"var res = Tooltip.open('%s', %s, %s, (typeof event != \"undefined\" ? event : null)%s%s);",
			name, data,
			target != null ? target : "this",
			position != null ? (", '" + position + "'") : ", false",
			start != null ? (", '" + start + "'") : ""
		));
		
		sb.append(String.format(
			"Click.setClickFunction(function() {%s}, this%s);",
			function != null ? (!function.contains("(") ? "()" : "") : "",
			name != null ? (", '" + name + "'") : ""
		));
		
		if (onClick != null) {
			sb.append(String.format("if (res) {%s(this, (typeof event != \"undefined\" ? event : null));}", onClick));
		}
		
		out.print(JavaExtensions.escapeHtml(sb.toString()));
	}
	
	public static void _tooltipTplStyled(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		if (body != null) {
			String function = getArgAsString("function", args);
			String name = getArgAsString("name", args);
			Integer width = getArgAsInt("width", args, 250);
			width = width < 0 ? 250 : width;
			String trianglePosition = getArgAsString("trianglePosition", args, "right").toLowerCase();
			String closeTitle = getArgAsString("closeTitle", args, "Close");
			String className = getArgAsString("closeTitle", args, "");
			String onclick = getArgAsString("onclick", args);
			Integer triangleTop = getArgAsInt("triangleTop", args);
			Integer triangleLeft = getArgAsInt("triangleLeft", args);
			Integer trianglePx = TRIANGLE_PX.get(trianglePosition);
			Boolean close = getArgAsBoolean("close", args);
			String triangleStyle = "";
			Map<String, Integer> margin = new HashMap<String, Integer>();
			String marginStyle = "";
			
			if (trianglePosition.equalsIgnoreCase(TRIANGLE_POSITION_RIGHT)) {
				if (triangleTop != null) {
					trianglePx += triangleTop;
				}
				triangleStyle = "top: " + trianglePx + "px;";
				margin.put("top", -trianglePx - 12);
				margin.put("right", 5);
			} else if (trianglePosition.equalsIgnoreCase(TRIANGLE_POSITION_BOTTOM)) {
				if (triangleLeft != null) {
					trianglePx += triangleLeft;
				}
				triangleStyle = "left: " + trianglePx + "px;";
				margin.put("left", -trianglePx - 12);
				margin.put("bottom", 5);
			} else if (trianglePosition.equalsIgnoreCase(TRIANGLE_POSITION_LEFT)) {
				if (triangleTop != null) {
					trianglePx += triangleTop;
				}
				triangleStyle = "top: " + trianglePx + "px;";
				margin.put("top", -trianglePx - 12);
				margin.put("left", 5);
			} else if (trianglePosition.equalsIgnoreCase(TRIANGLE_POSITION_TOP)) {
				if (triangleLeft != null) {
					trianglePx += triangleLeft;
				}
				triangleStyle = "left: " + trianglePx + "px;";
				margin.put("left", -trianglePx - 12);
				margin.put("top", 10);
			}
			
			for (String marginPos : margin.keySet()) {
				Integer marginVal = margin.get(marginPos);
				marginStyle += "margin-" + marginPos + ": " + marginVal + "px; ";
			}
			
			if (function == null) {
				function = "Tooltip.close('" + name + "')";
			}
			
			if (onclick != null) {
				onclick = String.format(" onclick=\"%s\" ", JavaExtensions.escapeHtml(onclick));
			}
			
			StringBuilder sb = new StringBuilder();
			
			sb.append("<div class=\"hidden ").append(className).append("\"")
				.append(" id=\"tooltip_").append(name).append("\"")
				.append(" style=\"position: absolute;\"").append(" ").append(onclick).append(">")
				.append("<script type=\"text/javascript\">")
				.append("dojo.ready(function(){Click.setClickFunction(function() {")
				.append(function).append(function.contains("(") ? "" : "()").append("}, false")
				.append(name != null ? (", \"tooltip_" + name + "\"") : "").append(");});</script>")
				.append("<div onclick=\"").append(JavaExtensions.escapeHtml(
					new StringBuilder().append("Click.setClickFunction(function() {").append(function)
					.append(function.contains("(") ? "" : "()").append("}, this")
					.append(name != null ? (", \"tooltip_" + name + "\"") : "").append(");").toString()
				)).append("\">")
				.append("<div class=\"tooltip").append(trianglePosition).append("\"")
				.append(" style=\"position: relative; width: ").append(width).append("px;").append(marginStyle).append("\">")
				.append("<div class=\"triangle\" style=\"").append(triangleStyle).append("\"><!-- --></div>")
				.append("<div class=\"top\"><div class=\"back\"></div><div class=\"right\"></div></div>")
				.append(close == null || close ? (
					new StringBuilder().append("<div class=\"close\" onclick=\"Tooltip.close('")
					.append(name).append("')\"").append(" title=\"").append(closeTitle).append("\"><!-- --></div>").toString()
				) : "")
				.append("<table class=\"cont\"><tr><td class=\"left\"></td><td class=\"back\"><div class=\"border\" style=\"overflow: hidden\">")
				.append(JavaExtensions.toString(body))
				.append("</div></td><td class=\"right\"></td></tr></table>")
				.append("<div class=\"bottom\"><div class=\"back\"></div><div class=\"right\"></div></div>")
				.append("</div></div></div>");
			
			out.print(sb.toString());
		}
	}
	
	public static void _tooltipTpl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		if (body != null) {
			String name = getArgAsString("name", args);
			String function = getArgAsString("function", args, "Tooltip.close('" + name + "');");
			String onclick = getArgAsString("onclick", args);
			String className = getArgAsString("class", args, "");
			Boolean registerGlobalClick = getArgAsBoolean("globalClick", args, true);
			
			if (onclick != null) {
				onclick = " onclick=\"" + JavaExtensions.escapeHtml(onclick) + "\" ";
			}
			
			StringBuilder sb = new StringBuilder();
			
			sb.append(String.format("<div class=\"hidden %s\" id=\"tooltip_%s\" style=\"position: absolute;\" %s>", className, name, onclick));
			
			if (registerGlobalClick) {
				sb.append("<script type=\"text/javascript\">");
				sb.append(String.format("dojo.ready(function(){Click.setClickFunction(function() {%s%s}, false%s);});</script>", 
					function,
					!function.contains("(") ? "()" : "",
					name != null ? (", 'tooltip_" + name + "'") : ""
				));
			}
			
			sb.append("<div");
			
			if (registerGlobalClick) {
				sb.append(String.format(" onclick=\"Click.setClickFunction(function() {%s%s}, this%s);\"",
					function,
					!function.contains("(") ? "()" : "",
					name != null ? (", 'tooltip_" + name + "'") : ""
				));
			}
			
			sb.append(">");
			sb.append(body != null ? JavaExtensions.toString(body) : "");
			sb.append("</div></div>");
			
			out.print(sb.toString());
		}
	}
	
	public static void _tooltipClose(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String name = getArgAsString("name", args);
		String function = getArgAsString("function", args);
		String invoke = getArgAsString("invoke", args);
		StringBuilder sb = new StringBuilder();
		
		if (StringUtils.isNotBlank(name)) {
			sb.append(String.format("Tooltip.close('%s');", name));
		}
		
		if (StringUtils.isNotBlank(invoke)) {
			sb.append(String.format("%s;", invoke));
		}
		
		if (function != null) {
			sb.append("return ").append(function).append("();");
		}
		
		out.print(JavaExtensions.escapeHtml(sb.toString()));
	}
	
	public static void _tooltipCloseIcon(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String id = getArgAsString("id", args);
		String className = getArgAsString("appendClass", args);
		
		out.print("<span ");
		
		if (StringUtils.isNotBlank(id)) {
			out.print(new StringBuilder().append(" id=\"").append(id).append("\" "));
		}
		
		out.print(" title=\"Close\" aria-hidden=\"true\" class=\"tooltipCloseIcon dijitDialogCloseIcon " + (StringUtils.isNotBlank(className) ? className : "") + "\" onclick=\"");
		_tooltipClose(args, body, out, template, fromLine);
		out.print("\"></span>");
	}
}