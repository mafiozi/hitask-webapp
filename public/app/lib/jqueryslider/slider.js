/**
 * Slider object
 * 
 * Valid events are:
 * 		change			- when slider value changes, to listening function is passed arguments: event, slider value
 * 
 * @param {Object} config
 */
function Slider(config) {
	//Set reference
		var self = this;
		
	//Set configuration
		this.config = $.extend({
			//Required
			element: null,
			slider: null,
			
			//Optional
			input: null,				//Input element into which value will be put
			values: [],					//List of index/values
			value: 0,					//Initial value
			itemTagName: 'A',			//Tag name
			orientation: 'horizontal',	//or vertical
			auto_width: false			//adjust width with script
		}, config || {}, {
			values2index: {},
			elements: null,				//Elements list
			index: 0,					//Initial index
			sliderPos: 0,				//Slider current position
			mouseDown: false			//Mouse button is pressed
		});

	//Set elements
		this.config.element = $(this.config.element);
		this.config.slider = $(this.config.slider);
		this.config.elements = this.config.element.find(this.config.itemTagName);
		
	//Populate values if needed
		var self = this;
		
		this.config.elements.each(function (index) {
			if (self.config.values[index] === undefined) {
				self.config.values[index] = index;
			}
			if (self.config.values[index] == self.config.value) {
				self.config.index = index;
			}
			
			self.config.values2index[self.config.values[index]] = index;
			
			$(this).data('indexInSlider', index);
			
			$(this).click(function (e) {
				return false;
			});
			
			$(this).mousedown(function (e) {
				self.config.mouseDown = true;
				self.setIndex($(this).data('indexInSlider'));
				
				return false;
			});
			
			$(document).mouseup(function (e) {
				if (!self.config.mouseDown) return;
				self.config.mouseDown = false;
				
				return false;
			});
			
			$(this).mouseover(function (e) {
				if (!self.config.mouseDown) return;
				self.setIndex($(this).data('indexInSlider'));
				
				return false;
			});
		});
		
		$(document.body).mousemove(function (e) {
			if (!self.config.mouseDown) return;
			return false;
		});

	//Set widths
		if (this.config.auto_width) {
			var sum = this.config.element.get(0).offsetWidth - 88;	//44 for first and last item
			var w = Math.floor(sum / (this.config.elements.length - 2)) + 'px';
			
			for(var i=0,j=this.config.elements.length; i<j; i++) {
				this.config.elements.eq(i).width((i == 0 || i == j-1 ? '44px' : w));
			}
		}
				
	//Set default value if it's invalid
		if (this.config.values2index[this.config.value] === undefined) {
			this.config.index = 0;
			this.config.value = this.config.values[0];
		}
		
		this.config.index = 999;
		this.setValue(this.config.value, true);
		
		var self = this;
		setTimeout(function () {
			self.config.index = 999;
			self.setValue(self.config.value, true);
		}, 10)
		
};

//Returns current index
Slider.prototype.getIndex = function () {
	return this.config.index;
}

//Returns current value
Slider.prototype.getValue = function () {
	return this.config.value;
}

//Returns slider container element
Slider.prototype.getEl = function () {
	return this.config.element;
};

//Move to specific value
Slider.prototype.setValue = function (value, quick) {
	if (this.config.values2index[value] !== undefined) {
		this.setIndex(this.config.values2index[value], quick);
	}
};

//Move to specific index
Slider.prototype.setIndex = function (element_index, quick) {
	if (this.config.values[element_index] !== undefined && element_index != this.config.index) {
		if (!quick) quick = false;
		
		this.config.elements.eq(this.config.index).removeClass('active');
		
		this.config.index = element_index;
		this.config.value = this.config.values[element_index];
		
		var el = this.config.elements.eq(this.config.index);
		var slider = this.config.slider;
		
		this.config.elements.eq(this.config.index).addClass('active');
		
		//Stop current animation
		this.config.slider.stop();
		
		if (this.config.orientation == 'horizontal') {
			var targ_pos = Math.floor(el.get(0).offsetLeft + (el.get(0).offsetWidth - slider.get(0).offsetWidth) / 2);
			this.config.slider.animate({left: targ_pos + 'px'}, 200);
		} else {
			var targ_pos = Math.floor(el.get(0).offsetTop + (el.get(0).offsetHeight - slider.get(0).offsetHeight) / 2);
			this.config.slider.animate({top: targ_pos + 'px'}, 200);
		}
		
		this.config.sliderPos = targ_pos;
		
		if (this.config.input) {
			$(this.config.input).val(this.config.values[this.config.index]);
		}
		
		//Fire event
		$(this).trigger('change', this.config.values[this.config.index]);
	}
};

//Configuration
Slider.prototype.config = {};