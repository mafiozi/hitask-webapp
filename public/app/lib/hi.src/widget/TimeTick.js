define([
	
	"dijit/_Widget",
	"dijit/_Templated"
	
], function (_Widget, _Templated) {
	
	dojo.declare("hi.widget.TimeTick", [_Widget,_Templated], {keys:"", templateString: "<div style=\"position: absolute\"><div dojoAttachPoint=\"timeTickNode\" class=\"timetable_currenctTimeTick icon-caret-right\" style=\"position:absolute;\"></div></div>", _timeoutId: null, fillInTemplate:function () {
		this._setPosition();
	}, _setPosition: function() {
		var time = new Date();
		var scope = HiTaskCalendar.getTimetableScope();
		if (time >= scope[0] && time <= scope[1]) {
			dojo.dom.show(this.timeTickNode);
			var top = time.valueOf() - scope[0].valueOf();
			top /=  1000 * 60;
			top *= HiTaskCalendar.pixelsPerMinute;
			top = Math.floor(top);
			this.timeTickNode.style.top = top + 'px';
		} else {
			dojo.dom.hide(this.timeTickNode);
		}
		var self = this;
		this._timeoutId = dojo.global.setTimeout(function(){self._setPosition()}, 60000);
	}, reload: function() {
		dojo.global.clearTimeout(this._timeoutId);
		this._setPosition();
	}});
	
	return hi.widget.TimeTick;
});