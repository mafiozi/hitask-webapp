package beans;

import com.google.gson.Gson;

import play.Play;

/**
 * Dummy java bean. Update and replace it as soon as API configuration will be available.
 * @author Vadim Manikhin
 */
public class ApiConfig {
	private String httpScheme = Play.configuration.getProperty("api.schemeName");
	private String apiHost = Play.configuration.getProperty("api.host");
	private String urlIndex = Play.configuration.getProperty("api.baseUrl");
	private String key = Play.configuration.getProperty("api.key");
	private String apiUrl;
	private String apiUrlWithoutIndex;
	
	public ApiConfig() {
		StringBuilder sb = new StringBuilder().append(httpScheme).append("://").append(apiHost);
		apiUrlWithoutIndex = sb.toString();
		apiUrl = sb.append(urlIndex).toString();
	}
	
	public String getVersion() {
		return "2.0";
	}
	
	public String getApiHost() {
		return apiHost;
	}
	
	public String getUrlIndex() {
		return urlIndex;
	}
	
	public String getKey() {
		return key;
	}
	
	public String getSessionName() {
		return "session_id";
	}
	
	public String getAsJson() {
		return new Gson().toJson(this);
	}
	
	public String getApiUrl() {
		return apiUrl;
	}
	
	public String getApiUrlWithoutIndex() {
		return apiUrlWithoutIndex;
	}
}