define([], function() {
	return {
		today: {
			id: 8,
			label: 'center.today',
			title: 'today',
			defaultShow: true
		},
		my: {
			id: 0,
			label: 'task.ungroup',
			title: 'my items',
			defaultShow: true
		},
		overdue: {
			id: 9,
			label: 'task.overdue',
			title: 'overdue',
			defaultShow: false
		},
		date: {
			id: 2,
			label: 'task.date',
			title: 'date',
			defaultShow: true
		},
		calendar: {
			id: 7,
			label: 'task.calendar',
			title: 'calendar',
			defaultShow: true
		},
		project: {
			id: 4,
			label: 'task.project',
			title: 'project',
			defaultShow: true
		},
		color: {
			id: 3,
			label: 'task.color',
			title: 'color',
			defaultShow: true
		},
		team: {
			id: 5,
			label: 'task.user',
			title: 'team',
			defaultShow: true
		},
		activity: {
			id: 1,
			label: 'task.activity_feed',
			title: 'activityfeed',
			defaultShow: false
		}
	};
});
