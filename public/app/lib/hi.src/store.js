define([
	"hi/store/Builder",
	'dojo/Deferred',
	"hi/widget/task/list",
	'hi/widget/task/listtooltip',
	"hi/widget/task/item",
	"hi/widget/task/itemnew",
	"hi/widget/task/projectlist",
	"hi/widget/task/group",
	"hi/widget/task/project",
	"hi/widget/task/itemproperties",
	"hi/widget/task/itemedit",
	"hi/widget/task/itemcolorvalue"
], function (hi_store, Deferred) {
	hi_store.init = function () {
		// summary:
		//		Initialize store
		//		Is overwriten in store-hitask.js and store-archive.js
		
		if (typeof hi_store.initStore === "function") {
			hi_store.initStore();
		}
		//hi_store.afterInit();
	};
	
	hi_store.afterInit = function() {
		dojo.global.reLayout();
		console.log('afterInit');
	};
	
	hi_store.dataLoaded = new Deferred();

	hi_store.afterLoad = function() {
		/*Callback in Observer*/
		this.dataLoaded.resolve();
		hideThrobber();
		dojo.global.reLayout();
		if (!hi_store.Project.refreshTimerOn) {
			hi_store.Project.refreshStartTimer();
		}
	};
	
	return hi_store;
});