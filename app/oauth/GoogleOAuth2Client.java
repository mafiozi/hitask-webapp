package oauth;

import java.util.Map;

public class GoogleOAuth2Client extends OAuth2Client {
	public GoogleOAuth2Client(String authorizationURL, String accessTokenURL, String clientid, String secret, String providerName) {
		super(authorizationURL, accessTokenURL, clientid, secret, providerName);
	}
	
	public GoogleOAuth2Client(String authorizationURL, String accessTokenURL, String clientid, String secret, String providerName, String icon) {
		super(authorizationURL, accessTokenURL, clientid, secret, providerName, icon);
	}
	
	public GoogleOAuth2Client(String authorizationURL, String accessTokenURL, String clientid, String secret, String providerName, String icon, String buttonClass) {
		super(authorizationURL, accessTokenURL, clientid, secret, providerName, icon, buttonClass);
	}
	
	@Override
	public void retrieveVerificationCode(String callbackURL, Map<String, String> parameters, String state) {
		parameters.put("response_type", "code");
		// If scope changed notify Android developer.
		parameters.put("scope", "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/tasks");
		parameters.put("access_type", "offline");
		if (isForceApprovalPrompt()) {
			parameters.put("approval_prompt", "force");
		} else {
			parameters.put("approval_prompt", "auto");
		}
		super.retrieveVerificationCode(callbackURL, parameters, state);
	}
}
