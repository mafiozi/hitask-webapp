define([
	"hi/upload/file",
	"hi/upload/widget",
	"hi/upload/droparea",
	"hi/upload/list",
	"hi/upload/container",
	"hi/upload/tooltip",
	"hi/upload/transport/html5",
	"hi/upload/transport/html5-file",
	"hi/upload/base"
], function () {
	return {};
});

/*
dojo.provide("hi.upload");

dojo.require("hi.upload.file");
dojo.require("hi.upload.widget");
dojo.require("hi.upload.droparea");
dojo.require("hi.upload.list");
dojo.require("hi.upload.container");
dojo.require("hi.upload.tooltip");
dojo.require("hi.upload.transport.html5");
dojo.require("hi.upload.base");
*/