/**
 * Build calendar
 * Build tabs
 * Start interval timers for getting messages, team member info. etc.
 * Instantiate modal info./message pop up dialog
 * Set up some preferences
 */
define([
	"dijit/Dialog",
	"dojo/dom-geometry",
	"dojo/aspect",
	"dojo/on",
	'dojo/_base/array',
	'dojo/window',
	'dijit/CheckedMenuItem',
	'hi/config/tabs',
	'hi/tabs'
], function (dijit_Dialog, domGeo, aspect, on, array, _window, CheckedMenuItem, tabsConfig, Tabs) {
	
	var Team = dojo.global.Team,
		Hi_Team	= dojo.global.Hi_Team,
		Project	= dojo.global.Project;
	
	Hi_Reminder.init();
	
	dojo.global.hi_init = function() {
		
		//Initialize hi.store
		hi.store.init();
		
		var browser_class = '';
		if (dojo.isOpera) { browser_class = 'opera'; }
		if (dojo.isIE && dojo.isIE <= 6) { browser_class = 'ie6'; }
		if (dojo.isIE && dojo.isIE < 8 && dojo.isIE >= 7) { browser_class = 'ie7'; }
		if (dojo.isIE && dojo.isIE == 8) { browser_class = 'ie8'; }
		if (dojo.isIE && dojo.isIE >= 9 && dojo.isIE < 10) { browser_class = 'ie9'; }
		if (dojo.isIE && dojo.isIE >= 10) { browser_class = 'ie10'; }
		if (dojo.isFF) { browser_class = 'firefox'; }
		if (dojo.isSafari) { browser_class = 'safari'; }
		if (dojo.isChrome) { browser_class = 'chrome'; }
		
		if (browser_class) {
			dojo.addClass(document.body, browser_class);
		}
		
		if (parseInt(Hi_Preferences.get('sidebar_calendar', '1')) == dojo.hasClass(dojo.byId('centerPane'), 'no_column1')) {
			toggleColumn('column1', 'calendar', 'messenger', null, true);
		}
		if (parseInt(Hi_Preferences.get('sidebar_messenger', '1')) == dojo.hasClass(dojo.byId('centerPane'), 'no_column2')) {
			toggleColumn('column2', 'messenger', 'calendar', null, true);
		}
		
		CONSTANTS.timezone = (new Date()).getTimezoneOffset();
		
		document.body.focus();
		
		/* Bug #2181 
		 * This sets the start and end times on the time table;
		 * it should be set before loading the load() call, which is where the time table is drawn
		 */
		var time;
		if (time = Hi_Preferences.get('ttstart')) {
			if ((/^((0\d)|(10))\:00/.test(time))) {
				// HiTaskCalendar.startTime = time;
				var timeA = time.split(':');
				timeA[0] = parseInt(timeA[0], 10);
				timeA[0] += 14;
				// HiTaskCalendar.endTime = timeA.join(':');
				timeA[0]--;
				timeA[1] = 59;
				// HiTaskCalendar.lastStartTime = timeA.join(':');
			}
		}
		
		HiTaskCalendar.load();
		
		dojo.connect(window, 'onresize', Hi_Calendar, 'moveCalendar');
		
		//When resizing, make sure to call only after resize has finished, because dropCache is expensive
		var resize_delay_timer = null;
		var resizing = false;
		function after_resize () {
			var c = Hi_Tag.tag_bar();
				c.style.height = 'auto';
				c.style.overflow = 'visible';
				
			Hi_Tag.dropCache();
			
			setTimeout(function () {
				clearTimeout(resize_delay_timer);
				resizing = false;
			}, 50);
		}
		function before_resize () {
			resizing = true;
			
			var c = Hi_Tag.tag_bar();
				c.style.height = '16px';
				c.style.overflow = 'hidden';
		}

		function getTabsWidth() {
			var tabMenu = dojo.query('#main .menu table')[0],
				tabMenuContainer = dojo.query('#main .menu')[0], paddingLeft, paddingRight,
				tabMore = dojo.query('.more')[0];

			if (!tabMenu || !tabMenuContainer) return;

			paddingLeft = domGeo.getPadBorderExtents(tabMenuContainer).l;
			paddingRight = domGeo.getPadBorderExtents(tabMenuContainer).r;
			tabMenuMaxWidth = tabMenu.clientWidth + paddingLeft + paddingRight + tabMore.clientWidth;

			return tabMenuMaxWidth;
		}

		function resizeTab(){
			var menu = dojo.query('#main .menu')[0],
				mainContent = dojo.byId('mainContent'),
				bigCalendar = dojo.byId('bigCalendar'), 
				mainWidth;

			if (!menu || !mainContent) {return;}
			dojo.removeClass(menu, 'shrink');

			mainWidth = (mainContent && mainContent.clientWidth) || (bigCalendar && bigCalendar.clientWidth);

			if (getTabsWidth() > mainWidth) {
				dojo.addClass(menu, 'shrink');
			} else {
				dojo.removeClass(menu, 'shrink');
			}
		}

		dojo.connect(window, 'onresize', function () {
			var afterResize = function() {
				after_resize();
				dojo.global.reLayout();
			};
			
			if (resizing) {
				clearTimeout(resize_delay_timer);
				resize_delay_timer = setTimeout(afterResize, 150);
			} else {
				resize_delay_timer = setTimeout(afterResize, 150);
				before_resize();
			}
			resizeTab();
			dojo.global.reLayout();

			if (dijit.popup._args) {
				var popup = dijit.popup._args.popup;

				if (popup && popup.domNode) popup.domNode.style.maxHeight = _window.getBox().h + 'px';
			}
		});

		aspect.after(dojo.global, "toggleColumn", function (deferred) {
			if(deferred && deferred.then){
				deferred.then(function(){
					resizeTab();
				});
			}
		});

		dojo.connect(Tabs, 'open', function(tab_id, index){
			resizeTab();
		});
		
		dojo.connect(window, 'onmouseup', Hi_Calendar, 'stopScroll');
		window.onbeforeunload = function() {
			if (dojo.global.inOnBeforeUnload === true) {
				return;
			}
			dojo.global.inOnBeforeUnload = true;
			var k, uploader, project,
				isUploading = false,
				isProjectEditing = false;

			for (k in FileUpload._uploaders) {
				if (k !== 1) {
					uploader = FileUpload._uploaders[k];
					array.forEach(uploader.files, function(file) {
						if (file.uploading) {
							isUploading = true;
						}
					});
				}
			}

			array.forEach(hi.items.ProjectListProjects.projects, function (project) {
				if (project.editing) isProjectEditing = true;
			});
			
			if (
				(Hi_Timer.isRunning() && !Hi_Timer.isPaused()) ||
				hi.items.manager.editingId() || isUploading ||
				(Tooltip.opened('newproject') || isProjectEditing)
			) {
				setTimeout(function() {
					if (dojo.global.isSessionEnd === true) {
						dojo.global.stayOnPage = true;
					}
					dojo.global.inOnBeforeUnload = false;
				}, 10);
				if (dojo.global.inLogout !== true) return ' ';
				// return hi.i18n.getLocalization('unload.warning');
			} else{
				dojo.global.inOnBeforeUnload = false;
			}
		};
		// Bug #5184
		var centerPaneContent = dojo.byId('centerPaneContent');
		if (centerPaneContent) {
			centerPaneContent.onscroll = function() {
				var pp = dojo.query('body>div[dijitpopupparent]'), parent;

				pp.forEach(function(p) {
					parent = p.getAttribute('dijitpopupparent');
					parent = parent && dijit.byId(parent);

					if (parent && parent.domNode &&
						centerPaneContent.contains(parent.domNode) &&
						p.clientHeight && p.clientWidth
					) {
						p.style.display = 'none';
					}
				});
			};
		}

		var archiveTabs = new Tabs('category_type', {
			content: false,
			selectedClass:'active', 
			onChange: switchCategory, 
			defaultTab: 1,
			elements: [1, 2, 4, 5, 6]
		});

		var mainTabs = new Tabs('grouping', {
			content: false,
			selectedClass:'active',

			onChange: function(n) {
				var old_n = Hi_Preferences.get('currentTab', 'my', 'string');
				Hi_Preferences.set('currentTab', n);
				Project.groupPending = null;

				var mainContainer = dojo.byId('mainContainer');
				var scrolltops = dojo.global.groupingScrollTop = dojo.global.groupingScrollTop || {};
				scrolltops[old_n] = mainContainer && mainContainer.scrollTop;
				hi.items.manager.groupingChange(n, old_n);
				/*var res = fillTasks();
				if (!res) {
					Hi_Preferences.set('currentTab', old_n);
					Project.groupPending = n;
					return false;
				}*/
				
				if (!Hi_Search.isResultsVisible()) {
					Hi_Tag.toggleBar();
					Hi_Tag.dropCache();
				}

				return true;
			},

			defaultTab: Hi_Preferences.get('currentTab', 'my', 'string') || 'my',
			elements: [8, 0, 2, 7, 4, 3, 5, 1],
			config: tabsConfig
		});
		dojo.global.tabs = {
			main: mainTabs,
			archive: archiveTabs
		};
		dojo.connect(mainTabs, 'onAddTab', function() {
			resizeTab();
		});
		dojo.connect(mainTabs, 'onRemoveTab', function() {
			resizeTab();
		});
		
		aspect.after(hi.items.manager, 'groupingChange', function(n, old_n){
			// https://red.hitask.net/issues/4163#note-41
			var mainContainer = dojo.byId('mainContainer');
			var scrolltops = dojo.global.groupingScrollTop || {};
			// reset scrollTop when switching tab
			if (mainContainer) mainContainer.scrollTop = (scrolltops && scrolltops[n]) || 0;
			dojo.global.reLayout();
		}, true);
		
		//TaskFilter
		Hi_TaskFilter.init();
		
		Team.businessLoad();
		// set interval loadBusiness 
		var s = parseInt(CONSTANTS.refreshIntervalBusiness);
		if (!isFinite(s) || s <= 0) {
			s = 10;
			CONSTANTS.refreshIntervalBusiness = s;
		}
		/* Bug #1823 
		Team.intervalBusiness = dojo.global.setInterval(Project.pingBusiness, s * 1000);
		*/
		Team.intervalBusiness = dojo.global.setInterval(function() {
			var obj = {
				request: Team.deferredBusinessRequest,
				requestStartTime: Team.businessRequestStartTime,
				doRequest:'businessLoad',
				scope:Team
			};
			dojo.global.fireRequest(obj);
			Team.deferredBusinessRequest = obj.request;
			Team.businessRequestStartTime = obj.requestStartTime;
		}, s*1000);
		
		Team.messagesLoad();
		
		// set interval loadMessages 
		s = parseInt(CONSTANTS.refreshIntervalMessages);
		if (!isFinite(s) || s <= 0)  {
			s = 10;
			CONSTANTS.refreshIntervalMessages = s;
		}
		/* Bug #1823 
		Team.intervalMessages = dojo.global.setInterval(Project.pingMessages, s * 1000);
		*/
		Team.counter = 1;
		Team.bogusInterval = 3;
		Team.intervalMessages = dojo.global.setInterval(function() {
			var obj = {
				request:Team.deferredRequest,
				requestStartTime:Team.requestStartTime,
				doRequest:Team.counter%Team.bogusInterval!=0?'messagesLoad':'bogusLoad',
				scope:Team
			};
			dojo.global.fireRequest(obj);
			Team.deferredRequest = obj.request;
			Team.requestStartTime = obj.requestStartTime;
			/* Debug #1823 - To test sending bad URL requests Team.counter++; */
			//console.log('pingMessages',test);
		},s*1000);
		
		// set interval loadContacts 
		s = parseInt(CONSTANTS.refreshIntervalContacts);
		if (!isFinite(s) || s <= 0)  {
			s = 10;
			CONSTANTS.refreshIntervalContacts = s;
		}
		/* Bug #1823 Team.intervalContacts = dojo.global.setInterval(Project.pingContacts, s * 1000); */
		Team.intervalContacts = dojo.global.setInterval(function() {
			var obj = {
				request:Team.deferredRequest,
				requestStartTime:Team.requestStartTime,
				doRequest:'pingContacts',
				scope:dojo.global.Project
			};
			dojo.global.fireRequest(obj);
			Team.deferredContactsRequest = obj.request;
			Team.contactsRequestStartTime = obj.requestStartTime;
			
			//console.log('pingMessages',test);
		},s*1000);
		
		dojo.connect(window, "onunload", Hi_Preferences, "send");
		dojo.connect(window, "onunload", Hi_Preferences, "clear");
		
		Hi_Team.closeChat();
		
		//Project "Add" button
		dojo.parser.instantiate(dojo.query('button[dojoType]', dojo.byId('project')));
		
		dojo.parser.instantiate(dojo.query('#addPeopleBtn'));
		dojo.removeClass('addPeopleBtnWrapper','hidden');
		
		//Create widget
		Project.alertElement = dijit.byId('alertElement');
		
		//Don't restore focus, this is handled manually
		Project.alertElement.refocus = false;
		
		Project.alertElement.showItem = function(id, data) {
			data = data || {};
			if (id == 3 || id == 4 || id == 6) {
				hideElement(Project.alertElement.closeButton);
			} else {
				showElement(Project.alertElement.closeButton);
			}
			if (id == 6) {
				Project.ajax = function() {};
			}
			openTab('alertElement', id, 0, data);
			Project.alertElement.show();
			/*#4449*/
			dojo.query(".dijitDialogUnderlayWrapper").style({
				'z-index': 10000
			});
			/*Bug #4348*/
			if(Project.alertElement && Project.alertElement.domNode){
				Project.alertElement.domNode.style.zIndex = 20000;
			}
		};
		
		/* Feature #1721 Project.alertElement.closeButton = dojo.query(".close", "alertElement")[0];*/
		Project.alertElement.closeButton = dojo.query(".tooltipCloseIcon", "alertElement")[0];
		dojo.connect(Project.alertElement.closeButton, 'click', function () {
			Project.alertElement.hide();
		});
		
		//Reminder
		Hi_Reminder.reminderElement = dijit.byId('reminder_popup');
		/* Feature #1721 Hi_Reminder.reminderElement.closeButton = dojo.query(".close", "reminder_popup")[0];*/
		Hi_Reminder.reminderElement.closeButton = dojo.query(".tooltipCloseIcon", "reminder_popup")[0];
		dojo.connect(Hi_Reminder.reminderElement.closeButton, 'click', function () {
			Hi_Reminder.reminderElement.hide();
		});

/*	Bug #2181 - moved this above HiTaskCalendar.load() call
		var time;
		if (time = Hi_Preferences.get('ttstart')) {
			console.log("TIME=",time);
			if ((/^((0\d)|(10))\:00/.test(time))) {
				HiTaskCalendar.startTime = time;
				var timeA = time.split(':');
				timeA[0] = parseInt(timeA[0], 10);
				timeA[0] += 14;
				HiTaskCalendar.endTime = timeA.join(':');
				timeA[0]--;
				timeA[1] = 59;
				HiTaskCalendar.lastStartTime = timeA.join(':');
			}
		}
*/		
		// Hi_Team.initResizers();
		Hi_Team.initButtons();
		
		var teamHelpClose = dojo.query('div.close', dojo.byId('team_help'))[0];
		if (teamHelpClose) {
			dojo.connect(teamHelpClose, 'click', function() {
				Hi_Preferences.set('hide_team_help', 1);
				hideElement('team_help');
			});
		}

		// sorting
		var sorting = Project.getSorting();
		// var node = dojo.query('#toolbar div.popup')[0];
	
		// dojo.addClass(node, 'selected-' + sorting);
		// dojo.addClass(nextElement(node), 'sort-' + sorting);
		var sortingSelect = dijit.byId('sortingSelect');
		if (sortingSelect) {
			sortingSelect.set('value', sorting);
			aspect.after(sortingSelect.dropDown, 'onItemClick', function(node, evt) {
				sortingSelect.inItemClick = true;
				// console.log('in sorting change');
				// Project.changeSorting(val);
			});
			on(sortingSelect, 'change', function(val) {
				if (sortingSelect.inItemClick){
					Project.changeSorting(val);
					sortingSelect.inItemClick = false;
				}
			});
		}
		
		var container = dojo.byId(Project.getContainer('tasksList'));
		dojo.addClass(container, 'sorting-' + sorting);
		// project sorting
		var projectSorting = Project.getSorting(2);
		// var node = dojo.query('#toolbar div.popup')[1];

		// dojo.addClass(node, 'selected-' + projectSorting);
		// dojo.addClass(nextElement(node), 'sort-' + projectSorting);
		var projectSortingSelect = dijit.byId('projectSortingSelect');
		if (projectSortingSelect) {
			projectSortingSelect.set('value', projectSorting);
			aspect.after(projectSortingSelect.dropDown, 'onItemClick', function(node, evt) {
				projectSortingSelect.inItemClick = true;
				// console.log('in sorting change');
				// Project.changeSorting(val);
			});
			on(projectSortingSelect, 'change', function(val) {
				if (projectSortingSelect.inItemClick){
					Project.changeProjectSorting(val);
					projectSortingSelect.inItemClick = false;
				}
			});
		}
		var filterSelect = dijit.byId('filterSelect');
		if (filterSelect) {
			dojo.safeMixin(filterSelect, {
				_setValueAttr: function(val) {
					val = val.value || val;
					this.inherited(arguments);
					if (val === '0') {
						this.containerNode.firstChild.innerHTML = 'Filter';
					}
				}
			});
			filterSelect.set('value', filterSelect.get('value'));
		}
		var tagbar = dojo.query('.tagbar')[0],
			notag = dojo.hasClass('toolbar', 'no-tagbar'),
			currentGroup = Hi_Preferences.get('currentTab', 'my', 'string');

		// #5361
		/*if (tagbar) {
			console.log('in set tag bar width');
			if (currentGroup === 4) {
				if (notag) {
					tagbar.style.width = toolbar.clientWidth - sortingSelect.domNode.clientWidth -
										sortingSelect.domNode.clientWidth - newProjectButtonStandalone.clientWidth;
				} else {
					tagbar.style.width = toolbar.clientWidth - sortingSelect.domNode.clientWidth -
										sortingSelect.domNode.clientWidth - newProjectButton.clientWidth;
				}
			} else {
				tagbar.style.width = toolbar.clientWidth - sortingSelect.domNode.clientWidth - sortingSelect.domNode.clientWidth + 'px';
			}
		}*/
		
		var container = dojo.byId(Project.getContainer('tasksList'));
		dojo.addClass(container, 'sorting-' + sorting);
		
		//#316 Overwrite flash embeded javascript to prevent errors
		dojo.connect(window, "onbeforeunload", function () {
			dojo.global.__flash__removeCallback = function (instance, name) {
				try {
					instance[name] = null;
				} catch (e) {}
			};
		});
		
		//#438 If there are queue requests, ask for confirmation
		//If there are items in queue show confirmation
		dojo.connect(window, 'onbeforeunload', function(e) {
			if (ConnectionQueue.list.length) {
				var mesg = hi.i18n.getLocalization('notification.logout_confirmation');
				// For IE
				e.returnValue = mesg;
				// For all others
				return mesg;
			}
		});
		
		//Handle new project textarea resize
		handleDDElement(dojo.byId('newProjectResize').parentNode);

		var tabMenuMaxWidth = 0;

		if(preLoadDoneDeferred){
			preLoadDoneDeferred.then(function(){
				var tabMenu = dojo.query('#main .menu table')[0],
					tabMenuContainer = dojo.query('#main .menu')[0], paddingLeft, paddingRight,
					tabMore = dojo.query('.more')[0];

				if(!tabMenu || !tabMenuContainer){
					return;
				}

				paddingLeft = domGeo.getPadBorderExtents(tabMenuContainer).l;
				paddingRight = domGeo.getPadBorderExtents(tabMenuContainer).r;
				tabMenuMaxWidth = tabMenu.clientWidth + paddingLeft + paddingRight + tabMore.clientWidth;
				resizeTab();
			});
		}

		dojo.global.tasksListLoading();
		dojo.global.bigCalendarLoading();

		//Desktop notifications state change
		var notifyStateLink = dojo.byId('notifyState'),
			notifyStateMenuLink = dojo.byId('notifyMenuState'),
			desktopNotifyState = parseInt(Hi_Preferences.get('desktopNotifyState'));

		function setNotifyState(state) {
			if ("Notification" in window) {
				dojo.removeClass(notifyStateMenuLink, 'hidden');
				if (state == 0 || Notification.permission == 'denied' || Notification.permission == 'default') {
					notifyStateLink.innerHTML = hi.i18n.getLocalization("notification.notification_enable");
					// notifyStateMenuLink.innerHTML = hi.i18n.getLocalization("notification.notification_enable");
				} else {
					notifyStateLink.innerHTML = hi.i18n.getLocalization("notification.notification_disable");
					// notifyStateMenuLink.innerHTML = hi.i18n.getLocalization("notification.notification_disable");
				}
			} else {
				dojo.addClass(notifyStateLink, 'hidden');
				dojo.addClass(notifyStateMenuLink, 'hidden');
			}
		}
		
		setNotifyState(desktopNotifyState);

		function toggleNotifyState() {
			if (desktopNotifyState == 0 || Notification.permission == 'default') {
				dojo.global.requestNotifyPermissions(function(result) {
					switch (result) {
						case 'granted':
							desktopNotifyState = 1;
							Hi_Preferences.set('desktopNotifyState', 1);
							break;
						case 'denied':
							desktopNotifyState = 0;
							Hi_Preferences.set('desktopNotifyState', 0);
							break;
						case 'default':
							desktopNotifyState = 0;
							Hi_Preferences.set('desktopNotifyState', 0);
							break;
						case 'unsupported':
							desktopNotifyState = 0;
							Hi_Preferences.set('desktopNotifyState', 0);
							break;
					}
					setNotifyState(desktopNotifyState);
				});
			} else {
				Hi_Preferences.set('desktopNotifyState', 0);
				desktopNotifyState = 0;
				setNotifyState(desktopNotifyState);
			}
			Hi_Preferences.send();
		}

		dojo.connect(notifyStateMenuLink, 'click' ,function() {
			toggleNotifyState();
			dojo.toggleClass('personal_menu','hidden');
		});
	};
	
	return {
		'hi_init': hi_init
	};
});
