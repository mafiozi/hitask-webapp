//dojo.provide("hi.timetracking.personselect");
define(function () {
	
	return dojo.global.Hi_PersonSelect = {
		node: null,
		input: null,
		target: null,
		
		initialized: false,
		emptyFriend: {'image': 'url(/avatar/.16.gif)', 'name': '', 'id': null},
		
		fillPersonEntry: function (node, friend_data) {
			if (node && friend_data) {
				node.innerHTML = '<span class="person-avatar" style="background: ' + friend_data.image + '; background-size: contain; "></span><span class="person-name">' + friend_data.name + '</span>';
			}
		},
		
		_onEntryClick: function (event, target) {
			var friend = Project.getFriend(target.entryId);
			if (!friend) friend = dojo.mixin({}, Hi_PersonSelect.emptyFriend, {'name': Project.user_name, 'id': Project.user_id});
			
			this.input.value = target.entryId;
			if (friend && friend.id) {
				friend.image = 'url(' + dojo.global.getAvatar(16, friend.id) + ')';
			}
			this.fillPersonEntry(this.target, friend);
			
			dojo.stopEvent(event);
			this.hide();
		},
		_createDropDown: function () {
			var node = document.createElement('UL');
				node.className = 'person-dropdown';
				
			var self = this;
			
			var friends = [];
			// add "myself" to top of the list
			friends.push({'name': Project.user_name, 'id': Project.user_id, 'image': 'url(/avatar/.16.gif)'});
			if(dojo.isArray(Project.friends)){
				for(var i in Project.friends){
					friends.push(Project.friends[i]);
				}
			}
			
			for(var i=0,ii=friends.length; i<ii; i++) {
				var entry = document.createElement('LI');
					entry.entryId = friends[i].id;
					node.appendChild(entry);
				
				if (friends[i] && friends[i].id) {
					friends[i].image = 'url(' + dojo.global.getAvatar(16, friends[i].id) + ')';
				}
				this.fillPersonEntry(entry, friends[i]);
				
				dojo.connect(entry, 'click', function (event) { self._onEntryClick(event, this); });
			}
			
			document.body.appendChild(node);
			
			return node;
		},
		
		_position: function (target) {
			var pos = dojo.position(target, true);
			this.node.style.left = ~~(pos.x) + 'px';
			this.node.style.top = ~~(pos.y) + 'px';
		},
		
		show: function (target, input) {
			this.init();
			
			this.input = input;
			this.target = target;
			
			this.node.style.display = 'block';
			this._position(target);
		},
			
		hide: function () {
			if (this.initialized) {
				this.node.style.display = 'none';
			}
		},
		
		init: function () {
			if (this.initialized) return;
			this.initialized = true;
			
			this.node = this._createDropDown();
			
			dojo.connect(document, 'click', this, this.hide);
		}
	};
	
});
