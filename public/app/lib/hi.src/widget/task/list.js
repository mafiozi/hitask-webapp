/**
 * The widget container for task items on the UI.  Examples include hi.items.TaskListUngrouped and hi.items.TaskListToday.
 * 
 * The majority of lists are instantiated in hi/items/view-X.js.  They use an observer of the datastore to determine when
 * items should be added, removed, or moved on their own lists, and keep a hash of items that basically references items
 * in the hi.items.manager._items hash.  This is handled via watchChanges and watchChangesRecurring
 */
define([
	"dojo/_base/array", // array.forEach array.map
	"dojo/_base/declare", // declare
	"dojo/cldr/supplemental", // cldrSupplemental.getFirstDayOfWeek
	"dojo/date", // date
	"dojo/date/locale",
	"dojo/dom", // dom.setSelectable
	"dojo/dom-class", // domClass.contains
	"dojo/_base/event", // event.stop
	"dojo/_base/lang", // lang.getObject, lang.hitch
	"dojo/_base/sniff", // has("ie") has("webkit")
	"dojo/string", // string.substitute
	"dojo/_base/window", // win.doc.createTextNode
	"dijit/_Widget",
	"dijit/_Templated",
	"dojo/Evented",
	'dojox/css3/transition',
	'dojo/query',
	"dojo/text!./list.html",
	//"hi/widget/task/item",
	//'hi/widget/task/item_noDijit',
	'hi/widget/task/item_DOM',
	'hi/widget/task/itemproperties'

	//'hi/widget/task/item_noDijit2'
], function(array, declare, cldrSupplemental, date, local, dom, domClass, event, lang, has, string, win,
			_WidgetBase, _TemplatedMixin, Evented, css3fx, dojoQuery, template, item_DOM, TaskItemProperties){
	// module:
	//		hi/widget/List
	// summary:
	//		Task list which updates itself on store QueryResults data change
	//		Note: hi.widget.TaskListTooltip extends from this one and doesn't have some of the elements
	
	
	var VALIDATION_SUCCESS = function () { return true; };
	
	
	var TaskList = declare("hi.widget.TaskList", [_WidgetBase, _TemplatedMixin, Evented], {
		// summary:
		//		Task list which updates itself on store QueryResults data change
		
		// Locales
		localeNoItems: hi.i18n.getLocalization("center.no_items"),
		localeNoProjectItems: hi.i18n.getLocalization("project.create_new"),
		localeItemCollapsed: "",  // single item
		localeItemsCollapsed: "", // multiple items
		
		// Template for the list
		templateString: template,

		// title: String
		//		Current list title 
		title: '',
		
		// listName: String
		//		Default task name sufix for hi.items.manager
		listName: null,
		
		// itemIds: Object
		//		List of ids including item children ids, used for counting items
		itemIds: null,
		
		// percentage: Object
		//		List of item percentage
		percentage: null,
		
		// itemWidgets: Object
		//		Item widget list
		itemWidgets: null,
		
		// itemCount: Number
		//		Number of items in the list
		itemCount: 0,
		
		// parentId: Number
		//		Default parent ID for new items
		parentId: 0,
		_setParentIdAttr: function (parentId) {
			this._set("parentId", parentId);
			this.listNode.setAttribute("data-list-id", parentId);
		},
		
		// initialyCollapsed: Boolean
		//		List should be collapsed on initial load
		//		Used in team and project lists for ungrouped tasks
		initialyCollapsed: false,
		
		// collapsed: Boolean
		//		List is collapsed and children isn't rendered
		collapsed: false,
		_setCollapsedAttr: function (collapsed) {
			var prevVal = this.collapsed;
			this._set("collapsed", collapsed);
			
			if (this.collapsedNode) {
				dojo.toggleClass(this.collapsedNode, "hidden", !collapsed);
			}
			
			if (prevVal == true && !collapsed) {
				//Render all items
				/* #2509
				this.rendered = true;
				this.reset();
				*/
				refreshTab([], [this], null, true);
			}
			if (prevVal == false && collapsed) {
				//List collapsed
				this.set("completedSubListExpanded", false);
				if (this.completedLabelNode) {
					dojo.addClass(this.completedLabelNode, "hidden");
				}
			}
			
			this.updateContainerItemCount();
			this.updateContainerPercentage();
		},
		
		// completedSubList: Boolean
		//		Completed items show at the bottom of the list
		completedSubList: true,
		_setCompletedSubListAttr: function (completedSubList) {
			this._set("completedSubList", completedSubList);
			
			if (completedSubList) {
				this.updateCompletedSubList();
			} else {
				if (this.completedLabelNode) {
					dojo.addClass(this.completedLabelNode, "hidden");
				}
			}
			
			if (completedSubList && !this.completedSubListExpanded) {
				dojo.addClass(this.domNode, "completed-hidden");
			} else {
				dojo.removeClass(this.domNode, "completed-hidden");
			}
		},
		
		// completedSubListExpanded: Boolean
		//		Completed items are expanded
		completedSubListExpanded: false,
		_setCompletedSubListExpandedAttr: function (completedSubListExpanded) {
			this._set("completedSubListExpanded", completedSubListExpanded);
			
			if (this.completedSubList && !completedSubListExpanded) {
				dojo.addClass(this.domNode, "completed-hidden");
			} else {
				dojo.removeClass(this.domNode, "completed-hidden");
				
				if (supportsCSS3Transitions()) {
					var self = this;
					var items = dojo.query('li.completed', this.domNode);
					for (var i = 0, len = items.length; i < len; i++) {
						css3fx.fade(items[i], {
							'in': true,
							duration: CONSTANTS.animationDuration.groupFadeMs,
							direction: 1
						}).play();
					}
				}
			}
		},
		
		// dragSource: Object
		//		dojo.dnd.HiDragSource instance
		dragSource: null,
		
		
		/* ----------------------------- ATTRIBUTES ------------------------------ */
		
		
		// filter: Function
		//		Filter function
		filter: null,
		
		_setFilterAttr: function (/*Function*/filter) {
			this._set('filter', filter || VALIDATION_SUCCESS);
			this.reset();
		},
		
		// filter type: String
		//		Filter type, "all" or "any"
		filterType: "custom",
		
		// readStore: hi.store.Cache
		//		Store instance for reading data
		readStore: null,
		
		// writeStore: hi.store.Observable
		writeStore: null,
		
		// container
		//		Container widget, can be null
		container: null,
		_setContainerAttr: function (container) {
			var old_container = this.container;
			
			this._set("container", container);
			this.updateContainerItemCount();
			this.updateContainerPercentage();
			
			if (container != old_container) {
				if (old_container) old_container.removeList(this);
				if (container) container.addList(this);
			}
			
			if (container) {
				dojo.removeClass(this.domNode, "tasks");
			} else {
				dojo.addClass(this.domNode, "tasks");
			}
		},
		
		// store: dojo.store.QueryResults
		//		Store QueryResults to which list is binded to
		query: null,
		
		_setQueryAttr: function (/*Object*/query) {
			if (this.queryObserver) {
				this.queryObserver.cancel();
				this.queryObserver = null;
			}
			
			if (query) {
				this.queryObserver = query.observe(this.proxy(this.watchChanges), 'deep');
			}
			
			this._set('query', query);
			//this.reset();
		},

		// Disabled
		disabled: false,
		showing:false,
		/* #2509 - to indicate whether or not this list has been fully initialized yet */
		initialized:false,
		_setDisabledAttr: function (disabled) {
			this._set("disabled", disabled);

			// Render list of items
			if (!this.rendered && !disabled && !this.collapsed && this.itemDOMs && this.itemCount > 0/*&& this.itemDOMs.length>0*/){
				this.rendered = true;
				var tmp = '',
					len = 0, i;
				
				for (i in this.itemDOMs) {
					this.itemWidgets[i].taskItem.widgetId = this.itemWidgets[i].widgetId;
					hi.items.manager.removeItem(i, this.itemWidgets[i].taskItem.eventdate||this.listName || (this.itemWidgets[i].recurring ? this.itemWidgets[i]['eventdate'] : null) );
					hi.items.manager.addItem(i,this.itemWidgets[i],this.itemWidgets[i].taskItem.eventdate||this.listName || (this.itemWidgets[i].recurring ? this.itemWidgets[i]['eventdate'] : null));
				}
				sorted = item_DOM._sortDOM(this.itemDOMs,this.itemWidgets);
				len = sorted.length;
				//console.log("SORTED",sorted);
				for (/*var i in this.itemDOMs*/i = 0; i < sorted.length; i++) {
					tmp+= this.itemDOMs[sorted[i]];
				}
				if (tmp.length>0) dojo.place(tmp,this.listNode,'first');
				sorted = [];
				if (this.completedSubList) {
					tmp = '';
					for (i in this.completedItemDOMs) {
						//tmp+=this.completedItemDOMs[i];
						this.itemWidgets[i].taskItem.widgetId = this.itemWidgets[i].widgetId;
						hi.items.manager.removeItem(i, this.itemWidgets[i].taskItem.eventdate||this.listName || (this.itemWidgets[i].recurring ? this.itemWidgets[i]['eventdate'] : null) );
						hi.items.manager.addItem(i,this.itemWidgets[i],this.itemWidgets[i].taskItem.eventdate||this.listName || (this.itemWidgets[i].recurring ? this.itemWidgets[i]['eventdate'] : null));
					}
					sorted = item_DOM._sortDOM(this.completedItemDOMs,this.itemWidgets);
					len = sorted.length;
					
					for (i = 0; i < sorted.length; i++) tmp += this.completedItemDOMs[sorted[i]];
					if (tmp.length > 0) dojo.place(tmp, dojo.query('.task_hidden', this.listNode)[0], 'after');
					// for (i in this.completedItemDOMs) {
					// 	if (him.get(i, 'recurring')) {
					// 		var node = dojo.byId('itemNode_' + this.itemWidgets[i].widgetId);
					// 		if (node) dojo.addClass(node, 'hidden');
					// 	}
					// }
				}
				Hi_Overdue.reload();
				//DnD
				setTimeout(dojo.hitch(this,function() {
					for (var i in this.itemWidgets)
					{
						this.itemWidgets[i].dragSource = new dojo.dnd.HiDragSource(dojo.byId('children_'+this.itemWidgets[i].widgetId), {});
					}
					if (this.dragSource) {
						this.dragSource.sync();
					}
					if (this.container && this.container.dragSource) {
						this.container.dragSource.sync();
					}
				}),200);
				/* #2418 */this.updateContainerItemCount();
				this.updateCompletedSubList();
			} /*else if (!disabled)
			{
				this.rendered = true;
				for (var i in this.itemWidgets)
				{
					hi.items.manager.removeItem(i);
					hi.items.manager.addItem(i,this.itemWidgets[i],this.listName || (this.itemWidgets[i].recurring ? this.itemWidgets[i]['eventdate'] : null));
				}
				
			}*/
			
			dojo.toggleClass(this.domNode, "hidden", disabled);
			this.emit("disabled", disabled);
			//this.reset()
		},
		
		/* #2571 - customized reset after a filter change so that projects are rendered and taken out of view
		 * as appropriate
		 * 
		 * OBSOLETE
		 */
		afterFilterChange:function()
		{
			this._set("disabled", false);
			dojo.toggleClass(this.domNode, "hidden", false);
			this.reset({filterChange:true});

		},
		
		// QueryResults change observer object
		queryObserver: null,
		
		
		
		constructor: function (/*Object*/args) {
			this.itemIds = {};			// used for counting items
			this.percentage = {};		// for counting percentage
			this.itemWidgets = {};		// item widgets
			this.itemDOMs = {};
			this.completedItemDOMs = {};
		},
		
		buildRendering: function() {
			this.inherited(arguments);
			this.set("collapsed", this.initialyCollapsed);
			
			this.buildDragAndDrop();
		},

		postCreate: function(){
			this.domNode.removeAttribute('title');
		},
		
		buildDragAndDrop: function () {
			// summary:
			//		Attach drag and drop
			
			this.dragSource = new dojo.dnd.HiDragSource(this.listNode, {});
		},
		
		uninitialize: function () {
			// summary:
			//		Destructor
			/*
			if (this.container && this.container.title=='000123') console.log('KILL');
			*/
			
			if (this.queryObserver)
			{

				this.queryObserver.cancel();
				this.queryObserver = null;
			}
			this.set('query',null);
			if (this.dragSource) {
				this.dragSource.destroy();
			}
			
			/*
			var itemWidgets = this.itemWidgets;
			for(var key in itemWidgets) {
				itemWidgets[key].destroy();
			}
			*/
			/* Bug #2112 itemWidgets needs to remain an object */
			this.set('disabled',true);
			
			dojo.query('.task_li',this.listNode).orphan();
			this.itemWidgets = /*null*/{};
			this.itemDOMs = {};
			this.itemIDs = {};
			this.completedItemDOMs = {};
			this.inherited(arguments);
		},
		
		mainList:null,
		queryCount:0,
		queryCounter:1,
		watchChanges: function (/*Object*/object, /*Number*/removed, /*Number*/inserted, /*Object*/diff, skipProjectsAndAll) {
			// summary:
			//		Observe QueryResult changes and react on them
			// object:
			//		Item data which changed
			// removed:
			//		Item index from which item was removed
			// inserted:
			//		Item index where item was added to
			// diff:
			//		If item changed holds previous values
			//
			// Modifications:
			//  #3184 - calculate instance_date for recurring items when doing add so the corresponding DOM element 
			//          has the correct eventdate attribute set
			
			// #2509
			if (this.disabled) return;
			/* #2509 - This runs once - during page load to ensure that items are never added incrementally to the DOM */
			if (!this.initialized) {
				if (this.queryCounter == 1) {
					this.queryCount = this.readStore.query().length;
					initializers[this.id] = 1;
				}
				if (this.queryCounter >= this.queryCount) this.set('initialized',true);
				this.queryCounter++;
			}
			/* Versions previous to 10 allowed items to reference themselves as parent - this will cause browser lockup in versions >=10 */
			if (object.parent == object.id) object.parent = 0;
			if (object.recurring && inserted != -1 /*&& ((diff && diff.instances) || (diff && diff.recurring_interval) || removed == -1) */) {		
				// If recurring item "instances" changed or recurring item was added
				// We also need to check if recurrence_interval changed
				// We also need to check if start_date or end_date changed since that may affect date displayed
				// And check recurring_end_date
				if (diff && ('recurring_interval' in diff || 'instances' in diff || 'start_date' in diff || 'end_date' in diff ||
					'recurring' in diff || 'recurring_end_date' in diff ||
					(object.recurring && 'completed' in diff) || (object.recurring && 'starred' in diff)  ) || removed == -1
				) {
					if (this.watchChangesRecurring(object, removed, inserted, diff)) { 
						//Changes were made
						return;
					}
				}
			}
			// #4389: redraw attachments list if parent currently is expanded.
			if (diff && 'previews' in diff && object && object.parent && object.parent == hi.items.manager.openedId()) {
				FileList.forceRedrawOpened();
			}
			/* #2509
			var inList = this.has(object.id);
			*/
			var inList = this._has(object.id);
			
			if (removed != -1 && inserted != -1) {
				//CHANGED
				var matches = this.filterMatches(object) && this.filterFilters(object);
				if (matches && !inList) {
					//Previously item didn't matched, now it matches
					/*
					 * #2578
					 * If this new item already has children in this list, they need to be removed
					 * and added back into the hierarchical structure of this parent
					 */
					if (this['_flatten_'+this.filterType]) {
						var children = hi.items.manager.get({parent:object.id});
						for (var i = 0; i < children.length; i++) {
							if (this.filterMatches(children[i])) this.order(children[i]);
						}
					}
					this.add(object, inserted,object.recurring ? this.calculateInstanceDate(object) : null, this.listName === 'tooltip');
				} else if (!matches && inList) {
					//Previously item matched, now it doesn't
					/* 
					 * #2578
					 * may need to flatten out dated children
					 */
					this.remove(object, inserted, diff);
					if (this['_flatten_'+this.filterType]) {
						var children = hi.items.manager.get({parent:object.id});
						for (var i = 0;i < children.length; i++){
							if (this.filterMatches(children[i])) this.order(children[i]);
						}
					}
					
				} else if (inList || this.isFirstLevelSubtask(object)) {
					if (diff && "parent" in diff) {
						//Parent changed, remove from previous position and add to new one
						this.remove({"id": object.id, "parent": diff.parent});
						this.add(object, inserted,object.recurring ? this.calculateInstanceDate(object) : null, this.listName === 'tooltip');
					} else if (diff && 'recurring' in diff) {
						/*
						 * #3312
						 */
						this.order(object);
					} else {
						if (diff) {
							this.changed(object, diff);
							/* Force an order if sortable attribute changed and that attribute matches the current sort choice */
							for (var d in diff)
							{
								if (hi.store.Project.attributeRequiresSort(d))
								{
									this.order(object);
									break;
								}
							}
							/* 
							 *  #2578
							 */
							if (this['_flatten_'+this.filterType] )
							{
								var children = hi.items.manager.get({parent:object.id});
								var nChildren=0;
								for (var i = 0;i<children.length;i++)
								{
									if (children[i].id>0) nChildren++;
								}
								for (var i = 0;i<children.length;i++){
									if (/*children[i].id>0 && */!diff.children && this.filterMatches(children[i])) this.order(children[i]);
								}
								/*
								 * Parent might be in the list, but the parent and child (object) may need to be flattened;
								 * This is handled in add()
								 */
								var parent = object.parent||null;
								if (parent /*&& this['_flatten_'+this.filterType](hi.items.manager.get(parent),object)*/) this.order(object);
							}
						}
						
						if (typeof diff == 'object')
						{
							if (/*removed != inserted ||*/ (("completed" in diff || "instances" in diff) && this.completedSubList)) {
								//ORDER CHANGED
								this.order(object, removed, inserted);
							} else if ('start_date' in diff || 'end_date' in diff || 'message' in diff || 'time_track' in diff || 'previews' in diff || 'participants' in diff) {
								// #3576 and #3607 and #3613 and #5171:
								this.order(object);
							}
						}
					}
				} 
			} else if (removed == -1 && inserted != -1) {
				//ADDED

				if (!inList && this.filterMatches(object) && this.filterFilters(object)) {
					if (object.id && object.id < 0) {
						this.add(object,0,object.recurring ? this.calculateInstanceDate(object) : null, this.listName === 'tooltip');
					} else {
						// #5026
						if (object && object.id > 0 && object.parent) {
							item_DOM.currentlyHighlighting[object.id] = true;
						}
						this.add(object, inserted, object.recurring ? this.calculateInstanceDate(object) : null, this.listName === 'tooltip');
					}
				}
				
			} else if (removed != -1 && inserted == -1) {
				//REMOVED
				if (inList) {
					this.remove(object, removed);
				}
			} else {
				//ORDER CHANGED
				this.order(object, removed, inserted);
			}
		},
		
		watchChangesRecurring: function (/*Object*/object, /*Number*/removed, /*Number*/inserted, /*Object*/diff) {
			// summary:
			//		Observe QueryResult changes and react on them
			// object:
			//		Item data which changed
			// removed:
			//		Item index from which item was removed
			// inserted:
			//		Item index where item was added to
			// diff:
			//		If item changed holds previous values
			/* #2509
			var inList = this.has(object.id),
			*/
			var inList = this._has(object.id),
				instances = object.instances /* instances isn't necesarily defined yet? */|| [],
				instance = null,
				i = 0,
				
				inListInstance = null,
				temp = null,
				matches = false,
				
				done = false; // changes has been done, don't look at other instances
				/* #2509
				inListInstance = this.has(object.id*, false,instance.start_date*);
				*/
				inListInstance = this._has(object.id);

				matches = this.filterMatches(object) && this.filterFilters(object);
				
				if (matches && !inListInstance) {
					//Previously there wasn't item with this instance date
					
					if (inList) {
						//Item which matched not correct anymore
						this.remove(object, removed);
						inList = false;
					}
					this.add(object, inserted,this.calculateInstanceDate(object)/*_getCurrentEventDateAttr(object.id,object.start_date)*/);
					done = true;
				} else if (!matches && inListInstance) {
					//Previously there was an item, but now it's not valid anymore
					this.remove(object, removed);
					inList = false;
					done = true;
				} else if (matches && inListInstance) {
					//Previously was and still is
					
					/* If click complete, check which instance is being observed;
					 * If it is not the same as the item already on this list, get out of here
					 */
					if (diff && 'completed' in diff)
					{
						var instance_date = time2str(new Date(object.instance_date_time),'y-m-d');
						var current_instance_date = this.calculateInstanceDate(object);
						if (instance_date != current_instance_date ) return true;
					}
					
					/*
					 * Changing recurrence interval may require removing the current object
					 * and replacing it with one with a new instance_date
					 */
					if (diff && ('recurring_interval' in diff || 'start_date' in diff || 'recurring' in diff) ) {
						/*
						 * #3252 - If this list is collapsed, we will not have access to its items;
						 * and we don't need to observe anyway
						 */
						if (this.collapsed) return true;
						/*
						 * Also, if the item is a child and has not yet been rendered, there is nothing to do
						 */
						var instance_date;
						if (object.parent) {
							var childWidget = getChildWidget(object.id,this);
							
							if (!childWidget) return;
							if (!childWidget.taskItem || !childWidget.taskItem.eventdate) return;
							instance_date = childWidget.taskItem.eventdate||null;
						} else {
							instance_date = this.itemWidgets[object.id].taskItem.eventdate;//time2str(new Date(this.itemWidgets[object.id].taskItem.eventdate),'y-m-d');
						}
						//if (!this.itemWidgets[object.id]) return;
						var current_instance_date = this.calculateInstanceDate(object);
						// when instance date not changed, we still need to 
						// update the item since the item_DOM.date_title would change.
						// if (instance_date != current_instance_date) {
						this.remove(/*{id:object.id}*/object);
						this.add(object,inserted,current_instance_date);
						return true;
						// }
					}
					
					if (diff && "parent" in diff) {
						//Parent changed, remove from previous position and add to new one
						this.remove({"id": object.id, "parent": diff.parent});
						this.add(object, inserted,/*_getCurrentEventDateAttr(object.id,object.start_date)*/this.calculateInstanceDate(object));
					} else {
						if (diff) {
							this.changed(object, diff,this.calculateInstanceDate(object));
							/* Force an order if sortable attribute changed and that attribute matches the current sort choice */
							for (var d in diff)
							{
								if (hi.store.Project.attributeRequiresSort(d))
								{
									this.order(object);
									break;
								}
							}
						}
						
						if (removed != inserted /*|| 'recurring_interval' in diff || 'recurring_end_date' in diff || 'start_date' in diff || */||(("completed" in diff || "instances" in diff) && this.completedSubList)) {
							//ORDER CHANGED
							this.order(object,removed, inserted);
						}

					}
				}	
				done = true;
				return true;
		},

		/* ----------------------------- API ------------------------------ */
		remove: function (/*Object*/item, /*Number*/index, /*Object*/diff) {
			// summary:
			//		Remove item from the list
			// item:
			//		Item which to remove
			// index:
			//		Index from where item was removed
			// diff:
			// 		previous item values
			
			//if (this.disabled) return;
			var widget = null,
				itemCache = hi.items.manager.get(item.id);
			
			if (item.id in this.itemIds) {
				delete(this.itemIds[item.id]);
				delete(this.percentage[item.id]);
				//Update count
				/*
				 * #3305, #3167 - to make sure we don't double count, check if this item has been rendered before counting
				 */
				if (this.itemWidgets[item.id]) {
					if (item.id > 0) {
						this.itemCount--;
					}
					this.itemCount -= this.updateItemCount(item.id);
				}else{
					if(!itemCache){
						this.itemCount--;
					}
				}
				//Update item count
				this.updateContainerItemCount();
				this.updateContainerPercentage();
			}
		
			if (this.itemWidgets[item.id]) {
				//widget.destroy();
				if (this.rendered) {
					// Bug #5366 - set opened to false before remove the dom node.
					hi.items.manager.opened(item.id, false);
					var itemNode = dojo.byId('itemNode_' + this.itemWidgets[item.id].widgetId);
					if (itemNode) {
						this.listNode.removeChild(itemNode);
					}
				}
				/*
				 * #3362 - we cannot delete item here, otherwise it might not be highlighted
				 hi.items.manager.removeItem(item.id,this.itemWidgets[item.id].taskItem.eventdate);
				*/
				/*
				 * #3333 - tear down drag source properly; this should remove references to DOM nodes and event listeners
				 */
				hi.items.manager._tearDownItem(this.itemWidgets[item.id]);

				delete(this.itemWidgets[item.id]);
				/* #2418 */this.updateContainerItemCount();
				// Update completed item list
				this.updateCompletedSubList();
				delete(this.itemDOMs[item.id]);
			} else {
				/* #2368 - on DnD action of moving sub-item out of task
				 * , the item's parent has already changed, so use the one in diff 
				 */
				/* TODO: use item_DOM.remove */
				var parent = null;
				if (item.parent) parent = this.has(item.parent);
				if (!parent) {
					if (diff && typeof diff === 'object' && 'parent' in diff) {
						parent = this.has(diff['parent']);
					}
				}
				if (parent) {
					item_DOM.remove(parent,item, index, diff);
			}
				}
		},

		_getItemCount: function() {
			var count = 0,
				k, item, parent;

			for (k in this.itemIds) {
				item = hi.items.manager.get(k);

				if (item) {
					parent = hi.items.manager.get(item.parent);
					
					if (item.parent && parent && (!this.itemIds[item.parent] && parent.category !== hi.widget.TaskItem.CATEGORY_PROJECT)) continue;

					if (!item.parent || (item.parent && !this.has(item.parent)) || (parent && parent.category == hi.widget.TaskItem.CATEGORY_PROJECT)) {
						count++;
						count += this.updateItemCount(item.id);
					}
				}
			}

			return count;
		},
		
		/* #2189 
		 * 
		 * Compare widget to be added to TaskList to specified widget in TaskList using 
		 * the sortOptions array, which is the output from hi.store.Project.getOrderConfiguration()
		 * 
		 */
		_doCompare: function(toAddWidget, addAt, sortOptions, index, debug) {
			//if (!addAt) console.log('NO ADDAT',[this.id,toAddWidget,addAt,index]);
			var addAtItem = this.itemWidgets[/*addAt.taskId*/dojo.dom.getTaskId(addAt)].taskItem,
				toAddItem = toAddWidget.taskItem,//this.itemWidgets[/*toAddWidget.taskId*/dojo.dom.getTaskId(toAddWidget)],
				SOLength = sortOptions.length,
				addAtValue,
				toAddValue;
			//if (!addAtItem) console.log('ADDAT ERROR',[this.id,addAt,addAt.taskId,toAddItem]);
			for (var i = 0;i<SOLength;i++)
			{
				addAtValue = addAtItem[sortOptions[i].attribute];
				toAddValue = toAddItem[sortOptions[i].attribute];
				if (sortOptions[i].caseInsensitive)
				{
					addAtValue = addAtValue.toUpperCase();
					toAddValue = toAddValue.toUpperCase();
				}
				
				if (addAtValue != toAddValue)
/* End of conflicting change >>>>>>> feature/taskitem */
				{
					addAtValue = addAtItem[sortOptions[i].attribute];
					toAddValue = toAddItem[sortOptions[i].attribute];
					
					if (sortOptions[i].caseInsensitive)
					{
						addAtValue = addAtValue.toUpperCase();
						toAddValue = toAddValue.toUpperCase();
					}
					
					if (addAtValue != toAddValue)
					{
						if (!addAtValue) return false;
						if (!toAddValue) return true;
						if (sortOptions[i].descending)
						{
							if (toAddValue < addAtValue) return true;
							else return false;
						} else
						{
							if (toAddValue > addAtValue) return true;
							else return false;
						}
					}
				}
			}
			
			return true;
		},
		
		/*
		 * Determine the location in a TaskList where completed tasks can be added
		 */
		_getCompletedSubListIndex:function()
		{
			if (!this.completedSubList) return null;
			/* #2965 var node = dojo.query('.task_completed',this.listNode)[0], */
			var node = dojo.query('.task_empty',this.listNode)[0],
			collection = this.listNode.children,
			length = collection.length;
		
			for (var i = 0;i<length;i++)
			{
				if (collection[i] == node)
				{
					return i;
				}
			}
			return null;
		},
		
		/*
		 * Determine the location in a TaskList of the node li.task_hidden
		 */
		_getCompletedSubListHiddenIndex:function()
		{
			if (!this.completedSubList) return null;
			var node = dojo.query('.task_hidden',this.listNode)[0],
			collection = this.listNode.children,
			length = collection.length;
		
			for (var i = 0;i<length;i++)
			{
				if (collection[i] == node)
				{
					return i;
				}
			}
			return null;
		},

		_getChildrenNodes:function()
		{
			var c = [];
			var nodes = this.listNode.children;
			for (var i = 0;i<nodes.length;i++)
			{
				if (dojo.hasClass(nodes[i],'task_li')) c.push(nodes[i]);
			}
			return c;
		},
		
		/*
		 * Bug #2189
		 * replaces some of the functionality of the add() method.  In particular, this method
		 * attempts to add a widget in the correct location according to the sort option specified
		 */
		_add: function(/* element in this.itemWidgets */widget,referenceDomNode) {
			var item = hi.items.manager.get(widget.taskId),
				//parentItem = widget.get('container'),
				isCompleted = hi.items.manager.getIsCompleted(item, widget.taskItem.eventdate || null),
				completedSubList = this.completedSubList;//&& parentItem === this;
			
			/* Sort item against items placed after completed sublist node */
			if (completedSubList && isCompleted) {
				this._addToCompletedSubList(widget);
				return;
			} 
			
			var sortAttribute = 'title',
				sortOrder = 'Ascending',
				insertAt = 0,
				sortConfiguration = hi.store.Project.getOrderConfiguration(true)[0],
				sortOptions = hi.store.Project.getOrderConfiguration(true),
				//childrenNodes = dojo.query('.task_li',parentItem.listNode),
				//childrenNodes = dojo.query('ul#'+this.id+'>li.task_li'),
				/* #2311 
				 *  This finds top-level taskItems, i.e., does not count sub-items, which screws up the sort 
				 */
				/* PERFORMANCE childrenNodes = dijit.findWidgets(parentItem.listNode),*/
				childrenNodes = this._getChildrenNodes(),
				end = null,
				nChildren = childrenNodes.length;
				
			sortAttribute = sortConfiguration['attribute'];
			if (this.completedSubList) end = this._getCompletedSubListIndex();
			else end = childrenNodes.length;
			if (sortConfiguration['descending']) sortOrder = 'Descending';

			while(insertAt<end) 
			{
				if (this._doCompare(widget,childrenNodes[insertAt],sortOptions,insertAt)) {
					insertAt++;
				}
				else break;
			}
			dojo.place(this.itemDOMs[widget.taskId],this.listNode,insertAt);
		},
		
		/*
		 * Does the same thing as the _add() method, except for completed items that belong in a completedSublist
		 */
		_addToCompletedSubList: function(widget) {
			var sortAttribute = 'title',
				sortOrder = 'Ascending',
				insertAt = 0,
				index = 0, node,
				nCompleted = this._getCompletedItemCount(this.itemWidgets),
				sortConfiguration = hi.store.Project.getOrderConfiguration(true)[0],
				sortOptions = hi.store.Project.getOrderConfiguration(true),
				childrenNodes = this.listNode.childNodes,
				nChildren = childrenNodes.length,
				sortAttribute = sortConfiguration['attribute'],
				item = him.get(widget.taskId);


			// if (!item || item.recurring) return false;

			if (sortConfiguration['descending']) sortOrder = 'Descending';
			
			/* The first one always goes in at the end */
			if (nCompleted == 1) {
				node = dojo.place(this.completedItemDOMs[widget.taskId], this.listNode);
				// if (item && item.recurring) {
				// 	dojo.addClass(node, 'hidden');
				// }
				return;
			} 
			
			insertAt = this._getCompletedSubListHiddenIndex()+1;
			var begin = insertAt;
			if (insertAt == -1) insertAt = 0;
			while (insertAt < nChildren) {
				if (dojo.hasClass(childrenNodes[insertAt],'task_li') && !this._doCompare(widget,childrenNodes[insertAt],sortOptions,insertAt)){
					break;
				}
				insertAt++;
			}
			
			var node = dojo.place(this.completedItemDOMs[widget.taskId], this.listNode, insertAt);
			// console.log(node);
			// if (item && item.recurring) {
			// 	dojo.addClass(node, 'hidden');
			// }
		},
		
		itemDOMs:null,
		completedItemDOMs:null,
		rendered:null,
		countPercentage: function(/*Object*/item) {
			return hi.items.manager.getIsCompleted(item) ? 1 : (item.time_track && item.time_spent ? Math.min(1, item.time_spent / item.time_est) : 0);
		},
		
		add: function (/*Object*/item, /*Number*/index, /*String*/instance_id,/* #2556 */inTaskEditTooltip) {
			// summary:
			//		Add item to the list
			// item:
			//		Item to add
			// index:
			//		Index where to insert item
			
			//if (this.disabled) return;
			var parent;

			if (item.category == hi.widget.TaskItem.CATEGORY_FILE && item.parent) {
				parent = hi.items.manager.get(item.parent);
				/* #2341 - check if parent exists; it might not if the parent is a project and we are not in the project tab */
				if (parent && parent.category) {
					var taskItem = this.has(item.parent);
					if (taskItem) {
						//Inform task -> file list that there is new item
						item_DOM.add(taskItem,item);
					}
					return; // File attached to item, don't add to this list
				}
			}
			
			if (!(item.id in this.itemIds)) {
				if (this.parentId && this.container) {
					if (item.parent == this.parentId) {
						this.percentage[item.id] = this.countPercentage(item);
					}
				}
				
				this.itemIds[item.id] = true;
				
				// #2733 update counter only if it is already persisted item with assigned ID.
				if (item.id > 0) {
					// Update count
					this.itemCount++;
					
					// Update container item count
					this.updateContainerItemCount();
					this.updateContainerPercentage();
				}
			}
			
			// Item is collapsed children isn't rendered
			if (this.collapsed) {
				/*
				if (!this.disabled && this.rendered)
				{
					this.itemCount+=this.updateItemCount(item.id);
					this.updateContainerItemCount();					
				}
				*/
				return;
			}
			
			// If child of one of the items don't add to the list, but add to the child instead
			if (item.parent && /* #2556 */!inTaskEditTooltip) {
				var readStore = this.readStore;
				parent = this.has(item.parent);
				
				if (parent) {
					// Item found, add as child
					/*
					 * #2578
					 * check if this sub item is to be flattened out
					 */
					if (this['_flatten_'+this.filterType])
					{
						if (!this['_flatten_'+this.filterType](hi.items.manager.get(parent.taskId),item)) return item_DOM.add(parent,item,instance_id);
					} else 
					{
						return item_DOM.add(parent,item,instance_id);
					}
				} else {
					// Item not found, but maybe it just isn't created yet
					parent = readStore.get(item.parent);
					/* #2341 - parent might not exist with respect to this user
					 * i.e., maybe it's deleted, maybe the user does not have permission to see it, etc.
					 */
					/*
					 * #2578
					 * check if this sub item is to be flattened out
					 */
					if (parent)
					{
						var flatten = false;
						if (this['_flatten_'+this.filterType])
						{
							if (this['_flatten_'+this.filterType](parent,item)) flatten = true;
						} 

						if (!flatten)
						{
							if (this.filterMatches(parent) && this.filterFilters(parent)) {
								return;
							} else {
								/*
								 * #2040 - check if parent is a project, if so, go ahead and instantiate item, otherwise
								 * get out of here
								 * 
								 */
								/* #525 */
								var tag = Hi_Preferences.get('tagfilter', '');
								
								/* #2414 */
								var searchMatches = this.filterSearch(item);
								
								if (parent.category != hi.widget.TaskItem.CATEGORY_PROJECT && !tag && !searchMatches) {
									delete this.itemIds[item.id];
									/* #2418 - update counts */
									// Update count
									this.itemCount--;
									
									// Update container item count
									this.updateContainerItemCount();
									this.updateContainerPercentage();
									return;
								}

							}
						}
						
					}

				}
			}
			
			// Create TaskItem
			var data = dojo.mixin({}, item);
			if (data.recurring != 0) {
				data.completed = hi.items.manager.getIsCompleted(data,instance_id);
			}

			/* #2509 - only invoke getChildrenData if children is set, otherwise, we incur
			 * the expense of a query calculation that will always result in an empty set for
			 * items with no children
			 */
			/*if ((item.children && item.children > 0) || (this.rendered && !this.disabled))*/
			item.children = this.getChildrenData(item.id).length;	//Bug #5292
			var precursor = {
				//widgetId:widgetId,
				eventdate:instance_id||null,
				//completed:data.completed,
				taskId:item.id/*,
				list:this,
				listName:this.listName
				*/
			};
			if (item.colorClass) delete item.colorClass;
			if (!item.color) item.color = 0;
			var domItem = item_DOM.createDOMItem(this,dojo.mixin(precursor,item));
			
			if (this.completedSubList && data.completed) {
				this.completedItemDOMs[item.id] = lang.replace(item_DOM.template, domItem);
			} else {
				this.itemDOMs[item.id] = lang.replace(item_DOM.template,domItem);
			}
			var taskItemMixin = {
				opened:0,
				expanded:0,
				widgetId:domItem.widgetId,
				colorClass:domItem['color_class'] || '',
				childrenClass:domItem['children_class'] || ''
			};
			if (instance_id)
			{
				taskItemMixin['eventdate'] = instance_id;
			}
			var tempList = null;
			if(item.list) {
				tempList = item.list;
				delete item.list;
			}
			var widget = this.itemWidgets[item.id] = dojo.mixin({
				childrenRendered : false,
				itemIDs : {},
				itemWidgets : {},
				itemDOMs : {},
				widgetId : domItem.widgetId,
				list : domItem.list,
				taskId : item.id
			}, {
				taskItem : dojo.mixin(dojo.clone(item), taskItemMixin)
			});
			if(tempList) {
				item.list = tempList;
				tempList = null;
			}
			//this.itemWidgets[item.id].taskItem.widgetId = this.itemWidgets[item.id].widgetId;
			//We only try to add individual items to the DOM when the list is already rendered
			if (this.rendered) 
			{
				this.itemWidgets[item.id].taskItem.widgetId = this.itemWidgets[item.id].widgetId;
				if (!this.get('disabled'))
				{
					var itemName;
					if (inTaskEditTooltip) itemName = 'tooltip';
					else if (instance_id) itemName = instance_id;
					else itemName = 'list';
					if (!inTaskEditTooltip) hi.items.manager.removeItem(item.id,instance_id);
					hi.items.manager.addItem(item.id,this.itemWidgets[item.id],itemName);
					if (!inTaskEditTooltip) item_DOM.highlight(this.itemWidgets[item.id]);
				}
				if (this.completedSubList && data.completed) {
					this._add(this.itemWidgets[item.id]);//dojo.place(this.completedItemDOMs[item.id],dojo.query('.task_hidden',this.listNode)[0],'after')
				}
				else {
					this._add(this.itemWidgets[item.id]);//dojo.place(this.itemDOMs[item.id],this.listNode,'first');
				}
				item_DOM.checkOverdue(this.itemWidgets[item.id]);
				if (!inTaskEditTooltip) item_DOM.highlight(this.itemWidgets[item.id]);
				this.itemCount += this.updateItemCount(item.id);
				this.updateContainerItemCount();
				this.itemWidgets[item.id].dragSource = new dojo.dnd.HiDragSource(dojo.byId('children_'+this.itemWidgets[item.id].widgetId), {});
				// Sync drag and drop
				if (this.dragSource) {
					this.dragSource.sync();
				}
				if (this.container && this.container.dragSource) {
					this.container.dragSource.sync();
				}
				this.updateCompletedSubList();
			}

			/* #2418 *//* #2509 - move to setDisabledAttr() this.updateContainerItemCount();*/
			/*
			// Sync drag and drop
			if (this.dragSource) {
				this.dragSource.sync();
			}
			if (this.container && this.container.dragSource) {
				this.container.dragSource.sync();
			}
			
			// Update completed item list
			this.updateCompletedSubList();
			*/
			return this.itemWidgets[item.id];
		},
		
		addNew: function (properties, parentTaskItem) {
			// summary:
			//		Add new item form to the list
			// properties: Object
			//		New item properties
			// parentTaskItem: Object
			//		Optional, parent task item
			//		Needed to correctly find newly created item
			
			/* Bug #1945 - A negative ID implies a new item to edit is already opened*/
			/* Bug #1869 - we also need to check in all projects and check for opened items*/
			if (hi.items.manager.openedId() && hi.items.manager.openedId() < 0) return false;
			// hi.items.manager.collapseOpenedItems(true,true);

			properties = properties || {};
			// If this is a new item, can't add another one under it - Bug #1945 - do not know if this condition is ever met
			if (properties.parent && properties.parent < 0) return false;
			
			var parent_data = properties.parent ? hi.items.manager.get(properties.parent) : null;
			/*
			 * #3305
			 * use newItemParentId to determine whether or not this item is initially completed, if, for example,
			 * it is corrected as a sub item in a completed list
			 */
			var parentTaskId = parentTaskItem.task && parentTaskItem.task.taskId?parentTaskItem.task.taskId : null;
			var newItemParentId = properties.parent || parentTaskId || null;
			var data = dojo.mixin({}, {
				"id": --hi.widget.TaskItem.ID_COUNTER,
				"parent": this.parentId || 0,
				completed:newItemParentId&&hi.items.manager.getIsCompleted(newItemParentId,this.calculateInstanceDate(newItemParentId))?1:0,
				"time_last_update": time2strAPI(new Date(0)), // start of the epoch
				"time_create": time2strAPI(new Date(0)), // start of the epoch
				
				"starred": false,
				"title": "",
				"message": "",
				"participants": [],
				"tags": [],
				"category": hi.widget.TaskItem.CATEGORY_TASK,
				"color": 0,
				//"completed": false,
				"reminder_time": 0,
				"reminder_time_type": "m",
				"reminder_enabled": false,
				"time_track": Hi_Preferences.get('enable_time_tracking', false, 'bool'),
				"time_est": 0,
				"recurring": 0,
				'recurring_interval':1,
				"priority": 20000,
				"is_all_day": true,
				"assignee": 0,
				"shared": Hi_Preferences.get('default_shared', false, 'bool'),
				"my": true,
				"user_id": Project.user_id
			}, properties);

			// start of Feature #5503
			if (data.parent) {
				var parent = hi.items.manager.get(data.parent);

				if (parent && (parent.category === hi.widget.TaskItem.CATEGORY_PROJECT)) {
					if (parent.color_value && parent.color_value !== CONSTANTS.defaultColorValue) {
						// ['#fc2f6a', '#fd9426', '#fecb2e', '#55CE2E', '#cb77df', '#a18460']
						switch (parent.color_value.toLowerCase()) {
							case '#fc2f6a':
								data.color = 1;
								break;
							case '#fd9426':
								data.color = 2;
								break;
							case '#fecb2e':
								data.color = 3;
								break;
							case '#55ce2e':
								data.color = 4;
								break;
							case '#cb77df':
								data.color = 6;
								break;
							case '#a18460':
								data.color = 7;
								break;
						}
					}
				} else {
					if (parent.color) data.color = parent.color;
				}
			}
			// end of Feature #5503

			hi.items.manager.set(data);
			
			// Collapse parent item and render children
			if (data.parent) {
				/* 
				 * #3362 - pull item by instance if it is recurring
				 */
				// var name = this.listName === 'tooltip' ? 'tooltip' : null;
				var name = null;
				var parent_item = hi.items.manager.item(data.parent, parentTaskItem.task ? (parentTaskItem.task.eventdate || null) : name);

				if (parent_item) {
					item_DOM.collapse(parent_item);
					item_DOM.expandChildren(parent_item, parentTaskItem.task.eventdate);
				}
			}
			var itterations = 50; // 3 seconds
			var expandItem = function() {
				var item = parentTaskItem && parentTaskItem.has ? parentTaskItem.has(data.id, true) : hi.items.manager.item(data.id);
				if (item) {
					item_DOM.expand(item);
					item_DOM.openEdit(item);
				} else {
					itterations--;
					if (itterations) {
						// #3608:
						setTimeout(function() {expandItem();}, dojo.isSafari && dojo.isSafari >= 6.1 ? 1 : 60);
					} else {
						//Something went terribly wrong, remove item
						hi.items.manager.remove(data.id);
					}
				}
			};
			expandItem();
			
			/* Bug #1862,#1869 
			// Expand item
			
			(function expandItem () {
				var item = parentTaskItem && parentTaskItem.has ? parentTaskItem.has(data.id, true) : hi.items.manager.item(data.id);
				if (item) {
					item.expand();
					item.openEdit();
				} else {
					itterations--;
					if (itterations) {
						setTimeout(expandItem, 60);
					} else {
						//Something went terribly wrong, remove item
						hi.items.manager.remove(data.id);
					}
				}
			})();
			*/
		},
		
		/* Bug #2189 - ordering is implicit in the .add() method now */
		/* #2823 - add instance date to add invocation for recurring items to maintain correct date on dateTitle node */
		order: function(item) 
		{
			var editingId = hi.items.manager.editingId();
			if (editingId && item && item.id && item_DOM.hasChildrenOpenForEdit(item.id, null, true)) {
				// #5862
				//console.debug('skip order for %s because editing %s', item.id, editingId);
			} else {
				var instance_id;
				if (item.recurring) instance_id = this.calculateInstanceDate(item);/*getCurrentEventDateAttr(item.id,item.start_date);*/
				this.remove(item);
				this.add(item,null,instance_id);
			}
		},
		
		/* Maybe should be removed */
		_order: function (/*Object*/item) {
			// summary:
			//		 Move item into correct position
			// item:
			//		Item to move
			// from:
			//		Old index
			// to:
			//		New index
			
			var taskItem = this.has(item.id);
			
			if (taskItem) {
				var parentItem = taskItem.container,
					items = this.readStore.query(),
					i = 0,
					ii = items.length,
					reference = null,
					place = "after",
					completedSubList = this.completedSubList && parentItem === this, // valid only direct child or list
					completed = null; 
				
				if (completedSubList) {
					//Needed only if there is a sub list
					completed = hi.items.manager.getIsCompleted(item);
				}
				
				//Find position by searching sibling data
				for (; i<ii; i++) {
					if (items[i].id == item.id) {
						if (!reference) {
							place = "before";
						} else {
							break;
						}
					} else if (parentItem.has(items[i].id, true)) {
						if (!completedSubList || (hi.items.manager.getIsCompleted(items[i]) == completed)) {
							reference = items[i];
							if (place == "before") break;
						}
					}
				}
				
				if (reference) {
					//Before / after
					dojo.place(taskItem.domNode, parentItem.itemWidgets[reference.id].domNode, place);
				} else {
					//Inside
					if (completedSubList && !completed) {
						//Place at the end of the list if completed or before "Completed items:" label
						dojo.place(taskItem.domNode, this.completedLabelNode, "before");
					} else {
						//Place at the end of the list
						dojo.place(taskItem.domNode, parentItem.childrenNode || parentItem.domNode);
					}
				}
			}
		},
		
		isFirstLevelSubtask: function(item) {
			var first = false;
			if (item && item.parent) {
				first = hi.items.manager.get({id: item.parent});
				first = first.length && first[0].parent && first[0].parent == this.parentId;
			}
			return first;
		},
		
		changed: function (/*Object*/item, /*Object*/diff, eventdate) {
			// summary:
			//		Handle item change
			// item:
			//		Item which changed
			// index:
			//		Previous values for properties which changed
			
			//console.log('CHANGED',[item.title,diff]);
			
			//if ('eventdate' in diff) delete diff['eventdate'];
			var taskItem = this.has(item.id),//this.itemWidgets[item.id],//this.has(item.id),
				key = null,
				didHighlight=false;
			
			if (taskItem) {
				for (key in diff) {
					//taskItem.set(key, item[key]);
					if (key in item_DOM['_propertiesHash'] && diff[key] != undefined) {
						if (this.rendered) {
							item_DOM.set(taskItem, key, item[key]);
							TaskItemProperties.set(taskItem, key, item[key]);

							/* #71 might be highlighting a sub-item, which will be in taskItem */
							if (!didHighlight) {
								item_DOM.highlight(this.itemWidgets[item.id]||taskItem,eventdate);
								didHighlight=true;
							}
						} else {
							var precursor = {
								//widgetId:widgetId,
								taskId:item.id/*,
								list:this,
								listName:this.listName
								*/
							};
							//console.log('CHANGED',[item.title,diff,key]);
							var domItem = item_DOM.createDOMItem(this,dojo.mixin(precursor,item));
							//var domItem = item_DOM.createDOMItem(this,item);
							this.itemDOMs[item.id] = lang.replace(item_DOM.template,domItem);
							
							var widget = this.itemWidgets[item.id] = dojo.mixin({childrenRendered:false,itemIDs:{},itemWidgets:{},itemDOMs:{},widgetId:domItem.widgetId,list:domItem.list,taskId:item.id},{taskItem:dojo.mixin(item,{
								//"id": null,
								//"eventdate": diff['eventdate']||null,
								opened:0,
								expanded:0,
								widgetId:domItem.widgetId
							})});
						}
					}
				}

				//If tasks completed state changed then update UI
				if ("completed" in diff && this.completedSubList) {
					this.updateCompletedSubList();
				}
			}
			
			// Sync drag and drop
			if (this.dragSource) {
				this.dragSource.sync();
			}
			if (this.container && this.container.dragSource) {
				this.container.dragSource.sync();
			}
			
			// Update percentage
			if (this.parentId && (item.parent == this.parentId || this.isFirstLevelSubtask(item)) && this.container) {
				if ('time_est' in diff || "parent" in diff || "time_track" in diff || "time_spent" in diff || "completed" in diff || "instances" in diff) {
					if (item.parent == this.parentId) this.percentage[item.id] = this.countPercentage(item);
					this.updateContainerPercentage();
				}
			}
		},
		
		/**
		 * Reset list
		 */
		reset: function (options) {
			// summary:
			//		Reset all list
			
			if (this.disabled) return;
			
			if (this.query && this.filter) {
				/* PERFORMANCE - At the moment this causes problems with sorting, tab changes, etc. 
				//Remove container from DOM for performance
				var referenceNode = null,
					place = null;
				
				if (this.domNode.parentNode) {
					referenceNode = previousElement(this.domNode);
					place = "after";
					
					if (!referenceNode) {
						referenceNode = this.domNode.parentNode;
						place = "first";
					}
					
					this.domNode.parentNode.removeChild(this.domNode);
				}
				*/
				//Remove items
				var itemWidgets = this.itemWidgets,
					key = null;
				if (!this.collapsed)
				{
					for (key in itemWidgets) {
						//itemWidgets[key].destroy();
						if (itemWidgets[key].dragSource) itemWidgets[key].dragSource.destroy();
					}
				}

				
				// Clean up drag and drop
				if (this.dragSource) {
					this.dragSource.sync();
				}
				if (this.container && this.container.dragSource) {
					this.container.dragSource.sync();
				}
				
				//Reset data
				this.itemWidgets = {};
				this.itemDOMs = {};
				this.completedItemDOMs = {};
				this.itemIds = {};
				this.percentage = {};
				this.itemCount = 0;
				dojo.query('.task_li',this.listNode).orphan();
				if (!this.disabled && (!options || options.fill !== false)) {
					// #3657: query() function uses the property 'priority' to sort the result, which is also appropriate sorting for
					//        executing the watchChanges() function, several rows below. If there is a task with attachment(s),
					//        then it is necessary to first initialize the task item itself and then to process the appropriate
					//        items with attachments.
					//Add items if not disabled and "fill" option is not false
					var data = this.readStore.query(),
						i = 0,
						ii = data.length;
					
					var length,slice,passthrough,toProcess,items;
					var self = this;
					/* #2167
					 * Split loop over items up to try to prevent browser locking up with 'unresponsive script' error'
					 */
					var skipWatchFlag = true;
					if (options && options.filterChange) skipWatchFlag = false;
//					var chunk = function(array) // this function is not used anywhere
//					{
//						items = array.concat(),
//						length = items.length,
//						slice = Math.min(length,5),
//						toProcess = items.slice(0,slice),
//						passthrough = items.slice(slice);
//						setTimeout(function() {
//							var sliceLen = toProcess.length;
//							for (var i = 0;i<sliceLen;i++)
//								self.watchChanges(items[i],-1,i,null,skipWatchFlag);
//							if (items.length>0) setTimeout(function() {chunk(passthrough)},2);
//						},2);
//					};
					
					for (; i < ii; i++) {
						this.watchChanges(data[i], -1, i, null, skipWatchFlag);
					}

					if (this.itemDOMs && this.itemCount>0) {
						this._renderWidgets();
						/* #2538 - check overdue */
						Hi_Overdue.reload();
					}
				} else {
					if (this.initialyCollapsed) {
						//Collapse list if it is allowed
						this.set("collapsed", true);
					}
				}
				/* PERFORMANCE - At the moment we are not removing container node from DOM
				//Restore in DOM
				if (referenceNode) {
					//dojo.place(this.domNode, referenceNode, place);
				}
				*/
				//Update item count
				this.updateCompletedSubList();
				this.updateContainerItemCount();
				this.updateContainerPercentage();
			}
		},
		
		_has: function(id) {
			return this.itemIds[id];
		},
		
		has: function (/*Number*/id, /*Boolean*/shallow, /*String*/instance_date) {
			// summary:
			//		Checks if item is in this list and returns item if it is
			//		or false if it isn't
			// id:
			//		Item id
			// shallow: 
			//		Check only direct children
			var item = null;

			if (id in this.itemWidgets) {
				item = this.itemWidgets[id];
				if (instance_date) {
					return instance_date == item.eventdate ? item : false;
				}
				return item;
			} else if (!shallow) {
				var items = this.itemWidgets,
					key = null;
				
				for (key in items) {
					item = item_DOM.has(items[key],id, false, instance_date);
					if (item) return item;
				}
				
			}
			return false;
		},
		
		//Place widgets on DOM after a reset event, such as a tab switch
		_renderWidgets: function() {
			var tmp = '', i,
				len = 0;
		
			for (i in this.itemDOMs) {
				this.itemWidgets[i].taskItem.widgetId = this.itemWidgets[i].widgetId;
				/*#2221 hi.items.manager.removeItem(i); */
				var propName = this.itemWidgets[i].taskItem.eventdate || this.listName || (this.itemWidgets[i].recurring ? 
								this.itemWidgets[i].eventdate : null);
				hi.items.manager.addItem(i, this.itemWidgets[i], propName);
			}
			var sorted = item_DOM._sortDOM(this.itemDOMs,this.itemWidgets);
			len = sorted.length;
			
			for (/*var i in this.itemDOMs*/i = 0; i < sorted.length;i++) {
				tmp += /*this.itemDOMs[i];*/this.itemDOMs[sorted[i]];
				//this.itemWidgets[i].taskItem.widgetId = this.itemWidgets[i].widgetId;
				
				//hi.items.manager.removeItem(i);
				//hi.items.manager.addItem(i,this.itemWidgets[i],this.listName || (this.itemWidgets[i].recurring ? this.itemWidgets[i]['eventdate'] : null));
			}
			if (tmp.length>0) dojo.place(tmp,this.listNode,'first');
			sorted = [];
			if (this.completedSubList) {
				tmp = '';
				for (i in this.completedItemDOMs) {
					//tmp+=this.completedItemDOMs[i];
					this.itemWidgets[i].taskItem.widgetId = this.itemWidgets[i].widgetId;
					/* #2221 hi.items.manager.removeItem(i); */
					hi.items.manager.addItem(i,this.itemWidgets[i],this.itemWidgets[i].taskItem.eventdate||this.listName || (this.itemWidgets[i].recurring ? this.itemWidgets[i]['eventdate'] : null));
				}
				sorted = item_DOM._sortDOM(this.completedItemDOMs,this.itemWidgets);
				len = sorted.length;
				
				for (i = 0;i<sorted.length;i++) tmp+= this.completedItemDOMs[sorted[i]];
				if (tmp.length>0) dojo.place(tmp,dojo.query('.task_hidden',this.listNode)[0],'after');
			}
			//DnD
			var self = this;
			setTimeout(function() {
				for (var i in self.itemWidgets) {
					self.itemWidgets[i].dragSource = new dojo.dnd.HiDragSource(dojo.byId('children_'+self.itemWidgets[i].widgetId), {});
				}
				if (self.dragSource) {
					self.dragSource.sync();
				}
				if (self.container && self.container.dragSource) {
					self.container.dragSource.sync();
				}
			}, 100);

		},
		
		proxy: function (/*Function*/fn) {
			// summary:
			//		Create a proxy function binded to this instance
			// fn:
			//		Function which will be binded
			
			var self = this;
			return function () {
				fn.apply(self, arguments);
			};
		},
		
		toggleCompletedList: function () {
			// summary:
			//		Toggle completed item sublist
			
			this.set("completedSubListExpanded", !this.completedSubListExpanded);
		},
		
		toggleCollapsed: function () {
			// summary:
			//		Toggle collapsed stated
			
			this.set("collapsed", false);
		},
		
		/* ----------------------------- DATA API ------------------------------ */
		/*
		 * For recurring items, calculate the first instance date that should appear in this list
		 * if a specified method is not supplied, default to calculating the instance date based on today
		 */
		calculateInstanceDate: function(id) {
			var item = typeof id === 'object' ? id : this.readStore.get(id),
				today,
				series;

			if (!item.recurring) return null;
			if (this.myCalculateInstanceDate) {
				return this.myCalculateInstanceDate(item);
			} else {
				/*
				 * #3229
				 * This branch of logic most likely should be used for ungrouped lists and lists NOT in the Date tab
				 */
				//First find out if there is an instance that starts today or later
				today = new Date();
				series = hi.items.date.generateRecurrenceSeries(item,time2str(today,'y-m-d'));
				if (!series || series.length <= 0) {
					//No instances that start today or later, so see if there is one in the past
					var start_time = str2timeAPI(item.start_date);
					if (start_time < today) series = hi.items.date.generateRecurrenceSeries(item,time2str(start_time,'y-m-d'));
				}
				if (series && series.length>0)
					return series[0];
				else
					return null;
			}
		},
		
		/*
		 * Return first available instance date of specified item, even if it does not necessarily fall through
		 * this list's filter
		 */
		calculateFirstAvailableInstanceDate: function(id) {
			var item = typeof id === 'object' ? id : this.readStore.get(id),
				initial_date,
				series,
				instance_date;
			
			if (!item.recurring) return null;
			//First check if one matches this list's criteria
			instance_date = this.calculateInstanceDate(item);
			if (instance_date) return instance_date;
			
			initial_date = time2str(str2timeAPI(item.start_date),'y-m-d');
			
			series = hi.items.date.generateRecurrenceSeries(item,initial_date);
			if (series && series.length > 0)
				return series[0];
			else
				return null;
		},
		
		/*
		 * #2578
		 * Reusable part of filterMatches that filters items based on this list's filter type
		 */
		_filterByType: function(item, type, fn, readStore) {
			var firstItem = item,
				lastItem = null,
				result;
			// we don't filter projects, since project are not rendered
			while (item && item.category) {
				if (type === "all" ) {
					result = fn.call(this, item, true);
					if (!result || result.length <= 0) {
						return false;
					}
				} else if (type === "any" /*&& fn.call(this, item, true)*/) {
					//return true;
					result = fn.call(this, item, true);
					if (result) return true;
					//return fn.call(this, item, true);
				} else if (type === "top") {
					lastItem = item;
				} 
				/*
				 * #3256
				 * This will cause more or less endless-recursion if it happens to be the case
				 * that an item references itself as its parent
				 */
				item = item.parent && item.id != item.parent ? readStore.get(item.parent) : null;
			}
			
			if (type == "top") {
				return fn.call(this, lastItem, true); // If top parent matches
			} else if (type == "all") {
				return true; // All items were checked and all matched
			} else {
				return false; // All items were checked and none matched
			}
		},
		
		filterMatches: function (id, type, inputFn) {
			// summary:
			//		Returns true if item should be in the list (item and all of its parents matches filter),
			//		otherwise returns false
			//		If item filter type is "any", then item or any ancestor must pass filter
			//		If item filter type is "all", then item and all ancestors must pass filter
			//		If item filter type is "top", then item and all ancestors must pass filter
			// id:
			//		Task id
			// type:
			//		Match type, "all" if all items must match, "any" if any of the ancestors must match
			
			type = type || this.filterType;
			
			var readStore = this.readStore,
				item = typeof id === 'object' ? id : readStore.get(id),
				lastItem = null, matched = false;
			
			var fn = this[inputFn || "filter"];
			
			if (Hi_Preferences.get('tagfilter', '')) {
				if (type == "top") type = "any";
			}
			
			if (item) {
				//If checking project then fail
				//If item is recurring and it was removed, then fail
				if (!item.category /* #3093 || hi.items.manager.getIsRemoved(item)*/) return false;
				
				if (type == "custom") return fn.call(this, item);
				
				/*
				 * #2578 
				 * use filterType = dated_X to flatten dated/recurring hierarchical items
				 */
				if (type.match(/^dated_/)){
					var parent = item.parent ? hi.items.manager.get(item.parent) : null,
						checkFlatten = parent ? this['_flatten_' + type](parent,item) : false;
						
					if (checkFlatten){
						if (!parent || parent.category != hi.widget.TaskItem.CATEGORY_PROJECT){
							return fn.call(this,item,true);
						}
					} else {
						if (parent) return this.filterMatches(parent,this.filterType,inputFn);
						return this._filterByType(item,type.split(/_/)[1],fn,readStore);
					}
				} else {
					return this._filterByType(item, type, fn, readStore);
				}
				/*
				// we don't filter projects, since project are not rendered
				while(item && item.category) {
					if (type == "all" ) {
						var result = fn.call(this, item, true);
						if (!result || result.length<=0)
							return false;
					} else if (type == "any" ) {
						//return true;
						var result = fn.call(this, item, true);
						if (result) return true;
						//return fn.call(this, item, true);
					} else if (type == "top") {
						lastItem = item;
					} else if (type.match(/^dated_/))
					{
						var parent,
							checkFlatten = true;
						
						if (!item.start_date || item.start_date == '') checkFlatten = false;
						if (item.parent && checkFlatten) 
						{
							parent = hi.items.manager.get(item.parent)||null;
							if (!parent || parent.category != hi.widget.TaskItem.CATEGORY_PROJECT)
							{
								return fn.call(this,item,true);
							}
						} else
						{
							var _type = type.split(/_/)[1];
							if (_type == 'all')
							{
								
							} else if (_type == 'any')
							{
								var result = fn.call(this, item, true);
								if (result) return true;
							} else if (_type == 'top')
							{
								
							}
						}
					}
					item = item.parent ? readStore.get(item.parent) : null;
				}
				
				if (type == "top") {
					return fn.call(this, lastItem, true); // If top parent matches
				} else if (type == "all") {
					return true; // All items were checked and all matched
				} else {
					return false; // All items were checked and none matched
				}
				*/
			}
			
			return false;
		},
		
		filterFilters: function (item) {
			// summary:
			//		Returns true if item passes filter and tag/search conditions
			
			if (item.id < 0) {
				// New item should always match
				return true;
			}
			
			// Tags
			var tag = Hi_Preferences.get('tagfilter', '');
			if (tag) {
				if (!item.tags || dojo.indexOf(item.tags, tag) === -1) {
					// tag filter set, but item doesn't have this tag
					return false;
				}
			}
			
			// Search
			if (Hi_Search && Hi_Search.isFormVisible()) {
				var regex = Hi_Search.getSearchRegEx(),
					matches = false;
				/*#2331 */if (!regex) return true;
				if (item.title.match(regex)) {
					matches = true;
				} else if (item.message && item.message.match(regex)) {
					matches = true;
				}
				
				if (!matches) {
					// search is active, but didn't matched
					return false;
				}
			}
			
			return true;
		},
		
		/* #2414 */
		filterSearch: function(item) {
			// Search
			if (Hi_Search.isFormVisible()) {
				var regex = Hi_Search.getSearchRegEx(),
					matches = false;
				/*#2331 */if (!regex) return true;
				if (item.title.match(regex)) {
					matches = true;
				} else if (item.message && item.message.match(regex)) {
					matches = true;
				}
				
				if (!matches) {
					// search is active, but didn't matched
					return false;
				}
			}
			
			return true;
		},
		
		/* NOT CURRENTLY IMPLEMENTED
		 * #3167, #2578
		 * Determine whether or not hierarchical item with dates should be flattened
		 * 
		 * returns true if sub-item should not be rendered under parent
		 * returns false if sub-item is to be 'flattened' out
		 */
		_flatten_dated: function(parentItem, childItem) {
			var parentDate = null,
				childDate = null;

			if (childItem.recurring) {
				childDate = this.calculateFirstAvailableInstanceDate(childItem);
			} else if (childItem.start_date && childItem.start_date != '') {
				childDate = time2str(str2timeAPI(childItem.start_date),'y-m-d');
			}
			if (!childDate) return false;
			if (parentItem.recurring) {
				parentDate = this.calculateInstanceDate(parentItem);
				
			}
			else if (parentItem.start_date && parentItem.start_date != '') {
				parentDate = time2str(str2timeAPI(parentItem.start_date),'y-m-d');
			}
			
			//If sub-item is dated and parent is not, flatten
			if (childDate && !parentDate) return true;

			//If parent has date and child does not, do not flatten
			if (parentDate && !childDate) {
				return false;
			}

			//If parent and child have same date, do not flatten
			if (childDate && parentDate) {
				if (childDate == parentDate) 
					return false;
				else
					return true;
			}
			
			//if (!parentDate) return false;
		},
		
		/*
		 * NOT CURRENTLY IMPLEMENTED
		 */
		flatten_dated:function(parentItem, childItem) {
			var parentDate = null,
			childDate = null;

			if (childItem.recurring) {
				childDate = this.calculateFirstAvailableInstanceDate(childItem);
			} else if (childItem.start_date && childItem.start_date != '') {
				childDate = time2str(str2timeAPI(childItem.start_date),'y-m-d');
			}
			if (!childDate) return false;
			
			while (parentItem) {
				if (parentItem.recurring) {
					parentDate = this.calculateInstanceDate(parentItem);
				}
				else if (parentItem.start_date && parentItem.start_date != '') {
					parentDate = time2str(str2timeAPI(parentItem.start_date),'y-m-d');
				}
				
				//If parent and child have same date, do not flatten
				if (parentDate) {
					if (childDate == parentDate)
						return false;
					else
						parentItem = parentItem.parent ? hi.items.manager.get(parentItem.parent) : null;
				} else
					parentItem = parentItem.parent ? hi.items.manager.get(parentItem.parent) : null;
			}
			return true;
			
		},
		
		
		/*NOT CURRENTLY IMPLEMENTED
		 * See _flatten_dated method
		 * 
		 * These methods are in principle invoked when filterType = dated_X and X is 'all', 'any', 'top', 'custom'
		 */
		_flatten_dated_all:function(p,c) {return this._flatten_dated(p, c);},
		_flatten_dated_any:function(p,c) {return this._flatten_dated(p, c);},
		_flatten_dated_top:function(p,c) {return this._flatten_dated(p, c);},
		_flatten_dated_custom:function(p,c) {return this._flatten_dated(p, c);},
		
		getChildrenData: function (id, filtered) {
			// summary:
			//		Returns all children data for given item which matches filter
			// id:
			//		Task ID
			// filtered:
			//		Filter only matches for list, default is true
			
			var readStore = this.readStore,
				items = readStore.query({"parent": id}),
				i = 0,
				ii = items.length,
				parent = hi.items.manager.get(id),
				matches = [];
			
			if (filtered === false || filtered === 0) {
				return items;
			}
			
			for (; i<ii; i++) {
				if (this.filterMatches(items[i]) && this.filterFilters(items[i])) {
					if (parent)
					{
						if (this['_flatten_'+this.filterType])
						{
							if (!this['_flatten_'+this.filterType](parent,items[i])) matches.push(items[i]);
						} else matches.push(items[i]);
					}
					else matches.push(items[i]);
				}
			}
			
			return matches;
		},
		
		
		/* ----------------------------- UI ------------------------------ */
		
		
		_getCompletedItemCount: function (items) {
			// summary:
			//		Returns number of completed items in the list
			
			// Count completed items
			var key = null,
				count = 0,
				completed = false;
			
			for (key in items) {
				if (items[key].taskItem.recurring) {
					completed = hi.items.manager.getIsCompleted(items[key].taskItem,items[key].taskItem.eventdate) ? 1 : 0;
				} else {
					completed = items[key].taskItem.completed ? 1 : 0;
				}
				
				count += completed;
				if (completed) {
					if (items[key].itemWidgets) {
						//Recursive
						count += this._getCompletedItemCount(items[key].itemWidgets);
					}
				}
			}

			return count;
		},
		
		_getPercentage: function () {
			// summary:
			//		Return percentage of completed tasks
			var self = this;
			
			/**
			 * Percentage map is not populated until any item changed, so in such case
			 * we need to calculate this map dynamically for project.
			 */
			var getPercentageMap = function() {
				// If map is defined and not empty return it.
				if (self.percentage) {
					for (var k in self.percentage) {
						var isFunc = typeof(self.percentage.hasOwnProperty) == 'function';
						if (!isFunc || (isFunc && self.percentage.hasOwnProperty(k))) {
							return self.percentage;
						}
					}
				}
				
				// Calculate map on the fly for project.
				if (self.container && self.container instanceof hi.widget.Project) {
					var projItem = hi.items.manager.get(self.parentId);
					if (projItem && projItem.category == item_DOM.CATEGORY_PROJECT) {
						var childItems = hi.items.manager.get({parent: projItem.id});
						var ret = {};
						for (var i = 0; i < childItems.length; i++) {
							ret[childItems[i].id] = self.countPercentage(childItems[i]);
						}
						
						return ret;
					}
				}
				
				return self.percentage;
			};
			
			if (this.parentId && this.container) {
				var items = getPercentageMap(),
					key = null,
					total = 0,
					count = 0,
					store = this.readStore,
					parent = null;
				
				for (key in items) {
					// #2429
					var item = hi.items.manager.get(key);
					if (item && item.time_track && parseInt(item.time_est) > 0) {
						var itemTotal = items[key];
						
						// #2669: include 1st level sub items in project progress calculation.
						if (itemTotal == 0) {
							var subItems = hi.items.manager.get({parent: item.id, time_track: true});
							var subItemsProgress = 0;
							var subItemsCount = 0;
							
							for (var j = 0; j < subItems.length; j++) {
								if (parseInt(subItems[j].time_est) > 0) {
									subItemsProgress += this.countPercentage(subItems[j]);
									subItemsCount++;
								}
							}
							subItemsProgress = subItemsCount ? subItemsProgress / subItemsCount : 0;
							itemTotal = subItemsProgress;
						}
						
						total += itemTotal;
						count++;
					}
				}
				
				var res = count ? Math.round(total / count * 100) : 0;
				return res > 100 ? 100 : res;
			}
			
			return 0;
		},
		
		// Feature #2697
		handleAddProjectItem: function() {
			if (!this.parentId) return;

			var project = dijit.registry.getEnclosingWidget(this.domNode.parentNode);
			if (project) project.handleAddItem(document.createEvent('Event'));
		},

		updateCompletedSubList: function () {
			if (!this.completedSubList) return;
			if (this.collapsed) return;
			// Count completed items
			var count = 0;//this._getCompletedItemCount(this.itemWidgets);
			for (var i in this.itemWidgets) {
				/*
				 * #3167
				 * If parent is not completed, don't count children, because the entire hierarchy will not be
				 * in the completed sublist
				 */
				if (hi.items.manager.getIsCompleted(this.itemWidgets[i].taskItem, this.itemWidgets[i].taskItem.eventdate)) {
					// if (hi.items.manager.get(i, 'recurring')) continue;
					count++;
					count += this.updateItemCount(i, true);
				}

			}
			// Update UI
			dojo.toggleClass(this.completedLabelNode, "hidden", !count);
			
			if (count) {
				var label_count = " ",
					label_title = "";
				
				if (count > 1) {
					label_count = " "+ hi.i18n.getLocalization("center.completed_items_count").replace('%s', count);
					label_title = hi.i18n.getLocalization("center.completed_items");
				} else {
					label_count = " "+ hi.i18n.getLocalization("center.completed_item_count").replace('%s', count);
					label_title = hi.i18n.getLocalization("center.completed_item");
				}
				
				setTextContent(this.completedLabelNodeCount, label_count);
				setTextContent(this.completedLabelNodeTitle, label_title);
			}
		},
		
		updateContainerPercentage: function (id) {
			// summary:
			//		Update container percentage attribute
			// id: String
			//		Optional, recheck given item
			
			if (this.container && this.container instanceof hi.widget.Project) {
				this.container.set("percentage", this._getPercentage());
			}
		},
		
		updateItemCount: function(widgetId, checkIsCompleted) {
			var children = hi.items.manager.get({parent:widgetId}) || [],
				i = 0,
				myCount;

			children = array.filter(children, function(item) {
				return item.category !== hi.widget.TaskItem.CATEGORY_FILE;
			});
			if (!children.length) return 0;

			if (!checkIsCompleted) {
				myCount = children.length;
			} else {
				myCount = 0;
				for (i = 0; i < children.length; i++) {
					if (children[i].category === hi.widget.TaskItem.CATEGORY_FILE) continue;
					if (children[i].recurring) {
						var eventdate = this.calculateInstanceDate(children[i].id) || null;
						if (eventdate) myCount += hi.items.manager.getIsCompleted(children[i],eventdate)? 1 : 0;
					} else {
						myCount += hi.items.manager.getIsCompleted(children[i])? 1 : 0;
					}
				}
			}
			for (i = 0; i < children.length; i++) {
				myCount += this.updateItemCount(children[i].id);
			}
			return myCount;
		},
		
		updateContainerItemCount: function (force_full_count) {
			// summary:
			//		Update container itemCount attribute
			if (this.container && this.container instanceof hi.widget.Group) {
				var self = this;
				var recalcProjectItems = function() {
					var proj = hi.items.manager.get(self.parentId);
					if (proj && proj.category == hi.widget.TaskItem.CATEGORY_PROJECT) {
						var count = 0;
						for (var k in self.itemIds) {
							count++;
						}
						self.itemCount = count;
					}else{
						self.itemCount = self._getItemCount();
					}
					return self.itemCount;
				};
				this.itemCount = recalcProjectItems();
				this.container.set("itemCount", this.itemCount);
			}
			
			if (this.collapsed) {
				var txt = " ";
				if (this.itemCount == 1) txt = " " + this.itemCount + " " + this.localeItemCollapsed;
				if (this.itemCount > 1) txt = " " + this.itemCount + " " + this.localeItemsCollapsed;
				if (this.itemCount != 0) {
					if (this.collapsedNode) {
						setTextContent(this.collapsedTextNode, txt);
						dojo.removeClass(this.collapsedNode, "hidden");
						// dojo.addClass(this.collapsedNode, "icon-caret-right");
						dojo.removeClass(this.iconNode, 'hidden');
					}
					if (this.noItemsNode) {
						dojo.addClass(this.noItemsNode, "hidden");
						dojo.addClass(this.noProjectItemsNode, "hidden");
						dojo.removeClass(this.noItemsNode, "icon-caret-right");
						// dojo.addClass(this.iconNode, 'hidden');
					}
				} else {
					if (this.collapsedNode) {
						dojo.addClass(this.collapsedNode,'hidden');
					}
				}
				dojo.toggleClass(this.noItemsNode, "hidden", this.itemCount || this.parentId);
				dojo.toggleClass(this.noProjectItemsNode, "hidden", this.itemCount || !this.parentId);
			} else {
				if (this.noItemsNode) {
					/* #2418 - */
					var count = 0;
					for (var i in this.itemWidgets) count++;
					dojo.toggleClass(this.noItemsNode, "hidden", count || this.parentId);
					dojo.toggleClass(this.noProjectItemsNode, "hidden", count || !this.parentId);

					dojo.toggleClass(this.noItemsNode, "icon-caret-right", count);
					dojo.toggleClass(this.noProjectItemsNode, "icon-caret-right", count);
				}
			}
		}
	});
	
	function setTextContent (node, str) {
		// summary:
		//		Helper function to escape string and set it as HTMLElement content
		// node:HTMLElement
		//		Element
		// str:String
		//		String content
		
		node.innerHTML = "";
		node.appendChild(win.doc.createTextNode(str));
	}

	return TaskList;
});
