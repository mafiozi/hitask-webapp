package controllers;

import java.util.HashMap;
import java.util.Map;

import oauth.OAuth2Client;
import oauth.OAuth2Error;
import oauth.OAuth2Response;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.Play;
import play.mvc.Router;
import play.mvc.Util;
import utils.Constants;
import utils.OAuth2Endpoints;
import api.ApiCall;
import api.ApiCallException;
import api.ApiGeneralResponse;
import beans.AuthenticateResponse;

public class OAuth extends ApplicationSecured {
	public enum StateType {
		LOGIN,
		SIGNUP,
		SIGNIN,
		LINK,
		SYNC
	}
	
	public static void socialAuth(String state, boolean forceApprovalPrompt) {
		OAuth2Client client = OAuth2Endpoints.getClient(new State(state).name);
		if (forceApprovalPrompt) {
			client.setForceApprovalPrompt(true);
		}
		
		client.retrieveVerificationCode(getSocialAuthCallbackUrl(), state);
	}
	
	public static void socialAuthCallback(String state) {
		Integer externalLoginFailed = null;
		String externalLoginFailedMsg = null;
		OAuth2Response response = null;
		State stateObj = new State(state);
		
		if (OAuth2Client.isCodeResponse()) {
			response = OAuth2Endpoints.getClient(stateObj.name).retrieveAccessToken(getSocialAuthCallbackUrl());
			if (StringUtils.isNotBlank(response.getAccessToken())) {
				Logger.info("got access token %s from %s (refresh token %s)", response.getAccessToken(), stateObj, response.getRefreshToken());
				
				if (stateObj.type != null && stateObj.type.equals(StateType.SIGNUP)) {
					Map<String, String> parameters = new HashMap<String, String>();
					parameters.put("accessToken", response.getAccessToken());
					parameters.put("refreshToken", response.getRefreshToken());
					parameters.put("providerId", stateObj.name.toUpperCase());
					callbackGoToSignup(parameters);
				} else if (stateObj.type != null && stateObj.type.equals(StateType.LINK)) {
					try {
						callbackDoLink(stateObj, response.getAccessToken(), response.getRefreshToken());
					} catch (ApiCallException e) {
						externalLoginFailed = e.getHttpStatus();
						externalLoginFailedMsg = e.getErrorMessage();
						Logger.info("External link failed: %s", e.getMessage());
					}
				} else {
					try {
						callbackDoLogin(stateObj, response.getAccessToken(), response.getRefreshToken());
					} catch (ApiCallException e) {
						externalLoginFailed = e.getHttpStatus();
						externalLoginFailedMsg = e.getErrorMessage();
						Logger.info("External login failed: %s", e.getMessage());
					}
				}
			} else {
				Logger.warn("oauth error %s from %s", response.getError().toString(), stateObj);
				
				if (OAuth2Error.Type.UNKNOWN.equals(response.getError().getType())) {
					Logger.warn("oauth raw error %s from %s", response.getHttpResponse().getString(), stateObj);
				}
			}
		}
		
		Map<String, String> parameters = new HashMap<String, String>();
		if (response != null && response.getError() != null) {
			parameters.put("externalLoginError", response.getError().toString());
		}
		if (externalLoginFailed != null) {
			parameters.put("externalLoginFailed", String.valueOf(externalLoginFailed));
			parameters.put("externalLoginType", stateObj.name != null ? StringUtils.capitalize(stateObj.name.toLowerCase()) : "External");
			if (stateObj.type != null && stateObj.type.equals(StateType.LINK) && StringUtils.isNotBlank(externalLoginFailedMsg)) {
				parameters.put("externalLoginFailedMsg", externalLoginFailedMsg);
			}
		}
		if (StringUtils.isNotBlank(stateObj.breadcrumb)) {
			parameters.put(State.BREADCRUMB_KEY, stateObj.breadcrumb);
		}
		
		if (stateObj.type != null && stateObj.type.equals(StateType.SIGNUP)) {
			callbackGoToSignup(parameters);
		} else if (stateObj.type != null && stateObj.type.equals(StateType.SIGNIN)) {
			callbackGoToSignin(parameters);
		} else if (stateObj.type != null && stateObj.type.equals(StateType.LINK)) {
			redirect(Constants.APP_BASE_URL_STRIPPED + "/app?" + buildQueryString(parameters));
		} else {
			redirect(Constants.APP_BASE_URL_STRIPPED + "/login?" + buildQueryString(parameters));
		}
	}
	
	private static void callbackDoLogin(State state, String accessToken, String refreshToken) throws ApiCallException {
		AuthenticateResponse authResp = new ApiCall().authenticateExternal(state.name.toUpperCase(), accessToken, refreshToken);
		if (authResp.success()) {						
			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put(Constants.SESSION_ID_KEY, authResp.getSessionId());
			parameters.put(REMEMBER_ME_KEY, Boolean.TRUE.toString());
			if (StringUtils.isNotBlank(state.breadcrumb)) {
				parameters.put(State.BREADCRUMB_KEY, state.breadcrumb);
			}
			
			if (state.type != null && state.type.equals(StateType.SIGNIN)) {
				callbackGoToSignin(parameters);
			} else {
				redirect(Constants.APP_BASE_URL_STRIPPED + "/app?" + buildQueryString(parameters));
			}
		}
	}
	
	private static void callbackDoLink(State state, String accessToken, String refreshToken) throws ApiCallException {
		ApiGeneralResponse resp = new ApiCall(StringUtils.isBlank(state.breadcrumb) ? getSessionId() : state.breadcrumb)
			.linkExternal(state.name.toUpperCase(), accessToken, refreshToken);
		if (resp.success()) {						
			redirect(Constants.APP_BASE_URL_STRIPPED + "/app");
		}
	}
	
	private static void callbackGoToSignup(Map<String, String> parameters) {
		redirect(Constants.APP_BASE_URL_STRIPPED + "/signup/external?" + buildQueryString(parameters));
	}
	
	private static void callbackGoToSignin(Map<String, String> parameters) {
		redirect(Constants.APP_BASE_URL_STRIPPED + "/websiteOAauthRedirect?" + buildQueryString(parameters));
	}
	
	@Util
	protected static String getSocialAuthCallbackUrl() {
		if (Play.mode.isDev()) {
			// Google works only with localhost in DEV environment.
			return "http://localhost:9200/login/socialauthcallback/";
		} else {
			return "https://" + Constants.DEFAULT_SECURE_DOMAIN + Router.reverse("OAuth.socialAuthCallback") + "/";
		}
	}
	
	private static class State {
		private static final String BREADCRUMB_KEY = "breadcrumb";
		
		private String name;
		private StateType type;
		private String breadcrumb;
		
		public State(String urlParam) {
			if (StringUtils.isNotBlank(urlParam)) {
				String[] split = StringUtils.split(urlParam, '.');
				if (split.length == 2 || split.length == 3) {
					name = split[0];
					type = parseType(split[1]);
				}
				if (split.length == 1) {
					name = split[0];
				}
				if (split.length == 3) {
					breadcrumb = split[2];
				}
			}
		}
		
		private StateType parseType(String type) {
			try {
				return StateType.valueOf(type.toUpperCase());
			} catch (Exception e) {
				return StateType.LOGIN;
			}
		}
		
		@Override
		public String toString() {
			return new StringBuilder().append("[name=").append(name).append(",type=").append(type).append(",breadcrumb=").append(breadcrumb).append("]").toString();
		}
	}
}
