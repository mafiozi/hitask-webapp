package beans;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;

public class LocaleWrapper extends SimpleWrapper<Locale>{
	public LocaleWrapper(Locale t) {
		super(t);
	}

	@Override
	protected void parseValue() {
		value = wrappedObject.getLanguage() + (StringUtils.isNotBlank(wrappedObject.getCountry()) ? ("_" + wrappedObject.getCountry()) : "");
	}

	@Override
	protected void parseName() {
		name = StringUtils.capitalize(wrappedObject.getDisplayLanguage(wrappedObject));
	}
}
