define([
	"dojo/_base/array", // array.forEach array.map
	"dojo/_base/declare", // declare
	"dojo/cldr/supplemental", // cldrSupplemental.getFirstDayOfWeek
	"dojo/date", // date
	"dojo/date/locale",
	"dojo/dom", // dom.setSelectable
	"dojo/dom-class", // domClass.contains
	"dojo/_base/event", // event.stop
	"dojo/_base/lang", // lang.getObject, lang.hitch
	"dojo/_base/sniff", // has("ie") has("webkit")
	"dojo/string", // string.substitute
	"dojo/_base/window", // win.doc.createTextNode
	"dijit/_Widget",
	"dijit/_Templated",
	'dojox/css3/transition',
	"dojo/text!./group.html"
], function(array, declare, cldrSupplemental, date, local, dom, domClass, event, lang, has, string, win,
			_WidgetBase, _TemplatedMixin, css3fx, template){
	
	//Replace localization string in template instead of using attributes
	template = template.replace(/\{#([^#]+)#\}/g, function (all, str) {
		var localized = hi.i18n.getLocalization(str.trim()) || '';
		if (!localized) console.warn('Missing localization for "' + str + '"');
		return localized;
	});
	
	// module:
	//		hi/widget/Group
	// summary:
	//		ProjectList group
	
	var Group = declare("hi.widget.Group", [_WidgetBase, _TemplatedMixin], {
		// summary:
		//		ProjectList Group
		//		Used for Starred, "My view" "Completed", date groups, colors and team 
		
		// Template for the group
		templateString: template,
		
		// Localization
		localeRemoveLink: hi.i18n.getLocalization("completed.removeLink"),
		localeRemoveLinkTitle: hi.i18n.getLocalization("completed.removeLinkTitle"),
		localeRemoveLinkText: hi.i18n.getLocalization("completed.removeLinkText"),
		localeArchiveLink: hi.i18n.getLocalization("completed.archiveLink"),
		localeOpenProject: hi.i18n.getLocalization("project.click_to_open_project"),
		
		itemCount: 0,
		_setItemCountAttr: function (count) {
			if (this.disabled || ((!count || parseInt(count) < 1) && !this.emptyVisible)) {
				//No items, so hide group
				dojo.addClass(this.outerNode, "hidden");
			} else {
				dojo.removeClass(this.outerNode, "hidden");
				
				var title = count ? count : hi.i18n.getLocalization("main.no");
				if (count % 10 == 1 && count % 100 != 11) {
					title += " " + hi.i18n.getLocalization("project.item");
				} else {
					title += " " + hi.i18n.getLocalization("project.items");
				}
				
				this.itemCountNode.innerHTML = title;
			}
			
			this._set("itemCount", count);
		},
		
		// Class name
		groupClassName: "",
		
		// If list is empty, then still show it
		emptyVisible: true,
		
		groupSubmenusVisible: false,
		animationEnabled: false,
		widgetCreated: false,
		
		// Disabled
		disabled: false,
		_setDisabledAttr: function (disabled) {
			this._set("disabled", disabled);
			
			for (var i=0,ii=this.lists.length; i<ii; i++) {
				this.lists[i].set("disabled", disabled);
			}
			
			if (disabled) {
				dojo.addClass(this.outerNode, "hidden");
			} else {
				if (this.itemCount || this.emptyVisible) {
					dojo.removeClass(this.outerNode, "hidden");
				} else {
					dojo.addClass(this.outerNode, "hidden");
				}
			}
		},
		
		// Project id
		projectId: null,
		_setProjectIdAttr: function (id) {
			this.outerNode.projectId = id;
			this.projectIdInputNode.value = id;
			this._set("projectId", id);
		},
		
		// Move title
		moveTitle: null,
		_setMoveTitleAttr: function (title) {
			this.projectMoveTitleNode.value = title;
			this._set("moveTitle", title);
		},
		
		// Title
		title: '',
		_setTitleAttr: function (title) {
			this._set("title", title);
			
			if (title) {
				this.projectTitleNode.innerHTML = "";
				this.projectTitleNode.appendChild(win.doc.createTextNode(title));
				dojo.removeClass(this.outerNode, "no_title");
			} else {
				dojo.addClass(this.outerNode, "no_title");
			}
		},
		
		// Project/Group lists
		lists: null,
		
		// Group is expanded
		expandable: true,
		_setExpandableAttr: function (expandable) {
			this._set("expandable", expandable);
			if (!expandable) this.set("expanded", true);
		},
		
		/*
		 * Callback after this group has finished expanding
		 */
		afterExpand:function()
		{
			for (var i = 0;i<this.lists.length;i++)
			{
				/* See hi/items/view-my and the specification for TaskListCompleted */
				if (this.lists[i].afterExpand) this.lists[0].afterExpand();
			}			
		},
		
		expanded: null,
		_setExpandedAttr: function (expanded) {
			if (!this.get("expandable")) {
				expanded = true;
			}

			var self = this;
			var animate = function(callback) {
				if (self.widgetCreated && self.animationEnabled && supportsCSS3Transitions()) {
					var transition = function(node) {
						css3fx.fade(node, {
							'in': expanded ? true : false,
							duration: CONSTANTS.animationDuration.groupFadeMs,
							direction: expanded ? 1 : -1,
							onAfterEnd: function() {
								node.style.display = '';
								if (typeof(callback) == 'function') {
									callback();
								}
								self.afterExpand();
							}
						}).play();
					};
					
					transition(self.tasksNode);
					transition(self.groupSubmenusNode);
				} else /*if (typeof(callback) == 'function')*/ {
					if (typeof(callback) == 'function') callback();
					self.afterExpand();
				}
			};
			
			// Style.
			if (expanded) {
				dojo.addClass(this.headerNode, "opened");
				if (this.groupSubmenusVisible) {
					dojo.removeClass(this.groupSubmenusNode, 'hidden');
				}
				
				dojo.addClass(this.headerExpandNode, 'hidden');
				dojo.removeClass(this.headerCollapseNode, 'hidden');
				dojo.removeClass(this.tasksNode, "hidden");
				
				animate();
			} else {
				dojo.removeClass(this.headerNode, "opened");
				dojo.removeClass(this.headerExpandNode, 'hidden');
				dojo.addClass(this.headerCollapseNode, 'hidden');
				dojo.addClass(self.tasksNode, "hidden");
				dojo.addClass(self.groupSubmenusNode, 'hidden');
			}
			
			if (this.expanded != expanded && dragSources.targetProjects) {
				// Make sure coordinates will be up to date.
				dragSources.targetProjects.resetProjectCoordinates();
			}
			
			//Save preferences
			if (this.expanded !== null && this.expanded != expanded) {
				var key = "p_" + this.preferencesGroup + '_' + this.projectId;
				if (key) {
					Hi_Preferences.set(key, expanded ? '1' : '0');
				}
			}
			
			this._set("expanded", expanded);
		},
		
		// Drag and drop source
		dragSource: null,
		
		// If project will be used with ProjectList widget or standalone
		// If will be ProjectList child, then there is no need for wrapping UL element
		isInProjectList: false,
		
		// Prefrences group, used when saving expanded state into preferences
		preferencesGroup: 0,
		
		
		constructor: function (/*Object*/args) {
			this.lists = this.lists || [];
		},
		
		uninitialize: function () {
			if (this.dragSource) {
				this.dragSource.destroy();
			}
		},
		
		buildRendering: function() {
			if (!this.isInProjectList) {
				//If not in the list then add wrapper
				this.templateString = "<ul class=\"projects\">" + this.templateString + "</ul>";
			}
			
			this.inherited(arguments);
			
			//Get preferences
			if (this.expandable && this.expanded === null) {
				var key = "p_" + this.preferencesGroup + '_' + this.projectId,
					pref = Hi_Preferences.get(key, "1");
				
				this.set("expanded", (pref == "1" ? true : false));
			}
			
			if (this.moveTitle) {
				this.buildDragAndDrop();
			}
		},
		
		postCreate: function() {
			this.inherited(arguments);
			this.widgetCreated = true;
		},
		
		buildDragAndDrop: function () {
			// summary:
			//		Attach drag and drop
			
			// /this.domNode.projectId = this.projectId;
			this.dragSource = new dojo.dnd.HiDragSource(this.domNode, {});
		},
		
		addChild: function (node, index) {
			// summary:
			//		Place child node
			
			dojo.place(node.domNode, this.contentNode, "first");
			node.set("container", this);
		},
		
		
		/* ----------------------------- API ------------------------------ */
		
		
		toggle: function () {
			this.set("expanded", !this.expanded);
		},
		
		expand: function () {
			this.set("expanded", true);
		},
		
		collapse: function () {
			this.set("expanded", false);
		},
		
		addList: function (list) {
			// summary:
			//		Add list to the group
			
			if (dojo.indexOf(this.lists, list) == -1) {
				this.lists.push(list);
				
				if (this.disabled) list.set("disabled", true);
				
				// Sync drag and drop
				if (this.dragSource) {
					this.dragSource.sync();
				}
			}
		},
		
		removeList: function (list) {
			// summary:
			//		Add list to the group
			
			var index = dojo.indexOf(this.lists, list);
			if (index != -1) {
				this.lists.splice(index, 1);
				
				// Sync drag and drop
				if (this.dragSource) {
					this.dragSource.sync();
				}
			}
		},
		
		expandCollapseAll: function() {
			var selectedTab = Tabs.active;
			if (selectedTab) {
				var thisExpanded = this.get('expanded');
				switch (selectedTab.grouping) {
					case 2:
						// Date tab.
						if (hi.items.DateGroupsList && hi.items.DateGroupsList.length > 0) {
							for (var i = 0; i < hi.items.DateGroupsList.length; i++) {
								if (hi.items.DateGroupsList[i]) {
									if (thisExpanded) {
										hi.items.DateGroupsList[i].expand();
									} else {
										hi.items.DateGroupsList[i].collapse();
									}
								}
							}
						}
						break;
					case 5:
						// Team tab.
						if (thisExpanded) {
							hi.items.ProjectListTeam.expandAll();
						} else {
							hi.items.ProjectListTeam.collapseAll();
						}
						break;
				}
			}
		},
		
		/* ----------------------------- EVENT BINDINGS ------------------------------ */
		
		
		handleToggle: function (e) {
			this.toggle();
			// #2561
			if (e && (e.shiftKey || e.ctrlKey || e.altKey)) {
				this.expandCollapseAll();
			}
		},
		
		handleRemoveCompleted: function (event) {
			Project.removeCompletedItems(this.groupSubmenuDeleteCompleted);
		},
		
		handleArchiveCompleted: function () {
			Project.removeArchivedItems(this.groupSubmenuDeleteCompleted);
		}
	});
	
	Group.GROUP_MY = 0;
	Group.GROUP_ACTIVITY = 1;
	Group.GROUP_DATE = 2;
	Group.GROUP_BIGCALENDAR = 7;
	Group.GROUP_PROJECT = 4;
	Group.GROUP_COLOR = 3;
	Group.GROUP_TEAM = 5;
	return Group;
	
});