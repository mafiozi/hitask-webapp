//Feature #1721
define(['dijit/TooltipDialog'/*,'./extendPopup'*/,
		'dijit/popup'
], function (TooltipDialog/*,hiPopup*/, popup) {

	var Tooltip = dojo.global.Tooltip = {
		
		/**
		 * Feature #1721 - set of tooltips converted to dijit.TooltipDialog
		 */
		_convertedTooltips:{
			'help':1,
			'search':1,
			'taskedit':1,
			'deleteconfirmation':1,
			'bigCalendarCreateEvent':1,
			'deleterecurring':1,
			'newproject':1,
			'changepassword':1,
			'settingschangedresult':1,
			'frfind':1,
			'frconfirm':1,
			'confirm_email':1,
			'formattinghelp':1,
			'tabs-menu': 1,
			'password_input_validation_error':1,
			'comment_input_validation_error':1,
			'timer':1,
			'upload':1,
			'report':1,
			'projectProgressReport':1,
			'export':1,
			'sync':1,
			feedback:1,
			news:1,
			message:1,
			queue:1,
			contact_info:1,
			'syncerrorinfo':1
		},
		
		fixedTooltips:{
			timer:1,
			search:1,
			help:1,
			upload:1,
			report:1,
			'export':1,
			'sync':1,
			feedback:1,
			queue:1
		},
		
		fixedWidthTooltips: {
			contact_info: 300,
			upload: 300
		},
		
		forceNoTriangle:{
			contact_info:1
		},
		/** 
		 * Feature #1721 - stack of popups
		 */
		//popups:{},
		
		tooltip_position_functions: {},
		openers: {},
		
		/**
		 * Update all tooltip positions
		 */
		updatePositions: function() {
			var i;
			for (i in Tooltip.tooltip_position_functions) {
				Tooltip.tooltip_position_functions[i]();
			}
		},
		
		/**
		 * Returns true if tooltip is opened
		 * 
		 * @param {String} name Tooltip ID
		 * @return True if tooltip is opened, otherwise false
		 * @type {Boolean}
		 */
		opened: function (name) {
			var el = dojo.byId('tooltip_' + name);
			var op = false;
			try {op = el.getAttribute("opened");} catch(e) {op = -1;}
			if (!op) {op = -1;}
			return (op != "1" ? false : true);
		},
		
		/**
		 * Open tooltip
		 * 
		 * @param {String} name Tooltip ID
		 * @param {Object} data Template data
		 * @param {HTMLElement} t Tooltip target element
		 * @param {Object} e 
		 * @param {String} p Optional. Tooltip position, "bottom", "top", "right" or "left". Default is "bottom"
		 * @param {HTMLElement} start Optional, if specified this template will be filled instead of tooltip
		 * @param {Boolean} whether an underLayWrapper will be shown
		 */
		open: function(name, data, t, e, p, start, dijitTooltipOptions, underLayWrapper){
			//console.log('open',[name,data,t,e,p,start]);
			//Feature #1721
			if (name in this._convertedTooltips) 
			{
				return this.createTooltipDialog(name, data, t, e, p, start,dijitTooltipOptions, underLayWrapper);
			}
			p = p || "bottom";
			data = data || templateParse(t.getAttribute('data')) || {};
			start = start || 'tooltip_' + name;
			start = dojo.byId(start);
			var el = dojo.byId('tooltip_' + name);
			if(!el){ return false; }

			var ex, op = false;
			try{
				op = el.getAttribute("opened");
			}catch(exp){
				op = -1;
			}
			if (!op) {op = -1;}
			if (op != "-1") return false;
			dojo.publish("TooltipOpenBefore", [name, t]);
			el.setAttribute("opened", "0");
			Tooltip.position(el, t, p);
			Tooltip.tooltip_position_functions[name] = function(){Tooltip.position(el, t, p);};
			showElement(el);
			fillTemplate(start, data);
			dojo.style(el, "opacity", 0);
			el.style.visibility = "visible";
			var anim = dojo.fadeIn({node: el, duration: dojo.isIE ? 1 : CONSTANTS.duration});
			dojo.connect(anim, "onEnd", function(){
				el.style.opacity=1;el.setAttribute("opened", "1");var focus = supra.common.getElementsByClass("focus", el);
				if (focus.length > 0){try{focus[0].focus();}catch(ex){ALERT(ex.message);}}
				dojo.publish("TooltipOpenAfter", [name, t]);
			});
			anim.play();
			Tooltip.openers[name] = t;
			return true;
		},
		
		/** 
		 * Feature #1721 open tooltip using dijit.TooltipDialog wrapper
		 */
		createTooltipDialog: function(name, data, t, e, p, start, dijitTooltipOptions, underLayWrapper){
			p = p ? [p] : ['below-centered', 'above-centered'];
			var popupOptions = {};
			
			//We will use dijitTooltipOptions to specify if a tooltip dialog points to a node ('orient')
			// or hovers over it instead ('x','y')
			if (dijitTooltipOptions){
				if (dijitTooltipOptions.orient){
					popupOptions.around = t;
					// popupOptions.orient = [dijitTooltipOptions.orient];
					popupOptions.orient = dijitTooltipOptions.orient;
				} else {
					if (!dijitTooltipOptions.x){
						var position = dojo.position(t,true);
						popupOptions.x = position.x - position.w;
						popupOptions.y = position.y - position.h;
					} else{
						popupOptions.x = dijitTooltipOptions.x;
						popupOptions.y = dijitTooltipOptions.y;
					}
				}
			} else {
				popupOptions.orient = p;
				popupOptions.around = t;
			}
			
			data = data || templateParse(t.getAttribute('data')) || {};
			
			var el = dojo.byId('tooltip_' + name);
			if(!el) return false;
			
			dojo.style(el,'position','relative');
			var dijitID = 'tooltip_' + name + '_TooltipDialog';
			//showElement(el);
			var ex, op = false;
			try{
				op = el.getAttribute("opened");
			}catch(exp){
				op = -1;
			}
			if (!op) {op = -1;}
			if (op != "-1") return false;
			dojo.publish("TooltipOpenBefore", [name, t]);
			el.setAttribute("opened", "0");
			
			showElement(el);
			fillTemplate(start, data);
			dojo.style(el, "opacity", 0);
			dojo.style(el, "z-index", 1000);

			if(underLayWrapper){
				this._addScreenBlock();
			}

			el.style.visibility = "visible";
			var anim = dojo.fadeIn({node: el, duration: dojo.isIE ? 1 : CONSTANTS.duration,onEnd:dijitTooltipOptions&&dijitTooltipOptions.onEnd?dijitTooltipOptions.onEnd:null});
			dojo.connect(anim, "onEnd", function(){
				el.style.opacity=1;el.setAttribute("opened", "1");var focus = supra.common.getElementsByClass("focus", el);
				if (focus.length > 0){try{focus[0].focus();}catch(ex){ALERT(ex.message);}}
				dojo.publish("TooltipOpenAfter", [name, t]);
			});
			//anim.play();
			Tooltip.openers[name] = t;
			
			var ttd = dijit.byId(dijitID);
			if (!ttd)
			{
				var ttdOpts = {
					id:dijitID,
					autoFocus:false,
					content:el
				};
				
				if (name in this.fixedWidthTooltips) {
					ttdOpts.style = "width: " + this.fixedWidthTooltips[name] + "px;";
				}
				ttd = new TooltipDialog(ttdOpts);
			}
			if (name in this.fixedTooltips) popupOptions.fixed=true;
			popupOptions.popup = ttd;
			popupOptions.onClose = function() {};
			/*
			popup.open({
				popup:ttd,
				around:t,
				orient:p,
				//orient:p,
				//x:position.x-position.w,
				//y:position.y-position.h,
				onClose:function() {*Tooltip.close(name);*}
			});
			*/
			window.popupOptions = popupOptions;
			popup.open(popupOptions, true/*isHiTooltip*/);
			if (name in this.forceNoTriangle)
			{
				dojo.removeClass(ttd.connectorNode,'dijitTooltipConnector');
			} else if (!popupOptions.orient) {
				dojo.removeClass(ttd.connectorNode,'dijitTooltipConnector');
			} else if (popupOptions.orient == 'below-centered') {
				// #2123.
				ttd.connectorNode.style.top = '';
			}
			
			anim.play();
			return true;
		},
				
		/**
		 * Position tooltip
		 * 
		 * @param {HTMLElement} el Tooltip element
		 * @param {HTMLElement} t Target element
		 * @param {String} p Tooltip position relative to element
		 */
		position: function(el, t, p) {
			if (!dojo.dom.isInDom(t)) {
				//If target element is not in DOM then can't position it
				return;
			}
			
			var fixed = dojo.hasClass(t, 'fixed');
			var pos = fixed ? dojo.position(t,false) : dojo.position(t,true);

			if (dojo.isOpera && dojo.hasClass(t,'operascrollfix')) {
				var t_ = t;
				while(t_ != document.body){
					if (dojo.hasClass(t_, 'scroll')) {
						pos.y -= isFinite(t_.scrollTop) ? t_.scrollTop : 0;
						pos.x -= isFinite(t_.scrollLeft) ? t_.scrollLeft : 0;
					}
					if(t_.scrollTop !== 0){ ALERT(t_.tagName, t_.scrollTop); }
					t_ = t_.parentNode;
				}
			}
			var tmp = elementVisible(el);
			if (!tmp) {
				el.style.top = "0px";
				el.style.left = "0px";
				el.style.visibility = "hidden";
				showElement(el);
			}
			var w = el.offsetWidth;
			var h = el.offsetHeight;
			if (!tmp) {
				hideElement(el);
				el.style.visibility = "visible";
			}
			pos.x-=1;
			dojo.style(el, "position", (fixed ? 'fixed' : 'absolute'));
			switch (p) {
				case "bottom":
					dojo.style(el, "top", (pos.y+t.offsetHeight)+"px");
					dojo.style(el, "left", (pos.x+Math.floor(t.offsetWidth/2))+"px");
					break;
				case "top":
					dojo.style(el, "top", (pos.y-h)+"px");
					dojo.style(el, "left", (pos.x+Math.floor(t.offsetWidth/2))+"px");
					break;
				case "right":
					dojo.style(el, "top", (pos.y+Math.round(t.offsetHeight/2))+"px");
					dojo.style(el, "left", (pos.x+t.offsetWidth)+"px");
					break;
				case "left":
					dojo.style(el, "top", (pos.y+Math.round(t.offsetHeight/2))+"px");
					dojo.style(el, "left", (pos.x-w)+"px");
					break;
			}
		},
		
		/**
		 * Close tooltip
		 * 
		 * @param {String} name Tooltip ID
		 */
		close: function(name, force) {
			var el = dojo.byId('tooltip_' + name), anim;
			
			if (!Tooltip.opened(name) && !force) return false;
			dojo.publish("TooltipCloseBefore", [name]);
			document.body.focus();
			el.setAttribute("opened", "0");
			// Feature #1721
			if (name in this._convertedTooltips)
			{
				hideElement(el);
				el.setAttribute('opened',"-1");
				//this.popups['tooltip_'+name+'_TooltipDialog'].close(dijit.byId('tooltip_'+name+'_TooltipDialog'));
				popup.close(dijit.byId('tooltip_'+name+'_TooltipDialog'));
				dojo.publish("TooltipCloseAfter", [name]);
			} else
			{
				try {
					if (dojo.isIE){
						anim = dojo.fadeOut({node: el, duration: 1});
					}else{
						anim = dojo.fadeOut({node: el, duration: CONSTANTS.duration});
					}
					dojo.connect(anim, "onEnd", function(){
						hideElement(el);el.setAttribute("opened", "-1");
						dojo.publish("TooltipCloseAfter", [name]);
					});
					anim.play();
				} catch (err) {
					hideElement(el);el.setAttribute("opened", "-1");
					dojo.publish("TooltipCloseAfter", [name]);
				}
			}

			this._removeScreenBlock();

			delete Tooltip.tooltip_position_functions[name];
			delete Tooltip.openers[name];
			return true;
		},
		
		/**
		 * Returns element, which was last target for given tooltip
		 * 
		 * @param {Object} name
		 * @return Element, which was last target for given tooltip
		 * @type {Array}
		 */
		opener: function(name) {
			return Tooltip.openers[name];
		},

		_addScreenBlock: function(){
			dojo.create("div", { 
				id: "lockScreenBlock", 
				'class': "lockOn",
				style: {
					width: document.body.clientWidth + "px",
					height: document.body.clientHeight + "px"
				} 
			}, dojo.body());
		},

		_removeScreenBlock: function(){
			dojo.destroy('lockScreenBlock');
		}
	};
	
	return Tooltip;
	
});