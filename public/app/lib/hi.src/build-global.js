dojo.global.hi = dojo.global.hi || {};

define([
	'moment/moment',
	"dojo/on",
	"dijit/_base",
	"dijit/Dialog",
	"supra/common",
	"hi/i18n",
	'hi/extendPopup',
	"hi/global",
	"hi/apiresponse",
	
	"hi/calendar",
	
	"hi/taskcalendar",
	"hi/overdue",
	"hi/base",
	"hi/form",
	"hi/tooltip",
	'hi/tabs',
	
	"hi/project",
	"hi/items",

	"hi/connectionqueue",
	"hi/manytasks",
	'hi/widget/task/stateful_taskitem'
	
], function(moment) {
	window.moment = moment;
	return {};
});