package tags;

import groovy.lang.Closure;

import java.io.PrintWriter;
import java.util.Map;

import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;
import play.templates.JavaExtensions;

@FastTags.Namespace("app.hi")
public class AjaxInline extends BaseTag {
	public static void _ajaxInline(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		if (body != null) {
			out.print(new StringBuilder().append("<div class=\"ajax_inline\">")
				.append(JavaExtensions.toString(body)).append("</div>").toString()
			);
		}
	}
	
	public static void _ajaxInlineClosed(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		if (body != null) {
			out.print(new StringBuilder().append("<div class=\"ajax_inline_closed\">")
				.append(JavaExtensions.toString(body)).append("</div>").toString()
			);
		}
	}
	
	public static void _ajaxInlineOpened(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		if (body != null) {
			String className = getArgAsString("class", args, "");
			
			out.print(new StringBuilder()
				.append("<form action=\"\" onsubmit=\"return false\" class=\"ajax_inline_opened hidden ")
				.append(className).append("\">").append(JavaExtensions.toString(body)).append("</form>").toString()
			);
		}
	}
	
	public static void _ajaxInlineCancel(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		out.print(new StringBuilder().append("var container=supra.common.getParentByClass(this,'project-edit');")
			.append("supra.common.removeClass(container, 'project-edit');")
			.append("hideElement(getParentByClass(this,'ajax_inline','ajax_inline_opened'));")
			.append("showElement(getParentByClass(this,'ajax_inline','ajax_inline_closed'));").toString()
		);
	}
	
	public static void _ajaxInlineSave(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Map<?, ?> parentArgs = getParentArgs(template);
		String after = getArgAsString("after", args);
		String url = getArgAsString("url", args);
		String validate = getArgAsString("validate", parentArgs);
		String function = getArgAsString("function", parentArgs);
		String className = getArgAsString("class", parentArgs);
		
		out.print(JavaExtensions.escapeHtml(
			new StringBuilder().append("var button=this.declaredClass ? this.domNode : this;")
				.append("var opened=getParentByClass(button,'ajax_inline_opened');")
				.append("var container=supra.common.getParentByClass(button,'project-edit');")
				.append("var params = dojo.form.getFormValues(opened);")
				.append(validate != null 
					? new StringBuilder().append("var ret_params = ").append(validate).append("(params);")
						.append("if (ret_params === false) return;")
						.append("if (typeof ret_params == \"object\") params = ret_params;").toString() 
					: "")
				.append(function).append("('").append(url).append("', params, function(data) {")
				.append("var closed=getParentByClass(button,'ajax_inline','ajax_inline_closed');")
				.append("try {var els_c=dojo.query('").append(className).append("',closed)[0];")
				.append("var els_o=dojo.query('").append(className).append("',opened)[0];")
				.append("els_c.innerHTML=htmlspecialchars(els_o.value);} catch (err) {};")
				.append("hideElement(opened);").append("showElement(closed);")
				.append("supra.common.removeClass(container, 'project-edit');")
				.append(after != null ? (after + "(data);") : "").append("});").toString()
		));
	}
}