define('arena/i18n/common', function () {
	
	/**
	 * Create a namespace
	 */
	dojo.ns = function (ns) {
		ns = ns.split(/[\/\.]/);
		
		var context = dojo.global;
		for(var i=0,ii=ns.length; i<ii; i++) {
			if (!context[ns[i]]) context[ns[i]] = {};
			context = context[ns[i]];
		}
	};
	
	
	dojo.ns("arena.i18n");
	
	arena.i18n._location = null;
	arena.i18n.setLocation = function(_path, root) {
		arena.i18n._location = _path;
		if (root) {
			dojo.registerModulePath(arena.i18n.getLocation(), '/' + arena.i18n.getLocation());
		}
	}
	arena.i18n.getLocation = function() {
		if (arena.i18n._location) {
			return arena.i18n._location;
		} else {
			arena.i18n._location = 'i18n';
			arena.i18n.setLocation('i18n', true);
			return 'i18n';
		}
	}
	arena.i18n.getData = function (file) {
		var _path = arena.i18n.getLocation();
		if (_path) {
			dojo['require' + 'Localization'](_path, file, dojo.locale, "");
			return dojo.i18n.getLocalization(_path, file, dojo.locale);
		} else {
			return null;
		}
	}
	arena.i18n.getValue = function (key) {
		key = dojo.trim(key);
		key = key.split('.');
		var file = key.shift();
		var data = arena.i18n.data;
		
		if (key.length > 1) {
			return (typeof data[key[0]] != 'undefined' ? data[key[0]][key[1]] : '');
		} else {
			return data[key[0]];
		}
	}

});