/**
 * A non-widget representation of a task item on the UI.  The goal of this object is to replace the capabilities and features
 * of hi/widget/task/item without the overhead associated with instantiating a dojo widget (see issue #2167).
 * 
 * This object contains:
 * - emulated setters/getters from hi/widget/task/item
 * - emulated event handlers from hi/widget/task/item
 * - string HTML template corresponding to an item on the UI
 */
define([
	"dojo/_base/lang",
	"dojo/_base/window",
	'dojo/_base/array',
	'dojo/_base/event',
	'dojox/image',
	'dojox/image/Lightbox',
	'dojo/on',
	'dojo/dom-style',
	'dojo/query',
	'dijit/registry',
	'dojo/dom-class'
], function(lang, win, dojoArray, dojoEvent, dojoxImage, dojoLightbox, dojoOn, dojoStyle, dojoQuery, registry, dojoClass) {
	
	var item_DOM = {
			lightboxDialog: null,
			createDOMItem: function(list, item) {
				/*
				 * Summary:
				 * Take specified properties from input item object and generated a task item object and its filled
				 * in UI HTML template
				 * 
				 * Input: 
				 * list hi/widget/task/list
				 * item JSON object of attributes for an item
				 */
				var Attr;
				var o = {
					taskId:item.id,
					widgetId:dijit.getUniqueId('hi_widget_TaskItem'),
					list: list,
					localeClickToView:this.localeClickToView,
					localeClickToComplete:this.localeClickToComplete
					
				};
				o.date_title_content = '';
				o.date_title_class = 'hidden';
				//o['widgetId'] = item['widgetId'];
				for (var i in item) {
					Attr = i.substr(0,1).toUpperCase()+i.substr(1);
					//console.log('ATTR',Attr);
					if (this['_handle' + Attr + 'Attr']){
						this['_handle' + Attr + 'Attr'](o, item[i], item, list);
					}
				}
				
				// The drag visible class
				//Drag visible class
				
				
				if (!this.dragHandleAllowed(item)) {
					//this.set("dragVisibleClass", "hidden"); // already default
					o.drag_visible_class = 'hidden';
				} else {
					o.drag_visible_class = '';
				}
				
				var hasNonProjectParentClass = '';
				if (item.parent) {
					var parent = hi.items.manager.get(item.parent);
					if (parent && parent.category != this.CATEGORY_PROJECT) {
						hasNonProjectParentClass = 'hasNonProjectParentClass';
					}
				}
				o.has_non_project_parent_class = hasNonProjectParentClass;
				
				this.preloadPreviewImages(item.id);
				if (!this.lightboxDialog) {
					this.lightboxDialog = new hi.widget.PreviewLightbox();
					this.lightboxDialog.startup();
				}
				
				return o;
			},
			
			/**
			 * Preload item image previews if any.
			 */
			preloadPreviewImages: function(id) {
				var o = hi.items.manager.get(id);
				if (o && o.previews) {
					dojox.image.preload(dojoArray.map(o.previews, function(x) {
						return x.url;
					}));
				}
			},
			
			/**
			 * Tries to find preview with specified size.
			 * If size = 'max' it searches for largest preview.
			 * Returns any preview if desired size does not exist.
			 */
			getPreviewImageUrl: function(taskId, size) {
				var o = hi.items.manager.get(taskId);
				var url = null;
				var retina = dojo.global.devicePixelRatio && dojo.global.devicePixelRatio > 1;
				var desiredType = 'PROPORTIONAL';
				var preview;
				var len, i;
				
				if (!size) {
					size = retina ? 1000 : 500;
				} else if (size == 'max') {
					var max = 0;
					if (o && o.previews) {
						for (i = 0, len = o.previews.length; i < len; i++) {
							preview = o.previews[i];
							max = Math.max(max, preview.size);
						}
					}
					size = max;
					desiredType = 'PROPORTIONAL';
				}
				
				var desiredSize = size;
				
				if (o && o.previews) {
					for (i = 0, len = o.previews.length; i < len; i++) {
						preview = o.previews[i];
						if (preview.size == desiredSize && preview.type == desiredType && preview.url) {
							// This is exactly what we need.
							return preview.url;
						} else {
							url = preview.url;
						}
					}
				}
				
				return url;
			},
			
			dragHandleAllowed: function(item) {
				var isChild = this.isChild(item),
					sorting = Project.getSorting(),
					group = Hi_Preferences.get('currentTab', 'my', "string");
				
				if ((!isChild && group == 'date') || sorting != Project.ORDER_PRIORITY || (item.taskItem && item.taskItem.completed)) {
					return false;
				} else {
					return true;
				}
			},
			
			createWidget:function(data,item,instance_id,parent) {
				
			},
			
			/** Sorting functionality */
			doCompare:function(toAddWidget,addAtWidget,sortOptions,index,debug)
			{			
				//if (!addAt) console.log('NO ADDAT',[this.id,toAddWidget,addAt,index]);
				var SOLength = sortOptions.length,
					addAtValue,
					toAddValue;
				//if (!addAtItem) console.log('ADDAT ERROR',[this.id,addAt,addAt.taskId,toAddItem]);
				for (var i = 0;i<SOLength;i++)
				{
					addAtValue = addAtWidget.taskItem[sortOptions[i].attribute];
					toAddValue = toAddWidget.taskItem[sortOptions[i].attribute];
					if (sortOptions[i].caseInsensitive)
					{
						addAtValue = addAtValue.toUpperCase();
						toAddValue = toAddValue.toUpperCase();
					}
					if (addAtValue != toAddValue)
					{
						addAtValue = addAtWidget.taskItem[sortOptions[i].attribute];
						toAddValue = toAddWidget.taskItem[sortOptions[i].attribute];
						if (sortOptions[i].caseInsensitive)
						{
							addAtValue = addAtValue.toUpperCase();
							toAddValue = toAddValue.toUpperCase();
						}
						
						if (addAtValue != toAddValue)
						{
							if (!addAtValue) return false;
							if (!toAddValue) return true;
							if (sortOptions[i].descending)
							{
								if (toAddValue < addAtValue) return true;
								else return false;
							} else
							{
								if (toAddValue > addAtValue) return true;
								else return false;
							}
						}
					}
				}
				
				return true;
			},
			
			_sortDOM:function(itemDOMs,itemWidgets) {
				var sortConfiguration = hi.store.Project.getOrderConfiguration(true)[0],
				sortOrder = sortConfiguration.descending? 'Descending' : 'Ascending',
				sortOptions = hi.store.Project.getOrderConfiguration(true),
				sortAttribute = sortConfiguration.attribute,
				sorted = [];
				
				for (var i in itemDOMs)
				{
					this._addDOM(sorted, itemWidgets, itemWidgets[i], sortOptions, i);
					//this._addBinaryDOM(sorted,itemWidgets,itemWidgets[i],sortOptions,i,0,sorted.length);
				}
				return sorted;

			},
			
			_addBinaryDOM: function(sorted, itemWidgets, itemWidget, sortOptions, widgetIndex, start, end){
				/*
				 * Summary:
				 * A non-working initial implementation of binary search for sorting items in a list
				 */
				//var SOLength = sortOptions.length,
				//len = sorted.length,
				//i = 0;
				if (start == 0 && end == 0) {sorted.push(widgetIndex);}
				else if (start == end){
					//console.log('end');
					sorted.splice(start,0,widgetIndex);
				}else{
					end = end - 1;
					var middle = end = 0 ? 0 : Math.floor((end+ start)/2);
					//console.log('middle',[middle,start,end]);
					if (this.doCompare(itemWidget,itemWidgets[sorted[middle]],sortOptions,middle,false)){
						//insert in the right somewhere
						this._addBinaryDOM(sorted,itemWidgets,itemWidget,sortOptions,widgetIndex,middle,end);
					} else{
						//insert in the left somewhere
						this._addBinaryDOM(sorted,itemWidgets,itemWidget,sortOptions,widgetIndex,start,middle);
					}
				}
			},
			
			_addDOM:function(sorted,itemWidgets,itemWidget,sortOptions,widgetIndex)
			{
				var SOLength = sortOptions.length,
					len = sorted.length,
					i = 0;
				if (len === 0) sorted.push(widgetIndex);
				else{
					while(i<len){
						if (this.doCompare(itemWidget, itemWidgets[sorted[i]], sortOptions, i, false))
							i++;
						else
							break;
					}
					sorted.splice(i,0,widgetIndex);
				}
			},
			
			
			_handleTaskIdAttr: function (o,taskId) {
				return;
				//If no taskId given, then add some fake one
				taskId = taskId || --this.ID_COUNTER;
				
				if (this.taskId) {
					hi.items.manager.removeItem(this);
					/* Bug #2092,#2095 hi.items.manager.removeItem(this.taskId);*/
				}
				if (taskId) {
					hi.items.manager.addItem(taskId || this.id, this, this.listName || (this.recurring ? this.get("eventdate") : null));
				}
				
				//this.childrenNode.setAttribute("data-task-id", taskId || "");
				//this.itemNode.setAttribute("data-task-id", taskId || "");
				//this.itemNode.taskId = taskId;
				this._set("taskId", taskId);
			},
			
			_handleParentAttr: function (o,parent) {
				var group = Hi_Preferences.get('currentTab', 'my', "string"),
					isSearchActive = Hi_Search ? Hi_Search.isFormVisible() : false,
					tagFilter = Hi_Preferences.get("tagfilter", ""),
					//parent = this.parent
					parent_item = parent ? hi.items.manager.get(parent) : null,
					parent_is_project = (parent_item && !parseInt(parent_item.category)),
					parent_title = [],
					project_title = "",
					ancestor_is_project,
					inTooltip = (o && o.list && o.list.listName == 'tooltip') || (this.list && this.list.listName == 'tooltip');
				
				//Parent title
				// Display it only in Date or Team or Today or All Items tabs, or while Tags Filter is active, or item is going to be rendered in tooltip.
				if (group == 'my' || group == 'team' || group == 'date' || group == 'today' ||
					(!isSearchActive && tagFilter) || (parent && !parent_item) || inTooltip
				) {
					if (parent_item && !parent_is_project) {
						//If parent is not project
						parent_title = parent_item.title;
						o.parent_title_content = parent_title;
						o.parent_title_class = 'parent_title';
						//setTextContent(this.parentTitleNode, parent_title);
						//dojo.addClass(this.parentTitleNode, "parent_title");
						//dojo.removeClass(this.parentTitleNode, "hidden");
					} else if (!parent_item && parent && (parent_title = hi.items.manager.get(o.taskId, 'parentTitle'))) {
						o.parent_title_content = parent_title;
						o.parent_title_class = 'parent_title';
					}
					if (inTooltip) {
						o['parent_title_content'] += '&nbsp;';
					}
					if (inTooltip) {
						o['parent_title_content'] += '&nbsp;';
					}
				}
				
				if (!parent_title.length) {
					o.parent_title_content = '';
					o.parent_title_class = 'hidden';
					//dojo.removeClass(this.parentTitleNode, "parent_title");
					//dojo.addClass(this.parentTitleNode, "hidden");
				}
				
				//Project title
				/* #3061 */
				if (parent && !parent_is_project)
				{
					var tmp = parent,
						tmpItem;
					while(tmp)
					{
						tmpItem = hi.items.manager.get(tmp);
						if (tmpItem && parseInt(tmpItem.category) === 0){
							ancestor_is_project = true;
							parent_item = tmpItem;
							break;
						}
						tmp = tmpItem ? tmpItem.parent : tmpItem;
					}
				}
				
				if (parent && (parent_is_project||ancestor_is_project) && (group != 'project' || inTooltip)) {
					project_title = parent_item.title;
					if (project_title) {
						o.project_title_content = ' ' + htmlspecialchars(project_title);
						//Assign lightened color of project to icon
						o.project_color = colorLuminance(parent_item.color_value,60);
						o.project_title_class = '';
						//setTextContent(this.projectTitleNode, " " + project_title);
						//dojo.removeClass(this.projectTitleNode, "hidden");
					}
				}
				
				if (!project_title) {
					o.project_title_content = '';
					o.project_title_class = 'hidden';
					//dojo.addClass(this.projectTitleNode, "hidden");
				}
			},
			
			_getParticipantsAttr:function(item,value)
			{
				return item.taskItem.participants;
			},
			
			_setParticipantsAttr:function(item,value)
			{
				item.taskItem.participants = value;
			},
			
			_setParentAttr: function (item,value) {
				//this._set("parent", value);
				console.log('set parent attr',[item,value]);
				item.taskItem.parent = value;
				var group = Hi_Preferences.get('currentTab', 'my', "string"),
					isSearchActive = Hi_Search.isFormVisible(),
					tagFilter = Hi_Preferences.get("tagfilter", ""),
					parent = item.taskItem.parent,
					parent_item = parent ? hi.items.manager.get(parent) : null,
					parent_is_project = (parent_item && !parseInt(parent_item.category)),
					parent_title = [],
					project_title = "";
				
				//Parent title
				if (group == 'team' || group == 'date' || (!isSearchActive && tagFilter)) {
					if (parent_item && !parent_is_project) {
						//If parent is not project
						parent_title = parent_item.title;
					
						setTextContent(dojo.byId('parentTitleNode_'+item.widgetId), parent_title);
						dojo.addClass(dojo.byId('parentTitleNode_'+item.widgetId), "parent_title");
						dojo.removeClass(dojo.byId('parentTitleNode_'+item.widgetId), "hidden");
					}
				}
				
				if (!parent_title.length) {
					dojo.removeClass(dojo.byId('parentTitleNode_'+item.widgetId), "parent_title");
					dojo.addClass(dojo.byId('parentTitleNode_'+item.widgetId), "hidden");
				}
				
				//Project title
				if (parent && parent_is_project && group != 4) {
					project_title = parent_item.title;
					if (project_title) {
						setTextContent(dojo.byId('projectTitleNode_'+item.widgetId), " " + project_title);
						dojo.removeClass(dojo.byId('projectTitleNode_'+item.widgetId), "hidden");
					}
				}
				
				if (!project_title) {
					dojo.addClass(dojo.byId('projectTitleNode_'+item.widgetId), "hidden");
				}
			},
			
			/* Type*/
			_handleCategoryAttr: function (o,category) {
				this._handleTypeClassAttr(o,this._typeClassNames[parseInt(category)] || "");
			},
			
			_handleTypeClassAttr: function (o,typeClass) {
				o.type_class = typeClass;
				//dojo.removeClass(this.headingNode, this.typeClass);
				//dojo.addClass(this.headingNode, typeClass);
				//this._set("typeClass", typeClass);
			},
			
			/* Priority */
			//_prioritySetter:function(value) {this._setPriorityAttr(value);},
			_handlePriorityAttr: function (o,value) {
				value = value || 20000;
				//var hiddenClass = "hidden";
				//dojo.addClass(this.priorityHighNode, hiddenClass);
				//dojo.addClass(this.priorityLowNode, hiddenClass);
				if (value >= 30000) {
					//dojo.removeClass(this.priorityHighNode, hiddenClass);
					o['priority-high'] = 'priority-high';
					o['priority-low'] = 'hidden';
				} else if (value < 20000) {
					//dojo.removeClass(this.priorityLowNode, hiddenClass);
					o['priority-high'] = 'hidden';
					o['priority-low'] = 'priority-low';

				} else
				{
					o['priority-high'] = 'hidden';
					o['priority-low'] = 'hidden';
				}
				//this._set("priority", value);
			},
			
			/* Priority */
			_setPriorityAttr: function (item,value) {
				value = value || 20000;
				var hiddenClass = "hidden",
					priorityLowNode = dojo.byId('priorityLowNode_'+item.widgetId),
					priorityHighNode = dojo.byId('priorityHighNode_'+item.widgetId);
				dojo.addClass(priorityHighNode, hiddenClass);
				dojo.addClass(priorityLowNode, hiddenClass);
				
				if (value >= 30000) {
					dojo.removeClass(priorityHighNode, hiddenClass);
					dojo.addClass(priorityHighNode, 'priority-high');
				} else if (value < 20000) {
					dojo.removeClass(priorityLowNode, hiddenClass);
					dojo.addClass(priorityLowNode, 'priority-low');
				}
				
				//this._set("priority", value);
				item.taskItem.priority=value;
			},
			
			/* Title */
			_setTitleAttr: function(item, title){
				//dojo.byId('titleNode_'+item.widgetId).innerHTML = title;
				dojo.byId('titleNodeSpan_' + item.widgetId).innerHTML = title;
				item.taskItem.title = title;
			},

			_setTagsAttr: function(item, tags) {
				item.taskItem.tags = tags;
			},
			
			/* Permission */
			_setPermissionAttr: function(item, value){
				item.taskItem.permission = value;
				var itemProperties = item.propertiesView,
					editingView = item.editingView,
					completeCheck = dojo.byId('completeNode_' + item.widgetId);

				if(itemProperties){
					itemProperties.set('permission', value);
				}
				if(editingView){
					editingView.set('permission', value);
				}
				if(completeCheck){
					if(value < Hi_Permissions.COMPLETE_ASSIGN){
						completeCheck.setAttribute('disabled', 'true');
					}else{
						completeCheck.removeAttribute('disabled');
					}
				}
			},
			_setPermissionsAttr: function(item, value){
				item.taskItem.permissions = value;
				var itemProperties = item.propertiesView,
					editingView = item.editingView,
					completeCheck = dojo.byId('completeNode_' + item.widgetId);

				if(itemProperties){
					itemProperties.set('permissions', value);
				}
				if(editingView){
					editingView.set('permissions', value);
				}
				// if(completeCheck){
				// 	if(value < Hi_Permissions.COMPLETE_ASSIGN){
				// 		completeCheck.setAttribute('disabled', 'true');
				// 	}else{
				// 		completeCheck.removeAttribute('disabled');
				// 	}
				// }
			},
			//_titleSetter:function(title) {this._setTitleAttr(title);},
			_handleTitleAttr: function (o,title) {
				//this._set("title", title);
				
				title = htmlspecialchars(title);
				//this.titleNode.innerHTML = title;
				o.title_content = title;
			},
			
			_setMessageAttr:function(item,value){
				item.taskItem.message=value;
			},
			
			/* Starred */
			//_starredSetter:function(value) {this._setStarredAttr(value);},
			_handleStarredAttr: function (o,value) {
				//this._set("starred", value);
				this._handleStarredClassAttr(o,value ? "star-selected" : "");
			},
			_setStarredAttr: function (item,value) {
				//this._set("starred", value);
				item.taskItem.starred = value;
				this._setStarredClassAttr(item,value ? "star-selected" : "");
			},
				
			_setStarredClassAttr: function (item, starredClass) {
				var starNode = dojo.byId('starredNode_'+item.widgetId);
				if (!starNode) return;
				dojo.removeClass(starNode, 'star-selected');
				dojo.addClass(starNode, starredClass);
				//this._set("starredClass", starredClass);
			},
			
			//_starredClassSetter:function(starredClass) {this._setStarredClassAttr(starredClass);},
			_handleStarredClassAttr: function (o,starredClass) {
				//dojo.removeClass(this.starredNode, this.starredClass);
				//dojo.addClass(this.starredNode, starredClass);
				o.starred_class = starredClass;
				//this._set("starredClass", starredClass);
			},

			/* Event date */
			//_eventdateSetter:function(eventdate) {this._setEventdateAttr(eventdate);},
			_handleEventdateAttr: function (o, eventdate) {
				//this.completeNode.setAttribute("eventdate", eventdate);
				//this._set("eventdate", eventdate);
				o.eventdate = eventdate || '';
				if (!o.eventdate) return;

				eventdate = new Date(eventdate);
				eventdate = hi.items.date.truncateTime(eventdate);

				var today = hi.items.date.truncateTime(new Date());
				if (eventdate.getTime() !== today.getTime() && o.list.listName !== 'tooltip') {
					o.completeCheckDisabledClass = 'completeInCalendar';
				} else {
					o.completeCheckDisabledClass = '';
				}
			},
			
			/* Permission */
			_handlePermissionAttr: function (o, pm) {
				if(pm === dojo.global.Hi_Permissions.VIEW_COMMENT){
				// if(pm === dojo.global.Hi_Permissions.EVERYTHING){
					o.completeDisabled = 'disabled';
				}else{
					o.completeDisabled = '';
				}
			},
			
			/* Completed */
			_handleCompletedAttr: function (o,value,item) {
				if (parseInt(item.recurring) > 0) {
					//For recurring even check if instance is completed
					//value = hi.items.manager.getIsCompleted(item.taskId/*, item.eventdate*/);
				}
				
				//this._set("completed", value);
				if (value)
					o.completed_class = 'completed';
				else
					o.completed_class  = '';
				
				//o['checked'] = !!value;
				if (value)
					o.checked = "checked=true";
				else
					o.checked = '';
				//dojo.toggleClass(this.itemNode, "completed", value);
				//this.completeNode.checked = !!value;
				
				// Drag and drop
				if (value && (!item.container || !(item.container instanceof hi.widget.noDijit_TaskItem && !this.container.completed))) {
					//dojo.removeClass(this.itemNode, "dojoDndItem");
					o.item_dnd = '';
				} else {
					//dojo.addClass(this.itemNode, "dojoDndItem");
					o.item_dnd = 'dojoDndItem';
				}
			},
			
			_setEventdateAttr: function (item,eventdate) {
				dojo.byId('completeNode_'+item.widgetId).setAttribute("eventdate", eventdate);
				item.taskItem.eventdate = eventdate;
			},
			
			_setCompletedAttr: function (item,value) {
				if (parseInt(item.taskItem.recurring) > 0) {
					//For recurring even check if instance is completed
					value = hi.items.manager.getIsCompleted(item.taskId, item.taskItem.eventdate);
				}
				
				//this._set("completed", value);
				item.taskItem.completed = value;
				
				dojo.toggleClass(dojo.byId('itemNode_' + item.widgetId), "completed", value);
				var completeNode = dojo.byId('completeNode_' + item.widgetId);
				if (completeNode) {
					completeNode.checked = !!value;
				}
				// Drag and drop
				/* TODO Dnd
				if (value && (!item.container || !(item.container instanceof hi.widget.noDijit_TaskItem && !this.container.completed))) {
					dojo.removeClass(this.itemNode, "dojoDndItem");
				} else {
					dojo.addClass(this.itemNode, "dojoDndItem");
				}
				*/
			},
			
			_setRecurringAttr:function(item, value) {
				item.taskItem.recurring=value;
			},
			
			_setRecurring_intervalAttr:function(item, value) {
				item.taskItem.recurring_interval=value;
			},

			_setRecurring_end_dateAttr:function(item,value)
			{
				item.taskItem.recurring_end_date = value;
			},

			/* Admin classname */
			/*
			//adminClass: Project.isBusinessAdministrator() ? "admin" : "",
			_adminClassSetter:function(adminClass) {this._setAdminClassAttr(adminClass);},
			_setAdminClassAttr: function (adminClass) {
				dojo.removeClass(this.itemNode, this.adminClass);
				dojo.addClass(this.itemNode, adminClass);
				this._set("adminClass", adminClass);
			},
			*/
			
			/* Shared classname */
			_handleSharedAttr: function (o,shared) {
				// #2205
				if (typeof(shared) != 'boolean') {
					shared = parseInt(shared);
					shared = !isNaN(shared) && shared > 0 ? true : false;
				}
				
				/* Bug #1956 shared = parseInt(shared, 10) || 0;*/
				//this._set("shared", shared);
				
				if (shared) {
					/* Bug #1956
					dojo.addClass(this.itemNode, "shared");
					dojo.removeClass(this.itemNode, "private");
					*/
					o['private_mark_class'] = 'hidden';
				} /*Bug #1956 else if (Project.account_level < Project.ACCOUNT_LEVEL_BUSINESS && !Project.businessId) {
					dojo.removeClass(this.itemNode, "shared");
					dojo.addClass(this.itemNode, "private");
				} */
				else o['private_mark_class'] = '';//dojo.removeClass(this.privateMarkNode,'hidden');
			},
			
			_handleSharedClassAttr: function (o, sharedClass) {
				//dojo.removeClass(this.itemNode, this.sharedClass);
				//dojo.addClass(this.itemNode, sharedClass);
				//this._set("sharedClass", sharedClass);
				o['shared_class'] = sharedClass || '';
			},
			
			_setSharedAttr: function (item,shared) {
				// #2205
				if (typeof(shared) != 'boolean') {
					shared = parseInt(shared);
					shared = !isNaN(shared) && shared > 0 ? true : false;
				}
				
				/* Bug #1956 shared = parseInt(shared, 10) || 0;*/
				//this._set("shared", shared);
				item.taskItem.shared=shared;
				
				if (shared) {
					/* Bug #1956
					dojo.addClass(this.itemNode, "shared");
					dojo.removeClass(this.itemNode, "private");
					*/
				} /*Bug #1956 else if (Project.account_level < Project.ACCOUNT_LEVEL_BUSINESS && !Project.businessId) {
					dojo.removeClass(this.itemNode, "shared");
					dojo.addClass(this.itemNode, "private");
				} */
			},
			
			_setSharedClassAttr: function (sharedClass) {
				dojo.removeClass(this.itemNode, this.sharedClass);
				dojo.addClass(this.itemNode, sharedClass);
				this._set("sharedClass", sharedClass);
			},
			
			/* User id*/
			//user_id: "",
			//_user_idSetter:function (user_id) {this._setUser_idAttr(user_id);},
			_handleUser_idAttr: function (o,user_id) {
				//this._set("user_id", user_id);
				//this.set("ownedClass", user_id == Project.user_id ? "owner" : "");
				this._handleOwnerClassAttr(o,user_id == Project.user_id ? "owner" : "");
				//if (user_id == Project.user_id) this.set("my", true);
			},
			
			//ownedClass: "",
			//_ownedClassSetter:function (ownedClass) {this._setOwnerClassAttr(ownedClass);},
			_handleOwnerClassAttr: function (o, ownedClass) {
				//dojo.removeClass(this.itemNode, this.ownedClass);
				//dojo.addClass(this.itemNode, ownedClass);
				//this._set("sharedClass", ownedClass);
				o['owner_class'] = ownedClass || '';
			},
			
			/* Business */
			//businessClass: (Project.businessId ? "business" : ""),
			//_businessClassSetter:function (businessClass) {this._setBusinessClassAttr(businessClass);},
			_handleBusinessClassAttr: function (o, businessClass) {
				o['business_class'] = businessClass || '';
				//dojo.removeClass(this.itemNode, this.businessClass);
				//dojo.addClass(this.itemNode, businessClass);
				//this._set("businessClass", businessClass);
			},
			
			/* Color */
			//color: "",
			//_colorSetter:function (color) {this._setColorAttr(color);},
			_handleColorAttr: function (o,color) {
				//this._set("color", color);
				//this.set("colorClass", color ? "color" + color : "");
				this._handleColorClassAttr(o,color ? "color" + color : "");
			},
			
			_handleColorClassAttr: function (o,colorClass) {
				//dojo.removeClass(this.completeClassNode, this.colorClass);
				//dojo.addClass(this.completeClassNode, colorClass);
				//this._set("colorClass", colorClass);
				o['color_class'] = colorClass;
			},

			_setColorAttr: function (item,color) {
				item.taskItem.color = color;//this._set("color", color);
				//this.set("colorClass", color ? "color" + color : "");
				this._setColorClassAttr(item,color ? "color" + color : "");
			},
			
			_setColorClassAttr: function (item,colorClass) {
				var completeClassNode = dojo.byId('completeClassNode_'+item.widgetId);
				dojo.removeClass(completeClassNode, item.taskItem.colorClass);
				dojo.addClass(completeClassNode, colorClass);
				//this._set("colorClass", colorClass);
				item.taskItem.colorClass=colorClass;
			},
			
			/* Children */
			//childrenRendered: false,
			//itemWidgets: null,
			
			//children: 0,
			_handleChildrenAttr: function (o,children,item,list) {
				children = parseInt(children, 10) || 0;
				//this._set("children", children);
				//this.set("childrenClass", children && this.hasNonFileChildren() ? "has-children" : "");
				this._handleChildrenClassAttr(o, children && this.hasNonFileChildren(item,list) ? "has-children" : "");
			},
			
			_handleChildrenClassAttr: function (o, childrenClass) {
				//dojo.removeClass(this.itemNode, this.childrenClass);
				//dojo.addClass(this.itemNode, childrenClass);
				//this._set("childrenClass", childrenClass);
				o['children_class'] = childrenClass || '';
			},
			
			/* Children setter not tested until do Dnd */
			_setChildrenAttr: function (item,children) {
				children = typeof(children) == 'boolean' && children ? 1 : (parseInt(children, 10) || 0);
				item.taskItem.children = children;
				this._setChildrenClassAttr(item,children && this.hasNonFileChildren(item,item.list) ? "has-children" : "");
			},
			
			_setChildrenClassAttr: function (item,childrenClass) {
				var itemNode = dojo.byId('itemNode_'+item.widgetId);
				item.taskItem.childrenClass = childrenClass;
				if (!itemNode) return;
				dojo.removeClass(itemNode, /*item.taskItem.childrenClass*/'has-children');
				dojo.addClass(itemNode, childrenClass);
				//this._set("childrenClass", childrenClass);
				//item.taskItem.childrenClass = childrenClass;
			},
			
			/* Dragable */
			_handleDragVisibleClassAttr: function (o,dragVisibleClass) {
				//dojo.removeClass(this.dragHandle, this.dragVisibleClass);
				//dojo.addClass(this.dragHandle, dragVisibleClass);
				//this._set("dragVisibleClass", dragVisibleClass);
				o['drag_visible_class'] = dragVisibleClass;
			},

			/* Cannot test until do Dnd */
			_setDragVisibleClassAttr: function (item,dragVisibleClass) {
				var dragHandle = dojo.byId('dragHandle_'+item.widgetId);
				if (item.taskItem.dragVisibleClass) dojo.removeClass(dragHandle, this.dragVisibleClass);
				dojo.addClass(dragHandle, dragVisibleClass);
				//this._set("dragVisibleClass", dragVisibleClass);
				item.taskItem.dragVisibleClass = dragVisibleClass;
			},
			
			/* Date */
			end_date: "",
			end_time: "",
			start_date: "",
			start_time: "",
			due_date: "",

			_handleDue_dateAttr: function (o, due_date, item, list) {
				this._handleDateTitleAttr(o, Math.random(), item, list);
			},

			_handleEnd_dateAttr: function (o, end_date, item, list) {
				return;
				/* Bug #2132 - the UTC format is dddd-dd-ddTdd:dd:dd.ddd-dd:dd, where d is a digit
				if (end_date && !end_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[\d\+:]+$/)) {
				*/
				if (end_date && !end_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}-[0-9]{2}:[0-9]{2}$/)) {
					// #2180
					//var time = this.get("end_time");
					//if (this.is_all_day || !time) {
					//	end_date = time2strAPI(HiTaskCalendar.getDateFromStr(end_date));
					//} else {
						end_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(end_date, time2str(str2timeAPI(end_date), 'h:i')));
					//}
				}
				
				//this._set("end_date", end_date);
				//this.set("dateTitle", Math.random());
				this._handleDateTitleAttr(o, Math.random(), item, list);
			},
			
			_getEnd_dateAttr: function (item) {
				
				var value = item.taskItem ? item.taskItem.end_date:item.end_date;
				if (value) value = time2str(str2time(value, "y-m-d"), Hi_Calendar.format);
				return value || "";
			},
			
			_setEnd_dateAttr: function (item,end_date) {
				/* Bug #2132 - the UTC format is dddd-dd-ddTdd:dd:dd.ddd-dd:dd, where d is a digit
				if (end_date && !end_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[\d\+:]+$/)) {
				*/
				if (end_date && !end_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}-[0-9]{2}:[0-9]{2}$/)) {
					// #2180
					//var time = this.get("end_time");
					//if (this.is_all_day || !time) {
					//	end_date = time2strAPI(HiTaskCalendar.getDateFromStr(end_date));
					//} else {
						end_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(end_date, time2str(str2timeAPI(end_date), 'h:i')));
					//}
				}
				
				//this._set("end_date", end_date);
				item.taskItem.end_date = end_date;
				this._setDateTitleAttr(item,Math.random());
			},
			
			//_end_timeSetter:function (end_time) {this._setEnd_timeAttr(end_time);},
			_handleEnd_timeAttr: function (o,end_time,item) {
				var end_date = this._getEnd_dateAttr(item);
				if (end_date) {
					end_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(end_date, end_time));
				} else {
					end_date = time2strAPI(HiTaskCalendar.getTimeFromStr(end_time));
				}
				
				//this.set("end_date", end_date);
				//this._set("end_time", end_time);
				this._handleEnd_dateAttr(o,end_time,item);
			},
			
			_setEnd_timeAttr: function (item,end_time) {
				var end_date = this._getEnd_dateAttr(item);
				if (end_date) {
					end_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(end_date, end_time));
				} else {
					end_date = time2strAPI(HiTaskCalendar.getTimeFromStr(end_time));
				}
				
				this._setEnd_dateAttr(item,end_date);
				item.taskItem.end_time = end_time;
			},
			
			//_start_dateSetter:function (start_date) {this._setStart_dateAttr(start_date);},
			_handleStart_dateAttr: function (o, start_date, item, list) {
				/* Bug #2132 - the UTC format is dddd-dd-ddTdd:dd:dd.ddd-dd:dd, where d is a digit
				 * 
				if (start_date && !start_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[\d\+:]+$/)) {
				*/
				if (start_date && !start_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}-[0-9]{2}:[0-9]{2}$/)) {
					// #2180
					//var time = this.get("start_time");
					//if (this.is_all_day || !time) {
					//	start_date = time2strAPI(HiTaskCalendar.getDateFromStr(start_date));
					//} else {
						start_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(start_date, time2str(str2timeAPI(start_date), 'h:i')));
					//}
				}
				
				//this._set("start_date", start_date);
				//this.set("dateTitle", Math.random());
				this._handleDateTitleAttr(o, Math.random(), item, list);
			},
			
			_getStart_dateAttr: function (item) {
				var value = item.taskItem.start_date;
				if (value) value = time2str(str2time(value, "y-m-d"), Hi_Calendar.format);
				return value || "";
			},
			
			_setStart_dateAttr: function (item, start_date) {
				/* Bug #2132 - the UTC format is dddd-dd-ddTdd:dd:dd.ddd-dd:dd, where d is a digit
				 * 
				if (start_date && !start_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[\d\+:]+$/)) {
				*/
				if (start_date && !start_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}-[0-9]{2}:[0-9]{2}$/)) {
					// #2180
					//var time = this.get("start_time");
					//if (this.is_all_day || !time) {
					//	start_date = time2strAPI(HiTaskCalendar.getDateFromStr(start_date));
					//} else {
						start_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(start_date, time2str(str2timeAPI(start_date), 'h:i')));
					//}
				}
				
				item.taskItem.start_date = start_date;//this._set("start_date", start_date);
				this._setDateTitleAttr(item,Math.random());
			},
			
			//_start_timeSetter:function (start_time) {this._setStart_timeAttr(start_time);},
			_handleStart_timeAttr: function (o, start_time,item) {
				var start_date = this.get("start_date");
				if (start_date) {
					start_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(start_date, start_time));
				} else {
					start_date = time2strAPI(HiTaskCalendar.getTimeFromStr(start_time));
				}
				
				//this.set("start_date", start_date);
				this._handleStart_dateAttr(o, start_date, item);
				//this._set("start_time", start_time);
			},
			
			_setStart_timeAttr: function (item, start_time) {
				var start_date = this._getStart_dateAttr(item);
				if (start_date) {
					start_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(start_date, start_time));
				} else {
					start_date = time2strAPI(HiTaskCalendar.getTimeFromStr(start_time));
				}
				
				this._setStart_dateAtt(item,start_date);
				item.taskItem.start_tiem = start_time;//this._set("start_time", start_time);
			},
			
			_getEnd_timeAttr: function (item) {
				var value = this._getEnd_dateAttr(item);
				
				if (item.taskItem.is_all_day) return "";
				
				if (value) value = str2timeAPI(item.taskItem.end_date);
				if (value) value = time2str(value, "h:i");
				return value || "";
			},
			
			_getStart_timeAttr: function (item) {
				var value = this._getStart_dateAttr(item);
				
				if (item.taskItem.is_all_day) return "";
				
				if (value) value = str2timeAPI(item.taskItem.start_date);
				if (value) value = time2str(value, "h:i");
				return value || "";
			},
			
			//_dateTitleSetter:function() {this._setDateTitleAttr();},
			_handleDateTitleAttr: function (o, value, item, list) {
				//this._set("dateTitle", "");
				var title = "";
				if (item.recurring && item.eventdate) {
					title = getTaskInstanceDateTitle(item.taskId, item.eventdate, list);
				} else {
					title = getTaskDateTitle(item.taskId, new Date(), item);
				}
				
				//setTextContent(this.dateTitleNode, title);
				o['date_title_content'] = title;
				if (title) {
					o['date_title_class'] = '';
				}
			},
			
			_setDateTitleAttr: function (item) {
				item.taskItem.dateTitle = '';
				
				var title = "";
				if (item.taskItem.recurring && item.taskItem.eventdate) {
					title = getTaskInstanceDateTitle(item.taskId, item.taskItem.eventdate);
				} else {
					title = getTaskDateTitle(item.taskId, new Date(), this.getData(item));
				}
				
				var node = dojo.byId('dateTitleNode_'+item.widgetId);
				setTextContent(node, title);
				dojo.removeClass(node, 'hidden');
			},
			
			_getAvatar: function(personId) {
				return dojo.global.getAvatar(32, personId);
			},
			
			/* Assignee */
			_handleAssigneeAttr: function (o,value,item) {
				//this._set("assignee", value);
				
				var person_name = null;
				
				if (value) {
					var isChild = Project.isTaskChild(item.taskId),
						group = Hi_Preferences.get('currentTab', 'my', "string");
					if (group != 'team' /* #3061 || isChild*/) {
						person_name = Project.getFriendName(value, 3);
					}
				}
				
				if (person_name) {
					//dojo.removeClass(this.personNode, "hidden");
					//setTextContent(this.personNode, person_name);
					o['assignee_class'] = '';
					o['person_content'] = htmlspecialchars(person_name);
				} else {
					//dojo.addClass(this.personNode, "hidden");
					o['assignee_class'] = 'hidden';
				}
				
				o['assignee_avatar'] = this._getAvatar(value);
			},
			
			_setAssigneeAttr: function (item,value) {
				item.taskItem.assignee = value;//this._set("assignee", value);
				
				var person_name = null,
					personNode = dojo.byId('personNode_'+item.widgetId),
					personNodeImg = dojo.byId('personNodeImg_'+item.widgetId),
					personNodeSpan = dojo.byId('personNodeSpan_'+item.widgetId);
				
				if (value) {
					var isChild = Project.isTaskChild(item.taskId),
						group = Hi_Preferences.get('currentTab', 'my', "string");
						
					if (group != 'team' || isChild) {
						person_name = Project.getFriendName(value, 3);
					}
				}
				
				if (person_name) {
					dojo.removeClass(personNode, "hidden");
					setTextContent(personNodeSpan, person_name);
					dojo.attr(personNodeSpan, 'title', htmlspecialchars(person_name));
					personNodeImg.src = this._getAvatar(value);
				} else {
					dojo.addClass(personNode, "hidden");
				}
				
			},
			
			/* Recurring event instance dates */
			//instances: null,
			//_instancesSetter:function (instances) {this._setInstancesAttr(instances);},
			_handleInstancesAttr: function (o,instances,item) {
				//this._set("instances", instances);
				
				if (instances && instances.length) {
					if (item.recurring != 0) {
						if (!item.eventdate) {
							//Find first not deleted item
							for (var i=0,ii=instances.length; i<ii; i++) {
								if (instances[i].status != 2) {
									//this.set("eventdate", instances[i].start_date);
									this._handleEventdateAttr(o, instances[i].start_date,item);
									break;
								}
							}
						}
						
						var completed = hi.items.manager.getIsCompleted(item.taskId, item.eventdate);
						
						if (item.completed && !completed) {
							//this.set("completed", false);
							this._handleCompletedAttr(o,false,item);
						} else if (!this.completed && completed) {
							//this.set("completed", true);
							this._handleCompletedAttr(o,true,item);
						}
					} else {
						if (item.eventdate) {
							//this.set("eventdate", null);
							this._handleEventdateAttr(o, null,item);
						}
					}
				}
			},
			
			/* TODO:NOT SURE HOW TO TEST */
			_setInstancesAttr: function (item, instances) {
				item.taskItem.instances = instances;//this._set("instances", instances);
				
				if (instances && instances.length) {
					if (item.taskItem.recurring != 0) {
						if (!item.taskItem.eventdate) {
							//Find first not deleted item
							for (var i=0,ii=instances.length; i<ii; i++) {
								if (instances[i].status != 2) {
									this._setEventdateAttr(item,instances[i].start_date);
									break;
								}
							}
						}
						
						var completed = hi.items.manager.getIsCompleted(item.taskId, item.taskItem.eventdate);
						
						if (item.taskItem.completed && !completed) {
							this._setCompletedAttr(item, false);
						} else if (!item.taskItem.completed && completed) {
							this._setCompletedAttr(item, true);
						}
					} else {
						if (item.taskItem.eventdate) {
							this._setEventdateAttr(item, null);
						}
					}
				}
			},
			
			/* Overdue / current */
			//styleOverdue: false,
			//styleCurrent: false,
			//_styleOverdueSetter:function (overdue) {this._setStyleOverdueAttr(overdue);},
			_handleStyleOverdueAttr: function (o, overdue) {
				//this._set("styleOverdue", overdue);
				//dojo.toggleClass(this.domNode, "overdue", overdue);
				if (overdue) o['overdue_class'] = 'overdue';
				else o['overdue_class'] = '';
			},
			//_styleCurrentSetter:function (current) {this._setStyleCurrentAttr(current);},
			_handleStyleCurrentAttr: function (o,current) {
				//this._set("styleCurrent", current);
				//dojo.toggleClass(this.domNode, "current", current);
				if (current) o['current_class'] = 'current';
				else o['current_class'] = '';
			},
			
			/* Overdue / current */
			_setStyleOverdueAttr: function (item,overdue) {
				//this._set("styleOverdue", overdue);
				dojo.toggleClass(dojo.byId('itemNode_'+item.widgetId), "overdue", overdue);
			},
			
			_setStyleCurrentAttr: function (item,current) {
				//this._set("styleCurrent", current);
				dojo.toggleClass(dojo.byId('itemNode_'+item.widgetId), "current", current);
			},
			
			checkOverdue: function (item) {
				// summary:
				//		Check if task is overdue and adjust style
				/* 
				 * #2538 (after ticket was closed) To check overdue:
				 * 1) if end_date, truncate to midnight, truncate today to midnight, compare times
				 * 2) else do the same thing to start_date
				 * 3) else it is not overdue because it does not have specified dates
				 */
				var is_overdue=false,
					is_current = Hi_Overdue.isActive(item.taskId),
					taskItem = item.taskItem,
					eventdate;
				
				if (taskItem.recurring) {
					if (taskItem.eventdate)
						eventdate = taskItem.eventdate;
					else
						eventdate = dojo.dom.getAllAttributes(dojo.byId('completeNode_' + item.widgetId))['eventdate'] || time2str(new Date(),'y-m-d');
					
					if (!eventdate) return;
					if (eventdate == 'undefined') return;
					if (eventdate == 'null') return;
				}

				if (taskItem.end_date && taskItem.end_date != '') {
					/* #2797 */
					var eD;
					//taskItem.recurring?eD=hi.items.date.getEventDate(taskItem.id):taskItem.end_date;
					if (taskItem.recurring) {
						eD = eventdate;
					} else {
						eD = taskItem.end_date;
					}
					if ( str2timeAPI(eD).getTime() < hi.items.date.truncateTime(new Date()).getTime() ) is_overdue = true;
				} else if (taskItem.start_date && taskItem.start_date != '') {
					/* #2797 */
					var sD;
					if (taskItem.recurring) {
						sD = eventdate;
					} else {
						sD = taskItem.start_date;
					}
					if ( str2timeAPI(sD).getTime() < hi.items.date.truncateTime(new Date()).getTime() ) is_overdue = true;
				}
				
				if (is_overdue) {
					//this.set("styleOverdue", true);
					//this.set("styleCurrent", false);
					this._setStyleOverdueAttr(item, true);
					this._setStyleCurrentAttr(item,false);
				} else if (is_current) {
					//this.set("styleOverdue", false);
					//this.set("styleCurrent", true);
					this._setStyleOverdueAttr(item, false);
					this._setStyleCurrentAttr(item,true);
				} else {
					//this.set("styleOverdue", false);
					//this.set("styleCurrent", false);
					this._setStyleOverdueAttr(item, false);
					this._setStyleCurrentAttr(item,false);
				}
			},
			
			/* Obsolete and replaced by checkOverdue() */
			_checkOverdue: function (item) {
				// summary:
				//		Check if task is overdue and adjust style
				
				var id = item.taskId,
					is_overdue = false,
					is_current = Hi_Overdue.isActive(id);
				
				if (Hi_Overdue.tillActive(id) <= 0) {
					var overdueTime = Hi_Overdue.overdueTime(id);
					if (overdueTime < 0) {
						// 2538: so if date is today and start_time is 1 hour in the past it's not overdue.
						/* #2538 - Hi_Overdue.nextEvent is ill-defined for non-recurring items? - check its validity before using */
						var nextOverDueEvent = Hi_Overdue.nextEvent(id);
						var millisFromMidnight;
						if (nextOverDueEvent && nextOverDueEvent.length && nextOverDueEvent[1])
						{
							millisFromMidnight = Hi_Overdue.nextEvent(id)[1] - HiTaskCalendar.todayMidnight;
							
							if (Math.abs(overdueTime) > millisFromMidnight) {
								is_overdue = true;
							}
						}

					}
				}
				
				if (is_overdue) {
					//this.set("styleOverdue", true);
					//this.set("styleCurrent", false);
					this._setStyleOverdueAttr(item, true);
					this._setStyleCurrentAttr(item,false);
				} else if (is_current) {
					//this.set("styleOverdue", false);
					//this.set("styleCurrent", true);
					this._setStyleOverdueAttr(item, false);
					this._setStyleCurrentAttr(item,true);
				} else {
					//this.set("styleOverdue", false);
					//this.set("styleCurrent", false);
					this._setStyleOverdueAttr(item, false);
					this._setStyleCurrentAttr(item,false);
				}
			},
			
			/* Locale */
			localeClickToView: hi.i18n.getLocalization("project.click_to_view"),
			localeClickToComplete: hi.i18n.getLocalization("project.click_to_complete"),

			/*
			 * The HTML templated filled in by the createItem() method
			 */
			template:[
					'<li id="itemNode_{widgetId}" class="task_li {item_dnd} {completed_class} {owner_class} {children_class} {has_non_project_parent_class}"',
						//'data-dojo-attach-event="onclick: handleItemClick" onmouseover="hoverElement(this)" onmouseout="houtElement(this)"',
						'title="{localeClickToView}" ',
						'data-task-id="{taskId}" ',
						'onclick="dojo.global.handleItemClick({taskId},\'{eventdate}\',event);" ',
						'onmouseover="dojo.global.handleItemMouseOver(event);" onmouseout="dojo.global.handleItemMouseOut(event);">',

							'<a class="toggle-children" id="toggleChildrenNode_{widgetId}" onmousedown="dojo.global.toggleChildren({taskId},\'{eventdate}\',event);">',
								'<span data-icon="&#xf0da;" aria-hidden="true" class="toggle-children-expand"></span>',
								'<span data-icon="&#xf0d7;" aria-hidden="true" class="toggle-children-collapse"></span>',
							'</a>',
							'<div class="task_div">',
								'<div ondblclick="dojo.global.handleItemDoubleClick({taskId},\'{eventdate}\',event);" class="heading {type_class}" id="headingNode_{widgetId}" onselectstart="return false;" onclick="dojo.global.handleExpandClick({taskId},\'{eventdate}\',event);" >',
								'<div class="check {color_class}" id="completeClassNode_{widgetId}">',
									'<input id="completeNode_{widgetId}" {checked} eventdate="{eventdate}" type="checkbox" title="{localeClickToComplete}" onclick="dojo.global.handleClickComplete({taskId},\'{eventdate}\',event);" ',
									'onmouseout="dojo.global.handleMouseOutComplete(event)" onmouseover="dojo.global.handleMouseOverComplete(event)" class="{completeCheckDisabledClass}" />',
								'</div>',
								'<div class="star {starred_class}" title="Click to mark item starred" id="starredNode_{widgetId}" onclick="dojo.global.handleClickStarred({taskId},\'{eventdate}\',event);"',
								'onmouseout="dojo.global.handleMouseOutStarred(event)" onmouseover="dojo.global.handleMouseOverStarred(event)"></div>',

								'<h3><div><div class="heading-inner">',
									'<div id="priorityHighNode_{widgetId}" class="{priority-high} "><span data-icon="&#xe090;" aria-hidden="true"></span></div>',
									'<div id="priorityLowNode_{widgetId}" class="{priority-low}"><span data-icon="&#xe096;" aria-hidden="true"></span></div>',
									
									'<div class="task_title title highlight forceWrap" id="titleNode_{widgetId}">',
										'<span class="{parent_title_class}" id="parentTitleNode_{widgetId}"><span id="parentTitleNodeSpan_{widgetId}" class="task_title_parent_wrapper">{parent_title_content}</span>&nbsp;<span data-icon="" aria-hidden="true"></span></span><span id="titleNodeSpan_{widgetId}">{title_content}</span>',
									'</div>',
									
									'<div class="in_project {project_title_class}" id="projectTitleNode_{widgetId}"><span data-icon="" aria-hidden="true" id="projectTitleIconNode_{widgetId}" style="color:{project_color}"></span><span title="{project_title_content}" id="projectTitleTextNode_{widgetId}"> {project_title_content}</span></div>',
									'<div class="with_assignee {assignee_class}" id="personNode_{widgetId}"><img id="personNodeImg_{widgetId}" src="{assignee_avatar}" width="16" height="16" />&nbsp;<span title="{person_content}" id="personNodeSpan_{widgetId}">{person_content}</span></div>',
									'<div class="date_title {date_title_class}" id="dateTitleNode_{widgetId}" title="{date_title_content}">{date_title_content}</div>',
								'<div class="dojoDndHandle btn {drag_visible_class} drag" id="dragHandle_{widgetId}"></div>',
								'<div class="btn collapse" data-icon="&#xe000;" id="btn_collapseNode_{widgetId}" onclick="dojo.global.handleCollapseClick({taskId},\'{eventdate}\',event);"></div>',
								'</div></div></h3>',
							
							'</div>',
						
							'<div  id="propertiesViewNode_{widgetId}" onkeydown="dojo.global.handleKeyPress({taskId},\'{eventdate}\',event);">',
								'<div class="opened hidden" style="padding-bottom: 0;"></div>',
							'</div>',
						
						'</div>',
					
						'<ul class="children" id="children_{widgetId}" data-task-id="{taskId}" ></ul>',
				'</li>'
				].join(''),
				
			ID_COUNTER:-2,

			CATEGORY_PROJECT:0,
			CATEGORY_TASK:1,
			CATEGORY_EVENT:2,
			CATEGORY_NOTE:4,
			CATEGORY_FILE:5,
			
			isValidCategory: function(c) {
				return [this.CATEGORY_PROJECT, this.CATEGORY_TASK, this.CATEGORY_EVENT, this.CATEGORY_NOTE, this.CATEGORY_FILE].indexOf(parseInt(c)) != -1;
			},

			/* Use this to tell TaskList observer what properties to watch for changes on */
			_propertiesHash:{
				'color':1,
				'title':1,
				'category':1,
				"message":1,
				"parent":1,
				"time_last_update":1,
				"time_create":1,
				"priority":1,
				"permission": 1,
				"permissions": 1,
				"time_spent":1,
				"time_track":1,
				"time_est":1,
				"changed":1,
				"starred":1,
				"tags":1,
				"assignee":1,
				"start_date":1,
				"start_time":1,
				"end_date":1,
				"end_time":1,
				"is_all_day":1,
				"next_event_date":1,
				"children":1,
				"recurring":1,
				'recurring_interval':1,
				'recurring_end_date':1,
				"reminder_enabled":1,
				"reminder_time":1, 
				"reminder_time_type":1,
				"completed":1,
				"participants:":1,
				'shared':1

			},
			_properties: [
				"id", "guid",
				"user_id", "shared", "my",
				"title",
				"category",
				"message",
				"parent",
				"time_last_update",
				"time_create",
				"priority",
				"time_spent",
				"time_track", "time_est",
				"changed",
				"starred",
				"tags",
				"assignee",
				"start_date", "start_time", "end_date", "end_time", "is_all_day", "next_event_date",
				"children",
				"recurring",
				"reminder_enabled", "reminder_time", "reminder_time_type",
				"color",
				"completed",
				"participants"
			],

			_typeClassNames: {
				1: "tasks",
				2: "event",
				3: "event",
				4: "note",
				5: "file"
			},

			get: function(/* String */ attr) {
				var Attr = attr.substr(0,1).toUpperCase()+attr.substr(1);
				if (this['_get'+Attr+'Attr']) return this['_get'+Attr+'Attr']();
				else if (this[attr] != undefined) return this[attr];
				else return undefined;
			},
			
			set: function(item, attr, value) {
				//if (attr in this) this[attr] = value;
				var Attr = attr.substr(0,1).toUpperCase() + attr.substr(1);
					func = this['_set' + Attr + 'Attr'];

				if (func) func.apply(this, [item, value]);
				//else this._set(attr,value);
			},
			
			getData: function (item) {
				// summary:
				//		Returns task data
				
				var props = this._properties,
					prop = null,
					i = 0,
					ii = props.length,
					data = {};
				
				for (i = 0, ii = props.length; i < ii; i++) {
					prop = props[i];
					if (prop == "id") {
						data[prop] = item.taskId;
					} else {
						data[prop] = item.taskItem[prop];//this.get(prop);
					}
				}
				
				return data;
			},
			
			/*
			 * Notify all items in hi.items.manager._items that the item with this ID has
			 * been modified
			 */
			_save: function (task,properties) {
				if (!task) return;
				var taskId = typeof task === "object" ? task.taskId : task;
				
				//for (var i in properties) task.set(i,properties[i]);
				var items = hi.items.manager._items[taskId];
				
				for (var i in items) {
					for (var j in properties) {
						if (items[i].propertiesView && items[i].propertiesView.task) {
							items[i].propertiesView.task.set(j, properties[j]);
						}
					}
				}
			},
			
			save: function (item,properties) {
				// summary:
				//		Save item properties, write to store
				// properties: {Object}
				//		Properties which will be written
				var self = this;
				var deferred = hi.items.manager.setSave(item.taskId||item.id, properties);
				deferred.then(function() {
					/* Bug #2042 - redraw calendar and toggle completed class on taskedit */
					HiTaskCalendar.update();
					/* Close widget, unless we clicked complete on calendar without opening a tooltip */
					if (!properties.withoutWidget) {
						self.closeEdit(item.taskId || item.id, item.eventdate || null, true);
					}
					return;
					/* #2905
					if (!dojo.byId('itemNode_'+item.widgetId)) return;
					if (!properties['withoutWidget']) {
						if (properties.completed) 
						{
							dojo.addClass(dojo.byId('itemNode_'+item.widgetId),'completed');
						} else dojo.removeClass(dojo.byId('itemNode_'+item.widgetId),'completed')
					}
					*/
					
				});
				return deferred;
			},

			/* #2293 */
			_doCompare:function(itemWidgets,toAddWidget,addAt,sortOptions,index,debug)
			{
				var addAtItem = itemWidgets[dojo.dom.getTaskId(addAt)].taskItem,
					toAddItem = itemWidgets[toAddWidget.taskId].taskItem,
					SOLength = sortOptions.length,
					addAtValue,
					toAddValue;

				if (typeof(sortOptions) == 'function') {
					return sortOptions(addAtItem, toAddItem) == -1;
				} else {
					for (var i = 0;i<SOLength;i++)
					{
						addAtValue = addAtItem[sortOptions[i].attribute];
						toAddValue = toAddItem[sortOptions[i].attribute];
						if (sortOptions[i].caseInsensitive)
						{
							addAtValue = addAtValue.toUpperCase();
							toAddValue = toAddValue.toUpperCase();
						}
						if (addAtValue != toAddValue)
						{
							if (!addAtValue) return false;
							if (!toAddValue) return true;
							if (sortOptions[i].descending)
							{
								if (toAddValue < addAtValue) return true;
								else return false;
							} else
							{
								if (toAddValue > addAtValue) return true;
								else return false;
							}
						}
					}
				}
				return true;
			},
			
			/* #2293 */
			_add: function(itemWidgets,widget,itemDOM,placeAt) {
				//var parentItem = widget.container;
				var sortAttribute = 'title',
					sortOrder = 'Ascending',
					insertAt = 0,
					sortConfiguration = hi.store.Project.getOrderConfiguration(true)[0],
					sortOptions = hi.store.Project.getOrderConfiguration(true),
					childrenNodes = placeAt.childNodes,
					end = childrenNodes.length,
					nChildren = childrenNodes.length;
					
				sortAttribute = sortConfiguration['attribute'];

				if (sortConfiguration['descending']) sortOrder = 'Descending';

				while(insertAt < end){
					if (this._doCompare(itemWidgets,widget,childrenNodes[insertAt],sortOptions,insertAt)) {
						insertAt++;
					} else {
						break;
					}
				}
				dojo.place(itemDOM,placeAt,insertAt);
			},
			
			add: function (parent,/*Object*/item, /*Number*/index) {
				// summary:
				//		Add item to the children list
				// item:
				//		Item to add
				// index:
				//		Index where to insert item
				
				if (item.parent == parent.taskId && item.category == this.CATEGORY_FILE) {
					if (parent["opened"] || parent["expanded"]) {
						FileList.load(parent.taskId);
					}
				}
				// #3657: this is the workaround. Already created item widget (hi.items.manager._items) is overwritten by the hi.items.manager.set()
				//        function (actually it is notifyComplete() from Observable). For this to work task item must be instantiated first and
				//        then file attachment item can be processed.
				if(!hi.items.manager.get(parent.taskId)) {
					/* #2991 */
					hi.items.manager.set(parent.taskId,{children:parent.list.getChildrenData(parent.taskId).length});
				}
				if (!parent.childrenRendered) {
					this._setExpandedAttr(parent,false,false);
					this._setChildrenAttr(parent, parent.list ? parent.list.getChildrenData(parent.taskId).length : 0);  
					return false;
				}
				
				if (!(item.id in parent.itemWidgets)) {
					// Update count
					this._setChildrenAttr(parent,parent.taskItem['children'] + 1);
				}
				
				if (item.parent == parent.taskId && item.category != this.CATEGORY_FILE) {
					// Create TaskItem
					var widget = this.renderChild(parent,item);
					
					// Update drag and drop
					if (parent.dragSource) {
						parent.dragSource.sync();
					}
					// #5026
					if (parent && widget && item && item.id > 0 && this.currentlyHighlighting[item.id]) {
						this.highlight(widget);
					}
					
					return widget;
				}
			},
			
			has: function (parent,/*Number*/id, /*Boolean*/shallow) {
				// summary:
				//		Checks if has child, return true if there is or false if there isn't
				// id:
				//		Task id
				// shallow:
				//		Check only direct children
				if (!parent) return false;
				if (!parent.childrenRendered) return false;
				
				if (id in parent.itemWidgets) {
					return parent.itemWidgets[id];
				} else if (!shallow) {
					var items = parent.itemWidgets,
						item = null,
						key = null;
					
					for (key in items) {
						item = this.has(items[key],id,false);//items[key].has(id, false);
						if (item) return item;
					}
				}
				return false;
			},
			
			animate: function (item,/*String*/effect, /*Boolean*/reverse, /*Function*/after) {
				// summary:
				//		Animate item
				// effect:
				//		Effect name: "fadeup", "fadedown", "puff"
				// reverse:
				//		Effect direction reversed or not
				// after:
				//		Callback function when animation finishes
				
				var callback = function () {
					if (dojo.isFunction(after)) return after();
				};
				
				//Effects were disabled in IE, because of performance issues
				if (effect == "none" || (dojo.isIE && dojo.isIE < 9)) return callback();
				
				var node = item ? dojo.byId('itemNode_' + item.widgetId) : null;
				
				if (node) {
					var callbackHide = function () {
						fakeNode.parentNode.removeChild(fakeNode);
						
						//If node shouldn't be visible it will be removed in callback
						node.style.visibility = "visible";
						
						callback();
					};
					
					var pos = dojo.position(node, true),
						fakeNode = null,
						
						opacityTo = 0,
						opacityFrom = 1,
						
						yFrom = ~~(pos.y),
						yTo = ~~(pos.y);
					
					fakeNode = dojo.create("div", {
						"class": "fake_task" + (dojo.hasClass(node, "highlight") ? " fake_task_highlight" : ""),
						"innerHTML": "<span>" +item.taskItem.title + "</span>",
						"style": {
							"left": ~~(pos.x) + "px",
							"top": ~~(pos.y) + "px",
							"width": node.offsetWidth - 2 + "px",
							"height": node.offsetHeight - 4 + "px"
						}
					}, dojo.body());
					
					//Effect
					var effectDir = ~~(node.offsetHeight * 1.4);
					
					if (effect == "fadeup" || effect == "copyup") {
						yTo -= effectDir;
					} else if (effect == "fadedown" || effect == "copydown") {
						yTo += effectDir;
					}
					
					if (reverse) {
						var tmp = yTo;
						yTo = yFrom;
						yFrom = tmp;
						
						opacityTo = 1;
						opacityFrom = 0;
					}
					
					if (effect == "fadeup" || effect == "fadedown" || effect == "puff") {
						node.style.visibility = "hidden";
					}
					
					if (effect == "fadeup" || effect == "fadedown" || effect == "copyup" || effect == "copydown") {
						dojo.anim(fakeNode, {
							top: {start: yFrom, end: yTo},
							height: {end: 28}
						}, 650);
					} else if (effect == "puff") {
						var scaleFactor = 20;
						dojo.anim(fakeNode, {
							top: {end: ~~(pos.y-scaleFactor)},
							left: {end: ~~(pos.x-scaleFactor)},
							width: {end: node.offsetWidth - 2 + scaleFactor * 2},
							height: {end: node.offsetHeight - 4 + scaleFactor * 2}
						}, 650);
					}
					
					dojo.anim(fakeNode, {
						opacity: {start: opacityFrom, end: opacityTo}
					}, (reverse ? 650 : 325), null, callbackHide, (reverse ? 0 : 325));
				} else {
					return callback();
				}
			},
			
			/* To indicate whether or not to carry out the highlight */
			globalHighlight: true,
			currentlyHighlighting: {},
			highlight: function (item, eventdate) {
				// summary:
				//		Animate element highlight
				if (!this.globalHighlight) return;
				//Don't highlight if task is opened
				if (item.taskItem.opened) return;
				var widgetId;
				
				if (eventdate) {
					if (item.taskItem.eventdate && eventdate == item.taskItem.eventdate) {
						widgetId = hi.items.manager._items[item.taskItem.id][eventdate].widgetId;
					} else {
						return;
					}
				} else {
					widgetId = item.widgetId;
				}
				var taskNode = dojo.byId('itemNode_' + item.widgetId);
				// If not visible try and highlight the parent.
				var parentItem = null;
				var itemObj = hi.items.manager.get(item.taskItem.id);
				if (!itemObj) return;

				if (!taskNode && itemObj && itemObj.parent && (parentItem = hi.items.manager.item(itemObj.parent))) {
					taskNode = dojo.byId('itemNode_' + parentItem.widgetId);
				}
				
				if (taskNode) {
					var that = this,
						heading = dojo.query("div.heading", taskNode),
						from = [255,252,159],
						//to = [245,245,245],
						to = [250,250,250],//HEX code = #FAFAFA => rgb = (250,250,250)
						diff = [to[0] - from[0],to[1] - from[1],to[2] - from[2]];
	
					heading = heading && heading.length ? heading[0] : heading;
					if (heading) {
						heading.style.background = '#FFFC9F';
					}
					
					if (itemObj.id > 0) {
						this.currentlyHighlighting[itemObj.id] = true;
					}
					dojo.anim(heading, {}, 1000, function (n) { 
						var c = [~~(from[0]+diff[0]*n),~~(from[1]+diff[1]*n),~~(from[2]+diff[2]*n)];
						if (heading) {
							heading.style.background = 'rgb(' + c.join(',') + ')';
						}
					}, function() {
						if (heading) {
							if (dojo.isIE && dojo.isIE < 9) {
								heading.removeAttribute('style');
							} else {
								heading.style.background = '';
							}
						}
						delete(that.currentlyHighlighting[itemObj.id]);
					}, 1000);
				}
			},
			
			/* ----------------------------- CHILDREN API (NO TESTED) ------------------------------ */
			
			hasNonFileChildren: function (item,list) {
				// summary:
				//		Return true if task has non file children
				var id = item.taskId?item.taskId:item.id,
					items = hi.items.manager.get({"parent": id}),
					taskList = list?list:item.list,
					i = 0,
					ii = items.length;
				
				for (; i<ii; i++) {
					if (items[i].category != this.CATEGORY_FILE) {
						if (taskList && taskList.filterMatches(items[i])) {
							return true;
						}
					}
				}
				
				return false;
			},
			
			hasChildrenOpenForEdit: function(parentId, event, silient) {
				var editing = hi.items.manager.editing();
				var getAllParents = function(childId) {
					var result = [];
					var childItem = hi.items.manager.get(childId);
					var dive = function(fromId) {
						var item = hi.items.manager.get(fromId);
						if (item && item.parent) {
							result.push(item.id);
							dive(item.parent);
						} else if (item) {
							result.push(item.id);
						}
					};
					
					if (childItem && childItem.parent) {
						dive(childItem.parent);
					}
					return result;
				};
				var getChildren = function(parentIds) {
					var res = [];
					var parentsLen = parentIds.length;
					
					for (var i = 0; i < parentsLen; i++) {
						var children = hi.items.manager.get({parent: parentIds[i]});
						var childrenLen = children.length;
						
						for (var j = 0; j < childrenLen; j++) {
							res.push(children[j].id);
						}
					}
					
					return res;
				};
				var parents = [];
				var family = [];
				if (editing && editing.taskItem && 
					(parents = getAllParents(editing.taskItem.id)) &&
					(family = getChildren(parents)) && 
					dojo.indexOf(parents.concat(family), parentId) != -1) {
					
					if (!silient) {
						Project.alertElement.showItem(14, {task_alert: 'Please finish editing open child item.'});
					}
					if (event) {
						dojo.stopEvent(event);
					}
					return true;
				}
				
				return false;
			},
			
			toggleChildren: function (id, eventdate, event, state) {
				// summary:
				//		Toggle children visibility
				var item = hi.items.manager._items[id][eventdate] || hi.items.manager._items[id].list;
					itemNode = item && dojo.byId('itemNode_' + item.widgetId),
					targetItemNode = null,
					currentList = null;
	
				if (!item || !itemNode || this.hasChildrenOpenForEdit(id)) {
					return;
				}

				if(event && event.target){
					targetItemNode = dojo.query(event.target).closest('.task_li')[0];
					if(itemNode != targetItemNode){
						var path = dojo.global.taskPath(id),
							parent = 0,
							itemWidget = null,
							currentListNode = dojo.query(event.target).closest('.task_ul')[0];

						if(!currentListNode){
							return;
						}

						currentList = registry.byNode(currentListNode);
						itemWidget = currentList && currentList.itemWidgets;
						while(path.length && itemWidget){
							parent = path.shift();
							itemWidget = itemWidget[parent];
						}
						item = itemWidget && itemWidget.itemWidgets[id];
					}
					if(!item) return;
					
					if (this.hasNonFileChildren(item)) {
						//this.set("expanded", !this.expanded);
						if (typeof(state) == 'undefined') {
							this._setExpandedAttr(item,!item.expanded, true, eventdate);
						} else {
							this._setExpandedAttr(item, state, event, eventdate);
						}
					}
				}
				
				if (!item.childrenRendered) {
					this.renderChildren(item);
				}
			},
			
			expandChildren: function (item, eventdate) {
				// summary:
				//		Expand children list
				
				if (this.hasNonFileChildren(item)) {
					//this.set("expanded", true);
					this._setExpandedAttr(item,true,true,eventdate);
				}
				
				if (!item.childrenRendered) {
					this.renderChildren(item);
				}
			},
			
			isChild: function (taskItem) {
				// summary:
				//		Returns true if this task is another tasks child (by data)
				if (taskItem) {
					var parent = taskItem.parent || (taskItem.taskItem ? taskItem.taskItem.parent : null),//this.get("parent"),
						item = null;
					
					if (parent) {
						item = hi.items.manager.get(parent);
						if (item && item.category != this.CATEGORY_PROJECT) {
							if (taskItem.container && taskItem.container instanceof hi.widget.TaskList) {
								return false;
							} else {
								return true;
							}
						}
					}
				}
				
				return false;
			},
			
			remove: function (parent, /*Object*/item, /*Number*/index, /*Object*/diff) {
				// summary:
				//		Remove item from the children list
				// item:
				//		Item which to remove
				// index:
				//		Index from where item was removed
				// diff:
				// 		previous item values
				
				if (!parent.childrenRendered) {
					//this.set("children", this.list ? this.list.getChildrenData(this.taskId).length : 0);
					this._setChildrenAttr(parent,parent.list ? parent.list.getChildrenData(parent.taskId).length : 0);
					return false;
				}
				
				var taskId = item.id,
					widget = parent.itemWidgets[item.id];

				if (widget) {
					//widget.destroy();
					var childrenList = dojo.byId('children_' + parent.widgetId),
						child = dojo.byId('itemNode_' + parent.itemWidgets[item.id].widgetId);

					if(parent.list.rendered){
						if(childrenList && child && child.parentNode === childrenList ){
							childrenList.removeChild(child);
							// dojo.byId('children_' + parent.widgetId).removeChild(dojo.byId('itemNode_' + parent.itemWidgets[item.id].widgetId));
						}else{
							if(childrenList && !child){
								child = dojo.query('li[data-task-id=' + taskId + ']', childrenList);
								if(child && child.length === 1){
									childrenList.removeChild(child[0]);
								}
							}
						}
					}
					delete(parent.itemWidgets[item.id]);
					delete(parent.itemDOMs[item.id]);
					//Update count
					this._setChildrenAttr(parent,parent.taskItem.children - 1);
				}
			},
			
			_setExpandedAttr: function (item, expanded, saveState, eventdate) {
				if (typeof(saveState) == 'undefined') {
					saveState = true;
				}
				item.expanded = expanded;
				if (!dojo.byId('itemNode_'+item.widgetId)) return;
				dojo.toggleClass(dojo.byId('itemNode_'+item.widgetId), "expanded", expanded);
				//this._set("expanded", expanded);

				if (saveState) {
					if (eventdate) {
						dojo.global.saveTaskItemState(item.taskId, expanded, eventdate);
					} else {
						dojo.global.saveTaskItemState(item.taskId, expanded);
					}
				}

				if (!expanded) {
					//Close opened sub task
					item = this.has(item,hi.items.manager.opened());
					if (item) {
						if (item.editing) {
							item.stopEditing();
						} else {
							item.collapse();
						}
					}
				}
				
			},
			
			/* ----------------------------- EVENT BINDINGS (NOT FULLY TESTED) ------------------------------ */
			
			/* #2965 - attribute to indicate if a task item is in the process of being expanded or collapsed */
			transitioning: null,
			
			/*
			 * Invoked when title bar of item is clicked
			 */
			handleExpandClick: function (id, eventdate, e) {
				var item = hi.items.manager._items[id] ? (hi.items.manager._items[id][eventdate] || hi.items.manager._items[id].list) : null;

				/*	#3597	*/
				var eventTaskLi = dojo.query(e.target).closest('.task_li'), 
					eventWidgetId;

				if (!item) {
					if (id == this.transitioning) {
						this.transitioning = false;
						console.warn('no item found!');
					}
					return false;
				}
				
				if(eventTaskLi.length){
					eventWidgetId = dojo.getAttr(eventTaskLi[0], 'id');
					var ta = eventWidgetId.split('_');
					ta.splice(0, 1);
					eventWidgetId = ta.join('_');
			
					if(item && item.widgetId === eventWidgetId){

					}else{
						if(item.opened){
							this.collapse(item);
						}

						item.propertiesView = null;
						item.editingView = null;

						item.widgetId = eventWidgetId;
						item.taskItem.widgetId = eventWidgetId;
						var pv = null, ev = null;
						if((pv = dojo.query('.properties_view', dojo.byId('propertiesViewNode_' + eventWidgetId))).length){
							pv = pv[0];
							pv.parentNode.removeChild(pv);
						}
						if((ev = dojo.query('.editing_view', dojo.byId('propertiesViewNode_' + eventWidgetId))).length){
							ev = ev[0];
							ev.parentNode.removeChild(ev);
						}
					}
				}

				if (hi.items.manager.openedId() == id && (eventdate && eventdate !='undefined')) {
					if (!(hi.items.manager.opened().taskItem.eventdate && eventdate && hi.items.manager.opened().taskItem.eventdate == eventdate)) {
						hi.items.manager.collapseOpenedItems();
					}
				}
				
				// #3161
				if (hi.items.manager.editing() || !item) {
					if (e) {
						dojo.stopEvent(e);
					}
				} else {
					if (item.opened) {
						//this.openEdit(item,'title'); // open edit for with title input focused
						this.collapse(item);
					} else {
						this.expand(item);
						this.toggleChildren(id, eventdate, e, true);
					}
				}
			},
			
			/*
			 * Invoked when the title bar of an expanded item is clicked
			 */
			handleCollapseClick: function (id,eventdate,e) {
				var item = hi.items.manager._items[id][eventdate]||hi.items.manager._items[id].list;
				if (item.opened && !item.editing) {
					dojo.stopEvent(e);
					this.collapse(item);
				}
			},
			
			handleItemClick: function (id, eventdate, e) {
				var item = hi.items.manager._items[id] ? (hi.items.manager._items[id][eventdate] || hi.items.manager._items[id].tooltip || hi.items.manager._items[id].list) : null;
				// When document will be clicked collapse items.
				if (item && Click.setClickFunction) {
					Click.setClickFunction(dojo.hitch(hi.items.manager, hi.items.manager.collapseOpenedItems), dojo.byId('itemNode_' + item.widgetId), "taskItemManager");
				}
			},
			
			/*
			 * Action for double clicking title bar of an item
			 */
			handleItemDoubleClick: function (id, eventdate, e) {
				/*
				if (hi.items.manager._items[id] && hi.items.manager._items[id].list) {
					this.openEdit(hi.items.manager._items[id].list);
				}
				*/
				var eventTaskLi = dojo.query(e.target).closest('.task_li'),
					eventWidgetId,
					taskItem = hi.items.manager.get(id),
					p = taskItem? parseInt(taskItem.permission, 10) : 0;

				if (!taskItem) return false;

				if(taskItem.permission < Hi_Permissions.MODIFY){
					return;
				}

				if (eventTaskLi.length) {
					eventWidgetId = dojo.getAttr(eventTaskLi[0], 'id');
					var ta = eventWidgetId.split('_');
					ta.splice(0, 1);
					eventWidgetId = ta.join('_');
				}

				var _item = hi.items.manager._items[id];
				var item = _item ? (_item[eventdate] || _item.list) : null;
				if (!item) return;
				
				if (item && item.widgetId === eventWidgetId) {
					this.openEdit(item);
				} else {
					item = dojo.mixin({}, item, {taskItem: dojo.mixin({}, item.taskItem, {widgetId: eventWidgetId})}, {widgetId: eventWidgetId});
					this.openEdit(item);
				}
			},
			
			/*
			 * Action for checking/unchecking the check box on the item in the UI
			 */
			handleClickComplete: function (taskId, eventdate, event, withoutWidget) {
				var taskItem,
					skipAllAnimation = false;
				/* #2791 */
				/* It might be the case that an item is not in hi.items.manager store
				 * For example, when clicking complete a calendar item that has no item on any list
				 * in the current tab
				 */
				if (event && event.target && dojoClass.contains(event.target, 'completeInCalendar')) {
					// #5848: show explanation.
					Project.alertElement.showItem(28, {alert: hi.i18n.getLocalization('project.click_specific_day_to_complete')});
					dojoEvent.stop(event);
					return false;
				}
				if (!Hi_Actions.validateAction('complete', taskId)) {
					if (event) dojoEvent.stop(event);
					return false;
				}
				if (!hi.items.manager._items[taskId]) {
					taskItem = {taskItem:hi.items.manager.get(taskId)};
					skipAllAnimation = true;
				} else if (hi.items.manager._items[taskId].tooltip) {
					taskItem = hi.items.manager._items[taskId].tooltip;
				} else if (eventdate && eventdate!='undefined') {
					/* User may have clicked on an inline check box on calendar (without opening tooltip)
					 * So no item DOM instance exists to animate
					 */
					if (!hi.items.manager._items[taskId][eventdate]) {
						taskItem = {taskItem:hi.items.manager.get(taskId)};
						skipAllAnimation = true;
					} else {
						taskItem = hi.items.manager._items[taskId][eventdate];
					}
				} else {
					taskItem = hi.items.manager._items[taskId].list;
				}
				
				if (this.hasChildrenOpenForEdit(taskId, event)) {
					return;
				}
				var completeNode = null;
				if (taskItem) {
					completeNode = dojo.byId('completeNode_' + taskItem.widgetId);
				}

				/* #2064 Block click if new sub task is opened */
				var itemID = hi.items.manager.openedId(), msg;
				if (itemID < 0 && !withoutWidget) {
					var parent = item && item.parent;
					var item = hi.items.manager.get(itemID);
					/* Bug #2094 - climb up the list of parent tasks 
					 * If we find find one; block until user saves the sub task that is in edit mode
					 */
					while (parent) {
						if (parent == this.taskId) {
							var editing = hi.items.manager.opened().editingView;
							if (itemID == editing.taskId) {
								Project.alertElement.showItem(14,{task_alert:'Please save this sub task first'});
								if (event) dojo.stopEvent(event);
								return;
							}
						}
						parent = hi.items.manager.get(parent).parent;
					}
				}

				/* #1203 - use getIsCompleted to determine completed status, since this handles both cases of recurring and non-recurring items */
				//var eventDateAttr = event && event.target ? dojo.getAttr(event.target, 'eventdate') : null,//eventDateAttr = eventDateAttr && eventDateAttr != 'undefined' ? eventDateAttr : null,
				var completed = !hi.items.manager.getIsCompleted(taskId, eventdate || null),//!hi.items.manager.get(this.taskId).completed;//!this.completed,
					animation = completed ? "fadedown" : "fadeup",
					properties = {
						"completed": completed ? 1 : 0
						// , recurring: taskItem.taskItem.recurring ? taskItem.taskItem.recurring : 0
					};

				if (Hi_Preferences.get('show_confirmation_complete', false, 'bool')) {
					// Show confirmation.
					if (completed) {
						msg = hi.i18n.getLocalization('tooltip.complete_confirmation');
					} else {
						msg = hi.i18n.getLocalization('tooltip.uncomplete_confirmation');
					}
					
					if (!confirm(msg)) {
						if (event) {
							event.target.checked = !event.target.checked;
							dojo.stopEvent(event);
						}
						return;
					}
				}
				if(taskItem && taskItem.taskItem && taskItem.taskItem.recurring){
					// remove for http://red.hitask.com/issues/4632#note-34
					properties['recurring'] = taskItem.taskItem.recurring;
					// properties['eventDateAttr'] = eventDateAttr && eventDateAttr != 'null' ? time2strAPI(str2timeAPI(eventDateAttr)) : null;
					properties['eventDateAttr'] = time2strAPI(str2timeAPI(eventdate))||null;
					
				}
				
				// Gets upcoming event date of recurring item in y-m-d format or null. 
				var _nextEvtDate = function() {
					var dateStr = hi.items.date.getEventDate(taskId);
					if (dateStr && dateStr.indexOf('T') != -1) {
						return dateStr.split('T')[0];
					}
					
					return null;
				};
				
				// #2543 and #1430
				/*
				if (properties['eventDateAttr'] && _nextEvtDate() != eventdate) {
					animation = 'none';
				} 
				*/
				var today = time2str(new Date(),'y-m-d');
				// Bug #5175 - all items should have animation effect when toggle complete.
				// if (eventdate != today) animation='none';
				//animation='none';
				
				if (Hi_Timer && Hi_Timer.isTimedTask(taskId)) {
					//Stop timer if it is running for this task
					Hi_Timer.stop();
				}
				
				// properties['withoutWidget'] = withoutWidget;
				
				if (this.isChild(taskItem)) {
					//Childs stay where they are
					this.save(taskItem && taskItem.taskItem ? taskItem.taskItem : taskItem, properties);
				} else {
					//Animate

					var animateThisTaskItem = /*hi.items.manager._items[taskId].list || */taskItem || null;
					var opened = hi.items.manager.opened();
					var closeTaskEditTooltip = function() {
						var curOpened = hi.items.manager.opened();

						if (opened && opened._setOpenedAttr) {
							opened._setOpenedAttr(opened, false);

							if (opened === curOpened) {
								Tooltip.close('taskedit');
							}
						}
					};
					var animateCallback = lang.hitch(this, function() { 
						/* Bug #2042 */
						if (!withoutWidget) {
							if (animateThisTaskItem && dojo.byId('completeNode_' + animateThisTaskItem.widgetId)) dojo.byId('completeNode_'+animateThisTaskItem.widgetId).checked=properties.completed;
							if (taskItem) this.set(taskItem,'complete',taskItem.taskItem.completed);
						}
						if (taskItem) this.save(taskItem, properties);
						closeTaskEditTooltip();
					});
					
					if (skipAllAnimation) {
						this.save(taskItem.taskItem, properties);
					} else if (animation != 'none') {
						// Do not animate tooltip widget.
						this.animate(animateThisTaskItem, animation, false, animateCallback);
						closeTaskEditTooltip();
					} else {
						animateCallback();
					}
				}
				
				if (event && event.stopPropagation) event.stopPropagation();
			},
			
			/*
			 * Action for clicking the star on the item
			 */
			handleClickStarred: function (taskId, eventdate, event) {
				/* #2064 Block click if new sub task is opened */
				/* #2989 - preferentially take task item corresponding to calendar tooltip if it is opened */
				var tooltip = hi.items.manager._items[taskId].tooltip;
				var taskItem = hi.items.manager._items[taskId][eventdate] || tooltip || hi.items.manager._items[taskId].list;
				if (
					!taskItem || this.hasChildrenOpenForEdit(taskId, event) ||
					(taskItem.taskItem && !Hi_Actions.validateAction('makeStarred', taskItem.taskItem)) /*Hi_Actions.makeStarred*/
				) { 
					// (taskItem.taskItem && taskItem.taskItem.permission < Hi_Permissions.MODIFY)) {
					// Project.alertElement.showItem(14, {task_alert: hi.i18n.getLocalization('no_permissions.no_permissions_to_modify') || 'Sorry, you have no permissions to Modify this item'});
					dojo.stopEvent(event);
					return;
				}


				var itemID = hi.items.manager.openedId();
				if (itemID < 0) {
					var parent;
					if (!hi.items.manager.get(itemID)) parent = null;
					else parent = hi.items.manager.get(itemID).parent;
					/* Bug #2094 - climb up the list of parent tasks 
					 * If we find find one; block until user saves the sub task that is in edit mode
					 */
					while(parent) {
						if (parent == taskId) {
							var editing = hi.items.manager.opened().editingView;
							if (itemID == editing.taskId) {
								Project.alertElement.showItem(14,{task_alert:'Please save this sub task before starring its parent'});
								dojo.stopEvent(event);
								return;
							}
						}
						parent = hi.items.manager.get(parent).parent;
					}
				}
				
				// #3593: collapse if current item is opened.
				if (hi.items.manager.openedId() == taskId) {
					this.collapse(taskItem);
				}

				var starred = !taskItem.taskItem.starred;
				if (taskItem.propertiesView && taskItem.propertiesView.task) {
					taskItem.propertiesView.task.starred = starred;
				}

				//this.set("starred", starred);
				this._setStarredAttr(taskItem,starred);
				if (tooltip && tooltip != taskItem) {
					// #2989: change Starred in tooltip too.
					this._setStarredAttr(tooltip, starred);
				}
				this.save(taskItem,{"starred": starred, 'withoutWidget': true});
				dojo.stopEvent(event);
			},
			
			handleKeyPress: function (id, eventdate, event) {
				var item = hi.items.manager._items[id].tooltip || hi.items.manager._items[id][eventdate] || hi.items.manager._items[id].list;
				//if (hi.items.manager._items[id].tooltip) item = hi.items.manager._items[id].tooltip;
				//else item = hi.items.manager._items[id].list;
				var keys = dojo.keys,
					key  = event.which || event.keyCode || false,
					el   = event.srcElement || event.target;
				
				event.cancelBubble = true;
				
				if (key == keys.KEY_ENTER && (el.tagName == "TEXTAREA" || (el.tagName == "INPUT" && el.type == "button") || el.tagName == "BUTTON")) {
					return;
				}
				
				if (key == keys.KEY_ESCAPE) {
					if (item.opened) {
						if (item.editing) {
							item.editingView.cancel();
						} else {
							this.collapse(item);
						}
					}
				} else if (key == keys.KEY_ENTER) {
					if (item.editing && item.opened && item.editingView) {
						// #3711
						item.editingView.set('title', item.editingView.titleInputNode.get('value'));
						item.editingView.set('message', item.editingView.messageInput.get('value'));
						item.editingView.save();
						//	#3751
						if (typeof event.target.blur === 'function') {
							if (event.target === document.activeElement) event.target.blur();
						}
					}
				}
			},
			
			/* ----------------------------- COLLAPSE/EXPAND API (NOT FULLY TESTED) ------------------------------ */
			showDragHandleIfRequired: function(item) {
				var node = dojo.byId('dragHandle_' + item.widgetId);
				var allowed = this.dragHandleAllowed(item);
				if (node && allowed) {
					dojo.removeClass(node, 'hidden');
				} else if (node && !allowed) {
					dojo.addClass(node, 'hidden');
				}
			},
			
			openEdit: function (item, name) {
				// summary:
				//		Open edit view
				var taskItem = item.taskItem,
					p = taskItem? parseInt(taskItem.permission, 10) : 0;

				if (p < Hi_Permissions.MODIFY) {
					return;
				}
				if (Project.checkLimits("TRIAL_TASKS") && item.opened && !item.editing) {
					//this.set("editing", true);
					this._setEditingAttr(item, true);
					item.editingView.focusInput(name);
				}
			},
			
			closeEdit: function (id, eventdate, notForce) {
				// summary:
				//		Close edit and expanded views
				var item; 
				/* Do not try to close if item is not in store, which can be the case for a calendar or tooltip item */
				if (!hi.items.manager._items[id] && !notForce) {
					hi.items.manager.collapseOpenedItems();
					return;
				}
				if (hi.items.manager._items[id].tooltip) {
					item = hi.items.manager._items[id].tooltip;
				} else if (eventdate) {
					item = hi.items.manager._items[id][eventdate];
				} else item = hi.items.manager._items[id].list;

				if (!item) {
					hi.items.manager.collapseOpenedItems();
					return;
				}
				/* #3218 if (item.opened && item.editing) { */
				this._setEditingAttr(item, false);
				if (item._setOpenedAttr) {
					item._setOpenedAttr(item,false);
				} else {
					this._setOpenedAttr(item, false);
				}
				/* #3218 }*/
				this.showDragHandleIfRequired(item);
			},
			
			collapse: function (item) {
				// summary:
				//		Collapse task
				if (item.editing) {
					//this.set("editing", false);
					this._setEditingAttr(item,false);
				}
				if (item.opened) {
					//this.set("opened", false);
					if (item._setOpenedAttr)
						item._setOpenedAttr(item, false);
					else
						this._setOpenedAttr(item, false);
				}
				this.showDragHandleIfRequired(item);
			},
			
			expand: function (item) {
				// summary:
				//		Expand task
				if (!item.opened && Project.checkLimits("TRIAL_TASKS")) {
					dojo.addClass(dojo.byId('dragHandle_' + item.widgetId), 'hidden');
					//this.set(item,"opened", true);
					this._setOpenedAttr(item, true);
				}
			},
			
			_setOpenedAttr: function (item,opened) {
				//Don't open if some other item is being edited
				/* Bug #2126 - only block if another existing item (taskId>0) is in edit mode
				 * if (opened && hi.items.manager.editing()) return; 
				 */

				if (opened && hi.items.manager.editing() && hi.items.manager.editing().taskId > 0) {
					return;
				}
				/* We must check for the node because an event like click complete will remove it from the DOM; possibly
				 * before this code is invoked
				 */
				if ( dojo.byId('itemNode_'+item.widgetId) ) dojo.toggleClass(dojo.byId('itemNode_'+item.widgetId), "highlight", opened);

				item.opened = opened;
				
				if (opened) {
					this.renderExpandedView(item);
					
					//Close all other tasks
					hi.items.manager.collapseOpenedItems();
					hi.items.manager.opened(item.taskId || item.id, true); // TaskId will be empty for new item
					item.propertiesView.expand();
				} else {
					if (item.propertiesView) item.propertiesView.collapse();
					hi.items.manager.opened(item.taskId || item.id, false); // TaskId will be empty for new item
				}
			},
			
			_setEditingAttr: function (item, editing) {
				item.editing = editing;
				
				/* We must check for the node because an event like click complete will remove it from the DOM; possibly
				 * before this code is invoked
				 */
				if (dojo.byId('itemNode_'+item.widgetId)) dojo.toggleClass(dojo.byId('itemNode_'+item.widgetId), "highlight-edit", editing);
				if ( dojo.byId('headingNode_'+item.widgetId) ) dojo.toggleClass(dojo.byId('headingNode_'+item.widgetId), "hidden", editing);
				
				if (editing) {
					this.renderEditingView(item);
					
					hi.items.manager.editing(item.taskId || item.id, true); // TaskId will be empty for new item
					
					if (item.propertiesView) {
						//Hide properties view
						item.propertiesView.collapse(true);
					}
					
					item.editingView.expand();
				} else {
					if (item.editingView) item.editingView.collapse();
					/* #3105
					if (item.propertiesView) {
						item.propertiesView.expand(true);
					}
					*/
					hi.items.manager.editing(item.taskId || item.id, false); // TaskId will be empty for new item
				}
			},
			
			renderChildren: function (item) {
				// summary:
				//		Render all children
				
				if (!item.childrenRendered) {
					item.childrenRendered = true;
					
					var items = hi.items.manager.get(),
						i = 0,
						ii = items.length,
						id = item.taskId,//this.get("taskId"),
						list = item.list,
						flatten = false;
					
					if (list) {
						for (; i<ii; i++) {
							if (items[i].parent == id && list.filterFilters(items[i]) ) {
								if (items[i].category != this.CATEGORY_FILE) {
									/*
									 * #2578 - check to make sure not to render any sub-items flattened out and rendered already
									 */
									if (list['_flatten_'+list.filterType])
									{
										flatten = list['_flatten_'+list.filterType](hi.items.manager.get(items[i].parent),items[i]);
									}
									if (!flatten) 
									{
										this.renderChild(item,items[i]);
										/* 
										 * #3328
										 */
										list.itemIds[items[i].id] = true;
									}
								} else {
									//Reload file list
									if (item.opened || item.expanded) {
										FileList.load(item.taskId);
									}
								}
							}
						}
					}
					
					if (item.dragSource) {
						item.dragSource.sync();
					}
				}
			},
			
			renderChild: function (parent,item,instance_id) {
				// summary:
				//		Render childre task
				// item:
				//		Children item data
				var eventdate = instance_id?instance_id:parent.list.calculateInstanceDate(item);
				
				if (item.recurring != 0) {
					item = dojo.mixin({}, item);
					item.completed = hi.items.manager.getIsCompleted(item);
				}
				
				// Create TaskItem

				item.children = parent.list.getChildrenData(item.id).length;
				var precursor = {
						eventdate: eventdate||null,
						//widgetId:widgetId,
						taskId: item.id/*,
						list:this,
						listName:this.listName
						*/
					};
					
				var domItem = item_DOM.createDOMItem(parent.list, dojo.mixin(precursor,item));
				//var domItem = item_DOM.createDOMItem(this,item);
				parent.itemDOMs[item.id] = lang.replace(item_DOM.template, domItem);
				
				var widget = parent.itemWidgets[item.id] = dojo.mixin({
						container:parent,
						childrenRendered:false,
						itemIDs: {},
						itemWidgets: {},
						itemDOMs: {},
						widgetId: domItem.widgetId,
						list: domItem.list,
						taskId:item.id
					}, {
						taskItem: dojo.mixin(dojo.clone(item),{
							opened:0,
							expanded:0,
							widgetId: domItem.widgetId,
							colorClass:domItem.color_class, 
							childrenClass:domItem.children_class || '',
							eventdate:eventdate || null
						})
					});
				hi.items.manager.removeItem(item.id, eventdate||'list');
				hi.items.manager.addItem(item.id, parent.itemWidgets[item.id],eventdate||'list');
				//dojo.place(parent.itemDOMs[item.id],dojo.byId('children_'+parent.widgetId));
				this._add(parent.itemWidgets,parent.itemWidgets[item.id],parent.itemDOMs[item.id],dojo.byId('children_' + parent.widgetId));
				/* #3026 */
				this.checkOverdue(widget);
				parent.itemWidgets[item.id].dragSource = new dojo.dnd.HiDragSource(dojo.byId('children_'+parent.itemWidgets[item.id].widgetId), {});
				parent.itemWidgets[item.id].dragSource.sync();
				if (parent.dragSource) parent.dragSource.sync();
				/*
				var widget = this.itemWidgets[item.id] = new noDijit_TaskItem(dojo.mixin({
					"taskId": item.id, // id is reserved by dojo widget
					"list": this.list,
					"container": this
				}, item, {
					//"id": null,
					
				}));
				*/
				/* #2293 */
				//this.itemWidgets[item.id].placeAt(this.childrenNode);
				/* TODO:Sort this._add(this.itemWidgets[item.id],item,this.childrenNode);*/
				//console.log('item widgets?',parent.itemDOMs[item.id]);
				return parent.itemWidgets[item.id];
			},
			
			renderExpandedView: function (item) {
				// summary:
				//		Render expanded view widget
				console.log('propertiesView?',item.propertiesView);
				if (Tooltip.opened("deleterecurring")) {
					Tooltip.close("deleterecurring");
				}
				if (Tooltip.opened("deleteconfirmation")) {
					Tooltip.close("deleteconfirmation");
				}
				if (item.propertiesView) return;
				require(['hi/widget/task/stateful_taskitem'],function(TaskItem) {
					if (item.taskItem && item.widgetId != item.taskItem.widgetId) item.taskItem.widgetId = item.widgetId;
					if (!item.taskItem.list) item.taskItem.list = item.list;//This may be causing problems in hi.store.Observable.diffObject
					var view = item.propertiesView = new hi.widget.TaskItemProperties({
						"task": new TaskItem(item.taskItem)
					});
					var viewNode = dojo.byId('propertiesViewNode_' + item.widgetId);
					if (viewNode) {
						view.placeAt(viewNode);
						view.startup();
					}
				});
				//this._supportingWidgets.push(view);
			},
			
			renderEditingView: function (item) {
				// summary:
				//		Render editing view widget
				
				// - At the moment, must re-instantiate in order to keep view of editing form updated if (item.editingView) return;
				if (item.editingView) return;
				var view = item.editingView = new hi.widget.TaskItemEdit({
					"task": item.propertiesView.task
				});
				
				view.placeAt(dojo.byId('propertiesViewNode_'+item.widgetId));
				view.startup();
				
				//this._supportingWidgets.push(view);
			}
	};

	function setTextContent (node, str) {
		// summary:
		//		Helper function to escape string and set it as HTMLElement content
		// node:HTMLElement
		//		Element
		// str:String
		//		String content
		
		node.innerHTML = "";
		node.appendChild(win.doc.createTextNode(str));
	}
	
	return item_DOM;
});
