define([
	"dojo/on",
	'dojo/Deferred',
	'hi/widget/task/item_DOM',
	'dojox/css3/transition',
	'dojox/socket',
	"dojo/date/locale",
	'dojo/query',
	'dojo/dom-attr',
	'hi/items/action'
], function (dojo_on, Deferred, item_DOM, css3fx, Socket, djDateLocale, dojoQuery, dojoDomAttr) {

	/**
	 * Copyright (C) 2005-2006 Vide Infra Grupa SIA, Riga, Latvia. All rights reserved.
	 * http://videinfra.com
	 * Version 1.1
	 */
	String.prototype.trim = function() {
		return this.replace(/(^\s*)|(\s*$)/g, "");
	};
	
	dojo.mixinDeep = function(obj, deep) {
		var objectClone = new (obj || {}).constructor();
		var property;
		for (property in obj)
		if (!deep)
			objectClone[property] = obj[property];
		else if (typeof obj[property] == 'object' && obj[property] !== null)
			objectClone[property] = dojo.mixinDeep(obj[property], deep);
		else
			objectClone[property] = obj[property];
		return objectClone;
	};
	
	dojo.keys = {KEY_BACKSPACE:8, KEY_TAB:9, KEY_CLEAR:12, KEY_ENTER:13, KEY_SHIFT:16, KEY_CTRL:17, KEY_ALT:18, KEY_PAUSE:19, KEY_CAPS_LOCK:20, KEY_ESCAPE:27, KEY_SPACE:32, KEY_PAGE_UP:33, KEY_PAGE_DOWN:34, KEY_END:35, KEY_HOME:36, KEY_LEFT_ARROW:37, KEY_UP_ARROW:38, KEY_RIGHT_ARROW:39, KEY_DOWN_ARROW:40, KEY_INSERT:45, KEY_DELETE:46, KEY_HELP:47, KEY_LEFT_WINDOW:91, KEY_RIGHT_WINDOW:92, KEY_SELECT:93, KEY_NUMPAD_0:96, KEY_NUMPAD_1:97, KEY_NUMPAD_2:98, KEY_NUMPAD_3:99, KEY_NUMPAD_4:100, KEY_NUMPAD_5:101, KEY_NUMPAD_6:102, KEY_NUMPAD_7:103, KEY_NUMPAD_8:104, KEY_NUMPAD_9:105, KEY_NUMPAD_MULTIPLY:106, KEY_NUMPAD_PLUS:107, KEY_NUMPAD_ENTER:108, KEY_NUMPAD_MINUS:109, KEY_NUMPAD_PERIOD:110, KEY_NUMPAD_DIVIDE:111, KEY_F1:112, KEY_F2:113, KEY_F3:114, KEY_F4:115, KEY_F5:116, KEY_F6:117, KEY_F7:118, KEY_F8:119, KEY_F9:120, KEY_F10:121, KEY_F11:122, KEY_F12:123, KEY_F13:124, KEY_F14:125, KEY_F15:126, KEY_NUM_LOCK:144, KEY_SCROLL_LOCK:145};
	
	dojo.dom = dojo.mixin(dojo.dom || {}, {
		getAncestors: function (node, filterFunction, returnFirstHit) {
			var ancestors = [];
			var isFunction = (filterFunction && (filterFunction instanceof Function || typeof filterFunction == "function"));
			while (node) {
				if (!isFunction || filterFunction(node)) {
					ancestors.push(node);
				}
				if (returnFirstHit && ancestors.length > 0) {
					return ancestors[0];
				}
				node = node.parentNode;
			}
			if (returnFirstHit) {
				return null;
			}
			return ancestors;
		},

		getAncestorsByTag: function (node, tag, returnFirstHit) {
			tag = tag.toLowerCase();
			return dojo.dom.getAncestors(node, function (el) {
				return ((el.tagName) && (el.tagName.toLowerCase() == tag));
			}, returnFirstHit);
		},

		isDisplayed: function (node) {
			return (node && dojo.getComputedStyle(node).display != "none" ? true : false);
		},

		isInDom: function (node) {
			while(node && node.parentNode) {
				if (node === document.body) return true;
				node = node.parentNode;
			}
			return false;
		},
		setCaretPosition: function (element, caretPos) {
			if(element) {
				if(element.createTextRange) {
					var range = element.createTextRange();
					range.move('character', caretPos);
					range.select();
				}
				else {
					if(element.selectionStart || element.selectionStart === 0) {
						element.focus();
						element.setSelectionRange(caretPos, caretPos);
					}
					else {
						element.focus();
					}
				}
			}
		},
		
		selectInputText: function (element) {
			var _window = dojo.global;
			var _document = document;
			element = dojo.byId(element);
			if (_document["selection"] && dojo.body()["createTextRange"]) {
				var range = element.createTextRange();
				range.moveStart("character", 0);
				range.moveEnd("character", element.value.length);
				range.select();
			} else {
				if (_window["getSelection"]) {
					var selection = _window.getSelection();
					element.setSelectionRange(0, element.value.length);
				}
			}
			element.focus();
		},
		show: function (node) {
			node = dojo.byId(node);
			if (dojo.style(node, "display") == "none") {
				dojo.style(node, "display", (node.dojoDisplayCache || ""));
				node.dojoDisplayCache = undefined;
			}
		},
		hide: function (node) {
			node = dojo.byId(node);
			if (typeof node["dojoDisplayCache"] == "undefined") {
				var d = dojo.style(node, "display");
				if (d != "none") {
					node.dojoDisplayCache = d;
				}
			}
			dojo.style(node, "display", "none");
		},
		getChildElements: function (node) {
			if (!node) return [];
			var childs = [];
			var childNodes = node.childNodes;
			for(var i=0,j=childNodes.length; i<j; i++) {
				if (childNodes[i].nodeType == 1) {
					childs[childs.length] = childNodes[i];
				}
			}
			
			return childs;
		},
		/* PERFORMANCE: To obtain taskId from DOM representation of a TaskItem widget */
		getTaskId: function(node) {
			return this.getAllAttributes(node)['data-task-id'];
		},
		
		getAllAttributes: function (node) {
			if (!node || !node.attributes) return {};
			var attr = {};
			var _attr = node.attributes;
			for(var i=0,j=_attr.length; i<j; i++) {
				attr[_attr[i].nodeName || _attr[i].name] = _attr[i].value;
			}
			return attr;
		}
	});
	
	dojo.global.getCursorPosition  = function(e) {
		e = e || dojo.global.event;
		var cursor = {x:0, y:0};
		if (e.pageX || e.pageY) {
			cursor.x = e.pageX;
			cursor.y = e.pageY;
		} else {
			var de = document.documentElement;
			var db = dojo.body();
			cursor.x = e.clientX + ((de || db)["scrollLeft"]) - ((de || db)["clientLeft"]);
			cursor.y = e.clientY + ((de || db)["scrollTop"]) - ((de || db)["clientTop"]);
		}
		return cursor;
	};
	
	dojo.global.removeNode = function(node, including) {
		node = dojo.byId(node);
		if (!node) return;
		if (typeof including == 'undefined') {
			including = 1;
		}
		if (including) {
			if (node && node.parentNode) {
				return node.parentNode.removeChild(node);
			}
		} else {
			while(node.firstChild) {
				node.removeChild(node.firstChild);
			}
			return true;
		}
	};
	
	dojo.global.prependChild  = function(node, parent) {
		if (parent.firstChild) {
			parent.insertBefore(node, parent.firstChild);
		} else {
			parent.appendChild(node);
		}
		return true;
	};
	
	dojo.global.previousElement = dojo.global.prevElement = function(node, tagName) {
		if (!node) {
			return null;
		}
		do {
			node = node.previousSibling;
		} while (node && node.nodeType != 1);
		if (node && tagName && tagName.toLowerCase() != node.tagName.toLowerCase()) {
			return previousElement(node, tagName);
		}
		return node;
	};
	
	dojo.global.nextElement = function(node, tagName) {
		if (!node) {
			return null;
		}
		do {
			node = node.nextSibling;
		} while (node && node.nodeType != 1);
		if (node && tagName && tagName.toLowerCase() != node.tagName.toLowerCase()) {
			return nextElement(node, tagName);
		}
		return node;
	};
	
	dojo.global.firstElement = function(parentNode, tagName) {
		var node = parentNode.firstChild;
		while (node && node.nodeType != 1) {
			node = node.nextSibling;
		}
		if (tagName && node && node.tagName && node.tagName.toLowerCase() != tagName.toLowerCase()) {
			node = nextElement(node, tagName);
		}
		return node;
	};
	
	dojo.global.lastElement = function(parentNode, tagName) {
		var node = parentNode.lastChild;
		while (node && node.nodeType != 1) {
			node = node.previousSibling;
		}
		if (tagName && node && node.tagName && node.tagName.toLowerCase() != tagName.toLowerCase()) {
			node = prevElement(node, tagName);
		}
		return node;
	};
	
	dojo.global.disableSelection = function(element) {
		element = dojo.byId(element) || dojo.body();
		
		if (dojo.isFF) {
			element.style.MozUserSelect = "none";
		} else {
			if (dojo.isWebKit) {
				element.style.KhtmlUserSelect = "none";
			} else {
				if (dojo.isIE) {
					element.unselectable = "on";
				} else {
					return false;
				}
			}
		}
		return true;
	};
	
	dojo.global.insertBefore = function(node, ref, force) {
		if ((force != true) && (node === ref || node.nextSibling === ref)) {
			return false;
		}
		var parent = ref.parentNode;
		parent.insertBefore(node, ref);
		return true;
	};
	
	dojo.global.insertAfter = function(node, ref, force) {
		var pn = ref.parentNode;
		if (ref == pn.lastChild) {
			if ((force != true) && (node === ref)) {
				return false;
			}
			pn.appendChild(node);
		} else {
			return insertBefore(node, ref.nextSibling, force);
		}
		return true;
	};
	
	dojo.global.getValue = function(s, def) {
		if (typeof def == 'undefined') def = '';
		var result;
		var el = dojo.byId(s);
		if (!el) {
			return def;
		}
		switch (el.tagName) {
			case 'TEXTAREA':
				result = el.value;
				break;
			case 'INPUT':
			case 'SELECT':
				if (el.type == 'checkbox') {
					result = el.checked ? (el.value ? el.value : 'on') : def;
				} else if (el.type == 'radio') {
					result = el.checked ? el.value : def;
				} else {
					result = el.value;
				}
				break;
			default:
				result = el.innerHTML;
				break;
		}
		return result;
	};
	
	dojo.global.setValue = function(s, val) {
		var el = dojo.byId(s);
		if (!el) {
			return false;
		}
		if (typeof val == 'undefined') {
			val = '';
		}
		switch (el.tagName) {
			case 'TEXTAREA':
				el.value = val;
				break;
			case 'INPUT':
			case 'SELECT':
				if (el.type == 'checkbox' || el.type == 'radio') {
					el.checked = val;
				} else {
					el.value = val;
				}
				break;
			default:
				el.innerHTML = val;
				break;
		}
		return true;
	};
	
	dojo.global.addslashes = function(str) {
		if (str && str.toString) {
			str = str.toString();
		} else {
			str = '';
		}
		str = str.replace(/\\/g, '\\\\');
		str = str.replace(/"/g, '\\\"');
		str = str.replace(/'/g, '\\\'');
		return str;
	};
	
	dojo.global.htmlspecialchars = function(ch) {
		if (typeof ch == 'undefined') {
			ch = '';
		}
		if (ch.toString) {
			ch = ch.toString();
		} else {
			ch = '';
		}
		ch = ch.replace(/&/g,"&amp;");
		ch = ch.replace(/\"/g,"&quot;");
		ch = ch.replace(/\'/g,"&#039;");
		ch = ch.replace(/</g,"&lt;");
		ch = ch.replace(/>/g,"&gt;");
		return ch;
	};
	
	dojo.global.unhtmlspecialchars = function(ch) {
		if (typeof ch != 'string') {
			ch = '';
		}
		ch = ch.replace(/&quot;/g,'"');
		ch = ch.replace(/&#039;/g,"'");
		ch = ch.replace(/&lt;/g,"<");
		ch = ch.replace(/&gt;/g,">");
		ch = ch.replace(/&amp;/g,"&");
		return ch;
	};
	
	dojo.global.parseIntRight = function(s) {
		s = s || '';
		var t = '';
		var l = s.length;
		while ((isFinite(parseInt(s.substr(l - 1, 1))) || s.substr(l - 1, 1) == '-') && t.substr(0, 1) != '-') {
			t = s.substr(l - 1, 1) + t;
			s = s.substr(0, l - 1);
			l--;
		}
		if (t == '') t = 0;
		return t;
	};
	
	dojo.global.childNode = function(el, n) {
		if (typeof n == 'undefined') {
			n = 0;
		}
		el = dojo.byId(el);
		var els = el.childNodes;
		var imax = els.length;
		var i = 0;
		while (n >= 0 && i < imax) {
			if (els[i].nodeType == 1) {
				n--;
			}
			i++;
		}
		if (n >= 0) return false;
		else return els[i - 1];
	};
	
	dojo.global.elementVisible = function(el) {
		el = dojo.byId(el);
		if (!el) return false;
		return !(el.style.display == 'none' || dojo.hasClass(el, 'hidden'));
	};
	
	dojo.global.hideElement = function(el) {
		el = dojo.byId(el);
		if (!el) return false;
		dojo.addClass(el, 'hidden');
		return true;
	};
	
	dojo.global.showElement = function(el) {
		el = dojo.byId(el);
		if (!el) return false;
		el.style.display = '';
		
		dojo.removeClass(el, 'hidden');
		return true;
	};
	
	var _ableElements = function(cont, enable) {
		var inputs = dojo.query('input,textarea,select', cont);
		var i;
		for (i = 0; i < inputs.length; i++) {
			inputs[i].disabled = !enable;
		}
	};

	dojo.global.disableElements = function(cont) {
		return _ableElements(cont, false);
	};

	dojo.global.enableElements = function(cont) {
		return _ableElements(cont, true);
	};
	
	dojo.global.getParentByClassQuick = function(el, cn1) {
		el = dojo.byId(el);
		while (el && el.className && (el.className.indexOf(cn1) == -1)) {
			el = el.parentNode;
		}
		return el;
	};
	
	dojo.global.getParentByClass = function(el, cn1, cn2) {
		el = dojo.byId(el);
		while (el && !dojo.hasClass(el, cn1)) {
			el = el.parentNode;
		}
		if (cn2) {
			return to_array(dojo.query('.' + cn2, el)).pop();
		} else {
			return el;
		}
	};
	
	/**
	 * getSelRange - returns selection start, end and selection text for input element t
	 */
	dojo.global.getSelRange = function(t) {
		if (typeof t == 'undefined') return false;
		var start = 0;
		var end = 0;
		var txt = '';
	
		/**
		 * Mozilla
		 */
		if (t.selectionStart || t.selectionEnd)
		{
			start = t.selectionStart;
			end = t.selectionEnd;
		}
	
		/**
		 * IE
		 */
		else if (document.selection)
		{
			var range = document.selection.createRange();
			var stored_range = range.duplicate();
			try {
				stored_range.expand('textedit');
				stored_range.setEndPoint('EndToEnd', range);
				start = stored_range.text.length - range.text.length;
				end = start + range.text.length;
			} catch (e) {
				/* Opera BUG */
				start = 0;
				end = 0;
			}
		}
		txt = t.value.substring(start, end);
		return {start:start,end:end,txt:txt};
	};
	
	dojo.global.permitExpression = function(e, t, regexp) {
		if (typeof e == 'undefined')return false;
		if (typeof t == 'undefined') return false;
		if (typeof regexp == 'undefined') return false;
		var range = getSelRange(t);
		var key = (e.which) ? e.which : (typeof event != 'undefined' ? event.keyCode : false);
		if (!key || key < 32) return true;
		if (e.ctrlKey) return true;
		var txt = t.value;
		txt = txt.substr(0, range.start) + String.fromCharCode(key) + txt.substr(range.end);
		if (!regexp.test(txt)) {
			e.returnValue = false;
			return false;
		} else {
			return true;
		}
	};
	
	/**
	 * Escaping string to put in regular expression
	 */
	dojo.global.escapeExpression = function(s) {
		return s.replace(/([\/\\\+\-\^\(\)\[\]\.\*\?\$])/g, '\\\$1');
	};
	
	dojo.global.Click = {
		click: 0,
		clickFunctions: [],
		names: [],
		setClickFunction: function(func, el, name) {
			var ex = false;
			var i, n, imax = Click.names.length;
			if (name) {
				for (i = 0; i < imax; i++) {
					if (Click.names[i] == name) {
						ex = true;
						
						break;
					}
				}
			}
			if (ex) {
				n = 1 << i;		//0x100000 n = 5, 0x1000 n = 3
			} else {
				Click.clickFunctions.push(func);
				Click.names.push(name);
				n = 1 << (Click.clickFunctions.length - 1);		//0x10000
			}
			if (el) Click.click |= n;
			return n;
		},
		removeClickFunction: function(name) {
			var i, imax = Click.names.length;
			for (i = 0; i < imax; i++) {
				if (Click.names[i] == name) {
					Click.names.splice(i, 1);
					Click.clickFunctions.splice(i, 1);
					return true;
				}
			}
			return false;
		},
		onclick: function(e) {
			e = e || window.event;

			var isFromCalendarPopup = (function() {
				var ps = dojo.query(e.target).closest('.dijitCalendarPopup');

				return ps && ps.length;
			})();

			var isFromFormattingHelp = (function() {
				var dom = dojo.query(e.target).closest('.dijitTooltipDialog');

				return dom && dom.length && dom[0].id === 'tooltip_formattinghelp_TooltipDialog';
			})();

			if ((e.which && e.which != 1 )|| isFromCalendarPopup ||
				isFromFormattingHelp || !e.target || !e.target.parentNode) 
				return Click.click = 0;;
				// || (isFromPopup && !dojo.hasClass(e.target, 'formating-help')) || ) return;

			func = Click.clickFunctions;
			if (func) {
				var i, imax = func.length;
				for (i = 0; i < imax; i++) {
					if (!(Click.click & (1 << i))) {
						try {
							if (func[i]) {
								func[i]();
							}
						} catch (e) {
							console.warn(e);
							if (e.stack) {
								console.debug(e.stack);
							}
						}
					}
				}
				Click.click = 0;
			}
		}
	};
	
	dojo_on(document, 'click', Click.onclick);
	
	dojo.global.replaceHTML = function(el, html) {
		var oldEl = typeof el === "string" ? document.getElementById(el) : el;
		/*@cc_on
			oldEl.innerHTML = html;
			return oldEl;
		@*/
		var newEl = oldEl.cloneNode(false);
		newEl.innerHTML = html;
		oldEl.parentNode.replaceChild(newEl, oldEl);
		return newEl;
	};
	
	dojo.global.fillStringTemplate = function(el, data, excludeEscaping) {
		excludeEscaping = excludeEscaping || [];
		for (var i in data) {
			el = el.split('%' + i + '%').join(dojo.indexOf(excludeEscaping, i) != -1 ? data[i] : htmlspecialchars(data[i]));
		}
		return el;
	};
	
	dojo.global.fillTemplate = function(el, data, clear) {
		var parseStringLogic = function (data, ev) {
			//ev = "category == 2 && id == 15) && 'id' != '15'";
			var i, x;
			for (i in data) {
				if (!i || i.trim() == '') continue;
				if (data[i] == undefined) continue;
				x = new RegExp('(^|[^\'"0-9a-zA-Z_])(' + i + ')([^\'"0-9a-zA-Z_]|$)', 'g');
				if (data[i] !== null && data[i].constructor == String) {
					ev = ev.replace(x, '$1"' + data[i] + '"$3');
				} else {
					ev = ev.replace(x, '$1' + data[i] + '$3');
				}
			}
			var err;
			try {
				return eval(ev);
			} catch (err) {
				return false;
			}
		};
		
		var getKeyValue = function(key, data) {
			key = key.trim();
			if (/^\d+$/.test(key)) {
				return key;
			}
			if (key[0] == '\'') {
				return key.substr(1, key.length() - 1);
			}
			if (key in data) {
				return data[key];
			} else {
				return '';
			}
		};

		var i, j;

		el = dojo.byId(el);
		if (!data) return true;
		if (data.constructor != Object) {
			data = templateParse(data);
		}
		var empty = true;
		for (i in data) {
			empty = false;
			break;
		}
		if (empty) return true;
		var fill = el.getAttribute ? el.getAttribute('fill') : false;
		
		if (clear) {
			try {
				el.removeAttribute('fill');
			} catch (err) {}
		}
		
		if (fill) {
			fill = templateParse(fill);
			var value, valueIf;
			for (i in fill) {
				if (i != 'call') {
					try {
						// filter simple variable values, will increase template parsing
						if (!(/[^a-zA-Z0-9_]/.exec(fill[i]))) {
							throw '';
						}
						value = parseStringLogic(data, fill[i]);
						if (typeof value == 'object') {
							throw 'Value cannot be an object';
						}
					}
					catch (e) {
						value = (fill[i] ? getKeyValue(fill[i], data) : '');
					}
				} else {
					value = fill[i];
				}
				
				valueIf = value? true : false;
				if (value == '0') valueIf = false;
				if (i.substr(0, 6) == 'style-') {
					j = i.substr(6);
					el.style[j] = value;
				} else if (i.substr(0, 8) == 'replace-') {
					var pos = i.indexOf('-', 8);
					var attr = i.substr(pos + 1);
					j = i.substring(8, pos);
					try {
						if (attr == 'class') {
							try {
								el.setAttribute('className', el.getAttribute('className').replace(j, value));
							} catch (err) {}
						}
						else if (attr == 'for') {
							try {
								if (el.getAttribute('htmlFor')) {
									el.setAttribute('htmlFor', el.getAttribute('htmlFor').replace(j, value));
								}
							} catch (err) {}
						}
						el.setAttribute(attr, el.getAttribute(attr).replace(j, value));
					} catch (err) {}
				} else {
					var emptyText;

					switch (i) {
						case "call":
							try { eval(value); } catch (e) {}
							break;
						case "html":
							el.innerHTML = '';
							el.appendChild(document.createTextNode(value));
							break;
						case "htmlUntrim":
							el.innerHTML = '';
							el.appendChild(document.createTextNode(value + ' '));
							break;
						case "htmlRaw":
							el.innerHTML = value;
							break;
						case "addHtml":
							el.appendChild(document.createTextNode(value + ' '));
							break;
						case "addHtmlRaw":
							el.innerHTML += value;
							break;
						/*Fix IE7 no delimiter space bug*/
						case "addSpace":
							emptyText = document.createTextNode(' ');
							el.appendChild(emptyText);
							break;
						case "addSpaceAfter":
							emptyText = document.createTextNode(' ');
							el.appendChild(emptyText);
							break;
						/*Adds class to element*/
						case "addClass":
							dojo.addClass(el, value);
							break;
						/*Overwrite element class*/
						case "class":
							el.className = value;
							break;
						/*Sets value*/
						case "value":
							if (el.type == 'checkbox') {
								el.checked = valueIf;
							} else {
								el.value = value;
							}
							break;
						/*Sets value if provided value not empty*/
						case "ifValueEnabled":
							
							break;
						/*Enables if value is true, disables if false*/
						case "enabled":
							el.disabled = !valueIf;
							break;
						/*Disables if value is true, enables if false*/
						case "disabled":
							el.disabled = valueIf;
							break;
						case "writeable":
							el.disabled = !valueIf;
							break;
						case "readonly":
							el.readOnly = valueIf;
							break;
						case "visible":
							if (valueIf) {
								dojo.removeClass(el, 'hidden');
							} else {
								dojo.addClass(el, 'hidden');
							}
							break;
							
						case "remove":
							if (valueIf) {
								removeNode(el, 0);
								return false;
							}	
							break;
						case "hidden":
							if (valueIf) {
								dojo.addClass(el, 'hidden');
							} else {
								dojo.removeClass(el, 'hidden');
							}
							break;
						case "data":
							if (fill[i] == '^') {
								el.setAttribute("data", "^");
							}
							break;
						default:
							el.setAttribute(i, value);
							break;
					}
				}
			}
		}
		var r, e, ch = true;
		try{
			r = el.getAttribute('data');
		} catch (e) {
			ch = false;
		}
		
		if (ch && r && (r == '^')) {
			var o = '';
			for (i in data) {
				o += i + ': ' + templateEscape(data[i]) + '; ';
			}
			el.setAttribute('data', o);
		}
		var els = el.childNodes;
		
		var imax = els.length;
		for (i = 0; i < imax; i++) {
			if (els[i].nodeType == 1) {
				fillTemplate(els[i], data, clear);
			}
		}
		return true;
	};
	
	dojo.global.addTemplate = function(parent, template, data) {
		if (!data || data.constructor != Object) {
			data = {value: data};
		}
		
		parent = dojo.byId(parent);
		//if (!parent) return;
		template = dojo.byId(template);
		if (!template) return;
		template = template.cloneNode(true);
		template.removeAttribute('id');
	
		var res = fillTemplate(template, data, true);
		
		if (!res) {
			return false;
		}
		if (!parent) {
			return template;
		}
		parent.appendChild(template);
	};
	
	dojo.global.addTemplateList = function(parent, template, data, emptyEl, start, end) {
		start = start || 0;
		end = end || 0;
		var k = 0;
		var _addChild = function(parent, template, data) {
			var res = addTemplate(null, template, data);
			if (res) {
				if (k >= start && (end == 0 || k < end)) {
					parent.appendChild(res);
				}
				k++;
			}
			return res;
		};
		template = dojo.byId(template);
		parent = dojo.byId(parent);
		var i;
		if (!data) return;
		var empty = true;
		var emptyGlobal = false;
		removeNode(parent, false);
		if (data.constructor == Array) {
			if (data.length > 0) {
				empty = false;
				for (i = 0; i < data.length ; i++) {
					
					result = _addChild(parent, template, data[i]);
					if (!result && emptyGlobal == false) { 
						empty = true;
					}
					else {
						emptyGlobal = true;
					}	
					if (end > 0 && k >= end) return;
				}
			}
		} else if (data.constructor == Object) {
			for (i in data) {
				empty = false;
				result = _addChild(parent, template, data[i]);
				if (!result && emptyGlobal == false) { 
					empty = true;
				}
				else {
					emptyGlobal = true;
				}
				if (end > 0 && k >= end) return;
			}
		}
		if (empty) {
			addTemplate(parent, emptyEl, []);
		}
	};
	
	dojo.global.templateParse = function(data) {
		if (!data) return {};
		var o = {};
		var a = data.split(";");
		var i, imax = a.length;
		for (i = 0; i < imax; i++) {
			if (a[i] != "") {
				a[i] = templateUnescape(a[i]);
				var pos = a[i].indexOf(":");
				var k = a[i].substr(0, pos);
				k = k.replace(/(^\s*)|(\s*$)/g, "");
				var v = a[i].substr(pos + 1);
				v = v.replace(/(^\s*)|(\s*$)/g, "");
				o[k] = v;
			}
		}
		return o;
	};
	
	dojo.global.templateEscape = function(data) {
		if (data === false) {
			data = '';
		}
		data = data + "";
		data = data.replace(/&/g, "&amp");
		data = data.replace(/;/g, "&sem");
		data = data.replace(/(^\s*)|(\s*$)/g, "");
		return data;
	};

	dojo.global.templateUnescape = function(data) {
		data = data || "";
		data = data + "";
		data = data.replace(/&sem/g, ";");
		data = data.replace(/&amp/g, "&");
		data = data.replace(/(^\s*)|(\s*$)/g, "");
		return data;
	};
	
	dojo.global.openTab = function(tab_id, num, start, data) {
		var i = start || 0;
		var tab;
		while (tab = dojo.byId(tab_id + '_' + i)) {
			hideElement(tab);
			i++;
		}
		tab = dojo.byId(tab_id + '_' + num);
		if (tab) {
			if (data) {
				fillTemplate(tab, data);
			}
			showElement(tab);
			
			//#563 
			tab.style.position = 'relative';
			tab.style.zIndex = 1;
			
			var focus = dojo.query('.focus', tab);
			if (focus.length > 0) {
				var e;
				try {focus[0].focus();} catch (e) {}
			}
		}
	};
	
	dojo.global.findPosX = function(obj, rel)
	{
		if (rel) {
			var pos = dojo.position(obj, true);
			return pos.x;
		}
		rel = rel || false;
		var curleft = 0;
		if (obj.offsetParent)
		{
			while (obj && (rel || dojo.getComputedStyle(obj).position != 'relative'))
			{
				curleft += obj.offsetLeft;
				ALERT(obj.nodeName, obj.offsetLeft);
				//obj.style.border ='1px solid red'
				obj = obj.offsetParent;
			}
		}
		else if (obj.x)
			curleft += obj.x;
		return curleft;
	};
	
	dojo.global.findPosY = function(obj, rel)
	{
		if (rel) {
			var pos = dojo.position(obj, true);
			return pos.y;
		}
		rel = rel || false;
		var curtop = 0;
		if (obj.offsetParent)
		{
			while (obj && (rel || dojo.getComputedStyle(obj).position != 'relative'))
			{
				curtop += obj.offsetTop;
				obj = obj.offsetParent;
			}
		}
		else if (obj.y)
			curtop += obj.y;
		return curtop;
	};
	
	dojo.global.findTextWidth = function(el) {
		var tmp = document.createElement('SPAN');
		var html = el.innerHTML;
		
		html = html.replace(/\<[^>]*>/g, '');
		tmp.innerHTML = html;
		
		document.body.appendChild(tmp);
		var tmp_width = tmp.offsetWidth;
		document.body.removeChild(tmp);
		// delete(tmp);
		
		return tmp_width;
	};
	
	dojo.global.getTimeZoneOffset = function (period, season) {
		var periodFactor = 1;
		period = period || 'm'; //show in minutes by default
		
		if(period == 's'){
			periodFactor = 60;
		}else if(period == 'ms'){
			periodFactor = 60 * 100;
		}else if(period == 'm'){
			periodFactor = 1;
		}
		
		var d = new Date();
		
		if(season == 'winter'){
			d = new Date(2030, 0, 30, 0, 0, 0, 0);
		}else if(season == 'summer'){
			d = new Date(2030, 6, 30, 0, 0, 0, 0);
		}
		
		return  -periodFactor * d.getTimezoneOffset();
	};
	
	dojo.global.time2strAPI = function (time) {
		if(!time || time.constructor != Date){
			return false;
		}
		return dojo.date.stamp.toISOString(time,{milliseconds: true});
	};
	
	dojo.global.str2timeAPI = function(str) {
		return typeof(str) == 'string' ? dojo.date.stamp.fromISOString(str) : false;
	};
	
	var component_regex = /(d|m|y|h|i|s)/g;
	dojo.global.str2time = function(str, format, UTC) {
		// console.log(str, format);
		if (!str) return false;

		if (str.indexOf('T') > 0) {		//don't lose timezone
			str = moment(str);
			if (!str.isValid()) return false;
			str = str.format();
		}

		// console.log(str, format);
		var mo, k;
		var formatMapping = {
			'y': 'YYYY',
			'm': 'MM',
			'd': 'DD',
			'h': 'HH',
			'i': 'mm',
			's': 'ss'
		};

		if (format) {
			for (k in formatMapping) {
				format = format.replace(k, formatMapping[k]);
			}
		}

		if (UTC) {
			mo = moment.utc(str, format);
		} else {
			mo = moment(str, format);
		}

		if (mo.isValid()) return mo.toDate();
		return false;
	};
	
	dojo.global.time2str = function(time, format, UTC) {
		var mo;

		UTC = UTC || false;
		if (typeof time === 'string') {
			time = time.toLowerCase();
			switch (time) {
				case 'now':
					time = new Date();
					break;
				case 'yesterday':
					time = new Date();
					time = new Date(time.valueOf() - 24 * 60 * 60 * 1000);
					break;
				case 'tomorrow':
					time = new Date();
					time = new Date(time.valueOf() + 24 * 60 * 60 * 1000);
					break;
			}
		}
		// if (time.constructor != Date) return;
		if (!(time instanceof Date)) return;

		if (UTC) {
			mo = moment.utc(time);
		} else {
			mo = moment(time);
		}
		var formatMapping = {
			'y': 'YYYY',
			'm': 'MM',
			'd': 'DD',
			'h': 'HH',
			'i': 'mm',
			's': 'ss'
		}, k;

		for (k in formatMapping) {
			format = format.replace(k, formatMapping[k]);
		}

		var formated = mo.format(format);
		// console.log(formated);
		// console.log('==================');
		return formated;
	};
	
	dojo.global.time24_to_time12 = function(timeStr) {
		if (Hi_Calendar.time_format == 12 && typeof(timeStr) == 'string' && timeStr.length > 0) {
			var splitted = timeStr.split(':');
			
			if (splitted.length != 2) {
				return timeStr;
			}
			
			var hours = splitted[0].length > 0 ? splitted[0] : '0';
			if (hours.charAt(0) == '0' && hours.length > 1) {
				hours = hours.substring(1);
			}
			hours = parseInt(hours);
			var minutes = splitted[1].length > 0 ? splitted[1] : '0';
			
			var h = hours % 12;
			if (h === 0) h = 12;
			return (h < 10 ? "0" + h : h) + ":" + minutes + (hours < 12 ? ' AM' : ' PM');
		} else {
			return timeStr;
		}
	};
	
	/**
	 * Parse time and return as decimal number or formated string
	 * Example:
	 * 		parseTimeString("1:30") -> 1.5
	 * 
	 * @param {Object} str
	 * 		Formats which must be accepted are describe in #82
	 * 
	 * @param {Object} format
	 * 		'decimal' - will return as decimal number (hours)
	 * 		'text' - will return in format "1h 30m"
	 * 		'%h hours %m minutes' - will return "1 hours 30 minutes"
	 */
	dojo.global.parseTimeString  = function(str, format) {
		if (!format) format = 'minutes';
		 
		var res, t, h=0, m=0, matches = false;
		
		if (typeof str == 'number') {
			m = str / 60;
			matches = true;
		} else {
			//Trim
			if (typeof str == 'string') str = str.replace(/^\s+|\s+$/g, '');
			
			if (res = str.match(/^(\d*)(:)(\d+)$/)) {
				h = parseFloat(res[1]) || 0;
				m = parseFloat(res[3]) || 0;
				m = m / 60;
				matches = true;
			} else if (res = str.match(/^(\d+)[ ]+(\d+)$/)) {
				h = parseFloat(res[1]) || 0;
				m = parseFloat(res[2]) || 0;
				m = m / 60;
				matches = true;
			} else if (res = str.match(/^(\d+)(h | h | hour |hour | hours |hours )(\d+)( m|m| min|min| minute|minute| minutes|minutes)$/)) {
				h = parseFloat(res[1]) || 0;
				m = parseFloat(res[3]) || 0;
				m = m / 60;
				matches = true;
			} else if (res = str.match(/^(\d+)(h| h| hours| hour)$/)) {
				h = parseFloat(res[1]) || 0;
				matches = true;
			} else if (res = str.match(/^(\d+)(m| min| minutes| minute)$/)) {
				m = parseFloat(res[1]) || 0;
				m = m / 60;
				matches = true;
			} else if (res = str.match(/^(\d*\.?\d*)(h| h| hours| hour)?$/)) {
				h = parseFloat(res[1]);
				if (!isNaN(h)) {
					matches = true;
				}
			}
		}
		
		if (matches) {
			if (format == 'decimal') {
				t = h+m;
				if (t > 32767) t = 32767;
			} else if (format == 'minutes') {
				t = Math.round((h+m) * 60);
				if (t > 32767) t = 32767;
			} else {
				//.23 from hours add to minutes
				m += h%1;
				h = ~~h;
				
				//Convert 0.5 into 30
				m = m*60;
				h += ~~(m/60);
				m = Math.round(m%60);
				if (h > 32767) h = 32767;
				
				//Format time
				if (format == 'text'){
					t = (h ? h + 'h' + (m ? ' ' : '') : '') + (m ? m + 'm' : '') + (!h && !m ? '0h' : '');
				} else {
					t = format.replace(/\%h/g, h).replace(/\%m/g, m);
				}
			}
		} else {
			//Doesn't matches any pattern
			t = null;
		}
		
		return t;
	};

	dojo.global.time2LocalizedStr = function(date, options) {
		if (!date) date = new Date();
		var now = new Date();
		var dateFormatted;

		options = dojo.mixin({
			format: 'm dy',
			format_year: ' &#146;y',
			add_year: null,			//null - auto detect, true - add, false - don't add
			use_locale_today: true
		}, options);
		
		if (options.use_locale_today && date.getFullYear() === now.getFullYear() && date.getMonth() == now.getMonth() && date.getDate() == now.getDate()) {
			dateFormatted = hi.i18n.getLocalization('calendar.today').toLowerCase();
		} else {
			dateFormatted = options.format;
			
			if (options.add_year !== false) {
				if (date.getFullYear() !== now.getFullYear() || options.add_year === true) {
					var y = options.format_year.replace('Y', date.getFullYear());
						y = y.replace('y', String(date.getFullYear()).substr(2));
						
					dateFormatted = dateFormatted.replace('y', y);
				} else {
					dateFormatted = dateFormatted.replace(/\s*(y|Y)/, '');
				}
			} else {
				dateFormatted = dateFormatted.replace(/\s*(y|Y)/, '');
			}
			
			var months = capitalizeFirstLetter(djDateLocale.getNames('months', 'abbr'));
			dateFormatted = dateFormatted.replace('m', months[date.getMonth()]);
			dateFormatted = dateFormatted.replace('d', date.getDate());
		}
		
		return dateFormatted;
	};

	dojo.global.formatTrackTime = function(t) {
		t = parseTimeString(t || 0, 'decimal');
		t = t.toFixed(2).replace(/\.(\d)0$/, ".$1");	//12.10 -> 12.1
		return '<span class="t">' + t +' '+ hi.i18n.getLocalization('properties.time_tracking_hours') + '</span>';
	};
	
	dojo.global.validateTime = function(s, value, secondTry) {
		value = value || false;
		secondTry = secondTry || false;
		var re = /^([0-9]{1,2}):([0-9]{1,2})$/.exec(s);
		if (!re) {
			if (secondTry) {
				return false;
			} else {
				return validateTime(HiTaskCalendar.standartTime(s), value, true);
			}
		}
		var h = parseInt(RegExp.$1, 10);
		var m = parseInt(RegExp.$2, 10);
		if (h < 0 || h > 24) return false;
		if (m < 0 || m > 59) return false;
		var t = 60 * h + m;
		if (t < 0 || t > 1440) return false;
		if (value) {
			return t;
		} else {
			return [h, m];
		}
	};
	
	dojo.global.strPadLeft = function(str, num, ch) {
		str = str.toString();
		ch = ch.toString();
		while (str.length < num && ch.length > 0) {
			str = ch + str;
		}
		return str;
	};
	
	dojo.global.ismaxlength = function(obj, cnt, cont, limit, alertText, alertFunction) {
		alertText = alertText || false;
		alertFunction = alertFunction || alert;
		cnt = cnt || false;
		cont = cont || false;
		var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : "";
		limit = limit || mlength;
		var ret = true;
		if (obj.getAttribute && obj.value.length > mlength) {
			obj.value = obj.value.substring(0, mlength);
			if (alertText) 
				alertFunction(alertText);
			ret = false;
		}
		if (cnt) {
			var c = mlength - obj.value.length;
			setValue(cnt, c);
			if (cont) {
				if (c > limit) {
					hideElement(cont);
				} else {
					showElement(cont);
				}
			}
		}
		return ret;
	};
	
	dojo.global.innerText = function(node, text) {
		node = dojo.byId(node);
		if (!node) {
			return;
		}
		text = (text || '').toString();
		if (text.length > 0) {
			var tmp = document.createTextNode(text);
			removeNode(node, false);
			node.appendChild(tmp);
		}
	};
	
	dojo.global.showLoading = function() {
		try {
			dojo.addClass(document.body.parentNode, 'loading');
		} catch (err) {}
	};
	
	dojo.global.checkLoadingStatus = function() {
		if (arguments.callee.prevCount == imageCached.count) {
			dojo.global.clearInterval(preloadImages.interval);
			preloadDone();
		} else {
			arguments.callee.prevCount = imageCached.count;
		}
	};
	
	//a deferred object to resolve when main content 
	//is showing and loading overlay is hidden
	dojo.global.preLoadDoneDeferred = new Deferred();

	dojo.global.reLayout = function() {
		var timetable = dojo.query('.timetable')[0],
			mainContainer = dojo.byId('mainContainer'),
			appContainer = dojo.byId('appContainer'),
			team_scroll = dojo.byId('team_scroll'),
			winH = dojo.window.getBox().h,
			main = dojo.byId('main'),
			appContainerPaddingBottom;

		if (!timetable || !mainContainer || !appContainer || !team_scroll || !main) return;

		appContainerPaddingBottom = dojo.style('appContainer', 'paddingBottom');
		if (dojo.hasClass(main, 'itemNewEdit')) return;

		maxHeight = appContainer.clientHeight - dojo.position(mainContainer).y - appContainerPaddingBottom;
		mainContainer.style.maxHeight = maxHeight + 'px';
	};

	dojo.global.preloadDone = function() {
		dojo.global.setTimeout(function(){
			var d = dojo.global.preLoadDoneDeferred;
			dojo.global.hideThrobber();
			dojo.removeClass(document.body.parentNode, 'loading');
			if (dijit.byId('appContainer')) {
				dijit.byId('appContainer').resize();
			}
			dojo.global.reLayout();
			d.resolve();
		}, 200);

		if (dojo.global.imagesLast) {
			preloadImages(dojo.global.imagesLast);
			dojo.global.imagesLast = null;
		}
	};

	dojo.global.throbberPosition = null;
	dojo.global.showThrobber = function(msg, marker) {
		try{
			if (msg) {
				dojo.byId('loadingThrobberMsg').innerHTML = msg;
			} else {
				dojo.byId('loadingThrobberMsg').innerHTML = loadingPageMsg;
			}
			var box = dijit.getViewport();
			var throbberBox = {w:400,h:121};
			var x = (box.w - throbberBox.w)/2;
			var y = (box.h - throbberBox.h)/2;
			dojo.style('loading_overlay','height',box.h+'px');
			dojo.style('loading_overlay','width',box.w+'px');
			dojo.style('loadingPageThrobber','top',y+'px');
			dojo.style('loadingPageThrobber','left',x+'px');
			dojo.style('loading_overlay','display','');
			throbberPosition = dojo.clone(dojo.position('loadingPageThrobber',true));
			
			if (marker) {
				dojo.attr('loading_overlay', 'data-marker', marker);
			}
		} catch(e) {
			console.log('e',e);
		}
	};

	dojo.global.hideThrobber = function(marker)
	{
		var thisMarker = dojo.getAttr('loading_overlay', 'data-marker');
		if (!thisMarker || thisMarker == marker) {
			dojo.style('loading_overlay','display','none');
			dojo.removeAttr('loading_overlay', 'data-marker');
		}
	};
	
	dojo.global.imageCached = function(count, barCount) {
		if (!arguments.callee.count) {
			arguments.callee.count = 0;
			arguments.callee.part = Math.round(count / barCount);
			//document.getElementById('loading_progress').className = 'started';
		}
		arguments.callee.count++;
		if (arguments.callee.count == count) {
 			preloadDone();
		}
		var el;
		if (arguments.callee.count % arguments.callee.part == 1) {
			if (el = document.getElementById('preload_progress_bar_' + Math.round((arguments.callee.count - 1) / arguments.callee.part))){
				el.className = '';
			}
		}
	};
	
	dojo.global.preloadImages = function(images, onload, barCount) {
		showThrobber();
		dojo.global.setTimeout(preloadDone, 60000);
		barCount = barCount || 1;
		onload = onload || function(){};
		var obj = [];
		var i, imax = images.length;
		var new_onload = function() {
			onload(imax, barCount);
		};

		for (i = 0; i < imax; i++) {
			obj[i] = new Image();
			obj[i].onload = new_onload;
			obj[i].onerror = new_onload;
			obj[i].onabort = new_onload;
			obj[i].src = images[i];
		}
		if (!('interval' in arguments.callee) && typeof imageCached != 'undefined') {
			arguments.callee.interval = dojo.global.setInterval(checkLoadingStatus, 5000);
		}
	};
	
	dojo.global.tasksListLoading = function() {
		var container = dojo.byId(Project.getContainer('tasksList'));
		dojo.addClass(container, "loading");
	};

	dojo.global.bigCalendarLoading = function() {
		var container = dojo.byId('bigCalendar');
		container && dojo.addClass(container, "loading");
	};
	
	dojo.global.toggleClass = function(element, class1, class2) {
		element = dojo.byId(element);
		if (!element) return false;
		if (dojo.hasClass(element, class1)) {
			dojo.removeClass(element, class1);
			dojo.addClass(element, class2);
		} else {
			dojo.removeClass(element, class2);
			dojo.addClass(element, class1);
		}
		return true;
	};
	
	dojo.global.check_variable_type = function(variable, type) {
		if (variable && variable.constructor == type) {
			return true;
		} else {
			return false;
		}
	};
	
	dojo.global.to_array = function(obj) {
		if (!obj) return [];
		var r = [];
		for(var i=0; typeof obj[i] != 'undefined'; i++) {
			r.push(obj[i]);
		}
		
		return r;
	};
	
	dojo.global.in_array = function(el, array) {
		if (!dojo.isArray(array)) {
			if (dojo.isArray(el)) {
				var _tmp = el;
				el = array;
				array = _tmp;
			} else {
				return false;
			}
		}
		return (dojo.indexOf(array, el) != -1 ? true : false);
	};
	
	dojo.global.array_cmp = function(a1, a2) {
		if (dojo.isArray(a1) && dojo.isArray(a2)) {
			var i;
			var imax = Math.min(a1.length, a2.length);
			for (i = 0; i < imax; i++) {
				if (a1[i] != a2[i]) {
					return a1[i] < a2[i] ? 1 : -1;
				}
			}
			if (a1.length != a2.length) {
				return a1.length < a2.length ? 1 : -1;
			}
			return 0;
		} else {
			return false;
		}
	};
	
	dojo.global.object_cmp = function(_o1, _o2) {
		if (dojo.isObject(_o1) && dojo.isObject(_o2)) {
			var o1 = dojo.clone(_o1);
			var o2 = dojo.clone(_o2);
			if(objectLength(o1) != objectLength(o2)){
				return false;
			}
			
			for(var i in o1){
				if(i in o2){
					if(dojo.isArray(o1[i])){
						if(array_cmp(o1[i],o2[i]) !== 0){
							return false;
						}
					}else if(dojo.isObject(o1[i])){
						if(!object_cmp(o1[i],o2[i])){
							return false;
						}
					}else if(o1[i] != o2[i]){
						return false;
					}
				}else{
					return false;
				}
			}
			//delete variable is meaningless here
			// delete o1;
			// delete o2;
		} else {
			return false;
		}
		return true;
	};
	
	dojo.global.objectLength = function(obj) {
		if(dojo.isObject(obj)){
			var length = 0;
			for(var i in obj){
				length++;
			}
			return length;
		}else if(dojo.isArray(obj)){
			return obj.length;
		}else{
			return 0;
		}
	};
	
	dojo.global.serializePg = function(array) {
		if (!dojo.isArray(array)) return null;
		var i;
		var tags = [];
		for (i = 0; i < array.length; i++) {
			tags.push('"' + addslashes(array[i]) + '"');
		}
		tags = '{' + tags.join(',') + '}';
		return tags;
	};
	
	/**
	 * Handle keypress event
	 * 
	 * @param {Object} e
	 */
	dojo.global.js_block_onkeypress = function (e) {
		var keys = dojo.keys,
			key  = e.which || e.keyCode || false,
			el   = e.srcElement || e.target;
		
		e.cancelBubble = true;
		
		if (key == keys.KEY_ENTER && (el.tagName == "TEXTAREA" || (el.tagName == "INPUT" && el.type == "button") || el.tagName == "BUTTON")) return;
		return key;
	};
	
	/**
	 * Submit form using ajax
	 * 
	 * @param {Object} t Form element
	 * @param {Object} validate Validation function
	 * @param {Object} disable Disable form before submit
	 * @param {Object} func Submit function
	 * @param {Object} action Ajax action
	 * @param {Object} success Success callback
	 * @param {Object} error Error callback
	 */
	dojo.global.tpl_function_ajax_submit = function (t, validate, disable, func, action, success, error) {
		ds = dojo.form.getFormValues(t);
		if(validate.constructor != Function || validate(ds)) {
			if (disable) dojo.form.disable(t);
			action = action || t.action;
			var pos = action.indexOf('://');
			if (pos !== false) {
				action = action.substr(action.indexOf('/', pos + 3) + 1);
			}
			func(action, ds,
				function(data, ajaxOptions, ajaxEvt){if(success.constructor == Function)success(data, ds, ajaxEvt); if(disable) dojo.form.enable(t);},
				function(data){if(error.constructor == Function)error(data, ds); if(disable) dojo.form.enable(t);}
			);
		}
	};
	
	/* Bug #1823 */
	dojo.global.clearIntervalTimers = function() {
		var intervals = [Team.intervalBusiness,Team.intervalMessages,Team.intervalContacts,hi.store.Project.intervalTimer];
		intervals.forEach(function(interval) {dojo.global.clearInterval(interval);});
	};
	dojo.global.requestTimeout = 60*1000;
	dojo.global.fireRequest = function(reqObj)
	{
		/* Summary
		 * Fire request specified in reqObj.
		 * If the time allotted for the request to be made has expired, kill the current request
		 * and try again.
		 * 
		 * If the request has gone through, invoke the specified callback
		 * See #1823
		 */
		var timeoutThreshold = reqObj.requestTimeout?reqObj.requestTimeout:dojo.global.requestTimeout;
		//if (reqObj.tag) console.log('fireRequest',reqObj.request);
		try{
		if (reqObj.request)
		{
			var date = new Date().getTime();
			//console.log('time test',[date,reqObj.requestStartTime,dojo.global.requestTimeout,date - reqObj.requestStartTime]);
			if (new Date().getTime() - reqObj.requestStartTime < timeoutThreshold)
			{
				return;
			}

			reqObj.request.cancel();
			reqObj.request = null;
		} 
		var a;
		if (reqObj.doRequestArg1) a = dojo.hitch(reqObj.scope,reqObj.doRequest,reqObj.doRequestArg1);
		else a = dojo.hitch(reqObj.scope,reqObj.doRequest);
		reqObj.request = a();
		reqObj.requestStartTime = new Date().getTime();
		
		} catch(e) {console.log('e',e);}
		
	};
	
	/*
	 * Fire a POST request (for creating a new item
	 * Then use an interval to determine if the request has been fulfilled or not
	 * If not, try to cancel request and clean up
	 * 
	 * See #2766
	 */
	dojo.global.firePOST = function(obj) {
		var deferred = obj.post.apply(obj.post,obj.postArgs);//hi.items.manager.setSave(changed);
		var intervalTimer = null;
		//Bug #5138
		// setInterval(function() {
		// 	// Ajax either successeded or failed due to HTTP status >= 400.
		// 	if ((deferred.isResolved() || deferred.isRejected()) && deferred.isFulfilled())
		// 	{
		// 		clearInterval(intervalTimer);
		// 	} else if (deferred.isRejected() || !deferred.isFulfilled())
		// 	{
		// 		deferred.cancel();
		// 		deferred = obj.post.apply(obj.post,obj.postArgs);//hi.items.manager.setSave(changed);
		// 	}
		// },5000);
		
		return {intervalTimer:intervalTimer, deferred:deferred};
	};
	
	/* OBSOLETE AND NOT USED
	 * #2167 - manager timers for tab switches 
	 *
	 * perhaps deprecate - since timers can run if we load TaskLists in small increments, which takes longer
	 * but makes the UI more interactive and usable
	 */
	dojo.global.monitorTabSwitch = function(groups, lists, intervalTimer, groupSelf)
	{
		intervalTimer = setInterval(function() {
			if (groupSelf != Hi_Preferences.get('currentTab', 'my', 'string')) {
				clearInterval(intervalTimer);
				intervalTimer = null;
				return;
			}
			var nCompleted = 0,
				nLists = 0,
				nGroups = groups.length,
				nUngroupedLists = lists.length,
				nGroupedLists = 0,
				nUngroupedList = 1, l, g, i;
			
			for (l = 0;l<nUngroupedLists;l++) 
			{
				nLists++;
				if (lists[l].get('resetCompleted')) nCompleted++;
			}
			
			for (g = 0;g<nGroups;g++)
			{
				nGroupedLists = groups[g].lists.length;
				for (i = 0;i<nGroupedLists;i++)
				{
					nLists++;
					if (groups[g].lists[i].get('resetCompleted')) nCompleted++;
				}
			}
		
			console.log('INTERVAL',[nCompleted,nLists]);
			if (nCompleted >= nLists) 
			{
				clearInterval(intervalTimer);
				for (l = 0;l<nUngroupedLists;l++) 
				{
					lists[l].set('resetCompleted',false);
				}
				for (g = 0;g<nGroups;g++)
				{
					nGroupedLists = groups[g].lists.length;
					for (i = 0;i<nGroupedLists;i++)
					{
						groups[g].lists[i].set('resetCompleted',false);
					}
				}
				if (groupSelf == Hi_Preferences.get('currentTab', 'my', 'string')) console.log('start timer again',groupSelf);
				if (groupSelf == Hi_Preferences.get('currentTab', 'my', 'string')) hi.store.Project.refreshStartTimer();
			}
		},5000);
	};
	
	/* #2509 - used to keep track of lists that are enabled at page load time */
	dojo.global.initializers = {};
	/* #2509 - waits until observers on all initially enabled lists have completed before 
	 * rendering lists.  This should ensure that all items in a list are rendered as one DOM string instead
	 * of added incrementally
	 */
	dojo.global.finalizeInitialization = function(groups, lists) {
		var myInterval = setInterval(function() {
			var initialized = true;
			for (var i in initializers) {
				if (!dijit.byId(i).get('initialized')) initialized=false;
			}

			if (initialized) {
				clearInterval(myInterval);
				initiateTab(groups, lists, null, true);
			}
		},100);
	};
	/*
	 * Prepare lists on a tab to be rendered
	 */
	dojo.global.initiateTab = function(groups,lists,/* #2571 */watchOptions) {
		var len = lists.length,
			gLen = groups.length,
			listLen, i;

		if (hi.items.manager.editing() && hi.items.manager.editing().editingView) {
			if (hi.items.manager.editing().editingView.validate(hi.items.manager.editing().editingView.getFormValues(), true)){
				hi.items.manager.editing().editingView.save();
			}
		}
		for (i = 0;i < gLen; i++) {
			groups[i].lists[0].set('rendered',false);
			groups[i].set('disabled',false);
			groups[i].lists[0].set('rendered',true);
		}
		for (i = 0;i < len; i++) {
			/* #2571 - this observes added/deleted project items from data store after a filter change */
			if (watchOptions && lists[i].get('mainList')) {
				lists[i].afterFilterChange();
			} else {
				lists[i].set('rendered',false);
				lists[i].set('disabled',false);
				lists[i].set('rendered',true);
			}
		}
		this.loadTaskItemsState();
	};

	/*
	 * Refresh lists on a tab, after an event such as a tab switch
	 */
	dojo.global.refreshTab = function(groups, lists, isProjectTab, animate) {
		var len = lists.length,
			gLen = groups.length,
			listLen, i;

		if (hi.items.manager.editing() && hi.items.manager.editing().editingView) {
			if (hi.items.manager.editing().editingView.validate(hi.items.manager.editing().editingView.getFormValues(),true))
				hi.items.manager.editing().editingView.save();
		}

		// if (hi.items.manager._opened) hi.items.manager._opened = [];

		for (i = 0; i < len; i++) {
			lists[i].set('initialized',true);
			lists[i].set('rendered',true);
			lists[i].set('disabled',false);
			lists[i].set('rendered',false);
			lists[i].reset();
			lists[i].set('rendered',true);
			
			if (animate && supportsCSS3Transitions()) {
				var items = dojo.query('li.task_li', lists[i].domNode);
				for (var j = 0, itemsLen = items.length; j < itemsLen; j++) {
					css3fx.fade(items[j], {
						'in': true,
						duration: CONSTANTS.animationDuration.groupFadeMs,
						direction: 1
					}).play();
				}
			}
		}
		
		for (i = 0; i < gLen; i++){
			listLen = groups[i].lists.length;
			groups[i].lists[0].set('initialized',true);
			groups[i].lists[0].set('rendered',true);
			groups[i].set('disabled',false);
			groups[i].lists[0].set('rendered',false);
			groups[i].lists[0].reset();
			groups[i].lists[0].set('rendered',true);

		}
		this.loadTaskItemsState();
	};

	dojo.global.openTaskPopup = function(taskId, self) {

		if(!(taskId in hi.store.ProjectCache.index)) {
			return;
		}

		var HiTaskCalendar = dojo.global.HiTaskCalendar;
		var opened = hi.items.manager.openedId();
		if(opened == taskId && Tooltip.opened('taskedit')){
			HiTaskCalendar.closeOpenedTooltip();
			return;
		}

		//If there is an item remove it
		if(HiTaskCalendar.id){
			//Remove item
			HiTaskCalendar.tooltipTaskList.remove({
				"id": HiTaskCalendar.id
			});

			HiTaskCalendar.id = null;
			Project.openedInstance = null;
			Tooltip.close('taskedit');
		}

		//Make sure all other opened tasks are closed
		hi.items.manager.collapseOpenedItems(true, true);

		if(taskId){
			Project.openedInstance = time2str(HiTaskCalendar.currentDate, 'y-m-d');
			HiTaskCalendar.id = taskId;
			HiTaskCalendar.showTaskEditTooltip(taskId, self);
		}
	};
	/*
	 * Refresh tasks (collapsed or expanded)
	 */
	dojo.global.loadTaskItemsState = function() {
		var allTasks = Object.keys(Hi_Preferences.preferences);
		allTasks = allTasks.filter(function(item) {
			return (item.indexOf('task_') == 0);
		});
		for (var i = 0; i < allTasks.length; i++) {
			var item = allTasks[i],
				parseData = item.split('#'),
				eventdate = (parseData.length > 1) ? parseData[1] : null,
				taskId = parseInt(parseData[0].substr(parseData[0].indexOf('_')+1)),
				taskItem = hi.items.manager.get(taskId),
				pref = Hi_Preferences.get(item, '0'),
				isTaskExist = (taskId in hi.items.manager._items),
				parent = taskItem && taskItem.parent;

			if(!taskId || !taskItem){return;}
			var parentObj = null;

			if (pref == '1' && isTaskExist != false && (!parent || (parent && (parentObj = hi.items.manager.item(parent)) && parentObj.expanded === true))) {
				item_DOM.toggleChildren(taskId, eventdate, false, true);
			}
		}
	};

	/*
	 * Save task state (collapsed or expanded)
	 */
	dojo.global.saveTaskItemState = function(id, expanded, eventdate) {
		var item = hi.items.manager.get(id);
		if (item.childrenRendered === false) {
			return;
		}
		var key = (eventdate) ? "task_" + id + "#" + eventdate : "task_" + id;
		if (key) {
			Hi_Preferences.set(key, expanded ? '1' : '0');
			Hi_Preferences.send();
		}
	};
	
	dojo.global.higuidS4 = function() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	};
	
	dojo.global.higuid = function() {
		return dojo.global.higuidS4() + dojo.global.higuidS4() + '-' + dojo.global.higuidS4() + '-' + dojo.global.higuidS4() +
			'-' + dojo.global.higuidS4() + '-' + dojo.global.higuidS4() + dojo.global.higuidS4() + dojo.global.higuidS4();
	};
	
	dojo.global.showNotifyEnabled = function() {
		var desktopNotifyState = parseInt(Hi_Preferences.get('desktopNotifyState'));
		return desktopNotifyState != 0 && "Notification" in window && Notification.permission === "granted";
	};
	
	dojo.global.showNotify = function(title, iconUrl, body) {
		if (dojo.global.showNotifyEnabled()) {
			var notification = new Notification(title, {body: body, icon: iconUrl, tag: dojo.global.higuid()});
			notification.onshow = function() {
				setTimeout(function() {
					notification.close();
				}, 5000);
			};
		}
	};

	dojo.global.requestNotifyPermissions = function(callback) {
		if (typeof(callback) == 'undefined') {
			console.warn('callback passed to dojo.global.requestNotifyPermissions is undefined');
			return;
		}
		
		if ("Notification" in window) {
			if (Notification.permission !== 'denied') {
				Notification.requestPermission(function (permission) {
					if (!('permission' in Notification)) {
						Notification.permission = permission;
						callback(Notification.permission);
						return;
					} else {
						callback(Notification.permission);
						return;
					}
				});
				callback(Notification.permission);
			} else {
				callback('denied');
			}
		} else {
			callback('unsupported');
		}
	};
	
	dojo.global.testHexColor = function(hex) {
		return typeof(hex) == 'string' && hex && /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/.test(hex);
	};
	
	/**
	 * Lighten hex color by percent.
	 * Caches result for faster performance
	 */
	dojo.global.colorLuminance = function(hex, percent) {
		if (!hex) {
			return CONSTANTS.defaultColorValue;
		}
		
		// Cache result for faster performance 
		// 1 initialize cache 
		if (!colorLuminance.cache) {
			colorLuminance.cache = {};
		}
		// 2 Calculate cache key
		var key = hex+'|'+percent, result;
		// 3 Try to get from cache
		if(colorLuminance.cache[key]){
			result=colorLuminance.cache[key];
			//console.log("Return color from cache:"+result)
			return result;
		}
		
		hex = hex.replace(/^\s*#|\s*$/g, '');

		// convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
		if(hex.length == 3){
			hex = hex.replace(/(.)/g, '$1$1');
		}

		var r = parseInt(hex.substr(0, 2), 16),
		g = parseInt(hex.substr(2, 2), 16),
		b = parseInt(hex.substr(4, 2), 16);

		result= '#' +
		((0|(1<<8) + r + (256 - r) * percent / 100).toString(16)).substr(1) +
		((0|(1<<8) + g + (256 - g) * percent / 100).toString(16)).substr(1) +
		((0|(1<<8) + b + (256 - b) * percent / 100).toString(16)).substr(1);
		colorLuminance.cache[key] = result;
		//console.log("Calculated color for:"+key);
		return result;
	};
	
	/* OBSOLETE
	 * #2493 - check recurring item against specified date
	 * 
	 * This is used for the various filters in view-X.js
	 */
	dojo.global.recurringItemMatchesDate = function(item, thisDate) {
		var eventArray = HiTaskCalendar.eventArray(item),
			eventArray_length = eventArray.length,
			current_time = new Date().getTime();
		
		for (var i = 0; i < eventArray_length; i++) {
			if (eventArray[i].endDate) {
				if (current_time >= eventArray[i].startDate && current_time <= eventArray[i].endDate) return true;
			} else {
				if (eventArray[i].start_date == HiTaskCalendar.getStrFromDate(HiTaskCalendar.today) ) return true;
			}
		}
		return false;
	};
	
	dojo.global.getProjectProgressReportUrl = function() {
		var url = '/report/?';
		require(["dojo/dom-form"], function(domForm) {
			url += domForm.toQuery("projectProgressReportForm") + '&' + getProjectGlobalReportDates(true);
		});
		return url;
	};
	
	dojo.global.capitalizeFirstLetter = function(strOrArrayOfStr) {
		if (typeof(strOrArrayOfStr) == 'string' && strOrArrayOfStr.length > 0) {
			strOrArrayOfStr = strOrArrayOfStr.charAt(0).toUpperCase() + strOrArrayOfStr.slice(1);
		}
		
		if (typeof(strOrArrayOfStr) == 'object' && strOrArrayOfStr.length > 0) {
			require(["dojo/_base/array"], function(array) {
				strOrArrayOfStr = array.map(strOrArrayOfStr, function(str) {
					return capitalizeFirstLetter(str);
				});
			});
		}
		
		return strOrArrayOfStr;
	};
	
	/* OBSOLETE */
	dojo.global._recurringItemMatchesDate = function(item,/* Date */thisDate)
	{
		var currentDateStr = thisDate,//HiTaskCalendar.getStrFromDate(HiTaskCalendar.today),//HiTaskCalendar.getCurrentDateStr(),
			instances_length = item.instances.length,
			start_date =  HiTaskCalendar.getStrFromDate(new Date(item.start_date)),
			end_date = item.end_date?HiTaskCalendar.getStrFromDate(new Date(item.end_date)):null,
			use_dateStr = currentDateStr,
			match = false, i, found;
		
		/* Have we gotten to the start date yet?  If not, get out of here */
		if (new Date(start_date) > new Date(currentDateStr) )
		{
			return false;
		}
		
		/*
		 * Iterate over instances
		 */
		if (item.recurring == 1)
		{
			if (start_date == use_dateStr) {
				found = false;
				for (i = 0;i<instances_length;i++) {
					if (item.instances[i].start_date == use_dateStr)
					{
						found = true;
						if (item.instances[i].status == 0) match = true;
					}
				}	

				if (found)
				{
					if (!match) return false;
				}
				
			} else
			{
				for (i = 0;i<instances_length;i++) {
					if (item.instances[i].start_date == use_dateStr && item.instances[i].status != 0) return false;
				}
			}

		} else
		{
			match = false;
			var today_time = hi.items.date.truncateTime(new Date()).getTime(),
				start_date_time = new Date(item.start_date).getTime(),
				end_date_time = end_date?new Date(item.end_date).getTime():null;
				
			/* Is today between the duration of this recurring item? */
			var goCheck = false;
			if (end_date_time)
			{
				if (today_time >= start_date_time && today_time <= end_date_time) goCheck = true;
			} else
			{
				if (start_date == use_dateStr) goCheck = true;
			}
			if (/*start_date == use_dateStr*/goCheck)
			{
				found = false;
				for (i = 0;i<instances_length;i++)
				{
					if (item.instances[i].start_date /*==*/< use_dateStr)
					{
						found = true;
						if (item.instances[i].status == 0) match = true;
					}
				}	
				if (found)
				{
					if (!match) return false;
				}
			}
			else 
			{
				
				for (i = 0;i<instances_length;i++)
				{
					if (item.instances[i].start_date /*==*/< use_dateStr && item.instances[i].status == 0) match = true;
				}
				if (!match) return false;
			}
		}
		return true;
	};
	
	// https://red.hitask.net/issues/5555#note-14
	dojo.global.handleItemMouseOver = function(e) {
		if (!e || !e.target) return;

		if (dojo.hasClass(document.body, 'dojoDndMove')) {
			e.target.setAttribute('title', '');
		}
	};
	
	// https://red.hitask.net/issues/5555#note-14
	dojo.global.handleItemMouseOut = function(e) {
		if (!e || !e.target) return;

		if (dojo.hasClass(document.body, 'dojoDndMove')) {
			e.target.setAttribute('title', item_DOM.localeClickToView);
		}
	};

	/* Events on non-dijit task items */
	dojo.global.handleItemClick = function(taskId, eventdate, e) {
		var editing = hi.items.manager.editing();
		if (!editing || editing.taskId == taskId || editing.taskItem) {
			item_DOM.handleItemClick(taskId, eventdate, e);
		} else if (e) {		//editing is new item
			hi.items.manager.collapseOpenedItems(true, false);
			dojo.stopEvent(e);
		}
	};
	
	dojo.global.handleItemDoubleClick = function(taskId, eventdate, e) {
		var editing = hi.items.manager.editing();
		if (!editing || editing.taskId == taskId) {
			item_DOM.handleItemDoubleClick(taskId, eventdate, e);
		} else if (e) {
			dojo.stopEvent(e);
		}
	};

	/* #2766 - handleExpandClick checks for validity of task id before trying to expand */
	dojo.global.handleExpandClick = function(taskId, eventdate, e) {
		var passEventdate = eventdate;
		if (!eventdate || eventdate.length <= 0) {
			passEventdate = null;
		}
		/* Quit if calendar tooltip opened and in edit mode */
		/* #2965 - block expanding/collapsing until animation completeds */
		if (item_DOM.transitioning || taskId < 0 || Tooltip.opened('taskedit') || hi.items.manager.editing()) {
			if (e) {
				dojo.stopEvent(e);
			}
		} else {
			/* #2973 - close calendar item tooltip if opened */
			HiTaskCalendar.closeOpenedTooltip();
			// item_DOM.transitioning = true;
			item_DOM.transitioning = taskId;
			item_DOM.handleExpandClick(taskId, passEventdate, e);
		}
	};

	dojo.global.handleCollapseClick = function(taskId, eventdate, e) {
		/* #3040 - block collapse if expand/collapse animation is already in progress */
		if (item_DOM.transitioning) return;
		item_DOM.transitioning = taskId;
		item_DOM.handleCollapseClick(taskId,eventdate,e);
	};

	dojo.global.handleKeyPress = function(taskId, eventdate, e) {
		item_DOM.handleKeyPress(taskId,eventdate,e);
	};
	
	dojo.global.toggleChildren = function(taskId, eventdate, e, state){
		item_DOM.toggleChildren(taskId, eventdate, e, state);
	};

	dojo.global.taskPath = function(taskId){
		var path = [],
			task = hi.items.manager.get(taskId);

		while (task && task.parent) {
			path.unshift(task.parent);
			task = hi.items.manager.get(task.parent);
		}

		return path;
	};

	dojo.global.handleClickStarred = function(taskId, eventdate, e) {
		item_DOM.handleClickStarred(taskId, eventdate, e);
	};
	
	// https://red.hitask.net/issues/5555#note-14
	dojo.global.handleMouseOverStarred = function(e) {
		if (!e || !e.target) return;

		if (dojo.hasClass(document.body, 'dojoDndMove')) {
			e.target.setAttribute('title', '');
		}
	};
	
	// https://red.hitask.net/issues/5555#note-14
	dojo.global.handleMouseOutStarred = function(e) {
		if (!e || !e.target) return;

		if (dojo.hasClass(document.body, 'dojoDndMove')) {
			e.target.setAttribute('title', 'Click to mark item starred');
		}
	};

	// https://red.hitask.net/issues/5555#note-14
	dojo.global.handleMouseOverComplete = function(e) {
		if (!e || !e.target) return;

		if (dojo.hasClass(document.body, 'dojoDndMove')) {
			e.target.setAttribute('title', '');
		}
	};
	
	// https://red.hitask.net/issues/5555#note-14
	dojo.global.handleMouseOutComplete = function(e) {
		if (!e || !e.target) return;

		if (dojo.hasClass(document.body, 'dojoDndMove')) {
			e.target.setAttribute('title', item_DOM.localeClickToComplete);
		}
	};
	
	dojo.global.handleClickComplete = function(taskId,eventdate,e,withoutWidget) {
		item_DOM.handleClickComplete(taskId,eventdate,e,withoutWidget);
	};
	
	/**
	 * Track event with analytics (Google, Mixpanel)
	 * Example parameters: {"gender": "male", "age": 13}
	 */
	dojo.global.trackEvent = function(name, parameters) {
		try {
			// Mixpanel
			if (typeof(mixpanel) != 'undefined') {
				mixpanel.track(name, parameters);
			}
			
			// GA: ga('send', 'event', 'category', 'action', 'opt_label', opt_value, {'nonInteraction': 1});
			// ensure that 'ga' is defined, so that 'ga' never fails
			if (typeof(ga) == 'function') {
				ga('send', 'event', 'Webapp', name);
			}
		} catch (e) {
			if (console) {
				console.error ? console.error(e) : console.log(e);
			}
		}
	};
	
	dojo.global.supportsCSS3Transitions = function() {
		// http://caniuse.com/transforms3d
		return (dojo.isChrome && dojo.isChrome >= 12) ||
				(dojo.isSafari && dojo.isSafari >= 4) ||
				(dojo.isFF && dojo.isFF >= 10) ||
				(dojo.isIE && dojo.isIE >= 10) ||
				(dojo.isOpera && dojo.isOpera >= 15);
	};
	
	dojo.global.trackEventNames = {
		PROJECT_CREATE: 'Create Project',
		PROJECT_MODIFY: 'Modify Project',
		ITEM_CREATE: 'Create Item',
		ITEM_MANY_CREATE: 'Create Many Tasks',
		ITEM_MODIFY: 'Modify Item',
		PAGE_OPEN_SETTINGS: 'Settings',
		PAGE_OPEN_ARCHIVE: 'Archive',
		PAGE_OPEN_APP: 'Open Webapp',
		CHAT_MESSAGE_SENT: 'Send Chat Message',
		CONTACT_ADD: 'Add Contact'
	};
	
	/*
	 * Get widget corresponding to given id in the specified list
	 * Input:
	 * childId => item id to search for in list
	 * list => instance of hi.widget.TaskList to search for child in
	 */
	dojo.global.getChildWidget = function(childId,list)
	{
		var ancestors = [],
			parentId = hi.items.manager.get(childId)?hi.items.manager.get(childId).parent:null,
			parentItem = hi.items.manager.get(parentId)||null,
			widget;
			
		if (!parentItem) return null;
		while(parentId)
		{
			ancestors.push(parentId);
			parentItem = hi.items.manager.get(parentItem.parent)||null;
			if (!parentItem) break;
			parentId = parentItem.id;
			
		}
		ancestors.reverse();
		if (!list.itemWidgets[ancestors[0]]) return null;
		widget = list.itemWidgets[ancestors[0]];
		for (var i = 1;i<ancestors.length;i++)
		{
			widget = widget.itemWidgets[ancestors[i]];
			if (!widget) return null;
		}
		widget = widget.itemWidgets[childId];
		return widget;
	};
	
	dojo.global.retinaReplace = function() {
		if (window.devicePixelRatio >= 1.2) {
			dojoQuery('.retinaReplace').forEach(function(x) {
				var replace = dojoDomAttr.get(x, 'data-retina-replace');
				if (replace) {
					dojoDomAttr.set(x, 'src', replace);
				}
			});
		}
	};
	
	dojo.global.userEmailConfirmed = function(userId) {
		if (Project.user_id == userId) {
			return Project.email_confirmed;
		} else if (Project.friends) {
			for (var i = 0, len = Project.friends.length; i < len; i++) {
				if (Project.friends[i].id == userId) {
					return Project.friends[i].emailConfirmed;
				}
			}
		} else if (Project.shadowFriends && userId in Project.shadowFriends) {
			return Project.shadowFriends[userId].emailConfirmed;
		}
		return false;
	};

	dojo.global.getAvatar = function(px, personId, skipRetina) {
		if(px !== 16 && px !== 32 && px !== 24 && px !== 48 && px !== 64){
			return '';
		}
		
		var avatar = '',
			isRetina = window.devicePixelRatio >= 1.2 && !skipRetina, friend;

		px = isRetina? px * 2 : px;

		if (!personId || personId === Project.user_id) {
			avatar = '/avatar/' + Project.userPictureHash + '.' + px + '.png';
		} else {
			friend = Project.getFriend(personId);
			if (friend) {
				avatar = '/avatar/' + friend.pictureHash + '.' + px + '.png';
			} else if (Project.shadowFriends && personId in Project.shadowFriends) {
				avatar = '/avatar/' + Project.shadowFriends[personId].pictureHash + '.' + px + '.png';
			}
		}

		return avatar;
	};

	// summary:
	//		Provides auto-reconnection to a websocket after it has been closed
	// socket:
	//		Socket to add reconnection support to.
	// reconnectTime:
	//		Pause for attempt to connect in milliseconds
	// onnewsocket:
	//		Callback function which provides new socket object
	// onmessage, onconnect, ondisconnect:
	//		Callbacks for new socket events
	// example:
	//			dojo.global.webSocketReconnect(socket, 10000, function(newSocket) {
	//				socket = newsocket;
	//			}, function(event) {
	//				console.log(event.data);
	//			});
	dojo.global.webSocketReconnect = function(socket, reconnectTime, onNewSocket, onMessage, onConnect, onDisconnect) {
		if(typeof(socket) == 'undefined' || typeof(onNewSocket) !== 'function') {
			return;
		}
		var reconnectTimeout = reconnectTime || 10000;
		var reconnectHandler = function() {
			setTimeout(function() {
				var newSocket = new Socket.WebSocket({
					url: socket.url || socket.URL
				});
				if(typeof(onConnect) == 'function') {
					newSocket.on("open", onConnect);
				}
				if(typeof(onDisconnect) == 'function') {
					newSocket.on("close", onDisconnect);
				}
				if(typeof(onMessage) == 'function') {
					newSocket.on("message", onMessage);
				}
				newSocket.on("close", reconnectHandler);
				onNewSocket(newSocket);
			}, reconnectTimeout);
		};
		socket.on("close", reconnectHandler);
	};
	
	dojo.global.retryUntilSuccess = function(funcToTry, funcToCheckSuccess, interval, maxTry, retryCounter) {
		interval = interval || 500;
		maxTry = maxTry || 10;
		retryCounter = retryCounter || 0;
		
		if (funcToCheckSuccess()) {
			funcToTry();
		} else {
			if (retryCounter < maxTry) {
				setTimeout(function() {
					retryUntilSuccess(funcToTry, funcToCheckSuccess, interval, maxTry, ++retryCounter);
				}, interval);
			} else {
				throw "Failed retry";
			}
		}
	};

	dojo.global.closeTrialWarning = function() {
		var warning = dojo.query('#header .notification-alert-global')[0],
			loading_done = dojo.byId('loading_done'),
			appContainer = dijit.byId('appContainer');

		if (!warning) return;
		dojo.addClass(warning, 'hidden');
		setTimeout(function() {
			showTrialWarning();
		}, 3600 * 1000);
		
		if (loading_done) {		//app
			dojo.removeClass(loading_done, 'trial');
			appContainer && appContainer.resize();
			dojo.global.reLayout();
		} else {		//setting
			setSettingsBlockStyle();
		}
	};

	dojo.global.showTrialWarning = function() {
		var warning = dojo.query('#header .notification-alert-global')[0],
			loading_done = dojo.byId('loading_done'),
			appContainer = dijit.byId('appContainer');

		if (!warning || Project.account_type !== 'TEAM_BASIC') return;
		dojo.removeClass(warning, 'hidden');
		
		if (loading_done) {		//app
			dojo.addClass(loading_done, 'trial');
			appContainer && appContainer.resize();
			dojo.global.reLayout();
		} else {		//setting
			setSettingsBlockStyle();
		}
	};
	
	dojo.global.Hi_TeamLevels = (function() {
		return {
			USER: 1,
			MANAGER: 2,
			ADMIN: 10,
			OWNER: 100
		};
	})();

	// This attribute is set to true when showing the item page

	dojo.global.SingleItem = false;
	
	return {};

});
