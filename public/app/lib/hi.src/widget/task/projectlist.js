/**
 * hi.items.ProjectListProjects - the dojo widget corresponding to the container that holds all project on the UI.  
 * 
 * Manages Dnd actions of project widgets
 * Manages adding/removing project widgets from UI
 * Manages sorting and some filtering of project widgets in UI
 */
define([
	"dojo/_base/array", // array.forEach array.map
	"dojo/_base/declare", // declare
	"dojo/cldr/supplemental", // cldrSupplemental.getFirstDayOfWeek
	"dojo/date", // date
	"dojo/date/locale",
	"dojo/dom", // dom.setSelectable
	"dojo/dom-class", // domClass.contains
	"dojo/_base/event", // event.stop
	"dojo/_base/lang", // lang.getObject, lang.hitch
	"dojo/_base/sniff", // has("ie") has("webkit")
	"dojo/string", // string.substitute
	"dojo/_base/window", // win.doc.createTextNode
	"dijit/_Widget",
	"dijit/_Templated"
], function(array, declare, cldrSupplemental, date, local, dom, domClass, event, lang, has, string, win,
			_WidgetBase, _TemplatedMixin){
	
	// module:
	//		hi/widget/ProjectList
	// summary:
	//		Task list which updates itself on store QueryResults data change
	
	var ProjectList = declare("hi.widget.ProjectList", [_WidgetBase, _TemplatedMixin], {
		// summary:
		//		Project list
		//		Used to manage hi.widget.Project instances
		
		// Template for the list
		templateString: "<ul class=\"projects\"></ul>",
		
		// Project list: Array
		//		Project list
		projects: null,
		
		// Allow sorting: Boolean
		//		Allow sorting child projects
		allowSorting: false,
		
		// Sorting drag object: Object
		//		dojo.dnd.HiProjectDragSource instance for sorting
		dragSource: null,
		
		
		constructor: function (/*Object*/args) {
			//supporting widgets (child widgets) will be cleaned up by dojo on widget destruction
			this._supportingWidgets = this.projects = this.projects || [];
		},
		
		buildRendering: function() {
			this.inherited(arguments);
			
			var projects = this.projects,
				domNode = this.domNode;
			
			for (var i=0, ii=projects.length; i<ii; i++) {
				projects[i].placeAt(domNode);
			}
			
			if (this.allowSorting) {
				this.bindDnD();
			}
		},

		/* ----------------------------- Drag and drop ------------------------------ */
		
		bindDnD: function () {
			var dragSource = this.dragSource = new dojo.dnd.HiProjectDragSource(this.domNode, {}),
				self = this;
			
			dojo.connect(dragSource, "onDndStart", function() {
				// if (Project.getSorting(2) !== Project.ORDER_PRIORITY) return;
				Project.dragging = true;
				Project.draggingProjectList = self;
			});
			
			dojo.connect(dragSource, "onDndCancel", function() {
				self.dndOnSort(this); 
			});
			dojo.connect(dragSource, "onDndDrop", function() { 
				self.dndOnSort(this); 
			});
		},
		
		dndOnSort: function (dragSource) {
			// summary:
			//		Handle sorting
			//		Execution context is dragged item
			
			//JDT var m = dojo.dnd.Manager(); 
			var projects = [];
			var insertIndex = -1;
			dojo.forEach(this.projects, function(item) {
				projects.push(item);
			});
			var pLen = projects.length;
			var m = dojo.dnd.Manager.manager();
			if (dragSource == m.source && dragSource.current && dragSource.dragNode) {		/*  m.canDropFlag &&  */
				if (Project.getSorting(2) !== Project.ORDER_PRIORITY) {
					var msg = hi.i18n.getLocalization('project.move_project_invalid_priority');
					alert(msg);
					return false;
				}
				for (var i = 0; i < pLen; i++) {
					if (projects[i] == dijit.byId(dragSource.current.id)) {
						insertIndex = i;
						break;
					}
				}
				/*
				if (dragSource.dragBefore) {
					insertBefore(dragSource.dragNode, dragSource.current);
				} else {
					insertAfter(dragSource.dragNode, dragSource.current);
				}
				*/
				this.remove(dijit.byId(dragSource.dragNode.id), true);
				this.add(dijit.byId(dragSource.dragNode.id),insertIndex, true);
				this.dndUpdatePriorities(dijit.byId(dragSource.dragNode.id));
				
				Project.dragging = false;
			}
		},
		
		/* Feature #2154 */
		dndUpdatePriorities: function(project) {
			/*
			 * Summary:
			 * For specified project widget, get projects it is between, calculate their average priority
			 * and assign it to input project.  If project is at bottom of list, its priority is 10 less
			 * than the one above it.  If project is at top of list, its priority is 10 more than the next
			 * project on the list.
			 * 
			 * Note: this method does nothing unless the sort mode is by priority
			 * 
			 * Input: hi/widget/task/Project
			 * Output: project widget with updated priority
			 */
			if (this.projects.length<=1) return;
			if (Hi_Preferences.get('currentTab', 'my', 'string') !== 'project' || Project.getSorting(2) != Project.ORDER_PRIORITY) return;
			var prev = null,
				next = null,
				avg = 0,
				projectIndex = -1,
				pLen = this.projects.length;

			for (var i = 0; i < pLen; i++) {
				if (this.projects[i] == project) {
					projectIndex = i;
					break;
				}
			}
			if (projectIndex < 0) return;
			if (projectIndex > 0 && projectIndex < this.projects.length - 1) {
				//Inserted somewhere in the middle
				prev = this.projects[projectIndex-1].priority;
				next = this.projects[projectIndex+1].priority;
				avg = Math.round( (prev+next)/2 );
			} else if (projectIndex === 0) {
				//Inserted at the very top
				next = this.projects[projectIndex+1].priority;
				avg = next+10;
				if (avg > 30000) avg = 30000;
			} else {
				//Inserted at the very bottom
				prev = this.projects[projectIndex-1].priority;
				avg = prev - 10;
				if (avg < 0) avg = 0;
			}
			hi.items.manager.setSave(project.projectId, {"priority": avg});
		},
		
		/*
		 * Obsolete and replaced by dndUpdatePriorities()
		 */
		_dndUpdatePriorities: function (li) {
			// summary:
			//		Update item priorities
			// li: HTMLElement
			//		Element which should be updated
			
			if (Hi_Preferences.get('currentTab', 'my', 'string') !== 'project' || Project.getSorting(2) != Project.ORDER_PRIORITY) return;
			
			var projectId = li.projectId;
			
			var prevSibl = prevElement(li);
			if (prevSibl) {
				prevSibl = prevSibl.projectId;
			}
			
			//Calculate and update priority
			if (prevSibl) {
				Project.moveProject(projectId, prevSibl);
			} else {
				Project.moveProject(projectId);
			}
		},

		/* ----------------------------- API ------------------------------ */
		_compareAscending: function(toAdd, addAt, attribute, caseInsensitive) {
			var v1 = toAdd[attribute];
			var v2 = addAt[attribute];

			v1 = v1 === undefined? '' : v1;
			v2 = v2 === undefined? '' : v2;

			if(caseInsensitive === true){
				v1 = v1.toUpperCase? v1.toUpperCase() : v1;
				v2 = v2.toUpperCase? v2.toUpperCase() : v2;
			}

			return v1 > v2;
		},

		_compareDescending: function(toAdd, addAt, attribute, caseInsensitive) {
			toAdd = toAdd[attribute];
			addAt = addAt[attribute];
			if(caseInsensitive === true){
				toAdd = toAdd.toUpperCase? toAdd.toUpperCase() : toAdd;
				addAt = addAt.toUpperCase? addAt.toUpperCase() : addAt;
			}

			return toAdd <= addAt;
		},
		/* Bug #2167 - add project in correct location (according to sort option) */
		// Project list initialized here and sorting logic implemented here.
		addNew: function(project) {
			var sortAttribute = 'title',
				sortOrder = 'Ascending',
				caseInsensitive = true,
				insertAt = 0,
				index = 0,
				projects = this.projects, tmp;

			if (this.sortIsSupported()) {
				// tmp = hi.store.Project.getOrderConfiguration(true)[0];
				tmp = hi.store.Project.getProjectOrderConfiguration(true)[0];
				sortAttribute = tmp.attribute;
				caseInsensitive = tmp.caseInsensitive;
				if (tmp.descending) sortOrder = 'Descending';
			}

			while(
				insertAt < projects.length &&
				this['_compare' + sortOrder](project, projects[insertAt], sortAttribute, caseInsensitive)
			) {
				insertAt++ ;
			}
			this.projects.splice(insertAt, 0, project);
			dojo.place(project.domNode, this.domNode, insertAt);
			
			//Update drag and drop list
			if (this.dragSource) {
				this.dragSource.sync();
			}
		},
		
		add: function (project, index, noSort) {
			// summary:
			//		Add project to the list
			if ((index || index === 0) && index < this.projects.length) {
				dojo.place(project.domNode, this.projects[index].domNode, "before");
				this.projects.splice(index, 0, project);
			} else {
				this.projects.push(project);
				project.placeAt(this.domNode);
			}
			
			//Update drag and drop list
			if (this.dragSource) {
				this.dragSource.sync();
			}
			/* Bug #2148 noSort is passed in from order() method */
			if (!noSort) this.order(project);
		},
		
		removeAll: function () {
			var projects = this.projects;
			var self = this;
			projects.forEach(function(p) {
				self.domNode.removeChild(p.domNode);
				p.destroy();
			});
			this.projects = [];
		},

		remove: function (project,forSort) {
			// summary:
			//		Remove project form the list
			// project: Object
			//		Project object or project id
			
			project = typeof project === "string" || typeof project === "number" ? this.get(project) : project;
			if (!project) return;
			
			var index = dojo.indexOf(this.projects, project);
			
			if (index !== -1) {
				project = this.projects[index];
				this.projects.splice(index, 1);
				
				//Destroy
				/* Bug #2148 - but don't if we are removing so we can stick in the correct spot, according to sort option */
				if (!forSort) project.destroyRecursive();
				
				//Update drag and drop list
				if (this.dragSource) {
					this.dragSource.sync();
				}
			}
		},
		
		/*
		 * Hide project's DOM node
		 */
		hideProject: function(project) {
			if (!project) return;
			if (!project.domNode) return;
			dojo.addClass(project.domNode,'hidden');
			
		},
		
		/*
		 * Show project's DOM node
		 */
		showProject: function(project) {
			if (!project) return;
			if (!project.domNode) return;
			if (Hi_Preferences.get('currentTab', 'my', 'string') != 'project') return;
			if (dojo.hasClass(project.domNode,'hidden')) dojo.removeClass(project.domNode,'hidden');
		},
		
		get: function (id) {
			// summary:
			//		Returns hi.widget.Group or hi.widget.Project matching given ID
			// id: Number
			//		Project or group id
			
			var projects = this.projects,
				i = 0,
				ii = projects.length;
			
			for (; i<ii; i++) {
				if (projects[i].projectId == id) return projects[i];
			}
			
			return null;
		},
		
		/* Bug #2148 
		 * Adapted from TaskList.order to account for items coming in on page load not obeying the specified sorting order
		 */
		order: function(project) {
			var query = this.getSortedQuery();
			var reference = null;
			var place;
			var projects = {};
			dojo.forEach(this.projects, function(p) {projects[p.projectId]=p;});
			for (var i = 0; i < query.length; i++) {
				if (project.projectId == query[i].id) {
					if (!reference) {
						place = i;
					} else break;
				} else if (projects[query[i].id]) {
					reference = query[i];
					place = i+1;
				}
			}
			
			if (reference) {
				this.remove(project,true);
				this.add(project,place,true);
			}
		},
		
		/* Bug #2148 - redraw the project list in the Project group based on sorting selection */
		/* #3072 - adjust to handle search queries */
		reset: function() {
			var doOrder = false;
			/* sort by start_date and due_date do not make sense, so temporarily change to by title */
			var query = this.getSortedQuery();
			//if (!this.sortIsSupported()) doOrder = true;
			var self = this;
			// Remove projects from DOM
			var projects = {};
			dojo.forEach(this.projects, function(p) {projects[p.projectId]=p;});
			for (var i in projects) {
				this.remove(i,true);
				//if (Hi_Search.isFormVisible()) {this.domNode.removeChild(projects[i].domNode);}
			}

			//Go through an add them in again, according to query order
			var c = 0,
				project;
			dojo.forEach(query, function(item) {
				project = projects[item.id]||null;
				if (project) self.add(projects[item.id],c++,doOrder);
				if (Hi_Search.isFormVisible() && project)
				{
					itemCount=0;
					for (var i in project.lists[0].itemIds)
					{
						itemCount++;
						break;
					}
					if (itemCount<=0)
					{
						if (!filterProject(item)) self.hideProject(project);
					}
				} else
				{
					self.showProject(project);
				}
			});
		},
		
		/* Bug #2148 - determine is current sort option is supported 
		 *  - sort by start_date is not
		 *  - sort by due_date is not
		 */
		sortIsSupported: function() {
			return !hi.store.Project.getOrder(2).match(/^startDate|endDate/);
		},
		
		/* Bug #2148 check for sort by priority since ordering is messed up at load time */
		sortByPriority: function() {
			return hi.store.Project.getOrder().match(/^priority/);
		},
		
		/* Bug #2148 - create the proper order of the query */
		getSortedQuery: function() {
			var query = hi.store.ProjectCache.query({category:hi.widget.TaskItem.CATEGORY_PROJECT});
			var sortOption = hi.store.Project.getProjectOrderConfiguration(true),
				attr, descending = false, tmp = [];

			dojo.forEach(query, function(q) {tmp.push(q);});
			/* sort by start_date and due_date do not make sense, so temporarily change to by title */
			if (!this.sortIsSupported()) {
				doOrder = true;
				tmp.sort(function(a, b) {
					a = a.title.toUpperCase && a.title.toUpperCase();
					b = b.title.toUpperCase && b.title.toUpperCase();
					if (a < b)
						return -1;
					else if (a == b)
						return 0;
					else
						return 1;
				});
			} else {
				if (sortOption && sortOption.length > 0) {
					attr = sortOption[0].attribute;
					descending = sortOption[0].descending;
					tmp.sort(function(a, b) {
						var va = a[attr];
						var vb = b[attr];

						va = va === undefined ? '' : va;
						vb = vb === undefined ? '' : vb;
						
						if (sortOption[0].hasOwnProperty('caseInsensitive') && sortOption[0].caseInsensitive) {
							va = va.toUpperCase && va.toUpperCase();
							vb = vb.toUpperCase && vb.toUpperCase();
						}
						if (va > vb) {
							return descending? -1 : 1;
						} else if (va == vb) {
							return 0;
						} else {
							return descending? 1 : -1;
						}
					});
				}
			}
			query = tmp;
			return query;
		},
		
		collapseAll: function() {
			if (this.projects && this.projects.length > 0) {
				for (var i = 0; i < this.projects.length; i++) {
					this.projects[i].collapse();
				}
			}
		},
		
		expandAll: function() {
			if (this.projects && this.projects.length > 0) {
				for (var i = 0; i < this.projects.length; i++) {
					this.projects[i].expand();
				}
			}
		}
	});
	
	return ProjectList;
});