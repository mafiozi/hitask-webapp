package beans;

import org.apache.commons.lang.StringUtils;

import utils.Constants;
import api.ApiGeneralResponse;

import com.google.gson.annotations.SerializedName;

public class AuthenticateResponse extends ApiGeneralResponse {
	@SerializedName("id")
	private Long userId;
	
	@SerializedName(Constants.SESSION_ID_KEY)
	private String sessionId;

	@SerializedName(Constants.SESSION_USER_LEVEL_KEY)
	private int level;

	@SerializedName(Constants.SESSION_ACCOUNT_TYPE_KEY)
	private String accountType;
	
	@SerializedName(Constants.SESSION_EXTERNAL_USER_ID_KEY)
	private String externalUserId;
	
	@SerializedName(Constants.SESSION_LOGIN_ID_KEY)
	private String login;
	
	@Override
	public boolean success() {
		return StringUtils.isNotBlank(sessionId);
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getExternalUserId() {
		return externalUserId;
	}

	public void setExternalUserId(String externalUserId) {
		this.externalUserId = externalUserId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
}
