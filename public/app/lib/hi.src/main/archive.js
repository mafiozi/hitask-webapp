//dojo.provide("hi.main.archive");

/**
 * Archive and archiving related functionality
 */
define(['hi/widget/task/item_DOM'],function (item_DOM) {
	dojo.global.archiveTask = function(id, options, isProject) {
		function closeTooltip () {
			var opened = hi.items.manager.opened();
			if (opened){
				opened.collapse();
			}
			Tooltip.close('archiveproject');
		}
		options = options || {};
		
		if ('confirmation' in options) {
			Tooltip.open('archiveproject', "", options.confirmation);
			
			var tooltip = dojo.byId('tooltip_archiveproject');
			if (!archiveTask.tooltipInitialized) {
				dojo.connect(tooltip, 'click', function (ev) {
					dojo.stopEvent(ev);
				});
				archiveTask.tooltipInitialized = true;
			}
			
			
			var btns = tooltip.getElementsByTagName('BUTTON');
				
				//Archive
				btns[0].onclick = function () {
					delete(options.confirmation);
					archiveTask(id, options);
					closeTooltip();
				};
				//Cabcel 
				btns[1].onclick = function () {
					closeTooltip();
				};
			return;
		}
		
		if (Hi_Timer.isTimedTask(id)) {
			Hi_Timer.stop();
		}
		
		var item = hi.items.manager.item(id);
		if (!item) {
			//Bug #4371
			//Sometimes in big calendar, recurring item can't get through hi.items.manager.item(), use tooltip instead
			//Project
			item = hi.items.ProjectListProjects.get(id) || hi.items.manager.item(id, 'tooltip');
		}

		var onAnimationComplete = function () {
			options.tarchive = true;
			var ajaxParams = {};
			
			if (hi.items.manager.get(id, 'category') == 0) {
				ajaxParams = {id: id, project:1};
			} else if (hi.items.manager.get(id, 'parent') > 0) {
				ajaxParams = {id: id, empty_project:1};
			} else {
				ajaxParams = {id: id};
			}
			
			Project.ajax('tarchive', ajaxParams, function() {hi.items.manager.refresh();}, function() {hi.items.manager.refresh();});
			
			//Remove item and all children
			if (item.list) {
				//Item
				var data = hi.items.manager.get(id);
				item.list.remove(data);
			} else {
				//Project
				hi.items.ProjectListProjects.remove(item);
			}
			
			hi.items.manager.removeRecursive(id);
			
			Hi_Notification.putMessageInQue('task_archive');
			HiTaskCalendar.update();
		};

		if (isProject) {
			if (item.animate) {
				item.animate('puff',false,onAnimationComplete);
			} else {
				onAnimationComplete();
			}
		} else if (item_DOM.animate && Hi_Preferences.get('currentTab') != 7/*Date tab*/) {
			item_DOM.animate(item, "puff", false, onAnimationComplete);
		} else {
			onAnimationComplete();
		}
		
		return;
	};
	
	return dojo.global.archiveTask;
	
});