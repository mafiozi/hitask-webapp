//dojo.provide("hi.project.duplicate");
define([
	"hi/project/base",
	'dojo/json',
	'dojo/_base/array'
], function(projectBase, dojoJson, dojoArray) {
	var Project = dojo.global.Project;
		
	dojo.mixin(Project, {
		
		duplicateItem: null,
		duplicateSource: null,
		
		tooltipMessageClose: function(timeout) {
			if (timeout && parseInt(timeout) > 0) {
				setTimeout(function() {
					Tooltip.close('message');
				}, timeout);
			} else {
				Tooltip.close('message');
			}
		},
		
		/* Duplicate task and all children */
		duplicateTree: function (id, options, overwrite, id_target) {
			var original_data = hi.items.manager.get(id),
				data = null;
			
			if (!original_data || original_data.category == 5) {
				Project.duplicateReceive(false, options, true);
				return;
			}
			
			var options = options || {};
			var rename = ('rename' in options ? options.rename : true);
			
			if (!id_target) {
				//Show "Creating copy..." message
				/*
				Tooltip.open('message', "", document.body);
				var scrolltop = dojo._docScroll().y;
				var viewport = dijit.getViewport().h;
				var el = dojo.byId("tooltip_message");
					el.style.top = 0;
		    		el.style.top = ~~(viewport / 2 - el.offsetHeight / 2 + scrolltop) + 'px';
		    	*/
				Tooltip.open('message','',document.body,null,null,null,{orient:false});
				var scrolltop = dojo._docScroll().y;
				var viewport = dijit.getViewport();
				
				var el = dojo.byId("tooltip_message_TooltipDialog").parentNode;//dojo.byId("tooltip_message_tooltipDialog");
					el.style.top = 0;
		    		el.style.top = ~~(viewport.h / 2 - el.offsetHeight / 2 + scrolltop) + 'px';
		    		el.style.left = ~~(viewport.w/2 - el.offsetWidth/2) + 'px';
			}
			
			if (!id_target || id_target === true) {
				//Duplicating main item
				
				var parent = (overwrite && overwrite.parent ? overwrite.parent : original_data.parent);
				var that = this;
				
				//Duplicate task
				Project.duplicate(id, {'rename': rename, 'after': function (data) {
					var opt = dojo.mixin({}, options, {
							after: function () {
								that.tooltipMessageClose(500);
								/*#2509hi.items.manager.refresh(); // update for item order*/
								
								if (dojo.isFunction(options.after)) options.after();
							}
						});
					
					if (data && 'id' in data) {
						//Call to duplicate all children
						Project.duplicateTree(id, opt, null, data.id);
					} else {
						opt.after();
					}
					
				}}, {'parent': parent});
				
			} else {
				var cnt = 0;
				
				var fn_on_finalize = function () {
					Tooltip.close('message');
					/*#2509 hi.items.manager.refresh(); // update for item order */
					
					if (dojo.isFunction(options.after)) options.after();
				};
				
				var fn_on_inner_complete = function () {
					cnt--;
					
					if (cnt == 0) {
						fn_on_finalize();
					}
				};
				
				var fn_on_complete = function (data, old_id) {
					var children = hi.items.manager.get({"parent": old_id});
					if (data && 'id' in data && children.length) {
						
						var opt = dojo.mixin({}, options, {after: fn_on_inner_complete});
						Project.duplicateTree(old_id, opt, null, data.id);
						
					} else {
						cnt--;
						
						if (cnt == 0) {
							fn_on_finalize();
						}
					}
				};
				
				var new_parent = (id_target && id_target !== true ? id_target : id),
					items = hi.items.manager.get({"parent": id}),
					i = 0,
					ii = items.length;
				
				for (; i<ii; i++) {
					//No files
					if (items[i].category != 5) {
						
						cnt++;
						
						//Preserve ID
						var fn = (function (id) {
							return function (data) { fn_on_complete(data, id); };
						})(items[i].id);
						
						Project.duplicate(items[i].id, {'rename': false, 'after': fn}, {'parent': new_parent});
						
					}
				}
				
				if (cnt == 0) {
					fn_on_finalize();
				}
			}
		},
		
		/* Dublicate task */
		duplicate: function (id, options, overwrite) {
			var original_data = hi.items.manager.get(id),
				data = null;
			
			if (!original_data || original_data.category == 5) {
				Project.duplicateReceive(false, options, true);
				return;
			}
			
			var defOpt = {after: function(){}, afterRequest: function(){}};
				options = dojo.mixin(defOpt, options || {});
			
			var data = dojo.mixin({}, original_data, {
				'my': true,
				'user_id': Project.user_id,
				'time_last_update': '',
				'last_comment': '',
				'last_comment_create_datetime': '',
				'last_comment_id': '',
				'last_comment_user_id': ''
			}, overwrite || {});
			
			//New task doesn't have id
			delete(data.id);
			/*
			 * #3200 - and remove instances data, otherwise the taskid of the new item
			 * and the task id in the instances array will not match
			 */
			if ('instances' in data) delete(data['instances']);
			//Check if sharing is allowed
			if (!Project.businessId) {
				delete(data.shared);
			}
			// #4904 and #5857
			dojoArray.forEach(['permissions', 'alerts'], function(x, i) {
				if (x in data && typeof(data[x]) == 'object') {
					data[x] = dojoJson.stringify(data[x]);
				}
			});
			
			//New task title should be "Item 1" or "Item N"   
			if (!('rename' in options) || options.rename) {
				data.title = this.renameTitle(data.title);
			}
			
			if (!Project.businessId) {
				delete(data.permissions);
			}
			
			Project.duplicateSource = id;
			Project.duplicateItem = data;
			
			options.duplicateSource = id;
			options.duplicateItem = data;
			
			if (Hi_Preferences.get('duplicate_with_files', false, 'bool')) {
				data.duplicateFilesFrom = id;
			}
			
			//Save
			hi.items.manager.setSave(data).then(function (id) {
				Project.duplicateReceive(hi.items.manager.get(id), options);
				if (original_data.time_track && original_data.time_est > 0) {
					Project.duplicateTimeLogs(original_data.id, id);
				}
			}, function (error) {
				Project.duplicateReceive(false, options);
			});
		},
		
		duplicateTimeLogs: function(originalTaskId, newTaskId) {
			if (Hi_TimeTracking.list && Hi_TimeTracking.list.data) {
				var sent = false;
				for (var i = 0, len = Hi_TimeTracking.list.data.length; i < len; i++) {
					var log = Hi_TimeTracking.list.data[i];
					if (log.item_id == originalTaskId) {
						Hi_TimeTracking.list._sendDataToServer(newTaskId, log);
						sent = true;
					}
				}
				
				// #3471: it may happen that duplicated item was not expanded and list is empty,
				// so we need to fetch time log from server.
				if (!sent) {
					Hi_TimeTracking.list.fetchData(originalTaskId, function(data) {
						for (var i = 0, len = data.length; i < len; i++) {
							var log = data[i];
							if (log.item_id == originalTaskId) {
								Hi_TimeTracking.list._sendDataToServer(newTaskId, log);
							}
						}
					});
				}
			}
		},
		
		renameTitle: function(title) {
			// #2522
			var titleNew = new String(title);
			var curId = title.match(/\d+$/);
			var newId = curId ? parseInt(curId[0]) + 1 : 1;
			
			do {			
				if (curId) {
					titleNew = title.replace(/\d+$/, newId);
				} else {
					titleNew = title + ' ' + newId;
				}
			} while (hi.store.ProjectCache.query({title: titleNew}).length != 0 && ++newId < 100);
			
			return titleNew;
		},
		
		duplicateReceive: function (data, options, silent) {
			if (data == false) {
				//Clean up
				Project.duplicateItem = false;
				Project.duplicateSource = false;
				Tooltip.close('message');
				
				silent = silent || false;
				if (!silent) {
					//ALERT('Error! Task isn\'t duplicated. data == false');
					alert('Oops! There was an error duplicating task. Please try again later.');
				}
				return false;
			}
			
			try {
				Project.duplicateFinalize(data, options);
			} catch (e) {
				//Catch errors in duplication process
				Tooltip.close('message');
				hi.items.manager.refresh(); // update for item order
			}
			
			if (data.id > 0) {
				// setTimeout because of unknown Safari bug with innerHTML
				dojo.global.setTimeout(function() {HiTaskCalendar.update()}, 100);
			}
		},
		
		duplicateFinalize: function (data, options) {
			var duplicateItem = options.duplicateItem || Project.duplicateItem;
			var duplicateSource = options.duplicateSource || Project.duplicateSource;
			
			options = options || {};
			
			if (data.id > 0) {
				Hi_Reminder.setReminder(data.id);
				Hi_Tag.dropCache();
			}
			
			Project.duplicateSource = null;
			Project.duplicateItem = null;
			delete(options.duplicateItem);
			delete(options.duplicateSource);
			
			if (('after' in options) && typeof options.after == 'function') {
				options.after(data);
			}
		},
		
		duplicateProjectItem: null,
		duplicateProjectSource: null,
		
		duplicateProject: function (project_id) {
			//Project duplication already in progress
			if (Project.duplicateProjectSource) return;
			
			Project.duplicateProjectSource = project_id;
			
			//New project title should be "Item 1" or "Item N"   
			var title = hi.items.manager.get(project_id, 'title'),
				curId = title.match(/\d+$/),
				counter = 1;
			title = title.replace(/\s+\d+\s*$/,'');
			
			/*Bug #1874 - create a unique title
			if (curId) {
				title = title.replace(/\d+$/, parseInt(curId[0]) + 1);
			} else {
				title += ' 1';
			}
			*/
			if (hi.items.manager.get({title:title+' '+counter}).length>0) {
				while (hi.items.manager.get({title:title+' '+counter}).length>0) {counter++;}
				title += ' ' + counter;
			} else
			{
				title += ' 1';
			}
			var perms = hi.items.manager.get(project_id, 'permissions');
			/* #2114 - add priority for duplication */
			Project.duplicateProjectItem = {
				title: title,
				message: hi.items.manager.get(project_id, 'message'),
				shared: hi.items.manager.get(project_id, 'shared'),
				user_id: hi.items.manager.get(project_id, 'user_id'),
				permissions: typeof(perms) == 'string' ? perms : dojoJson.stringify(perms),
				category: 0,
				priority:/* #2114 hi.items.manager.get(project_id, 'priority')*/Project.getProjectMaxPriority(1) + 10
			};
			
			if (hi.items.manager.get(project_id, 'color_value')) {
				Project.duplicateProjectItem['color_value'] = hi.items.manager.get(project_id, 'color_value');
			}
			
			if (!Project.businessId) {
				delete(Project.duplicateProjectItem.shared);
				delete(Project.duplicateProjectItem.permissions);
			}
			
			//Show "Creating copy..." message
			//Tooltip.open('message', "", document.body);
			Tooltip.open('message','',document.body,null,null,null,{orient:false});
			var scrolltop = dojo._docScroll().y;
			var viewport = dijit.getViewport();
			
			var el = dojo.byId("tooltip_message_TooltipDialog").parentNode;//dojo.byId("tooltip_message_tooltipDialog");
				el.style.top = 0;
	    		el.style.top = ~~(viewport.h / 2 - el.offsetHeight / 2 + scrolltop) + 'px';
	    		el.style.left = ~~(viewport.w/2 - el.offsetWidth/2) + 'px';

			hi.items.manager.setSave(Project.duplicateProjectItem).then(function (id) {
				Project.duplicateProjectReceive(id);
			}, function (error) {
				Project.duplicateProjectReceiveError(error);
			});
		},
		
		duplicateProjectReceiveError: function (error) {
			//Clean up
			Project.duplicateProjectSource = null;
			Project.duplicateProjectItem = null;
			Tooltip.close('message');
			
			var message = "Project wasn't saved! Try later.";
			
			if (error.responseText) {
				try {
					var response = dojo.fromJson(error.responseText);
					message = response.error_message || message;
				} catch (error) {}
			}
			
			ALERT('Error! Project isn\'t saved. data == false');
			alert(message);
		},
		
		duplicateProjectReceive: function (id) {
			//Duplicate tasks
			var pdata = hi.items.manager.get({"parent": Project.duplicateProjectSource}),
				ids = [],			//list of task ids, which should be duplicated
				completed = 0;		//duplicated item count 
			
			//Filter tasks, which belongs to project and are not files
			for(var i=0, ii=pdata.length; i<ii; i++) {
				var category = parseInt(pdata[i]['category']);
				if (pdata[i].id > 0 && category != 0 && category != 5) {
					ids.push(pdata[i].id);
				}
			}
			
			/* #2778 - set new task list as rendered so it can have new items added to it */
			var newProject = hi.items.ProjectListProjects.get(id);
			if (newProject && newProject.lists) newProject.lists[0].set('rendered',true);
			
			Project.duplicateProjectSource = null;
			var that = this;
			
			//Redraw when all tasks are duplicated
			var fn_on_complete = function () {
				completed++;
				
				if (completed >= ids.length) {
					that.tooltipMessageClose(500);
					/* #2509 hi.items.manager.refresh();*/
				} else if (ids.length == 0) {
					that.tooltipMessageClose(500);
				}
			};
			
			//Duplicate tasks
			if (ids.length) {
				for(var i=0,ii=ids.length; i<ii; i++) {
					Project.duplicateTree(ids[i], {'rename': false, 'after': fn_on_complete}, {"parent": id});
				}
			} else {
				fn_on_complete();
				//Tooltip.close('message');
			}
		}
		
	});

	return Project;

});