//dojo.provide("hi.project.task");
define([
    'dojo/on',
	"hi/project/base"
], function (dojo_on) {
	
	var Project = dojo.global.Project;
			
	dojo.mixin(Project, {
		
		dragged: false,
		opened: false,
		openedInstance: false,
		clickedTaskNode: false,
		openEdit: false,
		opening: false,
		propOpenProc: false,
		lists: {
			color: {}
		},
		push: function (data) {
			var def = {
				user_id: Project.user_id,
				title: '',
				parent: '',
				color: '',
				assign_user: '',
				assign: [''],
				category: '0',
				message: '',
				parent: '',
				recurring: '0',
				time_last_update: '',
				start_date: '',
				start_time: '',
				end_date: '',
				end_time: '',
				shared: 0,
				reminder_enabled: 0,
				reminder_time: 0,
				reminder_time_type: 'm',
				completed: 0,
				time_track: 0,
				time_est: 0,
				priority: 20000
			};
			data = dojo.mixin(def, data);
			if (!data.id) {
				return false;
			}
			Project.data[data.id] = data;
			Project.doCounting();
			return true;
		},
		js_containers_loaded: false,
		js_containers: {},
		containerCounter: 0,
		setContainer: function (func, fname) {
			var name = '_js_container_' + (++Project.containerCounter);
	/*		if (func.name) {
				var fname = func.name.toString();
			} else {
				var fname = func.toString();
				var p1 = fname.indexOf(' ');
				var p2 = fname.indexOf('(');
				fname = fname.substring(p1 + 1, p2);
			}
	*/
			Project.js_containers[fname] = {id: name, func: func};
			return name;
		},
		getContainer: function (name) {
			var con = Project.js_containers;
			if (name in con) {
				return con[name]['id'];
			} else {
				return false;
			}
		},
		
		setPriorities: function (priorities) {
			// summary:
			//		Update task priorities
			// priorities: Object
			//		List with task id: priority
			
			var has_changes = false;
			var changes = {};
			var deferred = null;
			
			for(var id in priorities) {
				if (hi.items.manager.get(id, "priority") != priorities[id]) {
					deferred = hi.items.manager.setSave(id, {'priority': priorities[id]});
				}
			}
			
			if (deferred) {
				deferred.then(function () {
					hi.items.manager.refresh();
				});
			}
		},
		
		_move_ts: function(name, id, id2, before) {
			before = before || false;
			id2 = id2 || false;
			
			//If not sorted by priority, then task order shouldn't be changed
			if (Project.getSorting() != Project.ORDER_PRIORITY || id == id2) return;
			
			var s = [], id2_index = 0;
			var pdata = hi.items.manager.get();
			var updated = false;
			var update_list = {};
			
			function getPriorityGroup(p) {
				p = parseInt(p);
				return (p < 20000 ? 0 : (p >= 30000 ? 2 : 1));
			}
			function normalizeInGroup(p, g) {
				var max = (g == 0 ? 19999 : (g == 1 ? 29999 : 39999));
				var min = (g == 0 ? 10000 : (g == 1 ? 20000 : 30000));
				return Math.max(Math.min(p, max), min);
			}
			
			
			var fn = function () { return true; };
			
			//Removes items which are not in same group or same level (task children)
			if (id2) {
				var group = Hi_Preferences.get('currentTab','','string'),
					item2 = id2 ? hi.items.manager.get(id2) : null,
					i2_ch = item2 && Project.isTaskChild(item2.id) ? item2.parent : false;
					fn = null;	
				
				switch(group) {
					case 'my':
						//Must have same starred value and same task parent
						fn = function (item1) {
							if (!item2) return true;
							
							//Check if same parent task (by parent task only, items are not grouped by project)
							//Children files are not visible in the list
							var i1_ch = Project.isTaskChild(item1.id) && item1.category != 5 ? item1.parent : false;
							
							return item1.starred == item2.starred && i1_ch == i2_ch;
						};
						break;
					case 'project':
						//Project
						fn = function (item1) {
							return item2 ? item1.parent == item2.parent : true;
						};
						break;
					case 'color':
						//Color
						fn = function (item1) {
							if (!item2) return true;
							
							//Check if same parent task (by parent task only, items are not grouped by project)
							//Children files are not visible in the list
							var i1_ch = Project.isTaskChild(item1.id) && item1.category != 5 ? item1.parent : false;
							
							if (i1_ch || i2_ch) {
								//One or both are children of task, so not grouped by color
								return i1_ch == i2_ch;
							} else {
								return item1.color == item2.color;
							}
						};
						break;
					case 'team':
						//Team
						fn = function (item1) {
							if (!item2) return true;
							
							//Check if same parent task (by parent task only, items are not grouped by project)
							//Children files are not visible in the list
							var i1_ch = Project.isTaskChild(item1.id) && item1.category != 5 ? item1.parent : false;
							
							if (i1_ch || i2_ch) {
								//One or both are children of task, so not grouped by assignee
								return i1_ch == i2_ch;
							} else {
								return item1.assignee == item2.assignee;
							}
						};
						break;
				}
			}
			
			//Get item list with priorities
			for (var i=0,ii=pdata.length; i<ii; i++) {
				if (pdata[i].category != 0 && pdata[i].id != id && fn(pdata[i])) {
					s.push({'id': pdata[i].id, 'priority': parseInt(pdata[i].priority) || 20000});
				}
			}
			
			//Sort by priority
			s.sort(function (a, b) {
				return (a.priority == b.priority ? 0 : (a.priority < b.priority ? 1 : -1));
			});
			
			for(var i=0,ii=s.length; i<ii; i++) {
				if (s[i].id == id2) {
					id2_index = i;
					break;
				}
			}
			
			//Change item priority
			if (id2 === false) {
				if (dojo.dnd.HiSortDragExtra.last_priority == dojo.dnd.HiSortDragExtra.initial_priority) {
					//Item was moved to different project/color/starred/..., but priority didn't changed
				} else {
					//Try finding next element
					var item = hi.items.manager.item(id);
						next_item = item ? item.next() : null;
						
					if (next_item) {
						//Use next element for reference
						var id2 = next_item.taskId;
						
						var priority = parseInt(hi.items.manager.get(id2, 'priority'));
						
						//Droping item before first item must increase priority group
						var dnd = dojo.dnd.HiSortDragExtra;
						if (dnd.force_priority && dnd.last_priority) {
							priority = dnd.last_priority;
						}
						
						var group = getPriorityGroup(priority);
						
						priority++;
						priority = normalizeInGroup(priority, group);
						
						if (hi.items.manager.get(id, "priority") != priority) {
							hi.items.manager.setSave(id, {"priority": priority});
						}
						
						updated = true;
					} else {
						//Change priority using first item in the list as reference
						var prev_item = item ? item.prev() : null;
						
						if (prev_item) {
							if (s.length) {
								var priority = s[0].priority + 1;
								var group = getPriorityGroup(priority);
								priority++;
								
								//Droping item before first item must increase priority group
								var dnd = dojo.dnd.HiSortDragExtra;
								if (dnd.force_priority && dnd.last_priority) {
									priority = dnd.last_priority;
									group = getPriorityGroup(priority);
								}
								
								priority = normalizeInGroup(priority, group);
								
								if (hi.items.manager.get(id, "priority") != priority) {
									hi.items.manager.setSave(id, {"priority": priority});
								}
								
								updated = true;
							}
						}
					}
				}
			} else {
				//Use previous or next item for reference
				if (before) {
					var start_index = id2_index - 1;
					var priority = parseInt(hi.items.manager.get(id2, 'priority')) || 20000;
					var group = getPriorityGroup(priority);
						priority = priority + 1;
						priority = normalizeInGroup(priority, group);
					
				} else {
					if (id2_index + 1 >= s.length) {
						var priority = parseInt(hi.items.manager.get(id2, 'priority')) || 20000;
						var start_index = id2_index;
					} else {
						var priority = parseInt(s[id2_index + 1].priority);
						var group = getPriorityGroup(priority);
						priority = priority + 1;
						priority = normalizeInGroup(priority, group);
						var start_index = id2_index;
					}
				}
				
				//Droping item after last item must decrease priority group to "Normal" or "Low"
				var dnd = dojo.dnd.HiSortDragExtra;
				if (dnd.force_priority && dnd.last_priority) {
					priority = dnd.last_priority;
				}
				
				var priority_group = getPriorityGroup(priority);
				update_list[id] = priority;
				
				var group = getPriorityGroup(priority);
				for(var i=start_index; i>=0; i--) {
					priority = normalizeInGroup(priority+1, group);
					if (getPriorityGroup(s[i].priority) == priority_group) {
						update_list[s[i].id] = priority;
					} else {
						break;
					}
				}
				
				Project.setPriorities(update_list);
				updated = true;
			}
		},
		_move: function(name, id, id2, before, looped) {
			if (name == 'ts_') return Project._move_ts(name, id, id2, before ,looped);
			
			before = before || false;
			id2 = id2 || false;
			looped = looped || false;
			
			if (id == id2) return;
			
			function getPriority(id) {
				return parseInt(hi.items.manager.get(id, 'priority') || 0);
			}
			
			var priorityTop = 32000,
				priorityBottom = 10000,
				priorityNew = 0,
				priorityOld = getPriority(id);
			
			if (id2) {
				if (before) {
					priorityBottom = getPriority(id2);
				} else {
					priorityTop = getPriority(id2);
				}
			}
			
			var priorities = {},
				s = hi.items.manager.get({"category": 0});
			
			s = s.sort(function (a, b) {
				return (a.priority || 0) > (a.priority || 0);
			});
			
			if (before) {
				for (i = 0; i < imax; i++) {
					var priority = s[i].priority || 0;
					if (priority > priorityBottom && priority < priorityTop && s[i].id != id) {
						priorityTop = priority;
					}
				}
			} else {
				var imax = s.length;
				for (i = 0; i < imax; i++) {
					var priority = s[i].priority || 0;
					if (priority > priorityBottom && priority < priorityTop && s[i].id != id) {
						priorityBottom = priority;
					}
				}
			}
				
			//Old priority is still correct (between next and previous items)
			if (priorityTop > priorityOld && priorityOld > priorityBottom) return;
			
			if ((priorityTop - priorityBottom) <= 2) {
				//Reset all priorities
				var l = s.length;
				for (i = 0; i < l; i++) {
					if (id != s[i].id) {
						var priority = 10000 + Math.round((i + 1) / (l + 1) * 20000);
						var order = 50000 - priority;
						
						priorities[s[i].id] = priority;
					}
				}
				
				Project.setPriorities(priorities);
				
				var order = Project._move(name, id, id2, before, true);
				priorityNew = 50000 - order;
			} else {
				priorityNew = ~~((priorityTop - priorityBottom) / 2 + priorityBottom);
				var order = 50000 - priorityNew;
				
				if (hi.items.manager.get(id, "priority") != priorityNew) {
					hi.items.manager.setSave(id, {"priority": priorityNew});
				}
			}
			
			HiDragSourceCached.project = false;
			
			//Return new order
			return 50000 - priorityNew;
		},
		
		moveTask: function(id, id2, before) {
			var m = Project._move('ts_', id, id2, before);
			if (m !== undefined && m !== null) {
				var taskItem = hi.items.manager.item(id);
				if (taskItem) {
					taskItem.highlight();
				}
			}
			return m;
		},
		
		/**
		 * Returns current sorting order, if none is set, then
		 * returns subject (ORDER_PRIORITY) constant value
		 * 
		 * @return Sorting order constant
		 * @type {Number}	{0: default, 2: Project}
		 */
		getSorting: function (type) {
			if (!type) return Hi_Preferences.get('sorting', Project.ORDER_PRIORITY, 'int');

			if (type === 2) return Hi_Preferences.get('projectSorting', Project.ORDER_SUBJECT, 'int');
		},
		
		hideSortingAndFilterPopups: function() {
			var sortingNodes = dojo.query('#toolbar .sorting div.popup'),
				sortingNodeLen = sortingNodes.length,
				filterNode = dojo.query('#taskFilterPopup'), i,
				wrapper = dojo.query('div.wrapper', 'centerPaneContent')[0];

			for (i = 0; i < sortingNodeLen; i++) {
				dojo.removeClass(sortingNodes[i], 'popup-opened');
			}
			if (filterNode.length) {
				dojo.removeClass(filterNode[0], 'popup-opened');
				if (wrapper) wrapper.style.overflow = '';
			}
		},
		

		changeProjectSorting: function(sorting) {
			var old = Project.getSorting(2);
			var sorting = parseInt(sorting) || Project.ORDER_PRIORITY;
			Hi_Preferences.set('projectSorting', sorting);
			
			//Apply styles
			// var container = dojo.byId(Project.getContainer('tasksList'));
			// dojo.removeClass(container, 'sorting-' + old);
			// dojo.addClass(container, 'sorting-' + sorting);
			
			//Display Loading in the center column like when page is first loaded
			var count = hi.store.ProjectCache.query().length,
				delay = 1;
			
			if (count > 250) {
				//If more than 250 tasks, then show loading icon
				dojo.addClass(container, "loading");
				delay = 160;
			}
			
			// //Set sorting
			// hi.store.ProjectCache.sort = hi.store.Project.getOrderConfiguration();
			
			//Redraw list
			//Delayed because we want to allow do other things before afterSorting using dojo.connect 
			setTimeout(function () {
				Project.afterSorting(sorting);
				
				if (count > 250) {
					setTimeout(function () {
						dojo.removeClass(container, "loading");
					}, 16);
				}
			}, delay);
		},

		sortingSelectChange: function(val) {
			console.log('in sorting change');
			this.changeSorting(val);
		},

		projectSortingSelectChange: function(val) {
			console.log('in project sorting change');
			this.changeProjectSorting(val);
		},
		/**
		 * Changes sorting order and reloads data
		 * 
		 * @param {Object} order
		 */
		changeSorting: function (sorting) {
			var old = Project.getSorting();
			var sorting = parseInt(sorting) || Project.ORDER_PRIORITY;
			Hi_Preferences.set('sorting', sorting);
			
			//Apply styles
			var container = dojo.byId(Project.getContainer('tasksList'));
			dojo.removeClass(container, 'sorting-' + old);
			dojo.addClass(container, 'sorting-' + sorting);
			
			//Display Loading in the center column like when page is first loaded
			var count = hi.store.ProjectCache.query().length,
				delay = 1;
			
			if (count > 250) {
				//If more than 250 tasks, then show loading icon
				dojo.addClass(container, "loading");
				delay = 160;
			}
			
			//Set sorting
			hi.store.ProjectCache.sort = hi.store.Project.getOrderConfiguration();
			
			//Redraw list
			//Delayed because we want to allow do other things before afterSorting using dojo.connect 
			setTimeout(function () {
				Project.afterSorting(sorting);
				
				if (count > 250) {
					setTimeout(function () {
						dojo.removeClass(container, "loading");
					}, 16);
				}
			}, delay);
		},
		
		afterSorting: function (sorting) {
			hi.items.manager.redraw();
		},
		
		/* Feature #2154 */
		getProjectMaxPriority:function(level)
		{
			var max = (level == 0 ? 10000 : (level == 1 ? 20000 : 30000));
			dojo.forEach(hi.items.manager.get({category:hi.widget.TaskItem.CATEGORY_PROJECT}), function(item) {
				var priority = parseInt(item.priority);
				if ((level == 0 && priority < 20000) || (level == 1 && priority >= 20000 && priority < 30000) || (level == 2 && priority >= 30000)) {
					max = Math.max(max, priority);
				}

			});
			//Make sure priority + 1 is not exceededing max
			if (level == 0) {
				max = Math.min(max, 19998);
			} else if (level == 1) {
				max = Math.min(max, 29998);
			} else {
				max = Math.min(max, 39998);
			}		
			return max;
		},
		
		/**
		 * Returns max priority for given level
		 * (0 - low, 1 - medium, 2 - high)
		 */
		getMaxPriority: function (level, andIncrease) {
			//Get minimum priority
			var max = (level == 0 ? 10000 : (level == 1 ? 20000 : 30000));
			var items = hi.items.manager.get({}),
				i = 0,
				ii = items.length;
			
			for (; i<ii; i++) {
				if (items[i].category != 0) {
					var priority = parseInt(items[i].priority);
					if ((level == 0 && priority < 20000) || (level == 1 && priority >= 20000 && priority < 30000) || (level == 2 && priority >= 30000)) {
						max = Math.max(max, priority);
					}
				}
			}
			
			if (typeof(andIncrease) == 'number') {
				max += parseInt(andIncrease);
			}
			
			//Make sure priority + 1 is not exceededing max
			if (level == 0) {
				max = Math.min(max, 19998);
			} else if (level == 1) {
				max = Math.min(max, 29998);
			} else {
				max = Math.min(max, 39998);
			}
			
			return max;
		},
		
		selectColor: function(t, e) {
			var li = getParentByClass(t, 'task_li');
			var color = 0;
			for (var i = 1; i < 8; i++) {
				if (dojo.hasClass(li, 'color' + i)) {
					color = i;
					break;
				}
			}
			var el = dojo.byId('colors');
			dojo.forEach(dojo.query('.selected', el), function(el) {dojo.removeClass(el, 'selected');});
			if (color) {
				var el = dojo.query('.color' + color, el);
				if (el.length) dojo.addClass(el[0], 'selected');
			} else {
				var el = dojo.query('.nocolor', el);
				if (el.length) dojo.addClass(el[0], 'selected');
			}
		},
		getNewTaskText: function() {
			return hi.i18n.getLocalization('project.enter_item_name');
		},
		
		printTasks: function () {
			//Call print
			dojo.global.print();
		},
		
		getEventStartTimeWidget: function(start_date, start_time) {
			var d = start_date;
			var t = start_time;
			if (!d) {
				return false;
			}
			if (!t) {
				if (start_date) {
					t = '00:00';
				} else {
					return false;
				}
			}
			return str2time(d + ' ' + t, Hi_Calendar.format + ' h:i');
		},
		getEventEndTimeWidget: function(end_date, end_time) {
			var d = end_date;
			var t = end_time;
			if (!d) {
				return false;
			}
			if (!t) {
				if (end_date) {
					t = '23:59';
				} else {
					return false;
				}
			}
			return str2time(d + ' ' + t, Hi_Calendar.format + ' h:i');
		},
		
		
		recurringName: function(id) {
			switch (parseInt(id)) {
				case 1: return hi.i18n.getLocalization('project.daily');
				case 2: return hi.i18n.getLocalization('project.weekly');
				case 3: return hi.i18n.getLocalization('project.monthly');
				case 4: return hi.i18n.getLocalization('project.yearly');
				default: return hi.i18n.getLocalization('project.none');
			}
		},
		iterateTaskElements: function(id, func, className) {
			var el = dojo.byId('task_' + id);
			
			if (!el) return;
			var taskEl = null;
			if (className) {
				taskEl = el;
				el = dojo.query('.' + className, el);
				el = (el.length ? el[0] : null);
			}
			
			if (!el) return;
			func(el, id, taskEl);
		},
		
		getRecurringInstanceFromDate: function(item, date){
			if (typeof item.recurring_instances != 'undefined') {
				
				for(var i in item.recurring_instances){
					var recurring_instance = item.recurring_instances[i];
					
					if(!item.end_date){
						if(date == i){
							return i;
						}
					}else{
						
						var calendarDate = new Date(date); 
						
						switch(parseInt(item.recurring)){
							case 4:
								var start = new Date(i);
								var end = new Date(start);
								end.setYear(start.getFullYear() + 1);
								
								if(calendarDate >= start && calendarDate < end){
									return i;
								}
								break;
							case 3:
								var start = new Date(i);
								var end = new Date(start);
								end.setMonth(start.getMonth() + 1);
								
								if(calendarDate >= start && calendarDate < end){
									return i;
								}
								break;
							case 2:
								var start = new Date(i);
								var end = dojo.date.add(start, 'week', 1);
								
								if(calendarDate >= start && calendarDate < end){
									return i;
								}
								break;
							case 1:
								if(i == date){
									return i;
								}
								break;
						}
					}
				}
			}
			return false;
			
		},
		
		getEventStatus: function (id, event_date) {
			if (Project.data[id]){
				var recInst = Project.getRecurringInstanceFromDate(Project.data[id],event_date);
				if(recInst){
					return Project.data[id].recurring_instances[recInst].status;
				}
			}
			return 0;
		},
		
		isEventComplete: function (id, instance_date) {
			if (Project.data[id]){
				var recInst = Project.getRecurringInstanceFromDate(Project.data[id],instance_date);
				if(recInst){
					return Project.data[id].recurring_instances[recInst].status == 1;
				}
			}
			return false;
		},
		
		/**
		 * Returns true if ID belongs to task. If tasks doesn't exist, return false;
		 * 
		 * @param {Number} id
		 */
		isTask: function (id) {
			var category = parseInt(hi.items.manager.get(id, "category") || 0);
			return (category ? true : false);
		},
		
		/**
		 * Returns number of children this task has
		 * @param {Number} id
		 * @return Number of children
		 * @type {Number}
		 */
		hasChildren: function (id) {
			return hi.items.manager.get({"parent": id}).length;
		},
		
		/**
		 * Returns number of children which are not files
		 * Only valid if children are loaded
		 * @param {Number} id
		 * @return Number of children
		 * @type {Number}
		 */
		hasNonFileChildren: function (id) {
			var children = hi.items.manager.get({"parent": id}),
				count = children.length;
			
			if (!count) return 0;
			
			for(var i=0, ii=children.length; i<ii; i++) {
				if (children[i].category == 5) count--;
			}
			
			return count;
		},
		
		/**
		 * Returns true if task is a children of another task, otherwise false
		 * 
		 * @param {Number} id
		 */
		isTaskChild: function (id) {
			return Project.isTask(hi.items.manager.get(id, "parent") || 0);
		},
		
		isTaskExpanded: function (id) {
			return (id in Project.expandedTasks && Project.expandedTasks[id]); 
		},
		setTaskExpanded: function (id, state) {
			Project.expandedTasks[id] = state;
		},
		
		/**
		 * Returns parent task ID or 0
		 * 
		 * @param {Number} id
		 */
		getParentTask: function (id) {
			var parent = hi.items.manager.get(id, 'parent') || 0;
			if (parent) {
				return parseInt(hi.items.manager.get(parent, 'category') || 0) > 0 ? parent : 0;
			} else {
				return 0;
			}
		},
		
		fadeOutTask: function (id, event, after) {
			var node = dojo.byId('task_' + id);
			var callback = function () {
				if (dojo.isFunction(after)) return after();
			};
			
			//Effects were disabled in IE, because of performance issues
			if (!node || (dojo.isIE && dojo.isIE < 9)) return callback();
			
			var callbackHide = function () {
				fakeNode.parentNode.removeChild(fakeNode);
				callback();
			};
			
			var endOpacity = 0;
			var pos = dojo.position(node, true);
			var fakeNode = document.createElement('DIV');
				fakeNode.className = 'fake_task';
				fakeNode.style.left = ~~(pos.x) + 'px';
				fakeNode.style.top = ~~(pos.y) + 'px';
				fakeNode.style.width = node.offsetWidth - 2 + 'px';
				fakeNode.style.height = node.offsetHeight - 4 + 'px';
				fakeNode.innerHTML = '<span>' + Project.get(id, 'title') + '</span>';
			
			document.body.appendChild(fakeNode);
			
			var effect = 'dropout';
			var effectDir = ~~(node.offsetHeight * 1.4);
			
			if ((event == 'complete' && Project.get(id, 'completed') == '1') || event == 'archive' || event == 'restore' || event == 'restore_copy') {
				effectDir = -effectDir;
			} else if (event == 'duplicate') {
				effectDir = - node.offsetHeight;
				effect = 'dropin';
			} else if (event == 'remove') {
				effect = 'puff';
			} else if (event == 'test') {
				effect = 'none';
				endOpacity = 1;
				callbackHide = function () {
					callback();
				};
			}
			
			if (dojo.hasClass(node, 'highlight')) {
				fakeNode.className = 'fake_task fake_task_highlight';
			}
			
			if (event == 'duplicate') {
				node.style.visibility = 'hidden';
				endOpacity = 1;
				
				setTimeout(function () {
					node.style.visibility = 'visible';
				}, 650);
			} else if (event != 'restore_copy' && event != 'restore') {
				node.style.visibility = 'hidden';
			
				var ic = 0; for(var i in Project.data) if (Project.data[i] != '0') ic++;
				
				if (ic <= 20) {
					node.style.padding = '0';
					dojo.anim(node, {
						height: {end: 0}
					}, 650);
				}
			}
			
			if (effect == 'dropout') {
				dojo.anim(fakeNode, {
					top: {end: ~~(pos.y+effectDir)},
					height: {end: 28}
				}, 650);
			} else if (effect == 'dropin') {
				dojo.anim(fakeNode, {
					top: {end: ~~(pos.y), start: ~~(pos.y+effectDir)}
				}, 650);
			} else if (effect == 'puff') {
				var scaleFactor = 20;
				dojo.anim(fakeNode, {
					top: {end: ~~(pos.y-scaleFactor)},
					left: {end: ~~(pos.x-scaleFactor)},
					width: {end: node.offsetWidth - 2 + scaleFactor * 2},
					height: {end: node.offsetHeight - 4 + scaleFactor * 2}
				}, 650);
			}
			
			dojo.anim(fakeNode, {
				opacity: {end: endOpacity}
			}, 325, null, callbackHide, 325);
		},
		
		showDeleteConfirmation: function (message, target, callback) {
			var node = dojo.byId('deleteconfirmation_message');
			node.innerHTML = message;
			
			Tooltip.open('deleteconfirmation', "", target, null, null, null, {orient:false});
			
			//Initialize tooltip if needed
			var tooltip = dojo.byId('tooltip_deleteconfirmation');
			if (!Project.removeTaskTooltip2Initialized) {
				dojo.connect(tooltip, 'click', function (ev) {
				    dojo.stopEvent(ev);
				});
				Project.removeTaskTooltip2Initialized = true;
			}
			
			var closeTooltip = function () {
				Tooltip.close('deleteconfirmation');
			};
			/* #2292
			var btns = tooltip.getElementsByTagName('BUTTON');
				
				//Delete task events
				btns[0].onclick = function () {
				    closeTooltip();
					callback();
				};
				//Delete task 
				btns[1].onclick = function (event) {
				   dojo.stopEvent(event);
				   closeTooltip();
				};
			*/
			var okSignal,cancelSignal;
			okSignal = dojo_on.once(dijit.byId('deleteConfirmationOKBtn'),'Click',function() {
				closeTooltip();
				callback();
				okSignal.remove();
				if (cancelSignal) cancelSignal.remove();
			});
			cancelSignal = dojo_on.once(dijit.byId('deleteConfirmationCancelBtn'),'Click',function(e) {
				dojo.stopEvent(e);
				closeTooltip();
				if (okSignal) okSignal.remove();
				cancelSignal.remove();
			});
			
		}
		
	});

	return Project;

});