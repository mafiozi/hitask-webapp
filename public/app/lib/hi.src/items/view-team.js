/**
 * Instantiation of task lists (and the groups that contain them) on Team tab:
 * hi.items.ProjectListTeam.projects[]
 * hi.items.TaskListTeamUngrouped
 */
define([
	"hi/store",
	"hi/items/base",
	'dojo/aspect'
], function (hi_store, hi_items,dojo_aspect) {
	
	//After hi.store.init create color groups
	dojo.global.viewTeamTab = function(q) {
	//dojo.connect(hi_store, "init", hi_store, function () {
		// Create task groups (each color + default list) and set filters
		
		var readStore = hi_store.ProjectCache,
			writeStore = hi_store.Project,
			query = q,//readStore.query(),
			
			groupSelf = 'team',
			groupCurrent = Hi_Preferences.get('currentTab', 'my', "string"),
			
			container = dojo.byId(Project.getContainer("tasksList")),
			
			group = null,
			taskList  = null,
			
			teamList = [];
		
		if (groupCurrent == groupSelf) {
			var myT = dojo.connect(hi_store,'afterLoad',function() {
				// Add to container
				if (groupSelf == groupCurrent) {
					var groups = hi_items.ProjectListTeam.projects;
					finalizeInitialization(groups,[hi_items.TaskListTeamUngrouped]);
				}
				dojo.disconnect(myT);
			});	
		}
		
		function getTeamList () {
			// summary:
			//		Returns sorted team member
			
			var output = [{"id": 0}],
				frlist = Project.friends,
				main = Hi_Preferences.get("main_assign_user");
			
			if (frlist) {
				var i, imax = frlist.length;
				for (i = 0; i < imax; i++) {
					if (frlist[i].id != Project.user_id && (frlist[i].subscription == "BOTH" || frlist[i].subscription == "BIS")) {
						output.push(frlist[i]);
					}
				}
			}
			
			output.sort(function (b, a) {
				if (b.id == 0) return -1;
				if (a.id == 0) return 1;
				
				var a_id = Math.round((a.id - a.id % 2) / 2);
				var b_id = Math.round((b.id - b.id % 2) / 2);
				if (a_id == b_id) {
					var a_type = a.id % 2;
					var b_type = b.id % 2;
					return a_type < b_type;
				}
				if (a_id != main && b_id != main) {
					return 0;
				}
				if (a_id == main && b_id != main) {
					return 1;
				}
				if (b_id == main && a_id != main) {
					return -1;
				}
				return 0;
			});
			
			return output;
		}
		dojo.global.getTeamList = getTeamList;
		
		function getTeamListDiff (a, b) {
			// summary:
			//		Returns object with index: item for items which were in a, but are not in b anymore
			// a: Array
			//		First array
			// b: Array
			//		Second array
			
			var i=0, ii=a.length,
				k=0, kk=b.length,
				found = false,
				output = {};
			
			for (; i<ii; i++) {
				found = false;
				for (k=0; k<kk; k++) {
					if (a[i].id == b[k].id) {
						found = true; break;
					}
				}
				if (!found) output[i] = a[i];
			}
			
			return output;
		}
		
		function getTeamGroup (id) {
			// summary:
			//		Returns hi.widget.Group from hi.items.ProjectListTeam by id
			
			var projects = hi_items.ProjectListTeam.projects,
				i = 0,
				ii = projects.length;
			
			for (; i<ii; i++) {
				if (projects[i].teamMemberId == id) return projects[i];
			}
		}
		
		function createGroup (item, index) {
			// summary:
			//		Create new group in team list (each group is a single user)
			
			var user_id = item.id || Project.user_id;
			var isMe = !item.id || item.id == Project.user_id;
			var friendName = !isMe ? Project.getFriendName(item.id) : "";
			var group = new hi.widget.Group({
				"projectId": "team" + (isMe ? item.id * 2 + 1 : 0),
				"teamMemberId": item.id,
				"emptyVisible": isMe,  // shown if there are no tasks in it
				"expanded": true,      // by default expanded
				
				"title": isMe ? hi.i18n.getLocalization("main.assigned_to_me") : hi.i18n.getLocalization("main.assigned_to") + " " + friendName,
				"moveTitle": isMe ? hi.i18n.getLocalization("team_list.myself") : friendName,
				"groupClassName": isMe ? "assigned_self" : "color2",
				
				"disabled": groupSelf != groupCurrent,
				"preferencesGroup": groupSelf,
				"isInProjectList": true
			});
			var taskList = new hi.widget.TaskList({
				"readStore": readStore,
				"writeStore": writeStore,
				"query": q,//groupSelf == groupCurrent?query:null,
				
				"disabled": groupSelf != groupCurrent,
				
				"filterType": "any", // if most outer parent matched then this item should be in the list
				"filter": function (item, strict) {
					// summary:
					//		Filter items which should be in the list. Returns true on success,
					//		false on failure
					// item:
					//		Item data
					/* #3229 - check there are any instances left */
					if (item.recurring) {
						if (!this.calculateInstanceDate(item)) return false;
					}
					if (item.assignee && item.assignee == user_id) return true;
					if (!strict && this.filterMatches(item.parent)) return true;
					return false;
				}
			});
			
			taskList.placeAt(group);
			hi_items.ProjectListTeam.add(group, index);
			return group;
		}
		
		hi_items.ProjectListTeam = new hi.widget.ProjectList();
		
		// Team
		teamList = getTeamList();
		dojo.forEach(teamList, function (item) {
			createGroup(item);
		});
		
		// Reset functionality
		hi_items.ProjectListTeam.reset = function () {
			// summary:
			//		Check user list for changes and update groups
			
			var projectListTeam = hi_items.ProjectListTeam,
				oldTeamList  = teamList,
				newTeamList  = getTeamList(),
				removedItems = getTeamListDiff(oldTeamList, newTeamList),
				addedItems   = getTeamListDiff(newTeamList, oldTeamList),
				found        = false,
				index;
			
			// Remove items
			for (index in removedItems) {
				projectListTeam.remove(getTeamGroup(removedItems[index].id));
			}
			
			// Add items
			for (index in addedItems) {
				var group = createGroup(addedItems[index], index);
				
				// Render data
				if (groupCurrent == groupSelf) {
					var k = 0;
						kk = group.lists.length;
					
					for (; k<kk; k++) {
						group.lists[k].reset({"fill": true});
					}
				}
			}
		};
		
		hi_items.ProjectListTeam.disableNonexistGroups = function() {
			var groups = hi_items.ProjectListTeam.projects;
			if (groups && groups.length) {
				for (var i = 0, len = groups.length; i < len; i++) {
					var friendId = groups[i].teamMemberId;
					if (friendId) {
						var friend = Project.getFriend(friendId);
						if (!friend) {
							groups[i].set('disabled', true);
						}
					}
				}
			}
		};
		
		// Ungrouped items
		hi_items.TaskListTeamUngrouped = new hi.widget.TaskList({
			"readStore": readStore,
			"writeStore": writeStore,
			"query": q,//groupSelf == groupCurrent?query:null,
			mainList:true,
			"disabled": groupSelf != groupCurrent,
			
			"initialyCollapsed": true, // on load list should be collapsed "xxx items not assigned"
			"localeItemCollapsed": hi.i18n.getLocalization("center.item_not_assigned"),
			"localeItemsCollapsed": hi.i18n.getLocalization("center.items_not_assigned"),
			
			"filterType": "top", // if most outer parent matched then this item should be in the list
			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				// strict:
				//		If true then don't check ancestors
				/* #3229 - check there are any instances left */
				if (item.recurring) {
					if (!this.calculateInstanceDate(item)) return false;
				}
				if (!item.assignee) return true;
				
				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			}
		});
		
		hi_items.ProjectListTeam.placeAt(container);
		hi_items.TaskListTeamUngrouped.placeAt(container);
		
		hi_items.ProjectListTeam.onGroupingChange = function (group, groupPrev) {
			// summary:
			//		On group change enable/disable lists
			var groups = hi_items.ProjectListTeam.projects;
			if (group == groupSelf && groupPrev != groupSelf) {
				refreshTab(groups,[hi_items.TaskListTeamUngrouped]);
			} else if (group != groupSelf && groupPrev == groupSelf) {
				for (var i=0, count=groups.length; i<count; i++) {
					groups[i].set("disabled", true);
				}
				hi_items.TaskListTeamUngrouped.set("disabled", true);
			}
			
			groupCurrent = group;
		};
		
		dojo.connect(hi.items.manager, "groupingChange", hi.items.manager, hi_items.ProjectListTeam.onGroupingChange);
		
		dojo.connect(Project, "changeSorting", Project, function () {
			// summary:
			//		On sorting change empty lists
			var groups = hi_items.ProjectListTeam.projects;
			/* #2509
			if (groupCurrent == groupSelf) {
				var groups = hi_items.ProjectListTeam.projects,
					lists = null, k = 0, kk = 0;
				
				for (var i=0, count=groups.length; i<count; i++) {
					lists = groups[i].lists;
					k = 0;
					kk = lists.length;
					
					for (; k<kk; k++) {
						lists[k].reset({"fill": false});
					}
				}
				
				hi_items.TaskListTeamUngrouped.reset({"fill": false});
			}
			*/
			for (var i=0, count=groups.length; i<count; i++) {
				groups[i].set("disabled", true);
			}
			hi_items.TaskListTeamUngrouped.set("disabled", true);
		});
		
		dojo.connect(hi.items.manager, "filterChange", hi.items.manager, function () {
			// summary:
			//		On filter empty lists
			
			if (groupCurrent == groupSelf) {
				/* #2509
				var groups = hi_items.ProjectListTeam.projects,
					lists = null, k = 0, kk = 0;
				
				for (var i=0, count=groups.length; i<count; i++) {
					lists = groups[i].lists;
					k = 0;
					kk = lists.length;
					
					for (; k<kk; k++) {
						lists[k].reset({"fill": false});
					}
				}
				
				hi_items.TaskListTeamUngrouped.reset({"fill": false});
				*/
				var groups = hi_items.ProjectListTeam.projects;
				for (var i=0, count=groups.length; i<count; i++) {
					groups[i].set("disabled", true);
				}
				
				hi_items.TaskListTeamUngrouped.set("disabled", true);
			}
		});
		
		/* This is invoked after refresh data store on filter selection change 
		 * This should just initiate the query observers on the lists and enable the lists
		 */
		dojo.connect(hi.items.manager, "redraw", hi.items.manager, function () {
			// summary:
			//		Some major change happened, redraw lists
			if (groupCurrent == groupSelf) {
				var groups = hi_items.ProjectListTeam.projects;
				/* #2331 - rereshTab on searchChange */
				//if (hi_items.TaskListTeamUngrouped.get('disabled')) initiateTab(groups,[hi_items.TaskListTeamUngrouped]);
				/*else*/ refreshTab(groups,[hi_items.TaskListTeamUngrouped]);
			}
		});
		
		hi.items.manager.getTaskLists[groupSelf] = function () {
			// summary:
			//		Return all task lists for this group
			
			var list = [hi_items.TaskListTeamUngrouped],
				projects = hi_items.ProjectListTeam.projects,
				i = 0,
				imax = projects.length;
			
			for (; i<imax; i++) list = list.concat(projects[i].lists);
			
			return list;
			
		};
	
	};//);

});