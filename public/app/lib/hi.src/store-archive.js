define([
	"hi/store"
], function (hi_store) {
	//hi_store === hi.store
	
	hi_store.initStore = function () {
		
		// Create project dojo store
		hi_store.create("Project", {
			target: "list/find",
			idAttribute: "id"
		});
		
		hi_store.Project.getOrder = function () {
			return "";
		};
		
		hi_store.Project.refresh = function () {
			// summary:
			//		Sync with server
			
			// Get offset
			var params = {
				"depth": Project.archive_max_items_per_page,
				"offset": 0,
				"realm": archived
			};
			
			return this.query({}, params);
		};
		
		// Load data
		//hi_store.Project.query({}).then(function () {
			// Once finished loading show content
			//var container = dojo.byId(Project.getContainer('tasksList'));
			//dojo.removeClass(container, "loading");
		//});
		Hi_Archive.getTaskList();
		
		dojo.parser.parse(dojo.byId('archiveSearchContainer'));
		dijit.byId('archiveSearchWrapper').on('change', Hi_Archive.onSearch);
	};
	
	return hi_store;
	
});
