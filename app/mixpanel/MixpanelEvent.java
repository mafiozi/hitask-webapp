package mixpanel;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents single Mixpanel event with event name and event parameters.
 * @author Vadim Manikhin
 */
public class MixpanelEvent {
	public static final String PROPERTY_NAME_OS = "$os";
	public static final String PROPERTY_NAME_BROWSER = "$browser";
	
	private String name;
	private Map<String, Object> params = new HashMap<String, Object>();
	
	public MixpanelEvent(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public MixpanelEvent addParam(String name, Object value) {
		params.put(name, value);
		return this;
	}
	
	public MixpanelEvent addOsParam(String value) {
		params.put(PROPERTY_NAME_OS, value);
		return this;
	}
	
	public MixpanelEvent addBrowserParam(String value) {
		params.put(PROPERTY_NAME_BROWSER, value);
		return this;
	}
	
	public Map<String, Object> getParams() {
		return params;
	}
}
