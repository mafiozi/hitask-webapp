package beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.Logger;

import com.google.gson.Gson;

/**
 *   0 - load after all required are loaded
 *   1 - ungrouped
 *   2 - NOT USED
 *   4 - date
 *   8 - color
 *  16 - project
 *  32 - user
 *  64 - calendar opened
 * 128 - team sidebar opened
 * 256 - latest news are shown
 *  -1 - don't load
 * @author Vadim Manikhin
 */
public class UserImages {
	private static final String IMAGES_ROOT = "/img";
	private static final int ALL = 1 | 2 | 4 | 8 | 16 | 32 | 64 | 128;
	private static Map<String, Integer> IMAGE_RULES = new HashMap<String, Integer>();
	
	static {
		IMAGE_RULES.put("icon_calendar.gif", -1);
		IMAGE_RULES.put("icon_search.gif", -1);
		IMAGE_RULES.put("loading.gif", -1);
		IMAGE_RULES.put("btn_drag.gif", ALL);
		IMAGE_RULES.put("btn_project_drag.gif", ALL);
		IMAGE_RULES.put("calendar*", 64);
		IMAGE_RULES.put("collapse.gif", ALL);
		IMAGE_RULES.put("colors_*", 0);
		IMAGE_RULES.put("colors.gif", 0);
		IMAGE_RULES.put("column_*", 64 | 128);
		IMAGE_RULES.put("crop_*", 0);
		IMAGE_RULES.put("datetime_*", 0);
		IMAGE_RULES.put("header_logo.png", ALL);
		IMAGE_RULES.put("icon_*", ALL);
		IMAGE_RULES.put("main_border_*", ALL);
		IMAGE_RULES.put("main_menu.*", ALL);
		IMAGE_RULES.put("main_navigation_*", -1);
		IMAGE_RULES.put("main_period.gif", 0);
		IMAGE_RULES.put("main_project_day_tasks_*t.gif", -1);
		IMAGE_RULES.put("main_project_today_tasks_*t.gif", -1);
		IMAGE_RULES.put("main_project_color*_tasks_*t.gif", -1);
		IMAGE_RULES.put("main_project_color1*", -1);
		IMAGE_RULES.put("main_project_color2*", -1);
		IMAGE_RULES.put("main_project_color3*", -1);
		IMAGE_RULES.put("main_project_color4*", -1);
		IMAGE_RULES.put("main_project_color5*", -1);
		IMAGE_RULES.put("main_project_color6*", -1);
		IMAGE_RULES.put("main_project_color7*", -1);
		IMAGE_RULES.put("main_project_color8*", 8);
		IMAGE_RULES.put("main_project_day_*", 4);
		IMAGE_RULES.put("main_project_today_*", 4);
		IMAGE_RULES.put("main_submenu_*", ALL);
		IMAGE_RULES.put("main_tasks_opened_btns_*", 0);
		IMAGE_RULES.put("main_top_*", ALL);
		IMAGE_RULES.put("messenger_conversation_*", 128);
		IMAGE_RULES.put("messenger_team_*", 128);
		IMAGE_RULES.put("popup_*", 0);
		IMAGE_RULES.put("px.gif", ALL);
		IMAGE_RULES.put("settings*", 0);
		IMAGE_RULES.put("slider_back.png", 0);
		IMAGE_RULES.put("team_menu_*", 0);
		IMAGE_RULES.put("tooltip_*", 0);
		IMAGE_RULES.put("ical_icon.gif", ALL);
		IMAGE_RULES.put("feed-icon-14x14.png", ALL);
		IMAGE_RULES.put("feed_*", ALL);
		IMAGE_RULES.put("main_latest_news_*", 256);
		IMAGE_RULES.put("main_project_add.gif", 16);
		IMAGE_RULES.put("picker_*", 0);
		IMAGE_RULES.put("print_*", -1);
		IMAGE_RULES.put("tagcloud_*", -1);
		IMAGE_RULES.put("pro.gif", -1);
		IMAGE_RULES.put("business.gif", -1);
		IMAGE_RULES.put("business_adm.gif", -1);
		IMAGE_RULES.put("lock.gif", ALL);
		IMAGE_RULES.put("lock_project.gif", ALL);
		IMAGE_RULES.put("main_project_8_*", -1);
		IMAGE_RULES.put("favicon.*", -1);
		IMAGE_RULES.put("edit*", 16);
		IMAGE_RULES.put("icons_sprite.png", 1 | 4 | 8 | 16 | 32);
		IMAGE_RULES.put("sort_icons.png", 1);
		IMAGE_RULES.put("item_sprite_2.png", 1);
		IMAGE_RULES.put("item_sprite_11.png", 1);
		IMAGE_RULES.put("loading.gif", 0);
		IMAGE_RULES.put("columns_2.gif", 0);
		IMAGE_RULES.put("header-sprite.*", ALL);
		IMAGE_RULES.put("header-report-item-selected.gif", 0);
		IMAGE_RULES.put("theme_icons.png", ALL);
		
		Map<String, Integer> newRules = new HashMap<String, Integer>();
		for (String name : IMAGE_RULES.keySet()) {
			String newName = "^.*\\/" + name.replace(".", "\\.").replace("*", ".*") + "$";
			Integer rule = IMAGE_RULES.get(name);
			newRules.put(newName, rule);
		}
		
		IMAGE_RULES = newRules;
	}
	
	private List<String> firstImages = new ArrayList<String>();
	private List<String> lastImages = new ArrayList<String>();
	
	public UserImages() {
		processImages();
	}
	
	private void processImages() {
		String[] images = getImages();
		
		for (String image : images) {
			boolean done = false;
			for (String ruleName : IMAGE_RULES.keySet()) {
				if (image.matches(ruleName)) {
					int rule = IMAGE_RULES.get(ruleName);
					if (rule == -1) {
						
					} else if (rule > 0) {
						firstImages.add(image);
					} else {
						lastImages.add(image);
					}
					
					done = true;
					break;
				}
			}
			
			if (!done) {
				Logger.info("Preload: No pattern for image %s", image);
			}
		}
	}
	
	private String[] getImages() {
		// TODO: Replace with something like that: $images = ArenaUser::getImages();
		return new String[] {
			"/img/icon_meeting.gif", "/img/icon_event.gif", "/img/icon_tasks.gif", "/img/icon_birthday.gif", "/img/icon_view.gif", 
			"/img/icon_note.gif", "/img/icon_file.gif", "/img/icon_feed.gif", "/img/icon_close.gif", "/img/icon_search_2.gif", 
			"/img/icon_delete.gif", "/img/icon_project.gif", 
			"/img/column_lt.gif", "/img/main_submenu_project_triangle.gif", "/img/column_inner_rt.gif", 
			"/img/messenger_conversation_title_rt.gif", "/img/btn_project_drag.gif", "/img/messenger_conversation_title_me_rt.gif", 
			"/img/messenger_conversation_title_me_lt.gif", "/img/main_submenu_project_lt.gif", "/img/ical_icon.gif", 
			"/img/main_top_back.png", "/img/main_top_lt.gif", "/img/main_submenu_search_lb.gif", "/img/main_top_body_type_lb.gif", 
			"/img/column_rt.gif", "/img/calendar_navigation_right.gif", "/img/calendar_timetable_tasks_back.gif", 
			"/img/lock_project.gif", "/img/main_submenu_back.png", "/img/main_top_body_type_rb.gif", "/img/main_submenu_btn_rt.gif", 
			"/img/main_border_rb.gif", "/img/column_lb.gif", "/img/messenger_conversation_person_rt.gif", "/img/column_rb.gif", 
			"/img/main_submenu_search_rb.gif", "/img/main_submenu_btn_lt.gif", "/img/main_border_rc.gif", 
			"/img/messenger_conversation_person_lt.gif", "/img/collapse.gif", "/img/messenger_conversation_title_lb.gif", 
			"/img/main_top_rt.gif", "/img/main_submenu_btn_lb.gif", "/img/main_submenu_project_lb.gif", 
			"/img/main_top_body_type_lt.gif", "/img/btn_drag.gif", "/img/calendar_navigation_left.gif", 
			"/img/main_submenu_btn_rb.gif", "/img/column_inner_rb.gif", "/img/column_inner_lb.gif", 
			"/img/main_submenu_project_rb.gif", "/img/main_submenu_project_rt.gif", "/img/messenger_conversation_title_me_lb.gif", 
			"/img/calendar.gif", "/img/main_submenu_search_triangle.gif", "/img/calendar_navigation_select_bull.gif", 
			"/img/messenger_conversation_title_me_rb.gif", "/img/main_top_body_type_rt.gif", 
			"/img/main_submenu_border.png", "/img/messenger_conversation_person_rb.gif", "/img/messenger_team_border.gif", 
			"/img/main_border_lc.gif", "/img/messenger_conversation_title_lt.gif", "/img/column_inner_lt.gif", 
			"/img/main_submenu_search_rt.gif", "/img/main_border_rt.gif", "/img/feed-icon-14x14.png", 
			"/img/lock.gif", "/img/messenger_conversation_person_lb.gif", "/img/icons_sprite.png", 
			"/img/messenger_conversation_title_rb.gif", "/img/px.gif", "/img/main_submenu_search_lt.gif", 
			"/img/header_logo.png", "/img/main_menu.jpg",
			"/img/main_project_color6_rb.gif", "/img/main_project_day_lb.gif",
			"/img/main_project_color3_rb.gif", "/img/datetime_calendar_top_right.png", "/img/header-sprite.gif",
			"/img/team_menu_top_left.png","/img/main_project_color6_border.gif","/img/settings_gray2_b.gif",
			"/img/main_project_color4_lt.gif","/img/crop_handle.png","/img/main_project_color2_border.gif",
			"/img/settings_box_2_lt.gif","/img/columns_2.gif","/img/main_project_today_rt.gif","/img/main_project_color6_tasks_rt.gif",
			"/img/colors.gif","/img/main_project_color3_tasks_lt.gif","/img/main_project_color5_tasks_rb.gif",
			"/img/main_project_add.gif","/img/popup_bottom_left.png","/img/colors_top_left.png","/img/settings_buy.gif",
			"/img/main_project_color7_tasks_rb.gif","/img/main_project_color7_tasks_lb.gif","/img/main_project_color7_rt.gif",
			"/img/datetime_calendar_cont_back.png","/img/team_menu_cont_back.png","/img/datetime_calendar_rt.gif",
			"/img/slider_back.png","/img/main_tasks_opened_btns_box_lt.gif","/img/main_project_color8_rb.gif",
			"/img/main_project_color2_lt.gif","/img/main_project_color3_tasks_lb.gif","/img/tooltip_cont_right.png",
			"/img/main_project_today_border.gif","/img/tooltip_bottom_left.png","/img/main_project_color2_tasks_rt.gif",
			"/img/tooltip_cont_left.png","/img/datetime_calendar_navigation_prev.gif","/img/edit.gif","/img/settings_box_lt.gif",
			"/img/main_project_color8_border.gif","/img/settings_gray2_t.gif","/img/main_latest_news_lt.gif",
			"/img/main_project_color5_lt.gif","/img/main_project_color5_border.gif","/img/settings_free.gif",
			"/img/main_project_color8_header_2.gif","/img/tooltip_triangle_top.png","/img/team_menu_top_back.png",
			"/img/main_project_color6_rt.gif","/img/settings_gray_t.gif","/img/main_project_day_rb.gif","/img/edit_hover.gif",
			"/img/popup_top_left.png","/img/main_project_color7_rb.gif","/img/settings_menu_back_arrow.gif",
			"/img/main_project_day_lt.gif","/img/datetime_calendar_bottom_right.png","/img/popup_bottom_right.png",
			"/img/main_project_color1_tasks_lt.gif","/img/popup_bottom_back.png","/img/settings_premium_background.gif",
			"/img/datetime_time_arrows.gif","/img/main_project_color5_tasks_lt.gif","/img/main_project_color4_rb.gif",
			"/img/main_latest_news_rb.gif","/img/settings_box_2_rt.gif","/img/main_project_color4_lb.gif",
			"/img/settings_box_lb.gif","/img/main_project_color2_rb.gif","/img/main_project_color4_tasks_rb.gif",
			"/img/main_project_color3_rt.gif","/img/tooltip_triangle_bottom.png","/img/settings_gray_b.gif",
			"/img/main_project_color1_rt.gif","/img/main_project_color6_tasks_lt.gif","/img/settings_upgrade_business.gif",
			"/img/settings_add_licenses.gif","/img/main_project_color4_tasks_lb.gif","/img/main_project_color4_tasks_lt.gif",
			"/img/settings_premium_rt.gif","/img/main_project_day_rt.gif",
			"/img/main_project_color4_tasks_rt.gif","/img/main_latest_news_lb.gif","/img/colors_top_back.png",
			"/img/datetime_calendar_bottom_back.png","/img/main_project_color5_tasks_lb.gif","/img/tooltip_top_left.png",
			"/img/loading.gif","/img/tooltip_close_darkbg.png","/img/main_project_color4_rt.gif",
			"/img/main_project_color7_border.gif","/img/main_project_color8_lb.gif","/img/main_project_color7_lb.gif",
			"/img/main_project_color7_lt.gif","/img/main_project_color1_lb.gif","/img/settings_box_2_lb.gif",
			"/img/team_menu_top_right.png","/img/settings_premium_lb.gif","/img/main_project_color1_lt.gif",
			"/img/main_project_color5_lb.gif","/img/colors_cont_back.png","/img/tooltip_bottom_right.png",
			"/img/main_project_color6_lt.gif","/img/main_project_day_tasks_rb.gif","/img/popup_top_right.png",
			"/img/main_project_color2_tasks_rb.gif","/img/main_tasks_opened_btns_box_rt.gif",
			"/img/main_project_color1_rb.gif","/img/main_project_today_lt.gif","/img/main_project_color1_tasks_lb.gif",
			"/img/main_project_color5_tasks_rt.gif","/img/main_project_day_border.gif","/img/settings_box_rb.gif",
			"/img/main_project_today_tasks_rb.gif","/img/main_project_color5_rt.gif","/img/main_project_color2_rt.gif",
			"/img/colors_top_right.png","/img/main_project_color6_tasks_lb.gif","/img/main_project_color1_tasks_rt.gif",
			"/img/main_project_color2_tasks_lb.gif","/img/settings_box_rt.gif",
			"/img/main_tasks_opened_btns_box_rb.gif","/img/settings_icons.png","/img/tooltip_top_back.gif",
			"/img/main_tasks_opened_btns_box_lb.gif","/img/main_period.gif","/img/main_project_color2_tasks_lt.gif",
			"/img/tooltip_triangle_left.png","/img/datetime_calendar_top_left.png","/img/main_project_color7_tasks_lt.gif",
			"/img/main_project_color1_border.gif","/img/main_latest_news_rt.gif","/img/main_project_color3_border.gif",
			"/img/settings_premium_lt.gif","/img/settings.gif","/img/main_project_color1_tasks_rb.gif",
			"/img/main_project_today_lb.gif","/img/main_project_color6_lb.gif","/img/main_project_color5_rb.gif",
			"/img/tooltip_top_right.png","/img/datetime_calendar_lt.gif","/img/popup_cont_right.png",
			"/img/datetime_calendar_navigation_next.gif","/img/header-report-item-selected.gif","/img/main_project_day_tasks_lb.gif",
			"/img/main_project_color3_tasks_rb.gif","/img/main_project_color4_border.gif","/img/main_project_color3_lt.gif",
			"/img/main_project_color6_tasks_rb.gif","/img/settings_renew.gif","/img/settings_premium_rb.gif",
			"/img/settings_box_2_rb.gif","/img/main_project_color2_lb.gif","/img/main_project_color3_lb.gif",
			"/img/main_project_color3_tasks_rt.gif","/img/tooltip_triangle_right.png","/img/datetime_calendar_cont_right.png",
			"/img/crop_overlay.png","/img/settings_box_2.gif","/img/main_project_today_tasks_lb.gif",
			"/img/main_project_color7_tasks_rt.gif","/img/main_project_today_rb.gif","/img/datetime_calendar_bottom_left.png",
			"/img/popup_cont_back.png","/img/tooltip_bottom_back.png",
			"/img/theme_icons.png"
		};
	}
	
	public List<String> getFirstImages() {
		return firstImages;
	}
	
	public List<String> getLastImages() {
		return lastImages;
	}
	
	public String getFirstImagesAsJson() {
		return new Gson().toJson(firstImages);
	}
	
	public String getLastImagesAsJson() {
		return new Gson().toJson(lastImages);
	}
	
	public String getAjaxLoaderImg() {
		return IMAGES_ROOT + "/loading.gif";
	}
	
	public String getProImg() {
		return IMAGES_ROOT + "/pro.gif";
	}
	
	public String getBusinessImg() {
		return IMAGES_ROOT + "/business.gif";
	}
	
	public String getBusinessAdmImg() {
		return IMAGES_ROOT + "/business_adm.gif";
	}
	

	public String getLockProjectImg() {
		return IMAGES_ROOT + "/lock_project.gif";
	}
	
	public String getLockImg() {
		return IMAGES_ROOT + "/lock.gif";
	}
	
	public String getBtnDragImg() {
		return IMAGES_ROOT + "/btn_drag.gif";
	}
	
	public String getBtnProjectDragImg() {
		return IMAGES_ROOT + "/btn_project_drag.gif";
	}
}