
define([
	'dojo/_base/lang',
	'dojo/on',
	'dojo/query',
	'dojo/dom-class',
	'hi/items/base',
	'hi/store',
	'hi/widget/task/item_DOM',
	'hi/settings'
], function (lang, on, Query, domClass, hi_items, hi_store, item_DOM) {

	dojo.global.viewDate = function() {
		return false;
	};

	dojo.global.viewProjectTab = function() {
		return false;
	};

	dojo.global.viewTeamTab = function() {
		return false;
	};

	dojo.global.viewColorTab = function() {
		return false;
	};

	dojo.global.viewMyTab = function() {
		return false;
	};

	dojo.global.viewAllTab = function(q) {
		var readStore = hi_store.ProjectCache,
			writeStore = hi_store.Project,
			query = q;

		var myV = dojo.connect(hi_store,'afterLoad',function() {
				finalizeInitialization([], [hi_items.TaskList]);
			dojo.disconnect(myV);
		});				

		hi_items.TaskList = new hi.widget.TaskList({
			"readStore":     readStore,
			"writeStore":    writeStore,
			"query":         query,
			"disabled":      false,
			"localeNoItems": "",
			"mainList":      true,
			"filterType":    'all',
			"completedSubList": false,
			
			"filter": function (item, strict) {
				return true;
			}
		});

		on (hi_items.TaskList, "disabled", function(disabled){
			if (!disabled) {
				for (var itemId in this.itemWidgets) {
					if (dojo.byId("itemNode_" + this.itemWidgets[itemId].widgetId)) {
						item_DOM.expand(this.itemWidgets[itemId]);
					}
				}
			}

			setTimeout(lang.hitch(this, function() {

				var parent = hi.items.manager.get({parent:0});

				if (lang.isArray(parent) && parent.length > 0) {

					var children = hi.items.manager.get({
						parent: parent[0].id,
						category: 1
					});
					toggleChildren(parent[0].id);


					if (children.length == 0) {
						dojo.global.handleExpandClick = function() {};
						hi_items.manager.collapseOpenedItems = function() {};
						
						
						dojo.global.handleClickComplete = function(taskId,eventdate,e,withoutWidget) {
							item_DOM.handleClickComplete(taskId,eventdate,e,true);
						};
						dojo.global.reportTooltipCloseHandler = function() {};

						Query(".tasks li .heading h3").forEach(function(node){
							domClass.add(node, "defaultCursor");
						});
					}
				}
			}),500);
			
		});

		var container = dojo.byId(Project.getContainer('tasksList'));
		
		hi_items.TaskList.placeAt(container);

		hi.items.manager.getTaskLists[0] = function () {
			return [
				hi_items.TaskList
			];
		};
	};
	
	dojo.global.hi_init = function() {
		hi.store.init();
		dojo.global.SingleItem = true;
		dojo.global.Hi_Search = null;
		//dojo.global.Hi_Timer = null;
		item_DOM.openEdit = function() {};
		dojo.global.Click = function() {};
		dojo.global.handleItemDoubleClick = function() {};
		dojo.global.Hi_Search = {
			emptyFunction:function() {},
			isFormVisible:function() {}
		};
		hi.items.manager.openedId = function() {};
		FileUpload.enabled = true;
		FileUpload.listEnabled = true;
		FileList.fadeOutFile = function() {};
	};

	return {};
	
});