package api;

/**
 * Exception class that used for wrapping the API error message
 * and API response code received from API server and HTTP status code.
 * @author Vadim Manikhin
 */
public class ApiCallException extends RuntimeException {
	private String errorMessage = "";
	private int responseStatus = 0;
	private int httpStatus = 0;
	private Exception previous;
	
	public ApiCallException(String errorMessage, int responseStatus, int httpStatus) {
		this.errorMessage = errorMessage;
		this.responseStatus = responseStatus;
		this.httpStatus = httpStatus;
	}
	
	public ApiCallException(ApiGeneralResponse apiErrorResponse, int httpStatus) {
		this.errorMessage = apiErrorResponse != null ? apiErrorResponse.getErrorMessage() : null;
		this.responseStatus = apiErrorResponse != null ? (apiErrorResponse.getResponseStatus() != null ? apiErrorResponse.getResponseStatus() : -1) : -1;
		this.httpStatus = httpStatus;
	}
	
	public ApiCallException(Exception previous) {
		this.previous = previous;
	}
	
	@Override
	public String getMessage() {
		return new StringBuilder().append(errorMessage).append("[response_status: ").append(responseStatus)
			.append(", http_status: ").append(httpStatus).append("]").toString();
	}
	
	@Override
	public Throwable getCause() {
		return previous;
	}
	
	public int getResponseStatus() {
		return responseStatus;
	}
	
	public int getHttpStatus() {
		return httpStatus;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
}
