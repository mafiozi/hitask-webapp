define([
	"hi/store",
	'dojox/socket',
	'dojo/aspect',
	'hi/widget/activityFeedContainer',
	'dojo/json',
	"dojo/_base/sniff",
	'dojo/on',
	'dojo/date'
], function (hi_store, Socket, aspect, ActivityFeedContainer, dojoJson, dojoSniff, dojoOn, dojoDate) {
	dojo.global.viewActivityFeedTab = function(q) {
		if (dojo.byId('singleTask') || !('WebSocket' in dojo.global)) {
			return;
		}
		var util = supra.common,
			groupSelf = 'activity',
			groupCurrent = Hi_Preferences.get('currentTab', 'my', 'string'),
			isFirstPortionLoaded = false,
			noMoreOldEvents = false,
			reconnectTime = 10000,
			activityFeedSocket = null,
			activityFeedContainer = null;

		activityFeedContainer = new ActivityFeedContainer({
			id: "activityFeedContainer"
		});

		var container = dojo.byId(Project.getContainer('tasksList'));
		var submenu = dojo.byId('submenu');
		var searchILContainer = dojo.byId('searchItemListContainer');
		activityFeedContainer.placeAt(container);
		
		if(groupCurrent != groupSelf) {
			util.hideElement(dojo.byId('activityFeedContainer'));
		} else {
			dojo.setStyle(submenu, {display: 'none'});
			dojo.addClass(searchILContainer, 'hidden');
			dojo.addClass(mainContainer, 'activityFeedContainer');
		}
		
		if (Project.behindProxy) {
			activityFeedContainer.websocketBehindProxyError.innerHTML = htmlspecialchars(hi.i18n.getLocalization('common.behind_proxy'));
		}

		dojo.connect(hi.items.manager, "groupingChange", hi.items.manager, function (group, groupPrev) {
			// summary:
			//		On group change show/hide feed and toolbar
			var mainContainer = dojo.byId('mainContainer');
			if(!mainContainer) return;

			if (group == groupSelf && groupPrev != groupSelf) {
				util.showElement(dojo.byId('activityFeedContainer'));
				dojo.setStyle(submenu, {display: 'none'});
				dojo.addClass(searchILContainer, 'hidden');
				dojo.addClass(mainContainer, 'activityFeedContainer')
			} else if (group != groupSelf && groupPrev == groupSelf) {
				util.hideElement(dojo.byId('activityFeedContainer'));
				dojo.removeAttr(submenu, 'style');
				dojo.removeClass(searchILContainer, 'hidden');
				dojo.removeClass(mainContainer, 'activityFeedContainer')
			}
			groupCurrent = group;
		});

		var storeLoadingHandler = aspect.after(hi_store, 'afterLoad', function() {
			dojo.removeClass(activityFeedContainer.noItemsNode, "hidden");
			if(groupCurrent == groupSelf) {
				dojo.removeClass(dojo.byId('activityFeedContainer'), "hidden");
			}

			//
			if (dojo.isIE) {
				document.baseURI = (document.URL || ((document.location) ? document.location.href : document.baseURI));
			}

			activityFeedSocket = new Socket({
				url: 'wss://' + Project.api2_base_host + '/api/v3/activityfeed?limit=64&session_id=' + Project.api2_session_id
			});

			var onMessage = function(event) {
				var message = event.data;
				var items_data = "pong";
				try {
					items_data = JSON.parse(message);
				} catch (e) {
					console.error('activity feed received broken json: ' + items_data);
					console.warn(e);
				}
				if (items_data == "pong") {
					// #5038: Do nothing - just ping-pong.
					return;
				}
				var len = items_data.length;
				
				if (len === 0) {
					noMoreOldEvents = true;
				}
				
				if (len > 0 && !items_data[0].recent) {
					items_data.reverse(function(a, b) {
						return dojoDate.compare(new Date(a.eventTime), new Date(b.eventTime));
					});
				}
				
				for (var i = len - 1; i > -1; i--) {
					activityFeedContainer.addPost(items_data[i], !isFirstPortionLoaded);
				}
				isFirstPortionLoaded = true;
				if (isFirstPortionLoaded === true) {
					var loadingBar = dojo.byId('feedLoadingBar');
					dojo.addClass(loadingBar, "hidden");
				}
			};

			//Adding reconnector to socket
			dojo.global.webSocketReconnect(activityFeedSocket, reconnectTime, function(newSocket) {
				activityFeedSocket = newSocket;
			}, onMessage);

			activityFeedSocket.on("message", onMessage);

			storeLoadingHandler.remove();
			
			// Send "ping" message to server in order to keep websocket connection alive.
			setInterval(function() {
				if (activityFeedSocket && activityFeedSocket.readyState === WebSocket.OPEN) {
					activityFeedSocket.send("ping");
				}
			}, 50000);
		});
		
		function calculateScrollHeight(){
			return (document.documentElement.scrollHeight - document.documentElement.clientHeight) - 100;
		}
		var max = calculateScrollHeight();
		var scrollingDetector = (function() {
			return function(){
				if (max < window.pageYOffset) {
					max = calculateScrollHeight();
					if (noMoreOldEvents === false) {
						getOlderPosts();
					}
				}
			};
		})();
		
		activityFeedContainer.on('domCleanup', function() {
			max = calculateScrollHeight();
		});

		dojo.connect(window, 'onscroll', this, function(event) {
			scrollingDetector();
		});

		function getOlderPosts() {
			var feed = dojo.byId('activityFeedContainer');
			var loadingBar = dojo.byId('feedLoadingBar');
			if (isFirstPortionLoaded === true) {
				dojo.place(loadingBar,feed, 'last');
				dojo.removeClass(loadingBar, "hidden");
			}
			var query = {
				offset: activityFeedContainer.getPostsCount(),
				limit: 5
			};
			if (activityFeedSocket && activityFeedSocket.readyState === WebSocket.OPEN) {
				activityFeedSocket.send(JSON.stringify(query));
			}
		}
	};
});