//dojo.provide("hi.googlecal");

define(function () {

	dojo.global.apiKey = null;
	dojo.global.apiVersion = null;
	dojo.global.prefs = null;
	dojo.global.json = true;
	dojo.global.intervalID = null;
	dojo.global.default_date = null;
	dojo.global.tasksByDate = null;
	dojo.global.first_time_call = 1;
	dojo.global.taskMessageGlobal = [];
	dojo.global.taskTitleGlobal = [];
	dojo.global.authenticationsCount = 0;
	dojo.global.taskMessageGlobal[0] = [];
	dojo.global.taskTitleGlobal[0] = [];
	
	dojo.ready(function () {
		if (!apiKey) {
			apiKey = document.getElementById('apiKey').value;
		}
		if (!apiVersion) {
			apiVersion = document.getElementById('apiVersion').value;
		}
		if (!tasksByDate && document.getElementById('date').value != '') {
			tasksByDate = document.getElementById('date').value;
		}
		getTaskList();
		
		intervalID = setInterval(refreshWindow, 60000);
	});
	
	dojo.global.error = function() {
		document.getElementById('errorSpan').innerHTML = 'Service is unavailable';
		document.getElementById('errorDiv').style.display = '';
		document.getElementById('block2').style.display = 'none';
		document.getElementById('block3').style.display = 'none';
	}
	
	dojo.global.getTaskList = function() {
		
		sessionID = dojo.cookie('session_id');
		
		var params = {AS: sessionID, apiKey: apiKey, apiVersion: apiVersion, json: json, apiDate: tasksByDate, depth: 10};
		ajaxRequestGet('itemlist', params, checkGlobalResponse, error);
	}
	
	dojo.global.setTaskComplete = function(taskID) {
		var inp = document.getElementById(taskID);
		var completed;
		var spanID = 'span_' + taskID;
		var taskSpan = document.getElementById(spanID);
		if (inp.checked) {
			completed = 1;
			taskSpan.style.textDecoration = 'line-through';
		}
		else {
			completed = 0;
			taskSpan.style.textDecoration = 'none';
		}
		document.getElementById(taskID).className='checked';
		value = taskID.split('-');
		id = value.pop();
		sessionID = dojo.cookie('session_id');
		var params = {AS: sessionID, id: id, completed: completed, apiKey: apiKey, apiVersion: apiVersion, json: json};
		ajaxRequestGet('itemcomplete', params, taskCompleted, error);
	}
	
	dojo.global.taskCompleted = function() {
		return true;
	}
	
	dojo.global.refreshWindow = function() {
		getTaskList();
	}
	
	dojo.global.templateFill = function(data) {
		var paddingArray = [];
		var padding;
		if (first_time_call == 1) {
			document.getElementById('block_loading').style.display = 'none';
			var taskListDiv = document.getElementById('block2');
			taskListDiv.style.display = '';
			first_time_call = 0;
		}
		delete data.response_status;
		var found = false;
		var taskListTable = document.getElementById('taskList');
		while (taskListTable.childNodes.length >= 1) {
			taskListTable.removeChild(taskListTable.firstChild);
		}
		var apiDate = document.getElementById('date').value;
		var split = apiDate.split('-');
		var myDate = new Date(split[0], split[1], split[2]);
		var tbody = document.createElement('tbody');
		var parentTR;
		for (i in data) {
			found = true;
			if (data[i].completed == 1) {
				continue;
			}
			
			if (data[i].parent) {
				var childs = tbody.childNodes;
				
				for (k in childs) {
					if (childs[k].id == 'task_tr_' + data[i].parent) {
						parentTR = childs[k];
						if (!paddingArray[data[i].parent]) {
							padding = 10;
						}
						else {
							padding = paddingArray[data[i].parent];
						}
						
						paddingArray[data[i].id] = padding + 10;
						taskTR = createTaskRow(data[i], parentTR, padding);
						
						insertafter(taskTR, parentTR);
						break;
					}
				}
				continue;
			}
			
			taskTR = createTaskRow(data[i]);
			tbody.appendChild(taskTR);
			
		}
		taskListTable.appendChild(tbody);
		if (found == false) {
			var taskEmptyTR = document.createElement('tr');
			var taskEmptyTD = document.createElement('td');
			taskEmptyTD.innerHTML = 'You have no tasks for selected date';
			taskEmptyTR.appendChild(taskEmptyTD);
			taskListTable.appendChild(taskEmptyTR);
		}
		
		return;
		
	}
	
	dojo.global.createTaskRow  = function(task, parentTR, padding) {
		var taskTR;
		
		taskTR = document.createElement('tr');
		taskTR.id = 'task_tr_' + task.id;
		var taskTD = document.createElement('td');
		taskTD.id = 'task_td_' + task.id;
		var checkboxTD = document.createElement('td');
		var taskTimeTD = document.createElement('td');
		checkboxTD.style.width = '5%';
		if (parentTR) {
			taskTD.style.paddingLeft = padding + 'px';
		}
		if (task.end_time) {
			taskTimeTD.innerHTML = '<h2>'+task.end_time+'</h2>';
		}
		var taskTitle = task.title;
		var taskMessage;
		
		if (task.message != undefined) {
			taskMessage	= task.message;	
		}
		else {
			taskMessage = '';
		}
		 
		
		
		if (taskMessage != '') {
			taskMessageGlobal[0][task.id] = taskMessage;
		}
		taskTitleGlobal[0][task.id] = taskTitle;
		taskTD.innerHTML = '<input type="checkbox" id="'+ task.id +'" title="Click to complete task" onClick="setTaskComplete(\'' + task.id + '\')"><span id="span_'+ task.id +'" style="cursor:pointer" onClick="showTaskData(\'' + task.id + '\')">' + task.title + '</span>';
		
		
		taskTR.appendChild(checkboxTD);
		taskTR.appendChild(taskTD);
		taskTR.appendChild(taskTimeTD);
		return taskTR; 
	}
	
	
	dojo.global.hideLoginBlock = function(data) {
		templateFill(data);
	}
	
	dojo.global.showLoginBlock = function() {
		document.getElementById('loginBlock').style.display = 'block';
	}
	
	dojo.global.checkLogin = function(data) {
		var apiDate = document.getElementById('date').value; 
		if (authenticationsCount > 3) 
				location.href = '/googlecalendar/login?apiKey=' + apiKey + '&apiVersion=' + apiVersion + '&apiDate=' + apiDate;;
		if (data.error_response) {
			if (data.error_response == 1) {
				location.href = '/googlecalendar/login?apiKey=' + apiKey + '&apiVersion=' + apiVersion + '&apiDate=' + apiDate;;
			}
			else {
				error();
			}
		}
		else {
			getTaskList();
		}
	}
	
	dojo.global.loginRequest = function(apiType, formID) {
			authenticationsCount = authenticationsCount + 1;
			try {
				var params = {apiType: apiType, apiKey: apiKey, apiVersion: apiVersion, json: json};
				ajaxRequestGet('authenticate', params, checkLogin, error);
			}
			catch (ex) {
				console.log(ex);
			}
	}
	dojo.global.showTaskForm = function() {
		document.getElementById('task_form').style.display = 'block';
	}
	dojo.global.getTodayDate = function() {
		var date;
		var myDate = new Date();
		var date1 = strPadLeft(myDate.getDate(), 2, '0');
		var date2 = strPadLeft(myDate.getMonth() + 1, 2, '0');
		var date3 = myDate.getFullYear();
		date = date3 + '-' + date2+ '-' + date1;
		return date;
	}
	
	dojo.global.insertafter = function(newChild, refChild) { 
		refChild.parentNode.insertBefore(newChild,refChild.nextSibling); 
	} 
	
	dojo.global.checkGlobalResponse = function(data) {
		var apiDate = document.getElementById('date').value; 
		
		switch(data.response_status) {
			case 1: {
				hideLoginBlock(data);
			}
			break;
			case 2 : {
				if (dojo.cookie('widget_remember_me')) {
					loginRequest('googlecalendar', 'form1');
				}
				else {
					location.href = '/googlecalendar/login?apiKey=' + apiKey + '&apiVersion=' + apiVersion + '&apiDate=' + apiDate;
				}
			}
			break;
			case 3 : {
				showLoginBlock();
			}
			break;
			case 4 : {
				document.getElementById('errorDiv').innerHTML = 'Service is unavailable';
				document.getElementById('errorDiv').style.display = '';						
			}
			break;
		}
	}
	
	dojo.global.showTaskData  = function(taskID) {
		var taskDiv = document.getElementById('block3');
		var taskTitleEl = document.getElementById('taskTitle');
		var taskDescription = document.getElementById('taskMessage');
		var taskList = document.getElementById('block2');
		var message = taskMessageGlobal[0][taskID];
		
		taskTitleEl.innerHTML = taskTitleGlobal[0][taskID];
		document.getElementById('errorDiv').style.display = 'none';
		if (message != undefined && message != '') {
			taskDescription.innerHTML  = message.replace(/\n/g, "<br />");	
		}
		else {
			taskDescription.innerHTML = '';
		}
		taskList.style.display = 'none';
		taskDiv.style.display = '';
		
	}
	
	dojo.global.showTaskList = function() {
		var taskDiv = document.getElementById('block3');
		var taskList = document.getElementById('block2');
		taskDiv.style.display = 'none';
		taskList.style.display = '';
	}
	
	dojo.global.showCreateTaskForm  = function() {
		
		dojo.byId('task_title').value = '';
		dojo.byId('newItem_start_date_1').value = default_date;
		//dojo.byId('newItem_end_date_1').value = default_date;
		dojo.byId('newItem_start_time_1').value = '';
		dojo.byId('newItem_end_time_1').value = '';
		dojo.byId('recurring').selectedIndex = '0';
		dojo.byId('message').value = '';
		document.getElementById('b2_content').style.display = 'none';
		document.getElementById('block2').style.display = 'none';
		document.getElementById('create_task').style.display = '';
		
	}
	dojo.global.hideCreateTaskForm  = function() {
		document.getElementById('create_task').style.display = 'none';
		document.getElementById('b2_content').style.display = '';
		document.getElementById('block2').style.display = '';
	}
	
	dojo.global.createNewTask  = function() {
		var sessionID = dojo.cookie('session_id');
		var recurringEl = dojo.byId('recurring');
		if (dojo.byId('task_title').value == '') {
			alert('Please enter task title');
			return;
		}
		var selectedRecurringIndex = recurringEl.selectedIndex;
		var c = 1;
		var start_time = getValue('newItem_start_time_' + c);
		var end_time = getValue('newItem_end_time_' + c);
		
		var start_date = '';
		var end_date = '';
		
		if (el = dojo.byId('newItem_start_date_' + c)) {
			start_date = el.value;
			
			start_date = time2str(str2time(start_date, Hi_Calendar.format), 'y-m-d');
			if (!start_date || start_date == '') {
				start_date = '';
			}
		}
		if (el = dojo.byId('newItem_end_date_' + c)) {
			end_date = el.value;
			end_date = time2str(str2time(end_date, Hi_Calendar.format), 'y-m-d');
			if (!end_date || end_date == '') {
				end_date = '';
			}
		}
		
		tmp = validateTime(start_time);
		if (!tmp) {
			start_time = '';
		} else {
			start_time = strPadLeft(tmp[0], 2, '0') + ':' + strPadLeft(tmp[1], 2, '0');
		}
		tmp = validateTime(end_time);
		if (!tmp) {
			end_time = '';
		} else {
			end_time = strPadLeft(tmp[0], 2, '0') + ':' + strPadLeft(tmp[1], 2, '0');
		}
		if (!start_date && start_time) {
			start_time = '';
		}
		if (!end_date && end_time) {
			end_time = '';
		}
		if (end_date && end_time && start_date && !start_time) {
			start_time = end_time;
		}
		if (end_date && end_date + end_time < start_date + start_time) {
			end_date = start_date;
			if (start_time) {
				end_time = start_time;
			}
		}
		
		
		
		
		var params = {AS: sessionID, apiKey: apiKey, apiVersion: apiVersion, json: json, apiDate: tasksByDate,
					  title: dojo.byId('task_title').value,
					  start_date:start_date,
					  end_date:end_date,
					  start_time:start_time,
					  end_time:end_time,
					  recurring:recurringEl.options[selectedRecurringIndex].value,
					  message:dojo.byId('message').value,
					  category:1};
				  
		ajaxRequestGet('itemnew', params, function  () {
			getTaskList();
			hideCreateTaskForm();
		}, error);
	}
	
	dojo.global.nullAuthenticationsCount  = function() {
		authenticationsCount = 0;
	}

});