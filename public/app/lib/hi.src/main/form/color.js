//dojo.provide("hi.main.form.color");
define(function () {
	
	dojo.global.colorPickerClick = function(el, col, is_new) {
		var els = el.parentNode.getElementsByTagName('DIV');
		
		for(var i=0,j=els.length; i<j; i++) {
			dojo.removeClass(els[i], 'selected');
		}
		
		dojo.addClass(el, 'selected');
		
		if (!is_new) {
			chooseColor(col ,Project.opened);
		} else {
			newItemChooseColor(col);
		}
	}
	
	dojo.global.chooseColor = function(n, op, li) {
		op = op || false;
		if (!op) {
			var op = Project.opened;
			if (!op) {
				return;
			}
		}
		
		var li = dojo.byId('task_' + op);
		var check = dojo.query('.check', li)[0];
		
		if (!dojo.hasClass(dojo.byId('tooltip_taskedit'), 'hidden')) {
			var tooltip = dojo.byId('tooltip_tasks');
			var subitem = firstElement(tooltip);
			if (subitem) {
				subitem = dojo.query('.check', subitem)[0];
				if (subitem) {
					li = subitem;
				}
			}
		}
		
		if (!check) return;
		dojo.removeClass(check, 'color1');
		dojo.removeClass(check, 'color2');
		dojo.removeClass(check, 'color3');
		dojo.removeClass(check, 'color4');
		dojo.removeClass(check, 'color5');
		dojo.removeClass(check, 'color6');
		dojo.removeClass(check, 'color7');
		
		if (n) {
			dojo.addClass(check, 'color' + n);
		}
		if (dojo.byId('color_' + op)) {
			dojo.byId('color_' + op).value = n;
		}
	}
	
	dojo.global.newItemChooseColor = function(n) {
		var off = false;
		if (typeof n == 'undefined') {
			off = true;
		}
		n = n || 0;
		setValue('newItem_color', n);
		
		var cont = dojo.byId('newItem_color_container');
		var color_cont = dojo.query('.color_picker', cont)[0];
		var colors = color_cont.getElementsByTagName('DIV');
		
		for(var i=0; i<colors.length; i++) {
			if (i != n) {
				dojo.removeClass(colors[i], 'selected');
			} else {
				dojo.addClass(colors[i], 'selected');
			}
		}
	}
	
	return {
		'colorPickerClick': colorPickerClick,
		'chooseColor': chooseColor,
		'newItemChooseColor': newItemChooseColor
	};
});