//dojo.provide("hi.timetracking.base");
define(function () {
	
	return dojo.global.Hi_TimeTracking = {
		getInputs: function (element) {
			var ret = {};
			var inputs = element.getElementsByTagName('INPUT');
			
			for(var i=0,ii=inputs.length; i<ii; i++) {
				ret[inputs[i].name] = inputs[i];
			}
			
			return ret;
		}
	};

});