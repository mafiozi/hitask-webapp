//dojo.provide("hi.project.calendar");

define([
	"hi/project/base"
], function () {
	
	var Project = dojo.global.Project;
	
	dojo.mixin(Project, {
			
		/**
		 * When calendar is moved to the next day at midnight refresh task list
		 */
		rolloverNextDay: function() {
			hi.items.manager.redraw();

			if(Project.account_type === 'TEAM_BASIC'){
				var infoDiv = dojo.query('#section-trial-expire-notification')[0];
				//upgradeDiv = dojo.query('#header .section-upgrade')[0];

				if(infoDiv){
					if(Project.expireIn > 1){
						Project.expireIn--;
						infoDiv.innerHTML = hi.i18n.getLocalization('trial.warning').replace('%d', Project.expireIn);
					}else if(Project.expireIn === 1){
						Project.expireIn--;
						// dojo.global.hideElement(infoDiv);
						infoDiv.innerHTML = hi.i18n.getLocalization('trial.warning1');
					}
				}
			}
		},
		
		/**
		 * Returns next event occurrence
		 * @param {Object} id
		 */
		getNextEvent: function (id) {
			var item = hi.items.manager.get(id);
			if (!item) return;
			
			var recurring = parseInt(item.recurring || 0);
			if (!recurring) return false;
			
			var start_date = str2timeAPI(hi.items.manager.get(id, 'start_date'));
			start_date.setHours(23);
			start_date.setMinutes(59);
			start_date.setSeconds(59);
			start_date.setMilliseconds(999);
			
			var d = HiTaskCalendar.nextEvent(start_date, recurring);
			
			if (d) {
				return d[1];
			} else {
				return false;
			}
		},
		
		getNextEventDate: function (id, date) {
			var start_date = hi.items.date.getEventStartTime(id, '00:00'),
				recurring  = parseInt(hi.items.manager.get(id, 'recurring') || 0);

			date = date || hi.items.date.truncateTime(new Date());
			start_date = hi.items.date.truncateTime(start_date);
			
			var instance_date = HiTaskCalendar.nextEvent(start_date, recurring, date);
			instance_date = (instance_date && instance_date[1] ? time2str(instance_date[1], 'y-m-d') : null);

			return instance_date;
		}
		
		
	});
	
	return Project;

});