define([
	"dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/_base/event",
	"dojo/_base/array",
	'dojo/sniff',
	'dojo/aspect',
	'dojo/on',
	'dojo/dom-construct',
	'dojo/dom-class',
	'dojo/keys',
	'dojo/string',
	"dojo/dom-geometry",

	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dijit/_WidgetsInTemplateMixin',
	'dijit/form/ComboBox',
	'dijit/form/_TextBoxMixin',
	'dijit/_HasDropDown' ,

	'hi/widget/form/hiSharing/hiSharingDropDown',
	'hi/shortcuts'
], function(declare, lang, event, array, sniff, aspect, on, domConstruct, domClass, keys, string, domGeo,
	_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, ComboBox, _TextBoxMixin,
	_HasDropDown) {

	var hiTagComboBox = declare("hi.widget.Tag.ComboBox", [ComboBox], {
		//Fire the change event for every text change
		intermediateChanges: true,

		baseClass: 'hiTagComboBox dijitTextBox dijitComboBox dijitValidationTextBox',

		_onDropDownMouseDown: function(/*Event*/ e) {
			var val = this.get('value');

			if (val) {
				this.set('value', '');
			} else {
				this.inherited(arguments);
			}
		},

		_setValueAttr: function(val) {
			this.inherited(arguments);

			if (val) {
				domClass.add(this._buttonNode, 'hiTagSet');
				domClass.add(this._buttonNode, 'icon-close');
			} else {
				domClass.remove(this._buttonNode, 'hiTagSet');
				domClass.remove(this._buttonNode, 'icon-close');
			}
		},

		_setLabelValue: function(val) {
			var ov = this.get('value');

			if (ov !== val) {
				this._skipChange = true;
				this.set('value', val);
			}
		}
	});

	return hiTagComboBox;
});