/**
 * The task list that contains a tooltip item.  It is a slightly modified (extended) version of hi/widget/task/list
 */
define([
	"dojo/_base/array", // array.forEach array.map
	"dojo/_base/declare", // declare
	"dojo/cldr/supplemental", // cldrSupplemental.getFirstDayOfWeek
	"dojo/date", // date
	"dojo/date/locale",
	"dojo/dom", // dom.setSelectable
	"dojo/dom-class", // domClass.contains
	"dojo/_base/event", // event.stop
	"dojo/_base/lang", // lang.getObject, lang.hitch
	"dojo/_base/sniff", // has("ie") has("webkit")
	"dojo/string", // string.substitute
	"dojo/_base/window", // win.doc.createTextNode
	"dojo/aspect",
	"dijit/_Widget",
	"dijit/_Templated",
	"dijit/_WidgetsInTemplateMixin",
	"dijit/form/_ComboBoxMenu",
	"dojo/text!./assignTo.html"
	// "hi/widget/task/item",
	// "hi/widget/task/list",
	// 'hi/widget/task/item_DOM'
], function(array, declare, cldrSupplemental, date, local, dom, domClass, event, lang, has, string, win, aspect,
			_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _ComboBoxMenu, template){
	// module:
	//		hi/widget/List
	// summary:
	//		Task list which updates itself on store QueryResults data change
	
	
	var VALIDATION_SUCCESS = function () { return true; };
	
	
	var TaskAssignTo = declare("hi.widget.TaskAssignTo", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
		
		templateString: template,

		widgetsInTemplate: true,

		taskId: undefined,

		value: '',
		_setValueAttr: function(value) {
			var a = this.assigneeComboBox.store.get(value);
			if (a) {
				this.assigneeComboBox.set('value', a.name);
			}
		},
		
		buildRendering: function () {
			var __onClick = _ComboBoxMenu.prototype._onClick,
				t = this;

			this.inherited(arguments);
			this.assigneeStore = this.assigneeComboBox.get('store');
			this._fillAssigneeList();

			lang.mixin(_ComboBoxMenu.prototype, {
				_onClick: function(/*Event*/ evt, /*DomNode*/ target){
					event.stop(evt);
					__onClick.apply(this, arguments);
				}
			});
			this.assigneeComboBox.set('labelFunc',dojo.hitch(this, this.renderLabel));
			this.assigneeComboBox.dropDownClass = _ComboBoxMenu;
			this.own(
				aspect.after(dojo.global, 'handleItemClick', function(){
					var editing = hi.items.manager.editing();
					if(!editing){
						if(t.assigneeComboBox._opened === true){
							t.assigneeComboBox.closeDropDown();
						}
					}
				})
			);

		},

		_fillAssigneeList: function () {
			// summary:
			//		Fill assignee list
			
			if (!('friends' in Project) && this.get('assignee') != Project.user_id) return;
			var taskId = this.taskId || 0,
				newInput = this.assigneeComboBox,
				default_value = Hi_Preferences.get('default_assign_user', 0, 'int') || 0,
				shared = taskId && hi.items.manager.get(taskId, 'shared');
			
			// add None to the list
			/* #2162 newInput.store.put({name:'None',id:this.assignee});*/
			newInput.store.put({name:'None',id:0});
			
			var tmp;
			// add myself to list
			newInput.store.put({
				name: Project.getMyself(),
				id: Project.user_id,
				image24src: dojo.global.getAvatar(24)
			});
			
			// add friends
			if ('friends' in Project) {
				var friends = Project.friends,
					imax = friends.length, i = 0;

				for (i = 0; i < imax; i++) {
					id = friends[i].id;
					if (taskId && Project.canAssign(taskId, id, shared)){
						if (friends[i].activeStatus || !friends[i].waitStatus || friends[i].businessStatusInactive === true) {
							tmp = dojo.clone(Project.getFriend(id));
							//Must have a name attribute in order for store to return something the combobox can render
							if (!tmp['name'] || tmp['name'].length <= 0) {
								tmp['name'] = tmp['firstName'] + ' ' + tmp['lastName'];
							}
							newInput.store.put(tmp);
						}
					}else if(!taskId){
						tmp = dojo.clone(Project.getFriend(id));
						//Must have a name attribute in order for store to return something the combobox can render
						if (!tmp['name'] || tmp['name'].length <= 0) {
							tmp['name'] = tmp['firstName'] + ' ' + tmp['lastName'];
						}
						newInput.store.put(tmp);
					}
				}
			}
		},

		renderLabel:function(item,store) 
		{
			if (!item.id) {
				return '<div class="comboboxNoneSelection">' + item.name + '</div>';
			} else {
				return '<div class="' + (item.name == this.assigneeComboBox.getValue() ? 'selected' : '') + '">' 
					+ (item.image24src ? ('<img class="comboboxAvatar" src="' + item.image24src + '" width="24" height="24" />') : '') 
					+ item.name
					+ '</div>';
			}
		},
	});
	
	return TaskAssignTo;

});