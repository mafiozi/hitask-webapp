package beans;


public class UserExternalAccount {
	private String type;
	private String externalUserId;
	private String accessToken;
	private String refreshToken;
	private String externalUserHumanReadableId;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getExternalUserId() {
		return externalUserId;
	}

	public void setExternalUserId(String externalUserId) {
		this.externalUserId = externalUserId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getExternalUserHumanReadableId() {
		return externalUserHumanReadableId;
	}

	public void setExternalUserHumanReadableId(String externalUserHumanReadableId) {
		this.externalUserHumanReadableId = externalUserHumanReadableId;
	}
}
