package controllers;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;

import api.ApiCall;
import api.ApiCallException;
import beans.ApiConfig;
import beans.Build;
import beans.HelpData;
import beans.HelpTopic;
import beans.JsConstants;
import beans.LocaleWrapper;
import beans.Report;
import beans.TimeZoneWrapper;
import beans.UserImages;
import beans.UserPreferences;
import beans.UserSession;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.data.validation.Required;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Http;
import play.mvc.Http.Header;
import play.mvc.Http.StatusCode;
import play.templates.Template;
import play.templates.TemplateLoader;
import play.vfs.VirtualFile;
import utils.Constants;

public class Application extends ApplicationSecured {
	private static final String DEFAULT_LOCALE = Play.configuration.getProperty("messagefiles.defaultLocale", "en");
	private static final Map<Long, String> COLORS = new HashMap<Long, String>();
	private static final String USER_AGENT_HEADER = "user-agent";
	private static final String HELP_MENU_MESSAGES_WILDCARD = "help.helpmenu.*";
	private static final String FAQ_MENU_MESSAGES_WILDCARD = "help.faqmenu.*";
	private static final String[] TEMPLATE_NAMES = { "file", "task_archive", "project_archive" };
	private static final String TERMINATED_COOKIE_NAME = "hiterminated";
	private static final String REPORT_GUID_FLASH_KEY = "report_guid";
	private static final String REPORT_FORMAT_FLASH_KEY = "report_format";
	private static final String[] PROXY_HEADERS = {
		"http-via", "http-x-forwarded-for", "http-forwarded-for", "http-x-forwarded", "http-forwarded", "http-client-ip", 
		"http-forwarded-for-ip", "via", /*"x-forwarded-for", Nginx adds this header*/"forwarded-for", "x-forwarded", "forwarded", "client-ip", 
		"forwarded-for-ip", "http-proxy-connection", "x-bluecoat-via"
	};

	static {
		COLORS.put(1L, "#F17F7F");
		COLORS.put(2L, "#EFBD6E");
		COLORS.put(3L, "#EAD76D");
		COLORS.put(4L, "#B7D772");
		COLORS.put(5L, "#91C8DE");
		COLORS.put(6L, "#E497C2");
		COLORS.put(7L, "#B09280");
	}

	@Before(priority = 1)
	static void putRequiredArgs() {
		// #3570.
		response.removeCookie(TERMINATED_COOKIE_NAME);
		
		UserPreferences userPref = getUserPreferences();
		UserSession userBean = getUserSession();
		userBean.setIeUser(isIe());
		userBean.setTimezone(userPref.getTimeZone());

		renderArgs.put("intercomData", generateIntercomData(userBean));

		JsConstants constants = new JsConstants();
		UserImages userImages = new UserImages();

		session.put(Constants.SESSION_USER_ID_KEY, userBean.getId());
		session.put(Constants.SESSION_USER_LEVEL_KEY, userBean.getLevel());

		renderArgs.put("developmentMode", Play.mode.isDev());
		renderArgs.put("developmentModeScripts", Play.configuration.get("development.mode.scripts"));
		renderArgs.put("build", new Build());
		renderArgs.put("userPref", userPref);
		renderArgs.put("userPrefJson", userPref.toJson());
		renderArgs.put("jsonColors", toJson(COLORS));
		renderArgs.put("userBean", userBean);
		renderArgs.put("apiConfig", new ApiConfig());
		renderArgs.put("constants", constants);
		renderArgs.put("constantsJson", toJson(constants));
		renderArgs.put("preloadBarCount", 10);
		renderArgs.put("userImages", userImages);
		renderArgs.put("templatesJson", getTemplatesAsJson(userImages));
		//renderArgs.put("latestNews", new LatestNews());
		renderArgs.put("latestNews", null);
		renderArgs.put("randomTip", getRandomTip());
		renderArgs.put("behindProxy", isBehindProxy());
		
		// #2619 Set Play language using language from user preferences.
		if (userPref != null) {
			userPref.forceSetPlayLanguage();
		}
		
		Login.communitySsoProcessRedirectIfRequired(userBean);
		redirectExpiredUser(userBean);
	}
	
	private static boolean isBehindProxy() {
		for (String h : PROXY_HEADERS) {
			if (request.headers.keySet().contains(h)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Redirects to Renew page if user account is expired.
	 * 
	 * @param userSession
	 */
	private static void redirectExpiredUser(UserSession userSession) {
		if (request.actionMethod != "settings") {
			final String redirectedToRenew = "redirectedToRenew" + userSession.getId();
			final String redirectedToRenewExpire = "1min";
	
			// Allow user to enter the App in 1 minute after redirect.
			if (userSession.isExpired() && Cache.get(redirectedToRenew) == null) {
				if (userSession.isPremium()) {
					Cache.set(redirectedToRenew, userSession.getId(), redirectedToRenewExpire);
					redirect("/premium/renew?ga_affiliation=expired_premium_upgrade");
				} else if (userSession.isBusinessOwner() && userSession.isTeamBusiness()) {
					Cache.set(redirectedToRenew, userSession.getId(), redirectedToRenewExpire);
					redirect("/business/renew?ga_affiliation=expired_team_upgrade");
				} else if (userSession.isBusinessOwner() && userSession.isTeamBasic()) {
					Cache.set(redirectedToRenew, userSession.getId(), redirectedToRenewExpire);
					redirect("/teambasic/upgrade?ga_affiliation=expired_trial_upgrade");
				}
			}
		}
	}

	/**
	 * Data for intercom.io integration
	 * 
	 * @param userBean
	 * @return
	 */
	private static HashMap generateIntercomData(UserSession userBean) {
		Mac hmacSHA256 = null;
		
		try {
			hmacSHA256 = Mac.getInstance("HmacSHA256");
			SecretKeySpec secretKey = new SecretKeySpec(String.valueOf(Play.configuration.get("intercom.secret")).getBytes(), "HmacSHA256");
			hmacSHA256.init(secretKey);
		} catch (NoSuchAlgorithmException e1) {
			Logger.error(e1.getMessage());
		} catch (InvalidKeyException e) {
			Logger.error(e.getMessage());
		}

        byte[] output = hmacSHA256.doFinal(userBean.getUserId().toString().getBytes());
        String userHash = Hex.encodeHexString(output);
		
		HashMap<String, String> hm = new HashMap<String, String>();

		hm.put("user_id", String.valueOf(userBean.getUserId()));
		hm.put("user_hash", userHash);
		hm.put("email", userBean.getAnyEmail());
		hm.put("name", userBean.getFullName());
		hm.put("created_at", userBean.getAccountCreateTimeDate() != null 
			? String.valueOf(userBean.getAccountCreateTimeDate().getTime() / 1000) 
			: null
		);
		hm.put("created_at_iso", userBean.getAccountCreateTime() != null 
			? userBean.getAccountCreateTime()
			: null
		);
		hm.put("items_count", String.valueOf(userBean.getNumberOfItems()));
		hm.put("projects_count", String.valueOf(userBean.getNumberOfProjects()));
		hm.put("archived_count", String.valueOf(userBean.getNumberOfArchived()));
		hm.put("contacts_count", String.valueOf(userBean.getNumberOfContacts()));
		
		if (userBean.isPremium()) {
			hm.put("plan", "personal");
		} else {
			hm.put("plan", "free");
		}
		if (userBean.isTeamBasic()) {
			hm.put("plan", "trial");
		}
		
		if (userBean.isBusinessOwner()){
			hm.put("business_owner", "true");
		} else{
			hm.put("business_owner", "false");
		}
		
		if (userBean.getBusiness() != null) {
			if (userBean.isTeamBasic()) {
				hm.put("plan", "trial");
			} else {
				hm.put("plan", "team");
			}
			hm.put("company_id", String.valueOf(userBean.getBusiness().getId()));
			hm.put("company_name", userBean.getBusiness().getTitle());

			if (userBean.getBusiness().getDomainName() != null) {
				hm.put("domain_name", userBean.getBusiness().getDomainName());
			}
			
			if (userBean.getLastTransaction()!=null) {
				if (userBean.getLastTransaction().getAmount()!=null){
					hm.put("company_last_transaction_spent", String.valueOf(userBean.getLastTransaction().getAmount().intValue()));
				}
				if (userBean.getLastTransaction().getProduct()!=null){
					hm.put("company_last_transaction", String.valueOf(userBean.getLastTransaction().getProduct()));
				}
			}
			
			hm.put("company_license_count", String.valueOf(userBean.getBusiness().getLicenseCount()));
			hm.put("company_license_usage", String.valueOf(userBean.getBusiness().getLicenseUsage()));
			hm.put("company_licenses_remaining", String.valueOf(userBean.getBusiness().getLicensesRemaining()));
			hm.put("company_items_count", String.valueOf(userBean.getBusiness().getNumberOfItems()));
			hm.put("company_projects_count", String.valueOf(userBean.getBusiness().getNumberOfProjects()));
			hm.put("company_archived_count", String.valueOf(userBean.getBusiness().getNumberOfArchived()));
			hm.put("company_ltv", String.format("%.2f", userBean.getBusiness().getPaymentAmountLifetime()));
			hm.put("company_domain", userBean.getBusiness().getDomainName());
			
			hm.put("company_expires_at", userBean.getBusiness().getExpirationDateDate() != null 
				? String.valueOf(userBean.getBusiness().getExpirationDateDate().getTime() / 1000)
				: null
			);
			hm.put("company_expires_at_iso", userBean.getBusiness().getExpirationDate() != null 
				? userBean.getBusiness().getExpirationDate()
				: null
			);
			hm.put("company_created_at", userBean.getBusiness().getAccountCreateTimeDate() != null 
				? String.valueOf(userBean.getBusiness().getAccountCreateTimeDate().getTime() / 1000)
				: null
			);
			hm.put("company_created_at_iso", userBean.getBusiness().getAccountCreateTime() != null 
				? userBean.getBusiness().getAccountCreateTime()
				: null
			);
		}
		
		if (userBean.getMonthlyRate() != null) {
			hm.put("monthly_rate", String.format("%.2f", userBean.getMonthlyRate()));
		}
		
		return hm;
	}
	
	/**
	 * #5703
	 * @return returns time format based on locale from accept-languages header.
	 */
	private static int detectTimeFormat() {
		try {
			for (String s : request.acceptLanguage()) {
				if (s != null && !s.contains("-")) {
					String time = DateFormat.getTimeInstance(DateFormat.FULL, new Locale(s)).format(new Date()).toLowerCase();
					if (time.contains("pm") || time.contains("am")) {
						return UserPreferences.TIME_FORMAT_12;
					} else {
						return UserPreferences.TIME_FORMAT_24;
					}
				}
			}
		} catch (Exception e) {
			Logger.error(e, "could not detect time format: %s", e.getMessage());
		}
		
		return UserPreferences.TIME_FORMAT_24;
	}

	public static void index() {
		HelpData helpData = getHelpData();
		render(helpData);
	}

	public static void archive() {
		render();
	}

	public static void settings() {
		List<TimeZoneWrapper> timezones = getTimezones();
		List<LocaleWrapper> locales = getLocales();
		List<String> weekdays = getWeekdays();
		render(timezones, locales, weekdays);
	}

	public static void item(/*TODO: @MinLength(30)*/ String  guid) {
		renderArgs.put("helpData", getHelpData());

		ApiCall apic = new ApiCall(getSessionId());
		try {
			renderArgs.put("item", apic.getItem(guid));
			//TODO: Get ItemHistory
			render();
		} catch (ApiCallException e) {
			response.status = StatusCode.NOT_FOUND;
			renderTemplate("Application/itemNotFound.html");
		}
	}

	public static void help(@Required String helpId) {
		String html = getHelpHtml(helpId);
		if (html != null) {
			renderHtml(html);
		} else {
			notFound();
		}
	}

	public static void popupTest() {
		HelpData helpData = getHelpData();
		render(helpData);
	}
	
	public static void report() {
		ApiCall apic = new ApiCall(getSessionId());
		try {
			Report r = apic.report(params.allSimple());
			notFoundIfNull(r);
			notFoundIfNull(r.getGuid());
			notFoundIfNull(r.getFormat());
			
			session.put(REPORT_GUID_FLASH_KEY, r.getGuid());
			session.put(REPORT_FORMAT_FLASH_KEY, r.getFormat());
			reportResult();
		} catch (ApiCallException e) {
			logDetailedError(e, "while calling report");
			error();
		}
	}
	
	public static void reportResult() {
		String guid = session.get(REPORT_GUID_FLASH_KEY);
		String format = session.get(REPORT_FORMAT_FLASH_KEY);
		notFoundIfNull(guid);
		notFoundIfNull(format);
		
		render(guid, format);
	}
	
	public static void reportStatus(String guid) {
		ApiCall apic = new ApiCall(getSessionId());
		try {
			String url = apic.reportStatus(guid);
			renderText(url);
		} catch (ApiCallException e) {
			if (e.getHttpStatus() == 102) {
				renderText("");
			}
			
			logDetailedError(e, "");
			if (e.getHttpStatus() == Http.StatusCode.NOT_FOUND) {
				notFound();
			} else {
				error();
			}
		}
	}

	/**
	 * Tries to find file with specified help topic. It searches for files in
	 * ${play.path}/help directory using user current language as subdir name.
	 * 
	 * @param helpId
	 * @return String html content of file
	 */
	private static String getHelpHtml(String helpId) {
		if (helpId != null) {
			VirtualFile html = Play.getVirtualFile(
				// #2620 and #3106
				// Locales don't have help.properties, so it should use from English (can not use Lang.get()).
				new StringBuilder().append("/help/").append(DEFAULT_LOCALE).append("/")
					.append(helpId).append(".html").toString()
			);

			if (html != null && html.exists()) {
				return html.contentAsString();
			}
		}

		return null;
	}

	private static UserSession getUserSession() {
		try {
			ApiCall apic = new ApiCall(getSessionId());
			UserSession session = apic.getUser();
			session.postInit(getSessionId(), apic.getUserBusiness());
			
			if (session.getBusiness() != null) {
				session.setLastTransaction(apic.getBusinessLastTransaction());
			} else {
				session.setLastTransaction(apic.getUserLastTransaction());
			}
			
			Application.session.put(Constants.SESSION_USER_ID_KEY, session.getUserId());
			Application.session.put(Constants.SESSION_USERNAME_KEY, session.getUserId());
			Application.session.put(Constants.SESSION_USER_LEVEL_KEY, session.getLevel());
			Application.session.put(Constants.SESSION_LOGIN_KEY, session.getLogin());
			Application.session.put(Constants.SESSION_LOGIN_ID_KEY, session.getLogin());
			Application.session.put(Constants.SESSION_ACCOUNT_TYPE_KEY, session.getAccountType());
			Application.session.put(Constants.SESSION_FULLNAME_KEY, session.getFullName());
			Application.session.put(Constants.SESSION_EXTERNAL_USER_ID_KEY, session.getUserSession() != null ? session.getUserSession().getUserExternalId() : null);

			return session;
		} catch (ApiCallException e) {
			if (e.getHttpStatus() == Http.StatusCode.UNAUTHORIZED) {
				Login.out();
			} else {
				logDetailedError(e, "while constructing User Session bean");
				renderBackendErrorTemplate(e);
			}

			return null;
		}
	}

	private static UserPreferences getUserPreferences() {
		try {
			UserPreferences up = new ApiCall(getSessionId()).getUserPreferences();
			
			try {
				if (up.getRawPreferences() != null && !up.getRawPreferences().containsKey(UserPreferences.TIME_FORMAT_PROPERTY)) {
					Map<String, String> prefs = new HashMap<String, String>();
					prefs.put(UserPreferences.TIME_FORMAT_PROPERTY, String.valueOf(detectTimeFormat()));
					new ApiCall(getSessionId()).saveUserPreferences(prefs);
					up = new ApiCall(getSessionId()).getUserPreferences();
				}
			} catch (Exception e) {
				Logger.error(e, "failed to auto detect user's time format: %s", e.getMessage());
			}
			
			return up;
		} catch (ApiCallException e) {
			if (e.getHttpStatus() == Http.StatusCode.UNAUTHORIZED) {
				Login.out();
			} else {
				logDetailedError(e, "while constructing User Preferences bean");
				renderBackendErrorTemplate(e);
			}

			return null;
		}
	}

	private static List<String> getTips() {
		List<String> tips = new ArrayList<String>();
		tips.add("Foobar Tip #1");
		tips.add("Foobar Tip #2");
		tips.add("Foobar Tip #3");

		return tips;
	}

	private static String getRandomTip() {
		List<String> tips = getTips();
		return tips.get(new Random().nextInt(tips.size()));
	}

	/**
	 * Creates HelpData object which contains Help and FAQ topic names and
	 * respective identifiers. It searches for Help topic names in
	 * help.properties localization file. It searches for helpmenu.* messages
	 * and takes found key as Help ID and respective value as Help Topic.
	 * 
	 * @return
	 */
	private static HelpData getHelpData() {
		HelpData hd = new HelpData();

		hd.setHelpTopics(getHelpMenus(HELP_MENU_MESSAGES_WILDCARD));
		hd.setFaqTopics(getHelpMenus(FAQ_MENU_MESSAGES_WILDCARD));
		hd.sortAllTopics();

		return hd;
	}

	private static LinkedList<HelpTopic> getHelpMenus(String searchWildcard) {
		LinkedList<HelpTopic> menus = new LinkedList<HelpTopic>();
		Set<String> helpMenus = new HashSet<String>();
		helpMenus.add(searchWildcard);
		// #2620 and #3106
		// Locales don't have help.properties, so it should use from English (can not use Lang.get()).
		Properties helpMenuProps = Messages.find(DEFAULT_LOCALE, helpMenus);
		
		for (Object key : helpMenuProps.keySet()) {
			String[] splittedKey = ((String) key).split("\\.");

			// Key contains index value for ordering.
			if (splittedKey.length == 4) {
				int index = 0;

				try {
					index = Integer.parseInt(splittedKey[2]);
				} catch (NumberFormatException nfe) {
					
				}

				menus.add(new HelpTopic(splittedKey[3], helpMenuProps.getProperty((String) key), index));
			} else if (splittedKey.length == 3) {
				menus.add(new HelpTopic(splittedKey[2], helpMenuProps.getProperty((String) key)));
			}
		}

		return menus;
	}

	private static String getTemplatesAsJson(UserImages userImages) {
		Map<String, String> tpls = new HashMap<String, String>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("_userImages", userImages);

		for (String tplName : TEMPLATE_NAMES) {
			Template tpl = TemplateLoader.load(VirtualFile.fromRelativePath(String.format("/app/views/tags/common/templates/%s.html", tplName)));
			tpls.put(tplName, tpl.render(params).replace("\r\n", " ").replace("\n", " "));
		}

		return toJson(tpls);
	}

	private static boolean isIe() {
		Header uaHeader = request.headers.get(USER_AGENT_HEADER);
		return uaHeader != null && uaHeader.value().toLowerCase().contains("msie") ? true : false;
	}

	private static String toJson(Object obj) {
		return new Gson().toJson(obj);
	}

	private static LinkedList<TimeZoneWrapper> getTimezones() {
		LinkedList<TimeZoneWrapper> timezones = new LinkedList<TimeZoneWrapper>();
		List<TimeZone> zones = new ArrayList<TimeZone>();
		final long d = new Date().getTime();

		for (String tzId : TimeZone.getAvailableIDs()) {
			zones.add(TimeZone.getTimeZone(tzId));
		}
		Collections.sort(zones, new Comparator<TimeZone>() {
			@Override
			public int compare(TimeZone o1, TimeZone o2) {
				long off1 = o1.getOffset(d);
				long off2 = o2.getOffset(d);
				
				if (off1 < off2) {
					return -1;
				} else if (off1 > off2) {
					return 1;
				}
				return 0;
			}
		});
		
		for (TimeZone tz : zones) {
			timezones.add(new TimeZoneWrapper(tz));
		}
		
		return timezones;
	}

	private static LinkedList<LocaleWrapper> getLocales() {
		LinkedList<LocaleWrapper> locales = new LinkedList<LocaleWrapper>();

		for (String localeName : StringUtils.split(Play.configuration.getProperty("application.langs", ""), ",")) {
			locales.add(new LocaleWrapper(localeName.contains("_") ? new Locale(localeName.split("_")[0], localeName.split("_")[1]) : new Locale(localeName)));
		}

		return locales;
	}
	
	private static List<String> getWeekdays() {
		List<String> list = new ArrayList<String>();
		
		for (int i = 0; i < 7; i++) {
			list.add(Messages.get("settings.display.weekday_" + i));
		}
		
		return list;
	}
}
