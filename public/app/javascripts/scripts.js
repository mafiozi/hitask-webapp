if($('.tabbable.tabs-left').length !== 0) {

    var activePane = '.tabbable.tabs-left .tab-pane.active';

    $(activePane).clone().width($(activePane).width()).attr('id', 'tab-clone').removeClass('active').appendTo($(activePane).parent());
    $('#tab-clone .fancybox').removeAttr('rel');

    $('.tabbable.tabs-left .nav-tabs > li > a').click(function(){

        if(!$(this).parent().hasClass('active')){

            $('#tab-clone').width($('.tabbable.tabs-left .tab-pane.active').width()).animate({
                opacity: 0.3
            }, 225, 'linear', function(){
                $(this).animate({
                    opacity: 0
                }, 500, 'linear', function(){
                    $('#tab-clone').html($('.tabbable.tabs-left .tab-pane.active').html());
                    $('#tab-clone .fancybox').removeAttr('rel');
                });
            });
        }
    });
}


$(function(){

    // Function to activate the tab
    function activateTab() {
    	try {
		    var activeTab = $('[href=' + window.location.hash.replace('/', '') + ']');
		    activeTab && activeTab.tab('show');
    	} catch (e) {}
    }

    if (window.location.hash) {
    	try {
    		$('#tab-clone').html($(window.location.hash.replace('/', '')).html());
    	} catch (e) {}
    }

    // Trigger when the page loads
    activateTab();

    // Trigger when the hash changes (forward / back)
    $(window).hashchange(function(e) {
        activateTab();
    });

    // Change hash when a tab changes
    $('a[data-toggle="tab"].hash').on('shown', function () {
        window.location.hash = '/' + $(this).attr('href').replace('#', '');
    });

});



if (typeof($.fancybox) != 'undefined') {
	$(".fancybox").fancybox();
}


$('a[rel=popover]').popover({
	placement: 'bottom',
    trigger: 'manual'
}).click(function(){
    $('a[rel=popover]').not($(this)).popover('hide');
    $(this).popover('toggle');
});

$('.slider').click(function(){
	$(this).toggleClass('off');
});

$('.form:not(".novalidate")').each(function(){
	$(this).validate({
		errorClass:'error',
		validClass:'success',
		errorElement:'div',
		rules: {
			firstname: {
				required: true
			},
			lastname: {
				required: true
			},
			username: {
				required: true,
                minlength: 5
			},
			email: {
				required: true,
				email: true
			},
			password: {
				required: true,
				minlength: 6
			},
			confirm_password: {
				required: true,
				equalTo: "#password",
				minlength: 6
			},
			terms: {
				required: true
			}
		}
	});
});


$(function() {

    $(window).scroll(function(){

        if($(window).width() < 768 || $('.navbar-fixed-top').hasClass('navbar-small')) {
            $('.navbar-fixed-top').stop().removeClass('navbar-scroll');
            return;
        }

        var scrollTop = $(window).scrollTop();
        //if(scrollTop !== 0)
        if (scrollTop>20)
            $('.navbar-fixed-top').stop().addClass('navbar-scroll');
        else
            $('.navbar-fixed-top').stop().removeClass('navbar-scroll');
    });

    $('.navbar-fixed-top').hover(
        function (e) {
            var scrollTop = $(window).scrollTop();
            if(scrollTop !== 0){
                $('.navbar-fixed-top').stop().removeClass('navbar-scroll');
            }
        },
        function (e) {
            var scrollTop = $(window).scrollTop();
            if(scrollTop !== 0){
                $('.navbar-fixed-top').stop().addClass('navbar-scroll');
            }
        }
    );
});

$(document).ready(function() {
	var appVersion = navigator.appVersion;
	if (appVersion && typeof(appVersion) == 'string') {
		if (appVersion.search('MSIE 8.') != -1) {
			$('body').addClass('msie8');
		}
	}
	$.smartbanner({title: 'HiTask', onlyTypes: ['android']});
});
