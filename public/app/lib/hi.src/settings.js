define([
	"dojox/layout/ResizeHandle",
	"dojox/layout/ContentPane",
	"dijit/layout/TabContainer",
	"dijit/ProgressBar",
	"hi/sound"
], function () {
	//Scipts
	dojo.global.CONSTANTS = dojo.mixin(dojo.global.CONSTANTS || {}, {
		duration: 200
	});
	
	dojo.global.setupAva = function(initialized) {
		try {
			if (document.getElementById("avatar")) {
				var start_w = null;
				var handle = document.getElementById("ava_handle");
				var avatar = document.getElementById("avatar");
					avatar.style.display = '';
				var drager = document.getElementById("ava_drager");
				var slider = document.getElementById("ava_slider");
				if (!initialized)
				{
					Drag.init(handle, null, 0, 134, 0, 0);
					Drag.init(drager, avatar, -100, 350, -100, 350);
					dojo.style('ava_handle','left','0');
				} else
				{
					dojo.style('ava_handle','left','0');
				}
				avatar.onDragStart = function (x, y) {
					drager.minX = 140 + x - parseInt(avatar.style.left) - parseInt(avatar.style.marginLeft) - parseInt(avatar.clientWidth);
					drager.minY = 140 + y - parseInt(avatar.style.top) - parseInt(avatar.style.marginTop) - parseInt(avatar.clientHeight);
					drager.maxX = 60 + x - parseInt(avatar.style.left) - parseInt(avatar.style.marginLeft);
					drager.maxY = 60 + y - parseInt(avatar.style.top) - parseInt(avatar.style.marginTop);
				};
				start_w = avatar.width;
				var start_h = avatar.height;
				if (!start_w) {
					setTimeout(setupAva, 1000);
					avatar.style.display = 'none';
					return;
				}

				var ratio = (start_h / start_w);
				var new_h;
				var new_w;
				if (ratio > 1) {
					new_w = 80;
					new_h = (80*start_h)/start_w;
				} else {
					new_h = 80;
					new_w = (80*start_w)/start_h;
				}

				avatar.style.top = '100px';
				avatar.style.left = '100px';
				avatar.style.width = new_w + 'px';
				avatar.style.height = new_h + 'px';
				avatar.style.margin = '-' + (new_h / 2) + 'px 0 0 -' + (new_w / 2) + 'px';
				handle.style.margin = '3px 0 0 20px';
				
				handle.onDrag = function(x, y) {
					var n_width = (new_w + (x * 2));
					var n_height = (new_h + ((x * 2) * ratio));
					avatar.style.width = n_width + 'px';
					avatar.style.height = n_height+ 'px';
					avatar.style.margin = '-' + (n_height / 2) + 'px 0 0 -' + (n_width / 2) + 'px';
					if (parseInt(avatar.style.top) + parseInt(avatar.style.marginTop) > 60) {
						avatar.style.top = (60 - parseInt(avatar.style.marginTop)) + 'px';
					}
					if (parseInt(avatar.style.left) + parseInt(avatar.style.marginLeft) > 60) {
						avatar.style.left = (60 - parseInt(avatar.style.marginLeft)) + 'px';
					}
					if (parseInt(avatar.style.top) + parseInt(avatar.clientHeight) + parseInt(avatar.style.marginTop) < 140) {
						avatar.style.top = (140 - parseInt(avatar.clientHeight) - parseInt(avatar.style.marginTop)) + 'px';
					}
					if (parseInt(avatar.style.left) + parseInt(avatar.clientWidth) + parseInt(avatar.style.marginLeft) < 140) {
						avatar.style.left = (140 - parseInt(avatar.clientWidth) - parseInt(avatar.style.marginLeft)) + 'px';
					}
				};
			}
		} catch(e) {console.log('setupAva error',e);}
	};
	dojo.global.crop_calc = function() {
		var a = dojo.byId('avatar');
		setValue('ava_width', a.clientWidth);
		setValue('ava_x', 60 - parseInt(a.style.left) - parseInt(a.style.marginLeft));
		setValue('ava_y', 60 - parseInt(a.style.top) - parseInt(a.style.marginTop));
	};
	dojo.global.previewSound = function(el) {
		Sound.play(dojo.byId(el).value);
	};
	dojo.global.hideElementFade = function(el) {
		dojo.fadeOut({node: el, duration: 200}).play();
	};
	dojo.global.showElementFade = function(el) {
		dojo.fadeIn({node: el, duration: 200}).play();
	};
	dojo.global._mchange = function(type) {
		if (_mchange.type) {
			hideElementFade('mchange_' + _mchange.type);
		}
		_mchange.type = type;
		showElementFade('mchange_' + type);
	};
	dojo.global.mchange_wait = function() {
		_mchange('wait');
	};
	dojo.global.mchange_ok = function(data) {
		if (data.error) {
			_mchange('error');
		} else {
			_mchange('ok');
		}
	};
	dojo.global.mchange_error = function() {
		_mchange('error');
	};

	var Team = dojo.global.Team;
	require(["dojo/domReady!"], function () {
	//dojo.global.initSettingTabs = function() {
		try {
		
		dojo.parser.parse();
		
		//Contacts for default assignee list in preferences tab -- oops, not requested to be implemented yet
		
		var processListContacts = function(dataResponse) {
			var d = dataResponse.getData();
			Project.friends = d.length ? d : [];
			var i = 0,
				pf = Project.friends,
				c = pf.length;
				// permissoin
			for(; i < c; i++){
				pf[i].image24src = '/avatar/' + (pf.pictureHash || '') + '.24.gif';
				pf[i].image16src = '/avatar/' + (pf.pictureHash || '') + '.16.gif';
			}
			assigneeWidget._fillAssigneeList();
			if (typeof(sharingWidget) != 'undefined') {
				sharingWidget.fillSharingList();
			}
			if (typeof(defaultSharingWidget) != 'undefined') {
				defaultSharingWidget.fillSharingList();
			}

			var select = dojo.query('select[name=default_assign_user]')[0];
			if(!select){ return; }
			dojo.place(dojo.create('option',{value:0,innerHTML:'None'}),select,'last');
			dojo.place(dojo.create('option',{value:Project.user_id,innerHTML:Project.getMyself()}),select,'last');
			if(!(dataResponse instanceof hiApiResponse && dataResponse.isStatusOk())){
				//Remove default assignee select option
				return;
			} else
			{
				var name,
					value;
				dojo.forEach(dataResponse.response,function(item) {
					name = '';
					value = item.id;
					if (item.firstName && item.firstName.length>0)
					{
						name = item.firstName;
						if (item.lastName && item.lastName.length>0)
						{
							name += ' ' + item.lastName;
						}
					} else
					{
						name = item.email;
					}
					dojo.place(dojo.create('option',{value:value,innerHTML:name}),select,'last');
				});
				
			}
			
		};
		Project.ajax('listcontacts', {}, processListContacts, processListContacts);
		
		var tabContainer = dijit.byId('settingTabs');
		if (tabContainer) {
			var tabs = tabContainer.getChildren();
			dojo.style(dojo.byId(tabContainer.id),'display','block');
			tabContainer.resize();
			//Update progress bar label
			dojo.forEach(['licenseProgressBar', 'storageProgressBar'], function (id) {
				var progressBar = dijit.byId(id);
				if (progressBar) {
					progressBar.report = function () { return progressBar.title; };
					progressBar.update();
				}
			});
			
			//Open tab by #hash
			if (document.location.hash) {
				var hash = document.location.hash.replace('#', '');
				
				for(var i=0,ii=tabs.length; i<ii; i++) {
					if (tabs[i].get('domNode').getAttribute('data') == hash) {
						tabContainer.selectChild(tabs[i]);
						break;
					}
				}
			}
			
			dojo.subscribe("settingTabs-selectChild", function (child) {
				document.location.hash = '#' + child.get('domNode').getAttribute('data');
			});
		}
		} catch(e) {console.log('settings error',e);}
	});
	
	return {};
	
});