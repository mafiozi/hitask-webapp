package utils;

import java.util.Scanner;
import java.util.UUID;
import java.util.regex.MatchResult;

import javax.swing.text.html.HTML;

import net.sf.textile4j.Textile;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import play.Logger;

public class HTMLUtils {
	private static final String[] ADDITIONAL_KNOWN_TAGS = {"ins"};
	/**
	 * Used to escape unknown/invalid HTML tags.
	 * 
	 * @param input
	 * @return
	 */
	private static String escapeUnknownTags(String input) {
		Scanner scan = new Scanner(input);

		StringBuilder builder = new StringBuilder();

		while (scan.hasNext()) {
			String s = scan.findWithinHorizon("[^<]*</?[^<>]*>?", 1000000);

			if (s == null) {
				builder.append(escape(scan.next(".*")));
			} else {
				processMatch(s, builder);
			}
		}
		scan.close();

		return Jsoup.clean(builder.toString(), Whitelist.relaxed().addTags(ADDITIONAL_KNOWN_TAGS));
	}

	private static void processMatch(String s, StringBuilder builder) {
		if (!isKnown(s)) {
			String escaped = escape(s);
			builder.append(escaped);
		} else {
			builder.append(s);
		}
	}

	private static String escape(String s) {
		s = s.replaceAll("<", "&lt;");
		s = s.replaceAll(">", "&gt;");
		return s;
	}

	private static boolean isKnown(String s) {
		Scanner scan = new Scanner(s);
		if (scan.findWithinHorizon("[^<]*</?([^<> ]*)[^<>]*>?", 10000) == null) {
			scan.close();
			return false;
		}

		MatchResult mr = scan.match();

		try {
			String tag = mr.group(1).toLowerCase();
			scan.close();
			if (HTML.getTag(tag) != null || ArrayUtils.contains(ADDITIONAL_KNOWN_TAGS, tag)) {
				return true;
			}
		} catch (Exception e) {
			// Should never happen.
			Logger.warn("%s::isKnown exception: %s", HTMLUtils.class.getName(), e.getMessage());
		}

		return false;
	}

	/**
	 * Processes textile formatted String to HTML String.
	 * 
	 * @param textile
	 * @param cleanResult should the resulting HTML be cleaned?
	 * @return
	 */
	public static String textileToHtml(String textile, boolean cleanResult) {
		if (StringUtils.isNotBlank(textile)) {
			try {
				String html = new Textile().process(prepareTextile(textile));
	    		return cleanResult ? escapeUnknownTags(html) : html;
	    	} catch (Exception e) {
	    		Logger.warn(e, "Error occured while processing textile to html: %s", e.getMessage());
	    		return textile;
	    	}
		} else {
			return textile;
		}
	}

	public static String textileToHtml(String textile) {
		return textileToHtml(textile, true);
	}
	
	private static String prepareTextile(String textile) {
		textile = replaceBlockquotes(textile);
		textile = replaceSpaces(textile);
		textile = replaceHashes(textile);
		return textile;
	}
	
	private static String replaceSpaces(String textile) {
		try {
			if (StringUtils.isNotBlank(textile)) {
				return textile.replaceAll("(?m)((?:^ {4}\\S.*$\\r?\\n)*^ {4}\\S.*$)", "<pre>\n$1\n</pre>");
			} else {
				return textile;
			}
		} catch (Exception e) {
			return textile;
		}
	}
	
	private static String replaceHashes(String textile) {
		try {
			if (StringUtils.isNotBlank(textile)) {
				return textile.replaceAll("(?m)((?:^# \\S.*$\\r?\\n)*^# \\S.*$)", "<p>\n$1\n</p>");
			} else {
				return textile;
			}
		} catch (Exception e) {
			return textile;
		}
	}
	
	private static String replaceBlockquotes(String textile) {
		try {
			if (StringUtils.isNotBlank(textile)) {
				String guid = UUID.randomUUID().toString();
				String bq = "bq. ";
				
				if (textile.substring(0, 2).equals("> ")) {
					textile = StringUtils.replaceOnce(textile, "> ", "\n> ");
				}
				
				textile = StringUtils.replace(textile, "\n> ", "\n" + guid + bq);
				String[] split = textile.split("\n" + guid);
				boolean inbq = false;
				for (int i = 0, len = split.length; i < len; i++) {
					String s = split[i];
					if (s.startsWith(bq)) {
						if (inbq) {
							split[i] = StringUtils.replaceOnce(s, bq, "");
						} else {
							inbq = true;
						}
						if (s.contains("\n")) {
							inbq = false;
						}
					} else {
						inbq = false;
					}
				}
				
				return StringUtils.join(split, "\n");
			} else {
				return textile;
			}
		} catch (Exception e) {
			return textile;
		}
	}
}
