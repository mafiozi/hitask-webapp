import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import play.Logger;
import play.i18n.Messages;


public class LocalizationMissingMessagesTest extends GroovyViewsBaseTest {

	/**
	 * This test checks if all messages that used in templates
	 * are defined in localization. Test searches for message
	 * keys in templates using regular expressions and tries
	 * to fetch respective message value.
	 */
	@Test
	public void checkThatAllUsedMessagesExist() {
		checkThatAllUsedMessagesExist(getAllViews(), LocalizationTest.MESSAGE_USAGE_PATTERN_IMPLICIT);
		checkThatAllUsedMessagesExist(getAllViews(), LocalizationTest.MESSAGE_USAGE_PATTERN_EXPLICIT);
		checkThatAllUsedMessagesExist(getAllJavascripts(), LocalizationTest.MESSAGE_USAGE_PATTERN_JS);
	}

	private void checkThatAllUsedMessagesExist(Iterator<File> fileIterator, String searchPattern) {
		Pattern pattern = Pattern.compile(searchPattern, Pattern.MULTILINE);
		// Pass this dummy parameters to Messages.get, otherwise it will fail with MissingFormatArgumentException exception 
		// if message contains format specifier.
		Object[] dummyParams = {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null};
		List<Error> errors = new ArrayList<Error>();
		
		while (fileIterator.hasNext()) {
			File f = fileIterator.next();
			String content = getFileAsString(f, false);
			Matcher matcher = pattern.matcher(content);
			
			while (matcher.find()) {
				String msgKey = matcher.group(1);
				
				if (msgKey != null) {
					String msg = Messages.get(msgKey, dummyParams);
					
					if (msg == null || msg.equals(msgKey)) {
						errors.add(new Error(msgKey, msg, f.getAbsolutePath()));
					}
				}
			}
		}
		
		if (errors.size() > 0) {
			Logger.error("%s failed", getClass().getName());
			for (Error error : errors) {
				Logger.error("%s", error);
			}
			fail("Test failed. See Play logs for more information.");
		}
	}
	
	private class Error {
		private String key;
		private String message;
		private String filename;
		
		private Error(String key, String message, String filename) {
			this.key = key;
			this.message = message;
			this.filename = filename;
		}
		
		@Override
		public String toString() {
			return String.format("The key: [%s]. Got message: [%s]. In %s", key, message, filename);
		}
	}
}
