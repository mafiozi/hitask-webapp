/**
 * dojo widget corresponding to a project item on the Project tab
 * 
 * this widget can be referenced from hi.items.ProjectListProjects.projects[]
 */
define([
	"dojo/_base/array", // array.forEach array.map
	"dojo/_base/declare", // declare
	"dojo/cldr/supplemental", // cldrSupplemental.getFirstDayOfWeek
	"dojo/date", // date
	"dojo/date/locale",
	"dojo/dom", // dom.setSelectable
	"dojo/dom-class", // domClass.contains
	'dojo/dom-attr',
	"dojo/_base/event", // event.stop
	"dojo/_base/lang", // lang.getObject, lang.hitch
	"dojo/_base/sniff", // has("ie") has("webkit")
	"dojo/string", // string.substitute
	"dojo/_base/window", // win.doc.createTextNode
	"dijit/_Widget",
	"dijit/_Templated",
	"dijit/_WidgetsInTemplateMixin",
	"hi/widget/task/group",
	"dojo/text!./project.html"
], function(array, declare, cldrSupplemental, date, local, dom, domClass, domAttr, event, lang, has, string, win,
			_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, hi_widget_Group, template){
	
	//Replace localization string in template instead of using attributes
	template = template.replace(/\{#([^#]+)#\}/g, function (all, str) {
		var localized = hi.i18n.getLocalization(str.trim()) || '';
		if (!localized) console.warn('Missing localization for "' + str + '"');
		return localized;
	});
	
	// module:
	//		hi/widget/Project
	// summary:
	//		ProjectList project
	
	var Group = declare("hi.widget.Project", [hi_widget_Group, _WidgetsInTemplateMixin], {
		// summary:
		//		ProjectList Group
		//		Represents a single project
		
		// Template for the group
		templateString: template,
		
		// Class name
		groupClassName: "color8",
		
		// Parse widgets in template
		widgetsInTemplate: true,
		
		colorValueWidget: null,
		
		preventMainMenuClicks: false,

		fields: ['title', 'message', 'permissions', 'start_date', 'due_date'],

		// Title
		title: "",
		_setTitleAttr: function (title) {
			this._set("title", title);
			
			if (title) {
				this.projectTitleInput.set('value', title);
				this.projectTitleNode.innerHTML = "";
				this.projectTitleNode.appendChild(win.doc.createTextNode(title));
				dojo.removeClass(this.outerNode, "no_title");
			} else {
				this.projectTitleInput.set('value', "");
				this.projectTitleNode.innerHTML = "";
				dojo.addClass(this.outerNode, "no_title");
			}
		},
		
		// Message
		message: "",
		_setMessageAttr: function (message) {
			if(message === undefined || message === null){message = '';}
			this._set("message", message);
			
			this.projectMessageInput.set('value', message);
			
			if (message) {
				message = (message ? formatMessage(message) : '');
				this.projectMessageNode.innerHTML = message;
				
			}
			
			dojo.toggleClass(this.projectMessageNode.parentNode, "hidden", !message);
		},

		// start date
		start_date: "",
		_setStart_dateAttr: function (start_date) {
			// if (!start_date) return;

			this._set("start_date", start_date);
			
			this.startDateWidget.set('value', start_date || null);
		},

		// end date
		due_date: "",
		_setDue_dateAttr: function (due_date) {
			// if (!due_date) return;

			this._set("due_date", due_date);
			
			this.dueDateWidget.set('value', due_date || null);
			this.projectDueDateNode.innerHTML = due_date ? moment(due_date).format('DD/MM') : '';
		},

		colorValuePrev: CONSTANTS.defaultColorValue,
		_setColorValuePrevAttr: function(value) {
			if (testHexColor(value)) {
				this._set('colorValuePrev', value);
			}
		},
		
		/* #3052 - add color_value attribute to project */
		color_value:'',
		_setColor_valueAttr:function(value) {this._setColorValueAttr(value);},
		
		colorValue: CONSTANTS.defaultColorValue,
		_setColorValueAttr: function(value) {
			if (testHexColor(value)) {
				this._set('colorValue', value);
				//var gradientTopValue = colorLuminance(value, -0.1);
				//var gradientBottomValue = colorLuminance(value, -0.3);
				
				//this.projectInnerDiv.style.borderColor = colorLuminance(value, 80);//value;
				this.projectInnerDiv.style.borderColor = colorLuminance(value, 50);//value;
				
				/* Disabling gradient header
				this.headerNode.style.background = gradientTopValue; // Old browsers 
				// IE9 SVG, needs conditional override of 'filter' to 'none'
				this.headerNode.style.background = '-moz-linear-gradient(top,  ' + gradientTopValue + ' 0%, ' + gradientBottomValue + ' 100%)';
				this.headerNode.style.background = '-webkit-gradient(linear, left top, left bottom, color-stop(0%,' + gradientTopValue + '), color-stop(100%,' + gradientBottomValue + '))'; 
				this.headerNode.style.background = '-webkit-linear-gradient(top,  ' + gradientTopValue + ' 0%,' + gradientBottomValue + ' 100%)';  
				this.headerNode.style.background = '-o-linear-gradient(top,  ' + gradientTopValue + ' 0%,' + gradientBottomValue + ' 100%)'; 
				this.headerNode.style.background = '-ms-linear-gradient(top,  ' + gradientTopValue + ' 0%,' + gradientBottomValue + ' 100%)'; 
				this.headerNode.style.background = 'linear-gradient(to bottom,  ' + gradientTopValue + ' 0%,' + gradientBottomValue + ' 100%)'; 
				*/
//				dojo.forEach(dojo.query('span.project_name_input', this.projectInnerDiv), function(span) {
//					span.style.color = value;//colorLuminance(value, -20);
//				});
				dojo.forEach(dojo.query('span.folder', this.projectHeaderIconWrapper), function(span) {
					//var iconValue = colorLuminance(value, 0.4);
					//span.style.color = iconValue;
					span.style.color = value;
				});
			}
		},

		permission: 0,
		_setPermissionAttr: function(value){
			this._set('permission', value);

			var show = dojo.global.showElement,
				hide = dojo.global.hideElement;

			// switch(value) {
			// 	case Hi_Permissions.VIEW_COMMENT:
			// 		show(this.menuContainer);
			// 		hide(this.editButton);
			// 		hide(this.removeButton);
			// 		hide(this.duplicateButton);
			// 		hide(this.addNewButton);
			// 		show(this.reportButton);
			// 		show(this.publishButton);
			// 		hide(this.projectSharingWidgetContainer);
			// 		break;
			// 	case Hi_Permissions.COMPLETE_ASSIGN:
			// 		show(this.menuContainer);
			// 		hide(this.editButton);
			// 		hide(this.removeButton);
			// 		hide(this.duplicateButton);
			// 		show(this.addNewButton);
			// 		show(this.reportButton);
			// 		show(this.publishButton);
			// 		hide(this.projectSharingWidgetContainer);
			// 		break;
			// 	case Hi_Permissions.MODIFY:
			// 		show(this.menuContainer);
			// 		show(this.editButton);
			// 		hide(this.removeButton);
			// 		hide(this.duplicateButton);
			// 		show(this.addNewButton);
			// 		show(this.reportButton);
			// 		show(this.publishButton);
			// 		hide(this.projectSharingWidgetContainer);
			// 		break;
			// 	case Hi_Permissions.EVERYTHING:
			// 		show(this.menuContainer);
			// 		show(this.editButton);
			// 		show(this.removeButton);
			// 		show(this.duplicateButton);
			// 		show(this.addNewButton);
			// 		show(this.reportButton);
			// 		show(this.publishButton);
			// 		show(this.projectSharingWidgetContainer);
			// 		break;
			// }
			if (!Project.businessId) {
				hide(this.projectSharingWidgetContainer);
			}
		},

		permissions: null,
		_setPermissionsAttr: function(pms){
			if(typeof pms === 'string'){
				pms = JSON.parse(pms);
				hi.items.manager.set(this.projectId, {permissions: pms}, true/*silent*/);
			}
			this._set('permissions', pms);
			this.projectSharingWidget.taskId = this.projectId; // #5014
			this.projectSharingWidget.fillSharingList(pms);
		},

		// Project is being edited
		editing: false,
		_setEditingAttr: function (editing) {
			this._set("editing", editing);

			/* #3948 force inputs to update value for value trimed*/
			var t = this;
			array.forEach(this.fields, function(item) {
				t.set(item, t[item]);
			});
			// this.set('title', this.title);
			// this.set('message', this.message);
			// this.set('permissions', this.permissions);

			dojo.toggleClass(this.editClosedNode, "hidden", editing);
			dojo.toggleClass(this.editOpenedNode, "hidden", !editing);
			dojo.toggleClass(this.outerNode, "project-edit", editing);
			
			if (!this.expanded) {
				this.set("expanded", true);
			}
			if (editing) {
				this.projectTitleNode.focus();
			}
		},
		
		// Group is expanded
		expandable: true,
		_setExpandableAttr: function (expandable) {
			this._set("expandable", expandable);
			if (!expandable) this.set("expanded", true);
		},
		
		expanded: null,
		_setExpandedAttr: function (expanded) {
			if (!this.get("expandable")) {
				expanded = true;
			}
			
			//Style
			if (expanded) {
				dojo.addClass(this.headerNode, "opened");
				dojo.addClass(this.headerExpandNode, 'hidden');
				dojo.removeClass(this.tasksNode, "hidden");
				dojo.removeClass(this.headerCollapseNode, "hidden");
			} else {
				dojo.removeClass(this.headerNode, "opened");
				dojo.removeClass(this.headerExpandNode, 'hidden');
				dojo.addClass(this.tasksNode, "hidden");
				dojo.addClass(this.headerCollapseNode, 'hidden');
			}
			
			//Save preferences
			if (this.expanded !== null && this.expanded != expanded) {
				var key = "p_" + this.preferencesGroup + '_' + this.projectId;
				if (key) {
					Hi_Preferences.set(key, expanded ? '1' : '0');
				}
			}
			
			this._set("expanded", expanded);
		},
		
		addChild: function (node, index) {
			// summary:
			//		Place child node
			
			dojo.place(node.domNode, this.contentNode, "first");
			node.set("container", this);
		},
		
		percentage: 0,
		_setPercentageAttr: function (percentage) {
			this._set("percentage", percentage);
			
			var toggleClass = !percentage && !this.hasTimeTrackingItems();
			dojo.toggleClass(this.itemPercentageNode, "hidden", toggleClass);
			dojo.toggleClass(this.itemPercentageNodeCell, "hidden", toggleClass);
			this.itemPercentageNode.innerHTML = percentage + "%";
		},
		
		shared: 0,
		_setSharedAttr: function (shared) {
			this._set("shared", shared);
			
			if (Project.businessId && !shared) {
				dojo.removeClass(this.privateProjectNodeCell, "hidden");
			} else {
				dojo.addClass(this.privateProjectNodeCell, "hidden");
			}
			
			if (!Project.businessId) {
			}
			
			this.showHidePublishMenu();
		},
		
		// Drag handle visibility
		sortable: false,
		_setSortableAttr: function (sortable) {
			this._set("sortable", sortable);
			dojo.toggleClass(this.outerNode, "sortable", sortable);
		},
		
		
		buildRendering: function() {
			this.inherited(arguments);

			var datePattern = Hi_Preferences.get('date_format').replace('m', 'MM').replace('d', 'dd').replace('y', 'yyyy'),
			constraints = this.startDateWidget.get("constraints");
				
			constraints.datePattern = datePattern;
			this.startDateWidget.set("constraints", constraints);
			this.dueDateWidget.set("constraints", constraints);

			var datePlaceholder = (datePattern || 'date').toUpperCase();
			var placeholderAttr = this.hasNativePlaceholderSupport() ? 'placeholder' : 'placeHolder';
			
			this.startDateWidget.set(placeholderAttr, datePlaceholder);
			this.dueDateWidget.set(placeholderAttr, datePlaceholder);
			
			// Handle edit form textarea resize
			handleDDElement(this.projectMessageInput.domNode.parentNode);
			
			this.colorValueWidget = new hi.widget.ItemColorValue().placeAt(this.projectColorValuePickerWrapper);
			this.colorValueWidget.on('change', dojo.hitch(this, this.handleColorChange));
			this.colorValueWidget.set('color', this.get('colorValue'));
			
			/* #2288 - handled automatically now dojo.connect(Project, "changeSorting", this, this.handleSortingChange);*/

			this.showHidePublishMenu();
		},
		
		hasNativePlaceholderSupport: function() {
			var inp = document.createElement('input');
			return typeof(inp.placeholder) != 'undefined';
		},

		/* ----------------------------- API ------------------------------ */
		remove: function (includeTasks) {
			// summary:
			//		Remove project and optionally also tasks
			
			var errMsg = hi.i18n.getLocalization('no_permissions_to_delete.not_project_owner_or_administrator') || 'Sorry, you do not have permissions to delete this project.';
			if (!Project.canModifyDeleteX(this.projectId, errMsg, hi.widget.TaskItem.CATEGORY_PROJECT)) {
				return false;
			} else {
				hi.items.manager.removeSave(this.projectId, {
					"cascade": (includeTasks ? 1 : 0)
				});
			}
		},
		
		save: function (event) {
			// summary: 
			//		Save changes
			
			var values = dojo.form.getFormValues(this.editOpenedNode),
				validation = Hi_Validate.ptitle(values),
				permission = this.get('permission');
			
			if (validation === false || permission < Hi_Permissions.MODIFY) return;
			if (typeof validation == "object") values = validation;
			
			trackEvent(trackEventNames.PROJECT_MODIFY);
			
			var data ={
				"title": values.title,
				"message": values.message,
				'color_value': this.get('colorValue')
			};
			if (this.get('permission') === Hi_Permissions.EVERYTHING && Project.businessId) {
				data.permissions = JSON.stringify(this.projectSharingWidget.get('value'));
			}

			var start_date = this.startDateWidget.get('value');
			var due_date = this.dueDateWidget.get('value');

			data.start_date = start_date ? time2strAPI(start_date) : '';

			data.due_date = due_date ? time2strAPI(due_date) : '';

			hi.items.manager.setSave(this.projectId, data);

			this.set('colorValuePrev', this.get('colorValue'));
			this.set("editing", false);
			
			if (event) dojo.stopEvent(event);
		},
		
		cancel: function (event) {
			// summary:
			//		Cancel changes
			
			this.projectTitleInput.set('value', this.title);
			this.projectMessageInput.set('value', this.message);
			if (this.get('colorValuePrev') != this.get('colorValue')) {
				this.set('colorValue', this.get('colorValuePrev'));
			}
			this.handleMessageBlur();
			
			this.set("editing", false);
			
			if (event) dojo.stopEvent(event);
		},
		
		edit: function (event) {
			// summary:
			//		Start editing project
			
			if (event) dojo.stopEvent(event);

			if (!Hi_Actions.validateAction('modify', this.projectId)) return false;
			
			if (!Project.businessId || Project.checkLimits("TRIAL_PROJECTS")) {
				var errMsg = hi.i18n.getLocalization('no_permissions_to_edit.not_shared_or_assignee') || 'Sorry, you do not have permissions to edit this project.';
				if (!Project.canModifyX(this.projectId, errMsg, hi.widget.TaskItem.CATEGORY_PROJECT)) {
					return false;
				} else {
					this.set("editing", true);
					this.colorValueWidget.set('color', this.get('colorValue'));
				}
			}
		},
		
		addNew: function () {
			// summary:
			//		Show new item form
			
			var list = this.lists[0],
				parentPermissions = this.permissions || [];		// Bug #5197

			if (list) {
				// #4918: need to pass initial permission in order to setup Item New widget properly.
				list.addNew({permission: Hi_Permissions.EVERYTHING, permissions: parentPermissions}, list);
			}
		},
		
		share: function (includeTasks) {
			// summary:
			//		Make project shared and optionally also tasks
			// includeTasks: Boolean
			//		Make tasks also shared
			
			//If sharing is allowed
			if (Project.businessId && Project.checkLimits("TRIAL_PROJECTS")) {
				
				if (this.editing) {
					this.cancel();
				}
				
				hi.items.manager.setSave(this.projectId, {"shared": 1}).then(dojo.hitch(this, function () {
					this.hideSubMenu("subsubmenu-share");
					this.hideSubMenu("subsubmenu-unshare");
					
					if (includeTasks) {
						this.shareTasks();
					}
					else {
						this.unShareTasks();
					}
				}));
			}
			
		},

		unShare: function () {
			// summary:
			//		Make project private

			var errMsg = hi.i18n.getLocalization('no_permissions_to_edit.no_permission_to_set_as_private') || 'Project belongs to another team member. Only an administrator or owner can make this project Private!';
			if (!Project.canUnShareX(this.projectId, errMsg)) {
				return false;
			} else {
				if (this.editing) {
					this.cancel();
				}

				this.unShareTasks();
				
				hi.items.manager.setSave(this.projectId, {"shared": 0}).then(dojo.hitch(this, function () {
					this.hideSubMenu("subsubmenu-unshare");
				}));
			}
		},
		
		shareTasks: function () {
			// summary:
			//		Make all tasks shared
			
			var items = hi.items.manager.get({"parent": this.projectId}),
				i = 0,
				ii = items.length;
			
			for (; i<ii; i++) {
				if (!items[i].shared || items[i].shared === "0") {
					hi.items.manager.setSave(items[i].id, {"shared": 1});
				}
			}
		},

		unShareTasks: function() {
			// summary:
			//		Make tasks private

			var items = hi.items.manager.get({"parent": this.projectId});

			/*
			#3533
			Item owner 		sharing	 action
			-------------+----------+----------------------------
			current user	shared	 make private
			current user	private	 no change
			other user	 	shared	 shared, move out of project
			other user	 	private	 private, move out of project

			*/

			array.forEach(items, function(item) {
				if (Project.user_id == item.user_id && item.shared) {
					hi.items.manager.setSave(item.id, {"shared": 0});
				}
				else if (Project.user_id == item.user_id && !item.shared) {
					//Do nothing
				}
				else if (Project.user_id != item.user_id && item.shared) {
					hi.items.manager.setSave(item.id, {"parent": ""});
				}
				else if (Project.user_id != item.user_id && !item.shared) {
					hi.items.manager.setSave(item.id, {"parent": ""});
				}
			}, this);
		},
		
		showSubMenu: function (submenuClass) {
			// Toggle project submenu
			if (!submenuClass) return;
			
			var target = dojo.query("li a[data-submenu='" + submenuClass + "']", this.submenusNode).pop();
			if (!target) return;
			
			var listItem = target.parentNode,
				submenuItem = dojo.query("div." + submenuClass, this.submenusNode).pop();
			
			if (this.editing) this.cancel();
			
			var selected = dojo.query("li.selected a", this.submenusNode);
			if (selected.length) {
				//Close previous opened item
				var hideSubmenuClass = selected[0].getAttribute("data-submenu");
				if (hideSubmenuClass != submenuClass) {
					this.hideSubMenu(hideSubmenuClass);
				}
			}
			
			dojo.addClass(listItem, "selected");
			dojo.removeClass(submenuItem, "hidden");
			
			if (dojo.isIE && dojo.isIE <= 7) {
				forceReflowIE(this.tasksNode);
			}
		},
		
		hideSubMenu: function (submenuClass) {
			// Toggle project submenu
			var target;

			if (!submenuClass) {
				target = dojo.query("li a.selected", this.submenusNode).pop();
				if (target) {
					submenuClass = target.getAttribute("data-submenu");
				}
				if (!submenuClass) {
					return;
				}
			}
			
			target = dojo.query("li a[data-submenu='" + submenuClass + "']", this.submenusNode).pop();
			if (!target) return;
			
			var listItem = target.parentNode,
				submenuItem = dojo.query("div." + submenuClass, this.submenusNode).pop();
			
			if (this.editing) this.cancel();
			
			dojo.removeClass(listItem, "selected");
			dojo.addClass(submenuItem, "hidden");
			
			if (dojo.isIE && dojo.isIE <= 7) {
				forceReflowIE(this.tasksNode);
			}
		},
		
		hasTimeTrackingItems: function() {
			return dojo.some(hi.items.manager.get({parent: this.projectId}), function(item) {
				if (item.time_track) {
					return true;
				}
			});
		},
		
		showHidePublishMenu: function() {
			if (this.get('permission') >= Hi_Permissions.VIEW_COMMENT) {
				domClass.remove(this.publishButton, "hidden");
				
				var p = hi.items.manager.get(this.projectId);
				
				if (p && p.publish_url) {
					domClass.add(this.submenuPublishNode, 'hidden');
					domClass.remove(this.submenuUnpublishNode, 'hidden');
					domClass.remove(this.submenuPublishedNode, 'hidden');
					domAttr.set(this.submenuPublishedLink, 'href', p.publish_url);
					this.submenuPublishedText.innerHTML = p.publish_url;
				} else {
					domClass.remove(this.submenuPublishNode, 'hidden');
					domClass.add(this.submenuUnpublishNode, 'hidden');
					domClass.add(this.submenuPublishedNode, 'hidden');
				}
			} else {
				domClass.add(this.publishButton, "hidden");
			}
		},
		
		
		/* ----------------------------- EVENT BINDINGS ------------------------------ */
		handleAddItem: function (event) {
			// summary:
			//		Handle new item link click
			if (this.preventMainMenuClicks || !Hi_Actions.validateAction('addSub', this.projectId)) {
				dojo.stopEvent(event);
				return false;
			}
			
			/* #2589 */if (Hi_Search.isFormVisible()) Hi_Search.closeTooltipDialog('search');
			if (!hi.items.manager.editing() && Project.checkLimits('TRIAL_TASKS')) {
				this.addNew();
			}
			dojo.stopEvent(event);
		},
		
		handleToggleSubMenu: function (event) {
			// Toggle project submenu
			var target = event.target,
				listItem = target.parentNode,
				submenuClass = target.getAttribute("data-submenu");
			
			if (!Project.businessId || Project.checkLimits("TRIAL_PROJECTS")) {
				if (dojo.hasClass(listItem, "selected")) {
					this.hideSubMenu(submenuClass);
				} else {
					this.showSubMenu(submenuClass);
				}
			}
			
			dojo.stopEvent(event);
		},
		
		handleToggle: function (e) {
			if (!this.editing) {
				this.toggle();
				
				// #2561
				if (e && (e.shiftKey || e.ctrlKey || e.altKey)) {
					if (this.get('expanded')) {
						hi.items.ProjectListProjects.expandAll();
					} else {
						hi.items.ProjectListProjects.collapseAll();
					}
				}
			}
		},
		
		handleDuplicateClick: function (event) {
			// summary:
			//		Duplicate project and tasks
			
			if (Project.checkLimits('TRIAL_PROJECTS')) {
				Project.duplicateProject(this.projectId);
			}
			
			dojo.stopEvent(event);
		},
		
		handleArchiveClick: function (event) {
			// summary:
			//		Archive project and all tasks
			
			if (event) {
				dojo.stopEvent(event);
			}

			if (!Hi_Actions.validateAction('archive', this.projectId)) return false;
			
			var errMsg = hi.i18n.getLocalization('no_permissions_to_archive.not_shared_or_assignee') || 'Sorry, you do not have permissions to archvie this project.';
			if (!Project.canModifyX(this.projectId, errMsg, hi.widget.TaskItem.CATEGORY_PROJECT)) {
				return false;
			} else {
				archiveTask(this.projectId, null, true);
			}
		},
		
		handlePublishClick: function(e) {
			// summary:
			//		Publishes project.
			if (e) {
				dojo.stopEvent(e);
			}
			
			var showResult = function(url) {
				Project.alertElement.showItem(27, {url: url});
			};
			var p = hi.items.manager.get(this.projectId);
			var self = this;
			
			if (p && p.publish_url) {
				showResult(p.publish_url);
			} else if (p) {
				this.preventMainMenuClicks = true;
				this.hideSubMenu('subsubmenu-publish');
				
				Project.ajax('itempublish', {id: p.id}, function(resp) {
					this.preventMainMenuClicks = false;
					showResult(resp.response);
					hi.items.manager.refresh(true);
					self.showHidePublishMenu();
				}, function(a, b, resp, d) {
					this.preventMainMenuClicks = false;
					switch (resp.getStatus()) {
						case 1:
							alert('The item you are trying to publish could not be found.');
							break;
						case 6:
							alert('The item you are trying to publish either was deleted or archived.');
							break;
						default:
							alert(resp.getErrorMessage() || 'Error occurred.');
					}
				});
			}
		},
		
		handleUnpublishClick: function(e) {
			// summary:
			//		Unublishes project.
			if (e) {
				dojo.stopEvent(e);
			}
			
			var p = hi.items.manager.get(this.projectId);
			var self = this;
			
			if (confirm('Stop publishing?') && p) {
				this.preventMainMenuClicks = true;
				this.hideSubMenu('subsubmenu-publish');
				
				Project.ajax('itemunpublish', {id: p.id}, function(resp) {
					this.preventMainMenuClicks = false;
					hi.items.manager.refresh(true);
					self.showHidePublishMenu();
				}, function(a, b, resp, d) {
					this.preventMainMenuClicks = false;
					switch (resp.getStatus()) {
						case 1:
							alert('The item you are trying to stop publishing could not be found.');
							break;
						default:
							alert(resp.getErrorMessage() || 'Error occurred.');
					}
				});
			}
		},
		
		handleDeleteClick: function (event) {
			// summary:
			//		Delete project but keep tasks
			
			dojo.stopEvent(event);
			if (!Hi_Actions.validateAction('delete', this.projectId)) return false;

			if (confirm(hi.i18n.getLocalization("project.delete_project_confirmation"))) {
				this.remove(false);
			}
			
		},
		
		handleDeleteAllClick: function (event) {
			// summary:
			//		Delete project and tasks
			dojo.stopEvent(event);
			if (!Hi_Actions.validateAction('delete', this.projectId)) return false;

			if (confirm(hi.i18n.getLocalization("project.delete_project_confirmation"))) {
				this.remove(true);
			}
			
		},
		
		handleProgressReportClick: function (event) {
			// summary:
			//		Open project progress report dialog
			
			dojo.byId('projectProgressReportId').value = this.projectId;
			Tooltip.open('projectProgressReport', null, event.target, event, 'below-centered', undefined, {orient: ['below-centered', 'above-centered', 'before-centered', 'after-centered']});
			dojo.stopEvent(event);
		},
		
		handleTimeReportClick: function (event) {
			// summary:
			//		Open project time report
			
			window.open('/report/?code=project_time&format=html&project_id=' + this.projectId,'reportwindow','scrollbars=yes,resizable');
			dojo.stopEvent(event);
		},
		
		handleMessageFocus: function (event) {
			// summary:
			//		On focus hide message label
			
			hideElement(previousElement(this.projectMessageInput));
		},

		handleMessageBlur: function (event) {
			// summary:
			//		On focus hide message label
			
			if (!this.projectMessageInput.value) {
				showElement(previousElement(this.projectMessageInput));
			}
		},
		
		handleEditKeyPress: function (event) {
			// summary:
			//		Handle return and escape keys
			var key  = event.keyCode || event.which,
				keys = dojo.keys,
				el   = event.target;
			
			if (key == keys.KEY_ENTER && (el.tagName == "TEXTAREA" || (el.tagName == "INPUT" && el.type == "button") || el.tagName == "BUTTON")) {
				event.cancelBubble = true;
				return;
			}
			
			if (key == keys.KEY_ESCAPE) {
				dojo.stopEvent(event);
				this.cancel();
			} else if (key == keys.KEY_ENTER) {
				dojo.stopEvent(event);
				this.save();
			}
		},
		
		handleMessageTooltip: function (event) {
			// summary:
			//		Show message tooltip
			
			Tooltip.open("formattinghelp", "", this.projectMessageTooltip, event, "below-centered");
			Click.setClickFunction(function() {}, this.projectMessageTooltip, "formattinghelp");
			/* Bug #1873 */
			var self = this;
			require(['dojo/aspect'],function(aspect) {
				aspect.before(self,'cancel',function() {if (Tooltip.opened('formattinghelp')) Tooltip.close('formattinghelp');});
				aspect.after(self,'save',function() {if (!self.editing && Tooltip.opened('formattinghelp')) Tooltip.close('formattinghelp'); });
			});
		},
		
		handleProjectShare: function (event) {
			if (Project.checkLimits("TRIAL_PROJECTS")) {
				this.share(false);
				dojo.stopEvent(event);
			}
		},
		
		handleProjectShareAll: function (event) {
			if (Project.checkLimits("TRIAL_PROJECTS")) {
				this.share(true);
				dojo.stopEvent(event);
			}
		},

		handleProjectUnShare: function (event) {
			this.unShare();
			dojo.stopEvent(event);
		},
		
		handleSortingChange: function () {
			// summary:
			//		On sorting change show/hide drag handle
			
			this.set("sortable", Project.getSorting() == Project.ORDER_PRIORITY);
		},
		
		handleColorChange: function(value) {
			this.set('colorValue', value);
		},

		handleStartDateChange: function(value) {
			var due = this.dueDateWidget.get('value');

			if (!value || !due) return;

			var start = value;

			if (start.getTime() > due.getTime()) {
				this.dueDateWidget.set('value', start);
			}
		},

		handleDueDateChange: function(value) {
			var start = this.startDateWidget.get('value');

			if (!value || !start) return;

			var due = value;

			if (start.getTime() > due.getTime()) {
				this.startDateWidget.set('value', due);
			}
		},

		destroy:function() {
			this.inherited(arguments);
		},
		
	});
	
	return Group;
	
});
