//dojo.provide("hi.dnd.target.task");
define(function () {
	
	dojo.declare("dojo.dnd.HiDragTargetSelf", [dojo.dnd.Source], {
		isSource: false,

		selfAccept: false,
		
		accept: ["dragged_task"], 
		
		dropValue: null,
		
		permissionsAllowed: [Hi_Permissions.COMPLETE_ASSIGN, Hi_Permissions.MODIFY, Hi_Permissions.EVERYTHING],

		action: 'assign',
		
		/* #2535 via https://support.sitepen.com/issues/22098 */
		onSelectStart:function(e) {return;},
		onMouseOut: function (e) {
			dojo.dnd.HiDragTargetSelf.superclass.onMouseMove.call(this, e);
			
			//JDT var m = dojo.dnd.Manager();
			var m = dojo.dnd.Manager.manager();
			if (m.source instanceof dojo.dnd.HiDragSource) {
				m.avatar.updateText('');
				this.dropValue = false;
			}
		},
		
		onMouseOver: function (e) {
			dojo.dnd.HiDragTargetSelf.superclass.onMouseOver.call(this, e);
			
			if (this.isDragging) {
				//var m = dojo.dnd.Manager();
				var m = dojo.dnd.Manager.manager(), active_user;


				if (m.source instanceof dojo.dnd.HiDragSource) {
					var txt = '';
					var dragId = m.source.dragNode.dragId;
					
					if (this.node.id == 'conversation_container') {
						if (parseInt(Hi_Preferences.get('sidebar_messenger')) == 0) return;
						active_user = parseInt(getValue('chat_user_id'));
					} else {
						active_user = parseInt(Project.user_id);
					}
					
					if (Project.canAssign(dragId, active_user)) {
						txt = this.text + Project.getFriendName(active_user);
						this.dropValue = active_user;
					}
					
					m.avatar.updateText(txt);
				}
			}
		}
	});
	
	return dojo.dnd.HiDragTargetSelf;
});