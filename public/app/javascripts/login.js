/**
 * @namespace formUtils
 * Object for submitting login
 */
var formUtils = {

	requestType:'GET',
	ajaxURL: ApiConfig.apiUrl + '/user/authenticate',
	dataType:null,
	api_key: ApiConfig.key,
	async:true,
	minFieldValue:4,
	
	prepareForm: function() {
		$('#username, #password').popover({placement: 'right', trigger: 'manual', html: true, animation: false});
		$('#username, #password').click(function() {
			$(this).popover('hide');
		});
	},
	
	/**
	 * Action of the form's onsubmit method<br>
	 * Kill the event
	 * trim username and password fields
	 * validate username and password fields for length
	 * try to submit to authenticate service
	 * 
	 */
	submitForm:function(e)
	{
		this.hideAllErrorPopovers();
		var self = this;
		if (e) {
			e.returnValue=false;
			if (e.preventDefault) {
				e.preventDefault();
			}
		}
		//Trim white space from beginning and end
		var username = this._trim($('#username').val());
		var password = this._trim($('#password').val());
		var inputError = false;
		
		if	(username.length==0) {
			this.showErrorPopover($('#username'), i18n('hi.login.empty_username'));
			inputError = true;
		} else if (!this.validateLength(username)) {
			this.showErrorPopover($('#username'), i18n('hi.login.short_username', this.minFieldValue));
			inputError = true;
		}
		
		if	(password.length==0) {
			this.showErrorPopover($('#password'), i18n('hi.login.empty_password'));
			inputError = true;
		} else if (!this.validateLength(password)) {
			this.showErrorPopover($('#password'), i18n('hi.login.short_password', this.minFieldValue));
			inputError = true;
		}
		
		if (inputError) {
			return;
		}
		
		//Encrypt password
		password = CryptoJS.MD5(password).toString();
		
		//Do Ajax
		var dataObj = {
			api_key:this.api_key,
			login:username,
			password:password
		};
		
		var obj = {
			cache:false,
			type:this.requestType,
			url:this.ajaxURL,
			//IE throws up with JSON (No Transport error)
			dataType:$.browser['msie']&&useJSONP?'jsonp':'json',
			data:dataObj,
			timeout: 30000
		};
		
		//Push progress bar into view
		var top = ($('form').height()/2) - 10;		
		$('#thinker').css({opacity: 1, visibility: 'visible'});
		$('#thinker').css('top',top+'px');
		$('form').css('opacity','0');
		
		//Currently cannot handle specific errors, so just
		//user an error box with a general error
		$.ajax(obj)
		
		.success(function(data,textStatus,jqXHR) {
			if (typeof(data.error_message) != 'undefined') {
				self.showError(i18n('hi.login.error_incorrect_login'), '<a href="/recover">' + i18n('hi.login.error_help_login') + '</a>');
			} else {
				window.location.href = '/app?session_id=' + data.session_id + '&rememberme10=' + $('#remember').is(':checked');
			}
		})
		.error(function(jqXHR, textStatus) {
			//On bad login, we either get textStatus='error' or textStatus='parseerror'
			self.handleLoginError(jqXHR.status, jqXHR.statusText, textStatus);
		});
	},
	
	handleLoginError: function(status, statusText, textStatus, externalType) {
		if (status == 504 || statusText == 'timeout' || textStatus == 'timeout') {
			this.shakeForm();
			this.showError(i18n('hi.login.error_timeout'));
		} else if (status == 403 && !externalType) {
			this.shakeForm();
			this.showError(i18n('hi.login.error_incorrect_login'), '<a href="/recover">' + i18n('hi.login.error_help_login') + '</a>');
		} else if (status == 403 && externalType) {
			this.shakeForm();
			this.showError(i18n('hi.login.error_incorrect_external_login', externalType));
		} else if (status == 404 || status >= 500) {
			this.showError(i18n('hi.login.error_service'));
		} else {
			// Some unexpected error occured.
			this.showError(i18n('hi.login.error_service'));
		}
	},
	
	/**
	 * Validate length of an input field value
	 * @param {String) value  
	*/
	validateLength:function(value)
	{
		return value.length>=this.minFieldValue;
	},
	
	/**
	 * Remove white space from the front and back of a string
	 * @param {String} value 
	*/
	_trim:function(value)
	{
		return value.replace(/(^\s*)*(\s*$)*/g,'')
	},
	
	/**
	 * An error template box: from http://twitter.github.com/bootstrap/components.html#alerts
	 */
	errorTemplate:[
					'<div class="alert alert-error" >',
						'<div>{MSG}</div>',
						'<div>{SUB_MSG}</div>',
					'</div>',
	               ].join(''),
	               
	hideError: function() {
		$('#thinker').css({opacity: 0, visibility: 'hidden'});
		$('form').css('opacity','1');
		$('.alert').remove();
	},
	
	/**
	 * Show an error box using errorTemplate
	 * @param {String} msg The error message to put in the error box
	 */
	showError:function(msg, subMsg)
	{
		this.hideError();
		var content = this.errorTemplate;
		content = content.replace('{MSG}',msg);
		content = content.replace('{SUB_MSG}',subMsg ? subMsg : '');
		
		$(content).insertAfter('#signinForm fieldset legend');
		
	},
	
	showErrorPopover: function($on, msg)
	{
		this.hideError();
		$on.attr('data-content', msg);
		$on.popover('show');
	},
	
	hideAllErrorPopovers: function() {
		$('#username, #password').popover('hide');
	},
	
	shakeForm:function() {
	    var p = [5, 10, 5, 0, -5, -10, -5, 0,5, 10, 5, 0, -5, -10, -5, 0];
	  //  var x = $('#form-container').offset().left;
	    var x = parseInt($('#form-container').css("margin-left"),10);
	    var speed = 15;
	    $.each(p, function() {
	        $('#form-container').animate({'margin-left': x + this}, speed);
	    });
	}
};

$(document).ready(function() {
	formUtils.prepareForm();
	if ($('#signinForm').length > 0) {
		$('#signinForm')[0].submit = function() {
			formUtils.submitForm();
			return false;
		};
	}
});