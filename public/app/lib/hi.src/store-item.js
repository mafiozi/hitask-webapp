define([
	"hi/store"
], function (hi_store) {
	//hi_store === hi.store
	
	hi_store.initStore = function () {
		
		// Create project dojo store
		hi_store.create("Project", {
			target: "item/" + Project._itemPage.id,
			idAttribute: "id",
			targetOptions: {
				doNotAppendId: false,
				removeBaseId: true
			}
		});
		
		hi_store.Project.getOrder = function () {
			return "";
		};
		
		hi_store.Project.refresh = function () {
			// summary:
			//		Sync with server
			return this.query({}, params);
		};

		hi_store.Project.refreshStartTimer = function () {};

		hi_store.Project.attributeRequiresSort = function(attribute) {
			var currentSortAttribute = hi_store.Project.getOrderConfiguration(true),
				sortAttribute = null;

			for (var i = 0; i < currentSortAttribute.length; i++) {
				sortAttribute = currentSortAttribute[i].attribute;
				if (sortAttribute == attribute) return true;
			}

			return false;
		};

		hi_store.Project.getOrderConfiguration = function(asArray) {
			var sorting = Project.getSorting();
			
			if(sorting == Project.ORDER_SUBJECT) {
				if (asArray) {
					return [{attribute: "title", descending: false, caseInsensitive: true}];
				} else {
					return function(a, b) {
						var aValue = typeof(a.title) == 'string' ? a.title.toUpperCase() : a.title;
						var bValue = typeof(b.title) == 'string' ? b.title.toUpperCase() : b.title;
						var descending = false;
						
						if (aValue != bValue) {
							return !!descending == (aValue == null || aValue > bValue) ? -1 : 1;
						}
						
						return 0;
					};
				}
			} else if (sorting == Project.ORDER_START_DATE) {
				return [{attribute: "start_date", descending: false}, {attribute: "start_time", descending: false}, {attribute:'priority',descending:true}];
			} else if (sorting == Project.ORDER_LAST_MODIFIED) {
				return [{attribute: "time_last_update", descending: true}];
			} else if (sorting == Project.ORDER_PRIORITY) {
				return [{attribute: "priority", descending: true}];
			} else if (sorting == Project.ORDER_DUE_DATE) {
				return [{attribute: "end_date", descending: false}, {attribute: "end_time", descending: false}, {attribute:'priority',descending:true}];
			} else {
				return null;
			}
		};
		
		// Load data
		setTimeout(function() {
			hi_store.Project.query({includeChildren: true}).then(function (item) {
				var container = dojo.byId(Project.getContainer('tasksList'));
				dojo.removeClass(container, "loading");
			});	
		}, 2000);
		
	};
	
	return hi_store;
	
});