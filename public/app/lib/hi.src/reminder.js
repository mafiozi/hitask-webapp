define(function () {
	
	return dojo.global.Hi_Reminder = {
		default_time: 5,
		default_time_type: 'm',
		sound: 1,
		enabled: true,
		//dateStart: '8:00',
		dateStart: false,
		_timeouts: {},
		_interval: false,
		
		getTimeBefore: function(task_id) {
			var enabled = hi.items.manager.get(task_id, 'reminder_enabled');
			if (!enabled) return false;
			var time = hi.items.manager.get(task_id, 'reminder_time');
			var time_type = hi.items.manager.get(task_id, 'reminder_time_type');
			time = parseFloat(time);
			if (!isFinite(time)) time = 0;
			switch (time_type) {
				case 's':
					break;
				case 'm':
					time *= 60;
					break;
				case 'h':
					time *= 3600;
					break;
				case 'd':
					time *= 3600 * 24;
				default:
					return false;
			}
			return time * 1000;
		},
		init: function() {
			if (!Hi_Reminder._interval) {
				Hi_Reminder._interval = dojo.global.setInterval(Hi_Reminder.resetAllReminders, 86400000 / 2);
			}
		},
		setTimeout: function(task_id, interval, time) {
			Hi_Reminder._timeouts[task_id] = dojo.global.setTimeout(function() {Hi_Reminder.popup(task_id, time)}, interval + Math.round(Math.random() * 2000 - 1000));
		},
		setReminder: function(task_id) {
			Hi_Reminder.clearTimeout(task_id);
			
			if (Project.assigned(task_id) || hi.items.manager.get(task_id, 'completed')) return;
			
			var time = hi.items.date.getEventStartTime(task_id, Hi_Reminder.dateStart);
			if (!time) return;
			var before = Hi_Reminder.getTimeBefore(task_id);
			if (before === false) {
				return;
			}
			var recurring;
			if (hi.items.manager.get(task_id, 'category') == 5) {
				recurring = 4;
			} else {
				recurring = hi.items.manager.get(task_id, 'recurring');
			}
			
			var ret = HiTaskCalendar.nextEvent(time, recurring);
			
			//Check that recurring task instance is not completed or deleted
			if (recurring && ret) {
				if (!Hi_Reminder.validateReminder(task_id, ret[1])) {
					return;
				}
			}
			
			var interval = ret[0] - before;
			var time = ret[1];
			if (interval < 0 || interval > 86400000) return;
			Hi_Reminder.setTimeout(task_id, interval, time);
		},
		clearAllTimeouts: function() {
			var i;
			for (i in Hi_Reminder._timeouts) {
				Hi_Reminder.clearTimeout(i);
			}
		},
		clearTimeout: function(task_id) {
			if (task_id in Hi_Reminder._timeouts) {
				dojo.global.clearTimeout(Hi_Reminder._timeouts[task_id]);
				delete Hi_Reminder._timeouts[task_id];
				return true;
			}
			return false;
		},
		resetAllReminders: function() {
			Hi_Reminder.clearAllTimeouts();
			var items = hi.items.manager.get();
			for (var i=0, ii=items.length; i<ii; i++) {
				Hi_Reminder.setReminder(items[i].id);
			}
		},
		validateReminder: function (task_id, time) {
			var recurring = hi.items.manager.get(task_id, 'recurring');
			
			if (hi.items.manager.get(task_id, 'completed')) {
				return false;
			}
			
			//Check that recurring task instance is not completed or deleted
			if (recurring && time) {
				var instance_date = time2str(time, 'y-m-d'),
					instances = hi.items.manager.get(task_id, 'instances'),
					status = null;
				
				if (instances) {
					for (var i=0, imax=instances.length; i<imax; i++) {
						if (instances[i].start_date == instance_date) {
							return (instances[i].status == 1 || instances[i] == 2 ? false : true);
						}
					}
				}
			}
			
			return true;
		},
		popup: function(task_id, time) {
			//Make sure task exists and is not completed
			var item = hi.items.manager.get(task_id);
				
			if (item && Hi_Reminder.validateReminder(task_id, time)) {
				var list = dojo.byId('reminder_popup_list');
				var html = '';
				var title = hi.items.manager.get(task_id, 'title') || '';
				
				time = time2str(time, Hi_Calendar.format) + ' ' + HiTaskCalendar.formatTime(time2str(time, 'h:i'));
				var text = '<li>' + htmlspecialchars(title) + '<div class="time">' + htmlspecialchars(time) + '</div></li>';
				
				if (showNotifyEnabled()) {
					showNotify('Reminder: ' + title, null, time);
				} else {
					if (Hi_Reminder.reminderElement.open) {
						list.innerHTML += text;
					} else {
						list.innerHTML = text;
						Hi_Reminder.reminderElement.show();
					}
					list = null;
					dojo.global.setTimeout(function() {Hi_Reminder.alert(task_id)}, 5000);
				}
				
				if (Hi_Reminder.sound) {
					Hi_Reminder.playSound();
				}
			}
		},
		alert: function(task_id) {
			if (arguments.callee.opened || !Hi_Reminder.reminderElement.open) {
				return;
			}
			arguments.callee.opened = true;
			alert('Check your schedule!');
			dojo.global.setTimeout(function(){Hi_Reminder.alert.opened = false;}, 1000);
		},
		soundInterval: false,
		playSound: function() {
			if (Hi_Reminder.reminderElement.open) {
				Sound.play(Hi_Reminder.sound);
			} else {
				dojo.global.clearInterval(Hi_Reminder.soundInterval);
				Hi_Reminder.soundInterval = false;
			}
		}
	};
});