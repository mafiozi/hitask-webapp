//dojo.provide("hi.sortable.manager");
define([
	"dojo/_base/lang",
	'dojo/_base/window',
	'dojo/touch',
	'dojo/topic',
	'dojo/dnd/common',
	'dojo/on',
	'dojo/dnd/Manager'
], function(lang, win, touch, topic, dnd, on, Manager) {
	lang.extend(Manager,{
		overSource: function(source) {
			if (this.avatar) {
				this.target = (source && source.targetState != "Disabled") ? source : null;
				this.canDropFlag = Boolean(this.target);
				if (!this.source.noAvatar) {
					this.avatar.update();
				}
			}
			dojo.publish("/dnd/source/over", [source]);
		},
		
		outSource: function(source) {
			if (this.avatar) {
				if (this.target == source){
					this.target = null;
					this.canDropFlag = false;
					
					if (!this.source.noAvatar) {
						this.avatar.update();
					}
					
					dojo.publish("/dnd/source/over", [null]);
				}
			} else {
				dojo.publish("/dnd/source/over", [null]);
			}
		},
		
		startDrag: function(source, nodes, copy) {
			if (nodes && nodes.length && nodes[0] == null) {
				console.warn('there is something wrong with dnd initialization nodes');
				return;
			}
			this.source = source;
			this.nodes = nodes;
			this.copy = Boolean(copy); // normalizing to true boolean
			
			if (!this.source.noAvatar) {
				this.avatar = this.makeAvatar();
				dojo.body().appendChild(this.avatar.node);
			} else {
				this.avatar = true;
			}
			
			// dojo.publish("/dnd/start", [source, nodes, this.copy]);
			topic.publish("/dnd/start", source, nodes, this.copy);

			function stopEvent(e){
				e.preventDefault();
				e.stopPropagation();
			}

			this.events = [
				on(win.doc, touch.move, lang.hitch(this, "onMouseMove")),
				on(win.doc, touch.release,   lang.hitch(this, "onMouseUp")),
				on(win.doc, "keydown", lang.hitch(this, "onKeyDown")),
				on(win.doc, "keyup", lang.hitch(this, "onKeyUp")),

				// cancel text selection and text dragging
				on(win.doc, "dragstart", stopEvent),
				on(win.body(), "selectstart", stopEvent)
			];

			var c = "dojoDnd" + (copy ? "Copy" : "Move");
			dojo.addClass(dojo.body(), c);
		},
		
		canDrop: function(flag) {
			var canDropFlag = Boolean(this.target && flag);
			if (this.canDropFlag != canDropFlag) {
				this.canDropFlag = canDropFlag;
				
				if (!this.source.noAvatar) {
					this.avatar.update();
				}
			}
		},
		
		stopDrag: function() {
			dojo.removeClass(dojo.body(), ["dojoDndCopy", "dojoDndMove"]);
			dojo.forEach(this.events, function(evt){evt.remove();});
			this.events = [];
			
			if (this.avatar && this.avatar.destroy) {
				this.avatar.destroy();
			}
			
			this.avatar = null;
			this.source = this.target = null;
			this.nodes = [];
		},
		
		updateAvatar: function() {
			if (!this.source.noAvatar) {
				this.avatar.update();
			}
		},
		
		onMouseMove: function(e) {
			var a = this.avatar;
			if (a) {
				//dojo.dnd.autoScrollNodes(e);
				// dojo.dnd.autoscroll.autoScrollNodes(e);
				if (!this.source.noAvatar && !this.avatar.dontMove) {
					var s = a.node.style;
					s.left = (e.pageX + this.OFFSET_X) + "px";
					s.top = (e.pageY + this.OFFSET_Y) + "px";
				}
				
				var copy = Boolean(this.source.copyState(dojo.isCopyKey(e)));
				if (this.copy != copy) {
					this._setCopyStatus(copy);
				}
			}
		},
		
		onMouseUp: function(e) {
			if (this.avatar){
				if (this.source.getItem() && this.target && this.canDropFlag) {
					var copy = Boolean(this.source.copyState(dojo.isCopyKey(e))),
						params = [this.source, this.nodes, copy, this.target, e];
					
					dojo.publish("/dnd/drop/before", params);
					dojo.publish("/dnd/drop", params);
				} else {
					dojo.publish("/dnd/cancel");
				}
				this.stopDrag();
			}
		},
		
		onKeyDown: function(e) {
			if (this.avatar) {
				switch (e.keyCode) {
					case dojo.keys.CTRL:
						var copy = Boolean(this.source.copyState(true));
						if(this.copy != copy){
							this._setCopyStatus(copy);
						}
						break;
					case dojo.keys.ESCAPE:
						dojo.publish("/dnd/cancel");
						this.stopDrag();
						break;
				}
			}
		},
		
		onKeyUp: function(e) {
			if (this.avatar && e.keyCode == dojo.keys.CTRL) {
				var copy = Boolean(this.source.copyState(false));
				if (this.copy != copy) {
					this._setCopyStatus(copy);
				}
			}
		},
		
		_setCopyStatus: function(copy) {
			this.copy = copy;
			this.source._markDndStatus(this.copy);
			
			if (!this.source.noAvatar) {
				this.updateAvatar();
			}
			
			dojo.replaceClass(dojo.body(),
				"dojoDnd" + (this.copy ? "Copy" : "Move"),
				"dojoDnd" + (this.copy ? "Move" : "Copy"));
		},
		
		makeAvatar: function () {
			if (this.source.avatarClass) {
				return new (this.source.avatarClass)(this);
			} else {
				return new (dojo.dnd.Avatar)(this);
			}
		}
	});
	
	dojo.declare("dojo.dnd.HiSortDragNone", [dojo.dnd.Avatar], {
		type: 'task',
		
		constructor: function (manager) {
			//Create fake element, can it be avoided?
			this.node = null;
			var ul = dojo.create('ul', {'style': 'display: none;'});
			var li = dojo.create('li', {}, ul);
			
			this.node = ul;
		},
		
		update: function () {}
	});
});
/*
define(function () {
	
	dojo.declare("dojo.dnd.Manager", [dojo.dnd.Manager], {
	
		overSource: function(source) {
			if (this.avatar) {
				this.target = (source && source.targetState != "Disabled") ? source : null;
				this.canDropFlag = Boolean(this.target);
				if (!this.source.noAvatar) {
					this.avatar.update();
				}
			}
			dojo.publish("/dnd/source/over", [source]);
		},
		
		outSource: function(source) {
			if (this.avatar) {
				if (this.target == source){
					this.target = null;
					this.canDropFlag = false;
					
					if (!this.source.noAvatar) {
						this.avatar.update();
					}
					
					dojo.publish("/dnd/source/over", [null]);
				}
			} else {
				dojo.publish("/dnd/source/over", [null]);
			}
		},
		
		startDrag: function(source, nodes, copy) {
			this.source = source;
			this.nodes  = nodes;
			this.copy   = Boolean(copy); // normalizing to true boolean
			
			if (!this.source.noAvatar) {
				this.avatar = this.makeAvatar();
				dojo.body().appendChild(this.avatar.node);
			} else {
				this.avatar = true;
			}
			
			dojo.publish("/dnd/start", [source, nodes, this.copy]);
			this.events = [
				dojo.connect(dojo.doc, "onmousemove", this, "onMouseMove"),
				dojo.connect(dojo.doc, "onmouseup",   this, "onMouseUp"),
				dojo.connect(dojo.doc, "onkeydown",   this, "onKeyDown"),
				dojo.connect(dojo.doc, "onkeyup",     this, "onKeyUp"),
				// cancel text selection and text dragging
				dojo.connect(dojo.doc, "ondragstart",   dojo.stopEvent),
				dojo.connect(dojo.body(), "onselectstart", dojo.stopEvent)
			];
			var c = "dojoDnd" + (copy ? "Copy" : "Move");
			dojo.addClass(dojo.body(), c);
		},
		
		canDrop: function(flag) {
			var canDropFlag = Boolean(this.target && flag);
			if (this.canDropFlag != canDropFlag) {
				this.canDropFlag = canDropFlag;
				
				if (!this.source.noAvatar) {
					this.avatar.update();
				}
			}
		},
		
		stopDrag: function() {
			dojo.removeClass(dojo.body(), ["dojoDndCopy", "dojoDndMove"]);
			dojo.forEach(this.events, dojo.disconnect);
			this.events = [];
			
			if (this.avatar && this.avatar.destroy) {
				this.avatar.destroy();
			}
			
			this.avatar = null;
			this.source = this.target = null;
			this.nodes = [];
		},
		
		updateAvatar: function() {
			if (!this.source.noAvatar) {
				this.avatar.update();
			}
		},
		
		onMouseMove: function(e) {
			var a = this.avatar;
			if (a) {
				dojo.dnd.autoScrollNodes(e);
				
				if (!this.source.noAvatar && !this.avatar.dontMove) {
					var s=a.node.style;
					s.left=(e.pageX+this.OFFSET_X)+"px";
					s.top=(e.pageY+this.OFFSET_Y)+"px";
				}
				
				var copy = Boolean(this.source.copyState(dojo.isCopyKey(e)));
				if (this.copy != copy) {
					this._setCopyStatus(copy);
				}
			}
		},
		
		onMouseUp: function(e) {
			if (this.avatar){
				if (this.source.getItem() && this.target && this.canDropFlag) {
					var copy = Boolean(this.source.copyState(dojo.isCopyKey(e))),
						params = [this.source, this.nodes, copy, this.target, e];
					
					dojo.publish("/dnd/drop/before", params);
					dojo.publish("/dnd/drop", params);
				} else {
					dojo.publish("/dnd/cancel");
				}
				this.stopDrag();
			}
		},
		
		onKeyDown: function(e) {
			if (this.avatar) {
				switch (e.keyCode) {
					case dojo.keys.CTRL:
						var copy = Boolean(this.source.copyState(true));
						if(this.copy != copy){
							this._setCopyStatus(copy);
						}
						break;
					case dojo.keys.ESCAPE:
						dojo.publish("/dnd/cancel");
						this.stopDrag();
						break;
				}
			}
		},
		
		onKeyUp: function(e) {
			if (this.avatar && e.keyCode == dojo.keys.CTRL) {
				var copy = Boolean(this.source.copyState(false));
				if (this.copy != copy) {
					this._setCopyStatus(copy);
				}
			}
		},
		
		_setCopyStatus: function(copy) {
			this.copy = copy;
			this.source._markDndStatus(this.copy);
			
			if (!this.source.noAvatar) {
				this.updateAvatar();
			}
			
			dojo.replaceClass(dojo.body(),
				"dojoDnd" + (this.copy ? "Copy" : "Move"),
				"dojoDnd" + (this.copy ? "Move" : "Copy"));
		},
		
		makeAvatar: function () {
			if (this.source.avatarClass) {
				return new (this.source.avatarClass)(this);
			} else {
				return new (dojo.dnd.Avatar)(this);
			}
		}
	});
	
	dojo.declare("dojo.dnd.HiSortDragNone", [dojo.dnd.Avatar], {
		type: 'task',
		
		constructor: function (manager) {
			//Create fake element, can it be avoided?
			this.node = null
			var ul = dojo.create('ul', {'style': 'display: none;'});
			var li = dojo.create('li', {}, ul);
			
			this.node = ul;
		},
		
		update: function () {}
	});

	return dojo.dnd.Manager;
});
*/