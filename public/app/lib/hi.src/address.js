//dojo.provide("hi.address");

/**
 * Copyright (C) 2005-2006 Vide Infra Grupa SIA, Riga, Latvia. All rights reserved.
 * http://videinfra.com
 * Version 1.1
 */
define(function () {
	
	return dojo.global.Hi_Address = {
		base: false,
		params: false,
		explain: function(s) {
			if (Hi_Address.base) return;
			s = s || location.href;
			var b, q;
			var p = s.indexOf('#');
			if (p == -1) {
				b = s;
				q = '';
			} else {
				b = s.substr(0, p);
				q = s.substr(p + 1);
			}
			q = q.split('&');
			var i, imax = q.length, o = {};
			for (i = 0; i < imax; i++) {
				p = q[i].indexOf('=');
				if (p > 0) {
					o[decodeURIComponent(q[i].substr(0, p))] = decodeURIComponent(q[i].substr(p + 1));
				}
			}
			Hi_Address.base = b;
			Hi_Address.params = o;
			return;
		},
		set: function(k, v, silent) {
			silent = silent || false;
			Hi_Address.explain();
			var o = Hi_Address.params;
			if (k in o && o[k] == v) return;
			o[k] = v;
			Hi_Address.params = o;
			if (silent) return;
			Hi_Address.go();
			return;
		},
		remove: function(k) {
			Hi_Address.explain();
			var o = Hi_Address.params;
			if (k in o) {
				delete o[k];
				Hi_Address.params = o;
				Hi_Address.go();
				return;
			}
		},
		removeAll: function(k) {
			Hi_Address.explain();
			Hi_Address.params = {'_': ''};
			Hi_Address.go();
		},
		get: function(k, d) {
			k = k || false;
			d = d || '';
			Hi_Address.explain();
			var o = Hi_Address.params;
			if (k) {
				if (k in o) {
					return o[k];
				} else {
					return d;
				}
			} else {
				return o;
			}
		},
		go: function() {
			var o = Hi_Address.params;
			var i;
			Hi_Address.explain();
			var s = Hi_Address.base;
			s += '#';
			var f = false;
			for (i in o) {
				s += (f ? '&' : '') + encodeURIComponent(i) + '=' + encodeURIComponent(o[i]);
				f = true;
			}
			location.href = s;
		}
	};
	
});