package utils;

import java.util.HashMap;
import java.util.Map;

import oauth.FacebookOAuth2Client;
import oauth.GoogleOAuth2Client;
import oauth.OAuth2Client;

import org.apache.commons.lang.StringUtils;

import play.Play;

public class OAuth2Endpoints {
	private static final String PROVIDER_NAME_GOOGLE = "google";
	private static final String PROVIDER_NAME_FACEBOOK = "facebook";
	
	private static final String[] providerNames = {PROVIDER_NAME_GOOGLE, PROVIDER_NAME_FACEBOOK};
	
	public static OAuth2Client[] getClients() {
		Map<String, OAuth2Client> clients = getClientsMap();
		return clients.values().toArray(new OAuth2Client[clients.size()]);
	}
	
	private static Map<String, OAuth2Client> getClientsMap() {
		Map<String, OAuth2Client> clients = new HashMap<String, OAuth2Client>();
		for (String providerName : providerNames) {
			String id = Play.configuration.getProperty("social." + providerName + ".id");
			String secret = Play.configuration.getProperty("social." + providerName + ".secret");
			String authUrl = Play.configuration.getProperty("social." + providerName + ".auth_url");
			String tokenUrl = Play.configuration.getProperty("social." + providerName + ".token_url");
			String icon = Play.configuration.getProperty("social." + providerName + ".icon");
			String buttonClass = Play.configuration.getProperty("social." + providerName + ".buttonClass");
			
			if (StringUtils.isNotBlank(id) && StringUtils.isNotBlank(secret) && StringUtils.isNotBlank(authUrl) && StringUtils.isNotBlank(tokenUrl)) {
				clients.put(providerName, createClient(authUrl, tokenUrl, id, secret, providerName, icon, buttonClass));
			}
		}
		
		return clients;
	}
	
	public static OAuth2Client getClient(String providerName) {
		return getClientsMap().get(providerName);
	}
	
	private static OAuth2Client createClient(String authUrl, String tokenUrl, String id, String secret, String providerName, String icon, String buttonClass) {
		if (PROVIDER_NAME_GOOGLE.equals(providerName)) {
			return new GoogleOAuth2Client(authUrl, tokenUrl, id, secret, providerName, icon, buttonClass);
		} else if (PROVIDER_NAME_FACEBOOK.equals(providerName)) {
			return new FacebookOAuth2Client(authUrl, tokenUrl, id, secret, providerName, icon, buttonClass);
		} else {
			return new OAuth2Client(authUrl, tokenUrl, id, secret, providerName);
		}
	}
}
