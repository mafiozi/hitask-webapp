/**
 * Specifies tooltip on Project drop target
 */
//dojo.provide("hi.dnd.target.project");
define(function () {
	
	dojo.declare("dojo.dnd.HiDragTargetProject", [dojo.dnd.Source], {
		isSource: false,
		selfAccept: false,
		accept: ["dragged_task"],
		dropValue: null,

		permissionsAllowed: [Hi_Permissions.MODIFY, Hi_Permissions.EVERYTHING],

		action: 'modify',

		/* #2535 via https://support.sitepen.com/issues/22098 */
		onSelectStart:function(e) {return;},
		onMouseOut: function (e) {
			dojo.dnd.HiDragTargetProject.superclass.onMouseOut.call(this, e);
			
			//if (this.isDragging && this.targetState == "Disabled") { return; } 
			if (this.isDragging) {
				var m = dojo.dnd.Manager.manager();
				/* #2366 */
				if (!this.dropValue) {
					dojo.dnd.HiSortDragExtra.groupChange = null;
				}
				
				//JDT var m = dojo.dnd.Manager();
				
				if (m.source instanceof dojo.dnd.HiDragSource) {
					if (dojo.isIE && dojo.isIE < 9) {	//Fix text constantly showing/hiding
						if (e.target && e.relatedTarget) {
							var t1 = dojo.dom.getAncestorsByTag(e.target, 'UL', true);
							var t2 = dojo.dom.getAncestorsByTag(e.relatedTarget, 'UL', true);
							
							if (t1 == t2) return;
						} else {
							return;
						}
					}
					
					m.avatar.updateText('');
					this.dropValue = false;
				}
			}
		},

		onMouseOver: function (e) {
			dojo.dnd.HiDragTargetProject.superclass.onMouseOver.call(this, e);
			
			if (this.isDragging) {
				
				//JDT var m = dojo.dnd.Manager();
				var m = dojo.dnd.Manager.manager();
				if (m.source instanceof dojo.dnd.HiDragSource && m.source.dragNode) {
					var txt = '';
					var dragId = m.source.dragNode.dragId;
					
					if (dojo.isIE && dojo.isIE < 9) {	//Fix text constantly showing/hiding
						if (e.target && e.relatedTarget) {
							var t1 = dojo.dom.getAncestorsByTag(e.target, 'UL', true);
							var t2 = dojo.dom.getAncestorsByTag(e.relatedTarget, 'UL', true);
							
							if (t1 == t2) return;
						}
					}
					
					// #5015
					var parentEls = dojo.query(e.relatedTarget).parents('li.project');
					dojo.dnd.HiSortDragExtra.pEl = parentEls && parentEls.length ? parentEls[0] : null;
					if (!dojo.dnd.HiSortDragExtra.pEl && e.relatedTarget.tagName == 'LI' && dojo.hasClass(e.relatedTarget, 'project')) {
						dojo.dnd.HiSortDragExtra.pEl = e.relatedTarget;
					}
					
					var group = Hi_Preferences.get('currentTab', 'my', 'string');
					if (group === 'my' || group === 'date' || group === 'color' || group === 'project' || group === 'team' || group === 'today') {
						this.getProjectCoordinates();
						
						var b = dojo.position(dojo.byId(Project.getContainer('tasksList')), true),
							x = e.pageX - b.x,
							y = e.pageY - b.y,
							c = HiDragSourceCached.project,
							j, jmax = c.length, d, old_parent,
							ungroup = 'Unsort',
							item = hi.items.manager.get(dragId),
							type = item.category;
						
						switch (group) {
							case 'my':
								this.text = hi.i18n.getLocalization('center.mark_starred') + ' ';
								ungroup = '';
								// ungroup = hi.i18n.getLocalization('center.mark_unstarred');
								/* #2366 old_parent = item.starred; */
								old_parent = item.starred?1:0;
								break;
							case 'today':
								// this.text = hi.i18n.getLocalization('center.mark_starred') + ' ';
								this.text = hi.i18n.getLocalization('center.make_start') + ' ';
								ungroup = '';
								// https://red.hitask.net/issues/5555#note-4
								if (type === hi.widget.TaskItem.CATEGORY_NOTE || type === hi.widget.TaskItem.CATEGORY_FILE) return;
								// ungroup = hi.i18n.getLocalization('center.mark_unstarred');
								/* #2366 old_parent = item.starred; */
								// old_parent = item.starred?1:0;
								old_parent = 0;
								break;
							case 'project':
								this.text = hi.i18n.getLocalization('center.move_to_project') + ' ';
								ungroup = hi.i18n.getLocalization('center.move_out_of_project');
								old_parent = item.parent;
								break;
							case 'color':
								this.text = hi.i18n.getLocalization('center.change_color');
								ungroup = hi.i18n.getLocalization('center.remove_color');
								old_parent = item.color;
								break;
							case 'date':
								this.text = hi.i18n.getLocalization('center.make_start') + ' ';
								old_parent = item.start_date;
								type = item.category;
								/* #2928 - do not allow Dnd in Date tab for recurring items */
								if (item.recurring) return;
								// #2928 - as per version 9, do not allow items spanning multiple days to be dragged and dropped in Date tab
								if (hi.items.date.itemSpansMultipleDays(item)) return;
								/* #3059 Note and file can't be d&d to the calendar and set the time*/
								if (type === hi.widget.TaskItem.CATEGORY_NOTE || type === hi.widget.TaskItem.CATEGORY_FILE) return;
								/* #2634
								if (type == 1 || type == 2) {
									var end_date = item.end_date;
									if (end_date != '' && old_parent != end_date) return;
								}
								*/
								if (old_parent == time2str('now', Hi_Calendar.format)) {
									old_parent = 0;
								} else if (old_parent == time2str('tomorrow', Hi_Calendar.format)) {
									old_parent = 1;
								} else {
									old_parent = -1;
								}
								break;
							case 'team':
								this.text = hi.i18n.getLocalization('center.assign_to') + ' ';
								ungroup = hi.i18n.getLocalization('center.unassign') + ' ';
								old_parent = item.assignee;
								break;
						}
						old_parent = parseInt(old_parent);
						if (!isFinite(old_parent)) {
							old_parent = 0;
						}
						for (j = 0; j < jmax; j++) {
							d = c[j];
							if (d[0].x <= x && d[0].y <= y && d[0].x + d[1].w > x && d[0].y + d[1].h > y) {
								if (old_parent != d[3]) {
									if (group != 'team' || d[3] == 0 || Project.canAssign(dragId, d[3])) {
										this.dropValue = d[3];
										/* #2366 var txt = this.dropValue ? this.text + d[2] : ungroup; */
										txt = ungroup;
										dojo.dnd.HiSortDragExtra.groupChange = true;
										if (this.dropValue) {
											txt = this.text + d[2];
											dojo.dnd.HiSortDragExtra.groupChange = true;
										}
									}
								}
								break;
							}
						}
					}

					m.avatar.updateText(txt);
				}
			}
		},
		
		getProjectCoordinates: function () {
			// summary:
			//		Collect project positions, titles, etc.
			
			if (!HiDragSourceCached.project) {
				HiDragSourceCached.project = [];
				var group = Hi_Preferences.get('currentTab', 'my', 'string');
				var c = dojo.byId(Project.getContainer('tasksList'));
				var b = dojo.position(c, true);
				var d = dojo.query('li.project', c);
				var i;
				var j = 0;
				
				var lists = hi.items.manager.getTaskLists(),
					imax = lists.length,
					projectGroup = null,
					projectId = null,
					projectName = null;
				
				for (i = 0; i < imax; i++) {
					el = lists[i].domNode;
					projectGroup = lists[i].container;
					projectName = "";
					
					if (projectGroup) {
						el = projectGroup.domNode;
						projectId = "teamMemberId" in projectGroup ? projectGroup.teamMemberId : projectGroup.projectId;
						
						if (projectId == "completed") {
							//Can't drop on completed list
							continue;
						} else {
							projectName = projectGroup.moveTitle || projectGroup.title;
							if (group == 'date') { // Date
								//Only allow drop on today and tomorrow
								if (projectId != "today" && projectId != "tomorrow") continue;
							} else if (group == 'team') { // Team
								if (projectId == 0) {
									projectId = Project.user_id;
								}
							}
						}
					} else {
						//If there is no projectGroup, then this is list for ungrouped tasks
						if (group == 'date') {
							//Can't drop on ungrouped date list
							continue;
						} else {
							projectId = 0;
							projectName = ' ';
						}
					}
					
					var p = dojo.position(el, true);
					p.x -= b.x;
					p.y -= b.y;
					HiDragSourceCached.project[j] = [p, dojo.coords(el), projectName, projectId];
					j++;
				}
			}
		},
		
		resetProjectCoordinates: function () {
			// summary:
			//		Reset project cache
			
			HiDragSourceCached.project = null;
		}
	});
	
	return dojo.dnd.HiDragTargetProject;
});