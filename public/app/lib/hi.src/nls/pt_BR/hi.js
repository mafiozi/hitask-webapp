define(
//AUTO GENERATED FILE. DO NOT MODIFY.
//If you need to add,update,modify messages: Update /languages/[locale]/js.properties and run "ant generate-js-localization" to build this file.
//begin v1.x content
({
  "team_list": {
    "myself": "eu mesmo",
    "add_person": "Nova pessoa..."
  },
  "color": {
    "red": "vermelho",
    "orange": "laranja",
    "green": "verde",
    "gray": "cinza",
    "blue": "azul",
    "yellow": "amarelo",
    "purple": "rosa",
    "none": "sem cor"
  },
  "friendfind": {
    "unconfirmed_email": "Por favor, confirme seu endereço de e-mail",
    "in_business": "Esta pessoa já está em um time"
  },
  "tooltip": {
    "delete_message_task_4": "Excluir nota?",
    "delete_message_task_5": "Excluir arquivo?",
    "uncomplete_confirmation": "Restaurar?",
    "delete_message_task_2": "Excluir evento?",
    "delete_message_task_1": "Excluir tarefa?",
    "delete_comment": "Excluir comentário?",
    "complete_confirmation": "Completar?",
    "delete_completed_items": "Excluir itens concluídos?",
    "archive_completed_items": "Arquivar itens concluídos?"
  },
  "project": {
    "cancel": "Cancelar",
    "make_task_attach": "Anexo a este item",
    "reports": "Relatórios",
    "drag_to_sort": "Arraste e solte para ordenar",
    "move_out_confirmation": "Remover desde item?",
    "none": "nenhum",
    "color_orange": "laranja",
    "click_to_add_task": "Clique para adicionar tarefa",
    "click_to_open_project": "Clique para expandir/recolher projeto",
    "click_to_complete": "Clique para completar/reabrir tafefa",
    "click_to_view": "Clique para ver",
    "color_red": "vermelho",
    "item": "Item",
    "edit": "Editar",
    "color_yellow": "amarelo",
    "project_title": "Nome do projeto",
    "duplicate": "Duplicar...",
    "done": "Concluído %d",
    "private_project": "Projeto privado",
    "project_description": "Digite a descrição do projeto aqui...",
    "delete_confirmation": "Este item pertence à outro membro da equipe. Você tem certeza?",
    "click_to_edit": "Clique para editar",
    "daily": "diariamente",
    "move_task_confirmation": "Fazer esta uma tarefa secundária?",
    "monthly": "mensalmente",
    "add_new_person": "Adicionar novo usuário",
    "share_project": "Compartilhar com o time",
    "yearly": "anualmente",
    "create_new_project": "Criar novo projeto...",
    "items": "Itens",
    "private": "Privado",
    "save": "Salvar",
    "color_none": "sem cor",
    "delete": "Remover, arquivar...",
    "weekly": "semanalmente",
    "move_attach_confirmation": "Anexar à este item?",
    "share_project_only": "Compartilhar apenas o projeto",
    "unshare_project": "Tornar privado",
    "enter_item_name": "Inserir nome do item",
    "share": "Compartilhar...",
    "move_invalid_priority": "Para reordenar os itens, por favor, mudar para a classificação por Prioridade",
    "color_blue": "azul",
    "delete_project": "Deletar projeto e tarefas",
    "delete_project_confirmation": "Deletar projeto?",
    "color_gray": "cinza",
    "color_purple": "roxo",
    "color_green": "verde",
    "make_task_nested": "Tornar dependente de outro item",
    "share_project_and_tasks": "Compartilhar projetos e suas tarefas",
    "move_task_out": "Sair do item",
    "click_specific_day_to_complete": "To complete recurring task, please complete it in Calendar by clicking the checkbox on that task for specific day.",
    "move_to_archive": "Mover para Arquivo",
    "duplicate_project": "Duplicar projeto e tarefas",
    "delete_but_keep_tasks": "Deletar projetos, manter tarefas",
    "create_new": "Adicionar novo item",
    "move_project_invalid_priority": "To reorder projects, please switch to sorting by Priority",
    "double_click_to_edit": "Double click to edit"
  },
  "main": {
    "no": "Não",
    "send_back_to": "Enviado de volta para",
    "assigned_to_me": "Atribuído para mim",
    "deleted_by": "excluído por",
    "from": "De",
    "returned_to": "volta a",
    "assigned_to": "Atribuído para"
  },
  "trial": {
    "warning1": "Seu período de teste expirou.",
    "warning": "Obrigado por testar nosso aplicativo! Seu período de testes expira em \u003cstrong\u003e%d\u003c/strong\u003e dias."
  },
  "notification": {
    "project_was_created": "Projeto foi criado",
    "notification_enable": "Habilitar notificações no navegador",
    "task_modified": "Item foi modificado",
    "task_modified_starred": "Item iniciado com status modificado",
    "project_restore": "Projeto foi restaurado",
    "project_was_deleted": "Projeto foi deletado",
    "project_was_updated": "Projeto foi atualizado",
    "task_uncompleted": "Item foi restaurado",
    "task_modified_color": "Cor do item foi modificada",
    "team_member_business_deleted": "Membro do time foi removido",
    "task_copy_and_restore": "Cópia do item foi criada",
    "task_modified_date": "Data do item foi modificada",
    "notification_disable": "Desabilitar notificações no navegador",
    "task_deleted": "Item foi excluído",
    "task_modified_project": "Item foi movido para o projeto",
    "task_restore": "Item foi restaurado",
    "project_copy_restore": "Cópia do projeto foi criada",
    "team_member_deleted": "Membro do time removido",
    "task_created": "Item foi criado",
    "logout_confirmation": "Existem operações incompletas. Tem certeza que deseja sair do hiTask?",
    "team_member_added": "Membro adicionado ao time",
    "team_member_invited": "Membro convidado para o time",
    "task_completed": "Item foi marcado como concluído",
    "task_duplicate": "Item foi duplicado",
    "task_assigned": "Item foi atribuído",
    "task_archive": "Item foi movido para o arquivo"
  },
  "no_permissions_to_delete": {
    "not_item_owner_or_administrator": "Você deve ser proprietário desse item ou o administrador da equipe, a fim de eliminar este item!",
    "not_project_owner_or_administrator": "Você deve ser proprietário desse projeto ou administrador de equipe, para eliminar este item!"
  },
  "no_permissions_to_archive": {
    "not_shared_or_assignee": "Você deve ser proprietário/cessionário/participante deste item ou este item deve ser compartilhada, para arquivá-lo!"
  },
  "common": {
    "add": "Adicionar",
    "logTimeAction": "Log de tempo para este item",
    "behind_proxy": "Parece que você está utilizando proxy. Alguns recursos podem não funcionar (Tabs, atividades, etc.) enquanto estiver conectado à Internet via proxy.",
    "start_upload": "Enviar arquivo",
    "duplicateAction": "duplicar item",
    "text_formatting_help": "ajuda de formatação de texto",
    "attachAction": "anexar arquivo a este item",
    "deleteAction": "excluir este item",
    "not_permitted_warning": "Você não tem permissão para ${0}. Você deve perguntar para ${1} ${2} para lhe conceder permissão a ${3}.",
    "addSubAction": "Adicionar subtarefa para este item",
    "assignAction": "Atribuir este item",
    "archiveAction": "arquivar este item",
    "completeAction": "completar este item",
    "modifyAction": "Modificar este item",
    "makeStarredAction": "marcar como iniciado"
  },
  "permissions": {
    "title_50": "Completar, Atribuir",
    "title_60": "Modificar",
    "title_100": "Tudo",
    "title_20": "Visualizar, Comentar"
  },
  "unload": {
    "warning": "Tem certeza que deseja sair desta página?"
  },
  "tag": {
    "add_no_more_tags": "Nenhuma tag para escolher",
    "add_title": "Clique para adicionar o item com %tag"
  },
  "taskfilter": {
    "status_my_created": "Mostrar meus itens e tarefas compartilhadas criadas por mim",
    "status_my": "Mostrando itens privados e atribuído a mim",
    "status_my_unassigned": "Mostrar meus itens e tarefas compartilhadas não atrbuídos"
  },
  "no_permissions": {
    "no_permissions_to_modify": "Desculpe, você não tem permissões para modificar este item."
  },
  "calendar": {
    "next_year": "Ir para o ano seguinte",
    "change_year": "Mudar o ano",
    "tomorrow": "Amanhã",
    "next_month": "Ir para o próximo mês",
    "next_week": "Próxima semana",
    "previos_month": "Ir para o mês anterior",
    "yesterday": "Ontem",
    "this_month": "Ir para este mês",
    "previos_year": "Ir para o ano anterior",
    "today": "Hoje",
    "this_yeay": "Ir para este ano",
    "this_date": "Ir para esta data",
    "all_day_events": "Eventos de dias inteiros"
  },
  "no_permissions_to_edit": {
    "not_shared_or_assignee": "Você deve ser proprietário/cessionário/participante deste item ou este item deve ser compartilhada, para editá-lo!",
    "no_permission_to_set_as_private": "Projeto pertence a outro membro da equipe. Somente um administrador ou proprietário pode fazer este projecto privado!"
  },
  "center": {
    "move_to_project": "Mover para projeto",
    "items_not_in_project": "itens não estão no projeto...",
    "remove_color": "Remover cor",
    "assign_to": "Designar para",
    "after_week": "Próxima semana",
    "move_out_of_project": "Mover para fora do projeto",
    "completed_items_count": "%s itens completos...",
    "loading_items": "Carregando %s itens...",
    "due_on": "Due on",
    "completed_items": "Itens completados: ",
    "overdue": "Tarefas vencidas",
    "starred": "Estrelado",
    "loading_item": "Carregando item...",
    "today": "Hoje",
    "mark_starred": "Marcar",
    "from": "de",
    "make_start": "Começar",
    "change_color": "Mudar cor",
    "start_on": "Começar em",
    "completed_item_count": "%s item completo...",
    "item_not_assigned": "item não designado...",
    "drag_items_here": "Arraste e solte aqui",
    "tomorrow": "Amanhã",
    "item_assigned_to": "Item associado para",
    "during_week": "Próximos 7 dias",
    "no_items": "Sem itens",
    "mark_unstarred": "Desmarcar estrelado",
    "unassign": "Remover associação",
    "item_received_from": "Item associado de",
    "daily": "Diariamente",
    "completed_item": "Item completado: ",
    "before_today": "Ontem",
    "to": "para",
    "item_not_in_project": "item não está no projeto...",
    "items_not_assigned": "itens não designados..."
  },
  "display": {
    "weekday_0": "Sunday",
    "weekday_2": "Tuesday",
    "weekday_1": "Monday",
    "weekday_4": "Thursday",
    "weekday_3": "Wednesday",
    "weekday_6": "Saturday",
    "weekday_5": "Friday"
  },
  "completed": {
    "archiveLinkTitle": "Clique para arquivar itens completados",
    "removeLinkTitle": "Clique para remover itens completados",
    "submenuRemoveLink": "Apagar completado",
    "could_not_archive": "Algumas tarefas concluídas talvez não poderão ser arquivas porque você não tem permissão para arquivar estes itens.",
    "archiveLink": "Arquivar",
    "removeLink": "Remover",
    "title": "Completado",
    "submenuArchiveLink": "Arquivo completo",
    "removeLinkText": "Itens completados"
  },
  "team": {
    "deleted_by": "deletado por",
    "are_you_sure": "Você tem certeza?"
  },
  "history": {
    "week": "semana",
    "weeks": "semanas",
    "color": "Cor",
    "year": "ano",
    "ago": "atrás",
    "description": "Descrição",
    "title": "Título",
    "years": "anos",
    "enabled": "Ativado",
    "second": "segundo",
    "seconds": "segundos",
    "hour": "hora",
    "disabled": "Desativado",
    "tag": "Tags",
    "time_track": "Controle de tempo",
    "day": "dia",
    "hours": "horas",
    "months": "meses",
    "reminder": "Lembrete",
    "recurring": "Retorno",
    "time_est": "Tempo estimado",
    "minutes": "minutos",
    "end_time": "Hora final",
    "no_value": "sem valor",
    "minute": "minuto",
    "start_time": "Hora de início",
    "month": "mês",
    "days": "dias",
    "comment": "Seu comentário aqui..."
  },
  "priority": {
    "high": "alta",
    "low": "baixa",
    "change_priority": "Alterar prioridade para",
    "medium": "média",
    "increase": "Aumentar prioridade",
    "decrease": "Diminuir prioridade"
  },
  "sync": {
    "init_targets_retrieving": "Recuperando calendários da sua conta do Google..."
  },
  "task": {
    "date": "Date",
    "file_delete": "Excluir",
    "ago": "atrás",
    "add_comment": "Adicionar",
    "project": "Projeto",
    "or_cancel": "ou",
    "none": "Nenhuma",
    "start_time_help": "Hora de início",
    "type": "Tipo",
    "end_date_help": "Data final",
    "recurring_none": "Não",
    "heading_sharing": "Compartilhamento",
    "create_tasks_button": "Criar tarefas",
    "time_tracking_details": "Detalhes...",
    "time_tracking_hours": "horas",
    "message_max_size": "A descrição não deve exceder %d caracteres! Texto foi truncado.",
    "starts": "Começa",
    "last_modified": "Última modificação:",
    "click_to_add_project": "Clique para adicionar projeto",
    "remember_other_sharing": "lembrar para novos itens",
    "add_item": "Subtarefas",
    "many_tasks_format": "Formato separados por vírgulas: título, data de início, data de término, descrição",
    "add_tags": "Adicionar tag",
    "time_tracking_today": "hoje",
    "delete_button": "Excluir",
    "time_tracking_estimated": "Estimado",
    "time_tracking_spent": "Tempo gasto",
    "recurring": "Repetir",
    "time_tracking_enable": "Permitir",
    "time_tracking_timer": "Hora de início",
    "archive": "Arquivo",
    "priority": "Prioridade",
    "loading": "Carregando...",
    "sharing": "Compartilhando:",
    "recurring_monthly": "Mensal",
    "tags": "Tags",
    "task": "Tarefa",
    "duplicate_button": "Duplicar",
    "time_tracking_track": "Registro de tempo",
    "birthday": "Aniversário",
    "message_empty": "Descrição do item aqui",
    "note": "Nota",
    "shared": "Compartilhado como time",
    "private": "Privado",
    "time_tracking_estimation": "Tempo estimado",
    "many_tasks_go_back": "Algo parece não estar certo? \u003ca href\u003d\"javascript://\" onclick\u003d\"NewItemManyTasks.goStepBack()\"\u003eClique aqui para voltar e alterar.\u003c/a\u003e",
    "color": "Cor",
    "assign_to": "Atribuído para:",
    "end_time_help": "Hora final",
    "description": "Descrição",
    "ungroup": "Minha visualização",
    "recurring_yearly": "Anual",
    "recurring_every": "Todo",
    "time_tracking_by": "por",
    "delete_recurring_message": "Deletar item recorrente de toda a serie ou apenas deste evento?",
    "start_date_help": "Data de início",
    "tag_add": "Adicionar",
    "before_start": "antes de iniciar",
    "choose_tag": "Escolher a partir de minhas tags",
    "recurring_end": "Repetição termina:",
    "file": "Arquivo",
    "overdue": "Atrasadas",
    "more_tags": "Mais tags",
    "ends": "Termina",
    "before_due_start": "before due date (if specified) or start",
    "event": "Evento",
    "recurring_weekly": "Semanal",
    "time_tracking": "Controle de tempo",
    "heading_assign": "Atribuído",
    "participants": "Participantes",
    "cancel_button": "Cancelar",
    "calendar": "Calendar",
    "reminder": "Lembrete",
    "assign_button": "Atribuir",
    "recurring_daily": "Diário",
    "title_max_size": "O título não pode exceder %d caracteres!",
    "many": "Muitas tarefas",
    "priority_medium": "Média",
    "priority_low": "Baixa",
    "click_to_set_color": "Clique para escolher uma cor para este item",
    "save_button": "Salvar minhas alterações",
    "many_tasks_description": "Cole arquivo CSV ou digite uma tarefa por linha ...",
    "modify": "Modificar",
    "time_tracking_save": "Salvar",
    "tag_click_help": "Clique para ver apenas itens marcados com TAG",
    "open_file": "Abrir arquivo em nova janela",
    "due": "Due",
    "report": "Relatório",
    "progress": "Progresso",
    "proceed_button": "Proceguir",
    "activity_feed": "Activity",
    "is_all_day": "Dia todo",
    "priority_high": "Alta",
    "user": "Time",
    "show_history": "Mostrar todo histórico"
  },
  "properties": {
    "shared": "Compartilhado",
    "private": "Privado",
    "file_upload_over_quota": "Você utilizou todo armazenado disponível em sua conta. \u003ca href\u003d\"/subscribe\"\u003ePor favor faça um upgrade para aderir mais espaço!\u003c/a\u003e",
    "more_actions": "More Actions",
    "assigned_none": "nenhum",
    "short_datetime_daily": "Every day %time",
    "or_cancel": "ou",
    "datetime_monthly_interval": "Todo %interval meses em %date %time até %recurrence_end_date",
    "drag_file": "ou arraste e solte o arquivo aqui",
    "select_file": "Selecione um arquivo para upload",
    "datetime_daily": "Todo dia %time até %recurrence_end_date",
    "file_upload_error": "Erro ao enviar o arquivo, por favor tente novamente.",
    "file_upload_attach": "Arraste aqui os arquivos para anexar",
    "file_upload_drop": "Arraste aqui os arquivos para upload",
    "short_datetime_daily_interval": "Every %interval days %time",
    "short_datetime_weekly": "Every %day %time",
    "attach_file": "Anexar arquivo",
    "time_tracking_hours": "horas",
    "datetime_time": "na %time",
    "error_other": "Oops! Não podemos executar essa operação no momento. Por favor, tente novamente.",
    "datetime_yearly": "Anual na %date %month %time até %recurrence_end_date",
    "assigned_myself": "Eu mesmo",
    "attach_another_file": "Anexar outro arquivo",
    "error_timeout": "Houve um erro de conexão com hitask.com. Por favor, verifique sua conexão com a internet e tente novamente.",
    "cancel_button": "cancelar",
    "short_datetime_weekly_interval": "Every %interval weeks on %day %time",
    "short_datetime_time": "at %time",
    "datetime_weekly_interval": "Todo %interval semanas no %day %time até %recurrence_end_date",
    "short_datetime_monthly": "Every month on %date %time",
    "datetime_weekly": "Todo %day %time até %recurrence_end_date",
    "datetime_daily_interval": "Todo %interval dias %time até %recurrence_end_date",
    "file_upload_multiple": "Envio de %count arquivos",
    "datetime_monthly": "Mensal em %date %time até %recurrence_end_date",
    "file_upload_single": "Upload",
    "save_button": "Salvar",
    "short_datetime_yearly_interval": "Every %interval years on %date %month %time",
    "file_upload_failed": "Erro de upload",
    "datetime_yearly_interval": "Todo %interval anos em %date %month %time até %date %month %time",
    "short_datetime_yearly": "Every year on %date %month %time ",
    "short_datetime_monthly_interval": "Every %interval months on %date %time",
    "cancel_upload": "Cancelar",
    "copy_link": "Copy Link"
  },
  "staticTask": {
    "reminder_time": "Lembrete de tempo",
    "shared": "Compartilhado",
    "time_create": "Criado as",
    "recurring": "Recorrente",
    "time_est": "Tempo estimado",
    "false": "Falso",
    "time_spent_subitems": "Tempo gasto em sub itens",
    "completed": "Concluído",
    "title": "Título",
    "color_value": "Valor da cor",
    "time_spent": "Tempo gasto",
    "reminder_time_type": "Tipo de lembrete de tempo",
    "archived": "Arquivado",
    "starred": "Iniciado",
    "assigneeName": "Nome cessionário",
    "true": "Verdadeiro",
    "last_comment": "Último comentário",
    "recurring_end_date": "Data fim da recorrencia",
    "time_track": "Linha do tempo",
    "reminder_enabled": "Lembrete habilitado",
    "time_est_subitems": "Tempo estimado para sub itens",
    "category": "Categoria",
    "recurring_interval": "Intervalo na recorrência"
  }
})
//end v1.x content
);