/*
dojo.provide("hi.build-settings");
dojo.require("hi.settings");
*/

define([
	"hi/reminder",
	"hi/preferences",
	"hi/main",
	"hi/settings",
	'hi/widget/image/Cropper',
	"hi/widget/form/hiSharing/hiSharingWidget",
	"hi/comments"
], function () {
	
	return {};
	
});
