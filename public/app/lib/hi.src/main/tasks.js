//dojo.provide("hi.main.tasks");
define([
	"dojo/_base/lang",
	"dojo/date/locale",
	'hi/widget/task/item_DOM'
], function (lang, djDateLocale, item_DOM) {
	dojo.global.getTaskNode  = function(id) {
		if (dojo.hasClass(dojo.byId('tooltip_taskedit'), 'hidden')) {
			var taskNodeId = 'task_' + id; 
			if(Project.clickedTaskNode && Project.clickedTaskNode.id == taskNodeId){ 
				return Project.clickedTaskNode; 
			}else{ 
				return dojo.byId(taskNodeId); 
			}
		}
	
		var ul = dojo.byId('tooltip_tasks');
		var li = firstElement(ul);
	
		var li_id = parseIntRight(li.id);
		if (li_id == id) {
			return li;
		} else {
			return dojo.byId('task_' + id);
		}
	};
	
	dojo.global.highlightTask = function(id) {
		var el = dojo.query('#task_' + id + ' div.heading')[0];
		if (!el) return;
		
		var from = [255,252,159];
		var to = [245,245,245];
		var diff = [to[0] - from[0],to[1] - from[1],to[2] - from[2]];
		el.style.background = '#FFFC9F';

		dojo.anim(el, {}, 1000, function (n) { 
			var c = [~~(from[0]+diff[0]*n),~~(from[1]+diff[1]*n),~~(from[2]+diff[2]*n)];
			el.style.background = 'rgb(' + c.join(',') + ')';
		}, function () {
			if (dojo.isIE && dojo.isIE < 9) {
				el.removeAttribute('style');
			} else {
				el.style.background = '';
			}
		}, 1000);
	};
	
	dojo.global.completeList = [];
	dojo.global.completeTimer = null;
	
	/**
	 * Changes product tag of task
	 */
	dojo.global.updateTask = function(taskId, targetItem, saveObj, parentSave) {
		if (!taskId) return;
		if (!targetItem) return;
		if (!saveObj) saveObj = {};
		var taskItem = hi.items.manager.get(taskId);
		if (!taskItem) return;

		var group = Hi_Preferences.get('currentTab', 'my', 'string');
		var taskListGroup = null;

		switch (group) {
			case 'today':
			case 'my': // My view
				/* Get sub-item's parent, so we can reliably determine starred attribute
				 * But also make sure its parent is not a project, which may not be rendered 
				 */
				var targetItemParent = targetItem.parent;
				/* #2385 */
				var parentItem = hi.items.manager.get(targetItemParent);
				if (targetItem.parent == 0 /* #2385 */ || !parentItem || hi.items.manager.get(targetItemParent).category == hi.widget.TaskItem.CATEGORY_PROJECT) {
					taskListGroup = targetItem.container.container;
				} else {
					//taskListGroup = hi.items.manager.item(targetItem.parent).container.container;
					if (hi.items.TaskListUngrouped && hi.items.TaskListUngrouped.has(targetItem.taskId)) {
						taskListGroup = 0;
					} else {
						taskListGroup = 1;
					}
				}
				
				var value = taskListGroup? 1 : 0;
				var taskItemVal = taskItem.starred? 1 : 0;

				// Feature #5555 - no need to update starred in "my" view("today" view now)
				if (taskItemVal != value && group === 'my') {
					saveObj.starred = value;
					//hi.items.manager.setSave(taskItem.taskId, {"starred": value});
				}

				// #3787 If an items parent is changed then it means the parents
				// 'children' attribute needs to be changed
				if (saveObj.parent) {
					var parent = hi.items.manager.get({id: saveObj.parent});
					if (lang.isArray(parent) && parent.length > 0) {
						hi.items.manager.set(saveObj.parent, {children: ++parent[0].children});
					}
				}

				hi.items.manager.setSave(taskId, saveObj);
				item_DOM._save(taskId,saveObj);
				break;
			case 'date':
			case 'overdue':
				hi.items.manager.setSave(taskId,saveObj);
				item_DOM._save(taskId,saveObj);
				break;
			case 'color':
				/*
				 * Change color if task is not a child of another task, for that
				 * needs to be used updateChildTask method
				 */

				var currentColor = hi.items.manager.get(taskId).color;
				var newColor = 0;
				/*
				if (!targetItem.container.container) newColor = 0;
				else newColor = parseInt(targetItem.container.container.groupClassName.replace(/color/,''));
				*/
				for (var i = 0;i<hi.items.TaskListColor.length;i++)
				{
					if (hi.items.TaskListColor[i].has(targetItem.taskId))
					{
						newColor = parseInt(hi.items.TaskListColor[i].container.groupClassName.replace(/color/,''));
					}
				}
				if (newColor != currentColor) saveObj.color = newColor;
				hi.items.manager.setSave(taskId,saveObj);
				item_DOM._save(taskId,saveObj);
				break;
			case 'project': // Projects

				hi.items.manager.setSave(taskId,saveObj);
				item_DOM._save(taskId,saveObj);
				break;
			case 'team': // Team
				hi.items.manager.setSave(taskId,saveObj);
				item_DOM._save(taskId,saveObj);
				break;
		}
	};
	
	dojo.global.getEventDateByNode  = function(node) {
	
		var parent = getParentByClass(node, 'task_li');
		if (parent) {
			var inp = parent.getElementsByTagName('INPUT');
			for(var i=0,j=inp.length; i<j; i++) {
				if (inp[i].getAttribute('type') == 'checkbox') {
					return inp[i].getAttribute('eventdate') || '';
				}
			}
		}
		
		return null;
	};
	
	/* #2791 */
	dojo.global.getCurrentEventDateAttr = function(id,instance_date)
	{
		instance_date = hi.items.date._getEventInstanceDate(id, instance_date);
		return time2str(instance_date,'y-m-d');
	};
	
	/* Replaces getCurrentEventDateAttr
	 * - to handle recurring_interval property
	 */
	dojo.global._getCurrentEventDateAttr = function(id,instance_date) {
		var nearest_recurrence_date = hi.items.date._getNextRecurrenceDate(id,instance_date);
		return time2str(nearest_recurrence_date,'y-m-d');
	};
	
	dojo.global.getTaskInstanceDateTitle  = function (id, instance_date, list) {
		var dateEnd = hi.items.date.getEventEndTime(id, '23:59');
		var dateStart = hi.items.date.getEventStartTime(id, '23:59');
		var recurring = parseInt(hi.items.manager.get(id, 'recurring') || 0), result;

		if (recurring) {
			/* #2493 instance_date = hi.items.date.getEventInstanceDate(id, instance_date);*/
			/*
			 * Account for recurrence interval
			 * instance_date = hi.items.date._getEventInstanceDate(id, instance_date);
			 */
			if (!instance_date) {
				instance_date = hi.items.date._getNextRecurrenceDate(id, instance_date);
			} else {
				instance_date = str2timeAPI(instance_date);
			}
			if (instance_date) {
				dateStart = instance_date;
				if (dateEnd) {
					dateEnd = instance_date;
				} else {
					dateStart = instance_date;
				}
			}
		}
		result = this.getTaskDateTitleByDates(id, dateStart, dateEnd, list);
		var todaylabel = hi.i18n.getLocalization('calendar.today');
		if (result !== todaylabel) {
			result = dojo.global.getRecurringTimeLabel(id);
		}
		return result;
	};

	dojo.global.getRecurringTimeLabel = function(id) {
		// summary:
		//		Returns date and time as string
		
		var time = "",
			date_str = "",
			item = him.get(id);

		if (!item) {
			console.error('item with id', id, 'not found');
		}
		
		if (item.start_date) {
			time = item.start_date;

			if (item.recurring) {
				var interval = item.recurring_interval != 1 ? '_interval' : '';
				switch(item.recurring) {
					case 4:
						date_str = hi.i18n.getLocalization("properties.short_datetime_yearly" + interval);
						break;
					case 3:
						date_str = hi.i18n.getLocalization("properties.short_datetime_monthly" + interval);
						break;
					case 2:
						date_str = hi.i18n.getLocalization("properties.short_datetime_weekly" + interval);
						break;
					case 1:
						date_str = hi.i18n.getLocalization("properties.short_datetime_daily" + interval);
						break;
				}
				
				if (item.recurring_interval != 1) {
					date_str = date_str.replace(/\%interval/,item.recurring_interval);
				}
				
				time = date_str;
			}
		}
		
		return time;
	},

	dojo.global.getRecurringTimeLabel = function(id) {
	// getTimeString: function() {
		// summary:
		//		Returns date and time as string
		
		var time = "",
			date_str = "",
			item = him.get(id);

		if (item.start_date) {
			time = item.start_date;

			if (item.recurring) {
				var interval = item.recurring_interval != 1 ? '_interval' : '';
				switch(item.recurring) {
					case 4:
						date_str = hi.i18n.getLocalization("properties.short_datetime_yearly" + interval); break;
					case 3:
						date_str = hi.i18n.getLocalization("properties.short_datetime_monthly" + interval); break;
					case 2:
						date_str = hi.i18n.getLocalization("properties.short_datetime_weekly" + interval); break;
					case 1:
						date_str = hi.i18n.getLocalization("properties.short_datetime_daily" + interval);
						break;
				}
				
				if (item.recurring_interval != 1) {
					date_str = date_str.replace(/\%interval/, item.recurring_interval);
				}
				
				var start_date = str2time(item.start_date) || new Date(item.start_date),
					str_date = start_date.getDate(),
					str_month = HiTaskCalendar.monthsAbbr[start_date.getMonth()],
					str_day = HiTaskCalendar.daysAbbr[start_date.getDay()],
					str_time = "";

				if (hi.items.manager.isTask(item.taskId) && item.due_date) {
					var dd = str2time(item.due_date, Hi_Calendar.format);
					str_day = HiTaskCalendar.days[dd.getDay()];
				}
				
				if (item.start_time) {
					str_time = hi.i18n.getLocalization('properties.datetime_time').replace(/\%time/, time24_to_time12(item.start_time));
				}
				
				date_str = date_str.replace(/\%time/, str_time);
				date_str = date_str.replace(/\%date/, str_date);
				date_str = date_str.replace(/\%day/, str_day);
				date_str = date_str.replace(/\%month/, str_month);
				date_str = date_str.replace(/\%recurrence_end_date/,time2str(str2timeAPI(item.recurring_end_date),Hi_Preferences.get('date_format')));
				
				time = date_str;
			} else {
				if (item.start_time) {
					time += " " + time24_to_time12(item.start_time);
				}
				var end_time = "";
				if (item.end_date) {
					if (item.end_date != item.start_date) {
						end_time = item.end_date;
					}
					if (item.end_time) {
						end_time += " " + time24_to_time12(item.end_time);
					}
				}
				if (end_time != "") {
					time += " - " + end_time;
				}
			}
		} else {
			var end_date = hi.items.manager.isTask(item.id) ? item.due_date : item.end_date;

			if (end_date) {
				time += item.end_date;
			}
		}
		
		return time;
	},	
	
	dojo.global.getTaskDateTitle = function(id, date, pdata) {
		var dateEnd = hi.items.date.getEventEndTime(id, '23:59');
		var dateStart = hi.items.date.getEventStartTime(id, '23:59');
		var recurring = parseInt(hi.items.manager.get(id, 'recurring') || 0);
		
		if (recurring) {
			var recurring_date = hi.items.date.getNextEventDate(id, date);
			if (!recurring_date) {
				recurring_date = HiTaskCalendar.nextEvent(dateStart, parseInt(hi.items.manager.get(id, 'recurring')));
				recurring_date = (recurring_date ? recurring_date[1] : null);
			} else {
				recurring_date = str2time(recurring_date, 'y-m-d');
			}
			
			if (recurring_date) {
				if (dateEnd) {
					dateEnd = recurring_date;
				} else {
					dateStart = recurring_date;
				}
			}
		}
		
		return this.getTaskDateTitleByDates(id, dateStart, dateEnd);
	};
	
	dojo.global.getTaskDateTitleByDates = function(id, dateStart, dateEnd, list) {
		var recurring = parseInt(hi.items.manager.get(id, 'recurring') || 0),
			sorting = Project.getSorting(),
			showEnd = sorting != Project.ORDER_START_DATE,
			date;

		/*#3956 - Sorting by "Start Date" and "Due Date" became broken*/
		if(list && (list.title === 'ungrouped' || list.title == 'dateUngrouped')){
			// console.log('list name is', list.listName);
			if(sorting === Project.ORDER_START_DATE){
				if(recurring && (item = hi.items.manager.get(id))){
					dateStart = dateStart || (item.start_date ? str2timeAPI(item.start_date) : '');
				}
				date = dateStart || '';
			}else if(sorting === Project.ORDER_DUE_DATE){
				if(recurring && (item = hi.items.manager.get(id))){
					dateEnd = item.end_date ? str2timeAPI(item.end_date) : '';
				}
				date = dateEnd || '';
				
				if (!date && !recurring) {
					// #4126: use start date in such case but only if item is not recurring.
					date = dateStart;
				}
			}else{
				date = dateEnd || dateStart;
			}
		}else if(list && (list.title === 'today' || list.title === 'tomorrow')){
			date = dateEnd || dateStart;
		}else{
			date = showEnd? (dateEnd || dateStart) : (dateStart || dateEnd);
		}
		
		if (date) {
			var dateTemp = date.getTime();
			var dateEndTemp = (dateEnd ? dateEnd.getTime() - 1 : null);
			var dateStartTemp = (dateStart ? dateStart.getTime() : null);
			
			var dayLength = 3600 * 24 * 1000;
			var todayStart = str2time(HiTaskCalendar.getTodayDateStr(), 'y-m-d').getTime();
			var todayEnd = todayStart + dayLength - 1;
			var yesterdayStart = todayStart - dayLength;
			var tommorowEnd = todayEnd + dayLength;
			
			var months = capitalizeFirstLetter(djDateLocale.getNames('months', 'abbr'));
			
			if (dateTemp < yesterdayStart || dateTemp >= tommorowEnd) {
				return date.getDate() + ' ' + months[date.getMonth()];
			} else {
				if (!recurring) {
					if (dateEndTemp) {
						if (dateEndTemp < todayStart) {
							return hi.i18n.getLocalization('calendar.yesterday');
						} else if (dateEndTemp <= todayEnd) {
							return hi.i18n.getLocalization('calendar.today');
						} else {
							return hi.i18n.getLocalization('calendar.tomorrow');
						}
					} else {
						if (dateStartTemp < todayStart) {
							return hi.i18n.getLocalization('calendar.yesterday');
						} else if (dateStartTemp < todayEnd) {
							return hi.i18n.getLocalization('calendar.today');
						} else {
							return hi.i18n.getLocalization('calendar.tomorrow');
						}
					}
				} else {
					if (dateTemp < todayStart) {
						return hi.i18n.getLocalization('calendar.yesterday');
					} else if (dateTemp <= todayEnd) {
						return hi.i18n.getLocalization('calendar.today');
					} else {
						return hi.i18n.getLocalization('calendar.tomorrow');
					}
				}
			}
		}
		
		return '';
	};
	
	//TODO
	return {};
	
});
