//dojo.provide("hi.project.project");
define([
	"hi/project/base"
], function () {
	
	var Project = dojo.global.Project;
			
	dojo.mixin(Project, {
		
		openedProject: false,
		
		/*Add project*/
		newProject: false,
		newProjectName: 'Project %N',
		projectColorValueWidget: null,
		
		getProjectCounter: function () {
			var i, ii, c;
			var re = new RegExp('^' + Project.newProjectName.replace(/%N/, '(\\d+)') + '$');
			var a = {};
			var items = hi.items.manager.get({"category": 0});
			for (i=0, ii=items.length; i<ii; i++) {
				if (re.exec(items[i].title)) {
					c = RegExp.$1;
					if (c > 0) {
						a[c] = true;
					}
				}
			}
			i = 1;
			while (i in a) {
				i++;
			}
			return i;
		},
		
		addProject: function (title) {
			// summary:
			//		Add new project
			
			var projectNameWidget = dijit.byId('newProjectName'),
				projectMessageNode = dijit.byId('newProjectMessage'),
				start_date = dijit.byId('newProjectStartDateWidget'),
				due_date = dijit.byId('newProjectDueDateWidget'),
				message = projectMessageNode.get('value'),
				defaultTitle = projectNameWidget.get('defaultValue'),
				defaultMessage = dojo.getAttr(projectMessageNode, 'defaultValue');
			
			title = projectNameWidget.getValue();
			title = title && typeof(title) === 'string' ? title.trim() : '';
			message = message && typeof(message) === 'string' ? message.trim() : '';

			start_date = start_date ? start_date.getValue() : '';
			due_date = due_date ? due_date.getValue() : '';

			start_date = start_date ? time2str(start_date, 'y-m-d') : '';
			due_date = due_date ? time2str(due_date, 'y-m-d') : '';
			
			//Validate
			if (title == '') {
				alert('Please specify a title');
				return;
			}
			
			if (message && message.length > CONSTANTS.taskMessageMaxLength) {
				alert("The description shouldn't exceed " + CONSTANTS.taskMessageMaxLength + " characters!");
				return;
			}
			
			//Save data
			this._doProjectAdd(title, message, start_date, due_date);
	
			dojo.byId('newProjectName').blur();
			Project.hideProjectDialog();
		},
		
		_doProjectAdd: function(title, message, start_date, due_date, callback) {
			var priority = Project.getProjectMaxPriority(1) + 10,
				shared = getValue('newProjectShared'),
				projectSharingWidget = dijit.byId('projectSharingWidget');
			
			var formData = {
				"title": title,
				"message": message || '',
				"category": 0,
				'newProject': true,
				'priority': priority,
				'user_id': Project.user_id,
				'color_value': CONSTANTS.defaultColorValue,
			};

			if (this.projectColorValueWidget) {
				formData['color_value'] = this.projectColorValueWidget.get('color');
			}
			if (projectSharingWidget) {
				formData.permissions = JSON.stringify(projectSharingWidget.get('value'));
			}

			if (start_date){
				formData.start_date = time2strAPI(HiTaskCalendar.getDateFromStr(start_date));
			}

			if (due_date) {
				formData.due_date = time2strAPI(HiTaskCalendar.getDateFromStr(due_date));
			}

			if(shared !== ""){
				formData.shared = shared;
			}else{
				formData.shared = false;
			}
			delete(formData.shared);
			trackEvent(trackEventNames.PROJECT_CREATE);
	
			return hi.items.manager.setSave(formData).then(function (/* Project ID */id) {
					if (id) {
						//If new item or task edit form opened update project list
						var item = hi.items.manager.editing();
						if (item) {
							item.editingView.set("parent", id);
							item.editingView.fillProjectList();
						}
						
						//Update list to make sure order is correct
						hi.items.manager.refresh().then(function(){
							/*#3904 Project tab - New Project doesn't get into correct position in the projects list with Alphabetical sorting enabled*/
							hi.items.ProjectListProjects.reset();
						});

						/* #2726 - set new task list as rendered so it can have new items added to it */
						var newProject = hi.items.ProjectListProjects.get(id);
						newProject.lists[0].set('rendered',true);
						
						if (typeof(callback) == 'function') {
							callback(id);
						}
					} else if (!Project.alertElement.open) {
						Project.showProjectDialog(null,null,null,formData);
					}
				});
		},
		
		showProjectDialog: function (task, target, tooltipOptions, formData) {
			// summary:
			//		Show new project dialog
			// 
			if (!this.projectColorValueWidget) {
				this.projectColorValueWidget = new hi.widget.ItemColorValue();
				this.projectColorValueWidget.placeAt(dojo.byId('newProjectColorPicker'));
			}
			
			this.projectColorValueWidget.set('color', CONSTANTS.defaultColorValue);
			this.projectColorValueWidget.on('change', function(value) {
				var icon = dojo.byId('newProjectIcon');
				if (icon && value) {
					icon.style.color = value;
				}
			});
			
			if (target) {
				target = dojo.byId(target);
			} else {
				// if (dojo.style(target, 'display') == 'none') {
					target = dojo.byId('newProjectButton');
				// }
			}
			
			var element = dojo.byId('project');
			if(!element) return false;
			

			if (!Project.projectDialogInitialized) {
				Project.projectDialogInitialized = true;

				dojo.subscribe('TooltipCloseAfter', function (type) {
				//On blur restore input values
					if (type == 'newproject') {
						var inputs = toArray(element.getElementsByTagName('INPUT'));
							inputs = inputs.concat(toArray(element.getElementsByTagName('TEXTAREA')));
						
						for (var i = 0; i < inputs.length; i++) {
							try {
								inputs[i].value = inputs[i].defaultValue;
								inputs[i].checked = inputs[i].defaultChecked;
								
								inputs[i].blur();
								if (typeof inputs[i].onblur == 'function') {
									inputs[i].onblur();
								}
							} catch (err) {}
						}
						var nameWidget = dijit.byId('newProjectName');
						if(nameWidget){
							nameWidget.set('value', '');
						}

						var descWidget = dijit.byId('newProjectMessage');
						if(descWidget){
							descWidget.set('value', '');
						}
					}
				});
			}
			/* Bug #1990 - oorient after dropdown */
			Tooltip.open('newproject', '', target, null, 'below-centered', null, tooltipOptions);
			var sw =  dijit.byId('projectSharingWidget');		//sharing widget
			if(sw){
				sw.fillSharingList();
			}
			
			var newProjectNameWidget = this.newProjectNameWidget = dijit.byId('newProjectName');
			dojo.addClass(newProjectNameWidget.textbox, 'txt');
			
			//Focus first input
			var inputs = toArray(element.getElementsByTagName('INPUT'));
				inputs = inputs.concat(toArray(element.getElementsByTagName('TEXTAREA')));
			
			//dojo.global.setTimeout(function(){ inputs[0].focus(); }, 100);
				
			if (formData) {
				if ('title' in formData){
					dijit.byId('newProjectName').set('value', formData['title']);
				}
				if ('message' in formData){
					dijit.byId('newProjectMessage').set('value', formData['message']);
				}
				if ('color_value' in formData) this.projectColorValueWidget.set('color',formData['color_value']);
				if ('shared' in formData && formData['shared']) setValue('newProjectShared','1');
			}

			var titleInput = dojo.byId('newProjectName');
			if (titleInput) {
				titleInput.focus && titleInput.focus();
			}

			dojo.global.setTimeout(function(){ inputs.forEach(function(item) {if (item.onblur) item.onblur();}); }, 100);
		},
		
		hideProjectDialog: function () {
			Tooltip.close('newproject');
		},
	
		doCounting: function() {
			//var completedCount = 0;
			var i;

			Project.projectCount = 0;
			Project.itemCount = 0;
			for (i in Project.data) {
				if (Project.data[i].user_id == Project.user_id) {
					if (Project.data[i].category == '0') {
						Project.projectCount++;
					} else {
						Project.itemCount++;
					}
				}
			}
		},
		
		moveProject: function(id, id2, before) {
			return Project._move('ps_', id, id2, before);
		},
		
		/**
		 * Returns true if ID belongs to project. If tasks doesn't exist, return false;
		 * 
		 * @param {Number} id
		 */
		isProject: function (id) {
			if (id in Project.data) {
				var category = parseInt(Project.data[id].category);
				return (category ? false : true);
			} else {
				return false;
			}
		}
	});


	return Project;

});
