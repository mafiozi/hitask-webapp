import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import play.Play;
import play.test.FunctionalTest;
import play.vfs.VirtualFile;


public class GroovyViewsBaseTest extends FunctionalTest {
	protected static final String APP_RELATIVE_PATH = "/app";
	protected static final String VIEWS_RELATIVE_PATH = APP_RELATIVE_PATH + "/views";
	protected static final String PUBLIC_RELATIVE_PATH = "/public";
	protected static final String HI_SRC_RELATIVE_PATH = PUBLIC_RELATIVE_PATH + "/app/lib/hi.src";
	
	@Test
	public void dummyTest() {
		
	}
	
	private Iterator<File> getFiles(String fromPath, final String[] extensions, final String[] parentDirsAllowed) {
		VirtualFile root = Play.getVirtualFile(fromPath);
		assertNotNull(root);
		assertTrue(root.exists());
		
		return FileUtils.iterateFiles(root.getRealFile(), new IOFileFilter() {
			@Override
			public boolean accept(File file) {
				return accept(file.getParentFile(), file.getName()) && checkParentsAndItself(file);
			}

			@Override
			public boolean accept(File dir, String name) {
				String ext = getExtension(name);
				return ext != null && ArrayUtils.indexOf(extensions, ext.toLowerCase()) != -1;
			}
			
			private boolean checkParentsAndItself(File file) {
				if (parentDirsAllowed == null) {
					return true;
				}
				
				if (ArrayUtils.indexOf(parentDirsAllowed, file.getAbsolutePath()) != -1) {
					return true;
				}
				
				File parent = file;
				while ((parent = parent.getParentFile()) != null) {
					if (ArrayUtils.indexOf(parentDirsAllowed, parent.getAbsolutePath()) != -1) {
						return true;
					}
				}
				
				return false;
			}
			
			private String getExtension(String filename) {
				String[] parts = StringUtils.split(filename, '.');
				return parts.length > 1 ? parts[parts.length - 1] : null;
			}
		}, TrueFileFilter.INSTANCE);
	}
	
	private Iterator<File> getFiles(String fromPath, final String[] extensions) {
		return getFiles(fromPath, extensions, null);
	}
	
	protected Iterator<File> getAllJava() {
		return getFiles(APP_RELATIVE_PATH, new String[] {"java"});
	}
	
	protected Iterator<File> getAllViews() {
		return getFiles(VIEWS_RELATIVE_PATH, new String[] {"html"});
	}
	
	protected Iterator<File> getAllJsViews() {
		return getFiles(HI_SRC_RELATIVE_PATH, new String[] {"html"});
	}
	
	protected Iterator<File> getAllJavascripts() {
		return getFiles(PUBLIC_RELATIVE_PATH, new String[] {"js"}, new String[] {
			Play.getVirtualFile(PUBLIC_RELATIVE_PATH + "/app/javascripts").getRealFile().getAbsolutePath(),
			Play.getVirtualFile(HI_SRC_RELATIVE_PATH).getRealFile().getAbsolutePath()
		});
	}
	
	protected String getFileAsString(File view, boolean checkIfEmpty) {
		try {
			String content = FileUtils.readFileToString(view);
			
			if (checkIfEmpty) {
				assertNotNull(view.getAbsolutePath() + " content is null", content);
				assertFalse(view.getAbsolutePath() + " is empty", content.trim().equals(StringUtils.EMPTY));
			}
			
			return content;
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		return null;
	}
}
