package mixpanel;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

import play.Logger;
import play.jobs.Job;
import play.libs.F.Promise;
import utils.Constants;

import com.mixpanel.mixpanelapi.ClientDelivery;
import com.mixpanel.mixpanelapi.MessageBuilder;
import com.mixpanel.mixpanelapi.MixpanelAPI;

/**
 * Simple wrapper for Mixpanel Java API.
 * @author Vadim Manikhin
 */
public class MixpanelUtils {
	/**
	 * Sends events to Mixpanel asynchronously.
	 * All events will be associated with specified identity.
	 * TODO: Probably it will be better to create separate events queue
	 * and process it in single thread. But for now this class is used
	 * not very match often and will not hit the Play Jobs thread pool too much.
	 * 
	 * @param identity to associate events with
	 * @param builder which contains events
	 * @return async promise
	 */
	public static Promise<Void> send(final String identity, final MixpanelEventsBuilder builder, int delay) {
		Job j = new Job<Void>() {
			@Override
			public void doJob() {
				try {
					String id = Constants.getMixpanelId();
					
					if (StringUtils.isNotBlank(id) && StringUtils.isNotBlank(identity)) {
						final ClientDelivery delivery = new ClientDelivery();
						MessageBuilder msgBuilder =  new MessageBuilder(id);
						MixpanelAPI mixpanelApi = new MixpanelAPI();
						
						// Just try to create new profile in case if it does not exist for this user id.
						mixpanelApi.sendMessage(msgBuilder.set(identity, new JSONObject()));
						
						for (MixpanelEvent event : builder.getEvents()) {
							delivery.addMessage(msgBuilder.event(identity, event.getName(), new JSONObject(event.getParams())));
						}
						
						mixpanelApi.deliver(delivery);
					}
				} catch (IOException e) {
					Logger.error(e, "io exception occured while sending events to mixpanel: %s", e.getMessage());
				} catch (Exception e) {
					Logger.error(e, "unexpected exception occured while sending events to mixpanel: %s", e.getMessage());
				}
			}
		};
		
		if (delay <= 0) {
			return j.now();
		} else {
			return j.in(delay);
		}
	}
	
	public static Promise<Void> send(final String identity, final MixpanelEventsBuilder builder) {
		return send(identity, builder, 0);
	}
}
