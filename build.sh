#!/bin/bash

BASEDIR=$(dirname $0)
DOJO_VERSION=1.10
PLAY_BASE=/opt/play-1.2.7
PLAY_CMD=$PLAY_BASE"/play"

if [ $# -lt 1 ]
then
		echo "-"
        echo "Usage:"
        echo "$BASH_SOURCE -build-number=123 -deploy-path=/tmp"
        echo "dojo-version default is $DOJO_VERSION. To override use -dojo-version="
        echo "-"
        exit 1
fi


for i in "$@"
do
case $i in
    -dojo-version=*)
    DOJO_VERSION="${i#*=}"
    shift
    ;;
    -build-number=*)
    BUILD_VERSION="${i#*=}"
    shift
    ;;
    -deploy-path=*)
    DEPLOY_PATH="${i#*=}"
    shift
    ;;
    -l=*|--lib=*)
    LIBPATH="${i#*=}"
    shift
    ;;
    --default)
    DEFAULT=YES
    shift
    ;;
    *)
            # unknown option
    ;;
esac
done


if [ -z "${BUILD_VERSION+xxx}" ]; then
	echo "Build version must be provided!"
	exit 1;
fi

if [ -z "${BASEDIR+xxx}" ]; then
        echo "BASEDIR variable is missing!"
        exit 1;
fi

#     V=$(getProp filename property)
#
function getProp () {
   # ignore lines with '#' as the first non-space character (comments)
   # grep for the property we want
   # get the last match just in case
   # strip the "property=" part but leave any '=' characters in the value

   echo `sed '/^[[:space:]]*\#/d' $1 | grep $2  | tail -n 1 | cut -d "=" -f2- | sed 's/^[[:space:]]*//;s/[[:space:]]*$//'`
}
RELEASE_VERSION=$(getProp $BASEDIR/build.properties build.version)

echo DOJO_VERSION=$DOJO_VERSION
echo RELEASE_VERSION=$RELEASE_VERSION
echo BUILD_VERSION=$BUILD_VERSION


LESS_SRC_PATH=public/app/css

echo "Less css..."
lessc --verbose $LESS_SRC_PATH/main.less $LESS_SRC_PATH/main.css
lessc --verbose $LESS_SRC_PATH/print.less $LESS_SRC_PATH/print.css
lessc --verbose $LESS_SRC_PATH/app.less $LESS_SRC_PATH/app.css
lessc --verbose $LESS_SRC_PATH/settings.less $LESS_SRC_PATH/settings.css




# Remove all previous Dojo builds for current release.
echo "rm $DEPLOY_PATH/public/app/lib/dojo.$DOJO_VERSION.hitask.$RELEASE_VERSION*"
rm -Rf "$DEPLOY_PATH"public/app/lib/dojo."$DOJO_VERSION".hitask."$RELEASE_VERSION"*
rm -Rf "$BASEDIR"/public/app/lib/dojo."$DOJO_VERSION".hitask."$RELEASE_VERSION"*

echo "Dojo build..."

BUILD_PROFILE_PATH=$BASEDIR/public/app/lib/dojo.$DOJO_VERSION.hitask.src

cd $BUILD_PROFILE_PATH/util/buildscripts

./build.sh baseUrl=../../dojo \
-opt 0 -version 140  load=build profile=../../../hi.src/hitask-dojo-build \
releaseDir=../../../ releaseName=dojo.$DOJO_VERSION.hitask.$RELEASE_VERSION.$BUILD_VERSION \
action=release cssOptimize=comments optimize=shrinksafe.keepLines mini=true copyTests=false \
create_source_map=library.min.js.map source_map_format=V3
if [ $? -gt 0 ]
then
	echo "Error occurred, exiting."
	exit 1;
fi

echo "Copy Play..."
cp -Rf $PLAY_BASE $BASEDIR/play
#rm -Rf $DEPLOY_PATH/play/documentation

echo "Play build..."
cd $BASEDIR
#./play/play deps --sync
#./play/play precompile .


echo "Replace build number..."
grep build.number conf/application.conf
echo "Replacing..."
replaceit --input="$BASEDIR"/conf/application.conf --wholeline "build.number" "build.number=${BUILD_NUMBER}"
grep build.number conf/application.conf

echo "Cleanup.. $BASEDIR/public/app/lib/dojo.$DOJO_VERSION.hitask.src"
rm -Rf "$BASEDIR"/public/app/lib/dojo."$DOJO_VERSION".hitask.src

if [ -z "${DEPLOY_PATH+xxx}" ]; then
	echo "Deploy path is not set, Not deploying. Done!"
	exit 0;
fi

#echo "Deploy from $BASEDIR to $DEPLOY_PATH..."
#rm -Rf $DEPLOY_PATH
#if [ ! -d "$DEPLOY_PATH" ]; then
#	echo "Creating $DEPLOY_PATH..."
#	mkdir $DEPLOY_PATH
#fi
#echo "Copy..."
#cp -Rfp $BASEDIR/* $DEPLOY_PATH
#echo "Chmod..."
#chmod a+rwx -Rf $DEPLOY_PATH

#rm -Rf "$DEPLOY_PATH"/public/app/lib/dojo."$DOJO_VERSION".hitask.src

echo "Done!"
