/*
 * This file is used only in /hi section
 */

define('hi/build-hitask', [
	'moment/moment',
	"supra/common",
	"hi/store-hitask",
	
	"hi/widget/TimeScroller",
	"hi/widget/TimeTick",
	
	"hi/reminder",
	
	"hi/feedback",
	"hi/preferences",
	"hi/address",
	
	"hi/dnd",

	"hi/sortable",

	"hi/team",
	"hi/notification",
	
	"hi/tag",
	"hi/participant",
	"hi/filter",
	"hi/search",
	"hi/shortcuts",
	"hi/help",
	"hi/news",
	"hi/sound",
	"hi/print",

	"hi/timetracking",
	"hi/comments",
	"hi/upload",
	"hi/main",
	"hi/widget/form/cleartextbox",
	"hi/widget/form/hiTextarea",
	"hi/widget/form/hiSharing/hiSharingWidget",
	"hi/widget/form/hiTag/hiTag",
	"hi/widget/form/hiTag/ComboBox",
	"hi/widget/hiCalendar",
	'hi/widget/image/PreviewLightbox'
], function (moment) {
	dojo.global.CONSTANTS = dojo.mixin(dojo.global.CONSTANTS || {}, {
		duration: 200,
		removeCompletedMinCount: 2
	});

	return {};
});