package beans;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import play.Logger;

public class BaseBean {
	private static final DateTimeFormatter API_DATE_TIME_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZZ");
	
	protected DateTime parseApiDateTime(String dateStr) {
		return parseApiDateTime(dateStr, true);
	}
	
	protected DateTime parseApiDateTime(String dateStr, boolean withOffsetParsed) {
		if (dateStr == null) {
			return null;
		}
		
		try {
			return DateTime.parse(dateStr, withOffsetParsed ? API_DATE_TIME_FORMATTER.withOffsetParsed() : API_DATE_TIME_FORMATTER);
		} catch (Exception e) {
			Logger.warn("Could not parse API date field: %s", dateStr);
			return null;
		}
	}
	
	protected Date parseApiDate(String dateStr) {
		DateTime dt = parseApiDateTime(dateStr);
		return dt != null ? dt.toDate() : null;
	}
}
