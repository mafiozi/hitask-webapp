package beans;

/**
 * Simple wrapper implementation that allows to wrap
 * some type and provide access to "value" and "name"
 * properties of wrapper object. Useful in HTML popup lists.
 * 
 * @author Vadim Manikhin
 *
 * @param <T> The wrapped type.
 */
public abstract class SimpleWrapper<T> {
	protected T wrappedObject;
	protected Object value;
	protected Object name;
	
	public SimpleWrapper(T t) {
		this.wrappedObject = t;
		parseValue();
		parseName();
	}
	
	abstract protected void parseValue();
	abstract protected void parseName();
	
	public final Object getName() {
		return name;
	}
	
	public final Object getValue() {
		return value;
	}
}
