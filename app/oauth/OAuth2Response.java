package oauth;

import java.util.Map;

import com.google.gson.JsonObject;

import utils.HttpResponseWrapper;

public class OAuth2Response {
	private static final String ACCESS_TOKEN_KEY = "access_token";
	private static final String REFRESH_TOKEN_KEY = "refresh_token";
	private static final String ERROR_KEY = "error";
	
	private final String accessToken;
	private final String refreshToken;
	private final OAuth2Error error;
	private final HttpResponseWrapper httpResponse;

	private OAuth2Response(String accessToken, String refreshToken, OAuth2Error error, HttpResponseWrapper response) {
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.error = error;
		this.httpResponse = response;
	}

	public OAuth2Response(HttpResponseWrapper response) {
		this.httpResponse = response;
		Map<String, String> querystring = response.getQueryString();
		if (querystring.containsKey(ACCESS_TOKEN_KEY)) {
			this.accessToken = querystring.get(ACCESS_TOKEN_KEY);
			this.refreshToken = querystring.containsKey(REFRESH_TOKEN_KEY) ? querystring.get(REFRESH_TOKEN_KEY) : null;
			this.error = null;
		} else if (response.getContentType().startsWith("application/json") && !response.getString().toLowerCase().contains(ERROR_KEY)) {
			JsonObject jsonResponse = response.getJson().getAsJsonObject();
			this.accessToken = jsonResponse.get(ACCESS_TOKEN_KEY).getAsString();
			this.refreshToken = jsonResponse.get(REFRESH_TOKEN_KEY) != null ? jsonResponse.get(REFRESH_TOKEN_KEY).getAsString() : null;
			this.error = null;
		} else {
			this.accessToken = null;
			this.refreshToken = null;
			this.error = OAuth2Error.oauth2(response);
		}
	}

	public static OAuth2Response error(OAuth2Error error, HttpResponseWrapper response) {
		return new OAuth2Response(null, null, error, response);
	}

	public String getAccessToken() {
		return accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public OAuth2Error getError() {
		return error;
	}

	public HttpResponseWrapper getHttpResponse() {
		return httpResponse;
	}
}