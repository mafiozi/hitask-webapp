package beans;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;


public class UserExternalSync {
	private static final String GOOGLE = "GOOGLE";
	
	private Long id;
	private String type;
	private String error;
	private String errorDescription;
	private String recentRunUTC;
	private String recentRun;
	private List<UserExternalSyncTarget> targets = new ArrayList<UserExternalSyncTarget>();

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getRecentRunUTC() {
		return recentRunUTC;
	}

	public void setRecentRunUTC(String recentRunUTC) {
		this.recentRunUTC = recentRunUTC;
	}

	public String getRecentRun() {
		return recentRun;
	}

	public void setRecentRun(String recentRun) {
		this.recentRun = recentRun;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public boolean isActive() {
		return StringUtils.isBlank(error) && StringUtils.isBlank(errorDescription);
	}

	public List<UserExternalSyncTarget> getTargets() {
		return targets;
	}

	public void setTargets(List<UserExternalSyncTarget> targets) {
		this.targets = targets;
	}
	
	public boolean isGoogle() {
		return type != null && type.equalsIgnoreCase(GOOGLE);
	}
	
	public boolean isForbiddenError() {
		return errorDescription != null && (errorDescription.contains("403 Forbidden") || errorDescription.contains("401 Unauthorized"));
	}
}
