//dojo.provide("hi.main.helper");
define(function () {
	
	//Item are beeing sorted
	dojo.global.isSorting = false;
	
	dojo.global.fnEmpty  = function() {}
	
	dojo.global.resetView  = function() {
		var tab = dojo.global.tabs.main;
		tab.open('my');
		Hi_Tag.filter('');
		Project.expandedTasks = {};
		Hi_TaskFilter.setFilter(0);
		Hi_Search.close();
		Project.changeSorting(4);
	}
	
	dojo.global.advFocus = function(obj, val) {
	    if(obj.value == val && !dojo.hasClass(obj, 'noPlaceHolder')) {
			obj.value = "";
			//dojo.removeClass(obj, 'empty');
			dojo.addClass(obj,'inputText');
			dojo.removeClass(obj, 'placeHolder');
		}
	}
	dojo.global.advBlur = function(obj, val, func, funcOk) {
	    func = func || false;
	    if(obj.value == "") {
			obj.value = val;
			//dojo.addClass(obj, 'empty');
			dojo.addClass(obj,'placeHolder');
			dojo.removeClass(obj,'noPlaceHolder');
			dojo.removeClass(obj,'inputText');
			if (func) {
				func();
			}
		} else if (funcOk) {
			funcOk();
		}
	}
	dojo.global.removeDefaultValues = function(form) {
		var elements = dojo.query('input,textarea', form);
		for(var i=0,ii=elements.length; i<ii; i++) {
			var def = elements[i].getAttribute('defaultvalue');
			if (def && elements[i].value == def) {
				advFocus(elements[i], def);
			}
		}
	}
	
	dojo.global.toArray  = function(nodes) {
		try {
			return [].slice.call(nodes, 0);
		} catch (e) {
			var r = [];
			for(var i=0,ii=nodes.length; i<ii; i++) {
				r.push(nodes[i]);
			}
			return r;
		}
	}
	
	dojo.global.hoverTimeout = null;
	dojo.global.curHoverElement = null;
	
	//IE7+ and other browsers support :hover, no need JS implementation
	if (!dojo.isIE || dojo.isIE >= 7) {
	
		dojo.global.hoverElement = function(element) {}
		dojo.global.houtElement = function(element) {}
		
	} else {
	
		dojo.global.hoverElement = function(element)
		{
			if (!dojo.isIE || dojo.isIE >= 7) return;
			if (Project.dragging) {
				return false;
			}
			if(curHoverElement != null) {
				clearTimeout(hoverTimeout);
				if(element != curHoverElement) {
					houtCurHoverElement();
				}
			}
			dojo.addClass(element, 'hover');
		}
		dojo.global.houtElement = function(element) {
			if (!dojo.isIE || dojo.isIE >= 7) return;
			curHoverElement = element;
			hoverTimeout = setTimeout(houtCurHoverElement, 10);
		}
	}
	
	dojo.global.houtCurHoverElement = function() {
		curHoverElement = curHoverElement || null;
		if (!curHoverElement) return; 
		dojo.removeClass(curHoverElement, 'hover');
		curHoverElement = null;
	}
	dojo.global.houtAllElements = function(el) {
		el = dojo.byId(el);
		var els = dojo.query('.hover', el);
		var i, imax = els.length;
		for (i = 0; i < imax; i++) {
			if (dojo.hasClass(els[i], 'hover'))
				dojo.removeClass(els[i], 'hover');
		}
	}
	
	dojo.global.objectLength  = function(obj) {
		var cnt = 0;
		for(var i in obj) if (obj.hasOwnProperty(i)) cnt++;
		return cnt;
	}
	
	dojo.global.toggleElement = function(a) {
		a = dojo.byId(a);
		if (a.style.display == 'none' || dojo.hasClass(a, 'hidden')) {
			a.style.display = '';
			dojo.removeClass(a, 'hidden');
			return true;
		} else {
			a.style.display = 'none';
			dojo.addClass(a, 'hidden');
			return false;
		}
	}
	
	dojo.global.forceReflowIE = function(element) {
		if (element.style.zoom == '1') {
			element.style.zoom = '100%';
		} else {
			element.style.zoom = '1'
		}
	}
	
	dojo.global.searchData = function(str) {
		var i;
		if (!dojo.isString(str)) {
			str = '';
		}
		str = str.toLowerCase().trim();
		var el;
		for (i in Project.data) {
			if (el = dojo.byId('task_' + i)) {
				el = dojo.query('.task_title', el)[0];
				if (str == '' || Project.data[i].title.toLowerCase().indexOf(str) == -1) {
					el.style.backgroundColor = '';
				} else {
					el.style.backgroundColor = 'yellow';
				}
			}
		}
	}
	
	dojo.global.log = function(str) {
		if (typeof console != 'object' || !('log' in console)) return;
		
		if (log.start_time) {
			console.log(str, ' [' + ((new Date()).valueOf() - log.start_time) + ']');
		} else {
			console.log(str, ' [' + Date() + ']');
		}
		log.start_time = new Date();
	}
	
	//TODO
	return {};
});