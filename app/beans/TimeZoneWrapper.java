package beans;

import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;

public class TimeZoneWrapper extends SimpleWrapper<TimeZone> {
	public TimeZoneWrapper(TimeZone timezone) {
		super(timezone);
	}
	
	@Override
	protected void parseValue() {
		value = wrappedObject.getID();
	}
	
	protected void parseName() {
		String[] splitted = StringUtils.split((String)value, "/");
		if (splitted.length > 0) {
			name = splitted[splitted.length - 1].replace("_", " ");
		} else {
			name = wrappedObject.getDisplayName();
		}
		
		prefixName();
	}
	
	private void prefixName() {
		int offset = wrappedObject.getOffset(new Date().getTime());
		String sign = offset >= 0 ? "+" : "-";
		name = String.format("(GMT %s%s) %s", sign, getDurationString(Math.abs(offset) / 1000), name);
	}
	
	private String getDurationString(int seconds) {
		int hours = seconds / 3600;
		int minutes = (seconds % 3600) / 60;
		seconds = seconds % 60;
		
		return twoDigitString(hours) + ":" + twoDigitString(minutes);
	}

	private String twoDigitString(int number) {
		if (number == 0) {
			return "00";
		}
		
		if (number / 10 == 0) {
			return "0" + number;
		}
		
		return String.valueOf(number);
	}
}
