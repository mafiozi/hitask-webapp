package controllers;

import play.Logger;
import play.mvc.Controller;
import play.mvc.Http.StatusCode;
import utils.Constants;
import api.ApiCall;
import api.ApiCallException;

public class PublishedItem extends Controller {
	public static void get(String path) {
		ApiCall apic = new ApiCall();
		String host = Constants.DEFAULT_DOMAIN;
		try {
			beans.PublishedItem item = apic.getItemPublished(path);
			render(item, host);
		} catch (ApiCallException e) {
			Logger.warn(e, e.getMessage());
			response.status = StatusCode.NOT_FOUND;
			renderTemplate("Application/itemNotFound.html");
		}
	}
}
