# This is the main configuration file for the application.
# ~~~~~
application.name=HiTask-Webapp

# Application mode
# ~~~~~
# Set to dev to enable instant reloading and other development help.
# Otherwise set to prod.
application.mode=dev
%production.application.mode=prod
%prod.application.mode=prod
%testask.application.mode=prod
%qatask.application.mode=prod

# Secret key
# ~~~~~
# The secret key is used to secure cryptographics functions
# If you deploy your application to several instances be sure to use the same key !
application.secret=vgdfCO6j3ykiVVpvQ80DYqfS00lkJ3KmhO2ZeJ1FzEE1bwiwm1xSggaNMfwfnmxh
%qatask.application.secret=111qatask
application.session.cookie=HI
%production.application.defaultCookieDomain=.hitask.com
%testask.application.defaultCookieDomain=.testask.com
%qatask.application.defaultCookieDomain=.qatask.com

%vadim.application.defaultCookieDomain=.hitask.local
%vadim_local_api.application.defaultCookieDomain=.hitask.local
%roman.application.defaultCookieDomain=.ht.local

# application.session.secure=true

# i18n
# ~~~~~
# Define locales used by your application.
# You can then place localized messages in conf/messages.{locale} files
application.langs=en,es,ru,fr,ja,tr,de,pt_BR,zh_CN,ar,pl
%production.application.langs=en,es,ru,fr,de,tr,zh_CN,ja,ar,pt_BR
%vadim_local_api.application.langs=en,es,ru,fr,ja,tr,de,pt_BR,zh_CN,ar,pl


# Date format
# ~~~~~
date.format=yyyy-MM-dd
# date.format.fr=dd/MM/yyyy

# Server configuration
# ~~~~~
# If you need to change the HTTP port, uncomment this (default is set to 9000)
http.port=9200
http.exposePlayServer = false

# application.session.cookie=HI_WEBAPP
# Default value for maxAge is "session scope". If user closes browser cookie is deleted.
# application.session.maxAge=24h
# application.session.secure=true

# JVM configuration
# ~~~~~
# Define which port is used by JPDA when application is in debug mode (default is set to 8000)
# jpda.port=8000
%vadim.jpda.port=8002
%vadim_local_api.jpda.port=8002
#
# Java source level => 1.5, 1.6 or 1.7 (experimental)
java.source=1.7

# Log level
# ~~~~~
# application.log=INFO
# application.log.system.out=off
application.log.path=/log4j.properties
%testask.application.log.path=/log4j-logentries-testask.xml
%qatask.application.log.path=/log4j-logentries-qatask.xml
%production.application.log.path=/log4j-logentries-production.xml

#
# Store path for Blob content
attachments.path=data/attachments

# Mail configuration
# ~~~~~
# Default is to use a mock Mailer
mail.smtp=mock

# Url-resolving in Jobs
# ~~~~~~
%production.application.baseUrl=http://hitask.com
%testask.application.baseUrl=http://testask.com
%qatask.application.baseUrl=http://qatask.com
%autobuild.application.baseUrl=http://testask.com
%vadim.application.baseUrl=http://hitask.local/
%vadim_local_api.application.baseUrl=http://hitask.local/
%roman.application.baseUrl=http://ht.local/


# Jobs executor
# ~~~~~~
# Size of the Jobs pool
# play.jobs.pool=10

# Execution pool
# ~~~~~
# Default to 1 thread in DEV mode or (nb processors + 1) threads in PROD mode.
# Try to keep a low as possible. 1 thread will serialize all requests (very useful for debugging purpose)
# play.pool=3

# Open file from errors pages
# ~~~~~
# If your text editor supports opening files by URL, Play! will
# dynamically link error pages to files
#
# Example, for textmate:
# play.editor=txmt://open?url=file://%s&line=%s

# Testing. Set up a custom configuration for test mode
# ~~~~~
#%test.module.cobertura=${play.path}/modules/cobertura
%test.application.mode=dev
%test.db.url=jdbc:h2:mem:play;MODE=MYSQL;LOCK_MODE=0
%test.jpa.ddl=create
%test.mail.smtp=mock

########### HiTask properties ###########
build.version=18.1
build.number=1
# Uncomment following property if you do not want webapp to send version to server,
# so server will never respond with 412 Precondition Failed that means upgrade required.
#build.doNotSendWebappVersionHeader=true

%vadim_local_api.build.version=18.1
%vadim_local_api.build.number=77

development.mode.scripts=true
%testask.development.mode.scripts=false
%qatask.development.mode.scripts=false
%production.development.mode.scripts=false

api.schemeName=https
api.host=secure.qatask.com
api.baseUrl=/api/v2
api.key=a03d2585-c087-4e9e-8b61-6fcb2edc22be

%production.api.schemeName=https
%production.api.host=secure.hitask.com
%production.api.baseUrl=/api/v2
%production.api.key=web1

%testask.api.schemeName=https
%testask.api.host=secure.testask.com
%testask.api.baseUrl=/api/v2
%testask.api.key=hitask_api_key_test

%qatask.api.schemeName=https
%qatask.api.host=secure.qatask.com
%qatask.api.baseUrl=/api/v2
%qatask.api.key=hitask_api_key_test

# %roman.api.schemeName=https
# %roman.api.host=secure.ht.local
# %roman.api.baseUrl=/api/v2
# %roman.messagefiles.enableDiagnostics=false

%vadim.api.schemeName=https
%vadim_local_api.api.schemeName=https
%vadim.api.host=secure.testask.com
%vadim_local_api.api.host=secure.hitask.local
%vadim.api.baseUrl=/api/v2
%vadim_local_api.api.baseUrl=/api/v2
%vadim.api.key=hitask_api_key_test
%vadim_local_api.api.key=hitask_api_key_test

%qi.api.host=secure.hitask.com


# Path to languages relative to Play Root.
# Must contain folders named as locale name (e.g. en, es, ru).
# Locale folders must contain *.properties files (e.g. login.properties, about.properties).
# Messages can be accessed using following keys: <file_name>.<property_key>
# For example: login.properties contains properties with 'feedback.title', 'feedback.from' and 'common.titleAll' keys.
# In this case messages will have following keys: login.feedback.title, login.feedback.from and login.common.titleAll.
messagefiles.path=/languages
# Default locale which messages will be placed into Play default messages properties set.
messagefiles.defaultLocale=en
# Indicates if diagnostic messages should be logged.
messagefiles.enableDiagnostics=false



# GA
%testask.google.analytics.id=UA-1572732-8
%production.google.analytics.id=UA-1572732-1
%vadim.google.analytics.id=UA-41964981-1
%vadim_local_api.google.analytics.id=UA-41964981-1

#Intercom
#%roman.intercom.id=21r8q5hy
#%roman.intercom.secret=ICUB4KQoYRC-xKVwNyOZfDLjkWKegaKL3T-oZOry
%testask.intercom.id=21r8q5hy
%testask.intercom.secret=ICUB4KQoYRC-xKVwNyOZfDLjkWKegaKL3T-oZOry

%qatask.intercom.id=21r8q5hy
%qatask.intercom.secret=ICUB4KQoYRC-xKVwNyOZfDLjkWKegaKL3T-oZOry

#%vadim_local_api.intercom.id=21r8q5hy
#%vadim_local_api.intercom.secret=ICUB4KQoYRC-xKVwNyOZfDLjkWKegaKL3T-oZOry

#Messagefiles plugin https://github.com/HiTask/play-messagefiles
%vadim.messagefiles.enableDiagnostics=false
%vadim_local_api.messagefiles.enableDiagnostics=false


hitask.reservedDomains = secure, www, www1, www2, www3, www4, fuck, shit, dev, securedev, test, securetest, cloudfront, cdn, cloud, static, img, images, hitask

completed.items.warning.threshold=100
%testask.completed.items.warning.threshold=30
%qatask.completed.items.warning.threshold=30
%vadim_local_api.completed.items.warning.threshold=5


# Social Sign In/Up integration.
social.facebook.icon=&#xf09a;
social.google.icon=&#xf0d5;
social.facebook.buttonClass=primary
social.google.buttonClass=success

#social.facebook.id=
#social.facebook.secret=
social.facebook.auth_url=https://graph.facebook.com/oauth/authorize
social.facebook.token_url=https://graph.facebook.com/oauth/access_token

#social.google.id=
#social.google.secret=
social.google.auth_url=https://accounts.google.com/o/oauth2/auth
social.google.token_url=https://accounts.google.com/o/oauth2/token

# Social sharing, liking, etc...
%vadim_local_api.social.facebook.id=474677262629531
%vadim_local_api.social.facebook.secret=97fbf7c6bec6a5a50ffe361c87e20cba

%vadim_local_api.social.google.id=588927581547.apps.googleusercontent.com
%vadim_local_api.social.google.secret=Vs888dc24YCDJVNhWLHjpAun

%qatask.social.facebook.id=109374791909
%qatask.social.facebook.secret=277abc7dbca276f29e9d8a6d4d2fc578

%testask.social.facebook.id=846374538723910
%testask.social.facebook.secret=ea4152b7e6eaa9436cf41f47420da07f

%testask.social.google.id=326771116683-691tko480phtfjbcapar51omcb9g9pq5.apps.googleusercontent.com
%testask.social.google.secret=AIHFcGZQmM3GjUokZffR2eY6

%qatask.social.google.id=326771116683.apps.googleusercontent.com
%qatask.social.google.secret=2kVZpPHfLJYSjv6TaHz-_M_6

%production.memcached=enabled
%production.memcached.host=172.30.2.32:11211
%vadim_local_api.memcached=enabled
%vadim_local_api.memcached.host=127.0.0.1:11211

# Recurring payments.
payment.stripe.api.key.public=pk_test_bB75LslTmB68uJKQuGGvheIo
%vadim.payment.stripe.api.key.public=pk_test_bB75LslTmB68uJKQuGGvheIo
%qatask.payment.stripe.api.key.public=pk_test_sE9GFRTBmPzYM0EPx21ZAkrL
%testask.payment.stripe.api.key.public=pk_test_lZi4jqTnk8L3MqEbaM0Qce7n
%production.payment.stripe.api.key.public=pk_live_uxZqmbROdyd5xbCNQyhHKaYl

# Outlook Add-in download location
outlook.addin.setup_url=https://cdn.hitask.com/HiTaskOutlookAddIn-Beta-Setup.exe
outlook.addin.setup_url.32=https://cdn.hitask.com/HiTaskOutlookAddIn-Beta-Setup32.msi

mimetype.mp3=audio/mpeg

community.sso.secret=d37960a3-f7b5-4324-81e1-5b8812d744cd
community.sso.redirect_url=https://community.hitask.com/session/sso_login
