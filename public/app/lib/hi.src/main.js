define([
	"dijit/form/DateTextBox",
	"dijit/form/TimeTextBox",
	"dijit/form/Button",
	
	"dojox/layout/ResizeHandle",
	"dojox/layout/ContentPane",
	
	//Scripts
	"hi/main/completed",
	"hi/main/tasks",
	"hi/main/projects",
	"hi/main/archive",
	"hi/main/team",
	"hi/main/helper",
	"hi/main/textile",
	
	"hi/main/form/properties",
	"hi/main/form/color",
	
	"hi/main/header",
	"hi/main/logout",
	
	"hi/main/base"
], function () {
	
	dojo.global.CONSTANTS = dojo.mixin(dojo.global.CONSTANTS || {}, {
		duration: 200,
		removeCompletedMinCount: 2
	});
	
	return {};
	
});