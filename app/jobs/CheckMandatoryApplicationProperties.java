package jobs;

import play.Logger;
import play.Play;
import play.jobs.Job;
import play.jobs.OnApplicationStart;

@OnApplicationStart(async=true)
public class CheckMandatoryApplicationProperties extends Job {
	private static final String[] MANDATORY_PARAMS = {
		"application.baseUrl"
	};
	
	@Override
	public void doJob() {
		for (String paramName : MANDATORY_PARAMS) {
			if (!Play.configuration.containsKey(paramName)) {
				Logger.error("Configuration is missing '%s' parameter", paramName);
			}
		}
		final String sslKeyStoreKey = "ssl.keyStore";
		if (Play.configuration.getProperty(sslKeyStoreKey, "").equals("glassfish")) {
			Play.configuration.setProperty(sslKeyStoreKey, "");
		}
    }
}