/*jshint -W117:false */
"use strict";

define([
	"hi/project/base",
	"hi/preferences",
	'hi/main/logout'
], function () {

	var Project = dojo.global.Project;
	
	dojo.mixin(Project, {
		
		TIMEOUT_RETY_INTERVAL: 15000,	//15 seconds
		
		VERSION: '',
		
		/*Ajax method*/
		ajax: function (url, params, func, errFunc, options, path, fromQueue) {
			params = params || {};
			options = options || {};
			func = func || function(){};
			errFunc = errFunc || function(){};
			path = path || 'data';
			fromQueue = fromQueue || false;
			
			var call_arguments = [url, params, func, errFunc, options, path, fromQueue];
			var i;
			
			//If in queue already are items
			if (url in ConnectionQueue.requests && ConnectionQueue.list.length && !fromQueue) {
				ConnectionQueue.list.push(call_arguments);
				ConnectionQueue.update();
				
				//Call error
				errFunc(false, options, true);
				
				if (url == 'itemnew') {
					//Make sure new item is not there until it's created on server
					//and has ID
					Project.opened = false;
					Project.openEdit = false;
					//setTimeout(fillTasks, 0);
				} else if (url == 'projectdelete' || url == 'pdelete' || url == 'ptdelete') {
					//Remove project and tasks (if needed) from data
					Project.dropProject[params['id']] = (url == 'ptdelete' || (url == 'projectdelete' && params.cascade == 1) ? 1 : 0);
					
					var p = Project.data;
					delete p[params.id];
					
					Click.removeClickFunction('project' + params.id + 'inline');
					for (i in p) {
						if (p[i].parent == params.id) {
							if (url == 'pdelete' || (url == 'projectdelete' && params.cascade == 0)) {
								p[i].parent = "";
							} else {
								delete p[i];
							}
						}
					}
					Project.doCounting();
					HiTaskCalendar.update();
					Hi_Reminder.resetAllReminders();
					Hi_Overdue.reload();
					Hi_Tag.dropCache();
					//fillTasks('pdelete');
				}
				
				return;
			}
			
			//success callback function
			var func2;
			if (typeof Hi_Notification != 'undefined') {
				var msg = url;
				
				if (url == 'itemsave') {
					var params_count = 0;
					for(i in params) if (i != 'id') params_count++;
					
					if (params_count == 1) {
						if ('color' in params) msg += '_color';
						if ('starred' in params) msg += '_starred';
						if ('parent' in params) {
							if (Project.isProject(params.parent)) {
								msg += '_project';
							}
						}
					} else if (params_count == 2 || params_count == 4) {
						if ('start_date' in params || 'end_date' in params) {
							msg += '_start_date';
						}
					}
				}
				
				func2 = function(dataResponse) {
					if (msg == 'fradd') {
						if (dataResponse.isStatusOk()) {
							// Show notification only if request is successful.
							Hi_Notification.putMessageInQue(msg);
						}
					} else {
						Hi_Notification.putMessageInQue(msg);
					}
					
					if (dataResponse instanceof hiApiResponse && dataResponse.getResponseType() == 'simple') {
						if (url == 'itemsave') {
							highlightTask(dataResponse.getDataId());
							
							if (Hi_Timer.isTimedTask(dataResponse.getDataId())) {
								Hi_Timer.view.update();
							}
							
						} else if (url == 'itemassign') {
						} else if (url == 'updatefull' || url == 'update') {
						} else {
							setTimeout(function(){
								highlightTask(dataResponse.getDataId());
							}, 0);
						}
					}
				};
			} else {
				func2 = function(){};
			}
			
			switch (url) {
				case 'itemnew':
					//Duplicate
					if (Project.duplicateItem) {
						if ('redraw' in options && options.redraw) {
							func2 = function(data) { Hi_Notification.putMessageInQue('task_duplicate'); };
						} else {
							func2 = function() {};
						}
					}
					
					break;
				case 'itemsave':
					if ('parent' in params && !params['parent']) {
						params['parent'] = 0;
					}
					
					break;
				case 'projectdelete':
				case 'pdelete':
				case 'ptdelete':
					
					options = dojo.mixin(options || {},{item_id:params['id']});
						
					var group = Hi_Preferences.get('currentTab', 'my', 'string');
					
					if (group == 'project' || fromQueue) {
						if (!fromQueue && !(params['id'] in Project.data)) {
							if (!ConnectionQueue.list.length) alert('Item does not exist.');
							return;
						}
						if (!fromQueue && params['id'] in Project.dropProject) {
							return;
						}
						Project.dropProject[params['id']] = (url == 'ptdelete' || (url == 'projectdelete' && params.cascade == 1) ? 1 : 0);
						
						var funcMergeChanges = function(data) {
							var p = Project.data;
							delete p[data.id];
							
							Click.removeClickFunction('project' + data.id + 'inline');
							for (var i in p) {
								if (p[i].parent == data.id) {
									if (url == 'pdelete' || (url == 'projectdelete' && params.cascade == 0)) {
										p[i].parent = "";
									} else {
										delete p[i];
									}
								}
							}
							Project.doCounting();
							HiTaskCalendar.update();
							Hi_Reminder.resetAllReminders();
							Hi_Overdue.reload();
							Hi_Tag.dropCache();
							//fillTasks('pdelete');
						};
						
						if (Project.faster) {
							funcMergeChanges({id:params['id']});
						}
						
						func2 = function(data, options) {
							var item_id = (options.item_id)?Number(options.item_id):0;
							if (!Project.faster) {
								funcMergeChanges({id:item_id});
							}
							delete Project.dropProject[item_id];
							Hi_Notification.putMessageInQue(url);
						};
						
						errFunc = function(data, options, silent) {
							if (!silent) {
								alert('Project was not removed. Try again later.');
							}
							delete Project.dropProject[params['id']];
							if (Project.faster) {
								//Project.load(false, true);
							}
						};
						break;
					}
					break;
//				case 'update':
//					if (Project.last_team_time) {
//						params['team_time'] = Project.last_team_time;
//					}
//					if (Project.last_chat_time) {
//						params['chat_time'] = Project.last_chat_time;
//					}
//				
//					break;
				case 'frfind':
					if(params['search']){
						params['query'] = params['search'];
						delete params['search'];
					}
					
					break;
				case 'feedback':
					options.noResponse = true;
					
					break;
				case 'tpurge':
					//add id list to params
					params.id = dojo.map(hi.items.manager.get(function(x) {
						return x.completed == 1 && x.permission >= Hi_Permissions.EVERYTHING;
					}), function (item) {
						return item.id;
					});
					
					var removePurged = function() {
						var sharedTasks = 0;
						var items = hi.items.manager.get();
						
						for (var i=0,ii=items.length; i<ii; i++) {
							if (parseInt(items[i].recurring)) {
								if (items[i].permission >= Hi_Permissions.EVERYTHING) {
									if (items[i].instances) {
										var instances = items[i].instances,
											instance = null,
											k = 0,
											kk = instances.length;
										
										for (; k<kk; k++) {
											instance = items[i].instances[k];
											if (instance.status == 1) {
												//instance['status'] = 2;
												/* Have not tested
												console.log('set',[instance.task_id,instance.start_date]);
												hi.items.manager.set({
													id:items[i].taskId,
													instances:[{
														'status':2,
														"start_date": instance.start_date,
														'start_date_time':instance.start_date_time,														
														"task_id": items[i].taskId
													}]
												});
												*/
												//hi.items.manager.setSave(items[i].id,{status:2,instance_date_time:instance.instance_date_time||instance.start_date_time});
												//hi.items.manager.removeSave(items[i].id, {"instance_date_time": instance.instance_date_time||instance.start_date_time});
												//hi.items.manager.setSave(items[i].id,{'instance_date':instance.start_date,instances:[instance]});
											}
										}
									}
								}
							} else if (items[i].completed == '1') { 
								if (items[i].permission >= Hi_Permissions.EVERYTHING) {
									
									if (Hi_Timer.isTimedTask(items[i].id)) {
										Hi_Timer.stop();
									}
									
									hi.items.manager.remove(items[i].id);
									
								} else {
									sharedTasks++;
								}
							}
						}
						
						HiTaskCalendar.update();
						Hi_Reminder.resetAllReminders();
						
						if (sharedTasks > 0) {
							Project.alertElement.showItem(7);
						}
					};
					if (Project.faster) {
						removePurged();
					}
					func2 = function() {
						if (!Project.faster) {
							removePurged();
						}
					};
					errFunc = function(data, options, silent) {
						if (!silent) {
							alert('Completed tasks were not removed. Try again later.');
						}
						if (Project.faster) {
							//Project.load(false, true);
						}
					};
					break;
				case 'tarchivepurge':
					//add id list to params
					// #2569
					// check that Archive completed not archiving Recurring
					params.id = dojo.map(hi.items.manager.get(function(x) {
						return x.completed == 1 && x.recurring == 0 && x.permission >= Hi_Permissions.ARCHIVE;
					}), function (item) {
						return item.id;
					});
					
					var removeArchived = function() {
						var notAllowedTasks = 0;
						var items = hi.items.manager.get();
						
						for (var i=0,ii=items.length; i<ii; i++) {
							if (parseInt(items[i].recurring)) {
								if (items[i].permission >= Hi_Permissions.ARCHIVE) {
									if (items[i].instances) {
										var instances = items[i].instances,
											instance = null,
											k = 0,
											kk = instances.length;
										
										for (; k<kk; k++) {
											instance = items[i].instances[k];
											if (instance.status == 1) {
												/* Bug #1203 hi.items.manager.removeSave(items[i].id, {"instance_date": instance.start_date});*/
											}
										}
									}
								}
							} else if (items[i].completed == '1') { 
								if (items[i].permission >= Hi_Permissions.ARCHIVE) {
									
									if (Hi_Timer.isTimedTask(items[i].id)) {
										Hi_Timer.stop();
									}
									
									hi.items.manager.remove(items[i].id);
									
								} else {
									notAllowedTasks++;
								}
							}
						}
						
						HiTaskCalendar.update();
						Hi_Reminder.resetAllReminders();
						
						if (notAllowedTasks > 0) {
							Project.alertElement.showItem(29, {task_alert: hi.i18n.getLocalization('completed.could_not_archive')});
						}
					};
					if (Project.faster) {
						removeArchived();
					}
					func2 = function() {
						if (!Project.faster) {
							removeArchived();
						}
					};
					errFunc = function(data, options, silent) {
						if (!silent) {
							alert('Completed tasks were not archived. Try again later.');
						}
						if (Project.faster) {
							//Project.load(false, true);
						}
					};
					break;
					
//				case 'itemlist':
//					params.depth = 20;
//					break;
			}
			
			
			//Request method
			var oldApiMethod = 'post';
			var getRequests = [
				'tlist', 'frlist',
				'thistory', 'thistory_archive',   
				'timezone' 
			];
			if (in_array(getRequests, url)) {
				oldApiMethod = 'get';
			}
			//API calls have additional parameters
			var apiRequests = [
				'feedback' 
			];
	
			var api_url = '';
			if(hiApiResponse.apiRequests[url]){
				
				
				//console.warn("*** " + time2strAPI(new Date()) + " start API #" + url + "#");
				
				
				// params cast for API2
				params = dojo.clone(params);
				var arrayToAPI2Serialize = function(array) {
					if (!dojo.isArray(array)) return null;
					return array.join(',');
				};
				var webToApi2Normalize = function(params){
					
					//start_date + start_time and end_date + end_time split
					//if(params.is_all_day && Number(params.is_all_day)){
					if((params.is_all_day != undefined && Number(params.is_all_day)) || (params.is_all_day == undefined && params.id && Project.data[params.id] && Project.data[params.id].is_all_day) && Number(Project.data[params.id].is_all_day)){
						//start date
						if(params.start_date){
							params.start_date = time2strAPI(HiTaskCalendar.getDateFromStr(params.start_date, true));
						}
						// end date
						if(params.end_date){
							params.end_date = time2strAPI(HiTaskCalendar.getDateFromStr(params.end_date, true));
						}
					}else{
						//start date
						if(params.start_time){
							if(params.start_date){
								params.start_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(params.start_date, params.start_time, true));
							}else{
								params.start_date = time2strAPI(HiTaskCalendar.getTimeFromStr(params.start_time, true));
							}
						}else if(params.start_date){
							params.start_date = time2strAPI(HiTaskCalendar.getDateFromStr(params.start_date, true));
						}
						// end date
						if(params.end_time){
							if(params.end_date){
								params.end_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(params.end_date, params.end_time, true));
							}else{
								params.end_date = time2strAPI(HiTaskCalendar.getTimeFromStr(params.end_time, true));
							}
						}else if(params.end_date){
							params.end_date = time2strAPI(HiTaskCalendar.getDateFromStr(params.end_date, true));
						}
					}
					if(params.start_time != undefined){delete params.start_time;}
					if(params.end_time != undefined){delete params.end_time;}
					
					var _params = {};
					for(var prm in params){
						
						if(dojo.isArray(params[prm])){
							// serialize array to API2 input array
							_params[prm] = arrayToAPI2Serialize(params[prm]);
						}else{
							_params[prm] = params[prm];
						}
						
						if(prm == 'shared'){
							if(Project.account_level < 100 && !Project.businessId){
								delete _params[prm];
							}else if(!_params[prm]){
								_params[prm] = 0;
							}
						}
						if(prm == 'participants'){
							if(Project.account_level < 100 && !Project.businessId){
								delete _params[prm];
							}
						}
						if(prm == 'assignee'){
							if(!_params[prm]){
								_params[prm] = 0;
							}
						}
						if(prm == 'tag'){
							_params['tags'] = _params[prm];
							delete _params[prm];
						}
					}
					return _params;
				};
				if(url == 'itemnewmulti'){
					//Transform data
					var new_params = {};
					for(i=0,ii=params.length; i<ii; i++) {
						params[i] = webToApi2Normalize(params[i]);
						for(var k in params[i]) {
							new_params['item<' + i + '>_' + encodeURIComponent(k)] = params[i][k];
						}
					}
					params = new_params;
				}else{
					params = webToApi2Normalize(params);
				}
				
				params['api_key'] = Project.api2_key;
				params['api_ver'] = Project.api_version;
				api_url = '/' + Project.api2_url_index + '/' + hiApiResponse.apiRequests[url].controller;
				if(hiApiResponse.apiRequests[url].method){
					api_url = api_url + '/' + hiApiResponse.apiRequests[url].method;
				}
			} else {
				
				if (in_array(apiRequests, url)) {
					path = 'api';
					params['json'] = 1;
					params['apiKey'] = Project.api_key;
					params['apiVersion'] = Project.api_version;
				}
	
				params[Project.session_name] = Project.session_id;
	
				api_url = '/' + path + '/' + url;
			}
			
//			//Serialize params into current _params list
//			var i, j = false, pars = '';
//			var tmp;
//			var _params = {};
//			if (typeof params == 'object') {
//				for (i in params) {
//					if (dojo.isArray(params[i])) {
//						_params[i] = serializePg(params[i]);
//					} else {
//						_params[i] = params[i];
//					}
//				}
//			} else {
//				// not allowed anymore
//				return;
//			}
			var _params = params;
			
			//Session ID
			if(hiApiResponse.apiRequests[url]){
				_params[Project.api2_session_name] = Project.api2_session_id;
			}else{
				_params[Project.session_name] = Project.session_id;
			}
			
			//Order parameter
			var paramOrder = ['update', 'updatefull', 'itemlist'];
			if (in_array(paramOrder, url)) {
				var orderQuery = '';
				if(Project.getSorting() == Project.ORDER_SUBJECT){
					orderQuery = 'title';
				}else if(Project.getSorting() == Project.ORDER_START_DATE){
					orderQuery = 'startDate,startTime';
				}else if(Project.getSorting() == Project.ORDER_LAST_MODIFIED){
					orderQuery = '-timeLastUpdate';
				}else if(Project.getSorting() == Project.ORDER_PRIORITY){
					orderQuery = 'priority';
				}else if(Project.getSorting() == Project.ORDER_DUE_DATE){
					orderQuery = 'endDate,endTime';
				}
				
				_params['orderby'] = orderQuery;
			}
			
			//Depth parameter
			var paramDepth = ['update', 'updatefull'];
			if (in_array(paramDepth, url)) {
				_params['depth'] = 10;
			}
			
			
			
			//Filter parameter
			var paramFilter = ['updatefull', 'update', 'itemlist'];
			
			if (typeof Hi_TaskFilter != 'undefined' && Hi_TaskFilter) {
				if (Hi_TaskFilter.hasFilter() && in_array(paramFilter, url)) {
					_params['filter'] = Hi_TaskFilter.getFilter();
				}
			}
			
			var properties = {
				url: Project.api2_base_url + api_url,
				load: options.noResponse ? function(){} : function(data, evt) {
					Project.ajaxGet(data, function(data, options) {
						func2(data, options, evt);
						func(data, options, evt);
					}, function(data,options,silent) {
						errFunc(false, options,silent);
					}, url, options, call_arguments, evt);
				},
				content: {},
				error: options.noResponse ? function(){} : function(data, evt) {
					if (evt.xhr) evt.xhr.abort();
					
					////Handle interval server error, status code 500
					//if (evt.xhr.status == 500){}
					
					if(data.responseText){
						data = data.responseText;
					}else{
						data = false;
					}
					
					Project.ajaxGetError(data, function(data, options, dataResponse) {
						func2(data, options, dataResponse);
						func(data, options, dataResponse);
					}, function(data,options,silent) {
						errFunc(false, options,silent);
					}, url, options, call_arguments);
				},
				failOk: true
			};
			
			//Webapp Version parameter
			//compares to legacy method names
			var paramVersion = ['listcontacts', 'listmessages', 'update', 'refreshItems'];
			if (in_array(paramVersion, url) && !Project.doNotSendWebappVersionHeader) {
				properties.headers = {'x-webapp2-version':Project.VERSION};
			}
			//If request is from queue
			if (url in ConnectionQueue.requests && ConnectionQueue.list.length && fromQueue) {
				properties.timeout = ConnectionQueue.REQUEST_TIMEOUT * 1000;
			}

			if (hiApiResponse.apiRequests[url]) {
				if (hiApiResponse.apiRequests[url].responseMethod.toLowerCase() == 'get') {
					properties.content = _params;
					return dojo.xhrGet(properties);
					
				} else if (hiApiResponse.apiRequests[url].responseMethod.toLowerCase() == 'put') {
					properties.postData = dojo.objectToQuery(_params);
					dojo.xhrPut(properties);
				} else if (hiApiResponse.apiRequests[url].responseMethod.toLowerCase() == 'delete') {
					properties.postData = dojo.objectToQuery(_params);
					dojo.xhrDelete(properties);
				} else {
					properties.postData = dojo.objectToQuery(_params);
					dojo.xhrPost(properties);
				}
			} else {
				if (oldApiMethod == 'get') {
					properties.content = _params;
					dojo.xhrGet(properties);
				} else {
					properties.postData = dojo.objectToQuery(_params);
					dojo.xhrPost(properties);
				}
			}
		},
		
		retryQueuedRequests: function () {
			if (ConnectionQueue.list.length && !ConnectionQueue.running) {
				ConnectionQueue.running = true;
				
				//Make sure timeout is removed
				clearTimeout(ConnectionQueue.timer);
				
				var req = [].concat(ConnectionQueue.list[0]);
				
				//fromQueue = true
				req[6] = true;
				
				if (req[0] == 'itemnew') {
					Project.newItem = (Project.newItemQueue.length ? Project.newItemQueue[0] : false);
				}
				
				Project.ajax.apply(Project, req);
			}
		},
		
		ajaxGetError: function (data, func, errFunc, url, options, call_arguments) {
			Project.ajaxGet.timeout = false;
			options = options || {};
			
			if (data === '') {
				//Server error occured
				errFunc(false, options, true);
				return;
			}
			
			if (data == '') {
				//Timeout
				if (url in ConnectionQueue.requests) {
					//On timeout add request to queue
					var fromQueue = call_arguments[6];
					if (!fromQueue) {
						//If not already in queue, then add
						ConnectionQueue.list.push(call_arguments);
						ConnectionQueue.update();
					
						if (url == 'itemnew' || url == 'projectdelete' || url == 'pdelete' || url == 'ptdelete') {
							//Make sure new item is not there until it's created on server
							//and has ID
							Project.opened = false;
							Project.openEdit = false;
							
							//setTimeout(fillTasks, 0);
						}
						
					} else {
						ConnectionQueue.running = false;
					}
					
					//Retry after 15 seconds
					clearTimeout(ConnectionQueue.timer);
					ConnectionQueue.timer = setTimeout(Project.retryQueuedRequests, Project.TIMEOUT_RETY_INTERVAL);
					
					//Call error
					Project.ajaxGet.timeout = true;
					errFunc(false, options, true);
				} else {
					//Timeout
					Project.ajaxGet.timeout = true;
					errFunc(false, options);
					ALERT('Request timeout');
				}
	
				return;
			}
			
			if (data == 'SEC') {
				//location.href = '/logout?' + Project.session_name + '=' + Project.session_id;
				return;
			}
			if (data.indexOf('TRIAL_') == 0) {
				errFunc(false, null, true);
				Project.trialWarning(data);
				return;
			}
			
			var ex;
			var plain = data;
			var dataResponse;
					
			if (options.response != 'plain') {
				try {
					if(hiApiResponse.apiRequests[url]){
						dataResponse = new hiApiResponse();
						dataResponse.init(url,data,options);
					}else{
						dataResponse = eval('(' + data + ')');
					}
					
				} catch (ex) {
					//Wrong response
					//console.log(ex);
					errFunc(false, options);
					ALERT('Exception while eval-ing response data - ' + ex);
					return;
				}
			}
	
			//Webapp update reuired alert/refresh:
			//When server responds with VERSION, show webapp version update alert and automatic refresh
			//If error code is 23: "version upgrade needed" then proceed
			if	(dataResponse.getStatus() == 23){
				/* Kill interval timers  */
				clearIntervalTimers();
				CONSTANTS.taskRefreshInterval = 86400;	//1 day
				Project.alertElement.showItem(6, {version: data.substr(8)});
				setTimeout(function () {
					dojo.global.showThrobber();
					window.document.location.reload();
				}, 20000);
				return;
			}
			
			//Request which has its own error handling
			var custom_error_handling = ['fradd','fradd_business','listcontacts','projectnew','mchange','filedownloadurl', 'logout'];
			
			// FIXME: API2. remove this condition "Project.api2Requests[url]" 
			// when transition to api2 will be completed
			if(hiApiResponse.apiRequests[url]){
				if (!options.ignore_error && (!dataResponse.response || (dataResponse.getStatus() > 0)) && dojo.indexOf(custom_error_handling, url) == -1) {
					//Timeout
					errFunc(false, options, dataResponse);
					
					if (dataResponse.getErrorMessage()) {
						ALERT(dataResponse.getErrorMessage());
					}
					if (dataResponse.getStatus()) {
						// FIXME: API2. which response_status must use in this case?
						if (dataResponse.getStatus() == 7) {
							logOut(null, true);
							dojo.global.isSessionEnd = true;
							//location.href = '/logout?' + Project.session_name + '=' + Project.session_id;
							if (dojo.global.stayOnPage !== true && dojo.global.inOnBeforeUnload !== true) {
								location.href = '/logout';
							}
							return;
						}
					}
					
					return;
				}
				func(dataResponse, options);
			}else{
				if (!options.ignore_error && dataResponse && dataResponse.error && dojo.indexOf(custom_error_handling, url) == -1) {
					//Timeout
					errFunc(false, options);
					
					if (dataResponse.response_msg) {
						ALERT(dataResponse.response_msg);
					}
					if (dataResponse.response_status) {
						if (dataResponse.response_status == 2) {
							//location.href = '/logout?' + Project.session_name + '=' + Project.session_id;
							location.href = '/logout';
							return;
						}
					}
					
					return;
				}
				func(dataResponse, options);
			}
		},
		
		ajaxGet: function (data, func, errFunc, url, options, call_arguments, ajaxEvt) {
			//Request was successful, proceed with queue
			var fromQueue = call_arguments ? call_arguments[6] : false,
				tnewRequest = call_arguments ? call_arguments[0] : false;
			
			if (fromQueue) {
				ConnectionQueue.running = false;
				
				//If current request was from queue, remove it
				ConnectionQueue.list.shift();
				ConnectionQueue.update();
				
				if (tnewRequest) {
					//If current request was itemnew, remove it
					Project.newItemQueue.shift();
				}
				
				clearTimeout(ConnectionQueue.timer);
				
				if (ConnectionQueue.list.length) {
					//Try running next request
					ConnectionQueue.timer = setTimeout(Project.retryQueuedRequests, 0);
				} else {
					//Since request was from queue, make sure all data are correct
					//setTimeout(function () { Project.load(null, true); }, 0);
				}
			} else {
				//Connection back on-line, check if there's anything in queue
				if (ConnectionQueue.list.length) {
					clearTimeout(ConnectionQueue.timer);
					ConnectionQueue.timer = setTimeout(Project.retryQueuedRequests, 0);
				}
			}
			
			var dataResponse;
			if (options.response != 'plain') {
				try {

					if(hiApiResponse.apiRequests[url]){
						dataResponse = new hiApiResponse();
						dataResponse.init(url,data,options);
					}else{
						dataResponse = eval('(' + data + ')');
					}
					
				} catch (ex) {
					//Wrong response
					//console.log(ex);
					errFunc(false, options);
					ALERT('Exception while eval-ing response data - ' + ex);
					return;
				}
			}else{
				dataResponse = data;
			}
			
			//Ok
			func(dataResponse, options, ajaxEvt);
		},
		
		timeZone: function() {
			//return;
			var time_zone = Hi_Preferences.get('time_zone');
			if (time_zone == '') {
				var d1 = new Date(2030, 0, 30, 23, 59, 59, 0);
				var d2 = new Date(2030, 6, 30, 23, 59, 59, 0);
				var d3 = new Date(2031, 0, 30, 23, 59, 59, 0);
				
				var w = d1.getTimezoneOffset();
				var s = d2.getTimezoneOffset();
				var w2s = '';
				var s2w = '';
				if (w != s) {
					while (d1.getTimezoneOffset() != s) {
						d1.setDate(d1.getDate() + 1);
					}
					w2s = strPadLeft(d1.getMonth() + 1, 2, '0') + '-' + strPadLeft(d1.getDate(), 2, '0');
					while (d2.getTimezoneOffset() != w) {
						d2.setDate(d2.getDate() + 1);
					}
					s2w = strPadLeft(d2.getMonth() + 1, 2, '0') + '-' + strPadLeft(d2.getDate(), 2, '0');
				}
				Project.ajax('timezone', {winterOffset: - 60 * w, summerOffset: - 60 * s, winterToSummer: w2s, summerToWinter: s2w}, Project.receiveTimeZone);
			}
		},
		receiveTimeZone: function(data) {
			if (typeof(data) == 'object' && data.isStatusOk()) {
				data = data.getData().time_zone;
				var old_time_zone = Hi_Preferences.get('time_zone');
				var time_zone = data;
				if (old_time_zone == '') {
					Hi_Preferences.set('time_zone', time_zone);
					Hi_Preferences.send();
				} else {
					// store only locally (session timezone)
					Hi_Preferences.preferences['time_zone'] = time_zone;
					// need to reload the task list if the timezone has been changed
					if (old_time_zone != time_zone) {
						//Project.load();
					}
				}
			}
		},
		
		updateDocumentTitle: function () {
			// summary:
			//		Update page title to "HiTask (NN)" where NN is users task count
			//		which are not completed
			
			var count = 0,
				data = hi.items.manager.get(),
				cat = null,
				myself = Project.user_id,
				i = 0,
				ii = data.length;
			
			for(; i<ii; i++) {
				//Not completed tasks only
				cat = data[i].category;
				if (cat && cat != '0' && cat != 5 && !data[i].completed) {
					var shared = data[i].shared;
					shared = typeof(shared) == 'boolean' ? shared : parseInt(shared);
					
					if (shared) {
						if (data[i].assignee == myself) count++;
					} else {
						if (!data[i].assignee || data[i].assignee == myself) count++;
					}
					
				}
			}
			hiFavico.badge(count);
		}
		
	});

	return Project;

});