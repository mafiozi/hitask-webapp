/**
 * Instantiation of task lists (and the groups that contain them) on Date tab:
 * hi.items.TaskListOverdue -> overdue tasks
 * hi.items.TaskListToday -> tasks for today
 * hi.items.TaskListTomorrow -> tasks for tomorrow
 * hi.items.TaskListDuringWeek -> tasks coming up within the next 7 days
 * hi.items.TaskListAfterWeek -> tasks coming up after this week
 * hi.items.TaskListDateUngrouped -> undated tasks
 */
define([
	'hi/store',
	'hi/items/base',
	'dojo/aspect',
	'hi/widget/task/item_DOM',
	'hi/items/date'
], function (hi_store, hi_items, dojo_aspect, item_DOM) {
	
	var getDayDiff = function (data) {
		// summary:
		//		Returns difference in days between task date and now
		//		If task is overdue then returns -1
		//		If task doesn't have a date then returns null
		// data: Object
		//		Task data
		/* 
		 * #2538 (after ticket was closed) To check overdue:
		 * 1) if end_date, truncate to midnight, truncate today to midnight, compare times
		 * 2) else do the same thing to start_date
		 * 3) else it is not overdue because it does not have specified dates
		 */
		if (data.end_date && data.end_date !== '') {
			/* #2797 */
			var eD;
			if (data.recurring) {
				//eD=hi.items.date.getEventDate(data.id);
				eD = time2strAPI(hi.items.date._getNextRecurrenceDate(data));
			} else {
				eD = data.end_date;
			}
			if ( hi.items.date.truncateTime(new Date(eD)).getTime() < hi.items.date.truncateTime(new Date()).getTime() ) return -1;
		} else if (data.start_date && data.start_date !== '') {
			/* #2797 */
			var sD;
			if (data.recurring) {
				//sD=hi.items.date.getEventDate(data.id);
				sD =time2strAPI(hi.items.date._getNextRecurrenceDate(data));
			} else {
				sD = data.start_date;
			}
			if ( hi.items.date.truncateTime(new Date(sD)).getTime() < hi.items.date.truncateTime(new Date()).getTime() ) return -1;
		}
		/*
		if (Hi_Overdue.isOverdue(data.id)) {
			var overdue_time = hi.items.date.getEventEndTime(data.id, '23:59');
			if (!overdue_time) {
				overdue_time = hi.items.date.getEventStartTime(data.id, '23:59');
				console.log('overdue time1',overdue_time)
			}
			if (overdue_time) {
				//var d = hi.items.date.truncateTime(new Date());
				* #2590 truncating time sets the 'current' date to today at midnight - so overdue_time -d is never < 0 *
				var d = new Date();
				if (overdue_time - d < 0) {
					return -1;
				}
			}
			
			return 0;
		}
		*/
		var date_start = false,
			date_end = false,
			recurring_date = null,
			dif_s = false,
			dif_e = false,
			dif = false,
			today = hi.items.date.truncateTime(new Date()).getTime();
			
			// #2748
			//today = new Date().getTime();/* #2742 - Don't truncate, same as #2590 hi.items.date.truncateTime(new Date()).getTime();*/
		
		if (data.end_date && data.end_date !== null && data.end_date !== '') {
			date_end = str2time(data.end_date);
			date_end = new Date(date_end.getTime() - 1);
			// date_end = str2time(data.end_date, 'y-m-d');
			date_end.setMinutes(0);
			date_end.setSeconds(0);
			date_end.setHours(0);
			date_end = date_end.valueOf();
		}
		if (data.start_date && data.start_date !== null && data.start_date !== '' && !parseInt(data.recurring)) {
			var item = HiTaskCalendar.nearestEvent(data);
			if (item.startDate) {
				date_start = item.startDate.valueOf();
			} else {
				date_start = false;
			}
			// if (item.endDate) {
			// 	date_end = item.endDate.valueOf() - 1;
			// } else {
			// 	date_end = false;
			// }
		}
		
		if (parseInt(data.recurring)) {
			date_end = hi.items.date.getEventEndTime(data.id, '23:59');
			date_start = hi.items.date.getEventStartTime(data.id, '23:59');
			
			if (date_end) date_end = date_end.getTime();
			if (date_start) date_start = date_start.getTime();
			
			//recurring_date = hi.items.date.getNextEventDate(data);
			/* #2493 recurring_date = data.instances.length ? hi.items.date.getEventInstanceDate(data) : null;*/
			recurring_date = hi.items.date._getNextRecurrenceDate/*_getEventInstanceDate*/(data);
			
			if (recurring_date) {
				hi.items.date.truncateTime(recurring_date);
				
				if (date_end) {
					date_end = recurring_date.getTime();
				} else {
					date_start = recurring_date.getTime();
				}
			}
		}
		
		if (date_start || date_end) {
			if (date_start) {
				dif_s = Math.round((date_start - today) / 86400000); // 86400000 = 24 hours = 24 * 60 * 60 * 1000
				/* #2742 - if no end date, refine difference down to the millisecond */
				// #2748
				//if ((!date_end || date_end==date_start) && dif_s == 0 && date_start < today) dif_s = -1;
				/* #2473 - ditto */
				// #2748
				//if ((!date_end || date_end==date_start) && dif_s == 0 && date_start > today) dif_s = 1;
			}
			if (date_end) {
				dif_e = Math.round((date_end - today) / 86400000);
				/* #2743 */
				//if (dif_e == 0 && date_end > today) dif_e = 1;
			}
			
			if (dif_e !== false && dif_s !== false) {
				if (dif_e < 0 && dif_s < 0) {
					dif = Math.max(dif_e, dif_s);
				} else if (dif_e > 0 && dif_s > 0) {
					dif = Math.min(dif_e, dif_s);
				} else {
					dif = dif_e;
				}
			} else if (dif_e !== false) {
				dif = dif_e;
			} else if (dif_s !== false) {
				dif = dif_s;
			}
			
			//Show yearly recurring events only 10 days prior
			if (data.recurring == 4) {
				if (dif !== false) {
					 if (dif_s !== false && dif_s <= 10) {
					 } else if (dif_e !== false  && dif_e <= 10) {
					 } else {
					 	return null;
					 }
				}
			}
			
			return dif;
		} else {
			return null;
		}
	};
	//After hi.store.init create date groups
	dojo.global.viewDate = function(q) {
	//dojo.connect(hi_store, "afterInit", hi_store, function () {
		// Create task groups (Overdue, Today, Tomorrow, etc.) and set filters
		
		var readStore = hi_store.ProjectCache,
			writeStore = hi_store.Project,
			query = q,//readStore.query(),
			groupSelf = 'date',
			groupCurrent = Hi_Preferences.get('currentTab', 'my', 'string');
		
		if (groupCurrent == groupSelf) {
			var myD = dojo.connect(hi_store,'afterLoad', function() {
				// Add to container
				if (groupSelf == groupCurrent) {
					finalizeInitialization([hi_items.GroupToday,hi_items.GroupTomorrow,hi_items.GroupDuringWeek,hi_items.GroupAfterWeek],[hi_items.TaskListDateUngrouped]);
				}
				dojo.disconnect(myD);
			});
		}
		
		// Task groups
		hi_items.GroupToday = new hi.widget.Group({
			"projectId": "today",
			"emptyVisible": true,
			"expanded": true,
			
			"title": hi.i18n.getLocalization("center.today"),
			"moveTitle": hi.i18n.getLocalization('center.today'),
			"groupClassName": "today",
			
			"disabled": groupSelf != groupCurrent,
			"preferencesGroup": groupSelf
		});
		
		hi_items.GroupTomorrow = new hi.widget.Group({
			"projectId": "tomorrow",
			"emptyVisible": true,
			"expanded": true,
			
			"title": hi.i18n.getLocalization("center.tomorrow"),
			"moveTitle": hi.i18n.getLocalization('center.tomorrow'),
			"groupClassName": "day",
			
			"disabled": groupSelf != groupCurrent,
			"preferencesGroup": groupSelf
		});
		
		hi_items.GroupDuringWeek = new hi.widget.Group({
			"projectId": "during_week",
			"emptyVisible": true,
			"expanded": true,
			
			"title": hi.i18n.getLocalization("center.during_week"),
			"moveTitle": "",
			"groupClassName": "day",
			
			"disabled": groupSelf != groupCurrent,
			"preferencesGroup": groupSelf
		});
		
		hi_items.GroupAfterWeek = new hi.widget.Group({
			"projectId": "after_week",
			"emptyVisible": true,
			"expanded": true,
			
			"title": hi.i18n.getLocalization("center.after_week"),
			"moveTitle": "",
			"groupClassName": "day",
			
			"disabled": groupSelf != groupCurrent,
			"preferencesGroup": groupSelf
		});
		
		hi.items.DateGroupsList = [hi.items.GroupAfterWeek, hi.items.GroupDuringWeek, hi.items.GroupTomorrow, hi.items.GroupToday];
		
		// Task lists
		hi_items.TaskListToday = new hi.widget.TaskList({
			"title": 'today',
			"readStore": readStore,
			"writeStore": writeStore,
			"query": query,//groupSelf == groupCurrent?query:null,
			"disabled": groupSelf != groupCurrent,
			showing:groupSelf == groupCurrent?true:false,
			"filterType": "any", // If item or any of the parent matches filter then this item can be shown in this list
			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				if (item.recurring) {
					return this.myCalculateInstanceDate(item);
				}
				if (getDayDiff(item) === 0) return true;
				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			},
			myCalculateInstanceDate:function(id) {
				/*
				 * Summary:
				 * Overrides the default action of calculateInstanceDate method
				 */
				//Find first non-removed,non-completed instance date in series that occurs today
				var item = typeof id === 'object' ? id : this.readStore.get(id),
					today = new Date(),
					series;
					
				series = hi.items.date.generateRecurrenceSeries(item,time2str(today,'y-m-d'),time2str(today,'y-m-d'));
				if (series && series.length > 0)
					return series[0];
				else
					return null;
			}
		});
		
		hi_items.TaskListTomorrow = new hi.widget.TaskList({
			"title": 'tomorrow',
			"readStore": readStore,
			"writeStore": writeStore,
			"query": query,//groupSelf == groupCurrent?query:null,
			"disabled": groupSelf != groupCurrent,
			showing:groupSelf == groupCurrent?true:false,
			"filterType": "any", // If item or any of the parent matches filter then this item can be shown in this list
			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				if (item.recurring) {
					return this.myCalculateInstanceDate(item);
				}
				if (getDayDiff(item) == 1) return true;
				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			},
			myCalculateInstanceDate: function(id) {
				/*
				 * Summary:
				 * Overrides the default action of calculateInstanceDate method
				 */
				//Find first non-removed,non-completed instance date in series that occurs 1 day from today
				var item = typeof id === 'object' ? id : this.readStore.get(id),
					tomorrow = new Date(),
					series;
					
				tomorrow.setDate(tomorrow.getDate() + 1);
				series = hi.items.date.generateRecurrenceSeries(item,time2str(tomorrow,'y-m-d'),time2str(tomorrow,'y-m-d'));
				if (series && series.length>0)
					return series[0];
				else
					return null;
			}
		});
		
		hi_items.TaskListDuringWeek = new hi.widget.TaskList({
			"title": 'duringWeek',
			"readStore": readStore,
			"writeStore": writeStore,
			"query": query,//groupSelf == groupCurrent?query:null,
			showing:groupSelf == groupCurrent?true:false,
			"disabled": groupSelf != groupCurrent,
			"filterType": "any", // If item or any of the parent matches filter then this item can be shown in this list
			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				if (item.recurring) {
					return this.myCalculateInstanceDate(item);
				}
				var diff = getDayDiff(item);
				if (diff >= 2 && diff <= 7) return true;
				
				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			},
			myCalculateInstanceDate:function(id) {
				/*
				 * Summary:
				 * Overrides the default action of calculateInstanceDate method
				 */
				//Find first non-removed,non-completed instance date in series that occurs between 2 and 7 days from today
				var item = typeof id === 'object' ? id : this.readStore.get(id),
					twoDays = new Date(),
					sevenDays = new Date(),
					series;
				
				sevenDays.setDate(sevenDays.getDate() + 7);
				twoDays.setDate(twoDays.getDate() + 2);
				series = hi.items.date.generateRecurrenceSeries(item,time2str(twoDays,'y-m-d'),time2str(sevenDays,'y-m-d'));
				if (series && series.length>0)
					return series[0];
				else
					return null;
			}
		});
		
		hi_items.TaskListAfterWeek = new hi.widget.TaskList({
			"readStore": readStore,
			"writeStore": writeStore,
			"query": query,//groupSelf == groupCurrent?query:null,
			"disabled": groupSelf != groupCurrent,
			"showing":groupSelf == groupCurrent?true:false,
			"filterType": "any", // If item or any of the parent matches filter then this item can be shown in this list
			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				if (item.recurring) {
					return this.myCalculateInstanceDate(item);
				}
				var diff = getDayDiff(item);
				
				if (diff >= 8) return true;

				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			},
			myCalculateInstanceDate:function(id) {
				/*
				 * Summary:
				 * Overrides the default action of calculateInstanceDate method
				 */

				var item = typeof id === 'object' ? id : this.readStore.get(id);
				//Find first non-removed,non-completed instance date in series that occurs after 8 days from today
				var afterWeekDateStr = new Date(),
					series;
				
				afterWeekDateStr.setDate(afterWeekDateStr.getDate() + 8);
				series = hi.items.date.generateRecurrenceSeries(item,time2str(afterWeekDateStr,'y-m-d'));
				if (series && series.length>0) return series[0];
				else return null;
			}
		});
		
		hi_items.TaskListDateUngrouped = new hi.widget.TaskList({
			"title": 'dateUngrouped',
			"readStore": readStore,
			"writeStore": writeStore,
			"query": query,//groupSelf == groupCurrent?query:null,
			"showing": groupSelf == groupCurrent? true : false,
			"disabled": groupSelf != groupCurrent,
			"filterType": "top", // if most outer parent matched then this item should be in the list
			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				// strict:
				//		If true then don't check ancestors
				/* #2555 if (item.recurring) return false;*/
				
				/* #3049 */
				// if (item.recurring) return false;

				var dayDiff = getDayDiff(item);
				if (him.isEvent(item)) {
					if (dayDiff < 0) return true;
				}

				if (dayDiff === null) return true;
				
				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			}
		});
		
		// Add to container
		
		var container = dojo.byId(Project.getContainer('tasksList'));
		
		hi_items.GroupToday.placeAt(container);
		hi_items.TaskListToday.placeAt(hi_items.GroupToday);
		
		hi_items.GroupTomorrow.placeAt(container);
		hi_items.TaskListTomorrow.placeAt(hi_items.GroupTomorrow);
		
		hi_items.GroupDuringWeek.placeAt(container);
		hi_items.TaskListDuringWeek.placeAt(hi_items.GroupDuringWeek);
		
		hi_items.GroupAfterWeek.placeAt(container);
		hi_items.TaskListAfterWeek.placeAt(hi_items.GroupAfterWeek);
		
		hi_items.TaskListDateUngrouped.placeAt(container);
		
		dojo.connect(hi.items.manager, "groupingChange", hi.items.manager, function (group, groupPrev) {
			// summary:
			//		On group change enable/disable lists
			
			if (group == groupSelf && groupPrev != groupSelf) {
				refreshTab([hi_items.GroupToday,hi_items.GroupTomorrow,hi_items.GroupDuringWeek,hi_items.GroupAfterWeek],[hi_items.TaskListDateUngrouped]);
			} else if (group != groupSelf && groupPrev == groupSelf) {
				hi_items.GroupToday.set("disabled", true);
				hi_items.GroupTomorrow.set("disabled", true);
				hi_items.GroupDuringWeek.set("disabled", true);
				hi_items.GroupAfterWeek.set("disabled", true);
				hi_items.TaskListDateUngrouped.set("disabled", true);
			}
			
			groupCurrent = group;
		});
		
		dojo.connect(Project, "changeSorting", Project, function () {
			// summary:
			//		On sorting change empty lists
			
			if (groupCurrent == groupSelf) {
				/* #2509 
				hi_items.TaskListOverdue.reset({"fill": false});
				hi_items.TaskListToday.reset({"fill": false});
				hi_items.TaskListTomorrow.reset({"fill": false});
				hi_items.TaskListDuringWeek.reset({"fill": false});
				hi_items.TaskListAfterWeek.reset({"fill": false});
				hi_items.TaskListDateUngrouped.reset({"fill": false});
				*/
				hi_items.GroupToday.set("disabled", true);
				hi_items.GroupTomorrow.set("disabled", true);
				hi_items.GroupDuringWeek.set("disabled", true);
				hi_items.GroupAfterWeek.set("disabled", true);
				hi_items.TaskListDateUngrouped.set("disabled", true);
			}
		});
		
		dojo.connect(hi.items.manager, "filterChange", hi.items.manager, function () {
			// summary:
			//		On sorting change empty lists
			/* #2509 
			if (groupCurrent == groupSelf) {
				hi_items.TaskListOverdue.reset({"fill": false});
				hi_items.TaskListToday.reset({"fill": false});
				hi_items.TaskListTomorrow.reset({"fill": false});
				hi_items.TaskListDuringWeek.reset({"fill": false});
				hi_items.TaskListAfterWeek.reset({"fill": false});
				hi_items.TaskListDateUngrouped.reset({"fill": false});
			}
			*/
			
			if (groupCurrent == groupSelf) {
				hi_items.GroupToday.set("disabled", true);
				hi_items.GroupTomorrow.set("disabled", true);
				hi_items.GroupDuringWeek.set("disabled", true);
				hi_items.GroupAfterWeek.set("disabled", true);
				hi_items.TaskListDateUngrouped.set("disabled", true);
			}
			
		});
		
		/* This is invoked after refresh data store on filter selection change 
		 * This should just initiate the query observers on the lists and enable the lists
		 */
		dojo.connect(hi.items.manager, "redraw", hi.items.manager, function () {
			// summary:
			//		Some major change happened, redraw lists
			
			if (groupCurrent == groupSelf) {
				/* #2331 - rereshTab on searchChange */
				//if (hi_items.TaskListOverdue.get('disabled')) initiateTab([hi_items.GroupOverdue,hi_items.GroupToday,hi_items.GroupTomorrow,hi_items.GroupDuringWeek,hi_items.GroupAfterWeek],[hi_items.TaskListDateUngrouped]);
				/*else*/
				refreshTab([
					hi_items.GroupToday,
					hi_items.GroupTomorrow,
					hi_items.GroupDuringWeek,
					hi_items.GroupAfterWeek
				], [hi_items.TaskListDateUngrouped]);
			}
		});
		
		hi.items.manager.getTaskLists[groupSelf] = function () {
			// summary:
			//		Return all task lists for this group
			
			return [
				hi_items.TaskListDateUngrouped,

				hi_items.TaskListToday,
				hi_items.TaskListTomorrow,
				hi_items.TaskListDuringWeek,
				hi_items.TaskListAfterWeek,
				hi_items.TaskListDateUngrouped
			];
		};
		
	};//);
	
	dojo.global.viewDate.getDayDiff = getDayDiff;
});
