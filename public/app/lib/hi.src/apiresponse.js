/**
 * hiApiResponse object
 */
define(function () {
	
	/**
	 * hiApiResponse object
	 */
	dojo.global.hiApiResponse = function () {
		
		/**
		 * Current API command 
		 */
		this.apiCommand;
		
		/**
		 * Current response JSON handler
		 */
		this.response;
		
		/**
		 * Initialization class with input params
		 * 
		 * <p>
		 * parameters:
		 * <li>apiCommand - current API command
		 * <li>data - input JSON
		 * 
		 */
		this.init = function(apiCommand,data,options){
			this.apiCommand = apiCommand;
			this.parse(data,options);
		}
		
		/**
		 * Parse JSON API response
		 * 
		 * <p>
		 * parameters:
		 * <li>jsonResponse - input JSON data
		 * 
		 */
		this.parse = function(jsonResponse,options){
			
			if(!hiApiResponse.apiRequests[this.apiCommand]){
				
				if (options && options.response == 'plain') {
					this.response = jsonResponse;
				} else {
					this.response = eval('(' + jsonResponse + ')');
				}
				
				return;
			}
			
			this.response = eval('(' + jsonResponse + ')');
			this.apiNormalise();
			
			if(hiApiResponse.apiRequests[this.apiCommand].responseType == 'listrefresh'){
				this.expansionClass('dataItems');
				//this.expansionClass('itemsRefresh');
			}else if(hiApiResponse.apiRequests[this.apiCommand].responseType == 'listitems'){
				this.expansionClass('dataItems');
			}else if(hiApiResponse.apiRequests[this.apiCommand].responseType == 'listarchive'){
				this.expansionClass('dataListArchive');
			}else if(hiApiResponse.apiRequests[this.apiCommand].responseType == 'listbusiness'){
				this.expansionClass('dataBusiness');
			}else if(hiApiResponse.apiRequests[this.apiCommand].responseType == 'listmessages'){
				this.expansionClass('dataMessages');
			}else if(hiApiResponse.apiRequests[this.apiCommand].responseType == 'listcontacts'){
				this.expansionClass('dataContacts');
			}else if(hiApiResponse.apiRequests[this.apiCommand].responseType == 'simple'){
				this.expansionClass('dataId');
			}else if(hiApiResponse.apiRequests[this.apiCommand].responseType == 'listhistory'){
				this.expansionClass('itemHistory');
			}else if(hiApiResponse.apiRequests[this.apiCommand].responseType == 'url'){
				this.expansionClass('url');
			}
		}
		
		/**
		 * Check if current JSON API response is empty.
		 * 
		 * @return <p>
		 *         Boolean
		 *         
		 */
		this.isEmpty = function(){
		if(!this.response){
			return true;
		}else if(this.response.constructor == Array && this.response.length > 0){
			return false;
		}else if(this.response.constructor == Object){
			for(var i in this.response){
				return false;
			}
		}else if(this.response){
			return false;
		}else{
			return true;
		}
	}
		
		/**
		 * Get error message from JSON API response.
		 * 
		 * @return <p>
		 *         String
		 *         
		 */
		this.getErrorMessage = function(){
			
			var _data = '';
			
			if(this.response.error_message){
				_data = this.response.error_message;
			}
			
			return _data;
			
		}
		
		/**
		 * Get current API command.
		 * 
		 * @return <p>
		 *         String
		 *         
		 */
		this.getApiCommand = function(){
			
			var _data = '';
			
			if(this.apiCommand){
				_data = this.apiCommand;
			}
			
			return _data;
			
		}
		
		/**
		 * Get type of response object.
		 * 
		 * @return <p>
		 *         String
		 *         
		 */
		this.getResponseType = function(){
			
			var _data = '';
			
			if(hiApiResponse.apiRequests[this.apiCommand]){
				_data = hiApiResponse.apiRequests[this.apiCommand].responseType;
			}
			
			return _data;
			
		}
		
		/**
		 * Get status of response object.
		 * 
		 * @return <p>
		 *         String
		 *         
		 */
		this.getStatus = function(){
			
			var _data = null;
			
			if(this.response.response_status != undefined){
				_data = Number(this.response.response_status);
			}
			
			return _data;
			
		}
		
		/**
		 * Check if status of response object is OK.
		 * 
		 * @return <p>
		 *         Boolean
		 *         
		 */
		this.isStatusOk = function(){
			
			var _data = false;
			
			if(this.response && (this.response.response_status == undefined || this.response.response_status == 0)){
				_data = true;
			}
			
			return _data;
			
		}
		
		/**
		 * Get data from response object.
		 * 
		 * @return <p>
		 *         String
		 *         
		 */
		this.getData = function(){
			return this.response;
		}
		
		/**
		 * Extension of the class by some functions depending on the type of input data.
		 * 
		 */
		this.expansionClass = function(method){
//			if(method == 'items'){
//				dojo.mixin(this,{
//					getItems: function(){
//						return this.response.items;
//					}
//				});
//			}
			
			if(method == 'itemsRefresh'){
				dojo.mixin(this,{
					getItemsChanged: function(){
						return this.response.changed;
					},
					getItemsUnchanged: function(){
						return this.response.unchanged;
					}
				});
			}
			
			if(method == 'dataItems'){
				dojo.mixin(this,{
					getDataItems: function(){
						return this.getData();
					}
				});
			}
			
			if(method == 'dataListArchive'){
				dojo.mixin(this,{
					getListArchive: function(){
						return this.getData();
					}
				});
			}
			
			if(method == 'dataId'){
				dojo.mixin(this,{
					getDataId: function(){
						return this.response.id;
					}
				});
			}
			
			if(method == 'dataBusiness'){
				dojo.mixin(this,{
					getDataBusiness: function(){
						return this.getData();
					}
				});
			}
			
			if(method == 'dataContacts'){
				dojo.mixin(this,{
					getDataContacts: function(){
						return this.getData();
					}
				});
			}
			
			if(method == 'dataMessages'){
				dojo.mixin(this,{
					getDataMessages: function(){
						return this.getData();
					}
				});
			}
			
			if(method == 'itemHistory'){
				dojo.mixin(this,{
					getItemHistory: function(){
						return this.getData();
					}
				});
			}
	
	        if(method == 'url'){
	            dojo.mixin(this,{
	                getUrl: function() {
						return this.response.url;
	                }
	            });
	        }
		}
		
		/**
		 * Normalise input data.
		 * 
		 */
		this.apiNormalise = function(){
			if(this.isEmpty()){
				return;
			}
			
			var _funcItemNormalise = function(_itm) {
				
				// starred normalise: boolean->integer
				if (_itm.starred) {
					_itm.starred = 1;
				} else {
					_itm.starred = 0;
				}
				
				// shared normalise: boolean->integer
				if (_itm.shared) {
					_itm.shared = 1;
				} else {
					_itm.shared = 0;
				}
				
				// property tags->tag
				if (_itm.tags != undefined) {
					dojo.mixin(_itm, {tag:_itm.tags});
					delete _itm.tags;
				}else{
					dojo.mixin(_itm, {tag:[]});
				}
				
				// assineeId
				var assign_id = "";
				if(_itm.assignee != undefined){
					assign_id = (_itm.assignee>0)?_itm.assignee:"";
				}
				dojo.mixin(_itm, {assign:[String(assign_id)],assign_user:String(assign_id)});
				
				// start-end date-time
				if(_itm.is_all_day){
					if(_itm.start_date != undefined){
						var _start_d = str2timeAPI(_itm.start_date);
						_itm.start_date = time2str(_start_d, 'y-m-d');
						_itm.start_time = '';
					}
					if(_itm.end_date != undefined){
						var _end_d = str2timeAPI(_itm.end_date);
						_itm.end_date = time2str(_end_d, 'y-m-d');
						_itm.end_time = '';
					}
				}else{
					if(_itm.start_date != undefined){
						var _start_d = str2timeAPI(_itm.start_date);
						_itm.start_date = time2str(_start_d, 'y-m-d');
						_itm.start_time = time2str(_start_d, 'h:i');
					}
					if(_itm.end_date != undefined){
						var _end_d = str2timeAPI(_itm.end_date);
						_itm.end_date = time2str(_end_d, 'y-m-d');
						_itm.end_time = time2str(_end_d, 'h:i');
					}
				}
				// start-end date-time is mandatory
				if(_itm.start_date == undefined){_itm.start_date = '';}
				if(_itm.start_time == undefined){_itm.start_time = '';}
				if(_itm.end_date == undefined){_itm.end_date = '';}
				if(_itm.end_time == undefined){_itm.end_time = '';}
				
				// recurring normalize
			    if (_itm.instances) {
				    var instances = _itm.instances;
					_itm.recurring_instances = {};
				    for(var j in instances) {
					    var instance = instances[j];						
					    _itm.recurring_instances[instance.start_date] = instance;
					}
					delete _itm.instances;
				}
				
				return _itm;
	
			}
			var _funcContactsNormalise = function(_contacts) {
					var fr;
					for (fr in _contacts) {
						var name = '';
						if(_contacts[fr]['firstName']) name = _contacts[fr]['firstName'].trim();
						if(_contacts[fr]['lastName']) name = name + ' ' + _contacts[fr]['lastName'].trim();
						if(name.trim().length == 0) name = (_contacts[fr]['email'])?_contacts[fr]['email']:'no name';
						_contacts[fr]['name'] = name.trim();
					}
					
					return _contacts;
			}
			
			if(this.apiCommand == 'update'){
				// items
				if(this.response){
					var tmp_data = new Array;
					for (var itm in this.response) {
						if(this.response[itm].changed || this.response[itm].instances){
							tmp_data[itm] = _funcItemNormalise(this.response[itm]);
						}else{
							tmp_data[itm] = this.response[itm];
						}
					}
					this.response = tmp_data;
				}
			}
			
			if(this.apiCommand == 'updatefull'){
				// items
				if(this.response){
					var tmp_data = new Array;
					for (var itm in this.response) {
						tmp_data[itm] = _funcItemNormalise(this.response[itm]);
					}
					this.response = tmp_data;
				}
			}
			
			if(this.apiCommand == 'itemlist'){
				if(this.response){
					var tmp_data = new Array;
					for (var itm in this.response) {
						tmp_data[itm] = _funcItemNormalise(this.response[itm]);
					}
					this.response = tmp_data;
				}
			}
			
			if(this.apiCommand == 'listcontacts'){
				// contacts
				if(this.response){
					this.response = _funcContactsNormalise(this.response)
				}
			}
			
			if(this.apiCommand == 'tlistarchive'){
				if(this.response.items){
					var tmp_data = new Array;
					for (var itm in this.response.items) {
						tmp_data[itm] = _funcItemNormalise(this.response.items[itm]);
					}
					this.response.items = tmp_data;
				}
			}
		}
		
	}
		
	/**
	 * (static) List of available API commands.
	 * 
	 */
	hiApiResponse.apiRequests = {
		
		logout:{controller:'user',method:'logout',responseType:'base',responseMethod:'get'},
		userinfo:{controller:'user',method:'info',responseType:'base',responseMethod:'get'},
		mchange: {controller: 'user', method: '', responseType: 'simple', responseMethod: 'put'},
		
		updatefull:{controller:'list',method:'items',responseType:'listitems',responseMethod:'get'},
		update: {controller:'list',method:'refreshItems',responseType:'listrefresh',responseMethod:'get'},
		listcontacts: {controller:'contact',method:'',responseType:'listcontacts',responseMethod:'get'},
		listmessages: {controller:'list',method:'refreshMessages',responseType:'listmessages',responseMethod:'get'},
		listbusiness: {controller:'list',method:'business',responseType:'listbusiness',responseMethod:'get'},
		itemlist: {controller:'list',method:'items',responseType:'listitems',responseMethod:'get'},
		
		timelist: {controller:'timelog',method:'',responseType:'simple',responseMethod:'get'},
		timelog: {controller:'timelog',method:'',responseType:'simple',responseMethod:'post'},
		timedelete: {controller:'timelog',method:'',responseType:'simple',responseMethod:'delete'},
		
		itemnew: {controller:'item',method:'',responseType:'simple',responseMethod:'post'},
		itemnewmulti: {controller:'item',method:'newmultiple',responseType:'simplemultiple',responseMethod:'post'},
		itemsave: {controller:'item',method:'',responseType:'simple',responseMethod:'put'},
		itemassign: {controller:'item',method:'',responseType:'simple',responseMethod:'put'},
		itemdelete: {controller:'item',method:'',responseType:'base',responseMethod:'delete'},
		itemcommentdelete: {controller:'item',method:'comment',responseType:'base',responseMethod:'delete'}, 
		itemcomplete: {controller:'item',method:'',responseType:'base',responseMethod:'put'},
		itempublish: {controller:'item',method:'publish',responseType:'string',responseMethod:'post'},
		itemunpublish: {controller:'item',method:'unpublish',responseType:'base',responseMethod:'post'},
		
		tarchivepurge: {controller:'item',method:'archivemultiple',responseType:'base',responseMethod:'post'}, 
		tpurge: {controller:'item',method:'deletemultiple',responseType:'base',responseMethod:'post'}, 
		
		projectnew: {controller:'item',method:'',responseType:'simple',responseMethod:'post'},
		pshare: {controller:'project',method:'',responseType:'simple',responseMethod:'put'}, 
		projectdelete: {controller:'item',method:'',responseType:'base',responseMethod:'delete'},
		
		itemhistory: {controller:'item',method:'history',responseType:'listhistory',responseMethod:'get'}, 
		itemcomment: {controller:'item',method:'comment',responseType:'simple',responseMethod:'post'}, 
		
		tlistarchive: {controller:'list',method:'find',responseType:'listarchive',responseMethod:'get'},
		tarchive: {controller:'item',method:'archive',responseType:'base',responseMethod:'post'},
		trestore: {controller:'archive',method:'restore',responseType:'base',responseMethod:'get'},
		prestore: {controller:'archive',method:'restore',responseType:'base',responseMethod:'get'},
		tcopyrestore: {controller:'archive',method:'restorecopy',responseType:'base',responseMethod:'get'},
		pcopyrestore: {controller:'archive',method:'restorecopy',responseType:'base',responseMethod:'get'},
		
		chsend: {controller:'message',method:'',responseType:'base',responseMethod:'post'},
		chread: {controller:'message',method:'',responseType:'base',responseMethod:'put'},
		
		syncEnable: {controller: 'sync', method: '', responseType: 'base', responseMethod: 'post'},
		syncListTargets: {controller: 'sync', method: 'targets', responseType: 'base', responseMethod: 'get'},
		syncClearError: {controller: 'sync', method: 'clearError', responseType: 'base', responseMethod: 'post'},
		
	    fileupload: {controller:'file',method:'upload',responseType:'base',responseMethod:'post'},
	    filedownloadurl: {controller:'file',method:'url',responseType:'url',responseMethod:'get'},
	    filestorageinfo: {controller:'file',method:'quota',responseType:'base',responseMethod:'get'},
		
		frfind: {controller:'contact',method:'search',responseType:'base',responseMethod:'get'},
		fradd: {controller:'contact',method:'invite',responseType:'base',responseMethod:'get'},
		frdelete: {controller:'contact',method:'',responseType:'base',responseMethod:'delete'},
		frapprove: {controller:'contact',method:'approve',responseType:'base',responseMethod:'get'},
		frdecline: {controller:'contact',method:'',responseType:'base',responseMethod:'delete'},
		fradd_business: {controller:'contact',method:'invite',responseType:'base',responseMethod:'get'},
		frdelete_business: {controller:'contact',method:'removefrombusiness',responseType:'base',responseMethod:'get'},
		binv_decline: {controller:'contact',method:'declinebusiness',responseType:'base',responseMethod:'get'},
		binv_accept: {controller:'contact',method:'approveBusiness',responseType:'base',responseMethod:'get'},
		
		frinvite: {controller:'contact',method:'invite',responseType:'base',responseMethod:'get'},
		frinvite_business: {controller:'contact',method:'invite',responseType:'base',responseMethod:'get'},
		feedback: {controller:'user',method:'feedback',responseType:'simple',responseMethod:'post'},
		feedid_get: {controller:'userfeedid',method:'',responseType:'simple',responseMethod:'get'},
		feedid_reset: {controller:'userfeedid',method:'',responseType:'simple',responseMethod:'post'},
		timezone: {controller:'user',method:'timezone',responseType:'simple',responseMethod:'get'},
		prefchange: {controller:'user',method:'preferences',responseType:'simple',responseMethod:'post'}
	};
	
	return hiApiResponse;
});
