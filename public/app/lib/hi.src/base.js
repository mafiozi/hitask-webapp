/**
 * Copyright (C) 2005-2006 Vide Infra Grupa SIA, Riga, Latvia. All rights reserved.
 * http://videinfra.com
 * Version 1.1
 */
define(function () {
	
	dojo.global.CONSTANTS = dojo.mixin(dojo.global.CONSTANTS || {}, {
		duration: 200
	});
	
	dojo.global.toggleAnswer = function(id) 
	{
		var question = dojo.byId('question' + id);
		var answer = dojo.byId('answer' + id);
		if(!question) return false;
		if(!answer) return false;
		
		if(question.className != 'active')
		{
			question.className = 'active';
			answer.className = answer.className.replace(' hidden', '');
		}
		else
		{
			answer.className += ' hidden';
			question.className = '';
		}
	};
	
	dojo.global.debugHighLight = function(t){
		switch(t){
			case 'number':
				return 'red';
			case 'string':
				return 'blue';
			case 'boolean':
				return 'green';
			default:
				return 'black';
		}
	};
	
	dojo.global.objectType = function(o) {
		if (typeof o != 'object') return typeof o;
		var c = o.constructor;
		if (c.name) return c.name.toString();
		c = c.toString();
		var p1 = c.indexOf(' ');
		var p2 = c.indexOf('(');
		return c.substring(p1 + 1, p2);
	};
	
	dojo.global.debugShowVar = function(x){
		function debugShowVar_(x){
			if(x==null)return 'NULL';
			if(x.sourceIndex+''!='undefined')return '<font color=blue>[HTMLObject]</font>\n'+showHTMLObj(x,0);
			if(typeof(x.screen)!='undefined')return '<font color=red>Window</font> ['+(x.document.title)+' ]';
			if((typeof(x)=='string')&&(x.indexOf('PACKED DATA')==0))return '<font color=gray>*** '+x+' ***</font>';
			if (objectType(x)=='Array') {
				return debugShowVar_('[' + x.toString() + ']');
			}
			if (objectType(x)=='Date') {
				return debugShowVar_('[' + x.toString() + ']');
			}
			if((z=typeof(x))=='object'){
				var a=[];
				level++;
				for(var i in x)
					if('function'!=typeof(x[i]))
						a[a.length]=''+debugShowVar_((String(i).match(/^\d+$/g))?parseInt(i):i)+' => '+debugShowVar_(x[i])+',';
					else {
						var func = x[i].toString();
						var pos = func.indexOf('(');
						a[a.length]=''+debugShowVar_((String(i).match(/^\d+$/g))?parseInt(i):i)+' => function '+debugShowVar_(func.substring(9, pos))+',';
					}
				var t='';
				for(var i=level;i--;)
					t+=ts;
				level--;
				return 'Array\n'+t+'(\n'+t+(a.length?ts+a.join('\n'+t+ts)+'\n'+t:'')+')';
			}
			return (z=='string'?'\'':'')+'<font color=' + debugHighLight(z) + '>'+htmlspecialchars(x)+'</font>'+(z=='string'?'\'':'');
		}
		var level=-1;
		var ts='   ';
		return '<pre style="font-size:10;margin-bottom:0">'+debugShowVar_(x)+'</pre>';
	};
	
	dojo.global.ALERT = function() {
		if (typeof(console) != 'undefined' && typeof(console.debug) == 'function') {
			for (var i = 0; i < arguments.length; i++) {
				Project.isDevMode && console.debug('DEBUG_CHILD: %s', arguments[i]);
			}
		}
		return;
		
		var t2 = new Date();
		if (typeof DEBUG_CHILD == 'undefined') {
			DEBUG_CHILD = document.createElement("DIV");
			DEBUG_CHILD.style.position = 'absolute';
			DEBUG_CHILD.style.top = '0px';
			DEBUG_CHILD.style.left = '0px';
			DEBUG_CHILD.style.width = '500px';
			DEBUG_CHILD.style.height = '10px';
			DEBUG_CHILD.style.overflow = 'hidden';
			DEBUG_CHILD.id = 'DEBUG_CHILD';
			DEBUG_CHILD.setAttribute('closed', 1);
			DEBUG_CHILD.onclick = function(){
				var el = dojo.byId('DEBUG_CHILD');
				if (el && el.getAttribute('closed') == '1') {
					el.setAttribute('closed', '-1');
					dojo.byId('DEBUG_CHILD').style.height = '600px';
					new dojo.fx.wipeIn(dojo.byId('DEBUG_CHILD'), CONSTANTS.duration, null, function(){dojo.byId('DEBUG_CHILD').setAttribute('closed', 0)}).play();
					el.style.overflow = 'scroll';
				}
			};
			DEBUG_CHILD.ondblclick = function(){
				var el = dojo.byId('DEBUG_CHILD');
				if (el && el.getAttribute('closed') == '0') {
					el.setAttribute('closed', '-1')
					el.style.overflow = 'hidden';
					new dojo.fx.wipeOut(dojo.byId('DEBUG_CHILD'), CONSTANTS.duration, null, function(){dojo.byId('DEBUG_CHILD').setAttribute('closed', 1);dojo.byId('DEBUG_CHILD').style.height = '10px';dojo.byId('DEBUG_CHILD').style.display = 'block';}).play();
				}
			};
			DEBUG_CHILD.oncontextmenu = function () {
				setValue('DEBUG_CHILD', '')
			}
			DEBUG_CHILD.style.backgroundColor = 'white';
			DEBUG_CHILD.style.border = '1px solid red';
			document.body.appendChild(DEBUG_CHILD)
		}
		if (arguments.length == 0) {
			return;
		}
		var i;
		var ul = document.createElement('UL');
		ul.style.borderBottom = '1px solid black';
		for (i = 0; i < arguments.length; i++) {
			var li = document.createElement('LI');
			li.innerHTML = debugShowVar(arguments[i]);
			ul.appendChild(li);
		}
		DEBUG_CHILD.appendChild(ul);
		if (!arguments.callee.time) {
			arguments.callee.time = new Date();
			return;
		}
		var t1 = arguments.callee.time;
		li.setAttribute('title', t2.valueOf() - t1.valueOf());
		li.innerHTML += ' <i>(' + (t2.valueOf() - t1.valueOf()) + 'sec)</i>';
		arguments.callee.time = new Date();
	};
	
	if (typeof console == 'undefined') {
		dojo.global.console = {
			log: function(){},
			debug: function(){},
			info: function(){},
			warn: function(){},
			error: function(){}
		};
	}
	
	return {};

});