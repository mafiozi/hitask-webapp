/**
 * Specifies tooltips on My Team drop targets
 */
//dojo.provide("hi.dnd.target.team");
define(function () {
	
	dojo.declare("dojo.dnd.HiDragTargetTeam", [dojo.dnd.Source], {
		
		isSource: false,
		
		selfAccept: false,
		
		accept: ["dragged_task"],
		
		dropValue: null,
		
		permissionsAllowed: [Hi_Permissions.COMPLETE_ASSIGN, Hi_Permissions.MODIFY, Hi_Permissions.EVERYTHING],

		action: 'assign',

		error: {
			assigned: "You can't modify task assigned to other user!",
			not_friend: "You cannot assign to this contact. This user<br/> has not yet approved you as a contact.",
			assigned_project: "You can't change project of task assigned to you!",
			incomplete_account: "Sorry, can't assign yet. <br/>This person must complete hiTask account <br/> registration and confirm email address.",
			shared: 'Can not assign shared task to friend.'
		},

		onMouseOut: function (e) {
			dojo.dnd.HiDragTargetTeam.superclass.onMouseMove.call(this, e);
			//JDT var m = dojo.dnd.Manager();
			var m = dojo.dnd.Manager.manager();


			if(m.source instanceof dojo.dnd.HiDragSource){
				if(this.isDragging){
					m.avatar.show();
					dojo.removeClass(m.avatar.nodeTxt, 'double-line');
					dojo.removeClass(m.avatar.nodeTxt, 'triple-line');
				}
				m.avatar.updateText('');
				this.dropValue = false;
			}
		},
		
		onMouseOver: function (e) {
			dojo.dnd.HiDragTargetTeam.superclass.onMouseOver.call(this, e);

			if (this.isDragging) {
				var m = dojo.dnd.Manager.manager();

				//JDT var m = dojo.dnd.Manager();
				if (m.source instanceof dojo.dnd.HiDragSource) {
					if (parseInt(Hi_Preferences.get('sidebar_messenger')) == 0) return;
					
					this.getUserCoordinates();
					
					var txt = '';
					var c = dojo.byId(Project.getContainer('fillUsers'));
					var b = dojo.position(c, true);
					
					var x = e.pageX - b.x;
					var y = e.pageY - b.y;
					if (dojo.isOpera) {
						y += dojo.byId('team_container_0').parentNode.scrollTop;
					}

					c = HiDragSourceCached.user;
					var j, jmax = c.length;
					var d;
					
					for (j = 0; j < jmax; j++) {
						d = c[j];
						if (d[0].x <= x && d[0].y <= y && d[0].x + d[1].w > x && d[0].y + d[1].h > y) {
		
							//if task already assigned, allow assigning back to user_id
							if (d[3] === false) {
								m.avatar.updateText(txt);
								return;
							}
							
							var dragId = m.source.dragNode.dragId,
								fr = Project.getFriend(d[3]);
							
							if (Project.canAssign(dragId, d[3]) && (fr.activeStatus || fr.waitStatus || fr.businessStatusInactive === true)) {
								this.dropValue = d[3];
								txt = this.text + d[2];
								// txt = this.error.incomplete_account;
							} else if (fr && fr.subscription != 'BOTH' && fr.subscription != 'BIS') {
								txt = this.error.not_friend;
							} else if (fr && fr.subscription != 'BIS' && hi.items.manager.get(dragId, 'shared')) {
								txt = this.error.shared;
							} else {
								txt = this.error.incomplete_account;
							}
							break;
						}
					}
					
					m.avatar.updateText(txt);
					if(txt.split('<br/>').length === 2){
						dojo.addClass(m.avatar.nodeTxt, 'double-line');
					}else if(txt.split('<br/>').length === 3){
						dojo.addClass(m.avatar.nodeTxt, 'triple-line');
					}
				}
			}
		},
		
		getUserCoordinates: function () {
			var n, m;
			if (!HiDragSourceCached.user) {
				HiDragSourceCached.user = [];
				var c = dojo.byId(Project.getContainer('fillUsers'));
				var b = dojo.position(c, true);
				var d = dojo.query('li.user_item', c);
				var i, imax = d.length;
				var j = 0;
				for (i = 0; i < imax; i++) {
					var st = dojo.query('input.status', d[i]);
					var bst = dojo.query('input.business_status', d[i]);
					if (st[0].value < 10 && st[0].value != 1 && bst[0].value <= 0 && !(st[0].value === '' && bst[0].value === '0')) {
						n = '';
						m = false;
					} else {
						n = dojo.query('input.drop_hidden_text', d[i])[0].value;
						m = dojo.query('input.drop_hidden', d[i])[0].value;
					}
					var p = dojo.position(d[i], true);
					p.x -= b.x;
					p.y -= b.y;
					HiDragSourceCached.user[j] = [p, dojo.coords(d[i]), n, m];
					j++;
				}
			}
		}
		
	});
	
	return dojo.dnd.HiDragTargetTeam;
});