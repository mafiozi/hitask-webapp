package beans;

import play.Play;

public class JsConstants {
	private static final String COMPLETED_ITEMS_WARNING_THRESHOLD_DEFAULT = "100";
	private static final String COMPLETED_ITEMS_WARNING_THRESHOLD_NAME = "completed.items.warning.threshold";
	// Use Upper case for color. 
	// If Updated :
	// 1. also update in hitask-play/models.items.Item.DEFAULT_COLOR_VALUE 
	// 2. Create db evolution to migrate to new color. (Example: see 24.sql)
	private static final String DEFAULT_COLOR_VALUE = "#2AAEF5";//"#5E93C3";
	
	private int refreshInterval = 25;
	private int refreshIntervalEmptyTeam = 30;
	private int taskRefreshInterval = 30;
	private int refreshIntervalBusiness = 60;
	private int refreshIntervalContacts = 60;
	private int refreshIntervalMessages = 30;
	private int maxProjects = 10;
	private int maxItems = 300;
	private int maxFriends = 10;
	private int archiveTasksPerPage = 25;
	private int taskMessageMaxLength = 10000;
	private int taskTitleMaxLength = 200;
	private int minPasswordLength = 8;
	private String defaultColorValue = DEFAULT_COLOR_VALUE;
	private int completedItemsWarningThreshold = Integer.parseInt(Play.configuration.getProperty(COMPLETED_ITEMS_WARNING_THRESHOLD_NAME, COMPLETED_ITEMS_WARNING_THRESHOLD_DEFAULT));
	private boolean jsConsole = false;
	private int maxUploadFileSizeMb = 40;
	private int manyTasksMaxItems = 50;
	private AnimationDuration animationDuration = new AnimationDuration();
	
	public AnimationDuration getAnimationDuration() {
		return animationDuration;
	}

	public void setAnimationDuration(AnimationDuration animationDuration) {
		this.animationDuration = animationDuration;
	}

	public int getRefreshInterval() {
		return refreshInterval;
	}
	
	public void setRefreshInterval(int refreshInterval) {
		this.refreshInterval = refreshInterval;
	}
	
	public int getRefreshIntervalEmptyTeam() {
		return refreshIntervalEmptyTeam;
	}
	
	public void setRefreshIntervalEmptyTeam(int refreshIntervalEmptyTeam) {
		this.refreshIntervalEmptyTeam = refreshIntervalEmptyTeam;
	}
	
	public int getTaskRefreshInterval() {
		return taskRefreshInterval;
	}
	
	public void setTaskRefreshInterval(int taskRefreshInterval) {
		this.taskRefreshInterval = taskRefreshInterval;
	}
	
	public int getRefreshIntervalBusiness() {
		return refreshIntervalBusiness;
	}
	
	public void setRefreshIntervalBusiness(int refreshIntervalBusiness) {
		this.refreshIntervalBusiness = refreshIntervalBusiness;
	}
	
	public int getRefreshIntervalContacts() {
		return refreshIntervalContacts;
	}
	
	public void setRefreshIntervalContacts(int refreshIntervalContacts) {
		this.refreshIntervalContacts = refreshIntervalContacts;
	}
	
	public int getRefreshIntervalMessages() {
		return refreshIntervalMessages;
	}
	
	public void setRefreshIntervalMessages(int refreshIntervalMessages) {
		this.refreshIntervalMessages = refreshIntervalMessages;
	}
	
	public int getMaxProjects() {
		return maxProjects;
	}
	
	public void setMaxProjects(int maxProjects) {
		this.maxProjects = maxProjects;
	}
	
	public int getMaxItems() {
		return maxItems;
	}
	
	public void setMaxItems(int maxItems) {
		this.maxItems = maxItems;
	}
	
	public int getMaxFriends() {
		return maxFriends;
	}
	
	public void setMaxFriends(int maxFriends) {
		this.maxFriends = maxFriends;
	}

	public int getTaskMessageMaxLength() {
		return taskMessageMaxLength;
	}

	public void setTaskMessageMaxLength(int taskMessageMaxLength) {
		this.taskMessageMaxLength = taskMessageMaxLength;
	}

	public boolean isJsConsole() {
		return jsConsole;
	}

	public void setJsConsole(boolean jsConsole) {
		this.jsConsole = jsConsole;
	}

	public int getArchiveTasksPerPage() {
		return archiveTasksPerPage;
	}

	public void setArchiveTasksPerPage(int archiveTasksPerPage) {
		this.archiveTasksPerPage = archiveTasksPerPage;
	}

	public int getMinPasswordLength() {
		return minPasswordLength;
	}

	public void setMinPasswordLength(int minPasswordLength) {
		this.minPasswordLength = minPasswordLength;
	}

	public int getTaskTitleMaxLength() {
		return taskTitleMaxLength;
	}

	public void setTaskTitleMaxLength(int taskTitleMaxLength) {
		this.taskTitleMaxLength = taskTitleMaxLength;
	}

	public int getCompletedItemsWarningThreshold() {
		return completedItemsWarningThreshold;
	}

	public void setCompletedItemsWarningThreshold(int completedItemsWarningThreshold) {
		this.completedItemsWarningThreshold = completedItemsWarningThreshold;
	}

	public String getDefaultColorValue() {
		return defaultColorValue;
	}

	public void setDefaultColorValue(String defaultColorValue) {
		this.defaultColorValue = defaultColorValue;
	}
	
	public int getMaxUploadFileSizeMb() {
		return maxUploadFileSizeMb;
	}

	public void setMaxUploadFileSizeMb(int maxUploadFileSizeMb) {
		this.maxUploadFileSizeMb = maxUploadFileSizeMb;
	}
	
	public int getManyTasksMaxItems() {
		return manyTasksMaxItems;
	}

	public void setManyTasksMaxItems(int manyTasksMaxItems) {
		this.manyTasksMaxItems = manyTasksMaxItems;
	}

	public static class AnimationDuration {
		private int sidePanelSlideMs = 500;
		private int groupFadeMs = 500;
		
		public int getSidePanelSlideMs() {
			return sidePanelSlideMs;
		}
		
		public void setSidePanelSlideMs(int sidePanelSlideMs) {
			this.sidePanelSlideMs = sidePanelSlideMs;
		}
		
		public int getGroupFadeMs() {
			return groupFadeMs;
		}
		
		public void setGroupFadeMs(int groupFadeMs) {
			this.groupFadeMs = groupFadeMs;
		}
	}
}