define([
	"dojo/_base/kernel",
	"dojo/_base/lang",
	"dojo/_base/Deferred",
	"dojo/_base/array"
], function(kernel, lang, Deferred, array) {
	// module:
	//		dojo/store/Observable
	// summary:
	//		TODOC

var hi_store = lang.getObject("hi.store", true);

return hi_store.Observable = function(store) {
	// summary:
	//		The Observable store wrapper takes a store and sets an observe method on query()
	//		results that can be used to monitor results for changes.
	//
	// description:
	//		Observable wraps an existing store so that notifications can be made when a query
	//		is performed.
	//
	// example:
	//		Create a Memory store that returns an observable query, and then log some
	//		information about that query.
	//
	//	|	var store = dojo.store.Observable(new dojo.store.Memory({
	//	|		data: [
	//	|			{id: 1, name: "one", prime: false},
	//	|			{id: 2, name: "two", even: true, prime: true},
	//	|			{id: 3, name: "three", prime: true},
	//	|			{id: 4, name: "four", even: true, prime: false},
	//	|			{id: 5, name: "five", prime: true}
	//	|		]
	//	|	}));
	//	|	var changes = [], results = store.query({ prime: true });
	//	|	var observer = results.observe(function(object, previousIndex, newIndex){
	//	|		changes.push({previousIndex:previousIndex, newIndex:newIndex, object:object});
	//	|	});
	//
	//		See the Observable tests for more information.

	var undef, queryUpdaters = [], queryCompletes = [], revision = 0;
	// a Comet driven store could directly call notify to notify observers when data has
	// changed on the backend
	store.notify = function(object, existingId, previousState, insertIndex){
		revision++;

		var updaters = queryUpdaters.slice();
		//console.log('updaters',[updaters.length]);
		for(var i = 0, l = updaters.length; i < l; i++){
			updaters[i](object, existingId, previousState, insertIndex);
		}
		
	};
	store.notifyComplete = function () {
		var listeners = queryCompletes.slice();
		for(var i = 0, l = listeners.length; i < l; i++){
			listeners[i]();
		}
	};
	
	store.diffObject = function(objA, objB, diff) { 
		//initialize
		diff = diff || false;
		//console.log('objA',[objA,objB]);
		//return false;
		if (typeof objA != "object" || typeof objB != "object") { // one or both are not object at all? 
			return diff;
		}
		
		//collect all properties of both objects in props 
		var prop = null,
			props = {},
			sub_diff;
		/*
		 * #3325?, #3336?
		 * Do not pass on withoutWidget in diff to oberver
		 */
		if ('withoutWidget' in objA) delete objA['withoutWidget'];
		if ('withoutWidget' in objB) delete objB['withoutWidget'];
		for (prop in objA) { 
			if (!dojo.isFunction(objA[prop]) && prop != 'list') // ignore functions 
				props[prop] = 1;
		} 
		for (prop in objB) { 
			if (!dojo.isFunction(objB[prop]) && prop != 'list') //ignore functions 
				props[prop] = 1;
		}
		
		//now loop through all collected properties of props 
		for (prop in props) {
			if (objA[prop] == undefined) {
				//property not existing in a or b
				diff = diff || {}; 
				diff[prop] = objB[prop];
			} else if (objB[prop] == undefined) {
				diff = diff || {};
				// Bug #5605 commented by qi
				// diff[prop] = undefined;
				diff[prop] = objA[prop];
			} else if (dojo.isArray(objA[prop]) && dojo.isArray(objB[prop])) {
				sub_diff = store.diffObject(objA[prop], objB[prop]);
				if (sub_diff !== false) {
					diff = diff || {};
					diff[prop] = objB[prop];
				}
			} else if (typeof objA[prop] == "object" && typeof objB[prop] == "object") { //property is object itself 
				sub_diff = store.diffObject(objA[prop], objB[prop]);
				if (sub_diff !== false) {
					diff = diff || {};
					diff[prop] = sub_diff;
				} 
			} else if (objA[prop] !== objB[prop]) { //different (atom) values 
				diff = diff || {};
				diff[prop] = objB[prop];
			}
		}
		
		return diff;
	};
	
	var originalQuery = store.query;
	store.query = function(query, options){
		options = options || {};
		var results = originalQuery.apply(this, arguments);
		if(results && results.forEach){
			var nonPagedOptions = lang.mixin({}, options);
			delete nonPagedOptions.start;
			delete nonPagedOptions.count;

			var queryExecutor = store.queryEngine && store.queryEngine(query, nonPagedOptions);
			var queryRevision = revision;
			var listeners = [], queryUpdater, queryComplete;
			var callQueue = [];
			results.observe = function(listener, includeObjectUpdates){
				if(listeners.push(listener) == 1){
					// first listener was added, create the query checker and updater
					
					queryCompletes.push(queryComplete = function () {
						var queue = [].concat(callQueue),
							k, item,
							copyListeners = listeners.slice(),
							i, listener;
						
						callQueue = [];

						/* #2167 - serialize listener actions and break loop over datastore items into chunks */
						var chunk3 = function(store, theListeners, process, originalStore) {
							var items = store.concat(),
								length = items.length,
								slice = Math.min(length,5),
								toProcess = items.slice(0,slice),
								passthrough = items.slice(slice);
							setTimeout(function() {
								var sliceLen = toProcess.length;
								for (var i = 0;i<toProcess.length;i++)
									process(toProcess[i][0],toProcess[i][1],toProcess[i][2],toProcess[i][3]);
								if (items.length == 0 && theListeners.length == 0) {
									//Callback for when all listeners have been invoked
									hi_store.afterLoad();
								}
								if (items.length > 0) {
									setTimeout(function() {chunk3(/*items*/passthrough,theListeners,process,originalStore);},5);
								} else {
									if (theListeners.length > 0) {
										var remainingListeners = theListeners.concat();
										setTimeout(function() {_chunk(originalStore,remainingListeners);},5);
									} 
								}

							}, 2);
						};
						
						var _chunk = function(store, listeners,context) {
							var myListeners = listeners.concat(),
								originalStore = store.concat();
							setTimeout(function() {
								var listener = myListeners.shift();
								//listener = myListeners.shift();
								setTimeout(function() {chunk3(store,myListeners,listener,originalStore);},5);
							},5);
						};
						/* PERFORMANCE
						if ( (dojo.isFF && queue.length>* #2288 cutoff *500) || (!dojo.isFF && queue.length > 2000))
						{
							//_chunk(queue,listeners);
							for (var i = 0;i<copyListeners.length;i++)
							{
								chunk3(queue,[],copyListeners[i],queue);
							}							
						} else
						{*/
							var callListeners = function() {
								for(k = 0;item = queue[k]; k++) {
									for(i = 0;listener = copyListeners[i]; i++){
										listener(item[0], item[1], item[2], item[3]);
									}
								}	
								hi_store.afterLoad();
							};
							if (dojo.isSafari && dojo.isSafari >= 6.1) {
								// Safari since 6.1 crashes while setting up observers.
								// The only possible way to prevent it from crashing is to apply small timeout.
								setTimeout(callListeners, 1); 
							} else {
								callListeners();
							}
						/*}*/
						
					});
					
					queryUpdaters.push(queryUpdater = function(changed, existingId, previousState, insertIndex) {
						Deferred.when(results, function(resultsArray){
							var atEnd = resultsArray.length != options.count;
							var i, l, listener;
							if(++queryRevision != revision){
								throw new Error("Query is out of date, you must observe() the query prior to any data modifications");
							}
							var removedObject, removedFrom = -1, insertedInto = -1;
							if(existingId !== undef) {
								// remove the old one
								for(i = 0, l = resultsArray.length; i < l; i++){
									var object = resultsArray[i];
									if(store.getIdentity(object) == existingId){
										removedObject = object;
										removedFrom = i;
										if(queryExecutor || !changed){// if it was changed and we don't have a queryExecutor, we shouldn't remove it because updated objects would be eliminated
											resultsArray.splice(i, 1);
										}
										break;
									}
								}
							}
							
							if(queryExecutor){
								// add the new one
								if(changed &&
										// if a matches function exists, use that (probably more efficient)
										(queryExecutor.matches ? queryExecutor.matches(changed) : queryExecutor([changed]).length)){

									var firstInsertedInto = typeof insertIndex === "number" ? insertIndex : (removedFrom > -1 ? 
										removedFrom : // put back in the original slot so it doesn't move unless it needs to (relying on a stable sort below)
										resultsArray.length);
									resultsArray.splice(firstInsertedInto, 0, changed); // add the new item
									insertedInto = array.indexOf(queryExecutor(resultsArray), changed); // sort it
									
									// we now need to push the chagne back into the original results array
									resultsArray.splice(firstInsertedInto, 1); // remove the inserted item from the previous index
									
									if((options.start && insertedInto == 0) ||
										(!atEnd && insertedInto == resultsArray.length)){
										// if it is at the end of the page, assume it goes into the prev or next page
										insertedInto = -1;
									}else{
										resultsArray.splice(insertedInto, 0, changed); // and insert into the results array with the correct index
									}
								}
							}else if(changed && !options.start){
								// we don't have a queryEngine, so we can't provide any information
								// about where it was inserted, but we can at least indicate a new object
								
								insertedInto = removedFrom >= 0 ? removedFrom : (store.defaultIndex || 0);
							}
							
							if((removedFrom > -1 || insertedInto > -1) &&
									(includeObjectUpdates || !queryExecutor || (removedFrom != insertedInto))){
								
								var diff;
								if (includeObjectUpdates == 'deep' && previousState && changed) {
									diff = store.diffObject(changed, previousState);
									if (diff === false && removedFrom === insertedInto) return;
								}
								
								callQueue.push([changed || removedObject, removedFrom, insertedInto, diff]);
							}
							
							//Update cache data
							store.setData([].concat(resultsArray));
						});
					});
				} 
				return {
					cancel: function(){
						// remove this listener
						var index = array.indexOf(listeners, listener);
						if(index > -1){ // check to make sure we haven't already called cancel
							listeners.splice(index, 1);
							if(!listeners.length){
								// no more listeners, remove the query updater too
								queryUpdaters.splice(array.indexOf(queryUpdaters, queryUpdater), 1);
								queryCompletes.splice(array.indexOf(queryCompletes, queryComplete), 1);
							}
						}
					}
				};
			};
		}
		return results;
	};
	var inMethod;
	store.whenFinished = function (method, action){
		var original = store[method];
		if(original){
			store[method] = function(value, options, insertIndex){
				var id = null,
					previousState = null;
				
				if (value && (id = store.getIdentity(value))) {
					previousState = store.get(id);
				}
				
				if(inMethod){
					// if one method calls another (like add() calling put()) we don't want two events
					return original.apply(this, arguments);
				}
				inMethod = true;
				try{
					var results = original.apply(this, arguments);
					Deferred.when(results, function(results){
						action((typeof results == "object" && results) || value, previousState, insertIndex);
					});
					return results;
				}finally{
					inMethod = false;
				}
			};
		}
	};
	
	// monitor for updates by listening to these methods
	store.whenFinished("put", function(object, previousState, insertIndex){
		store.notify(object, store.getIdentity(object), previousState, insertIndex);
	});
	store.whenFinished("add", function(object){
		store.notify(object);
	});
	store.whenFinished("remove", function(id){
		store.notify(undefined, id);
	});
	
	return store;
};

});
