/**
 * Task comments object
 */
define([
	"dojo/on"
], function (dojo_on) {
	
	dojo.global.Hi_Comments = {
		/*
		 * Temporary ID
		 * @type {Number}
		 */
		incremental_id: 0,
		
		/*
		 * Temporary ID to actual ID hash table
		 * @type {Object}
		 */
		ids: {},
		
		/*
		 * Task id
		 * @type {Number}
		 */
		task_id: null,
		
		/*
		 * Task history container node
		 * @type {HTMLElement}
		 */
		container: null,
		
		/*
		 * List of comments
		 * @type {Number}
		 */
		data: [],
		
		/*
		 * Comments has been loaded
		 * @type {Boolean}
		 */
		loaded: false,
		
		/*
		 * Comments are loading
		 * @type {Boolean}
		 */
		loading: false,
		
		/*
		 * Archive page
		 * @type {Boolean}
		 */
		is_archive: String(document.location).toLowerCase().indexOf('/archive') != -1,
		
		/*
		 * URL for retrieving list of comments
		 * @type {String}
		 */
		url_list: 'itemhistory',
		
		/*
		 * URL for adding a comment
		 * @type {String}
		 */
		url_add: 'itemcomment',
		
		/*
		 * URL for deleting comment
		 * @type {String}
		 */
		url_delete: 'itemcommentdelete',
		
		/**
		 * Return correct comment_id from temporary id
		 * 
		 * @param {Number} comment_id
		 * @return Comment ID
		 * @type {Number}
		 */
		normalizeCommentId: function (comment_id) {
			if (String(comment_id).indexOf('temp_') != -1) {
				if (comment_id in Hi_Comments.ids) {
					return Hi_Comments.ids[comment_id];
				} else {
					//Correct comment ID is not received yet
					return null;
				}
			}
			return comment_id;
		},
		
		/**
		 * Returns task ID
		 */
		getTaskId: function (reset) {
			if (reset) {
				this.reset();
			}
			if (typeof(reset) === "number" || (typeof(parseInt(reset)) === 'number' && parseInt(reset) > 0)) {
				this.setTaskId(parseInt(reset));
			}
			return this.task_id;
		},
		
		onReady: function (task_id) {
			if (this.getTaskId(task_id || true) && this.task_id > 0) {
				var task = hi.items.manager.get(this.task_id);
				if (!task) return;
				
				var textarea = dojo.query('textarea', this.container),
					hiTextarea = null;
				if (textarea.length && (hiTextarea = dijit.byNode(textarea[0]))) {
					var def_value = hi.i18n.getLocalization('history.comment');
					hiTextarea.set('value', '');
					hiTextarea.set('placeHolder', def_value);
				}
				
				if ('last_comment' in task && task.last_comment) {
					var name = '';
					if (Project.user_id == task.last_comment_user_id) {
						name = Project.user_name;
					} else {
						var friend = Project.getFriend(task.last_comment_user_id);
						name = friend ? friend.name : '';
					}
					
					var data = {
						'comment_id': 0,
						'comment': task.last_comment,
						'time': str2timeAPI(task.last_comment_create_datetime),
						'user_id': task.last_comment_user_id,
						'user_name': name
					};
					
					var cont = dojo.query('div.cont.last div.dataEl', Hi_Comments.container);
					cont[0].innerHTML = '';
					
					//Show caption
					var node = document.createElement('DIV');
					node.innerHTML = Hi_Comments.Output.formatCaption(data);
					cont[0].appendChild(node.firstChild);
					
					//Show new comment
					node = document.createElement('DIV');
					node.innerHTML = Hi_Comments.Output.formatComment(data);
					cont[0].appendChild(node.firstChild);
				} else {
					var cont = dojo.query('div.cont.last', Hi_Comments.container);
					cont[0].style.display = 'none';
				}
				
				if (dojo.isIE <= 7) {
					setTimeout(function () {
						document.body.className = document.body.className;
					}, 150);
				}
			}
		},
		
		/**
		 * Set task ID
		 * 
		 * @param {Number} task_id
		 * @param {Boolean} autoload If true, comments will be loaded
		 */
		setTaskId: function (task_id, autoload) {
			//Reset if task_id is not set
			if (!task_id) {
				return this.reset();
			}
			
			//Skip if ID didn't changed
			if (task_id == this.task_id) {
				if (autoload && !this.loaded && !this.loading) {
					this.loadComments();
				}
				return;
			}
			
			//Reset 
			this.reset();
			this.task_id = task_id;
			
			if (task_id) {
				var item = hi.items.manager.opened(task_id) || hi.items.manager.item(task_id);
				if (item && item.propertiesView) {
					this.container = item.propertiesView.commentNode;
				} else {
					// For Archive page.
					var cont = dojo.query('#element_properties_' + task_id + ' form.history');
					if (cont) {
						this.container = cont[0];
					}
				}
			}
			
			if (autoload) {
				this.loadComments();
			}
		},
		
		/**
		 * Find comment and add it
		 */
		handleAddComment: function () {
			if (!this.getTaskId()) return;
			
			var textarea = dojo.query('textarea', this.container),
				hiTextarea = null;

			if (textarea.length && (hiTextarea = dijit.byNode(textarea[0]))) {
				var comment = hiTextarea.get("value").replace(/^\s+|\s+$/g, '');
				if (comment) {
					this.addComment(comment);
					
					hiTextarea.set('value', '');
					textarea[0].blur();
					textarea[0].style.height = '2.3em';
					textarea[0].style.overflow = 'hidden';
				}
			}
		},
		
		/**
		 * Add comment
		 * 
		 * @param {String} message
		 */
		addComment: function (message) {
			if (!this.getTaskId()) return;
			
			var params = {
				'id': this.task_id,
				'message': message
			};
			
			Project.ajax(this.url_add, params, function (dataResponse) {
				if (dataResponse.isStatusOk()) {
					
					//Set correct comment id
					var allData = Hi_Comments.data;
					for(var i=0,ii=allData.length; i<ii; i++) {
						if (allData[i].comment_id == temporaryId) {
							allData[i].comment_id = dataResponse.getDataId();
						}
					}
					
					Hi_Comments.ids[temporaryId] = dataResponse.getDataId();
					
				}
			}, function () {
				ALERT('ERROR');
			});
			
			//Add comment immediately
			var time = new Date();
			var temporaryId = 'temp_' + (this.incremental_id++);
			var commentData = {
				'comment_id': temporaryId,
				'comment': htmlspecialchars(message),	//on server-side entities are escaped
				'time': new Date(),
				'user_id': Project.user_id,
				'user_name': htmlspecialchars(Project.user_name)
			};
			
			this.addCommentNode(commentData);
		},
		
		addCommentNode: function (data) {
			var order = Hi_Preferences.get('comment_order', 0, 'int');
			if (order) {
				//Latest at the top
				Hi_Comments.data.unshift(data);
			} else {
				//Latest at the bottom
				Hi_Comments.data.push(data);
			}
			
			//Set task data
			hi.items.manager.set({
				"id": this.task_id,
				"last_comment": data.comment,
				"last_comment_create_datetime": time2strAPI(data.time, 'y-m-d h:i:s'),
				"last_comment_user_id": data.user_id,
				"time_last_update": time2strAPI(data.time, 'y-m-d h:i:s')
			});
			
			if (Hi_Comments.loaded) {
				var cont = dojo.query('div.cont.fulllist div.dataEl', Hi_Comments.container);
			} else {
				var cont = dojo.query('div.cont.last div.dataEl', Hi_Comments.container);
				if (cont.length) {
					cont[0].innerHTML = '';
				}
			}
			
			if (cont.length) {
				//Add caption?
				var show_caption = true;
				var caption_node = null;
				
				if (Hi_Comments.data.length > 1 && Hi_Comments.loaded) {
					var next_item_index = order ? 1 : Hi_Comments.data.length-2;
					var tdif = data.time.getTime() - Hi_Comments.data[next_item_index].time.getTime();
					if (tdif <= 60000) {
						show_caption = false;
					}
				}
				if (show_caption) {
					caption_node = document.createElement('DIV');
					caption_node.innerHTML = Hi_Comments.Output.formatCaption(data);
					caption_node = caption_node.firstChild;
					
					if (order) {
						//Latest at the top
						if (cont[0].firstChild) {
							insertBefore(caption_node, cont[0].firstChild);
						} else {
							cont[0].appendChild(caption_node);
						}
					} else {
						//Latest at the bottom
						cont[0].appendChild(caption_node);
					}
				}
				
				//Show new comment
				var node = document.createElement('DIV');
					node.innerHTML = Hi_Comments.Output.formatComment(data);
					node = node.firstChild;
				
				if (order) {
					if (!caption_node) {
						//There is no caption node, insert before first item
						caption_node = dojo.query('div.caption', Hi_Comments.container);
						caption_node = caption_node.length ? caption_node[0] : null;
					}
					
					if (caption_node) {
						//Insert after caption
						insertAfter(node, caption_node);
					} else {
						//Append to parent
						cont[0].appendChild(node);
					}
				} else {
					//Latest at the bottom
					cont[0].appendChild(node);
				}
				
				if (dojo.isIE == 8) {
					var subnodes = [node.getElementsByTagName('DIV')[0], node.getElementsByTagName('DIV')[1], node.getElementsByTagName('IMG')[0]];
					for (var i=0, ii=subnodes.length; i<ii; i++) {
						if (subnodes[i]) {
							dojo.style(subnodes[i], {'opacity': 0});
							dojo.fadeIn({'node': subnodes[i], 'duration': 1000}).play();
						}
					}
				} else {
					dojo.style(node, {'opacity': 0});
					dojo.fadeIn({'node': node, 'duration': 1000}).play();
				}
				
				//Scroll into view
				if (order) {
					//Latest at the top
					cont[0].scrollTop = 0;
				} else {
					//Latest at the bottom
					cont[0].scrollTop = 9000;
				}
				
				
				//Show last comment box
				if (!Hi_Comments.loaded) {
					cont[0].parentNode.parentNode.style.display = '';
				}
			}
		},
		
		/**
		 * Show delete confirmation window
		 * 
		 * @param {Number} comment_id
		 */
		handleDeleteComment: function (comment_id, target) {
			var message = hi.i18n.getLocalization('tooltip.delete_comment');
			
			var correct_comment_id = Hi_Comments.normalizeCommentId(comment_id);
			if (correct_comment_id === null) {
				//Correct comment ID is not received yet
				return;
			}
			
			var node = dojo.byId('deleteconfirmation_message');
				node.innerHTML = message;
			
			Tooltip.open('deleteconfirmation', "", target);
			
			//Initialize tooltip if needed
			var tooltip = dojo.byId('tooltip_deleteconfirmation');
			if (!Hi_Comments.remove_tooltip_initialized) {
				dojo_on(tooltip, 'click', function (ev) {
				    dojo.stopEvent(ev);
				});
				Hi_Comments.remove_tooltip_initialized = true;
			}
			
			function closeTooltip () {
				Tooltip.close('deleteconfirmation');
			}
			
			var btns = tooltip.getElementsByTagName('BUTTON');
			
			var okSignal,cancelSignal;
			okSignal = dojo_on.once(dijit.byId('deleteConfirmationOKBtn'),'Click',function() {
				closeTooltip();
				okSignal.remove();
				if (cancelSignal) cancelSignal.remove();
				Hi_Comments.deleteComment(comment_id);
			});
			cancelSignal = dojo_on.once(dijit.byId('deleteConfirmationCancelBtn'),'Click',function(e) {
				dojo.stopEvent(e);
				closeTooltip();
				if (okSignal) okSignal.remove();
				cancelSignal.remove();
			});
		},
		
		/**
		 * Delete comment
		 * 
		 * @param {Number} comment_id
		 */
		deleteComment: function (comment_id) {
			if (!this.getTaskId()) return;
			
			var correct_comment_id = Hi_Comments.normalizeCommentId(comment_id);
			if (correct_comment_id === null) {
				//Correct comment ID is not received yet
				return;
			}
			
			var self = this;
			var params = {
				'id': correct_comment_id
			};
			
			//Fade out element
			if (dojo.isIE == 8) {
				var node = dojo.byId('comment' + comment_id);
				var subnodes = [node.getElementsByTagName('DIV')[0], node.getElementsByTagName('DIV')[1], node.getElementsByTagName('IMG')[0]];
				for (var i=0, ii=subnodes.length; i<ii; i++) {
					dojo.fadeOut({node: subnodes[i], duration: 1000}).play();
				}
			} else {
				var node = dojo.byId('comment' + comment_id);
				if (node) {
					dojo.fadeOut({node: node, duration: 1000}).play();
				}
			}
			
			Project.ajax(this.url_delete, params, function (dataResponse) {
				//On error nothing will be returned
				if (dataResponse.isStatusOk()) {
					var __removeElementFromArray = function(array, from, to) {
						var rest = array.slice((to || from) + 1 || array.length);
						array.length = from < 0 ? array.length + from : from;
						return array.push.apply(array, rest);
					};
					
					//Remove comment from data list
					for (var i = 0, ii = self.data.length; i < ii; i++) {
						if (self.data[i].id == correct_comment_id) {
							__removeElementFromArray(self.data, i);
							break;
						}
					}
					
					// Update item model last comment respectively.
					var comments = self.getDataCommentsOnly();
					if (comments.length > 0) {
						var lastComment = comments[comments.length - 1];
						if (hi.items.manager.get(self.getTaskId(), 'last_comment_id') != lastComment.id) {
							self.setItemLastComment(lastComment);
						}
					} else {
						self.resetItemLastComment();
					}
					
					//Remove comment from DOM
					var node = dojo.byId('comment' + comment_id);
					if (node) {
						//Remove caption if it's not needed anymore
						var prev = prevElement(node);
						if (dojo.hasClass(prev, 'caption')) {
							var next = nextElement(node);
							if (!next || dojo.hasClass(next, 'caption')) {
								prev.parentNode.removeChild(prev);
							}
						}
						
						node.parentNode.removeChild(node);
					}
				}
			}, function () {
				ALERT('ERROR');
			});
		},
		
		resetItemLastComment: function() {
			if (this.getTaskId()) {
				this.setItemLastComment(null);
			}
		},
		
		setItemLastComment: function(comment) {
			if (this.getTaskId()) {
				hi.items.manager.set(this.getTaskId(), {
					last_comment: comment ? comment.value : null,
					last_comment_create_datetime: comment ? time2strAPI(comment.time) : null,
					last_comment_id: comment ? comment.id : null,
					last_comment_user_id: comment ? comment.user_id : null
				});
			}
		},
		
		/**
		 * Load comments from server
		 */
		loadComments: function (force) {
			if (!this.getTaskId() || (!force && this.loaded) || this.loading) return;
			
			var forced = this.loaded && force;
			var order = Hi_Preferences.get('comment_order', 0, 'int');
			
			this.setClass('loading');
			this.loading = true;
			
			Project.ajax(this.url_list, {'id': this.task_id}, function (dataResponse) {
				
				if (dataResponse.getResponseType() != 'listhistory') {
					return;
				}
				
				var itemHistory = dataResponse.getItemHistory();
				
				Hi_Comments.loading = false;
				Hi_Comments.loaded = true;
				
				//Convert object into array
				var out = [];
				for(var i in itemHistory) {
					if (typeof itemHistory[i] == 'object') {
						if (itemHistory[i].time) {
							itemHistory[i].time = str2timeAPI(itemHistory[i].time);
						}
						out.push(itemHistory[i]);
					}
				}
				
				out.sort(function (a,b) {
					var at = a.time.getTime();
					var bt = b.time.getTime();
					if (order) {
						//Latest at the top
						return (at < bt ? 1 : (at > bt ? -1 : 0));
					} else {
						//Latest at the bottom
						return (at > bt ? 1 : (at < bt ? -1 : 0));
					}
				});
				
				Hi_Comments.data = out;
				Hi_Comments.Output.render(Hi_Comments.data);
				Hi_Comments.setClass('data');
			}, function () {
				this.loading = false;
				ALERT('ERROR');
			});
		},
		
		/**
		 * Reset comment data
		 */
		reset: function () {
			//Reset variables
			if (this.task_id) {
				var item = hi.items.manager.item(this.task_id);
				if (item && item.propertiesView) {
					item.propertiesView.resetCommentBox();
				}
			}
			
			this.task_id = null;
			this.loaded = false;
			this.loading = false;
			this.data = [];
			this.container = null;
		},
		
		/**
		 * Set class
		 * @param {String} class
		 */
		setClass: function (className) {
			for (var i in {'link':1,'loading':2,'data':3}) {
				if (className == i) {
					dojo.addClass(this.container, i);
				} else {
					dojo.removeClass(this.container, i);
				}
			}
		},
		
		/**
		 * Returns true if entity is a comment
		 * 
		 * @param {Number} comment_id
		 * @return True if comment, otherwise false
		 * @type {Boolean}
		 */
		isComment: function (comment_id) {
			var data = (typeof comment_id == 'object' ? comment_id : this.getDataById(comment_id));
			return (data.property_name == 'comment');
		},
		
		/**
		 * Returns data item with given ID
		 * 
		 * @param {Number} comment_id
		 * @return Item from data list
		 * @type {Object}
		 */
		getDataById: function (comment_id) {
			var d = this.data;
			for(var i=0,ii=d.length; i<ii; i++)  {
				if (d[i].comment_id == comment_id) return d[i];
			}
			return null;
		},
		
		getDataCommentsOnly: function() {
			var comments = [];
			dojo.forEach(this.data, function(d) {
				if (d.property_name == 'comment') {
					comments.push(d);
				}
			});
			return comments;
		},
		
		/**
		 * Returns true if user can delete comment
		 * 
		 * @param {Object} data
		 * @return True if comment can be deleted by user, otherwise false
		 * @type {Boolean}
		 */
		canDeleteComment: function (data) {
			if (this.is_archive) return false;
			
			//Free/Premium user can delete any comment for assigned task
			if (!Project.businessId) {
				if (hi.items.manager.get(this.task_id, "assignee") == Project.user_id) {
					return true;
				}
			}
			
			//User can delete his own comments, comments for his task or all if an administrator
			var is_comment_owner = (data.user_id == Project.user_id);
			var is_task_owner = hi.items.manager.get(this.task_id, "user_id") == Project.user_id;
			return (is_comment_owner || is_task_owner || Project.isBusinessAdministrator());
		}
		
	};
	
	return dojo.global.Hi_Comments;

});