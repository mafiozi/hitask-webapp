package api;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

import beans.AuthenticateResponse;
import beans.BusinessListItem;
import beans.PublishedItem;
import beans.Report;
import beans.Transaction;
import beans.UserBusiness;
import beans.UserPreferences;
import beans.UserSession;
import play.Logger;
import play.Play;
import play.libs.Crypto;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import play.mvc.Http;
import utils.Constants;

/**
 * This class allows to interact with HiTask API server.
 * @author Vadim Manikhin
 */
public class ApiCall {
	public static final String API_OAUTH_AUTH = Constants.API_URL + "/oauth2/auth";
	public static final String API_OAUTH_APPROVE = Constants.API_URL + "/oauth2/approve";
	private static final String API_USER_URL = Constants.API_URL + "/user";
	private static final String API_LOGOUT_URL = Constants.API_URL + "/user/logout";
	private static final String API_LOGIN_EXTERNAL = Constants.API_URL + "/user/authenticateExternal";
	private static final String API_LINK_EXTERNAL = Constants.API_URL + "/user/linkExternal";
	private static final String API_USER_BUSINESS_URL = Constants.API_URL + "/list/business";
	private static final String API_USER_PREFERENCES_URL = Constants.API_URL + "/user/preferences";
	private static final String API_BUSINESS_LIST_URL = Constants.API_URL + "/business/list";
	private static final String API_LAST_USER_TRANSACTION_URL = Constants.API_URL + "/transactions/last";
	private static final String API_LAST_BUSINESS_TRANSACTION_URL = Constants.API_URL + "/transactions/business/last";
	private static final String API_ITEM_URL = Constants.API_URL + "/item";
	private static final String API_ITEM_PUBLISHED_URL = Constants.API_URL + "/item/published";
	private static final String API_REPORT_URL = Constants.API_URL + "/report";
	private static final String API_REPORT_STATUS_URL = Constants.API_URL + "/report/status/";
	
	private String sessionId;
	
	public ApiCall() {
		this(null);
	}
	
	public ApiCall(String sessionId) {
		this.sessionId = sessionId;
	}
	
	public UserSession getUser() {
		return doGet(API_USER_URL, UserSession.class);
	}
	
	public Object getItem(String guid) {
		return doGet(API_ITEM_URL + "/" + guid, Map.class);
	}
	
	public Report report(Map<String, String> params) {
		return doGet(API_REPORT_URL, Report.class, params);
	}
	
	public String reportStatus(String guid) {
		Map m = doGet(API_REPORT_STATUS_URL + "/" + guid, Map.class);
		Object o = m.get("url");
		if (o instanceof String) {
			return (String)o;
		} else {
			return null;
		}
	}
	
	public PublishedItem getItemPublished(String path) {
		return doGetSessionless(API_ITEM_PUBLISHED_URL + "/" + path, PublishedItem.class);
	}
	
	public UserBusiness getUserBusiness() {
		return doGet(API_USER_BUSINESS_URL, UserBusiness.class);
	}
	
	public UserPreferences getUserPreferences() {
		HashMap<String, String> raw = doGet(API_USER_PREFERENCES_URL, HashMap.class);
		String json = new Gson().toJson(raw);
		
		UserPreferences up = new Gson().fromJson(json, UserPreferences.class);
		up.setRawPreferences(raw);
		return up;
	}
	
	public void saveUserPreferences(Map<String, String> prefs) {
		doPost(API_USER_PREFERENCES_URL, Object.class, prefs);
	}
	
	public ApiGeneralResponse logout() {
		return logout(true);
	}
	
	public AuthenticateResponse authenticateExternal(String providerId, String accessToken, String refreshToken) {
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("accessToken", accessToken);
		parameters.put("providerId", providerId);
		parameters.put("refreshToken", refreshToken);
		return doGetSessionless(API_LOGIN_EXTERNAL, AuthenticateResponse.class, parameters);
	}
	
	public ApiGeneralResponse linkExternal(String providerId, String accessToken, String refreshToken) {
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("accessToken", accessToken);
		parameters.put("providerId", providerId);
		parameters.put("refreshToken", refreshToken);
		return doGet(API_LINK_EXTERNAL, ApiGeneralResponse.class, parameters);
	}
	
	public BusinessListItem[] getBusinessList() {
		return doGetInternal(API_BUSINESS_LIST_URL, BusinessListItem[].class);
	}
	
	public Transaction getUserLastTransaction() {
		return doGet(API_LAST_USER_TRANSACTION_URL, Transaction.class);
	}
	
	public Transaction getBusinessLastTransaction() {
		return doGet(API_LAST_BUSINESS_TRANSACTION_URL, Transaction.class);
	}
	
	private ApiGeneralResponse logout(boolean checkIfLoggedIn) {
		if (checkIfLoggedIn) {
			try {
				getUser();
				return logout(false);
			} catch (ApiCallException e) {
				if (e.getHttpStatus() == Http.StatusCode.UNAUTHORIZED) {
					Logger.debug("session %s already terminated", sessionId);
				}
			}
			
			return null;
		} else {
			return doGet(API_LOGOUT_URL, ApiGeneralResponse.class);
		}
	}
	
	private <T> T performGet(WSRequest req, Class<T> resultClass) {
		return performRequest(req, resultClass, "get");
	}
	
	private <T> T performPost(WSRequest req, Class<T> resultClass) {
		return performRequest(req, resultClass, "post");
	}
	
	private <T> T performRequest(WSRequest req, Class<T> resultClass, String method) {
		String json = "";
		int status = 0;
		
		try {
			HttpResponse resp = null;
			if (method != null && method.equalsIgnoreCase("post")) {
				resp = req.post();
			} else {
				resp = req.get();
			}
			
			status = resp.getStatus();
			json = resp.getString();
			
			if (status == Http.StatusCode.OK) {
				return new Gson().fromJson(json, resultClass);
			}
		} catch (Exception e) {
			throw new ApiCallException(e);
		}
		
		throw new ApiCallException(parseApiErrorResponse(json), status);
	}
	
	private <T> T doPost(String url, Class<T> resultClass, Map<String, String> parameters) {
		return performPost(createRequestWithSessionId(url, parameters), resultClass);
	}
	
	private <T> T doGet(String url, Class<T> resultClass) {
		return performGet(createRequestWithSessionId(url), resultClass);
	}
	
	private <T> T doGet(String url, Class<T> resultClass, Map<String, String> parameters) {
		return performGet(createRequestWithSessionId(url, parameters), resultClass);
	}
	
	private <T> T doGetInternal(String url, Class<T> resultClass) {
		return performGet(createInternalApiRequest(url), resultClass);
	}
	
	private <T> T doGetSessionless(String url, Class<T> resultClass) {
		return performGet(createSessionlessRequest(url), resultClass);
	}
	
	private <T> T doGetSessionless(String url, Class<T> resultClass, Map<String, String> parameters) {
		return performGet(createSessionlessRequest(url, parameters), resultClass);
	}
	
	private WSRequest createRequestWithSessionId(String apiUrl) {
		return WS.url(apiUrl).setParameter(Constants.API_SESSION_ID_ARG, sessionId);
	}
	
	private WSRequest createRequestWithSessionId(String apiUrl, Map<String, String> parameters) {
		return WS.url(apiUrl).setParameter(Constants.API_SESSION_ID_ARG, sessionId).setParameters(parameters);
	}
	
	private WSRequest createInternalApiRequest(String apiUrl) {
		String random = Crypto.passwordHash(String.valueOf(Math.random() + System.currentTimeMillis()));
		String sign = Crypto.sign(random);
		
		return WS.url(apiUrl).setParameter(Constants.API_INTERNAL_ARG, sign).setParameter(Constants.API_INTERNAL_RANDOM_ARG, random);
	}
	
	private WSRequest createSessionlessRequest(String apiUrl) {
		return createSessionlessRequest(apiUrl, new HashMap<String, String>());
	}
	
	private WSRequest createSessionlessRequest(String apiUrl, Map<String, String> parameters) {
		return WS.url(apiUrl).setParameter(Constants.API_KEY_ARG, Play.configuration.getProperty("api.key")).setParameters(parameters);
	}
	
	private ApiGeneralResponse parseApiErrorResponse(String json) {
		try {
			return new Gson().fromJson(json, ApiGeneralResponse.class);
		} catch (Exception e) {
			return new ApiGeneralResponse();
		}
	}
}
