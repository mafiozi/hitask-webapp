//dojo.provide("hi.upload.widget");

/**
 * FileUploadWidget handles interactions with file list
 */
define(function () {
	
	dojo.global.FileUploadWidget = function (taskId, ignoreNew) {
		this.taskId = parseInt(taskId) || 0;
		this.files = [];
		
		this.create(ignoreNew);
	};
	dojo.global.FileUploadWidget.prototype = {
		/**
		 * Task id
		 * @type {Number}
		 */
		taskId: null,
		
		/**
		 * File list
		 * @type {Array}
		 */
		files: [],
		
		/**
		 * Task container node
		 * @type {HTMLElement}
		 */
		nodeContainer: null,
		
		/**
		 * Task list node
		 * @type {HTMLElement}
		 */
		nodeList: null,
		
		/**
		 * Update timer
		 * @type {Number}
		 */
		updateTimer: null,
		
		/**
		 * File drag and drop already is initialized
		 * @type {Boolean}
		 */
		dndInitialized: false,
		
		/**
		 * Uploader is in tooltip
		 * @type {Boolean}
		 */
		inTooltip: false,
		
		/**
		 * Returns total file count
		 * 
		 * @return File count
		 * @type {Number}
		 */
		size: function () {
			return this.files.length;
		},
		
		/**
		 * Wait 2 seconds and call update
		 */
		update: function () {
			if (this.updateTimer) {
				clearTimeout(this.updateTimer);
			}
			var self = this;
			this.updateTimer = setTimeout(function () {
				if (self.taskId && self.taskId > 0) {
					// Existing items only
					FileList.load(self.taskId, true);
				} else {
					hi.items.manager.refresh();
				}
			}, 2000);
		},
		
		/**
		 * Handle ready state
		 */
		updateReadyView: function (force) {
			//If uploader is in tooltip then upload button is not associated with this uploader anymore
			if (this.inTooltip) return;
			/*#2934
			 * We may have an item expanded, but if the user dropped a file on 
			 * the new item drop area, we should continue
			 * 
			 * Note: in droparea.js(onDragEnterGlobal()), we collapse opened items, but only if that
			 * item's drop area is hidden.  On ther other hand, if we get to here, it means the user
			 * dropped a file on the drop target corresponding to adding a new, separate file, but may 
			 * also have an item with attached files expanded.
			//Files attached to tasks doesn't have ready state and are uploaded immediately
			if (this.taskId > 0 || hi.items.manager.openedId() > 0) {
				return;
			}
			*/
			/*
			if (this.taskId >0)
			{
				if (hi.items.manager.openedId() > 0) return;
			} else if (hi.items.manager.openedId()>0)
			{
				hi.items.manager.collapseOpenedItems();
			}
			*/
			var opened = hi.items.manager.openedId()||null;
			if (this.taskId>0 && opened && opened > 0) return;
			/*
			if (this.taskId<0)
			{
				if (opened && this.taskId != opened) 
				{
					//hi.items.manager.remove(opened);
				}
			}
			*/
			if (opened >0) hi.items.manager.collapseOpenedItems();
			
			var files = this.files,
				has_ready_files = 0;
			
			if (typeof force == 'number') {
				has_ready_files = force;
			} else {
				for(var i=0,ii=files.length; i<ii; i++) {
					if (files[i].isready) {
						has_ready_files++;
					}
				}
			}
			
			var item = hi.items.manager.opened(this.taskId);
			if (!item) return;
			
			var view = item.editingView;
			
			if (view) {
				if (has_ready_files) {
					dojo.addClass(view.domNode, 'tab-file-ready');
					
					//"Upload N files" button label
					var msgId = has_ready_files > 1 ? 'file_upload_multiple'  :'file_upload_single',
						msg = hi.i18n.getLocalization('properties.' + msgId).replace('%count', has_ready_files);
					
					if (view && view.uploadButton) {
						view.uploadButton.set("label", msg);
					}
					if (view && view.fileUploadButton) {
						view.fileUploadButton.set("label", msg);
					}
				} else {
					dojo.removeClass(view.domNode, 'tab-file-ready');
				}
			}
		},
		
		/**
		 * Remove files which already are completed
		 */
		removeCompleted: function () {
			var files = this.files,
				file;
			
			for(var i=files.length-1; i>=0; i--) {
				file = files[i];
				
				//Canceled and uncompleted files are not removed, only completed and destroyed
				if (file.destroyed || file.completed) {
					if (!file.destroyed) file.destroy();
					
					//Destroy internally may remove file, make sure it's not already removed
					if (files[i] && (files[i].destroyed || files[i].completed)) {
						files.splice(i, 1);
					}
				}
			}
			
			this.updateReadyView();
		},
		
		/**
		 * Removes files
		 */
		removeFile: function (id) {
			var files = this.files,
				file;
			
			for(var i=files.length-1; i>=0; i--) {
				file = files[i];
				
				if (file.id == id) {
					if (!file.destroyed) file.destroy();
					files.splice(i, 1);
					break;
				}
			}
			
			this.updateReadyView();
		},
		
		/**
		 * Reset upload widget
		 */
		reset: function (ignoreNew) {
			//Destroy files
			var files = [],
				parentIsTask = (this.taskId && this.taskId >= -1);
			
			for(var i = this.files.length - 1; i >= 0; i--) {
				if (this.files[i] && !this.files[i].destroyed && !this.files[i].uploading) {
					if (parentIsTask && this.files[i].isready) {
						//File attached to task with status 'ready for upload' should stay
						files.push(this.files[i]);
						continue;
					}
					this.files[i].destroy();
				} else {
					if (parentIsTask) {
						files.push(this.files[i]);
					}
				}
			}
			
			this.files = files;
			this.updateReadyView();
			
			if (this.nodeContainer && this.nodeContainer.parentNode && dojo.dom.isInDom(this.nodeContainer)) {
				//Create one file
				if (!ignoreNew) {
					this.addFile();
				}
			} else {
				if (!ignoreNew) {
					//Restore uploader widget
					this.create(true);
					
					if (FileUpload.HTML5_SUPPORTED && this.dndInitialized) {
						//Recreate proxy
						this.initDnDNodes();
					}
					
					//Restore files
					for(i = 0,ii = this.files.length; i < ii; i++) {
						this.files[i].restore();
					}
					
					//Add "Attach file" button
					this.addFile();
				}
			}
		},
		
		/**
		 * Reset all files (uploading or not)
		 */
		resetAll: function (ignoreNew) {
			//Destroy files
			for(var i=this.files.length-1; i>=0; i--) {
				if (this.files[i]) this.files[i].destroy();
			}
			this.files = [];
			this.updateReadyView();
			
			//Create one file
			if (!ignoreNew) {
				this.addFile();
			}
		},
		
		/**
		 * Create list
		 */
		create: function (ignoreNew) {
			var nodeContainer = this.nodeContainer = dojo.byId('file_upload_form_' + this.taskId),
				taskItem;

			if (!nodeContainer) {
				
				if (this.taskId > 0) {
					taskItem = hi.items.manager.opened(this.taskId);
					if (!taskItem) return;
					
					var propertiesView = taskItem.propertiesView;
					nodeContainer = this.nodeContainer = propertiesView ? propertiesView.fileListNode : null;
				} else {
					taskItem = hi.items.manager.item(this.taskId);
					if (!taskItem) return;
					
					var editingView = taskItem.editingView;
					nodeContainer = this.nodeContainer = editingView ? editingView.fileUploadForm : null;
				}
			}
			if (!nodeContainer) return;
			
			var nodeList = this.nodeList = document.createElement('UL');
			
			nodeList.onclick = FileList.handleClick;
			nodeContainer.appendChild(nodeList);
			
			//Create one file
			if (!ignoreNew) {
				this.addFile();
			}
		},
		
		/**
		 * Enable file drag & drop if it's supported
		 */
		initDnD: function () {
			if (!FileUpload.HTML5_SUPPORTED || this.dndInitialized) return;
			var self = this;
			this.dndInitialized = true;
			this.initDnDNodes();
		},
		
		initDnDNodes: function () {
			//Text overlay
			var item = hi.items.manager.item(this.taskId);
			if (item) {
				var nodes = dojo.query('div.file-upload',dojo.byId('propertiesViewNode_'+item.widgetId)),
					i = 0,
					ii = nodes.length;
				
				for (; i<ii; i++) {
					this.dndProxy = this.initDnDNode(nodes[i]);
				}
				/* #3194 - partial solution 
				if (!this.dndProxy) this.dndProxy = this.initDnDNode(this.nodeContainer);
				*/
			} else {
				this.dndProxy = this.initDnDNode(this.nodeContainer);
			}
		},
		
		initDnDNode: function (node) {
			var proxy = document.createElement('DIV'),
				parentIsTask = (this.taskId && this.taskId >= -1),
				msg = (parentIsTask ? 'file_upload_attach' : 'file_upload_drop');
			
			proxy.innerHTML = '<span>' + hi.i18n.getLocalization('properties.' + msg) + '</span>';
			proxy.className = 'file-drop-proxy';
			proxy.taskId = this.taskId;
			
			if (node) {
				node.appendChild(proxy);
			} else if (this.nodeContainer) {
				this.nodeContainer.appendChild(proxy);
			}
			FileDropArea.initTask(proxy);
			return proxy;
		},
		
		/**
		 * Create new file instance
		 */
		addFile: function (guid, fileName, fileSize) {
			var classObject = FileUploadFile,
				instance;
			
			if (guid) {
				classObject = FileUploadTaskHTML5;
			}
			
			var obj = new classObject(this, guid, fileName, fileSize);
			this.files.push(obj);
			
			return obj;
		},
		
		/**
		 * Returns file by guid
		 * @param {Number} guid
		 */
		getFile: function (guid) {
			var files = this.files;
			for(var i=0,ii=files.length; i<ii; i++) {
				if (files[i].id == guid) return files[i];
			}
			return null;
		},
		
		/**
		 * Upload all pending files
		 */
		upload: function () {
			var files = this.files,
				hasFiles = false;
			
			for(var i=0,ii=files.length; i<ii; i++) {
				if (this.taskId < 0) {
					if (files[i].isready) {
						hasFiles = true;
						break;
					}
				} else {
					if (files[i].isready) {
						//One file at a time
						files[i].upload();
						return;
					}
				}
			}
			
			if (this.taskId < 0) {
				FileUploadTooltip.addUploader(this);
			}
			
			if (hasFiles) {
				//Show 'Uploading files' tooltip and hide 'New item'
				FileUploadTooltip.showWhenReady = true;
				/* Bug #2025 - This will cause an error because the taskID is negative */
				
				//Project.remove(this.taskId);
			}
		},
		
		/**
		 * Upload next pending file
		 */
		uploadNext: function () {
			if (this.taskId > 0) {
				var files = this.files,
					next = null;
			
				for(var i=0,ii=files.length; i<ii; i++) {
					if (files[i].uploading) {
						return;
					} else if (files[i].isready && !files[i].canceled && !next) {
						next = files[i];
					}
				}
				
				if (next) next.upload();
			} else {
				//Uploading tooltip is handling uploads for new item
				FileUploadTooltip.uploadNext();
			}
		},
		
		/**
		 * Destroy uploader
		 */
		destroy: function () {
			var files = this.files,
				uploading = false;
			
			//Destroy files
			this.reset(true);
			
			//Destroy nodes
			if (this.taskId && this.taskId >= -1) {
				removeNode(this.nodeList);
			}
			
			//Remove references
			delete(this.nodeContainer);
			delete(this.nodeList);
			delete(this.taskId);
		}
	};
	
	return dojo.global.FileUploadWidget;

});