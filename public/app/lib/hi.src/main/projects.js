//dojo.provide("hi.main.projects");
define(function () {
	
	dojo.global.onProjectSave = function(id, params) {
		if (params) {
			var message = params.message;
			var node = dojo.byId('project' + id);
			if (node) {
				var node = dojo.query('div.project-description', node);
				if (node.length) {
					node = node[0];
					var inner = node.getElementsByTagName('DIV')[0];
					
					if (message) {
						dojo.removeClass(node, 'hidden');
						inner.innerHTML = formatMessage(message);
					} else {
						dojo.addClass(node, 'hidden');
					}
				
				}
			}
		}
	}
	
	dojo.global.deleteProject = function(element, delete_tasks) {
		var url = 'projectdelete';
		
		var opened = getParentByClass(element, 'ajax_inline', 'ajax_inline_opened');
		var params = dojo.form.getFormValues(opened);
		if (!params.title) {
			params.title = Project.data[params.id].title;
		}
		
		params.cascade = (delete_tasks ? 1 : 0);
		
		Project.ajax(url,params/*,fillTasks*/);
	}
	
	dojo.global.toggleProject = function(el) {
		HiDragSourceCached.project = false;
		var btn = el;
		btn = getParentByClass(btn, 'header');
		el = getParentByClass(btn, 'project');
		
		if (dojo.hasClass(el, 'starred')) {
			return;
		}
		
		var id, key;
		if (el.id == 'project_completed') {
			id = '_completed';
			key = 'p_completed';
		} else {
			id = parseIntRight(el.id);
			key = 'p_' + Hi_Preferences.get('currentTab', 'my', 'string') + '_' + id;
		}
		var tasks = dojo.byId('project_tasks' + id);
		if (dojo.hasClass(tasks, 'hidden')) {
			dojo.removeClass(tasks, 'hidden');
			dojo.addClass(btn, 'opened');
			if (key) Hi_Preferences.set(key, '1');
		} else {
			dojo.addClass(tasks, 'hidden');
			dojo.removeClass(btn, 'opened');
			if (key) Hi_Preferences.set(key, '0');
		}
	}
	
	/**
	 * Changes product tag of task
	 */
	
	dojo.global.setProjectPercentage = function(el, item_cnt, completed_cnt) {
		var group = Hi_Preferences.get('currentTab', 'my', 'string');
		
		if (!el) return;
		if (group != 'project') return;
		
		var percentage = '';
		var c1 = getParentByClass(el, 'project');
			c1 = dojo.query('span.percentage', c1)[0];
		
		if (item_cnt && completed_cnt) {
			percentage = Math.round(completed_cnt / item_cnt * 100);
			if (!percentage) {
				percentage = '';
				c1.style.display = 'none';
			} else {
				percentage = hi.i18n.getLocalization('project.done').replace('%d', '<b>' + percentage + '%</b>');
				c1.style.display = 'block';
				c1.innerHTML = percentage;
			}
		} else {
			c1.style.display = 'none';
		}
	}
	
	dojo.global.setItemCount = function(el, c) {
		if (!el) return;
		c = c || 0;
		c = parseInt(c);
		if (!isFinite(c)) {
			c = 0;
		}
		var c1 = dojo.query('span.cnt_span', el)[0];
		var c2 = dojo.query('span.cnt_one', el)[0];
		var c3 = dojo.query('span.cnt_mult', el)[0];
		innerText(c1, c ? c : hi.i18n.getLocalization('main.no'));
		if (c % 10 == 1 && c % 100 != 11) {
			hideElement(c3);
			showElement(c2);
		} else {
			hideElement(c2);
			showElement(c3);
		}
	}
	
	return {};
	
});