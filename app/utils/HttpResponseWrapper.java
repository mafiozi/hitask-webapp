package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.w3c.dom.Document;

import com.google.gson.JsonElement;

import play.libs.WS.HttpResponse;

public class HttpResponseWrapper {
	private HttpResponse response;
	private InputStream stream;
	
	public HttpResponseWrapper(HttpResponse response) {
		this.response = response;
		stream = response.getStream();
		resetStream();
	}
	
	private void resetStream() {
		try {
			stream.reset();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public JsonElement getJson() {
		resetStream();
		return response.getJson();
	}
	
	public Map<String, String> getQueryString() {
		resetStream();
		return response.getQueryString();
	}
	
	public String getString() {
		resetStream();
		return response.getString();
	}
	
	public Document getXml() {
		resetStream();
		return response.getXml();
	}
	
	public String getContentType() {
		return response.getContentType();
	}
}
