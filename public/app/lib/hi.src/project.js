define([

	"hi/project/base",
	"hi/project/ajax",
	"hi/project/load",
	"hi/project/save",
	"hi/project/project",
	"hi/project/task",
	"hi/project/team",
	"hi/project/remove",
	"hi/project/duplicate",
	"hi/project/limits",
	"hi/project/calendar",
	"hi/project/validate"

], function () {
	return {};
});
