//dojo.provide("hi.help");
define(function () {
	
	return dojo.global.Hi_Help = {
		
		article_id: null,
		
		loading_article_id: null,
		
		articles: {
			'_loading': {
				node_content: null
			}
		},
		
		/**
		 * Show content
		 * 
		 * @param {Object} article_id
		 */
		show: function (article_id) {
			if (!('help' in Tooltip.openers)) {
				Tooltip.open('help', "", dojo.byId('helpLink'), null, "below-centered");
			}
			
			if (!article_id) article_id = this.getFirstArticleId();
			if (!article_id) return;
			
			if (!this.articles[article_id]) {
				
				if (this.loading_article_id) {
					//Make sure article menu item is not active
					var node_toc = dojo.byId('help_toc_' + this.loading_article_id);
					if (node_toc) {
						dojo.removeClass(node_toc, 'active');
					}
				}
				
				this.loading_article_id = article_id;
				
				this.showLoading();
				
				/*Project.ajax(article_id, {}, function (response, options) {
					var content = document.createElement('DIV');
						content.className = 'help_cont_inner hidden';
						content.innerHTML = response;
					
					dojo.byId('help_cont').appendChild(content);
					
					Hi_Help.addArticle(article_id, dojo.byId('help_toc_' + article_id), content);
					
					if (Hi_Help.loading_article_id == article_id) {
						Hi_Help.show(article_id);
					}
				}, null, {response: 'plain'}, 'help');*/
				require(["dojo/request/xhr"], function(xhr) {
					xhr("/app/help/" + article_id, {
						handleAs: "html"
					}).then(function(data) {
						var content = document.createElement('DIV');
						content.className = 'help_cont_inner hidden';
						content.innerHTML = data;
						
						dojo.byId('help_cont').appendChild(content);
						Hi_Help.addArticle(article_id, dojo.byId('help_toc_' + article_id), content);
						
						if (Hi_Help.loading_article_id == article_id) {
							Hi_Help.show(article_id);
						}
					}, function(err){
						// Handle the error condition
					}, function(evt){
						// Handle a progress event from the request if the
						// browser supports XHR2
					});
				});
				
			} else {
				this.hideLoading();
				
				if (this.article_id) {
					dojo.addClass(this.articles[this.article_id].node_content, 'hidden');
					dojo.removeClass(this.articles[this.article_id].node_toc, 'active');
				}
				
				this.article_id = article_id;
				
				dojo.removeClass(this.articles[this.article_id].node_content, 'hidden');
				dojo.addClass(this.articles[this.article_id].node_toc, 'active');
			}
			
			/*
			 * Resize help content
			 * 14px is content margin top, 18px is top and bottom paddings
			 */
			dojo.byId('help_cont').style.height = dojo.byId('help_toc').offsetHeight - 14 - 18 + 'px';
			
			return false;
		},
		
		/**
		 * Feature #1721
		 */
		_show:function(article_id)
		{
			if (!('help' in Tooltip.openers)) {
				Tooltip.open('help', "", dojo.byId('helpLink'), null, "below-centered");
			}
			
			if (!article_id) article_id = this.getFirstArticleId();
			if (!article_id) return;
			
			if (!this.articles[article_id]) {
				
				if (this.loading_article_id) {
					//Make sure article menu item is not active
					var node_toc = dojo.byId('help_toc_' + this.loading_article_id);
					if (node_toc) {
						dojo.removeClass(node_toc, 'active');
					}
				}
				
				this.loading_article_id = article_id;
				
				this.showLoading();
				
				/*Project.ajax(article_id, {}, function (response, options) {
					var content = document.createElement('DIV');
						content.className = 'help_cont_inner hidden';
						content.innerHTML = response;
					
					dojo.byId('help_cont').appendChild(content);
					
					Hi_Help.addArticle(article_id, dojo.byId('help_toc_' + article_id), content);
					
					if (Hi_Help.loading_article_id == article_id) {
						Hi_Help.show(article_id);
					}
				}, null, {response: 'plain'}, 'help');*/
				require(["dojo/request/xhr"], function(xhr) {
					xhr("/app/help/" + article_id, {
						handleAs: "html"
					}).then(function(data) {
						var content = document.createElement('DIV');
						content.className = 'help_cont_inner hidden';
						content.innerHTML = data;
						
						dojo.byId('help_cont').appendChild(content);
						Hi_Help.addArticle(article_id, dojo.byId('help_toc_' + article_id), content);
						
						if (Hi_Help.loading_article_id == article_id) {
							Hi_Help.show(article_id);
						}
					}, function(err){
						// Handle the error condition
					}, function(evt){
						// Handle a progress event from the request if the
						// browser supports XHR2
					});
				});
				
			} else {
				this.hideLoading();
				
				if (this.article_id) {
					dojo.addClass(this.articles[this.article_id].node_content, 'hidden');
					dojo.removeClass(this.articles[this.article_id].node_toc, 'active');
				}
				
				this.article_id = article_id;
				
				dojo.removeClass(this.articles[this.article_id].node_content, 'hidden');
				dojo.addClass(this.articles[this.article_id].node_toc, 'active');
			}
			
			/*
			 * Resize help content
			 * 14px is content margin top, 18px is top and bottom paddings
			 */
			dojo.byId('help_cont').style.height = dojo.byId('help_toc').offsetHeight - 14 - 18 + 'px';
			
			return false;			
		},
		
		showLoading: function () {
			if (!this.articles['_loading'].node_content) {
				this.articles['_loading'].node_content = dojo.byId('help_cont_loading');
			}
			
			if (this.article_id) {
				dojo.addClass(this.articles[this.article_id].node_content, 'hidden');
				dojo.removeClass(this.articles[this.article_id].node_toc, 'active');
			}
			
			if (this.loading_article_id) {
				var node_toc = dojo.byId('help_toc_' + this.loading_article_id);
				if (node_toc) {
					dojo.addClass(node_toc, 'active');
				}
			}
			
			dojo.removeClass(this.articles['_loading'].node_content, 'hidden');
		},
		
		hideLoading: function () {
			dojo.addClass(this.articles['_loading'].node_content, 'hidden');
		},
		
		hide: function () {
			Tooltip.close('help');
		},
		
		/**
		 * Add article to the stack
		 */
		addArticle: function (article_id, node_toc, node_content) {
			this.articles[article_id] = {
				id: article_id,
				node_toc: node_toc,
				node_content : node_content
			};
		},
		
		/**
		 * Returns first article id
		 */
		getFirstArticleId: function () {
			var node = dojo.byId('help_toc').getElementsByTagName('LI')[0];
			var id = node.getAttribute('id');
				id = id.match(/help_toc_([0-9a-z\-\_]*)/);
				
			return (id && id[1] ? id[1] : null);
		}
	};

});