//hiTask build profile

dependencies = {
	
	selectorEngine: "acme",
	cssOptimize: "comments",
	
	layers:  [
		{
			// This is a specially named layer, literally 'dojo.js'
			// adding dependencies to this layer will include the modules
			// in addition to the standard dojo.js base APIs. 
			name: "dojo.js",
			dependencies: [
				"dojo/require",
				"dojo/on",
				
				"dijit/dijit",
				"dojo/i18n",
				
				"dojo/dnd/common",
				"dojo/dnd/autoscroll",
				"dojo/dnd/Mover",
				"dojo/dnd/Moveable",
				"dojo/dnd/move",
				"dojo/dnd/TimedMoveable",
				"dojo/dnd/Container",
				"dojo/dnd/Avatar",
				"dojo/dnd/Manager",
				"dojo/dnd/Selector",
				"dojo/dnd/Source",
				"dojo/dnd/AutoSource",
				"dojo/dnd/Target",
				
				"dojo/html",
				"dojo/fx",
				"dojo/fx/Toggler",
				
				"dojox/layout/ResizeHandle",
				
				"dijit/Tooltip",
				"dijit/TooltipDialog",
				"dijit/form/Button",
				"dijit/form/ComboButton",
				"dijit/form/TextBox",
				"dijit/form/ValidationTextBox",
				"dijit/form/NumberTextBox",
				"dijit/form/CurrencyTextBox",
				"dijit/form/DateTextBox",
				"dijit/form/TimeTextBox",
				"dijit/form/Textarea",
				"dijit/layout/TabContainer",
				
				"dojox/flash",
				"dojox/form/Uploader",
				"dojox/form/uploader/plugins/Flash",
				"dojox/form/uploader/plugins/HTML5",
				"dojox/av/FLAudio",
				
				"dijit/Dialog",
				"dojo/currency",
				"dojo/date/locale",
				
				"supra/common",
				"moment/moment",
				
				"dojox/timing/doLater",
				"dojox/storage/LocalStorageProvider",
				"dojox/storage/CookieStorageProvider",
				"dojox/storage/FlashStorageProvider"
			]
		},
		{
			name: "../hi/build-api.js",
			dependencies: [
				"hi/build-api"
			]
		},
		{
			name: "../hi/build-archive.js",
			dependencies: [
				"hi/build-archive"
			]
		},
		{
			name: "../hi/build-global.js",
			dependencies: [
				"hi/build-global"
			]
		},
		{
			name: "../hi/build-hitask.js",
			dependencies: [
				"hi/build-hitask"
			]
		},
		{
			name: "../hi/build-igoogle.js",
			dependencies: [
				"hi/build-igoogle"
			]
		},
		{
			name: "../hi/build-settings.js",
			dependencies: [
				"hi/build-settings"
			]
		},
		{
			name: "../hi/build-timereport.js",
			dependencies: [
				"hi/build-timereport"
			]
		},
		{
			name: "../hi/build-googlecal.js",
			dependencies: [
				"hi/build-googlecal"
			]
		}
	],
	
	prefixes: [
		[ "dijit", "../dijit" ],
		[ "dojox", "../dojox" ],
		[ "hi", "../../hi.src" ],
		[ "supra", "../../supra" ],
		[ "moment", "../../moment" ],
		[ "css", "../../../css" ]
	]
};