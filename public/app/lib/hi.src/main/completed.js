//dojo.provide("hi.main.completed");

/*
 * Complete tasks functionality
 */
define(function () {
	
	/**
	 * Returns true if project completed tasks are visible, otherwise false
	 */
	dojo.global.isCompletedTasksVisible = function(project_id) {
		if (typeof isCompletedTasksVisible.visibilityData[project_id] == 'undefined') {
			isCompletedTasksVisible.visibilityData[project_id] = false;
		}
		
		return isCompletedTasksVisible.visibilityData[project_id];
	}
	/**
	 * Set if project completed tasks are visible
	 */
	dojo.global.setCompletedTasksVisible = function(project_id, is_visible) {
		isCompletedTasksVisible.visibilityData[project_id] = is_visible;
	}
	dojo.global.isCompletedTasksVisible.visibilityData = {};
	
	
	dojo.global.setCompletedListVisibility = function(ul, visible) {
		if (visible) {
			dojo.removeClass(ul, 'completed-hidden');
		} else {
			dojo.addClass(ul, 'completed-hidden');
		}
		
		setCompletedTasksVisible(ul.id, visible);
	}
	dojo.global.toggleCompletedListVisibility = function(el) {
		var ul = dojo.dom.getAncestorsByTag(el, 'UL')[0];
		if (isCompletedTasksVisible(ul.id)) {
			setCompletedListVisibility(ul, false);
		} else {
			setCompletedListVisibility(ul, true);
		}
	}
	
	/**
	 * Update "And X completed items..." text and show/hide this message
	 */
	dojo.global.updateCompletedListTitle = function(el, completed_count) {
		var div = el.getElementsByTagName('DIV')[0];
		
		if (completed_count == 0) {
			dojo.addClass(el, 'hidden');
		} else {
			dojo.removeClass(el, 'hidden');
			var it = '';
			
			if (completed_count == 1) {
				it = '<span class="t-1">' + hi.i18n.getLocalization('center.completed_item_count', '') + '</span>' +
					 '<span class="t-2">' + hi.i18n.getLocalization('center.completed_item', '') + '</span>';
			} else {
				it = '<span class="t-1">' + hi.i18n.getLocalization('center.completed_items_count', '') + '</span>' +
					 '<span class="t-2">' + hi.i18n.getLocalization('center.completed_items', '') + '</span>';
			}
			
			div.innerHTML = it.replace("%s", completed_count);
		}
	}
	
	//TODO
	return {};
});