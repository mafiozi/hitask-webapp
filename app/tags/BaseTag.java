package tags;

import java.util.Map;

import com.google.gson.Gson;

import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;

public class BaseTag extends FastTags {
	protected static final String DEFAULT_TAG = "div";
	protected static final String PARENT_ATTRS_TEMPLATE_VARIABLE_NAME = "attrs1";
	
	protected static Map<?, ?> getParentArgs(ExecutableTemplate template) {
		return (Map<?, ?>)template.getBinding().getVariables().get(PARENT_ATTRS_TEMPLATE_VARIABLE_NAME);
	}
	
	protected static Boolean getArgAsBoolean(String argName, Map<?, ?> args) {
		return args.get(argName) != null ? Boolean.parseBoolean((String)args.get(argName)) : null;
	}
	
	protected static Boolean getArgAsBoolean(String argName, Map<?, ?> args, Boolean defaultVal) {
		Boolean val = getArgAsBoolean(argName, args);
		return val != null ? val : defaultVal;
	}
	
	protected static Integer getArgAsInt(String argName, Map<?, ?> args) {
		try {
			return args.get(argName) != null ? Integer.parseInt((String)args.get(argName)) : null;
		} catch (NumberFormatException nfe) {
			return null;
		}
	}
	
	protected static Integer getArgAsInt(String argName, Map<?, ?> args, int defaultVal) {
		Integer val = getArgAsInt(argName, args);
		return val != null ? val : defaultVal;
	}
	
	protected static String getArgAsString(String argName, Map<?, ?> args) {
		return args.get(argName) != null ? (String)args.get(argName) : null;
	}
	
	protected static String getArgAsString(String argName, Map<?, ?> args, String defaultVal) {
		String val = getArgAsString(argName, args);
		return val != null ? val : defaultVal;
	}
	
	protected static String getArgAsJson(String argName, Map<?, ?> args) {
		return args.get(argName) != null ? toJson(args.get(argName)) : null;
	}
	
	protected static String toJson(Object obj) {
		Gson g = new Gson();
		return g.toJson(obj);
	}
}