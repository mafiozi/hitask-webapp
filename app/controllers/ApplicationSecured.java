package controllers;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import mixpanel.MixpanelEventsBuilder;
import mixpanel.MixpanelUtils;
import nl.bitwalker.useragentutils.UserAgent;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.Play;
import play.libs.Crypto;
import play.libs.Time;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Http.Cookie;
import play.mvc.Http.Header;
import play.mvc.Util;
import utils.Constants;
import api.ApiCall;
import api.ApiCallException;

public class ApplicationSecured extends Controller {
	protected static final String REMEMBER_ME_KEY = "rememberme10";
	private static final String REDIRECT_TO_AFTER_LOGIN_KEY = "redirectToAfterLogin";
	private static final String BREADCRUMB_KEY = "breadcrumb";
	private static final String LOGIN_ENTRY_POINT_ACTION = "Application.index";
	private static final String DEFAULT_REMEMBER_ME_COOKIE_MAX_AGE = "60d";
	private static final String REMEMBER_ME_COOKIE_SEPARATOR = "|";
	
	@Before(unless={"Login.out", "Login.index", "OAuth.socialAuth", "OAuth.socialAuthCallback", "Login.oauthApprovalDenied"}, priority=0)
	protected static void secure() {
		if (!isLoggedIn() || request.params.get(Constants.SESSION_ID_KEY) != null) {
			tryToEnter();
			
			// Save URL in cookie in order to redirect user back after successful login.
			sendRedirectToAfterLoginCookie("get".equalsIgnoreCase(request.method) ? Play.ctxPath + request.url : null);
			Login.index();
		}
	}
	
	@Util
	protected static void sendRedirectToAfterLoginCookie(String url) {
		if (!ArrayUtils.contains(new String[] {"Login.oauthApproval"}, request.action)) {
			response.setCookie(REDIRECT_TO_AFTER_LOGIN_KEY, url);
		}
	}
	
	@Util
	protected static void sendRememberMeCookie(String sessionId) {
		response.setCookie(REMEMBER_ME_KEY, rememberMeCookieValue(sessionId), DEFAULT_REMEMBER_ME_COOKIE_MAX_AGE);
	}
	
	/**
	 * Tries to log in user using passed request parameters or RememberMe cookie.
	 */
	private static void tryToEnter() {
		String sessionId = request.params.get(Constants.SESSION_ID_KEY);
		Cookie rememberMeCookie = request.cookies.get(REMEMBER_ME_KEY);
		
		if (request.action.equals(LOGIN_ENTRY_POINT_ACTION) && sessionId != null) {
			// If current action is entry point action for logging in user.
			session.put(Constants.SESSION_ID_KEY, sessionId);
			
			// Commented as requested in #2809.
			//sendMixPanelLoginEvent(sessionId);
			
			// #5261: always send RememberMe cookie.
			sendRememberMeCookie(sessionId);
			
			String breadcrumb = request.params.get(BREADCRUMB_KEY);
			if (StringUtils.isNotBlank(breadcrumb)) {
				flash.put(BREADCRUMB_KEY, breadcrumb);
			}
			
			redirectToOriginalUrl();
		} else if (rememberMeCookie != null && StringUtils.isNotBlank(rememberMeCookie.value)) {
			// Try to sign in user using RememberMe cookie.
			int firstIndex = rememberMeCookie.value.indexOf(REMEMBER_ME_COOKIE_SEPARATOR);
			int lastIndex = rememberMeCookie.value.lastIndexOf(REMEMBER_ME_COOKIE_SEPARATOR);
	
			if (lastIndex > firstIndex) {
				String sign = rememberMeCookie.value.substring(0, firstIndex);
				String restOfCookie = rememberMeCookie.value.substring(firstIndex + 1);
				sessionId = rememberMeCookie.value.substring(firstIndex + 1, lastIndex);
				String time = rememberMeCookie.value.substring(lastIndex + 1);
				Date expirationDate = null;
				try {
					expirationDate = new Date(Long.parseLong(time));
				} catch (NumberFormatException e) {
					Logger.warn("could not parse rememberMe cookie: %s", rememberMeCookie.value);
				}
				Date now = new Date();
				
				if (expirationDate == null || expirationDate.before(now)) {
					Login.out();
				}
				
				if (Crypto.sign(restOfCookie).equals(sign)) {
					session.put(Constants.SESSION_ID_KEY, sessionId);
					// Commented as requested in #2809.
					//sendMixPanelLoginEvent(sessionId);
					redirectToOriginalUrl();
				} else {
					Login.out();
				}
            } else {
            	Login.out();
            }
		}
	}
	
	protected static boolean isLoggedIn() {
		return session.get(Constants.SESSION_ID_KEY) != null;
	}
	
	protected static boolean rememberMeCookieSent() {
		Cookie c = request.cookies.get(REMEMBER_ME_KEY);
		return c != null && StringUtils.isNotBlank(c.value);
	}
	
	protected static String getSessionId() {
		return session.get(Constants.SESSION_ID_KEY);
	}
	
	protected static void sendMixPanelLoginEvent(String sessionId) {
		try {
			MixpanelUtils.send(String.valueOf(new ApiCall(sessionId).getUser().getId()), new MixpanelEventsBuilder(getUserAgent()).add("Log In"));
		} catch (Exception e) {
			Logger.warn("failed to send Log In event to mixpanel: %s", e.getMessage());
		}
	}
	
	/**
	 * Generates signed value for RememberMe cookie.
	 * @param sessionId
	 * @return
	 */
	private static String rememberMeCookieValue(String sessionId) {
		Date expiration = new Date();
		expiration.setTime(expiration.getTime() + Time.parseDuration(DEFAULT_REMEMBER_ME_COOKIE_MAX_AGE));
		return new StringBuilder().append(Crypto.sign(sessionId + REMEMBER_ME_COOKIE_SEPARATOR + expiration.getTime()))
			.append(REMEMBER_ME_COOKIE_SEPARATOR).append(sessionId).append(REMEMBER_ME_COOKIE_SEPARATOR).append(expiration.getTime())
			.toString();
	}
	
	/**
	 * Clear Play session and remove cookies related to user session.
	 */
	protected static void doLogout() {
		session.clear();
		response.removeCookie(REMEMBER_ME_KEY);
		response.removeCookie("remember_me");
		response.removeCookie("rememberme");
		response.removeCookie(Play.configuration.getProperty("application.session.cookie", ""));
	}
	
	private static void redirectToOriginalUrl() {
		Cookie redirectCookie = request.cookies.get(REDIRECT_TO_AFTER_LOGIN_KEY);
		
		// If respective redirect cookie is provided use its value for redirect.
		if (redirectCookie != null && redirectCookie.value != null) {
			response.removeCookie(REDIRECT_TO_AFTER_LOGIN_KEY);
			redirect(redirectCookie.value);
		} else {
			Application.index();
		}
	}
	
	protected static void renderBackendErrorTemplate(ApiCallException e) {
		renderTemplate("errors/backendError.html");
	}
	
	protected static UserAgent getUserAgent() {
		Header h = request.headers.get("user-agent");
		return h != null && StringUtils.isNotBlank(h.value()) ? UserAgent.parseUserAgentString(h.value()) : null;
	}
	
	protected static String encode(String part) {
        try {
            return URLEncoder.encode(part, "utf-8");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
	protected static String buildQueryString(Map<String, String> parameters) {
		StringBuilder sb = new StringBuilder();
        for (String key : parameters.keySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            Object value = parameters.get(key);

            if (value != null) {
                if (value instanceof Collection<?> || value.getClass().isArray()) {
                    Collection<?> values = value.getClass().isArray() ? Arrays.asList((Object[]) value) : (Collection<?>) value;
                    boolean first = true;
                    for (Object v : values) {
                        if (!first) {
                            sb.append("&");
                        }
                        first = false;
                        sb.append(encode(key)).append("=").append(encode(v.toString()));
                    }
                } else {
                    sb.append(encode(key)).append("=").append(encode(parameters.get(key).toString()));
                }
            }
        }
        return sb.toString();
	}
	
	protected static void logDetailedError(Exception e, String msg) {
		Logger.error(e, "Error occured %s, action %s, headers %s, params %s", msg, request.action, request.headers, request.params);
	}
}
