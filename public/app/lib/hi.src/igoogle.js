define([
	'dojo/on'
], function (dojo_on) {
	
  	var apiKey;
  	var apiVersion;
  	var prefs;
	var authenticationsCount = 0;
  	var json = true;
  	var intervalID;
  	
  	dojo.global.init = function (){
  		prefs = new _IG_Prefs();
  		apiKey = prefs.getString('apiKey');
  		
  		if (!apiKey) {
  			apiKey = document.getElementById('apiKey').value;
  		}
  		if (!apiVersion) {
  			apiVersion = document.getElementById('apiVersion').value;
  		}
  		getTaskList();
  		if (prefs.getInt('refreshInterval')) {
  			intervalID = setInterval(refreshWindow, prefs.getInt('refreshInterval'));
  		}
  		else {
  			intervalID = setInterval(refreshWindow, 60000);
  		}
  		
  		setInterval(nullAuthenticationsCount, 600000);
  		setTab(dojo.cookie('tabID') || 0);
	}
	
	dojo_on(window, 'load', init);
	
	dojo.global.loginRequest = function () {
			authenticationsCount  = authenticationsCount + 1;
  			try {
  				var params = {apiType: 'igoogle', apiKey: apiKey, apiVersion: apiVersion, json: json};
  				ajaxRequestGet('authenticate', params, checkLogin, error);
  			}
  			catch (ex) {
  				console.log(ex);
  			}
	}
  	
	dojo.global.setTaskComplete = function (taskID) {
		var inp = document.getElementById(taskID);
		var completed;
		if (inp.checked) {
			completed = 1;
		}
		else {
			completed = 0;
		}
		document.getElementById(taskID).className='checked';
		value = taskID.split('-');
		id = value.pop();
		sessionID = dojo.cookie('session_id');
		
		/*
		Project.ajax('itemnew', 
						  {AS: sessionID, id: id, completed: completed, apiKey: apiKey, apiVersion: apiVersion, json: json},
						  '', 
						  error, 
						  '', 
						 'data');*/
		var params = {AS: sessionID, id: id, completed: completed, apiKey: apiKey, apiVersion: apiVersion, json: json};
		ajaxRequestGet('itemcomplete', params, taskCompleted, error);				
	}
	dojo.global.taskCompleted = function () {
		return true;
	}
	
	dojo.global.refreshWindow = function () {
		getTaskList();
		Tabs.build("settings_tab", {
			content: "settings",
			selectedClass: "active", 
			defaultTab: getTabId,
			names:{0: "0",1: "1",2: "2",3: "3",4: "4",5: "5"},
			hashInterval: 100,
			openFirst: true,
			elements: [0,1,2,3,4,5]
		});
	}
	dojo.global.addBehaviour = function () {
		var holder = document.getElementById('googleWidget'); if (!holder) return;
		var inputs = holder.getElementsByTagName('input');
		
		var _on_change_handle = function () {
			setTaskComplete(this.id);
			this.parentNode.className = this.parentNode.className.replace(/linethrough/, '');
			
			if (this.checked)
			{
				this.parentNode.className += ' linethrough';
			}
		};
		
		for(var i=0, j=inputs.length; i<j; i++)
		{
			inputs[i].onclick = _on_change_handle;
			inputs[i].onmouseup = _on_change_handle;
			inputs[i].onkeyup = _on_change_handle;
			inputs[i].onchange = _on_change_handle;
		}
	}
	dojo.global.templateFill = function (data) {
		delete data.response_status;
		
		taskListULToday = document.getElementById('parentToday');
		taskListULAll = document.getElementById('parentAll');
		
		while (taskListULToday.childNodes.length >= 1) {
			taskListULToday.removeChild(taskListULToday.firstChild);
		}
		
		while (taskListULAll.childNodes.length >= 1) {
			taskListULAll.removeChild(taskListULAll.firstChild);
		}
		var node;
		data = data['items'];
		for (i in data) {
			 
			if (data[i].completed == 1 || data[i].category == 0) {
				continue;
			}
			var start_date = data[i].start_date;
			var end_date = data[i].end_date;
			
			if ((start_date > getTodayDate() || !start_date)
											 || (start_date < getTodayDate() && !end_date)
											 || (start_date <= getTodayDate() && end_date < getTodayDate() && end_date)
											 ) 
			{
				node = taskListULAll;
			}
			else {
				node = taskListULToday;
			}
			
			var taskLI = document.createElement('li');
			var taskTitle = data[i].title;
			var taskCheckbox = document.createElement('input');
			var taskLable = document.createElement('label');
			
			if (data[i].end_time) {
				taskLI.title = data[i].end_time;
			}
			
			taskCheckbox.type = 'checkbox';
			taskCheckbox.id = 'task-check-' + data[i].id;
			taskCheckbox.title="Click to complete task";
			taskCheckbox.onclick = function () {setTaskComplete(this.id)};
			
			taskLable.htmlFor = taskCheckbox.id;
			taskLable.innerHTML = taskTitle;
			
			taskLI.appendChild(taskCheckbox);
			taskLI.appendChild(taskLable);
			
			node.appendChild(taskLI);
		}
		
		if (taskListULAll.childNodes.length == 0) {
				var noTasksLiAll = document.createElement('li');
				noTasksLiAll.innerHTML = 'No tasks'; 
				taskListULAll.appendChild(noTasksLiAll);
		}
		if (taskListULToday.childNodes.length == 0) {
				var noTasksLi = document.createElement('li');
				noTasksLi.innerHTML = '<a href="#" onCLick="showAllTasks();return false;">Show All</a>';
				taskListULToday.appendChild(noTasksLi);
		}
			
		addBehaviour();
	}
	dojo.global.getTaskList = function () {
		sessionID = dojo.cookie('session_id');
		
		var params = {AS: sessionID, apiKey: apiKey, apiVersion: apiVersion, json: json};
		ajaxRequestGet('itemlist', params, checkGlobalResponse, error);
	}
	
	dojo.global.isSessionExists = function () {
		if (dojo.cookie('session_id')) {
			session_id = dojo.cookie('session_id');
			var params = {session: session_id, apiType: 'igoogle', apiKey: apiKey, apiVersion: apiVersion};
  			ajaxRequestGet('authenticate', params, hideLoginBlock, error);
		}
		else {
			return;
		}
		return true;				  		
	}
	
	dojo.global.showAllTasks = function () {
		Tabs.open('settings_tab', '1');
		setTab(1);
	}
	
	dojo.global.hideLoginBlock = function (data) {
		document.getElementById('loginBlock').style.display = 'none';
		document.getElementById('tabsDiv').className = '';
		templateFill(data);
	}
	dojo.global.showLoginBlock = function () {
		document.getElementById('loginBlock').style.display = 'block';
	}
	dojo.global.error = function (data) {
		document.getElementById('errorDiv').innerHTML = 'Service unavailable';
	}
	dojo.global.checkLogin = function (data) {
		if (authenticationsCount > 3) 
				location.href = '/igoogle/login?apiKey=' + apiKey + '&apiVersion=' + apiVersion;
		if (data.response_status) {
			if (data.error_response == 1) {
				location.href = '/igoogle/login?apiKey=' + apiKey + '&apiVersion=' + apiVersion;
			}
			else {
				error();
			}
		}
		else {
			dojo.cookie('session_id', data.session_id);
			getTaskList();
		}

	}
	dojo.global.switchTaskType = function (tabID) {
		Tabs.open('settings_tab', document.getElementById('taskType').selectedIndex);
	}
	dojo.global.showTaskForm = function () {
		document.getElementById('task_form').style.display = 'block';
	}
	
	dojo.global.strPadLeft = function (str, num, ch) {
		str = str.toString();
		ch = ch.toString();
		while (str.length < num && ch.length > 0) {
			str = ch + str;
		}
		return str;
	}
	
	dojo.global.getTodayDate = function () {
		var date;
		var myDate = new Date();
  		var date1 = strPadLeft(myDate.getDate(), 2, '0');
  		var date2 = strPadLeft(myDate.getMonth() + 1, 2, '0');
  		var date3 = myDate.getFullYear();
  		date = date3 + '-' + date2+ '-' + date1;
  		return date;
	}
	dojo.global.checkGlobalResponse = function (data) {
		if (authenticationsCount > 3) location.href = '/googlecalendar/login?apiKey=' + apiKey + '&apiVersion=' + apiVersion;
		switch(data.response_status) {
			case 1: {
				hideLoginBlock(data);
			}
			break;
			case 2 : {
				if (dojo.cookie('widget_remember_me')) {
					loginRequest();
				}
				else {
					location.href = '/igoogle/login?apiKey=' + apiKey + '&apiVersion=' + apiVersion;
				}
			}
			break;
			case 3 : {
				showLoginBlock();
			}
			break;
			case 4 : {
				document.getElementById('errorDiv').innerHTML = 'Service unavailable';						
			}
			break;
			case 5 : {
				document.getElementById('errorDiv').innerHTML = 'Widget update required. Please click <a target="_blank" href="'+document.getElementById('widgetHost').value+'/access">here</a> to download new version';
				clearInterval(intervalID);
			}
			break;
		}
	}
	
	dojo.global.setTab = function (tabID) {
		var curID = dojo.cookie('tabID');
		dojo.cookie('tabID', tabID);
		var el = document.getElementById('tab_id_' + tabID);
		var cur = document.getElementById('tab_id_' + curID);
		if (cur)
		{
			cur.className = cur.className.replace(/active/g, '');
			//alert(cur.className);
		}
		if (el)
		{
			el.className += ' active';
		}
	}
	
	dojo.global.getTabId = function (options) {
		return dojo.cookie('tabID');
	};
	
	dojo.global.nullAuthenticationsCount = function () {
		authenticationsCount = 0;
	};
	
	return {};

});