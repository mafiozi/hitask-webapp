define([
	'dojo/_base/sniff',
	'dojo/dom-class',
	'supra/common'
], function (has, domClass) {
	//hack dojo.isIE in IE 11 since (https://bugs.dojotoolkit.org/ticket/17311)
	var oldIE = has("ie");
	dojo.isIE = dojo.isIE || has.add("ie", oldIE, true, true);
	dojo.isIE = dojo.isIE ? dojo.isIE : (has('trident') ? 11 : false);
	/**
	 * Create a namespace
	 */
	// dojo.hasClass = cls.contains;
	// dojo.addClass = cls.add;
	// dojo.removeClass = cls.remove;
	// dojo.toggleClass = cls.toggle;
	// dojo.replaceClass = cls.replace;
	var _rc = domClass.remove;
	var _ac = domClass.add;
	var _hc = domClass.contains;
	var _tc = domClass.toggle;

	dojo.removeClass = domClass.remove = function(node, className) {
		if (!node || !dojo.byId(node)) return;

		_rc(node, className);
	};

	dojo.addClass = domClass.add = function(node, className) {
		if (!node || !dojo.byId(node)) return;

		_ac(node, className);
	};

	dojo.toggleClass = domClass.toggle = function(node, className, condition) {
		if (!node || !dojo.byId(node)) return;

		return _tc(node, className, condition);
	};

	dojo.hasClass = domClass.has = function(node, className) {
		if (!node || !dojo.byId(node)) return;

		return _hc(node, className);
	};

	dojo.ns = function (ns) {
		ns = ns.split(/[\/\.]/);
		
		var context = dojo.global;
		for(var i=0,ii=ns.length; i<ii; i++) {
			if (!context[ns[i]]) context[ns[i]] = {};
			context = context[ns[i]];
		}
	};
	
	dojo.ns("supra/common");
	
	supra.common = {
		showElement: function(el) {
			el = dojo.byId(el);
			if (!el) return false;
			el.style.display = '';
			supra.common.removeClass(el, 'hidden');
			return true;
		},
		
		hideElement: function(el) {
			el = dojo.byId(el);
			if (!el) return false;
			supra.common.addClass(el, 'hidden');
			return true;
		},
		
		addClass: function(el, cl) {
			return dojo.addClass(dojo.byId(el), cl);
		},
		
		removeClass: function(el, cl) {
			return dojo.removeClass(dojo.byId(el), cl);
		},
		
		hasClass: function(el, cl) {
			return dojo.hasClass(dojo.byId(el), cl);
		},
		
		parseIntRight: function(s) {
			if (typeof s == 'undefined') s = '';
			var t = '';
			var l = s.length;
			while ((isFinite(parseInt(s.substr(l - 1, 1))) || s.substr(l - 1, 1) == '-') && t.substr(0, 1) != '-') {
				t = s.substr(l - 1, 1) + t;
				s = s.substr(0, l - 1);
				l--;
			}
			if (t == '') t = 0;
			return t;
		},
		
		getElementsByClass: function(cl, el) {
			return dojo.query('.' + cl, el);
		},
		
		getParentByClass: function(el, cn1, cn2) {
			el = dojo.byId(el);
			while (el && !dojo.hasClass(el, cn1)) {
				el = el.parentNode;
			}
			if (cn2) {
				return supra.common.getElementsByClass(cn2, el).pop();
			} else {
				return el;
			}
		},
		
		htmlspecialchars: function (ch) {
			if (ch.toString) {
				ch = ch.toString();
			} else {
				ch = '';
			}
			ch = ch.replace(/&/g,"&amp;");
			ch = ch.replace(/\"/g,"&quot;");
			ch = ch.replace(/\'/g,"&#039;");
			ch = ch.replace(/</g,"&lt;");
			ch = ch.replace(/>/g,"&gt;");
			return ch;
		},
		
		unhtmlspecialchars: function (ch) {
			if (typeof ch != 'string') {
				ch = '';
			}
			ch = ch.replace(/&quot;/g,'"');
			ch = ch.replace(/&#039;/g,"'");
			ch = ch.replace(/&lt;/g,"<");
			ch = ch.replace(/&gt;/g,">");
			ch = ch.replace(/&amp;/g,"&");
			return ch;
		}
	};
	
	return supra.common;
	
});