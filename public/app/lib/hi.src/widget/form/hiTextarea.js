define([
	"dojo/_base/declare",
	"dijit/form/Textarea",
	'dojo/sniff',
	'dojo/on',
	'dojo/_base/event'
], function(declare, ta, sniff, dojoOn, dojoEvent) {

	var hiTextarea = declare("hi.widget.Textarea", [ta], {
		// Fire the change event for every text change
		intermediateChanges: true,

		//native support placeholder
		_sp: false,

		_inPlaceHolder: false,

		_shadowValue: '',

		// PostCreate method
		// Fires *after* nodes are created, before rendered to screen
		postCreate: function() {
			// Do what the previous does with this method
			var dn = this.domNode,
				sh = (dn.style.height && dn.style.height !== 'auto')? parseInt(dn.style.height, 10) : undefined;

			this.inherited(arguments);

			// Add widget class to the domNode
			this._sp = false;
			this.set('placeHolder', this.placeHolder);

			sh && this.set('widgetHeight', sh);

			// dojo.connect(this, 'resize', function(){
			// 	domNode.style.height = '';
			// });
			var that = this;
			dojoOn(this.domNode, 'keydown', function(e) {
				var max = that.get('maxLines');
				if (max) {
					var has = (that.get('value') || '').split("\n");
					if (e.keyCode == 13 && has.length >= max) {
						dojoEvent.stop(e);
					}
				}
			});
			dojoOn(this.domNode, 'keyup', function(e) {
				var max = that.get('maxLines');
				if (max) {
					var has = (that.get('value') || '').split("\n");
					if (has.length > max) {
						that.set('value', that._validateMaxLinesAndTruncate(that.get('value')));
					}
				}
			});
		},
		
		resize: function(){
			this.inherited(arguments);

			var widgetHeight = this.get('widgetHeight'),
				dn = this.domNode,
				sh = (dn.style.height && dn.style.height !== 'auto')? parseInt(dn.style.height, 10) : undefined;

			if(widgetHeight && sh && widgetHeight > sh){
				dn.style.height = widgetHeight + 'px';
			}
		},

		hasNativePlaceholderSupport: function() {
			var inp = document.createElement('input');
			return (this._sp = typeof(inp.placeholder) != 'undefined');
		},
		
		_validateMaxLinesAndTruncate: function(value) {
			if (value && this.get('maxLines')) {
				value = value.split("\n").slice(0, this.get('maxLines')).join("\n");
			}
			return value;
		},
		
		_setWidgetHeight: function(v){
			this._set('widgetHeight', v);
		},

		_setPlaceHolderAttr: function(v){
			this._set("placeHolder", v);

			if (this._sp) {
				dojo.attr(this.domNode, 'placeholder', v);
			} else {
				this._updatePlaceHolder();
			}
		},
		
		_setMaxLinesAttr: function(v) {
			v = parseInt(v);
			this._set('maxLines', isNaN(v) || v < 1 ? null : v);
		},

		_setValueAttr: function(value, /*Boolean?*/ priorityChange, /*String?*/ formattedValue){
			if (!this.textbox || !this.domNode) return false;
			if (value === undefined || value === null) value = '';
			
			this._shadowValue = value;
			this.inherited(arguments);
		},

		_getValueAttr: function(){
			return this._shadowValue;
		},

		_get: function(name){
			return this[name];
		},

		_updatePlaceHolder: function(){
			if(this._sp){
				return;
			}

			var _sv = this._shadowValue,
				nd = this.domNode,
				cp = 0,		//current textarea pointer position
				p = this.get('placeHolder');
			
			if(this.focused && nd.value == p){
				dojo.removeClass(nd, 'placeHolder');
				dojo.addClass(nd, 'inputText');
				if(sniff('ie') || sniff('trident')){
					cp = nd.selectionStart;
					nd.value = _sv;
					nd.setSelectionRange(cp, cp);
				}else{
					nd.value = _sv;
				}
			}else{
				if(_sv === '' && !this.focused){
					dojo.addClass(nd, 'placeHolder');
					dojo.removeClass(nd, 'inputText');
					nd.value = p
				}else{
					dojo.removeClass(nd, 'placeHolder');
					dojo.addClass(nd, 'inputText');
				}
			}
		},

		_onInput: function(e){
			this._shadowValue = this.domNode.value;
			this.inherited(arguments);
		},

		_onBlur: function(e){
			if(!this.disabled){ 
				var t = this;
				setTimeout(function(){
					t._updatePlaceHolder();
				}, 0);
			}
		}
	});
	
	return hiTextarea;
});