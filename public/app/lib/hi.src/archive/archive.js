define(function () {

	dojo.global.Hi_Archive = {
		firstLoaded: false,
		
		search_timeout: 1000, // timeout in MS before sending request to server after search box changed.
		
		search_max_str: 128,
		
		cur_offset : 0,
		
		cur_q: '',
		
		data: {},
		
		expandedChildren: {},
		
		generateChildrenHtml: function (id) {
			var node = dojo.byId('children_task_' + id);
			if (node) {
				var html = '';
				
				for(var i in Hi_Archive.data) {
					var data = Hi_Archive.data[i];
					if (data.category != 5 && data.parent == id) {
						var is_completed = (data.completed == '1' || data.completed === 1);
						html += Hi_Archive.generateTaskHTML(data, is_completed, 4);
					}
				}
				
				node.innerHTML = html;
				
				var li = dojo.byId('task_' + id);
				/* #3024 dojo.addClass(li, 'expanded'); */
				
				// #2579:
				if (Project.hasNonFileChildren(id)) {
					dojo.addClass(li, 'has-children');
				}
			}
		},
		
		isProjectItemCategory: function(category) {
			return category == hi.widget.TaskItem.CATEGORY_PROJECT || category == hi.widget.TaskItem.CATEGORY_PROJECT_ARCHIVE_MIRROR;
		},
		
		generateAllChildrenHTML : function (id, data_) {
			if (!data_ || !('items' in data_)) return;
			var data_ = data_.items;
			var dataIndexed  = {};
			
			for(var i in data_) {
				dataIndexed[data_[i].id] = data_[i];
			}
			
			data_ = dataIndexed;
			
			Project.data = dojo.mixin(Project.data, data_);
			Hi_Archive.data = dojo.mixin(Hi_Archive.data, data_);
			if (Hi_Archive.isProjectItemCategory(Project.data[id].category)){
				var childrenInserted = 0;
				for(var i in data_) {
					Hi_Archive.generateProjectChildrenHTML(id,data_[i]);
					childrenInserted++;
				}
				
				if (!childrenInserted) {
					Hi_Archive.generateProjectEmptyChildrenHTML(id);
				}
			}else{
				Hi_Archive.generateChildrenHtml(id);
			}
		},
		
		generateProjectEmptyChildrenHTML: function(project_id) {
			var taskNode = dojo.query('#project_tasks' + project_id + ' #tasks_completed');
			if (taskNode.length > 0){
				dojo._destroyElement(taskNode[0]);
			}
			
			var project = dojo.byId('project' + project_id);
			var project_tasks = dojo.query('div.tasks', project)[0];
			project_tasks.id = 'project_tasks' + project_id;
			var task_container = document.createElement('UL');
			task_container.className = 'task_ul';
			project_tasks = dojo.query('div.lb', project_tasks)[0];
			var ul = task_container.cloneNode(true);
			ul.id = 'tasks_completed';
			var project_tasks = project_tasks.insertBefore(ul, childNode(project_tasks, 0));
			project_tasks.innerHTML = '<li class="task_empty"><div>No items</div></li>';
		},
		
		generateProjectChildrenHTML: function(project_id, data){
			
			// delete task node if exists
			var taskNode = dojo.query('.task_ul #task_' + data.id);
			if(taskNode.length > 0){
				dojo._destroyElement(taskNode[0]);
			}
			
			var project = dojo.byId('project' + project_id);
			var project_tasks = dojo.query('div.tasks', project)[0];
			project_tasks.id = 'project_tasks' + data.parent;
			var task_container = document.createElement('UL');
			task_container.className = 'task_ul';
			project_tasks = dojo.query('div.lb', project_tasks)[0];
			var ul = task_container.cloneNode(true);
			ul.id = 'tasks_completed';
			var project_tasks = project_tasks.insertBefore(ul, childNode(project_tasks, 0));
			
			var is_completed = (data.completed == '1' || data.completed === 1);
			if (parseInt(data.recurring)) {
				var instance_date = data.next_event_date;
				if (instance_date && Project.isEventComplete(data.id, instance_date)) {
					is_completed = true;
				} else {
					is_completed = false;
				}
			}
			project_tasks_html = Hi_Archive.generateTaskHTML(data, is_completed, 4);
			Project.push(data);
			if (project_tasks != undefined) {
				(function fillHtml(project_tasks_html, project_tasks) {
					project_tasks.innerHTML = project_tasks_html;
				})(project_tasks_html, project_tasks);
			}
		},
		
		generateHTML : function (dataResponse) {
			
			try {
				var listArchive = dataResponse.getListArchive();
				var data = listArchive.items;
				var total_count = listArchive.size;
				var last_page = listArchive.is_last;
				var length = 0;

				for(var i in data) {
					if (data.hasOwnProperty(i)) length++;
				}

				if (total_count > Project.archive_max_items_per_page) {
					if (Hi_Archive.cur_offset == 0) {
						document.getElementById('headerNav1').style.visibility = 'hidden';
						document.getElementById('footerNav1Href').style.visibility = 'hidden';
					}
					else {
						document.getElementById('headerNav1').style.visibility = '';
						document.getElementById('footerNav1Href').style.visibility = '';
					}
					
					if (last_page) {
						document.getElementById('headerNav2').style.visibility = 'hidden';
						document.getElementById('footerNav2Href').style.visibility = 'hidden';
					}
					else {
						document.getElementById('headerNav2').style.visibility = '';
						document.getElementById('footerNav2Href').style.visibility = '';
					}
					document.getElementById('footerNav').style.display = '';
				} else {
					document.getElementById('headerNav1').style.visibility = 'hidden';
					document.getElementById('headerNav2').style.visibility = 'hidden';
					document.getElementById('footerNav').style.visibility = 'hidden';
				}		
				var ulMain = document.getElementById('no_project_tasks');
				var ulProjects = document.getElementById('projects');
				
				if (length === 0) {
					// No archived items found.
					ulMain.innerHTML = ulProjects.innerHTML = '';
					
					if (!Hi_Archive.cur_q) {
						// Do not show 'no tasks' message if search word specified.
						dojo.byId('no_archive_tasks').style.display = 'block';
					} else {	//Bug #5229
						dojo.byId('no_archive_tasks_search').style.display = 'block';
					}
					Hi_Archive.cur_offset = Hi_Archive.cur_offset - Project.archive_max_items_per_page;
					return; 
				} else {
					dojo.byId('no_archive_tasks').style.display = 'none';
					dojo.byId('no_archive_tasks_search').style.display = 'none';
					
					// Show search box if it is a first request being processed.
					if (!Hi_Archive.firstLoaded && Hi_Archive.getSearchWidget()) {
						dojo.removeClass(Hi_Archive.getSearchWidget().domNode, 'hidden');
					}
				}
				
				if (length <= 5) {
					document.getElementById('footerNav').style.display = 'none';
				}
				
				
				while (ulMain.childNodes.length >= 1) {
		      		ulMain.removeChild(ulMain.firstChild);
				}
				while (ulProjects.childNodes.length >= 1) {
		      		ulProjects.removeChild(ulProjects.firstChild);
				}
				var projectHTML = '';
				var dataIndexed = {};
				
				for (var i in data) {
					if (Hi_Archive.isProjectItemCategory(data[i].category)) {
						var html = fillStringTemplate(TEMPLATES.project_archive, {
						id: data[i].id, 
						group: 4, 
						name: data[i].title,
						project_description: data[i].message ? formatMessage(data[i].message) : '',
						borderColor: data[i].color_value,
						folderIconColor: colorLuminance(data[i].color_value, 0.4),
						gradientTopValue: colorLuminance(data[i].color_value, -0.1),
						gradientBottomValue: colorLuminance(data[i].color_value, -0.3),
						move_name: data[i].move_name,
						private_project_visible: data[i].private_project ? '' : 'style="display: none"',
						percentage: 10,
						submenusClass: data[i].category == hi.widget.TaskItem.CATEGORY_PROJECT_ARCHIVE_MIRROR ? 'hidden' : ''
						}, ['private_project_visible', 'project_description']);
						
						projectHTML += html;
						Project.push(data[i]);
					}
					
					dataIndexed[data[i].id] = data[i];
				}
				
				Hi_Archive.data = dojo.mixin(Hi_Archive.data, dataIndexed);
				
				
				if (projectHTML != '') {
					ulProjects.innerHTML = projectHTML; 
				}
				var project_tasks_html = '';
				var children_tasks = {};
				
				var tasks_html = '';
				
				for (i in data) {
					var parent = data[i].project || data[i].parent;
					if (!Hi_Archive.isProjectItemCategory(data[i].category) && parent && parent != '' && dataIndexed[parent] != undefined ) {
					//project as parent
						if (Hi_Archive.isProjectItemCategory(dataIndexed[parent].category)) {
							Hi_Archive.generateProjectChildrenHTML(parent, data[i]);
						} else {
							var parent = data[i].parent;
							var parent_parent = dataIndexed[parent].parent;
							
							if (!dataIndexed[parent].parent || (dataIndexed[parent_parent] && !Hi_Archive.isProjectItemCategory(dataIndexed[parent_parent].category) && dataIndexed[parent_parent].category != 5)) {
								children_tasks[parent] = true;
							}
						}
					}
					else if (!Hi_Archive.isProjectItemCategory(data[i].category)) {
						var is_completed = (data[i].completed == '1' || data[i].completed === 1);
						if (parseInt(data[i].recurring)) {
							var instance_date = data[i].next_event_date;
							if (instance_date && Project.isEventComplete(data[i].id, instance_date)) {
								is_completed = true;
							} else {
								is_completed = false;
							}
						}
						
						tasks_html += Hi_Archive.generateTaskHTML(data[i], is_completed, 4);
						Project.push(data[i]);
						
						(function fillHtml(tasks_html) {
							ulMain.innerHTML = tasks_html;
						})(tasks_html);
						
					}
				}
				
				for(var i in children_tasks) {
					Hi_Archive.generateChildrenHtml(i);
				}
				
				Hi_Notification.notificationDivId = 'notification_archive';
				Hi_Notification.borderClassName = 'border_archive';
				Hi_Archive.firstLoaded = true;
				Hi_Archive.extendTitles();
			}
			catch (el) {
				console.error(el);
			}
			
		},
		
		_get: function(id) {
			if (id in Hi_Archive.data) {
				return Hi_Archive.data[id];
			}
			return null;
		},
		
		/**
		 * Walks through all rendered items and if item has parent but parent was not rendered
		 * due to search query limitations item title for such items will be extended with titles of parent objects.
		 */
		extendTitles: function() {
			var lis = dojo.query('.task_li');
			for (var i = 0, len = lis.length; i < len; i++) {
				var li = lis[i];
				var id = parseIntRight(li.id);
				var item = Hi_Archive._get(id);
				if (item && item.parent && !dojo.byId('task_' + item.parent)) {
					var tdiv = dojo.query('h3 > div > div', li);
					if (tdiv.length) {
						tdiv[0].innerHTML = Hi_Archive.getComboTitle(id, true, '&nbsp;<span data-icon="" aria-hidden="true"></span>&nbsp;');
					}
				}
			}
		},
		
		createDiv : function (className) {
			var el = document.createElement('div');
			el.id = id;
			el.className = className;
			return el;
		},
		
		getNext: function () {
			Hi_Archive.cur_offset = Hi_Archive.cur_offset + Project.archive_max_items_per_page;
			Hi_Archive.getTaskList(Hi_Archive.cur_offset);
		},
		
		getPrev: function () {
			
			if (Hi_Archive.cur_offset > 0) {
				Hi_Archive.cur_offset = Hi_Archive.cur_offset - Project.archive_max_items_per_page;
			}
			
			Hi_Archive.getTaskList(Hi_Archive.cur_offset);
		},
		
		getSearchWidget: function() {
			return dijit.byId('archiveSearchWrapper');
		},
		
		/**
		 * Handles search text box changes.
		 */
		onSearch: function(q) {
			if (Hi_Archive.cur_q != q) {
				Hi_Archive.cur_offset = 0;
			}
			if (q && q.length > Hi_Archive.search_max_str) {
				q = q.substring(0, Hi_Archive.search_max_str);
				if (Hi_Archive.getSearchWidget()) {
					Hi_Archive.getSearchWidget().textbox.value = q;
				}
			}
			Hi_Archive.cur_q = q;
			
			if (Hi_Archive.cur_qtimeout) {
				// Clear previous timeout.
				clearTimeout(Hi_Archive.cur_qtimeout);
			}
			
			// Do not run request immediately. Setup timeout function for this.
			Hi_Archive.cur_qtimeout = setTimeout(function() {
				Hi_Archive.getTaskList(Hi_Archive.cur_offset);
			}, Hi_Archive.search_timeout);
		},
		
		getSearchWidget: function() {
			return dijit.byId('archiveSearchWrapper');
		},
		
		beforeRequest: function() {
			dojo.byId('archiveLoadingGif').style.display = 'block';
			dojo.addClass('archiveMainBlock', 'hidden');
			if (Hi_Archive.getSearchWidget()) {
				Hi_Archive.getSearchWidget().set('disabled', true);
			}
		},
		
		afterRequest: function() {
			dojo.byId('archiveLoadingGif').style.display = 'none';
			dojo.removeClass('archiveMainBlock', 'hidden');
			if (Hi_Archive.getSearchWidget()) {
				Hi_Archive.getSearchWidget().set('disabled', false);
			}
		},
		
		getTaskList : function (offset) {
			offset = offset && offset >= 0 ? offset : 0;
			var params = {depth: Project.archive_max_items_per_page, offset: offset, realm: 'archived', parent: '', orderby: '-timeLastUpdate'};
			delete params.parent; // #2579: return all items.
			if (Hi_Archive.cur_q) {
				params['q'] = Hi_Archive.cur_q;
			}
			if(Project.businessId){
				params.with_shared = 1;
			}
			this.beforeRequest();
			var self = this;
			Project.ajax('tlistarchive', params, Hi_Archive.generateHTML).then(function() {
				self.afterRequest();
			});
			if (!Project.alertElement) {
				createAlertElementWidget();
			}
		},
		
		getChildrenList : function (id, fileChildOnly) {
			var params = {parent: id, realm: 'archived'};
			if(Project.businessId){
				params.with_shared = 1;
			}
			Project.ajax('tlistarchive', params, function (dataResponse) {
				if (!fileChildOnly) {
					Hi_Archive.generateAllChildrenHTML(id, {items:dataResponse.getData()});
				} else {
					//FileList.loadReceive(dataResponse.getData());
					Hi_Archive.loadFileList(id, dataResponse.getData());
				}
				
				if (!Project.hasNonFileChildren(id) || !dataResponse.getData().length) {
					var li = dojo.byId('task_' + id);
					if (li) {
						dojo.removeClass(li, 'has-children');
					}
				}
			});
		},
		
		loadFileList: function(parentId, data) {
			dojo.forEach(data, function(item) {
				if (item.category == 5 && item.parent == parentId) {
					var formatFileSize = function (fileSize) {
						if (!fileSize) return '';
						
						fileSize = fileSize / 1024;
						if (fileSize > 1024) {
							return Math.round(fileSize / 1024) + 'MB';
						} else {
							return Math.ceil(fileSize) + 'KB';
						}
					};
					
					var _getNodeList = function () {
						var nodeContainer = dojo.query('.file-upload', dojo.byId('task_' + parentId));
						nodeContainer = nodeContainer && nodeContainer.length ? nodeContainer[0] : null;
						var nodeList = nodeContainer.getElementsByTagName('UL');
						if (nodeList.length) {	
							return nodeList[0];
						} else {
							nodeList = document.createElement('UL');
							nodeList.onclick = FileList.handleClick;
							nodeContainer.appendChild(nodeList);
							return nodeList;
						}
					};
					
					var _create = function(data) {
						var template_data = {
							'id': 'file_' + data.id,
							'filename': data.title,
							'fileSize': formatFileSize(data.file_size)
						};
						
						var node = document.createElement('UL');
						node.innerHTML = fillStringTemplate(TEMPLATES.file, template_data);
						
						return dojo.dom.getChildElements(node)[0];
					};
					
					var container = _getNodeList();
					var node = _create(item);
					
					if (node) {
						var items = dojo.dom.getChildElements(container);
						
						if (items.length) {
							container = items[0];
							
							for(var i=0,ii=items.length; i<ii; i++) {
								if (!dojo.hasClass(items[i], 'file')) {
									container = items[i]; break;
								}
							}
						}
						
						if (container.tagName == 'LI') {
							dojo.place(node, container, 'before');
						} else {
							container.appendChild(node);
						}
					}
				}
			});
		},
		
		animating: false,
		
		taskRestore: function(id) {
			/* #2465 - moved to callback afterRestore() 
			if (Project.fadeOutTask(id, 'restore', function () {
				var options = {item_id : id}; 
				Project.ajax('trestore', {id : id}, Hi_Archive.afterRestore, Hi_Archive.errorRestore,options);
			}));
			*/
			
			if (Hi_Archive.animating) {
				return;
			}
			var options = {item_id : id};
			Project.ajax('trestore', {id : id}, Hi_Archive.afterRestore, Hi_Archive.errorRestore, options);
		},
		taskCopyRestore : function (id) {
			/* #2465 - moved to callback afterCopyRestore()
			Project.fadeOutTask(id, 'restore_copy', function () {
				
				//Show message if there is a children which is file
				if (Hi_Archive.hasTaskChildren(id)) {
					Project.alertElement.showItem(0);
				}
				
				Project.ajax('tcopyrestore', {id : id},Hi_Archive.afterCopyRestore, Hi_Archive.errorRestore);
				
			});
			*/
			var options = {item_id : id}; 
			Project.ajax('tcopyrestore', {id : id},Hi_Archive.afterCopyRestore, Hi_Archive.errorRestore,options);
		},
		
		hasTaskChildren: function (id) {
			var children = Hi_Archive.data;
			for(var i in children) {
				if (children[i].parent == id || children[i].project == id) {
					if (children[i].category == 5) {
						return true;
					} else if (Hi_Archive.hasTaskChildren(children[i].id)) {
						return true;
					}
				}
			}
			
			return false;
		},
		
		/**
		 * #2579: Collects the full title for item.
		 * Returns array of all titles including parent titles and current item title.
		 * Optionalyy title values can be html sanitized and joined using specified string.
		 */
		getComboTitle: function(id, escape, join) {
			var titleArr = [];
			var item = this._get(id);
			if (item && !Hi_Archive.isProjectItemCategory(item.category)) {
				var title = escape ? htmlspecialchars(item.title) : item.title;
				titleArr.push(title);
				
				if (item.parent) {
					titleArr = titleArr.concat(this.getComboTitle(item.parent, escape));
				}
				if (item.parentTitle) {
					titleArr = titleArr.concat(escape ? htmlspecialchars(item.parentTitle) : item.parentTitle);
				}
			}
			
			titleArr = titleArr.reverse();
			return join ? titleArr.join(join) : titleArr;
		},
		
		generateTaskHTML : function (pdata, is_completed, group) {
			var id = pdata.id;
			
			var template_data = {
				id: 'task_' + id,
				color_class: '',
				shared_class: '',
				owned_class: '',
				admin_class: '',
				children_class: '',
				completed_class: '',
				completed_checked: '',
				type_class: '',
				task_title: '',
				person_class: '',
				person_title: '',
				person_name: '',
				person_name_class: 'hidden',
				project_title: '',
				project_name: '',
				project_name_class: 'hidden',
				drag_visible_class: '',
				starred_class: '',
				eventdate: (pdata.next_event_date ? pdata.next_event_date : ''),
				business_class: (Project.businessId ? 'business' : ''),
				date_title: getTaskDateTitle(pdata.id, new Date(), pdata),
				is_completed: pdata.complete,
				priority: 0,
				priority_name: '',
				parent_task_title_visible_class: 'hidden',
				parent_task_title: ''
			};
			
			var priority = pdata.priority || 20000;
			var priority_name = 'medium';
			if (priority >= 30000) {
				priority_name = 'high';
			} else if (priority < 20000) {
				priority_name = 'low';
			}
			
			template_data.priority = priority;
			template_data.priority_name = priority_name;
			
			// Completed task group
			if (is_completed) {
				//template_data.completed_class = 'completed';
				template_data.completed_checked = 'checked="checked"';
			}
			
			if (pdata.user_id == Project.user_id) {
				template_data.owned_class = 'owned';
			}
			
			if (parseInt(pdata.starred)) {
				template_data.starred_class = 'star-selected';
			}
			// #3676:
			template_data.starred_class = '';
			
			if (Project.isBusinessAdministrator()) {
				template_data.admin_class = 'admin';
			}
			
			if (pdata.children && Project.hasNonFileChildren(pdata.id, true) != 0) {
				template_data.children_class = 'has-children';
			}
		
			if (pdata.shared == '1') {
				template_data.shared_class = 'shared';
			}
			
			if (parseInt(pdata.color)) {
				template_data.color_class = 'color' + pdata.color;
			}
			
			switch (parseInt(pdata.category)) {
				case 1:
					template_data.type_class = 'tasks';
					break;
				case 2: case 3:
					template_data.type_class = 'event';
					break;
				case 5:
					template_data.type_class = 'file';
					break;
				case 4:
					template_data.type_class = 'note';
					break;
			}
			
			template_data.task_title = htmlspecialchars(pdata.title);
			
			if (group == 2) {
				template_data.drag_visible_class = 'hidden';
			}
			
			if (pdata.assign_user && group != 5) {
				if (pdata.assign_user == Project.user_id && pdata.assign_user != pdata.user_id) {
					var pClass = 'from';
					var pId = pdata.user_id;
					var pTitle = hi.i18n.getLocalization('center.item_received_from') + ' ';
				} else {
					var pClass = 'to';
					var pId = pdata.assign_user;
					var pTitle = hi.i18n.getLocalization('center.item_assigned_to') + ' ';
				}
				var pName = Project.getFriendName(pId);
				if (pName) {
					template_data.person_class = pClass;
					template_data.person_title = pTitle + pName;
					template_data.person_name = pName + ' ';
					template_data.person_name_class = '';
				}
			}
			if (pdata.parent && group != 4) {
				var pName = Project.get(pdata.parent, 'title');
				if (pName) {
					template_data.project_title = pName;
					template_data.project_name = ' ' + pName;
					template_data.project_name_class = '';
				}
			}
			
			return fillStringTemplate(TEMPLATES.task_archive, template_data, ["completed_checked", "task_title", "business_visible"]);
		},
		
		afterRestore: function(dataResponse, options) {
			if (dataResponse.isStatusOk()) {
				var id = options.item_id;
				Hi_Archive.animating = true;
				
				/* #2465 */
				Project.fadeOutTask(id, 'restore', function () {
					var el = document.getElementById('task_' + id);
					el.parentNode.removeChild(el);
					delete Project.data[id];
					delete Hi_Archive.data[id];
					Hi_Archive.afterEachChange();
					
					Hi_Notification.putMessageInQue('task_restore');
					Hi_Archive.animating = false;
				});
			} else {
				alert(dataResponse.getErrorMessage());
			}
		}, 
		
		errorRestore: function(data, options, apiResp) {
			alert(apiResp.getErrorMessage());
		}, 
		
		afterCopyRestore: function(dataResponse,options) {
			if (dataResponse.isStatusOk()) {
				var id = options.item_id;
				/* #2465 */
				Project.fadeOutTask(id, 'restore_copy', function () {
					
					//Show message if there is a children which is file
					if (Hi_Archive.hasTaskChildren(id)) {
						Project.alertElement.showItem(0);
					}
					Hi_Notification.putMessageInQue('task_copy_and_restore');
				});
				
			} else {
				alert(dataResponse.getErrorMessage());
			}
		},
		
		getTask: function(id) {
			if (id in Project.data) {
				return Project.data[id];
			}
			return null;
		},
		
		isOwner: function(taskId) {
			var task = this.getTask(taskId);
			return task && task.user_id == Project.user_id;
		},
		
		deleteTask: function(id, options) {
			if(!options.target) return;
			var task = this.getTask(id);
			
			if (task && task.permission >= Hi_Permissions.DELETE) {
				var task = this.getTask(id);
				var category = task && task.category;
				var message = hi.i18n.getLocalization('tooltip.delete_message_task_' + category);
				var btn = options.target;

				Project.showDeleteConfirmation(message, btn, function () {
					Project.fadeOutTask(id, 'remove', function () {
						Hi_Archive.remove(id, options);
					});
				});
			} else {
				alert('You do not have permissions to delete this task.');
			}
		},
		
		deleteProject: function(id) {
			var options = {item_id : id};
			var params = {id: id, cascade: 1}; 
			Project.ajax('itemdelete', params, Hi_Archive.afterDeleteProject, Hi_Archive.errorDelete,options);
		},
		
		
		restoreCopyProject: function(id) {
			//Show message if there is a children which is file
			if (Hi_Archive.hasTaskChildren(id)) {
				Project.alertElement.showItem(0);
			}
			
			Project.ajax('pcopyrestore', {id : id}, Hi_Archive.afterRestoreCopyProject, Hi_Archive.errorRestoreCopyProject);
		},
		
		afterRestoreCopyProject : function (dataResponse) {
			if (dataResponse.isStatusOk()) {
				Hi_Notification.putMessageInQue('project_copy_restore');
				Hi_Archive.afterEachChange();
			} else {
				alert(dataResponse.getErrorMessage());
			}
		},
		
		errorRestoreCopyProject : function (data, options, apiResp) {
			alert(apiResp.getErrorMessage());
		},
		
		restoreProject: function(id) {
			var options = {item_id : id}; 
			Project.ajax('prestore', {id : id}, Hi_Archive.afterRestoreProject, Hi_Archive.errorRestoreProject,options);
		},
		
		afterRestoreProject : function (dataResponse,options) {
			if (dataResponse.isStatusOk()) {
				Hi_Notification.putMessageInQue('project_restore');
				var id = options.item_id;
				var el = document.getElementById('project' + id);
				el.parentNode.removeChild(el);
				Hi_Archive.afterEachChange();
			} else {
				alert(dataResponse.getErrorMessage());
			}
		},
		
		errorRestoreProject : function(data, options, apiResp) {
			alert(apiResp.getErrorMessage());
		},
		
		
		
		afterDeleteProject : function (dataResponse,options) {
			if(!dataResponse.isStatusOk()){
				false;
			}
			var id = options.item_id;
			var el = document.getElementById('project' + id);
			if (el) {
				el.parentNode.removeChild(el);
				Hi_Archive.afterEachChange();
			}
			Hi_Notification.putMessageInQue('pdelete');
		},
		
		afterDelete : function (dataResponse,options) {
			if(!dataResponse.isStatusOk()){
				false;
			}
			var id = options.item_id;
			var el = document.getElementById('task_' + id);
			if (el) {
				el.parentNode.removeChild(el);
				Hi_Archive.afterEachChange();
			}
		},
		
		errorDelete : function (data, options, apiResp) {
			alert(apiResp.getErrorMessage());
		},
		
		remove: function(id, options) {
			if(!options.item_id){
				options.item_id = id;
			}
			Project.removeItem[id] = true;
			Project.ajax('itemdelete', {id:id}, Hi_Archive.afterDelete, Hi_Archive.errorDelete, options);
			return;	
			var func = function(){
				Project.removeElement(id);
			};
		},
		
		history: function (el) {
		},
		
		afterEachChange: function () {
			var ulMain = document.getElementById('no_project_tasks');
			var ulProjects = document.getElementById('projects');
			if (!ulMain.innerHTML.trim() && !ulProjects.innerHTML.trim()) {
				setTimeout(function () {
					Hi_Archive.cur_offset = Hi_Archive.cur_offset - Project.archive_max_items_per_page;
					if(Hi_Archive.cur_offset < 0) Hi_Archive.cur_offset = 0;
					Hi_Archive.getTaskList(Hi_Archive.cur_offset);
				}, 70);
			}
			else {
				Hi_Archive.getTaskList(Hi_Archive.cur_offset);
			}
			
		},
		
		/* #3024 - simple toggle of expanded attrubute */
		toggleChildren: function (id, expand, options) {
			var li = dojo.byId('task_' + id);
			if (li) {
				if (dojo.hasClass(li,'expanded')) {
					dojo.removeClass(li,'expanded');
				} else {
					dojo.addClass(li,'expanded');
				}
				
				// #3191: in case if children was not preloaded.
				if (dojo.query('ul.children li', li).length == 0) {
					var after = function () {
						if (options && options.after && typeof options.after == 'function') {
							options.after({'id': id});
						}
					};
					
					if (id in Hi_Archive.expandedChildren && Hi_Archive.expandedChildren[id] == 1) {
						//Hide
						var li = dojo.byId('task_' + id);
						if (!li) return;
						
						Hi_Archive.expandedChildren[id] = 0;
						dojo.removeClass(li, 'expanded');
						after();
					} else {
						//Load
						Hi_Archive.getChildrenList(id);
						Hi_Archive.expandedChildren[id] = 1;
						after();
					}
				}
			}
		}
	
	};
	
	Project.hasNonFileChildren = function(id, sameRealm) {
		var data = Hi_Archive.data;
		
		if (!(id in data)) return 0;
		
		if (data[id].children) {
			if (!sameRealm) {
				return 1;
			}
		}
		
		return 0;
	};

	return dojo.global.Hi_Archive;

});