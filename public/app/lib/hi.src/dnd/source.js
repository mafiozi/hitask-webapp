/**
 * Defines several objects for Dnd operations, what the behavior is when dragging, dropping, moving the cursor, etc.
 * Along with avatar.js, specifies some of the basic behavior for displaying tooltips while dragging
 */
//dojo.provide("hi.dnd.source");
define(['hi/widget/task/item_DOM'], function(item_DOM) {
	
	dojo.declare("dojo.dnd.HiDragSource", [dojo.dnd.Source], {
		
		type: "dragged_task",
		
		accept: [],
		
		dragHandleClass: "heading",
		
		withHandles: true,
		
		avatarClass: dojo.dnd.HiDragAvatar,
		
		delay: 5,
		
		mouseX: 0,
		
		isOut: false,
		
		isInitialized: false,
		
		permissionsAllowed: [Hi_Permissions.MODIFY, Hi_Permissions.EVERYTHING],

		action: 'modify',

		sorting: 0,
		/* #2535 via https://support.sitepen.com/issues/22098 */
		onSelectStart:function(e) {return;},
		onDocumentMouseMove: function (e) {
			if (!dojo.dnd.HiSortDragExtra.allowSort) return;
			
			this.mouseX = e.pageX;
			
			if (!this.isInitialized) {
				this.isInitialized = true;
			}
			var m, c;
			if (!this.isOut && (e.pageX < dojo.dnd.HiSortDragExtra.constraints[0] || e.pageX > dojo.dnd.HiSortDragExtra.constraints[1])) {
				this.isOut = true;
				//JDT var m = dojo.dnd.Manager();
				m = dojo.dnd.Manager.manager();
				if (this.dragNode) {
					dojo.dnd.HiSortDragExtra.resetDropIndicator(this.dragNode);
				}
				
				if (m && m.avatar && m.avatar.node) {
					c = dojo.byId(Project.js_containers.tasksList.id);
					dojo.removeClass(c, 'sortingInProgress');
				}
			} else if (this.isOut && (e.pageX >= dojo.dnd.HiSortDragExtra.constraints[0] && e.pageX <= dojo.dnd.HiSortDragExtra.constraints[1])) {
				this.isOut = false;
				//JDTvar m = dojo.dnd.Manager();
				m = dojo.dnd.Manager.manager();
				if (m && m.avatar && m.avatar.node) {
					c = dojo.byId(Project.js_containers.tasksList.id);
					dojo.addClass(c, 'sortingInProgress');
				}
			}
		},
		
		onDndStart: function(source, nodes, copy) {
			/*
			 * Summary
			 * Start Dnd operation
			 * Set up HiSortDragExtra if the Dnd operation is allowed and sorting needs to be done
			 */
			//if (!source.getItem(nodes[0].id)) return;
			dojo.dnd.HiDragSource.superclass.onDndStart.call(this, source, nodes, copy);
			// var m = dojo.dnd.Manager.manager();
			// m.canDrop(false);

			if (source == this) {
				this.dragNode = nodes[0];
				//this.dragNode.dragId = this.dragNode.dragId = this.dragNode.taskId;
				this.dragNode.dragId = dojo.dom.getTaskId(this.dragNode);
				this.dragNode.taskId = this.dragNode.dragId;
				Project.dragging = true;
				
				//Extra
				dojo.dnd.HiSortDragExtra.onDndStart.call(this, source, nodes, copy);
				
				//Mouse tracking
				var pos = dojo.position(this.dragNode.parentNode);
				pos = ~~pos.x;
				this.isOut = false;
				this.isInitialized = false;
				dojo.dnd.HiSortDragExtra.constraints = [pos - 20, pos + 20 + this.dragNode.parentNode.offsetWidth];
				dojo.dnd.HiSortDragExtra.connect = dojo.connect(document, 'mousemove', this, this.onDocumentMouseMove);
				
				//Sorting
				dojo.dnd.HiSortDragExtra.allowSort = true;
				/*#2432 partial - remove this logic */
				var grouping = Hi_Preferences.get('currentTab', 'my', 'string');
				if ( grouping === 'date'/* || grouping === 0*/) {
					//Sub tasks are allowed to be sorted in "by Date" group
					if (!Project.isTaskChild(this.dragNode.dragId)) {
						dojo.dnd.HiSortDragExtra.allowSort = false;
					}
				}
				/*#2432 partial*/
				if (dojo.dnd.HiSortDragExtra.allowSort) {
					var c = dojo.byId(Project.js_containers.tasksList.id);
					dojo.addClass(c, 'sortingInProgress');
					Project.sorting = true;
				}
			}
		},
		
		onFinalDrop: function (source, nodes, copy) {
			/*
			 * Summary:
			 * Save the modified item after the drop
			 * This method figures out what type of drop was carried out and notifies dojo.global.updateTask, which does
			 * the save
			 */
			var cancelComplete = false;
			var self = dojo.dnd.HiSortDragExtra;
			var sortingByPriority = (Project.getSorting() == Project.ORDER_PRIORITY);
			var fileAttach = false, cancel, c;
			
			if (self.connect) {
				dojo.disconnect(self.connect);
				self.connect = null;
			}
			// Bug #5625*
			// Don't tend to trigger onFinalDrop when groupChange
			if (Project.sorting && !self.waitingConfirmation && HiSortDragSource_dragNode &&
					!dojo.dnd.HiSortDragExtra.groupChange ||
					// https://red.hitask.net/issues/5555#note-8
					// "addSub" action has high priority
					dojo.dnd.HiSortDragExtra.actionType === 'addSub') {
				var dragId = parseInt(HiSortDragSource_dragNode.dragId),
					task = dragId && hi.items.manager.get(dragId);
				
				/* #3197 - see /items/dnd.js:dojo.global.dndHandleProjectDrop */
				//Hide avatar and hide mark
				//JDT var m = dojo.dnd.Manager();
				var m = dojo.dnd.Manager.manager();
				m.avatar.hide();
				
				if (dojo.dnd.HiSortDragExtra.drop_indicator) {
					dojo.dnd.HiSortDragExtra.drop_indicator.style.display = 'none';
				}
				
				//Move task before or after drop target
				if (self.drop_target && !item_DOM.hasChildrenOpenForEdit(dragId)) {
					if (!dojo.global.isDndValid.apply(this, []) /* #5015 */) {
						dojo.dnd.HiSortDragExtra.resetDropIndicator();
						return;
					}
					
					var target = self.drop_target,
						targetId = dojo.dom.getTaskId(self.drop_target),//self.drop_target.taskId,
						targetTaskItem = hi.items.manager.getItemByNode(target,targetId);
						//targetTaskItem = hi.items.manager._items[targetId].list.taskItem;
					
					var parentOld = Project.getParentTask(dragId);
					var parentNew = null;
					if (self.drop_type == 'line') {
						parentNew = Project.getParentTask(targetId);
					} else {
						parentNew = targetId;
					}

					cancel = false;
					var parentSave = {};
					var saveObj = {};
					var parentNewObj = hi.items.manager.get(parentNew) || {};
					if (parentNew != parentOld && (parentNewObj || parentNew == 0)) {
						if (parentNewObj.category == hi.widget.TaskItem.CATEGORY_FILE) {
							Project.alertElement.showItem(14, {task_alert: 'File can not be parent!'});
							cancel = true;
							this.dropValue = false;
						} else {
							parentSave['parent'] = parentNew;
							cancelComplete = true;
							
							//If item is file, then show 'Attach to item message'
							if (hi.items.manager.get(dragId, "category") == 5) {
								msg = hi.i18n.getLocalization('project.move_attach_confirmation');
								fileAttach = true;
							} else {
								msg = hi.i18n.getLocalization('project.move_task_confirmation');
								var draggingObj = hi.items.manager.get(dragId);
								if (draggingObj && parentNewObj && draggingObj.shared && draggingObj.user_id != Project.user_id && !parentNewObj.shared) {
									// #3258: trying to make a shared item owned by another user child of private item owned by current user.
									// This will make first item private and current user will no longer see it.
									msg += ' "' + draggingObj.title + '" is currently shared and owned by another user. It will become private and will not be visible for you unless it assigned to you! Are you sure you want to proceed?';
								}
							}
							
							if (!parentNew || Project.isProject(parentNew)) {
								msg = hi.i18n.getLocalization('project.move_out_confirmation');
							}
							
							//Disable sorting until confirmation is closed, in Safari browser not blocked
							if (dojo.isSafari) {
								self.waitingConfirmation = true;
								oldAllowSort = self.allowSort;
								self.allowSort = false;
							}
								
							if (!confirm(msg)) {
								cancel = true;
								this.dropValue = false;
								// #3431: if "undo" callback is provided then call it.
								if (typeof(m.avatar.onHiDndCancelled) == 'function') {
									m.avatar.onHiDndCancelled();
								}
							} else if (Hi_Preferences.get('currentTab', 'my', 'string') == 'team' && typeof(m.avatar.onHiDndCancelled) == 'function') {
								// #3431: For team tab do not auto-assign item to user if it is being DnDed directly on other item.
								m.avatar.onHiDndCancelled();
							}
							
							//Re-enable sorting
							if (dojo.isSafari) {
								self.waitingConfirmation = false;
								self.allowSort = oldAllowSort;
							}
							
							HiTaskCalendar.closeTaskEditTooltipIfOpen();
						}
					} else if (!sortingByPriority) {
						self.resetDropIndicator();
						Project.sorting = false;
						
						msg = hi.i18n.getLocalization('project.move_invalid_priority');
						// Bug #5625
						// When change group, stop the alert since it's not friendly to user,
						// although there are 2 dnd actions done at the same time.
						if (!dojo.dnd.HiSortDragExtra.groupChange) {
							alert(msg);
						}
						
						c = dojo.byId(Project.js_containers['tasksList'].id);
						dojo.removeClass(c, 'sortingInProgress');
						
						self.resetDropIndicator();
						Project.sorting = false;
						
						return;
					}

					if (!cancel && target && target.parentNode) {
						//console.log("INSIDE");
						if (self.drop_type == 'line') {
							/* #2311 set priority according to position item is dropped
							 *  - if between, then new priority is average
							 *  - if at top, new priority is highest in list + 10
							 *  - if at bottom, new priority is lowest in list - 10
							 *  
							 *  We also cannot use insertBefore/insertAfter; use setSave instead, which should
							 *  notify the TaskList of new item, and it will sort it correctly
							 */
							var avg;
							if (self.drop_place == 'before') {
								//Set initial priority, it will be updated later
								//hi.items.manager.set(dragId, {"priority": 30000});
								var before = prevElement(target);
								
								if (!before)
								{
									//Placed at top of TaskList
									//avg = dijit.byNode(target).priority+10;
									avg = targetTaskItem.priority + 10;
									
									// #3162
									if (dojo.dnd.HiSortDragExtra.last_priority > avg) {
										avg = dojo.dnd.HiSortDragExtra.last_priority;
									}
								} else if (before != hi.items.manager.item(dragId))
								{
									var beforeTaskItem = hi.items.manager.getItemByNode(before,dojo.dom.getTaskId(before));//hi.items.manager._items[dojo.dom.getTaskId(before)].list.taskItem;
									//Placed between two TaskItems
									//avg = Math.round( (dijit.byNode(before).priority+dijit.byNode(target).priority)/2 );
									avg = Math.round( (beforeTaskItem.priority+targetTaskItem.priority)/2 );
								} 
								//hi.items.manager.setSave(dragId,{priority:avg});
								saveObj['priority']=avg;
								if (parentSave && 'parent' in parentSave)
									hi.items.manager.setSave(dragId,parentSave).then(function() {updateTask(dragId,hi.items.manager.item(targetId),saveObj);});
								else updateTask(dragId,hi.items.manager.item(targetId),saveObj);
							} else {
								//Set initial priority, it will be updated later
								//hi.items.manager.set(dragId, {"priority": 19999});
								//myTest = insertAfter(HiSortDragSource_dragNode, target);
								var after = nextElement(target);
								if (!after||!dojo.hasClass(after,'task_li')|| /*dijit.byNode(after).taskId*/dojo.dom.getTaskId(after)==dragId)
								{
									//Placed at bottom of TaskList
									//avg = dijit.byNode(target).priority - 10;
									avg = targetTaskItem.priority - 10;
									// #3162
									if (dojo.dnd.HiSortDragExtra.last_priority < avg) {
										avg = dojo.dnd.HiSortDragExtra.last_priority;
									}
									
									if (avg < 0) {
										avg = 0;
									}
								} else if (after != dojo.byId('itemNode_'+hi.items.manager.item(dragId))/*hi.items.manager.item(dragId)*/)
								{
									var afterTaskItem =  hi.items.manager.getItemByNode(after,dojo.dom.getTaskId(after));//hi.items.manager._items[dojo.dom.getTaskId(after)].list.taskItem;
									//avg = Math.round( (dijit.byNode(after).priority+dijit.byNode(target).priority)/2 );
									avg = Math.round( (afterTaskItem.priority+targetTaskItem.priority)/2 );
								}
								//hi.items.manager.setSave(dragId,{priority:avg});
								saveObj['priority'] = avg;
								if (parentSave && 'parent' in parentSave) {
									hi.items.manager.setSave(dragId,parentSave).then(function() {updateTask(dragId,hi.items.manager.item(targetId),saveObj);});
								} else {
									updateTask(dragId,hi.items.manager.item(targetId), saveObj);
								}

							}
							cancel = true;
							
						} else if (targetId) {
							targetTaskItem = hi.items.manager.item(targetId);
							if (targetTaskItem) {
								saveObj = {parent:targetId, shared: targetTaskItem.taskItem.shared}; // #2458: need to copy visibility too.
								/*
								hi.items.manager.setSave(dragId,{parent:targetId}).then(function() {
									console.log("THEN",[dragId,hi.items.manager.get(dragId),hi.items.manager.item(dragId)]);
									updateTask(hi.items.manager.item(dragId),hi.items.manager.item(targetId));
								});
								*/
								updateTask(dragId,hi.items.manager.item(targetId),saveObj);
								cancel = true;
								/*
								var ul = dijit.byNode(target).container.domNode,
								var newParentId = ul.getAttribute("data-task-id"),
								newListId   = ul.getAttribute("data-list-id");
							
								if (newParentId === null && newListId === "0") {
									newParentId = 0;
								}
								if (newParentId !== null && newParentId != parentId) {
									hi.items.manager.setSave(taskId, {"parent": newParentId || 0});
									
									//return;
								}	
								*/
								//dojo.place(HiSortDragSource_dragNode, targetTaskItem.childrenNode);
							}
						}
					}
				} else {
					cancel = true;
				}
				
				c = dojo.byId(Project.js_containers['tasksList'].id);
				dojo.removeClass(c, 'sortingInProgress');
				
				self.resetDropIndicator();
				
				if (!cancel) {
					var prevSibl = prevElement(HiSortDragSource_dragNode);
					updateTask(HiSortDragSource_dragNode,null,null,dragId);
				}
				
				Project.sorting = false;
				/* #2366 */
				dojo.dnd.HiSortDragExtra.groupChange = null;
			}
			self.resetDropIndicator();
		},
		
		onDndCancel: function () {
			dojo.dnd.HiDragSource.superclass.onDndCancel.call(this);
			//Extra
			dojo.dnd.HiSortDragExtra.onDndCancel.call(this);
			
			this.onFinalDrop();
		},
		
		onDndDrop: function () {
			console.log('in on dnd drop');
			dojo.dnd.HiDragSource.superclass.onDndDrop.call(this);
			//Extra
			//console.log('extra dndDrop');
			dojo.dnd.HiSortDragExtra.onDndDrop.call(this);
			
			this.onFinalDrop();
		},
		
		onMouseMove: function (e) {
			// console.log('source move');
			dojo.dnd.HiDragSource.superclass.onMouseMove.call(this, e);
			//Extra
			dojo.dnd.HiSortDragExtra.onMouseMove.call(this, e);
		},
		
		onMouseOut: function (e) {
			dojo.dnd.HiDragSource.superclass.onMouseOut.call(this, e);
			
			//If user drags item out of project, then unmark item
			var rel = e.relatedTarget;
			
			if (Project.dragging && dojo.dnd.HiSortDragExtra.drop_target) {
				if (!dojo.hasClass(rel, 'taskDropIndicator') && !dojo.isDescendant(rel, this.node)) {
					dojo.dnd.HiSortDragExtra.markTarget(null);
				}
			}
		},
		
		/**
		 * Overwrite to allow more deeply nested dojoDndItem's to be used
		 * @param {Event} e
		 */	
		_getChildByEvent: function(e){
			// summary: gets a child, which is under the mouse at the moment, or null
			// e: Event: a mouse event
			var node = e.target;
			var child = null;
			
			if(node){
				for(var parent = node.parentNode; parent; node = parent, parent = node.parentNode){
					if (dojo.hasClass(node, "dojoDndItem") && !child) child = node;
					if(parent == this.parent && child){ return child; }
				}
			}
			return null;
		},
		
		_legalMouseDown: function(e){
			//Right mouse button must not initiate drag & drop
			if (e.button == 2) {
				return false;
			}
			
			if(!this.withHandles) {
				return true;
			}
			
			for(var _3e=e.target;_3e&&_3e!==this.node;_3e=_3e.parentNode){
				if (this.dragHandleClass) {
					if(dojo.hasClass(_3e,this.dragHandleClass)){
						return true;
					}
					if (dojo.hasClass(_3e, "dojoDndHandle")){
						return true;
					}
				} else if(dojo.hasClass(_3e,"dojoDndHandle")){
					return true;
				}
				if(dojo.hasClass(_3e,"dojoDndItem")){
					break;
				}
			}
			return false;
		}
	
	});
	
	return dojo.dnd.HiDragSource;

});