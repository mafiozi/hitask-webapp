define([
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dojo/date/locale',
	'dojo/aspect',
	"dojox/calendar/Calendar",
	"dojox/calendar/time",
	"dojox/calendar/MatrixView"
], function(declare, lang, locale, aspect, Calendar, timeUtil, MatrixView) {
	lang.extend(MatrixView, {
		_sortItemsFunction: function(a, b){
			// tags:
			//		private
			if(this.roundToDay){
				a = a.allDay === true ? this._roundItemToDay(a) : a;
				b = b.allDay === true ? this._roundItemToDay(b) : b;
			}
			var res = this.dateModule.compare(a.startTime, b.startTime);
			if(res == 0){
				res = -1 * this.dateModule.compare(a.endTime, b.endTime);
			}
			return res;
		}
	});

	var setPrintInfo = function() {
		var calendarEvents = dojo.query('.dojoxCalendarEvent'), parent, left, width;

		if (calendarEvents.length) {
			calendarEvents.forEach(function(node) {
				if (!node.clientHeight || !node.clientWidth) return;
				parent = node.parentNode;
				left = parseInt(node.style.left, 10);
				left = left * 100 / parent.clientWidth;
				left = left.toFixed(4);

				width = node.clientWidth;
				width = width * 100 / parent.clientWidth;
				width = width.toFixed(4);

				// console.log('left', left);
				node._printLeft = left;
				node._printWidth = width;
			});
		}
	};

	var hiCalendar = declare("hi.widget.Calendar", [Calendar], {
		// Fire the change event for every text change
		startTimeAttr: 'startDate',

		endTimeAttr: 'endDate',

		dateInterval: 'week',

		summaryAttr: "title",

		allDayAttr: 'isAllDay',

		animateRange: false,	//remove Fade effect when calendar is changing view

		columnViewProps: {minHours:0, maxHours:24},

		scrollable: false,

		showTodayButton: true,

		showDayButton: false,

		showFourDaysButton: false,

		showWeekButton: true,

		showMonthButton: true,

		_matrixExpandedCell: null,

		cssClassFunc: function(item){
			var cssClass = 'calendarEventColor' + item.color,
				completed = item.completed;
			if(completed){
				cssClass += ' calendarEventCompleted';
			}
			if (item.category === hi.widget.TaskItem.CATEGORY_TASK && item.isAllDay && item.due_date) {
				// #5735: task item with due date is all day.
				cssClass += ' noStartTimeLabel';
			}

			return cssClass;
		},

		formatItemTimeFunc: function(d, rd, view, item){
			var tf = Hi_Calendar.time_format;
			var id = item.id.split('_')[0];
			var hiItem = him.get(id);

			if (hiItem) {
				if (!hiItem.start_date && hiItem.end_date) {
					d = item.endTime;
				}
			}

			return rd.dateLocaleModule.format(d, {
				selector: 'time',
				timePattern: tf === '12' ? "h:mm a":"H:mm"
			}).toUpperCase();
		},

		buildRendering: function() {
			this.inherited(arguments);
			var t = this;
			var monthNode = document.createElement('span');
			// monthNode.innerHTML = locale.format(value, {selector:'date', datePattern: 'MMMM'});

			t.monthNode = monthNode;
			dojo.addClass(monthNode, 'currentMonth');
			t.toolbar.domNode.appendChild(monthNode);
		},

		postCreate: function(){
			var t = this;
			var cv = this.columnView;

			t.inherited(arguments);
			// t.configureButtons();
			if(t.showTodayButton === false){
				dojo.global.hideElement(t.todayButton.domNode);
			}
			if(t.showDayButton === false){
				dojo.global.hideElement(t.dayButton.domNode);
			}
			if(t.showFourDaysButton === false){
				dojo.global.hideElement(t.fourDaysButton.domNode);
			}
			if(t.showWeekButton === false){
				dojo.global.hideElement(t.weekButton.domNode);
			}
			if(t.showMonthButton === false){
				dojo.global.hideElement(t.monthButton.domNode);
			}

			aspect.after(t.matrixView, 'expandRow', function(rowId, colId){
				t._matrixExpandedCell = {r: rowId, c: colId};
			}, true);

			aspect.after(t.matrixView, 'collapseRow', function(){
				t._matrixExpandedCell = null;
			});

			aspect.after(t.matrixView, '_layoutHorizontalItemsImpl', function() {
				setPrintInfo();
			});
			aspect.after(cv.secondarySheet, '_layoutHorizontalItemsImpl', function() {
				setPrintInfo();
			});
			aspect.after(t.matrixView, '_layoutLabelItemsImpl', function() {
				setPrintInfo();
			});

			aspect.after(t.matrixView, 'refreshRendering', function(){
				var dateInterval = t.get('dateInterval');

				if(dateInterval === 'month' && t.matrixView && t._matrixExpandedCell){
					t.matrixView.expandRow(t._matrixExpandedCell.r, t._matrixExpandedCell.c, 0);
				}
			}, true);
		},

		refreshRendering: function(force) {
			if(force){
				this._timeRangeInvalidated = true;
			}
			this.inherited(arguments);
			if (this.monthNode) this.monthNode.innerHTML = locale.format(this.get('date'), {selector:'date', datePattern: 'MMMM'});
		},

		_setDateIntervalAttr: function(value){
			this.inherited(arguments);
			var cv = this.columnView;

			if (value === 'month') {
				dojo.global.showElement(this.monthNode);
			} else {
				dojo.global.hideElement(this.monthNode);
				setTimeout(function() {
					cv.set("startTimeOfDay", {hours: 8, duration: 0});
				}, 20);
			}
			Hi_Preferences.set('bigCalendarMode', value);
			Hi_Preferences.send();
		},

		_setDateAttr: function(value) {
			this.inherited(arguments);
			this.monthNode.innerHTML = locale.format(value, {selector:'date', datePattern: 'MMMM'});

			var timeInterval = this.computeTimeInterval(),
				s = timeInterval[0],
				e = timeInterval[1],
				today = new Date(),
				todayTime = today.getTime();

			if(!s || !e) return;
			// console.log(timeUtil.floorToWeek(value));

			if (this.isToday(value) || (s.getTime() <=  todayTime && todayTime < e.getTime())) {
				this.todayButton.set('label', 'Today');
			} else {
				this.todayButton.set('label', 'Day');
			}
			// this._set("date", value);
			// this._timeRangeInvalidated = true;
		},

		_setScrollableAttr: function(val){
			var cv = this.columnView;

			if(cv && cv.scrollBar){
				cv.scrollBar.scrollable = val;
			}
		},
	});

	return hiCalendar;
});