//dojo.provide("hi.tag");

define([
	'dojo/_base/array',
	'dojo/_base/lang',
	'dojo/dom-class',
	'dojo/store/Memory',
	'dojo/on'
], function (array, lang, domClass, Memory, on) {

	return dojo.global.Hi_Tag = {
		templates: {
			view: '<span><a title="Click to see only items tagged with %value%" href="javascript://" onclick="var s=this; Hi_Tag.filter(s);" class="tag">%value%</a></span>',
			edit: '<span class="tag" title="%value%"><a title="%value%" href="#" >%value%</a><a title="Click to remove tag %value%" class="remove_tag" href="" onclick="event.preventDefault();Hi_Tag.removePropertyTag(this.previousSibling)" data-icon=""></a></span>'
		},
		tags: {},
		
		tagsArray: [],

		tagsComboBox: null,

		_tag_bar: false,
		
		tag_bar: function() {
			if (!Hi_Tag._tag_bar) {
				Hi_Tag._tag_bar = dojo.query('div.tag_bar')[0];
			}
			return Hi_Tag._tag_bar;
		},
		
		tags_cached: false,
		

		/**
		 * refill tag drop down
		 * @param null
		 * @return null 
		 */
		dropCache: function() {
			var tagsCombo = dijit.byId('tagsComboBox');
			if (tagsCombo) this.tagsComboBox = tagsCombo;
			// var tagsComboBox = dijit.byId('tagsComboBox');
			Hi_Tag.tags_cached = false;
			Hi_Tag.fill();
			Hi_Tag.toggleBar();
			if (Hi_Tag.empty()) return;
			var tag_bar = Hi_Tag.tag_bar();
			 
			if (tag_bar.offsetHeight == 0) {
				dojo.global.setTimeout(Hi_Tag.dropCache, 3000);
				return;
			}
		},

		/**
		 * filter tags according to given tag name
		 * @param {string} tag tag name used to do filter
		 * @return
		 */
		filter: function(t) {
			/* Bug #2001 - block if there is a new task opened */
			if (hi.items.manager.openedId() && hi.items.manager.openedId() < 0) return false;

			Hi_Tag.filterPending = false;
			var old = Hi_Preferences.get('tagfilter', '');
			var tag = t.innerHTML ? unhtmlspecialchars(t.innerHTML) : t;
			Hi_Preferences.set('tagfilter', tag);
			hi.items.manager.tagChange(tag);
			Hi_Tag.barSelect();
		},

		/**
		 * hide or show tagbar when tags is or isn't empty
		 * @param
		 * @return
		 */
		toggleBar: function() {
			var group = Hi_Preferences.get("grouping", 'my', 'string');

			if (Hi_Tag.empty()) {
				hideElement(Hi_Tag.tag_bar());
				dojo.addClass('toolbar', 'no-tagbar');
				// #2127 show Add Project button only on Project tab.
				
				//Reflow
				// document.body.className = document.body.className;
			} else {
				showElement(Hi_Tag.tag_bar());
				Hi_Tag.barSelect();
				dojo.removeClass('toolbar', 'no-tagbar');
				
				//Reflow
				// document.body.className = document.body.className;
			}
		},

		/**
		 * update status when tagbar is/not selected
		 * @param
		 * @return
		 */
		barSelect: function() {
			var tag = Hi_Preferences.get('tagfilter', '');
			var tags = this.tagsComboBox && this.tagsComboBox.store.query(),
				selected = false, i;

			if (!tags || !tags.length) selected = false;

			for (i = 0; i < tags.length; i++) {
				if (tags[i].value === tag) {
					selected = true;
					// }
				}
			}
			this._updateTagStatusBar(tag);
			
			var bar = Hi_Tag.tag_bar();
			if (bar) {
				if (selected) {
					dojo.removeClass(bar, 'not_selected');
					dojo.addClass(bar, 'selected');
				} else {
					dojo.removeClass(bar, 'selected');
					dojo.addClass(bar, 'not_selected');
				}
			}
		},

		listPopuplar: function (except, array, count) {
			count = (count ? parseInt(count) : 3);
			var tags = Hi_Tag.list(except, false);
			var tagArr = [];
			var r = null;
			
			for(var i in tags) {
				tagArr.push({
					value: tags[i].value,
					count: tags[i].count
				});
			}
			
			tagArr.sort(function (a,b) {
				return a.count < b.count;
			});
			
			tagArr = tagArr.slice(0, count);
			
			if (array) {
				r = [];
				for(i in tagArr) r.push(tagArr[i].value);
			} else {
				r = {};
				for(i in tagArr) r[tagArr[i].value] = tagArr[i];
			}
			
			return r;
		},

		list: function(except, array) {
			var i, ii, j, removed, tags, tagsArray, maxCount = 0;
			
			tags = Hi_Tag.tags;
			tagsArray = Hi_Tag.tagsArray;
			
			if (Hi_Tag.tags_cached == false) {
				
				Hi_Tag.tags = {};
				tags = Hi_Tag.tags;
				Hi_Tag.tagsArray = [];
				tagsArray = Hi_Tag.tagsArray;
				data = hi.items.manager.get();
				
				for (i=0,ii=data.length; i<ii; i++) {
					if (dojo.isArray(data[i].tags)) {
						for (j = 0; j < data[i].tags.length; j++) {
							var tagName = data[i].tags[j];
							if (tagName in tags) {
								tags[tagName].count++;
								if (tags[tagName].count > maxCount) {
									maxCount = tags[tagName].count;
								}
							} else {
								tags[tagName] = {value: tagName, count: 0};
							}
							tagsArray.push(tagName);
						}
					}
				}
				
				tagsArray.sort();
				var newTags = {};
				for (i = 0; i < tagsArray.length; removed ? i: i++) {
					removed = false;
					if (i > 0 && tagsArray[i - 1] == tagsArray[i]) {
						tagsArray.splice(i, 1);
						removed = true;
					} else {
						newTags[tagsArray[i]] = tags[tagsArray[i]];
						if (maxCount > 5) {
							newTags[tagsArray[i]].count = Math.round(newTags[tagsArray[i]].count / maxCount * 5);
						}
					}
				}
				
				Hi_Tag.tags = newTags;
				tags = Hi_Tag.tags;
				
				Hi_Tag.tags_cached = true;
				var tagFilter = Hi_Preferences.get('tagfilter', '');
				if (tagFilter && !in_array(tagFilter, tagsArray)) {
					Hi_Tag.filter('');
				}
			}
			
			if (dojo.isArray(except)) {
				if (array) {
					tagsArray = [];
					for (i = 0; i < Hi_Tag.tagsArray.length; i++) {
						if (!in_array(Hi_Tag.tags[i], except)) {
							tagsArray.push(Hi_Tag.tags[i]);
						}
					}
				} else {
					tags = {};
					for (i in Hi_Tag.tags) {
						if (!in_array(i, except)) {
							tags[i] = Hi_Tag.tags[i];
						}
					}
				}
			}
			
			if (array) {
				return tagsArray;
			} else {
				return tags;
			}
		},

		listArray: function() {
			return tags = Hi_Tag.list(null, true);
		},

		empty: function() {
			return Hi_Tag.listArray().length == 0;
		},

		add: function(tag) {
			tag = Hi_Tag.trim(tag);
			if (!tag) return;
			tag = tag.split(',');
			var i;
			for (i = 0; i < tag.length; i++) {
				tag[i] = Hi_Tag.trim(tag[i]);
				Hi_Tag.tagsArray.push(tag[i]);
			}
			Hi_Tag.dropCache();
		},

		remove: function(tag) {
			tag = Hi_Tag.trim(tag);
			for (var i = 0; i < Hi_Tag.tagsArray.length; i++) {
				if (Hi_Tag.tagsArray[i] == tag) {
					Hi_Tag.tagsArray.splice(i, 1);
					Hi_Tag.dropCache();
					return;
				}
			}
		},

		/**
		 * fill tagsComboBox with tags
		 * @param 
		 * @return
		 */
		fill: function() {
			var tagsComboBox = dijit.byId('tagsComboBox'),
				tag = Hi_Preferences.get('tagfilter', '');

			if (!tagsComboBox) return;

			// if (except) {
			// 	except = Hi_Tag.getCurrItemTags(null, t);
			// 	console.log('except', except);
			// }
			var except = null,
				tags = Hi_Tag.list(),
				data = [], store, k;

			for (k in tags) {
				data.push(tags[k]);
			}
			// data.unshift({value: '--tag--'});
			store = new Memory({data: data});

			tagsComboBox.set('store', store);
			var _oldTag = tagsComboBox.get('value');
			if (tag !== _oldTag) {
				tagsComboBox._skipChange = true;
				tagsComboBox.set('value', tag);
			} 
			// tagsComboBox.set('')
			// addTemplateList(el, template, tags, empty);
			if (!tagsComboBox.initialized) {
				tagsComboBox.initialized = true;

				on(tagsComboBox, 'change', lang.hitch(this, function(val) {
					if (tagsComboBox._skipChange) {
						tagsComboBox._skipChange = false;
						return;
					}
					var tag = tagsComboBox.get('value');
					this.filter(tag);
				}));
			}
		},

		fillPopular: function(t, el, template, except, empty) {
			if (except) {
				except = Hi_Tag.getCurrItemTags(null, t);
			}
			var tags = Hi_Tag.listPopuplar(except);
			addTemplateList(el, template, tags, empty);
		},

		trim: function(str) {
			if (dojo.isString(str)) {
				/*Remove multiple spaces*/
				str = str.replace(/\s{2,}/g, ' ');
				str = str.trim();
				str = str.toLowerCase();
				return str;
			} else {
				return '';
			}
		},

		unique: function (arr) {
			// summary:
			//		Returns array removing all duplicate values
			// arr:Array
			//		Array which will be checked
			
			var unique = {};
			
			return dojo.filter(arr, function (value) {
				if (!unique[value]) {
					unique[value] = true;
					return true;
				} else {
					return false;
				}
			});
		},

		addTag: function(t, tags) {
			tags = tags.split(',');
			var i, tag, currTags = Hi_Tag.getCurrItemTags(null, t);
			var tag_template = Hi_Tag.templates.edit;
			var str;
			
			for (i = 0; i < tags.length; i++) {
				tag = Hi_Tag.trim(tags[i]);
				if (!tag) continue;
				if (in_array(tag, currTags)) continue;
				var tagEl = Hi_Tag.tagsElement(t);
				var tagsInput = this.getTagsInput(t);
				tagsInput = tagsInput && dijit.byNode(tagsInput);
				tagsInput.add(htmlspecialchars(tag));
				// str = tag_template.replace(/%value%/g, htmlspecialchars(tag)) + ' ';
				// tagEl.innerHTML = tagEl.innerHTML + str;
			}
			
			// Hi_Tag.fillMoreTags();
			
			return true;
		},

		getItemTags: function(id) {
			return hi.items.manager.get(id, "tags") || [];
		},

		getCurrItemTags: function(id, element) {
			var item = null,
				tagEl = null,
				tagEls = null,
				i = 0,
				tags = [], tagInput;
			
			if (element) {
				tagEl = Hi_Tag.tagsElement(element);
			} else {
				item = hi.items.manager.editing(id);
				if (item && item.editingView && item.editingView.tagInput) {
					// tagEl = dojo.query('.tags_edit',dojo.byId('propertiesViewNode_'+item.widgetId))[0];//item.query('.tags_edit', true);
					tagInput = item.editingView.tagInput;
					tags = tagInput.get('tags');
				}
			}
			
			return array.map(tags, function(tag){
				return unhtmlspecialchars(tag);
			});

			if (!tagEl) return tags;
			
			tagEls = dojo.query('.tag', tagEl);
			
			for (; i < tagEls.length; i++) {
				tags.push(unhtmlspecialchars(tagEls[i].getAttribute("title")/*innerHTML*/));
			}
			return tags;
		},

		tagsElement: function(t) {
			return getParentByClass(t, 'tag_cont', 'tags');
		},

		getTagsInput: function(t) {
			return getParentByClass(t, 'tag_cont', 'hiTags');
		},

		openedTagElement: function() {
			var item = hi.items.manager.editing();
			if (!item) return;
			var widgetNode = dojo.byId('propertiesViewNode_'+item.widgetId);
			var tagEdit = dojo.query('.tags_edit',widgetNode)[0];//item.query('.tags_edit', true);
			if (tagEdit) return tagEdit; // will always have if using TaskItem widget
			
			var tagCont = dojo.query('.tags_cont',widgetNode)[0];//item.query('.tag_cont', true);
			if (!tagCont) return;
			
			addTemplate(tagCont, 'properties_tag_template', {id: item.taskId});
			tagEdit = dojo.query('.tags_edit',widgetNode)[0];//item.query('.tags_edit', true);
			
			return tagEdit || null;
		},

		newTagElement: function() {
			return dojo.byId('newItem_tag');
		},

		viewTagElement: function() {
			var item = hi.items.manager.editing();
			if (!item) return;
			//return item.query('.tags_view', true);
			return dojo.query('.tags_view',dojo.byId('propertiesViewNode_'+item.widgetId))[0];
		},

		fillPropertyTags: function(id) {
			if (typeof id !== "number") {
				id = hi.items.manager.editingId();
			}
			if (!id || id === 0) return;

			var tags = Hi_Tag.getItemTags(id), tagEl, str, tag_template,
				item = hi.items.manager.item(id),
				ev = item && item.editingView,
				tagInput = ev && ev.tagInput;
				// tagInput = hi.items.manager.item(id).editingView.tagInput;
			var i, j;

			if(!tagInput) return;
			// console.log('tag input', tagInput);
			
			tagEl = Hi_Tag.openedTagElement();
			tagInput.removeAll();
			if (tagEl) {
				tag_template = Hi_Tag.templates.edit;
				for (i = 0, j = tags.length; i < j; i++) {
					// str = tag_template.replace(/%value%/g, htmlspecialchars(tags[i])) + ' ';
					str = htmlspecialchars(tags[i]);
					tagInput.add(str);
					// tagEl.innerHTML = tagEl.innerHTML + str;
				}
			}
			
			tagEl = Hi_Tag.viewTagElement();
			if (tagEl) {
				tag_template = Hi_Tag.templates.view;
				tagEl.innerHTML = '';
				for (i = 0, j = tags.length; i < j; i++) {
					str = tag_template.replace(/%value%/g, htmlspecialchars(tags[i])) + ' ';
					tagEl.innerHTML = tagEl.innerHTML + str;
				}
			}
			
			// Hi_Tag.fillMoreTags(id, true);
		},

		fillMoreTags: function (id, popular) {
			if (typeof popular == 'boolean') {
				Project.only_popular_tags = popular;
			} else {
				popular = Project.only_popular_tags;
			}
			
			var except = Hi_Tag.getCurrItemTags(id),
				taskItem = hi.items.manager.editing(id),
				node = taskItem ? dojo.query('.tag_cont',dojo.byId('propertiesViewNode_'+taskItem.widgetId))[0]/*taskItem.query('.tag_cont', true) */: null;
			
			if (!node) return;
			
			var tags = (popular ? this.listPopuplar(except) : this.list(except));
			
			if (popular) {
				var all_tags_unique = this.unique([].concat(this.listArray()).concat(except));
				var more_tags_node = dojo.query('.add_tag', node)[0];
				var more_tags_length = all_tags_unique.length - except.length - objectLength(tags);
				
				if (more_tags_length > 0) {
					dojo.removeClass(more_tags_node, 'hidden');
				} else {
					dojo.addClass(more_tags_node, 'hidden');
				}
			}
			
			//Hide "More tags..." if there are no tags at all
			var tagNode = dojo.query('div.tags_list', node)[0].parentNode;
			
			if (Hi_Tag.empty()) {
				tagNode.style.display = 'none';
			} else {
				tagNode.style.display = 'block';
			}
			
			var node_tags = to_array(dojo.query('.more_tags_list', node)).pop();
			var node_empty = to_array(dojo.query('.more_add_tag_list_empty', node)).pop();
			
			addTemplateList(node_tags, 'more_add_tag_list_item', tags, node_empty);
		},
		
		removePropertyTag: function(t) {
			if (t) {
				removeNode(t.parentNode);
			}
			
			// Hi_Tag.fillMoreTags();
		},
		/*
		select: function(tag) {
			Hi_Tag.selectedTag = tag;
			Hi_Preferences.set('tag', Hi_Tag.selectedTag);
		},
		openTag: function(t) {
			Tooltip.close('tag_open')
			Hi_Tag.select(unhtmlspecialchars(t.innerHTML));
			Tabs.open('grouping', 6);
		},
		*/
		addTagInput: function(t, e) {
			var input = getParentByClass(t, 'tag_cont', 'new_tag_input');
			var value = getValue(input);
			if (value === '') {
				if (e) {
					e.cancelBubble = false;
				}
			}
			var res = Hi_Tag.addTag(t, value);
			if (res) {
				setValue(input);
			}
		},

		emptyTagInput: function(t, e) {
			var input = getParentByClass(t, 'tag_cont', 'new_tag_input');
			setValue(input);
		},

		more: function() {
			var el = Hi_Tag.tag_bar();
			dojo.removeClass(el, 'closed');
			dojo.addClass(el, 'opened');
			dojo.global.reLayout();
		},

		less: function() {
			var el = Hi_Tag.tag_bar();
			dojo.removeClass(el, 'opened');
			dojo.addClass(el, 'closed');
			dojo.global.reLayout();
		},

		openTagEditDialog: function() {
			var dialog = dijit.byId('tagEditDialog');
			if (!dialog) return;

			dialog.show();
		},

		_updateTagStatusBar: function(tag) {
			var selected = dojo.byId('tag_bar_selected_tag'),
				bar = dojo.byId('tagStatus'),
				combo = dijit.byId('tagsComboBox');

			if (!selected || !bar) return false;

			combo._setLabelValue(tag);

			selected.innerHTML = tag;
			if (tag) {
				domClass.remove(bar, 'hidden');
			} else {
				domClass.add(bar, 'hidden');
			}
		}
	};
});