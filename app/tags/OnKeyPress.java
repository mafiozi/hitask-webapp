package tags;

import groovy.lang.Closure;

import java.io.PrintWriter;
import java.util.Map;

import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;
import play.templates.JavaExtensions;

@FastTags.Namespace("app.hi")
public class OnKeyPress extends BaseTag {
	public static void _onkeypress(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		if (body != null) {
			StringBuilder scriptSb = new StringBuilder();
			String tag = getArgAsString("tag", args, DEFAULT_TAG);
			
			scriptSb.append("keys=dojo.keys;").append("switch (js_block_onkeypress(event)) {");
			
			for (Object argKey : args.keySet()) {
				String argVal = (String)args.get(argKey);
				scriptSb.append(String.format("case keys.KEY_%s: %s ;break;",
					((String)argKey).toUpperCase(), argVal
				));
			}
			
			scriptSb.append("};");
			
			StringBuilder sb = new StringBuilder();
			sb.append("<").append(tag).append(" ").append("onkeypress=\"")
				.append(JavaExtensions.escapeHtml(scriptSb.toString())).append("\">");
			
			if (body != null) {
				sb.append(JavaExtensions.toString(body));
			}
			
			sb.append("</").append(tag).append(">");
			
			out.print(sb.toString());
		}
	}
}