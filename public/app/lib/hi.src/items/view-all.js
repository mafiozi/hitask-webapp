/**
 * Observer to notify calendar of changes made to items so that its view can be updated accordingly.  For example, if an item's color
 * is changed, it might need to be redrawn on the calendar
 */
define([
	'hi/store',
	'hi/items/base',
	'hi/items/date'
], function (hi_store, hi_items) {
	
	//After hi.store.init bind listeners
	//dojo.connect(hi_store, "init", hi_store, function () {
	dojo.global.viewAllTab = function(q) {
		// Do general stuff not specific to any view
		// update tags, calendar, project list dnd
		
		var readStore = hi_store.ProjectCache,
			writeStore = hi_store.Project,
			query = q;//readStore.query();
		
		
		function afterDataChange () {
			// summary:
			//		After data change update document title, tag bar, etc.
			
			//On data changes update document title
			Project.updateDocumentTitle();
			
			//Drop tag cache
			Hi_Tag.dropCache();

			dojo.global.reLayout();
			
			//Update drag and drop
			dragSources.targetProjects.sync();
			
			//Reset project coordinates
			dragSources.targetProjects.resetProjectCoordinates();
			
			//Update calendar
			if (updateCalendar) {
				updateCalendar = false;
				HiTaskCalendar.update(isUpdate);
			}
		}
		
		/* #2167 - This is now chained together with the observer in view-all and TaskList */
		// Observer data change
		var changeTimer = null,
			updateCalendar = false,
			isUpdate = false;
		//dojo.global.watchAll = function(object,removed,inserted,diff) {
		q.observe(function (object, removed, inserted, diff) {
			//Use timeout, because we want to update only when all item changes has been applied
			if (changeTimer) {
				clearTimeout(changeTimer);
			}
			changeTimer = setTimeout(afterDataChange, 1);
			
			// If any calendar related property changes then update calendar
			if (!diff) {
				isUpdate = false;
			} else {
				isUpdate = true;
			}
			if (!diff || "is_all_day" in diff || "title" in diff ||
				"recurring" in diff || "start_date" in diff || "start_time" in diff ||
				"end_date" in diff || "end_time" in diff || 'color' in diff ||
				'recurring_interval' in diff || 'recurring_end_date' in diff ||
				'completed' in diff || 'permission' in diff || 'instances' in diff
			) {
				updateCalendar = true;
			}
		}, 'deep');
		//};
		// Grouping change
		var onGroupingChange = function (group) {
			// summary:
			//		On group change show/hide "Add project" button
			/* #2965 - collapse expanded items on tab switch 
			 * Note: this should just be expanded items; items that were in editing
			 * mode will be saved
			 * */
			hi.items.manager.collapseOpenedItems();
			if (group == 'project' && !Hi_Search.isFormVisible()) {
				dojo.removeClass('newProjectButton', 'hidden');
			} else {
				dojo.addClass('newProjectButton', 'hidden');
			}
		};
		
		dojo.connect(hi.items.manager, "groupingChange", hi.items.manager, onGroupingChange);
		onGroupingChange(Hi_Preferences.get('currentTab', 'my', 'string'));
				
		// New item form
		var container = dojo.query("#searchItemListContainer ul").pop();
		var widget = new hi.widget.TaskItemNew({
			"taskId": -1, // id is reserved by dojo widget
			"listName": "newitem",
			"removeOnCollapse": false
		});
		
		widget.placeAt(container);
		widget.startup();
		
	};//);

});