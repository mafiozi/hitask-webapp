package api;

import com.google.gson.annotations.SerializedName;

/**
 * This class is used for mapping the JSON result of unsuccessful API call.
 * @author Vadim Manikhin
 */
public class ApiGeneralResponse {
	@SerializedName("error_message")
	private String errorMessage;
	@SerializedName("response_status")
	private Integer responseStatus;
	
	public boolean success() {
		return responseStatus == 0;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public Integer getResponseStatus() {
		return responseStatus;
	}
	
	public void setResponseStatus(Integer responseStatus) {
		this.responseStatus = responseStatus;
	}
}
