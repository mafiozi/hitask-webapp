package beans;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/**
 * Contains Help and FAQ topics.
 * @author Vadim Manikhin
 */
public class HelpData {
	private static final Comparator<HelpTopic> helpTopicComparator = new Comparator<HelpTopic>() {
		@Override
		public int compare(HelpTopic o1, HelpTopic o2) {
			if (o1.getIndex() == o2.getIndex()) {
				return 0;
			} else if (o1.getIndex() > o2.getIndex()) {
				return 1;
			} else if (o1.getIndex() < o2.getIndex()) {
				return -1;
			}
			
			return 0;
		}
	};
	
	private LinkedList<HelpTopic> helpTopics = new LinkedList<HelpTopic>();
	private LinkedList<HelpTopic> faqTopics = new LinkedList<HelpTopic>();
	
	public LinkedList<HelpTopic> getHelpTopics() {
		return helpTopics;
	}
	
	public void setHelpTopics(LinkedList<HelpTopic> helpTopics) {
		this.helpTopics = helpTopics;
	}
	
	public LinkedList<HelpTopic> getFaqTopics() {
		return faqTopics;
	}
	
	public void setFaqTopics(LinkedList<HelpTopic> faqTopics) {
		this.faqTopics = faqTopics;
	}
	
	public void sortHelpTopics() {
		Collections.sort(helpTopics, helpTopicComparator);
	}
	
	public void sortFaqTopics() {
		Collections.sort(faqTopics, helpTopicComparator);
	}
	
	public void sortAllTopics() {
		sortHelpTopics();
		sortFaqTopics();
	}
}