/**
 * Instantiation of Task lists on Color tab.  Instances of this lists are in:
 * hi.items.TaskListColor[]
 * hi.items.TaskListColorUngrouped
 */
define([
	'hi/store',
	'hi/items/base',
	'dojo/aspect',
	'hi/items/date'
], function (hi_store, hi_items, dojo_aspect) {
	
	//After hi.store.init create color groups
	dojo.global.viewColorTab = function(q) {
	//dojo.connect(hi_store, "init", hi_store, function () {
		// Create task groups (each color + default list) and set filters
		
		var readStore = hi_store.ProjectCache,
			writeStore = hi_store.Project,
			query = q,//readStore.query(),
			
			groupSelf = 'color',
			groupCurrent = Hi_Preferences.get('currentTab', 'my', 'string'),
			
			container = dojo.byId(Project.getContainer('tasksList')),
			
			i = 1,
			count = 7,
			group = null,
			taskList = null;

		if (groupCurrent == groupSelf) {
			var myC = dojo.connect(hi_store,'afterLoad',function() {
				// Add to container
				if (groupSelf == groupCurrent) {
					finalizeInitialization(hi_items.GroupColor,[hi_items.TaskListColorUngrouped]);
				}
				dojo.disconnect(myC);
			});
		}

		
		hi_items.GroupColor = [];
		hi_items.TaskListColor = [];
		
		for (; i <= count; i++) {
			
			group = new hi.widget.Group({
				"projectId": i,
				"emptyVisible": true, // shown if there are no tasks in it
				"expanded": true,     // by default expanded
				
				"title": "",
				"moveTitle": "",
				"groupClassName": "color" + i,
				
				"disabled": groupSelf != groupCurrent,
				"preferencesGroup": groupSelf
			});
			
			taskList = new hi.widget.TaskList({
				"readStore": readStore,
				"writeStore": writeStore,
				/* #2167 */
				"query": q,//groupSelf == groupCurrent?query:null,
				"disabled": groupSelf != groupCurrent,
				mainList:i==1?true:null,
				"localeNoItems": hi.i18n.getLocalization("center.drag_items_here"), // custom message
				
				"filterType": "top", // if most outer parent matched then this item should be in the list
				"filter": (function (color) {
					return function (item, strict) {
						// summary:
						//		Filter items which should be in the list. Returns true on success,
						//		false on failure
						// item:
						//		Item data
						/* #3229 - check there are any instances left */
						if (item.recurring) {
							if (!this.calculateInstanceDate(item)) return false;
						}
						if (item.color == color) return true;
						
						if (!strict && this.filterMatches(item.parent)) return true;
						return false;
					};
				})(i)
			});

			hi_items.GroupColor.push(group);
			hi_items.TaskListColor.push(taskList);
			
			group.placeAt(container);
			taskList.placeAt(group);
		}
		
		hi_items.TaskListColorUngrouped = new hi.widget.TaskList({
			"readStore": readStore,
			"writeStore": writeStore,
			"query": q,//groupSelf == groupCurrent?query:null,

			"disabled": groupSelf != groupCurrent,
			
			"filterType": "top", // if most outer parent matched then this item should be in the list
			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				// strict:
				//		If true then don't check ancestors
				/* #3229 - check there are any instances left */
				if (item.recurring) {
					if (!this.calculateInstanceDate(item)) return false;
				}
				if (!item.color) return true;
				
				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			}
		});
		
		hi_items.TaskListColorUngrouped.placeAt(container);
		
		dojo.connect(hi.items.manager, "groupingChange", hi.items.manager, function (group, groupPrev) {
			// summary:
			//		On group change enable/disable lists
			
			if (group == groupSelf && groupPrev != groupSelf) {
				refreshTab(hi_items.GroupColor,[hi_items.TaskListColorUngrouped]);
			} else if (group != groupSelf && groupPrev == groupSelf) {
				for (i=0; i<count; i++) {
					hi_items.GroupColor[i].set("disabled", true);
				}
				hi_items.TaskListColorUngrouped.set("disabled", true);
			}
			
			groupCurrent = group;
		});
		
		dojo.connect(Project, "changeSorting", Project, function () {
			// summary:
			//		On sorting change empty lists
			/* #2509
			if (groupCurrent == groupSelf) {
				for (i=0; i<count; i++) {
					hi_items.TaskListColor[i].reset({"fill": false});
				}
				hi_items.TaskListColorUngrouped.reset({"fill": false});
			}
			*/
			for (i=0; i<count; i++) {
				hi_items.GroupColor[i].set("disabled", true);
			}
			hi_items.TaskListColorUngrouped.set("disabled", true);
		});
		
		dojo.connect(hi.items.manager, "filterChange", hi.items.manager, function () {
			// summary:
			//		On sorting change empty lists
			
			if (groupCurrent == groupSelf) {
				/* #2509
				for (i=0; i<count; i++) {
					hi_items.TaskListColor[i].reset({"fill": false});
				}
				hi_items.TaskListColorUngrouped.reset({"fill": false});
				*/
				for (i=0; i<count; i++) {
					hi_items.GroupColor[i].set("disabled", true);
				}
				hi_items.TaskListColorUngrouped.set("disabled", true);
				
			}
		});
		
		/* This is invoked after refresh data store on filter selection change 
		 * This should just initiate the query observers on the lists and enable the lists
		 */
		dojo.connect(hi.items.manager, "redraw", hi.items.manager, function () {
			// summary:
			//		Some major change happened, redraw lists
			
			if (groupCurrent == groupSelf) {
				/* #2331 - rereshTab on searchChange */
				//if (hi_items.TaskListColorUngrouped.get('disabled')) initiateTab(hi_items.GroupColor,[hi_items.TaskListColorUngrouped]);
				/*else */ refreshTab(hi_items.GroupColor,[hi_items.TaskListColorUngrouped]);
			}
		});
		
		hi.items.manager.getTaskLists[groupSelf] = function () {
			// summary:
			//		Return all task lists for this group
		
			return [hi_items.TaskListColorUngrouped].concat(hi_items.TaskListColor);
		};
	
	};//);

});