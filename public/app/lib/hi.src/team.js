//dojo.provide("hi.team");
define(function () {
	
	var getValue = dojo.global.getValue,
		fillTemplate = dojo.global.fillTemplate,
		to_array = dojo.global.to_array;
	
	return dojo.global.Hi_Team = {
		chatMessages: {},
		unhoverTimeout: false,
		emptyUser: {
			'image': '',
			'image32': '',
			'image64': '',
			'offline': '',
			'menuStatus': '',
			'msgsStatus': '',
			'waitStatus': '',
			'reqStatus': '',
			'declStatus': '',
			'activeStatus': false,
			'premiumStatus': '',
			'businessStatus': '',
			'businessStatusDeclined': '',
			'businessStatusInactive': '',
			'businessStatusActive': '',
			'businessStatusAdmin': '',
			'businessStatusManager': ''
		},

		getContactById: function(id) {
			var i,
				tempt;

			id = parseInt(id, 10);

			for (i = 0; i < Project.friends.length; i++) {
				tempt = Project.friends[i];
				if(tempt.id === id){
					return tempt;
				}
			}

			return null;
		},

		validateSend: function(data) {
			if (data.message.trim() == '') return false;
			if (!isFinite(parseInt(data.userid)) || parseInt(data.userid) <= 0) return false;
			return true;
		},
		afterSend: function () {
			var chTxtInp = dojo.byId('chat_text_input');
			chTxtInp.value = '';
			chTxtInp.readonly = false;
			chTxtInp.disabled = false;
			try {
				chTxtInp.focus();
			} catch (err) {}
			Team.ping(true);
		},
		avatarOverTimeout: function() {
			if (elementVisible('tooltip_contact_info')) return;
			var t = this;
			if (t.getAttribute('showTimeout') == '1') {
				var c = Project.getContainer('fillUsers');
				var t_all = dojo.query('.user_item', dojo.byId(c));
				dojo.forEach(t_all, function(el) {if(el != t) dojo.removeClass(el, 'hover');});
				dojo.addClass(t, 'hover');
			} else {
				Tooltip.close('contact_info');
				dojo.removeClass(t, 'hover');
			}
		},
		avatarOver: function(b) {
			if (elementVisible(dojo.byId('tooltip_contact_info'))) {
				if (!Hi_Team.unhoverTimeout) {
					dojo.global.setTimeout(Hi_Team.unhoverAll, 500);
					Hi_Team.unhoverTimeout = true;
				}
				return;
			}
			var t = getParentByClass(this, 'user_item');
			t.setAttribute('showTimeout', b);
			if (dojo.hasClass(t, 'hover') == parseInt(b)) return;
			setTimeout(function(){Hi_Team.avatarOverTimeout.apply(t);}, b == 1 ? 0 : 500);
		},
		unhoverAll: function() {
			if (elementVisible(dojo.byId('tooltip_contact_info'))) {
				dojo.global.setTimeout(Hi_Team.unhoverAll, 500);
				return;
			}
			Hi_Team.unhoverTimeout = false;
			var c = Project.getContainer('fillUsers');
			var t_all = dojo.query('.user_item', dojo.byId(c));
			dojo.forEach(t_all, function(el) {dojo.removeClass(el, 'hover');});
		},
		confirm_delete: function(d, callback) {
			var message = hi.i18n.getLocalization("team.are_you_sure");
			Project.showDeleteConfirmation(message, d, callback);
			//return confirm(hi.i18n.getLocalization("team.delete_user_team_title1") + ' ' + d.name + ' ' + hi.i18n.getLocalization("team.delete_user_team_title2"));
		},
		confirm_delete_business: function(d, callback) {
			var message = hi.i18n.getLocalization("team.are_you_sure");
			Project.showDeleteConfirmation(message, d, callback);
			//return confirm(hi.i18n.getLocalization("team.delete_user_business_title1") + ' ' + d.name + ' ' + hi.i18n.getLocalization("team.delete_user_business_title2"));
		},
		confirm_send_again: function (d, callback) {
			var message = hi.i18n.getLocalization("team.are_you_sure");
			Project.showDeleteConfirmation(message, d, function () {
				callback();
				dojo.addClass(d, 'hidden');
				dojo.removeClass(nextElement(d), 'hidden'); 
				Tooltip.close('contact_info');
			});
		},
		closeChat: function() {
			var el;
			var id = parseInt(getValue('chat_user_id'));
			if (el = dojo.byId('friend_item_' + id)) {
				el.style.fontWeight = 'normal';
			}
			el = dojo.byId('conversation_container');
			dojo.addClass(to_array(dojo.query('.person', el)).pop(), 'invisible');
			fillTemplate(el, Hi_Team.emptyUser);
			dojo.byId('conversation').innerHTML = '';
			
			var buttonAddMessage = dijit.byId('buttonChatAddMessage');
			if (buttonAddMessage) {
				dijit.byId('buttonChatAddMessage').set('disabled', true);
			}
		},
		startChat: function(id) {
			Hi_Team.closeChat();
			id = id || false;
			if (!isFinite(parseInt(id))) id = false;
			id = id || getParentByClass(this, 'user_item', 'user_id').value;
			var u = Project.friends;
			var i = false;
			dojo.forEach(u, function(el){if(el.id == id) i = el;});
			if (i) {
				var el = dojo.byId('conversation_container');
				dojo.removeClass(to_array(dojo.query('.person', el)).pop(), 'invisible');
				fillTemplate(el, i);
				
				var buttonAddMessage = dijit.byId('buttonChatAddMessage');
				if (buttonAddMessage) {
					dijit.byId('buttonChatAddMessage').set('disabled', false);
				}
			}
			if (!(id in Hi_Team.chatMessages)) {
				Hi_Team.chatMessages[id] = [];
			}
	
			var umsg = Hi_Team.chatMessages[id];
			var c = dojo.byId('conversation');
			c.innerHTML = '';
			name = getValue('chat_user_name');
			dojo.forEach(umsg, function(el) {
				Hi_Team.addMessage(id, el, c, name, 1);
			});
			Hi_Team.scrollDown();
			var mc = parseInt(i['messages']);
			if (isFinite(mc) && mc > 0) {
				Project.ajax('chread', {userid: i['id'], time: Project.chatTime}, Hi_Team.messegeReadReceive, Hi_Team.messegeReadReceive,{userid: i['id']});
			}
			var el;
			if (el = dojo.byId('friend_item_' + id)) {
				el.style.fontWeight = 'bold';
			}
		},
		messegeReadReceive: function(dataResponse, options){
			if(!(dataResponse instanceof hiApiResponse) || !dataResponse.isStatusOk()){
				return;
			}
			Hi_Team.refreshMessageCount(options.userid,true);
			Hi_Team.updateAllMessagesEnvelope();
		},
		updateAllMessagesEnvelope: function(){
			if(Project.friends){
				// check if all messages readed for all contacts and hide icon newMessage for "My team"
				var msg_cnt = 0;
				for(var contactId in Project.friends){
					var count = parseInt(Project.friends[contactId].messages);
					if(count){
						msg_cnt += count;
					}
				}
				if (msg_cnt > 0) {
					dojo.removeClass('new_messages', 'invisible');
					dojo.removeClass('new_messages_collapsed', 'invisible');
					setValue(dojo.query('.message_count', dojo.byId('new_messages'))[0], msg_cnt);
				} else {
					dojo.addClass('new_messages', 'invisible');
					dojo.addClass('new_messages_collapsed', 'invisible');
				}
			}
			
		},
		addMessage: function (id, el, c, name, noscroll) {
			noscroll = noscroll || false;
			name = name || getValue('chat_user_name');
			c = c || dojo.byId('conversation');
			if (el.userFrom == id) {
				el['messageNew'] = false;
				el['me'] = '';
				el['sender_name'] = name;
				el['receiver_name'] = Project.user_name;
			} else {
				el['me'] = 'me';
				el['sender_name'] = Project.user_name;
				el['receiver_name'] = name;
			}
			var fTime = HiTaskCalendar.formatTime(time2str(str2timeAPI(el['messageTime']), 'h:i:s'));
			if (time2str(str2timeAPI(el['messageTime']), Hi_Calendar.format) == time2str('now', Hi_Calendar.format)) {
				el.datetime = fTime;
			} else {
				el.datetime = time2str(str2timeAPI(el['messageTime']), Hi_Calendar.format) + ' ' + fTime;
			}
			var t = childNode('message_template').cloneNode(true);
			fillTemplate(t, el);
			c.appendChild(t);
			if (!noscroll) {
				Hi_Team.scrollDown();
			}
		},
		scrollDown: function() {
			c_el = dojo.byId('conversation_scroll');
			c_el.scrollTop = c_el.scrollHeight;
		},
		refreshMessageCount: function(id, updateEnvelope) {
			if(!Project.friends){
				return 0;
			}
			updateEnvelope = updateEnvelope || false;
			var k, imax = Project.friends.length;
			for (k = 0; k < imax; k++) {
				var el = Project.friends[k];
				if (el.id == id) {
					var old_count = parseInt(Project.friends[k].messages || 0);
					var newm = 0;
					var umsg = Hi_Team.chatMessages[id];
					var i, imax = umsg ? umsg.length || 0 : 0;
					for (i = 0; i < imax; i++) {
						if (umsg[i].messageNew && umsg[i].userFrom == id) {
							newm++;
						}
					}
					if (old_count != newm) {
						Project.friends[k].messages = newm;
					}
					if(updateEnvelope){
						var imageMessageNode = dojo.query('img.message',dojo.byId('friend_item_' + id))[0];
						if(imageMessageNode){
							if(newm){
								showElement(imageMessageNode);
							}else{
								hideElement(imageMessageNode);
							}
						}
					}
					return newm;
				}
			}
		},
		viewAssignedTasks: function(id) {
			var tab = dojo.global.tabs.main;
			id = id || false;
			Hi_Preferences.set('main_assign_user', id);
			Hi_Preferences.set('p_5_' + (id * 2 + 1), 1);
			if (tab) {
				tab.open('team');
			}
		},
		hasAssignedTasks: function(t) {
			var id = parseIntRight(getParentByClass(t, 'user_item', 'user_id').value);
			var exists = hi.items.manager.get({"assignee": id}).length;
			if (exists) {
				showElement('viewAssignedTasks');
			} else {
				hideElement('viewAssignedTasks');
			}
		},
		openNew: function() {
			if (!Project.friends) return;
			var i, imax = Project.friends.length;
			for (i = 0; i < imax; i++) {
				if (Project.friends[i].messages) {
					Hi_Team.startChat(Project.friends[i].id);
					return;
				}
			}
		},
		initButtons: function () {
			dojo.parser.instantiate(dojo.query('button[dojoType]', dojo.byId('messenger')));
		},
		initResizers: function() {
			var containers = ["TeamResizeHandle", "ConversationResizeHandle"];
			for (var i = 0; i < containers.length; i++) {
				//Get default properties
				var props = dojo.dom.getAllAttributes(dojo.byId(containers[i]));
				props.id = undefined;
	
				//Create widget instance
				var widget = new dojox.layout.ResizeHandle(dojo.mixin({
					resizeAxis: 'y',
					constrainMax: true,
					maxHeight: (props.maxheight ? props.maxheight : 1000),
					minHeight: (props.minheight ? props.minheight : 0),
					targetId: (props.targetid ? props.targetid : ''),
					activeResize: true,
					animateSizing: false
				}, props)).placeAt(dojo.byId(containers[i]));
				
				widget.onResize = Hi_Team.saveContainerSize;
				Hi_Team.setContainerSize(widget);
			}
		},
		getContPrefName: function(wId) {
			switch (wId) {
				case 'TeamResizeHandle':
					return 'team_size';
				case 'ConversationResizeHandle':
					return 'conversation_size';
				default:
					return;
			}
		},
		saveContainerSize: function() {
			var name = Hi_Team.getContPrefName(this.id);
			if (!name) return;
			Hi_Preferences.set(name, dojo.style(this.targetId, 'height'));
		},
		setContainerSize: function(t) {
			var defaults = {team_size: 278, conversation_size: 122};
			var name = Hi_Team.getContPrefName(t.id);
			
			var hDefault = (defaults[name] ? defaults[name] : 200);
			var h = parseInt(Hi_Preferences.get(name, hDefault));
			
			dojo.style(t.targetId, "height", h+"px");
		},
		sound: 1,
		soundCreated: false,
		playSound: function() {
			Sound.play(Hi_Team.sound);
		}
	};

});