package beans;

/**
 * Dummy java bean. Update and replace it as soon as News support will be added to app.
 * @author Vadim Manikhin
 */
public class LatestNews {
	public int getId() {
		return 1;
	}
	
	public String getPermalink() {
		return "http://www.google.com";
	}
	
	public String getTitle() {
		return "Latest News Title";
	}
}