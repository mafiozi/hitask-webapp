define([
	"dojo/on",
	"dojo/fx",
	"dojo/date/locale"
], function (dojo_on, dojo_fx, djDateLocale) {

	dojo.global.CONSTANTS = dojo.mixin(dojo.global.CONSTANTS || {}, {
		duration: 200,
		removeCompletedMinCount: 2
	});

	dojo.global.fnEmpty = function() {};

	dojo.global.disableNew = function() {
		showElement('newItem_wait');
		var x = dojo.query('#newTaskBody input, #newTaskBody textarea, #newTaskBody select');
		var i, imax = x.length;
		for (i = 0; i < imax; i++) {
			x[i].disabled = true;
		}
	};

	dojo.global.archiveTask = function(id,options) {
		options.tarchive = true;
		if (Project.get(id, 'category') == 0) {
			Project.ajax('tarchive', {id: id, project:1});
		}
		else {
			Project.ajax('tarchive', {id: id});
		}
		Project.removeTask(id, options);
		Hi_Notification.putMessageInQue('task_archive');
		return;
	};

	dojo.global.sortAlphbetically = function() {
		var projects = dojo.byId('projects');
		var c = to_array(projects.getElementsByTagName('UL'));
		var i, imax = c.length;
		var cn = [];
		for (i = 0; i < imax; i++) {
			var id = parseIntRightLocal(c[i].id);
			if (id % 2) {
				cn.push(c[i]);
			}
		}
		c = cn;
	};

	dojo.global.enableNew = function() {
		hideElement('newItem_wait');
		var x = dojo.query('#newTaskBody input, #newTaskBody textarea, #newTaskBody select');
		var i, imax = x.length;
		for (i = 0; i < imax; i++) {
			x[i].disabled = false;
		}
	};

	dojo.global.openNewItemTab = function(tab_id, num) {
		var i = 0,
			tab,
			data = false;

		while (tab = dojo.byId(tab_id + '_' + i)) {
			if (num != i) {
				if (!dojo.hasClass(tab, 'hidden')) {
					var active = i;
					hideElement(tab);
					var inp = tab.getElementsByTagName('INPUT');
					var j, jmax = inp.length;
					data = {};
					for (j = 0; j < jmax; j++) {
						if (inp[j].name && inp[j].value) {
							if (inp[j].type == 'checkbox') {
								data[inp[j].name] = inp[j].checked ? 1 : 0;
							} else {
								data[inp[j].name] = inp[j].value;
							}
							data[inp[j].name + '_enabled'] = !inp[j].disabled;
						}
					}
					var inp = tab.getElementsByTagName('SELECT');
					var j, jmax = inp.length;
					for (j = 0; j < jmax; j++) {
						if (inp[j].name && inp[j].value) {
							data[inp[j].name] = inp[j].value;
							data[inp[j].name + '_enabled'] = !inp[j].disabled;
						}
					}
					data.hasStartDate = data.start_date ? true : false;
					data.hasStartDateTime = data.hasStartDate && data.start_time;
					break;
				}
			}
			i++;
		}

		//Tabs.open('category_type', num);

		tab = dojo.byId(tab_id + '_' + num);

		if (tab) {
			if (data) {
				fillTemplate(tab, data);
			}
			showElement(tab);
			var focus = dojo.query('.focus', tab);
			if (focus.length > 0) {
				var e;
				try {focus[0].focus();} catch (e) {}
			}

			dojo.byId('newItem_color_container').style.display = 'block';
		} else {
			//Tab 4 (Notes)
			dojo.byId('newItem_color_container').style.display = 'block';
		}
	};

	dojo.global.dragSources = {};
	dojo.global.dragSortLists = [];

	dojo.global.refreshSortables = function() {
		/**
		 * Cache global functions
		 */
		var parseIntRightLocal = parseIntRight;
		var Hi_Preferences_getLocal = Hi_Preferences.get;
		var Project_moveTaskLocal = Project.moveTask;
		var dojo_html_addClass = dojo.addClass;

		var projects = dojo.byId('projects');
		if (!projects) return;
		var group = Hi_Preferences.get('currentTab', 'my', 'string');
		var c = to_array(projects.getElementsByTagName('UL'));

		/**
		 * Remove categories "From user" from droppable places
		 */
		if (group == 'team') {
			var i, imax = c.length;
			var cn = [];
			for (i = 0; i < imax; i++) {
				var id = parseIntRightLocal(c[i].id);
				if (id % 2) {
					cn.push(c[i]);
				}
			}
			c = cn;
		}

		var i = 0;
		var imax = c.length;
		var a = [];
		var empty = dojo.byId('no_project_tasks');
		if (empty) {
			a = [empty];
		}
		for (i = 0; i < imax; i++) {
			a[a.length] = c[i];
		}
		if (empty) {
			c[c.length] = empty;
			imax++;
		}

		for(var i=0,j=dragSortLists.length; i<j; i++) {
			dragSortLists[i].destroy();
		}

		dragSortLists = [];

		for (i = 0; i < imax; i++) {
			//new dojo.dnd.HiSortDropTarget(c[i], ["sortTasks"]);

			var lis = c[i].getElementsByTagName("li");

			var old_id = false;
			var old_task_sort = false;
			var xmax = lis.length;

			for (var x = 0; x < xmax; x++) {
				//Sorting for unsorted tasks
				var id = parseIntRightLocal(lis[x].id);

				//Labels doesn't have D&D
				if (!id) continue;

				//No sorting for completed tasks
				if ((group == 'my' || group == 'date' || group == 'color' || group == 'project' || group == 'team') && (Project.data[id].completed === 1 || Project.data[id].completed == '1')) {
					continue;
				}

				var task_sort = parseInt(Hi_Preferences_getLocal('ts_' + id));
				if (!isFinite(task_sort)) {
					task_sort = false;
				}
				if (!task_sort) {
					if (old_task_sort) {
						task_sort = Project_moveTaskLocal(id);
					} else {
						task_sort = Project_moveTaskLocal(id);
					}
				}
				var old_id = id;
				var old_task_sort = task_sort;
				dojo_html_addClass(lis[x], 'dojoDndItem');
			}

			/**
			 * Dragging
			 */
			var dragS = new dojo.dnd.HiDragSource(c[i], {});
				dragSortLists[dragSortLists.length] = dragS;

			/**
			 * Sorting
			 */
			if (group != 'date' && !Hi_Search.isFormVisible()) {
				var dragS = new dojo.dnd.HiSortDragSource(c[i], {});
					dragS.dragClass = 'HiSortDragSourceClass';

				dragSortLists[dragSortLists.length] = dragS;


				dojo.connect(dragS, "onDndStart", function () {
					var c = dojo.byId(Project.js_containers['fillTasks'].id);
					dojo.addClass(c, 'sortingInProgress');
					Project.sorting = true;
				});

				var fn_drop = function () {
					if (Project.sorting && HiSortDragSource_dragNode) {
						var c = dojo.byId(Project.js_containers['fillTasks'].id);
						dojo.removeClass(c, 'sortingInProgress');
						updateTask(HiSortDragSource_dragNode);
						Project.sorting = false;
						//checkLists();
					}
				};

				dojo.connect(dragS, "onDndCancel", fn_drop);
				dojo.connect(dragS, "onDndDrop", fn_drop);
			}
		}

		var container = dojo.byId(Project.getContainer('fillTasks'));
		var drags = dojo.query('div.heading', container);
		var i, imax = drags.length;

		drag = [];

		/**
		 * Allow sorting of projects
		 */
		if (group == 'project') {
			var dragS = new dojo.dnd.HiProjectDragSource(dojo.byId('projects'), {});
			dragSortLists[dragSortLists.length] = dragS;

			dojo.connect(dragS, "onDndStart", function () {
				Project.dragging = true;
			});

			var fn_drop = function () {
				var m = dojo.dnd.Manager();

				if (this == m.source && this.current && this.dragNode) {		/*  m.canDropFlag &&  */
					if (this.dragBefore) {
						insertBefore(this.dragNode, this.current);
					} else {
						insertAfter(this.dragNode, this.current);
					}
					updateProjectPosition(this.dragNode);
				}

				Project.dragging = false;
			};

			dojo.connect(dragS, "onDndCancel", fn_drop);
			dojo.connect(dragS, "onDndDrop", fn_drop);

			imax = c.length;
			for (i = 0; i < imax - 1; i++) {
				var pr_li = getParentByClass(c[i], 'project');
				var id = parseIntRightLocal(pr_li.id);
				var pro_sort = parseInt(Hi_Preferences_getLocal('ps_' + id));
				if (!isFinite(pro_sort)) {
					pro_sort = false;
				}
				if (!pro_sort) {
					if (old_pro_sort) {
						pro_sort = Project.moveProject(id, old_id);
					} else {
						pro_sort = Project.moveProject(id);
					}
				}
				var old_id = id;
				var old_pro_sort = pro_sort;
				dojo_html_addClass(pr_li, 'sortable');
				dojo_html_addClass(pr_li, 'dojoDndItem');

			}
		}

		/**
		 * Calendar
		 */
		if (!dragSources.targetCalendar) {

			var fn_drop_calendar = function () {
				var m = dojo.dnd.Manager();
				if (this.dropTarget && m.source instanceof dojo.dnd.HiDragSource) {
					var dropValue = this.dropTarget.getElementsByTagName('INPUT');
					if (!dropValue.length || !dropValue[0]) return (Project.dragging = false);

					dropValue = dropValue[0].value;
					var dragId = parseIntRight(m.source.dragNode.id);

					var sd = Project.get(dragId, 'start_date');
					var ed = Project.get(dragId, 'end_date');
					var type = Project.get(dragId, 'category');

					if (sd && sd != '' && ed && ed != '') {
						var delta = str2time(dropValue, Hi_Calendar.format) - str2time(sd, Hi_Calendar.format);
						ed = time2str(new Date(str2time(ed, Hi_Calendar.format).valueOf() + delta), Hi_Calendar.format);
						Project.change({id: dragId, 'start_date': dropValue, end_date: ed, category: (type == 4) ? 2 : type});
						updateTaskHtml(dragId, {start_date: true});
					} else {
						Project.change({id: dragId, 'start_date': dropValue, category: (type == 4) ? 2 : type});
						updateTaskHtml(dragId, {start_date: true});
					}

					this.dropTarget = null;
				}

				Project.dragging = false;
			};

			var calendar = dojo.byId('hitask_calendar');
			var drop = new dojo.dnd.HiDragTargetCalendar(calendar);
			drop.text = hi.i18n.getLocalization('center.start_on') + ' ';

			dojo.connect(drop, "onDndCancel", fn_drop_calendar);
			dojo.connect(drop, "onDndDrop", fn_drop_calendar);

			dragSources.targetCalendar = drop;
		}

		/**
		 * Timetable
		 */
		if (!dragSources.targetTimetable) {

			var fn_drop_timetable = function () {
				var m = dojo.dnd.Manager();

				if (this.dropValue && m.source instanceof dojo.dnd.HiDragSource) {
					var dragId = parseIntRight(m.source.dragNode.dragId);
					var obj = {id: dragId};

					obj['start_date'] = HiTaskCalendar.getCurrentDateStr();
					obj['start_time'] = this.dropValue;

					if((Project.data[obj['id']]['end_date'] != '') && (Project.data[obj['id']]['end_time'] != ''))
					{
						var oldStartDate = HiTaskCalendar.getDateTimeFromStr(Project.data[obj['id']]['start_date'], Project.data[obj['id']]['start_time']);
						var newStartDate = HiTaskCalendar.getDateTimeFromStr(obj['start_date'], obj['start_time']);

						var dateOffset = newStartDate - newStartDate.getTimezoneOffset() * 60 * 1000 - oldStartDate + oldStartDate.getTimezoneOffset() * 60 * 1000;

						var oldEndDate = HiTaskCalendar.getDateTimeFromStr(Project.data[obj['id']]['end_date'], Project.data[obj['id']]['end_time']);

						var newEndDate = new Date();
						newEndDate.setTime(oldEndDate.getTime() + dateOffset);

						obj['end_date'] = HiTaskCalendar.getStrFromDate(newEndDate);
						obj['end_time'] = HiTaskCalendar.getStrFromTime(newEndDate);
					}
					if (Project.get(dragId, 'category') == 4) {
						obj['category'] = 2;
					}

					this.dropValue = null;

					Project.change(obj);
					updateTaskHtml(dragId, {start_date: true});
				}

				Project.dragging = false;
			};

			var timetable = dojo.byId('timetable');
			var drop = new dojo.dnd.HiDragTargetTimetable(timetable);
			drop.text = hi.i18n.getLocalization('center.start_on') + ' ';

			dojo.connect(drop, "onDndCancel", fn_drop_timetable);
			dojo.connect(drop, "onDndDrop", fn_drop_timetable);

			dragSources.targetTimetable = drop;
		}

		/**
		 * All day event
		 */
		if (!dragSources.targetAllDayEvents) {

			var fn_drop_day = function () {
				var m = dojo.dnd.Manager();

				if (this.inDropArea && m.source instanceof dojo.dnd.HiDragSource) {
					var dragId = parseIntRight(m.source.dragNode.dragId);
					var curTime = HiTaskCalendar.currentDate.getTime();
					var curStr = time2str(HiTaskCalendar.currentDate, Hi_Calendar.format);

					var sd = Project.get(dragId, 'start_date');
					var ed = Project.get(dragId, 'end_date');
					var type = Project.get(dragId, 'category');

					if (sd && sd != '' && ed && ed != '') {
						var delta = curTime - str2time(sd, Hi_Calendar.format);
						ed = time2str(new Date(str2time(ed, Hi_Calendar.format).valueOf() + delta), Hi_Calendar.format);
						Project.change({id: dragId, 'start_date': curStr, end_date: ed, category: (type == 4) ? 2 : type});
						updateTaskHtml(dragId, {start_date: true});
					} else {
						Project.change({id: dragId, 'start_date': curStr, category: (type == 4) ? 2 : type});
						updateTaskHtml(dragId, {start_date: true});
					}

					this.inDropArea = false;
				}

				Project.dragging = false;
			};

			var day = dojo.byId('day').parentNode;
			var drop = new dojo.dnd.HiDragTargetDay(day);
			drop.text = hi.i18n.getLocalization('center.start_on') + ' ';

			dojo.connect(drop, "onDndCancel", fn_drop_day);
			dojo.connect(drop, "onDndDrop", fn_drop_day);

			dragSources.targetAllDayEvents = drop;
		}

		/**
		 * Team list
		 */
		if (!dragSources.targetTeam) {

			var fn_drop_team = function () {
				var m = dojo.dnd.Manager();

				if (this.dropValue && m.source instanceof dojo.dnd.HiDragSource) {
					var dragId = m.source.dragNode.dragId;
					Project.assign(dragId, this.dropValue);
					this.dropValue = null;
				}

				Project.dragging = false;
			};

			var chat = dojo.byId('team_container_0');
			var drop = new dojo.dnd.HiDragTargetTeam(chat);
			drop.text = hi.i18n.getLocalization('center.assign_to') + ' ';

			dojo.connect(drop, "onDndCancel", fn_drop_team);
			dojo.connect(drop, "onDndDrop", fn_drop_team);

			dragSources.targetTeam = drop;
		}

		/**
		 * Self
		 */
		if (!dragSources.targetSelf) {
			var self = dojo.byId('header_myself');
			var drop = new dojo.dnd.HiDragTargetSelf(self);
			drop.text = hi.i18n.getLocalization('center.assign_to') + ' ';

			dojo.connect(drop, "onDndCancel", fn_drop_team);
			dojo.connect(drop, "onDndDrop", fn_drop_team);

			dragSources.targetSelf = drop;
		}

		/**
		 * Conversations
		 */
		if (!dragSources.targetConversations) {
			var cont = dojo.byId('conversation_container');
			var drop = new dojo.dnd.HiDragTargetSelf(cont);
			drop.text = hi.i18n.getLocalization('center.assign_to') + ' ';

			dojo.connect(drop, "onDndCancel", fn_drop_team);
			dojo.connect(drop, "onDndDrop", fn_drop_team);

			dragSources.targetConversations = drop;
		}

		/**
		 * Projects
		 */
		if (!dragSources.targetProjects) {
			var fn_drop_project = function () {
				var m = dojo.dnd.Manager();

				if (this.dropValue !== false && m.source instanceof dojo.dnd.HiDragSource) {
				var dragId = m.source.dragNode.dragId;

				var group = Hi_Preferences.get('currentTab', 'my', 'string');
				switch (group) {
					case 'my':
						this.dropValue = parseInt(this.dropValue);

						if (this.dropValue) {
							var targetUL = dojo.byId('tasks' + this.dropValue);
						} else {
							var targetUL = dojo.byId('no_project_tasks');
						}

						var li = dojo.byId('task_' + dragId);
						prependChild(li, targetUL);

						//Remove task_empty node
						var emptyLI = dojo.query('li.task_empty', targetUL);
						if (emptyLI[0]) {
							emptyLI[0].parentNode.removeChild(emptyLI[0]);
						}

						//Update data
						//checkLists();
						updateTask(li);

						break;
					case 'color':
						//Move html node

						if (this.dropValue) {
							var targetUL = dojo.byId('tasks' + this.dropValue);
						} else {
							var targetUL = dojo.byId('no_project_tasks');
						}

						var li = dojo.byId('task_' + dragId);
						targetUL.appendChild(li);

						//Remove task_empty node
						var emptyLI = dojo.query('li.task_empty', targetUL);
						if (emptyLI[0]) {
							emptyLI[0].parentNode.removeChild(emptyLI[0]);
						}

						//Update data
						//checkLists();
						updateTask(li);
						break;
					case 'date':
						var type = Project.get(dragId, 'category');
						this.dropValue = parseInt(this.dropValue);
						if (this.dropValue == 0) {
							this.dropValue = time2str('now', Hi_Calendar.format);
						} else if (this.dropValue == 1) {
							this.dropValue = time2str('tomorrow', Hi_Calendar.format);
						} else return;

						var end_date = false;
						if (type == 1 || type == 2) {
							end_date = Project.get(dragId, 'end_date');
						}
						if (type == 4) {
							type = 2;
						}

						if (end_date) {
							Project.change({id: dragId, category: type, 'start_date': this.dropValue, 'end_date': this.dropValue});
						} else {
							Project.change({id: dragId, category: type, 'start_date': this.dropValue});
						}

						break;
					case 'project':
						//Move html node
						if (this.dropValue) {
							var targetUL = dojo.byId('tasks' + this.dropValue);
						} else {
							var targetUL = dojo.byId('no_project_tasks');
						}

						var li = dojo.byId('task_' + dragId);
						targetUL.appendChild(li);

						//Update 'shared' property
						if (this.dropValue && Project.get(this.dropValue, 'shared') == '1') {
							var options = {redraw: false};

							setTimeout(function () {
								Project.set(dragId, 'shared', 1, options);
							}, 0);
							dojo.addClass(m.source.dragNode, 'shared');
						}

						//Update data
						//checkLists();
						updateTask(li);

						break;
					case 'team':
						//Move html node
						if (this.dropValue) {
							var targetId = this.dropValue * 2 + 1;
							var targetUL = dojo.byId('tasks' + targetId);
						} else {
							var targetUL = dojo.byId('no_project_tasks');
						}

						var li = dojo.byId('task_' + dragId);

						if (targetUL) {
							targetUL.appendChild(li);

							//Remove task_empty node
							var emptyLI = dojo.query('li.task_empty', targetUL);
							if (emptyLI[0]) {
								emptyLI[0].parentNode.removeChild(emptyLI[0]);
							}

							//Update data
							//checkLists();
							updateTask(li);
						} else {
							//Update data
							//checkLists();
							updateTask(li, {redraw: true});
						}

						break;
				}

				}
				Project.dragging = false;
				this.dropValue = false;
			};

			var cont = dojo.byId(Project.getContainer('fillTasks'));
			var drop = new dojo.dnd.HiDragTargetProject(cont);

			dragSources.targetProjects = drop;

			dojo.connect(drop, "onDndCancel", fn_drop_project);
			dojo.connect(drop, "onDndDrop", fn_drop_project);
		}

		checkLists();
	}

	/**
	 * Changes product tag of task
	 */
	dojo.global.updateTask = function(li, options) {
		var n = parseIntRight(li.getAttribute("data-task-id"));
		if (!li.parentNode) return;

		var group_id = parseIntRight(li.parentNode.getAttribute("data-list-id"));
		var group = Hi_Preferences.get('currentTab', 'my', 'string');

		var options = dojo.mixin({redraw: false}, options || {});
		switch (group) {
			case 'my':
				Project.setStarred(n, li, group_id);
				break;
			case 'date':
				break;
			case 'color':
				Project.set(n, 'color', group_id, options);
				chooseColor(group_id, n);
				break;
			case 'project':
				var current_group_id = Project.get(n, 'parent');
				Project.set(n, 'parent', group_id, options);

				//Make item shared if needed
				if (Project.get(group_id, 'shared') == '1' && Project.get(n, 'shared') == '0') {
					Project.set(n, 'shared', '1');
					var li = dojo.byId('task_' + n);
					dojo.addClass(li, 'shared');
				}

				if (current_group_id != group_id && Project.get(n, 'completed') == 0) {
					var tmp = prevElement(li);
					while(tmp && dojo.hasClass(tmp, 'completed')) {
						tmp = prevElement(tmp);
					}

					if (tmp && tmp !== li) {
						insertAfter(li, tmp);
					}
				}

				break;
			case 'team':
				if (group_id == 0) {
					Project.assign(n, false, Project.get(n, 'assign_user'), options);
				} else {
					var group_id = Math.round((group_id - 1) / 2);
					Project.assign(n, group_id, undefined, options);
				}
				break;
		}

		var prevSibl = prevElement(li);
		if (prevSibl) {
			prevSibl = parseIntRight(prevSibl.id);
		}
		if (prevSibl) {
			Project.moveTask(n, prevSibl);
		} else {
			Project.moveTask(n);
		}

		checkLists();
	}

	/**
	 * Changes product tag of task
	 */
	dojo.global.updateProjectPosition = function(li) {
		if (Hi_Preferences.get('currentTab', 'my', 'string') != 'project') return;
		var n = parseIntRight(li.id);

		var prevSibl = prevElement(li);
		if (prevSibl) {
			prevSibl = parseIntRight(prevSibl.id);
		}
		if (prevSibl) {
			Project.moveProject(n, prevSibl);
		} else {
			Project.moveProject(n);
		}
	}

	dojo.global.setProjectPercentage = function(el, item_cnt, completed_cnt) {
		var group = Hi_Preferences.get('currentTab', 'my', 'string');

		if (!el) return;
		if (group != 'project') return;

		var percentage = '';
		var c1 = getParentByClass(el, 'project');
			c1 = dojo.query('span.percentage', c1)[0];

		if (item_cnt && completed_cnt) {
			percentage = Math.round(completed_cnt / item_cnt * 100);
			if (!percentage) {
				percentage = '';
			} else {
				percentage += '%';
			}
		}

		innerText(c1, percentage);
	}

	dojo.global.setItemCount = function(el, c) {
		if (!el) return;
		c = c || 0;
		c = parseInt(c);
		if (!isFinite(c)) {
			c = 0;
		}
		var c1 = dojo.query('span.cnt_span', el)[0];
		var c2 = dojo.query('span.cnt_one', el)[0];
		var c3 = dojo.query('span.cnt_mult', el)[0];
		innerText(c1, c ? c : hi.i18n.getLocalization('main.no'));
		if (c % 10 == 1 && c % 100 != 11) {
			hideElement(c3);
			showElement(c2);
		} else {
			hideElement(c2);
			showElement(c3);
		}
	}

	dojo.global.log = function(str) {
		if (log.start_time) {
			console.log(str, ' [' + ((new Date()).valueOf() - log.start_time) + ']');
		} else {
			console.log(str, ' [' + Date() + ']');
		}
		log.start_time = new Date();
	}

	dojo.global.getEventDateByNode  = function(node) {

		var parent = getParentByClass(node, 'task_li');
		if (parent) {
			var inp = parent.getElementsByTagName('INPUT');
			for(var i=0,j=inp.length; i<j; i++) {
				if (inp[i].getAttribute('type') == 'checkbox') {
					return inp[i].getAttribute('eventdate') || '';
				}
			}
		}

		return null;
	}

	dojo.global.getTaskDateTitle  = function(id, date, pdata) {
		var dateEnd = hi.items.date.getEventEndTime(id, '23:59');
		var dateStart = hi.items.date.getEventStartTime(id, '23:59');
		var recurring = parseInt(Project.get(id, 'recurring', 0));

		if (recurring) {
			//recurring_date = Project.getNextEvent(id);
			/*
			var next_occurance = HiTaskCalendar.nearestEvent(Project.data[id]);
			dateStart = next_occurance.startDate;
			dateEnd = next_occurance.endDate;
			*/

			if (pdata && pdata.next_event_date) {
				var recurring_date = str2time(pdata.next_event_date, 'y-m-d');
			} else {
				var recurring_date = HiTaskCalendar.nextEvent(dateStart, parseInt(Project.get(id, 'recurring')));
					recurring_date = (recurring_date ? recurring_date[1] : null);
			}

			if (recurring_date) {
				if (dateEnd) {
					dateEnd = recurring_date;
				} else {
					dateStart = recurring_date;
				}
			}
		}

		var date = dateEnd || dateStart;

		if (date) {
			var dateTemp = date.getTime();
			var dateEndTemp = (dateEnd ? dateEnd.getTime() : null);
			var dateStartTemp = (dateStart ? dateStart.getTime() : null);

			var dayLength = 3600 * 24 * 1000;
			var todayStart = str2time(HiTaskCalendar.getTodayDateStr(), 'y-m-d').getTime();
			var todayEnd = todayStart + dayLength - 1;
			var yesterdayStart = todayStart - dayLength;
			var tommorowEnd = todayEnd + dayLength;

			var months = capitalizeFirstLetter(djDateLocale.getNames('months', 'abbr'));

			if (dateTemp < yesterdayStart || dateTemp >= tommorowEnd) {
				return date.getDate() + ' ' + months[date.getMonth()];
			} else {
				if (!recurring) {
					if (dateEndTemp) {
						if (dateEndTemp < todayStart) {
							return hi.i18n.getLocalization('calendar.yesterday');
						} else if (dateEndTemp <= todayEnd) {
							return hi.i18n.getLocalization('calendar.today');
						} else {
							if (dateStartTemp && dateStartTemp < todayEnd) {
								return hi.i18n.getLocalization('calendar.today');
							} else {
								return hi.i18n.getLocalization('calendar.tomorrow');
							}
						}
					} else {
						if (dateStartTemp < todayStart) {
							return hi.i18n.getLocalization('calendar.yesterday');
						} else if (dateStartTemp < todayEnd) {
							return hi.i18n.getLocalization('calendar.today');
						} else {
							return hi.i18n.getLocalization('calendar.tomorrow');
						}
					}
				} else {
					if (dateTemp < todayStart) {
						return hi.i18n.getLocalization('calendar.yesterday');
					} else if (dateTemp <= todayEnd) {
						return hi.i18n.getLocalization('calendar.today');
					} else {
						return hi.i18n.getLocalization('calendar.tomorrow');
					}
				}
			}
		}

		return '';
	}

	dojo.global.generateTaskHTML  = function(pdata, is_completed, group) {
		var id = pdata.id;

		var template_data = {
			id: 'task_' + id,
			color_class: '',
			shared_class: '',
			owned_class: '',
			admin_class: '',
			children_class: '',
			completed_class: '',
			completed_checked: '',
			type_class: '',
			task_title: '',
			person_class: '',
			person_title: '',
			person_name: '',
			person_name_class: 'hidden',
			project_title: '',
			project_name: '',
			project_name_class: 'hidden',
			drag_visible_class: '',
			starred_class: '',
			eventdate: (pdata.next_event_date ? pdata.next_event_date : ''),
			business_class: (Project.businessId ? 'business' : ''),
			date_title: getTaskDateTitle(pdata.id, new Date(), pdata)
		};

		// Completed task group
		if (is_completed) {
			template_data.completed_class = 'completed';
			template_data.completed_checked = 'checked="checked"';
		}

		if (pdata.user_id == Project.user_id) {
			template_data.owned_class = 'owned';
		}

		if (parseInt(pdata.starred)) {
			template_data.starred_class = 'star-selected';
		}

		if (Project.isBusinessAdministrator()) {
			template_data.admin_class = 'admin';
		}

		if (pdata.children && Project.hasNonFileChildren(pdata.id) != 0) {
			template_data.children_class = 'has-children';
		}

		if (pdata.shared == '1') {
			template_data.shared_class = 'shared';
		}

		if (parseInt(pdata.color)) {
			template_data.color_class = 'color' + pdata.color;
		}

		switch (parseInt(pdata.category)) {
			case 1:
				template_data.type_class = 'tasks';
				break;
			case 2: case 3:
				template_data.type_class = 'event';
				break;
			case 5:
				template_data.type_class = 'file';
				break;
			case 4:
				template_data.type_class = 'note';
				break;
		}

		template_data.task_title = htmlspecialchars(pdata.title);

		if (group == 'date') {
			template_data.drag_visible_class = 'hidden';
		}

		if (pdata.assign_user && group != 'team') {
			if (pdata.assign_user == Project.user_id && pdata.assign_user != pdata.user_id) {
				var pClass = 'from';
				var pId = pdata.user_id;
				var pTitle = hi.i18n.getLocalization('center.item_received_from') + ' ';
			} else {
				var pClass = 'to';
				var pId = pdata.assign_user;
				var pTitle = hi.i18n.getLocalization('center.item_assigned_to') + ' ';
			}
			var pName = Project.getFriendName(pId);
			if (pName) {
				template_data.person_class = pClass;
				template_data.person_title = pTitle + pName;
				template_data.person_name = pName + ' ';
				template_data.person_name_class = '';
			}
		}
		if (pdata.parent && group != 'project') {
			var pName = Project.get(pdata.parent, 'title');
			if (pName) {
				template_data.project_title = pName;
				template_data.project_name = ' ' + pName;
				template_data.project_name_class = '';
			}
		}

		return fillStringTemplate(TEMPLATES.task, template_data, ["completed_checked", "task_title", "business_visible"]);
	}


	var fillTasks = dojo.global.fillTasks = function(type) {
		type = type || '';
		//log('FT ' + type);
		var id;

		var res = Project.check_opened_task(type, function (){fillTasks(type)});
		if (res === false) return false;

		var isSearchActive = Hi_Search.isFormVisible();

		// include yet unsaved changes
		var pdata = Project.getData(true);
		var container = Project.getContainer('fillTasks');
		if (container == false) return;

		if (!Project.loaded) return;
		var type = 0;

		if (Project.groupPending) {
			Tabs.open('grouping', Project.groupPending);
			Project.groupPending = null;
			return;
		}

		if (Hi_Tag.filterPending) {
			var filterTagPening = Hi_Tag.filterPending;
			Hi_Tag.filterPending = false;
			Hi_Tag.filter(filterTagPening);
			return;
		}

		var group = Hi_Preferences.get('currentTab', 'my', 'string');
		var tagFilter = Hi_Preferences.get('tagfilter', '');

		if (!in_array(tagFilter, Hi_Tag.listArray())) {
			tagFilter = false;
			Hi_Preferences.set('tagfilter', '');
		}

		if (isSearchActive) {
			//Search resuts
			pdata = Hi_Search.getResults();
		}

		// show/hide new project btn
		if (group == 'project' && !isSearchActive) {
			dojo.removeClass('newProjectButton', 'hidden')
		} else {
			dojo.addClass('newProjectButton', 'hidden')
			dojo.addClass('project', 'hidden')
		}

		HiDragSourceCached.project = false;

		//Reset opened task
		Project.opening = false;
		Project.opened = false;

		var folder = [];
		var i;
		var sortProjects;

		var friendIds = {};
		var frlist = Project.friends;
		if (frlist) {
			var i, imax = frlist.length;
			for (i = 0; i < imax; i++) {
				if (frlist[i].status >= 10) {
					friendIds[frlist[i].id] = frlist[i];
				}
			}
		}
		switch (group) {
			case 'my':
				folder = [
					{id:1,name:hi.i18n.getLocalization('center.starred'),move_name:hi.i18n.getLocalization('center.starred'),color:'color3 starred',min:false,max:false}
				];

				break;
			case 'date':
				sortProjects = false;
				folder = [
					{id:-2,name:hi.i18n.getLocalization('center.overdue'),move_name:hi.i18n.getLocalization('center.overdue'),color:'day',min:false,max:-1},
					{id:0,name:hi.i18n.getLocalization('center.today'),move_name:hi.i18n.getLocalization('center.today'),color:'today',min:0,max:0},
					{id:1,name:hi.i18n.getLocalization('center.tomorrow'), move_name: hi.i18n.getLocalization('center.tomorrow'),color:'day',min:1,max:1},
					{id:7,name:hi.i18n.getLocalization('center.during_week'),color:'day',min:2,max:7},
					{id:14,name:hi.i18n.getLocalization('center.after_week'),color:'day',min:8,max:false}
				];
				break;
			case 'color':
				for (i in Project.lists.color) {
					folder.push({id: i, name: '', move_name: ' ', color: i});
				}
				break;
			case 'project':
				sortProjects = true;
				var i;
				for (i in pdata) {
					if (pdata[i].category == 0) {
						var shared = pdata[i].shared == '1';
						folder.push({id: i, name:pdata[i].title, color: 3, private_project: Project.businessId && !shared, public_project: shared});
					}
				}
				folder.sort(function(a,b){return parseInt(Hi_Preferences.get('ps_' + a.id)) > parseInt(Hi_Preferences.get('ps_' + b.id)) ? 1 : -1;});
				break;
			case 'team':
				if (!('friends' in Project)) {
					return;
				}
				folder.push({id: 0, name:hi.i18n.getLocalization('main.assigned_to_me'), color: 1});

				for (var i in friendIds) {
					if (friendIds[i].id != Project.user_id) {
						var friendName = Project.getFriendName(friendIds[i].id);
						folder.push({id: friendIds[i].id * 2 + 1, move_name: friendName, name:hi.i18n.getLocalization('main.assigned_to') +' ' + friendName, color: 2});
					}
				}

				var main = Hi_Preferences.get('main_assign_user');
				if (main) {
					folder.sort(function(b, a) {
						var a_id = Math.round((a.id - a.id % 2) / 2);
						var b_id = Math.round((b.id - b.id % 2) / 2);
						if (a_id == b_id) {
							var a_type = a.id % 2;
							var b_type = b.id % 2;
							return a_type < b_type;
						}
						if (a_id != main && b_id != main) {
							return 0;
						}
						if (a_id == main && b_id != main) {
							return 1;
						}
						if (b_id == main && a_id != main) {
							return -1;
						}
						return 0;
					});
				}
				break;
		}

		var t = [];
		var folderObj = {};
		var folderKey = {};
		var imax = folder.length;
		type = type || 0;

		var j, cont;
		var today = new Date();
		today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
		today = today.valueOf();
		for (i = 0; i < imax; i++) {
			folderObj[folder[i].id] = folder[i];
			folderKey[folder[i].id] = i;
			t[i] = [];
		}
		t[imax] = [];
		t[imax + 1] = [];
		switch (group) {
			case 'date':
				var daily, today_group, overdue_group;
				for (i = 0; i < imax; i++) {
					if (folder[i].id == 0) {
						today_group = i;
					} else if (folder[i].id == -2) {
						overdue_group = i;
					}
				}
				for (j in pdata) {
					var id = pdata[j].id;

					if (pdata[j].category == 0 || (type != 0 && pdata[j].category != type) || (tagFilter && dojo.indexOf(tagFilter, pdata[j].tag) == -1)) {
						continue;
					}

					if (Hi_Overdue.isOverdue(id))
					{
						cont = t[overdue_group];

						//Tasks which wen overdue today must be in todays group
						var overdue_time = hi.items.date.getEventEndTime(id, '23:59');
						if (!overdue_time) {
							var overdue_time = hi.items.date.getEventStartTime(id, '23:59');
						}

						if (overdue_time) {
							var d = new Date();
								d.setHours(0); d.setMinutes(0); d.setSeconds(0);

							overdue_time = overdue_time - d;

							if (overdue_time > 0) {
								cont = t[today_group];
							}
						}

					} else {
						var d_s = false, d_e = false;
						if (pdata[j].end_date && pdata[j].end_date != null && pdata[j].end_date != '') {
							d_e = str2time(pdata[j].end_date, 'y-m-d');
							d_e = d_e.valueOf();
						}
						if (pdata[j].start_date && pdata[j].start_date != null && pdata[j].start_date != '' && !parseInt(pdata[j].recurring)) {
							var item = HiTaskCalendar.nearestEvent(pdata[j]);
							if (item.startDate) {
								d_s = item.startDate.valueOf();
							} else {
								d_s = false;
							}
							if (item.endDate) {
								d_e = item.endDate.valueOf();
							} else {
								d_e = false;
							}
						}

						if (parseInt(pdata[j].recurring)) {
							var d_e = hi.items.date.getEventEndTime(id, '23:59');
							var d_s = hi.items.date.getEventStartTime(id, '23:59');

							if (d_e) d_e = d_e.getTime();
							if (d_s) d_s = d_s.getTime();

							var recurring_date = pdata[j].next_event_date;

							if (recurring_date) {
								var recurring_date = str2time(recurring_date, 'y-m-d');

								if (d_e) {
									d_e = recurring_date.getTime();
								} else {
									d_s = recurring_date.getTime();
								}
							}
						}

						if (d_s || d_e) {
							var dif_s = false, dif_e = false;
							if (d_s) {
								var dif_s = Math.round((d_s - today) / 86400000);
							}
							if (d_e) {
								var dif_e = Math.round((d_e - today) / 86400000);
							}
							if (dif_e !== false && dif_s !== false) {
								if (dif_e < 0 && dif_s < 0) {
									var dif = Math.max(dif_e, dif_s);
								} else if (dif_e > 0 && dif_s > 0) {
									var dif = Math.min(dif_e, dif_s);
								} else {
									var dif = dif_e;
								}
							} else if (dif_e !== false) {
								var dif = dif_e;
							} else if (dif_s !== false) {
								var dif = dif_s;
							}

							cont = false;
							for (i = 0; i < imax; i++) {
								if (folder[i].id == 100) {
									continue;
								}
								if ((folder[i].min === false || dif >= folder[i].min) && (folder[i].max === false || dif <= folder[i].max)) {
									cont = t[i];
									break;
								}
							}
						} else {
							cont = t[imax];
						}
					}
					if (!dojo.isArray(cont)) {
						cont = t[imax];
					}
					cont[cont.length] = j;
				}

				break;
			case 'color':
				for (j in pdata) {
					if (pdata[j].category == 0 || (type != 0 && pdata[j].category != type) || (tagFilter && dojo.indexOf(tagFilter, pdata[j].tag) == -1)) {
						continue;
					}
					var p_id = pdata[j].color;

					if (p_id in folderKey) {
						cont = t[folderKey[p_id]];
					} else {
						cont = t[imax];
					}
					if (!dojo.isArray(cont)) {
						cont = t[imax];
					}
					cont[cont.length] = pdata[j].id;
				}
				break;
			case 'project':
				for (j in pdata) {
					if (pdata[j].category == 0 || (type != 0 && pdata[j].category != type) || (!isSearchActive && tagFilter && dojo.indexOf(tagFilter, pdata[j].tag) == -1)) {
						continue;
					}
					var p_id = pdata[j].parent;

					if (p_id in folderKey) {
						cont = t[folderKey[p_id]];
					} else {
						cont = t[imax];
					}
					if (!dojo.isArray(cont)) {
						cont = t[imax];
					}
					cont[cont.length] = pdata[j].id;
				}
				break;
			case 'team':
				for (j in pdata) {
					if (pdata[j].category == 0 || (type != 0 && pdata[j].category != type) || (!isSearchActive && tagFilter && dojo.indexOf(tagFilter, pdata[j].tag) == -1)) {
						continue;
					}

					if (pdata[j].assign_user && pdata[j].assign_user != Project.user_id && (pdata[j].assign_user != Project.user_id || pdata[j].assign_user == pdata[j].user_id)) {
						cont = t[folderKey[pdata[j].assign_user * 2 + 1]];
					} else if (pdata[j].assign_user && pdata[j].assign_user == Project.user_id) {
						cont = t[folderKey[0]];
					} else {
						cont = t[imax];
					}
					if (!dojo.isArray(cont)) {
						cont = t[imax];
					}
					cont[cont.length] = pdata[j].id;
				}
				break;
			default:
				for (j in pdata) {
					if (pdata[j].category == 0 || (type != 0 && pdata[j].category != type) || (!isSearchActive && tagFilter && dojo.indexOf(tagFilter, pdata[j].tag) == -1)) {
						continue;
					}
					if (parseInt(pdata[j].starred)) {
						cont = t[folderKey[1]];
					} else if (pdata[j].completed == 1) {
						cont = t[imax + 1];
					} else
						var cont = t[1];
					if (!dojo.isArray(cont)) {
						cont = t[imax];
					}
					cont[cont.length] = pdata[j].id;
				}
		}


		if (!isSearchActive) { //If not search

			for (i = 0; i <= imax; i++) {
				t[i].sort(function(a,b){
					var p_1 = Hi_Preferences.get('ts_' + pdata[a].id, 0);
					var p_2 = Hi_Preferences.get('ts_' + pdata[b].id, 0);

					//In Projects view, complete tasks should be at the end of the list
					var compl_1 = ((group == 'project' || group == 'color' || group == 'team' || group == 'date') && (pdata[a].completed === 1 || pdata[a].completed == '1') ? true : false);
					var compl_2 = ((group == 'project' || group == 'color' || group == 'team' || group == 'date') && (pdata[b].completed === 1 || pdata[b].completed == '1') ? true : false);

					if (!compl_1 && compl_2) return -1;
					if (compl_1 && !compl_2) return 1;

					return parseInt((p_1 ? p_1 : 0)) > parseInt((p_2 ? p_2 : 0)) ? 1 : -1;
				});
			}

		}

		/*
		 * ---------------------------------------- GENERATE HTML --------------------------------------------------
		 */
		var template_data = {
			group_class: 'grouped_' + group,
			task_list: ''
		};

		var main = dojo.byId(container);
			main.innerHTML = '';	//faster than removing each node sepparate what removeNode does


		var projects = document.createElement('UL');
		projects.className = 'projects';
		projects.id = 'projects';
		main.appendChild(projects);

		dojo.addClass(projects, 'grouped_' + group);
		var task_container = document.createElement('UL');
		task_container.className = 'task_ul';
		var empty_project = task_container.cloneNode(false);
		dojo.addClass(empty_project, 'tasks');
		empty_project.id = 'no_project_tasks';
		var empty_div = document.createElement('DIV');
		empty_div.style.width = '100%';
		empty_div.appendChild(empty_project);

		//Set if completed items should be visible
		setCompletedListVisibility(empty_project, isCompletedTasksVisible(empty_project.id));

		var tmp;
		var projectHTML = '';
		for (i = 0; i < imax; i++) {
			var jmax = t[i].length;
			if (group == 'team' && jmax == 0) {
				continue;
			}

			var percentage = '';

			//Count completed tasks
			if (group == 'project') {
				var count = 0;
				var completed = 0;
				for(var folder_task in pdata) {
				    if (pdata[folder_task].parent == folder[i].id) {
				        count++;
				        if (pdata[folder_task].completed === '1' || pdata[folder_task].completed === 1) {
				            completed++;
				        }
				    }
				}

				if (count && completed) {
					var tmp = Math.floor(completed / count * 100);
					if (tmp) {
						percentage = tmp + '%';
					}
				}
			}

			var folder_description = (folder[i].id in pdata ? formatMessage(pdata[folder[i].id].message) : '');

			var html = fillStringTemplate(TEMPLATES.project, {
				id: folder[i].id,
				group: group,
				name: folder[i].name,
				move_name: folder[i].move_name,
				private_project_visible: folder[i].private_project ? '' : 'style="display: none"',
				percentage: percentage,
				percentage_visible: (percentage ? '' : 'display: none'),
				editable_visible: false,
				message: folder_description,
				message_visible: folder_description ? '' : 'hidden'
			}, ['private_project_visible']);

			projectHTML += html;

			template_data.task_list += html;
		}

		projects.innerHTML += projectHTML;
		projectHTML = '';

		for (i = 0; i <= imax + 1; i++) {
			var hideTasks = false;
			var jmax = t[i].length;
			if (i == imax) {
				if (group == 'activity') {
					empty_project = false;
				} else {
					project_tasks = empty_project;
					main.appendChild(empty_div);
				}
			} else if (i < imax) {
				if (group == 'team' && jmax == 0) {
					continue;
				}
				project = dojo.byId('project' + folder[i].id);


				if (group == 'date') {
					var ai = dojo.query('.ajax_inline', project)[0];
					ai.id = 'project' + folder[i].id + 'inline';
					ai.style.cursor = 'auto';
					ai.title = 'Click to edit';
					ai.onclick = function () {Click.setClickFunction(saveAllProjects, true, 'project')};
				}

				if (!isSearchActive && group != 'color' && group != 'my') {
					var header = dojo.query('table.header', project)[0];
					var opened = Hi_Preferences.get('p_' + group + '_' + folder[i].id, '2');
					if (opened == '2' && jmax == 0) {
						opened = '0';
					}
					if (opened == '0') {
						if (header) {
							hideTasks = true;
							dojo.removeClass(header, 'opened')
						}
					}
				}

				if (group == 'color') {
					dojo.addClass(project, 'no_title');
				} else {
					var cnt = dojo.query('img.cnt', project)[0];
					setItemCount(cnt, jmax);
				}

				if ('color' in folder[i] && folder[i].color != 0) {
					if (parseInt(folder[i].color) == folder[i].color) {
						var projectClass = 'color' + folder[i].color;
					} else {
						var projectClass = folder[i].color;
					}
				} else {
					var projectClass = 'color1';
				}
				dojo.addClass(project, projectClass);

				var project_tasks = dojo.query('div.tasks', project)[0];
				project_tasks.id = 'project_tasks' + folder[i].id;

				if (group != 'project') {
					var project_heading = getParentByClass(project_tasks, 'lb').getElementsByTagName('H1')[0];
						dojo.addClass(project_heading, 'uneditable');
				}

				if (hideTasks) {
					dojo.addClass(project_tasks, 'hidden')
				}
				project_tasks = dojo.query('div.lb', project_tasks)[0];

				var ul = task_container.cloneNode(true);
				ul.id = 'tasks' + folder[i].id;
				var project_tasks = project_tasks.insertBefore(ul, childNode(project_tasks, 0));

				//Set if completed items should be visible
				setCompletedListVisibility(ul, isCompletedTasksVisible('tasks' + folder[i].id));
			} else if (i == imax + 1) {
				if (jmax == 0) continue;
				group = 'my';
				var projectsCompleted = document.createElement('UL');
				projectsCompleted.className = 'projects';
				projectsCompleted.id = 'projects_completed';
				main.appendChild(projectsCompleted);

				var project = fillStringTemplate(TEMPLATES.project, {
					id: '_completed',
					group: 'my',
					name: hi.i18n.getLocalization('completed.title'),
					move_name: '',
					private_project_visible: 'style="display: none"'
				}, ['private_project_visible']);

				projectsCompleted.innerHTML += project;
				project = dojo.byId('project_completed');

				var header = dojo.query('table.header', project)[0];

				var project_heading = header.getElementsByTagName('H1')[0];
					dojo.addClass(project_heading, 'uneditable');

				var opened = Hi_Preferences.get('p_completed', '2');
				if (opened == '2') {
					opened = '0';
				}
				if (opened == '0') {
					if (header) {
						hideTasks = true;
						dojo.removeClass(header, 'opened')
					}
				}

				dojo.addClass(project, 'day');

				var cnt = dojo.query('div.cnt', project)[0];
				setItemCount(cnt, jmax);

				var project_tasks = dojo.query('div.tasks', project)[0];

				project_tasks.id = 'project_tasks_completed';
				if (hideTasks) {
					dojo.addClass(project_tasks, 'hidden')
				}
				project_tasks = dojo.query('div.lb', project_tasks)[0];

				var ul = task_container.cloneNode(true);
				ul.id = 'tasks_completed';
				var project_tasks = project_tasks.insertBefore(ul, childNode(project_tasks, 0));
			}

			var tasks_html = '';
			var template_data = {};

			var tasks_html = '';
			for (var j = 0; j < jmax; j++) {
				var is_completed = (pdata[t[i][j]].completed == '1' || pdata[t[i][j]].completed === 1);
				var is_completed = (i == imax + 1 || (group == 'my' && is_completed) || (group == 'project' && is_completed) || (group == 'date' && is_completed) || (group == 'color' && is_completed) || (group == 'team' && is_completed));

				if (parseInt(pdata[t[i][j]].recurring)) {

					var instance_date = pdata[t[i][j]].next_event_date;

					if (instance_date && Project.isEventComplete(pdata[t[i][j]].id, instance_date)) {
						is_completed = true;
					} else {
						is_completed = false;
					}
				}
				tasks_html += generateTaskHTML(pdata[t[i][j]], is_completed, group);

			}
			(function fillHtml(tasks_html, project_tasks) {
				project_tasks.innerHTML = tasks_html;
			})(tasks_html, project_tasks);
		}

		Project.opened = false;
		Project.openedInstance = false;
		Project.openEdit = false;

		refreshSortables();
		Hi_Overdue.reload();

		return true;
	}

	dojo.global.saveAllProjects = function() {
		var hids = dojo.dom.getChildElements(dojo.byId('projects'));
		for (var i = 0; i < hids.length; i++) {
			var hid = dojo.query('.project_inline_opened', hids[i]);
			if (!hid.length || dojo.hasClass(hid[0], 'hidden')) {
				continue;
			}
			var s = dojo.query('.save', hid[0]);
			if (s.length && typeof s.onclick == 'function') {
				s[0].onclick.apply(s);
			}
		}
	}

	dojo.global.createAlertElementWidget = function() {
		if (Project.alertElement) {
			return;
		}

		//Create widget
		Project.alertElement = dojo.parser.instantiate([dojo.byId('alertElement')])[0];
		if (!Project.alertElement) Project.alertElement = dijit.byId('alertElement');

		Project.alertElement.showItem = function(id, data) {
			data = data || {};
			if (id == 3 || id == 4 || id == 6) {
				hideElement(Project.alertElement.closeButton);
			} else {
				showElement(Project.alertElement.closeButton);
			}
			if (id == 6) {
				Project.ajax = function() {};
			}
			openTab('alertElement', id, 0, data);
			Project.alertElement.show();
		};

		Project.alertElement.closeButton = dojo.query(".close", "alertElement")[0];
		dojo.connect(Project.alertElement.closeButton, 'click', function () {
			Project.alertElement.hide();
		});
	}

	dojo.global.hi_init = function() {

		//Initialize hi.store
		hi.store.init();
		document.body.style.overflow = 'auto';

		Hi_Archive.getTaskList();

		createAlertElementWidget();
	}

	dojo.global.switchCategory  = function(tab_id) {
		if (Tabs.selected('category_type') != tab_id) {
			openNewItemTab('new_object', tab_id, 0)
		}
	}



	dojo.global.handleDD  = function(id) {
		var el = null;
		if (id) {
			el = dojo.byId('element_properties_' + id);
		} else {
			el = dojo.byId('newTaskBody');
		}

		var def_h = Hi_Preferences.get('description_input_height', 60);

		if (el) {
			var resize_handle = dojo.query('.textarea-resize', el)[0],
				is_drag = false,
				start_y = 0,
				delta_y = 0;

			var textarea = prevElement(resize_handle);
				textarea.style.height = def_h + 'px';

			dojo_on(resize_handle, 'mousedown', function (evt) {
				try {
				is_drag = true;
				start_y = evt.clientY;
				dojo.stopEvent(evt);
				} catch (e) {}
			}, true);
			dojo_on(document.body, 'mousemove', function (evt) {
				if (!is_drag) return;
				dojo.stopEvent(evt);

				delta_y = evt.clientY - start_y;
				start_y = evt.clientY;

				if (delta_y) {
					try {
						textarea.style.height = (parseInt(textarea.style.height) || def_h) + delta_y + 'px';
					} catch (e) {}
				}
			});
			dojo_on(document.body, 'mouseup', function (evt) {
				try {
				if (!is_drag) return;
				Hi_Preferences.set('description_input_height', parseInt(textarea.style.height));
				Hi_Preferences.send();
				is_drag = false;
				} catch (e) {}
			});
		}
	}

	dojo.global.newItemDDInitialized = false;

	dojo.global.openNewTab = function(def) {
		if (Project.newItemOpened == '-2'){
			if (dojo.byId('newItem_title').value != '' && dojo.byId('newItem_title').value != def) {
				dojo.byId('newItem_add').disabled = false;

				dojo.byId('newItem_color_container').style.display = 'block';

				if (!newItemDDInitialized) {
					newItemDDInitialized = true;
					handleDD();
				}

				var star = dojo.byId('newItem_starred');
					star.value = '0';
					dojo.removeClass(star.parentNode, 'star-selected');

				//If not current date is selected in calendar, then fill start date with it
				if (HiTaskCalendar.getCurrentDateStr() != HiTaskCalendar.getTodayDateStr()) {
					dojo.byId('newItem_start_date_1').value = time2str(HiTaskCalendar.currentDate, Hi_Calendar.format);
					dojo.byId('newItem_recurring_1').disabled = false;
				}

				dojo.addClass('newItem_wrapper', 'new-highlight');
				//dojo.removeClass('newItem_cancel', 'invisible');
				//resetForm();
				fillAssignList();
				Project.newItemOpened = 1;
				openTab('new_object', '1');
				Tabs.open('category_type', "1");

				//Reset tags list
				Hi_Tag.fillMoreTags(null, true);

					//Show more tags link
						//showElement(dojo.query('.add_tag', dojo.byId('newItem_tag').parentNode)[0]);
						//Handled in fillMoreTags

				var eff1 = dojo_fx.wipeIn({node: dojo.byId('newTaskBody'), duration: CONSTANTS.duration});
				dojo.connect(eff1, 'onEnd', function() {Project.newItemOpened=2;openNewTab(def); dojo.byId('newTaskBody').style.height='auto'});
				eff1.play();
			}
		} else if (Project.newItemOpened == 2) {
			if (dojo.byId('newItem_title').value == '' || dojo.byId('newItem_title').value == def) {
				dojo.byId('newItem_add').disabled = true;
				//dojo.addClass('newItem_cancel', 'invisible');

				Project.newItemOpened = -1;
				var eff1 = dojo_fx.wipeOut({node: dojo.byId('newTaskBody'), duration: CONSTANTS.duration});

				dojo.connect(eff1, 'onEnd', function() {Project.newItemOpened=-2;openNewTab(def)});

				setTimeout(function () {
					dojo.removeClass('newItem_wrapper', 'new-highlight');
				}, CONSTANTS.duration);

				eff1.play();
				noclose = false;
				Hi_Calendar.closeCalendars();
				Tooltip.close('color_picker');
			}
		}
	}

	dojo.global.resetForm = function() {
		var els = ['newItem_start_date', 'newItem_start_time', 'newItem_end_date', 'newItem_end_time', 'newItem_recurring', 'newItem_message', 'newItem_reminder_enabled', 'newItem_reminder_time', 'newItem_reminder_time_type', 'newItem_recurring_cont', 'newItem_reminder_cont', 'newItem_tagInput', 'newItem_shared', 'newItem_private', 'newItem_assign'];
		var i, imax = els.length;
		var el;
		var dateExp = Hi_Calendar.makeDateExp();

		newItemChooseColor();
		for (i = 0; i < imax; i++) {
			for (j = 0; j <= 5; j++) {
				el = dojo.byId(els[i] + (j == 0 ? '' : '_' + j));
				if (!el) {
					el = dojo.byId(els[i] + (j == 0 ? '_hidden' : '_hidden_' + j));
				}
				if (!el) continue;
				if (el.type == 'text' || el.type == 'hidden')
					el.value = el.defaultValue;
				if (el.type == 'checkbox')
					el.checked = el.defaultChecked;
				if (el.type == 'radio')
					el.checked = el.defaultChecked;
				if (el.tagName == 'SELECT') {
					var opt = el.options;
					var optNum = opt.length;
					var k;
					for (k = 0; k < optNum; k++) {
						opt[k].selected = opt[k].defaultSelected;
					}
				} else
				if (el.tagName == 'TEXTAREA') {
					el.value = el.defaultValue;
					el.onblur();
				} else
				if (els[i].indexOf('reminder_time') != -1 && el.type == 'text') {
					el.onkeypress = function($e) {
						var e = $e || window.event;
						return permitExpression(e, e.srcElement||e.target, /^\d{0,2}$/);
					};
				} else
				if (els[i].indexOf('date') != -1 && el.type == 'text') {
					el.onkeypress = function($e) {
						var e = $e || window.event;
						return permitExpression(e, e.srcElement||e.target, new RegExp(dateExp));
					};
				} else
				if (els[i].indexOf('time') != -1 && el.type == 'text') {
					el.onkeypress = function($e) {
						var e = $e || window.event;
						return permitExpression(e, e.srcElement||e.target, HiTaskCalendar.timeRegExp());
					};
				} else
				if (els[i].indexOf('recurring_cont') != -1) {
					disableElements(el);
				} else
				if (els[i].indexOf('reminder_cont') != -1) {
					disableElements(el);
				}
			}
		}
		setValue('newItem_tag');

		Tabs.open('category_type', '1');

		var tagFilter = Hi_Preferences.get('tagfilter', '');
		if (tagFilter) {
			Hi_Tag.addTag('newItem_tag', tagFilter);
		}

		delete fillAssignList.selected[0];
	}

	/**
	 * Returns true if project completed tasks are visible, otherwise false
	 */
	dojo.global.isCompletedTasksVisible = function(project_id) {
		if (typeof isCompletedTasksVisible.visibilityData[project_id] == 'undefined') {
			isCompletedTasksVisible.visibilityData[project_id] = false;
		}

		return isCompletedTasksVisible.visibilityData[project_id];
	}
	/**
	 * Set if project completed tasks are visible
	 */
	dojo.global.setCompletedTasksVisible = function(project_id, is_visible) {
		isCompletedTasksVisible.visibilityData[project_id] = is_visible;
	}

	dojo.global.isCompletedTasksVisible.visibilityData = {};


	dojo.global.setCompletedListVisibility = function(ul, visible) {
		if (visible) {
			dojo.removeClass(ul, 'completed-hidden');
		} else {
			dojo.addClass(ul, 'completed-hidden');
		}

		setCompletedTasksVisible(ul.id, visible);
	}
	dojo.global.toggleCompletedListVisibility = function(el) {
		var ul = dojo.dom.getAncestorsByTag(el, 'UL')[0];
		if (isCompletedTasksVisible(ul.id)) {
			setCompletedListVisibility(ul, false);
		} else {
			setCompletedListVisibility(ul, true);
		}
	}

	/**
	 * Update "And X completed items..." text and show/hide this message
	 */
	dojo.global.updateCompletedListTitle = function(el, completed_count) {
		var div = el.getElementsByTagName('DIV')[0];

		if (completed_count == 0) {
			dojo.addClass(el, 'hidden');
		} else {
			dojo.removeClass(el, 'hidden');
			var it = '';

			if (completed_count == 1) {
				it = '<span class="t-1">' + hi.i18n.getLocalization('center.completed_item_count', '') + '</span>' +
					 '<span class="t-2">' + hi.i18n.getLocalization('center.completed_item', '') + '</span>';
			} else {
				it = '<span class="t-1">' + hi.i18n.getLocalization('center.completed_items_count', '') + '</span>' +
					 '<span class="t-2">' + hi.i18n.getLocalization('center.completed_items', '') + '</span>';
			}

			div.innerHTML = it.replace("%s", completed_count);
		}
	}

	dojo.global.checkLists = function(a) {
		a = a || false;
		var group = Hi_Preferences.get('currentTab', 'my', 'string');
		var lists = dojo.query('.task_ul', dojo.byId('projects'));

		if (group == 'team') {
			var i, imax = lists.length;
			var listsn = [];
			for (i = 0; i < imax; i++) {
				var id = parseIntRight(lists[i].id);
				if (id % 2 || id == 0) {
					listsn.push(lists[i]);
				}
			}
			lists = listsn;
		}

		var i;
		var imax = lists.length;

		for (i = -1; i <= imax; i++) {
			var j = 0, c_completed = 0;
			if (i == imax) {
				x = dojo.byId('tasks_completed');
			} else if (i == -1) {
				x = dojo.byId('no_project_tasks');
			} else {
				var x = lists[i];
			}
			if (!x) continue;

			var tmp = firstElement(x);
			var tmp_last = lastElement(x);
			var tmp_prev = tmp;

			var l = 0,
				completedTask = null;

			if (tmp) {
				l = 1;

				if (dojo.hasClass(tmp, 'completed')) {
					c_completed++;
					completedTask = tmp;
				} else if (dojo.hasClass(tmp, 'task_completed')) {
					l--;
				}

				while (tmp && tmp != tmp_last) {
					l++;
					tmp_prev = tmp;
					tmp = nextElement(tmp);

					if (!tmp) continue;
					if (dojo.hasClass(tmp, 'task_completed')) {
						//"Completed items:" label
						l--;
					} else if (dojo.hasClass(tmp, 'completed')) {
						c_completed++;
						completedTask = completedTask || tmp;
					} else if (completedTask) {
						//task which is not completed is after completed task in the list
						//move it infront of all completed tasks
						if (tmp !== completedTask) {
							l--;
							insertBefore(tmp, completedTask);
							if (tmp_prev) tmp = tmp_prev;
						}
					}
				}
			}

			var c = l;
			if (i != imax) {
				if (l == 0) {

					var newElement = document.createElement('LI');
					var newElementDiv = document.createElement('DIV');
					newElement.appendChild(newElementDiv);
					newElement.className = 'task_empty dojoDndItem';
					if (!a) {
						var it = '';
						if (i == -1 && group != 'my' && imax > 0) {
							it = '';
						} else if (group == 'color') {
							it = hi.i18n.getLocalization('center.drag_items_here');
						} else {
							it = hi.i18n.getLocalization('center.no_items');
						}
						innerText(newElementDiv, it);
					} else {
						innerText(newElementDiv, hi.i18n.getLocalization('center.drag_items_here'));
					}
					x.appendChild(newElement);
				} else if (l == 2) {
					var j = 0;
					for (j = 0; j < 2; j++) {
						if (x.childNodes[j] && dojo.hasClass(x.childNodes[j], 'task_empty')) {
							x.removeChild(x.childNodes[j]);
							c--;
						}
					}
				} else if (l == 1) {
					var tmpElement = firstElement(x);
					var tmpElement2 = lastElement(x);
					if (tmpElement2 && dojo.hasClass(tmpElement2, 'task_empty')) {
						tmpElement = tmpElement2;
					}
					if (tmpElement && dojo.hasClass(tmpElement, 'task_empty')) {
						var newElement = tmpElement;
						var newElementDiv = firstElement(newElement);
						if (!a) {
							innerText(newElementDiv, i == -1 && group != 'my' ? '' : group == 'color' ? hi.i18n.getLocalization('center.drag_items_here') : hi.i18n.getLocalization('center.no_items'));
						} else {
							innerText(newElementDiv, hi.i18n.getLocalization('center.drag_items_here'))
						}
						c = 0;
					}
				}

				//And X completed items...
				var c_titles = dojo.query('li.task_completed', x);

				if (!c_titles.length) {
					var newElement = document.createElement('LI');
					var newElementDiv = document.createElement('DIV');
					newElement.appendChild(newElementDiv);
					newElement.className = 'task_completed';
					x.appendChild(newElement);
					c_titles.push(newElement);

					dojo_on(newElementDiv, 'click', function (ev) {
						var ev = ev || window.event;
						toggleCompletedListVisibility(ev.target);
					});
				}

				if (c_titles.length) {
					updateCompletedListTitle(c_titles[0], c_completed);

					if (completedTask) {
						insertBefore(c_titles[0], completedTask);
					}
				}
			}
			if (i != -1) {
				var cntId = 'cnt';
				if (i == imax) {
					cntId += '_completed';
					if (c == 0) {
						var el = dojo.byId('projects_completed');
						if (el) {
							removeNode(el);
						}
						return;
					}
				} else {
					cntId += parseIntRight(x.id);
				}
				setItemCount(dojo.byId(cntId), c);
				setProjectPercentage(lists[i], c, c_completed);
			}
		}
	}
	//Switching tasks type in the top of main section
	dojo.global.switchType = function(n) {
		Project.filterPending = null;

		if (!fillTasks()) {
			return false;
		}

		if (dojo.hasClass('newItem_title', 'empty')) {
			dojo.byId('newItem_title').value = Project.getNewTaskText();
		}

		return true;
	}

	dojo.global.toggleElement = function(a) {
		a = dojo.byId(a);
		if (a.style.display == 'none' || dojo.hasClass(a, 'hidden')) {
			a.style.display = '';
			dojo.removeClass(a, 'hidden');
		} else {
			a.style.display = 'none';
			dojo.addClass(a, 'hidden');
		}
	}

	dojo.global.toggleProject = function(el) {
		HiDragSourceCached.project = false;
		var btn = el;

		btn = getParentByClass(btn, 'header');
		el = getParentByClass(btn, 'project');

		var project_id = parseInt(el.id.replace(/project([0-9])/, '$1'));

		if (dojo.hasClass(el, 'starred')) {
			return;
		}

		var id, key;
		if (el.id == 'project_completed') {
			id = '_completed';
			key = 'p_completed';
		} else {
			id = parseIntRight(el.id);
			key = 'p_' + Hi_Preferences.get('currentTab', 'my', 'string') + '_' + id;
		}

		var tasks = dojo.byId('project_tasks' + id);
		var descr = dojo.byId('project_description' + id);

		if (dojo.hasClass(tasks, 'hidden')) {

			Hi_Archive.getChildrenList(project_id);

			dojo.removeClass(tasks, 'hidden');
			dojo.addClass(btn, 'opened');

			if (descr.innerHTML !== '') {
				dojo.removeClass(descr, 'hidden');
			}

		} else {
			dojo.addClass(tasks, 'hidden');
			dojo.removeClass(btn, 'opened');

			if (descr.innerHTML !== '') {
				dojo.addClass(descr, 'hidden');
			}
		}

		dojo.forEach(dojo.query('.toggle > span', btn), function(el) {
			dojo.toggleClass(el, 'hidden');
		});
	}

	//Item are beeing sorted
	dojo.global.isSorting = false;

	dojo.global.processPropertiesClick = function(t) {



		if (isSorting) return;

		var task = getParentByClass(t, 'task_li');

		var id = task.id;

		id = parseIntRight(id);

		Project.openedInstance = getEventDateByNode(task);

		if (id == Project.opened) {

			return false;

		}

		if (task.id.charAt(5) == 'N') {
			var task2 = dojo.byId('task_' + id);
			task2.id = task.id;
			task.id = 'task_' + id;
		}


		processProperties(id);
	}

	dojo.global.hidePropertiesIfNotEditing = function(id, options) {
		//#665 Item should not save+close when mouse clicked anywhere outside edit form

		if (Project.opened && Project.openEdit) {
			return;
		} else {

			processProperties(id, options);
		}
	}

	dojo.global.hidePropertiesDocumentClick = function(id, options) {
		processProperties(id, options);
	}

	dojo.global.processProperties = function(id, options) {

		try {
			if (!Project.opening) {
				Project.opening = id || false;
			}

			if (Project.opening == Project.opened) {
				Project.opening = false;

				return false;
			}

			options = dojo.mixin({afterHide: function(){}, save: true}, options||{})

			//temporary item
			if (Project.opened == -1) {
				options.tmpsave = true;
			}

			if (Project.dragged) {
				Project.dragged = false;
				Project.opening = false;
				return false;
			}

			if (Project.propOpenProc) {
				return false;
			}

			if (Project.opened) {
				hideProperties(Project.opened, options);
			} else if (Project.opening) {
				showProperties(Project.opening, options);
			}

			Hi_Archive.getChildrenList(id, true);

			return true;
		}
		catch(exc) {

			console.error(exc);
		}
	}

	dojo.global.createViewProperties = function(id, options) {
		if (!id) {
			return;
		}

		var options = options || {};
		var el = dojo.byId('task_' + id);
		if (!el) {
			return;
		}

		var ex = dojo.byId('element_properties_' + id);
		var n = ex;
		var t = Project.get(id, 'category');

		if (ex) {
			removeNode(ex);
			ex = false;
		}
		if (!ex) {
			var view = dojo.byId('properties_view').cloneNode(true);
			view.id = 'properties_view_' + id;
			var n = dojo.byId('properties_container_' + t).cloneNode(true);
			n.style.display = 'none';
			n.className = 'properties_view_container';
			n.id = 'element_properties_' + id;
			var sibling = dojo.query('.opened', el)[0];
			n = sibling.parentNode.appendChild(n);
			var btns = dojo.byId('process_properties_buttons').cloneNode(true);

			btns.id = null;
			if (options.deleteOnCancel) {
				dojo.addClass(btns, 'deleteOnCancel');
			}
			var opEl = dojo.query('.opened', n)[0];
			opEl.appendChild(btns);
			view = opEl.parentNode.appendChild(view);
		} else {
			var n = ex;
			var opEl = dojo.query('.opened', n)[0];
		}

		//Add file upload functionality
		if (FileUpload.listEnabled && Project.get(id, 'category') != 5) {
			FileUpload.reset(id);
		}

		n.style.display = 'none';

		var assign = parseInt(Project.get(id, 'assign_user'));
		var className = false;
		if (assign == Project.user_id) className = 'assigned_from';
			else if (assign) className = 'assigned_to';
		dojo.removeClass(opEl, 'assigned_to');
		dojo.removeClass(opEl, 'assigned_from');

		if (className) dojo.addClass(opEl, className);
		if (className == 'assigned_from') {
			if (Project.get(id, 'user_id') != Project.user_id) {
				var frName = Project.getFriend(Project.get(id, 'user_id')).name;
				var del = dojo.query('.prop_assfrom_delete', n);
				if (del.length) {
					del[0].innerHTML = hi.i18n.getLocalization('main.send_back_to') + ' ' + frName;
				}
			}
		}

		/*
		if (className == 'assigned_to') {
			dojo.query('.color_icon', n)[0].title = '';
		}
		*/

		var els = dojo.query('input, textarea, select', n);
		var i, imax = els.length;
		for (i = 0; i < imax; i++) {
			els[i].disabled = false;
		}

		return n;
	}

	//Saving task properties
	dojo.global.saveProperties = function(id, options) {
		var title = getValue('title_' + id, '');
		if (title.trim() == '') {
			return 'TITLE_EMPTY';
		}

		dojo.byId('message_' + id).onkeyup();
		var desc = getValue('message_' + id, '');
		if (dojo.byId('recurring_' + id) && !dojo.byId('recurring_' + id).disabled) {
			var recurring = getValue('recurring_' + id, '0');
		} else {
			var recurring = '0';
		}
		var reminder_time = getValue('reminder_time_' + id);
		var reminder_time_type = getValue('reminder_time_type_' + id);
		var reminder_enabled = getValue('reminder_enabled_' + id) ? 1 : 0;
		var shared = getValue('shared_' + id) ? 1 : 0;
		var start_time = getValue('start_time_' + id, '');
		var end_time = getValue('end_time_' + id, '');
		var start_date = getValue('start_date_' + id, '');
		var end_date = getValue('end_date_' + id, '');
		var color = getValue('color_' + id, '');
		var tagArray = Hi_Tag.getCurrItemTags(id);
		var category = getValue('category_2_' + id, '') || getValue('category_1_' + id, '') || getValue('category_4_' + id, '') || Project.get(id, 'category');;
		var starred = getValue('starred_' + id, '0');

		tagArray.sort();

		var newData = {
			id: id,
			title: title,
			message: desc,
			start_date: start_date,
			end_date: end_date,
			start_time: start_time,
			end_time: end_time,
			recurring: recurring,
			color: color,
			shared: shared,
			reminder_enabled: reminder_enabled,
			reminder_time: reminder_time,
			reminder_time_type: reminder_time_type,
			tag: tagArray,
			category: category,
			starred: starred
		};

		if (options && options.tmpsave) {
			var assign = getValue('assign_' + id, -1);

			if (assign != -1 && assign != '') {
				//Send also assign data during tnew
				newData['assign'] = '{"' + assign + '"}';
				newData['assign_user'] = assign;
			}
		}

		var res = Project.change(newData, options);

		if (!options || !options.tmpsave) {
			// assign/ unassign
			var assign = getValue('assign_' + id, -1);
			if (assign != -1 && assign != Project.get(id, 'assign_user')) {
				if (assign == '') {
					Project.unassign(id, {after: function(){}});
				} else {
					Project.assign(id, assign, null, {after: function(){}});
				}
			}
		}

		return res;
	}

	var fillViewProperties = dojo.global.fillViewProperties = function(id, el, options) {

		options = options || {};
		var fill = {};
		var i;
		var str_time = '';

		for (i in Project.data[id]) {
			fill[i] = Project.get(id, i);
		}
		fill.start_date = Project.get(id, 'start_date');
		fill.end_date = Project.get(id, 'end_date');
		fill.recurring = parseInt(fill.recurring);

		if (fill.start_time) {
			fill.start_time = HiTaskCalendar.formatTime(fill.start_time);
			str_time = hi.i18n.getLocalization('properties.datetime_time').replace(/\%time/, fill.start_time);
		}
		if (fill.end_time) {
			fill.end_time = HiTaskCalendar.formatTime(fill.end_time);
		}

		fill.has_time = false;

		if (fill.start_date) {
			fill.has_time = true;
			fill.time = fill.start_date;

			if (fill.recurring) {
				var date_str = '';

				switch(fill.recurring) {
					case 4:
						date_str = hi.i18n.getLocalization('properties.datetime_yearly'); break;
					case 3:
						date_str = hi.i18n.getLocalization('properties.datetime_monthly'); break;
					case 2:
						date_str = hi.i18n.getLocalization('properties.datetime_weekly'); break;
					case 1:
						date_str = hi.i18n.getLocalization('properties.datetime_daily'); break;
				}

				var start_date = str2time(fill.start_date, Hi_Calendar.format);

				var str_date = start_date.getDate();
				var str_month = HiTaskCalendar.months[start_date.getMonth()];
				var str_day = HiTaskCalendar.days[start_date.getDay()];

				date_str = date_str.replace(/\%time/, str_time);
				date_str = date_str.replace(/\%date/, str_date);
				date_str = date_str.replace(/\%day/, str_day);
				date_str = date_str.replace(/\%month/, str_month);
				date_str = date_str.replace(/\%recurrence_end_date/, time2str(str2timeAPI(fill.recurring_end_date), Hi_Preferences.get('date_format')));

				fill.time = date_str;
			} else {
				if (fill.start_time) {
					fill.time += ' ' + fill.start_time;
				}
				var end_time = '';
				if (fill.end_date) {
					if (fill.end_date != fill.start_date) {
						end_time = fill.end_date;
					} else {
						fill.end_date = '';
					}
					if (fill.end_time) {
						end_time += ' ' + fill.end_time;
					}
				}
				if (end_time != '') {
					fill.hyphen = true;
					fill.time += ' - ' + end_time;
				}
			}
		}

		if (typeof fill.message == 'string' && fill.message.trim()) {
			fill.has_message = 1;
			fill.message = formatMessage(fill.message);
		} else {
			if (options.allowEmptyMessage) {
				fill.has_message = 1;
			} else {
				fill.has_message = 0;
			}
		}
		fill.delete_completed = Project.user_id == fill.user_id && !parseInt(fill.assign_user) && parseInt(fill.completed) ? 1 : 0;
		// recurrence name
		var r;
		if (fill.category == 5) {
			r = '';
		} else {
			r = parseInt(fill.recurring);
		}
		if (r) {
			r = Project.recurringName(r);
		} else {
			r = '';
		}

		// Category 5 is a file
		fill.file_upload_allowed = FileUpload.enabled && Project.get(id, 'category') != 5;
		fill.file_list_allowed = FileUpload.listEnabled && Project.get(id, 'category') != 5;
		fill.is_file = Project.get(id, 'category') == 5;

		fill.has_tags = dojo.isArray(fill.tag) && fill.tag.length > 0;

		fill.hasStartDate = fill.start_date ? true : false;
		fill.hasStartDateTime = fill.hasStartDate && fill.start_time;
		fill.recurring_name = r;

		if (fill.assign_user) {
			fill.assigned = 1;

			if(fill.assign_user == Project.user_id) {
				fill.assigned_from = 1;
			} else {
				fill.assigned_to = 1;
			}
		} else {
			fill.assigned = 0;
		}

		if (fill.time_last_update) {
			//String with time difference: 3 minutes ago
			fill.time_last_update_dif = Hi_Comments.Date.timeDifString(fill.time_last_update, false);

			var t = Project.data[id].time_last_update;
				t = t.match(/(\d{4}.\d{2}.\d{2})\s(\d{1,2}:\d{1,2})/);

			if (t) {
				t[1] = str2time(t[1], 'y-m-d');
				t[1] = time2str(t[1], Hi_Calendar.format);
				fill.time_last_update = t[1] + ' ' + HiTaskCalendar.formatTime(t[2]);
			} else {
				fill.time_last_update = '';
			}
		}

		if (fill.user_id != Project.user_id) {
			fill.owner_name = Project.getFriendName(fill.user_id);
		} else {
			fill.owner_name  = '';
		}

		fillTemplate(el, fill);
	}

	dojo.global.getTaskNode  = function(id) {
	     return dojo.byId('task_' + id);
	}

	dojo.global.showProperties = function(id, options) {
		if (!id) {
			return;
		}
		//try {

		el = getTaskNode(id);

		var check = el;
		while (check) {
			if (!dojo.dom.isDisplayed(check)) {
				Project.opening = false;
				return;
			}
			check = check.parentNode;
		}

		if (!el) {
			Project.opening = false;
			return;
		};

		Project.opened = id;
		Project.opening = false;

		var t = Project.get(id, 'category');
		t = 1;
		var ex = dojo.byId('element_properties_' + id);


		if (ex) {
			removeNode(ex);
			ex = false;
		}
		if (!ex) {

			var view = dojo.byId('properties_view').cloneNode(true);
			view.id = 'properties_view_' + id;
			var n = dojo.byId('properties_container_' + t).cloneNode(true);
			n.style.display = 'none';
			n.id = 'element_properties_' + id;
			var sibling = dojo.query('.opened', el)[0];

			n = sibling.parentNode.appendChild(n);

			var btns = dojo.byId('process_properties_buttons').cloneNode(true);

			btns.id = null;
			if (options.deleteOnCancel) {
				dojo.addClass(btns, 'deleteOnCancel');
			}
			var opEl = dojo.query('.opened', n)[0];
			opEl.appendChild(btns);
			view = opEl.parentNode.appendChild(view);
		} else {
			var n = ex;
			var opEl = dojo.query('.opened', n)[0];
		}

		//Add file upload functionality
		if (FileUpload.listEnabled && Project.get(id, 'category') != 5) {
			FileUpload.reset(id);
		}

		n.style.display = 'none';

		var assign = parseInt(Project.get(id, 'assign_user'));
		var className = false;
		if (assign == Project.user_id) className = 'assigned_from';
			else if (assign) className = 'assigned_to';

		dojo.removeClass(opEl, 'assigned_to');
		dojo.removeClass(opEl, 'assigned_from');

		if (className) dojo.addClass(opEl, className);
		if (className == 'assigned_from') {
			if (Project.get(id, 'user_id') != Project.user_id && Project.get(id, 'assignee') != Project.user_id && !Project.isBusinessAdministrator()) {
				alert('You must be owner of this item or assignee of this item or team owner/administrator.');
				return false;
				var frName = Project.getFriend(Project.get(id, 'user_id')).name;
				var del = dojo.query('.prop_assfrom_delete', n);
				if (del.length) {
					del[0].innerHTML = hi.i18n.getLocalization('main.send_back_to') + ' ' + frName;
				}
			}
		}

		var els = dojo.query('input, textarea, select', n);
		var i, imax = els.length;
		for (i = 0; i < imax; i++) {
			els[i].disabled = false;
		}

		fillViewProperties(id, n);

		var els = n.getElementsByTagName('INPUT');
		var i, imax = els.length;
		var dateExp = Hi_Calendar.makeDateExp();
		for (i = 0; i < imax; i++) {

			if (els[i].name != '') {
				var prop = els[i].name;
				if (!ex) {
					if (els[i].type != 'radio') {
						els[i].id = prop + '_' + id;
					}
				}
				var sel_v = Project.get(id, prop);
				if (sel_v !== false) {
					if (sel_v) {
						if (els[i].type == 'checkbox') {
							els[i].checked = (sel_v == '1');
						} else if (els[i].type == 'radio') {
							els[i].checked = (sel_v == els[i].value);
						} else {
							if (els[i].id.indexOf('time') != -1 && els[i].id.indexOf('reminder_time') == -1) {
								els[i].value = HiTaskCalendar.formatTime(sel_v);
							} else {
								els[i].value = sel_v;
							}
						}
					} else {
						if (els[i].type == 'checkbox' || els[i].type == 'radio') {
							els[i].checked = els[i].defaultChecked;
						} else {
							els[i].value = els[i].defaultValue;
						}
					}
				}
			}
			if (els[i].id.indexOf('reminder_time') != -1) {
				if (!ex) els[i].onkeypress= function($e) {
					var e = $e || window.event;
					return permitExpression(e, e.srcElement||e.target, /^\d{0,2}$/);
				};
			} else
			if (els[i].id.indexOf('date') != -1) {
				if (!ex) els[i].onkeypress= function($e) {
					var e = $e || window.event;
					return permitExpression(e, e.srcElement||e.target, new RegExp(dateExp));
				};
			} else
			if (els[i].id.indexOf('time') != -1) {
				if (!ex) els[i].onkeypress = function($e) {
					var e = $e || window.event;
					return permitExpression(e, e.srcElement||e.target, HiTaskCalendar.timeRegExp());
				};
			}
		}

		var els = n.getElementsByTagName('SELECT');
		var i, imax = els.length;
		for (i = 0; i < imax; i++) {
			if (els[i].id != '') {
				var l = els[i].id.length;
				var prop = els[i].name;
				if (!ex) els[i].id = prop + '_' + id;
				var sel_v = Project.get(id, prop);
				if (sel_v) {
					els[i].value = sel_v;
				} else {
					els[i].selected = els[i].defaultSelected;
				}
			}
		}

		var els = n.getElementsByTagName('TEXTAREA');
		var i, imax = els.length;
		for (i = 0; i < imax; i++) {
			if (els[i].id != '') {
				var l = els[i].id.length;
				var prop = els[i].name;
				if (!ex) els[i].id = prop + '_' + id;
				var sel_v = Project.get(id, prop);
				if (sel_v) {
					els[i].value = sel_v;
				} else {
					els[i].value = els[i].defaultValue;
				}
			}
		}

		var els = n.getElementsByTagName('LABEL');
		var i, imax = els.length;
		for (i = 0; i < imax; i++) {
			if (els[i].htmlFor != '') {
				var l = els[i].htmlFor.length;
				var prop = els[i].htmlFor;
				if (!ex && dojo.byId(prop + '_' + id)) els[i].htmlFor = prop + '_' + id;
			}
		}
		var els = n.getElementsByTagName('DIV');
		var i, imax = els.length;
		for (i = 0; i < imax; i++) {
			if (els[i].id != '') {
				var l = els[i].id.length;
				var prop = els[i].id.substr(0, l - 1);
				if (!ex) els[i].id = prop + '_' + id;
			}
		}


		var inp = dojo.query('INPUT, TEXTAREA', el);
		for(var z=0; z<inp.length; z++) {
			dojo.connect(inp[z], "onselectstart", function (evt) {
				if (evt.stopPropagation) evt.stopPropagation();
				evt.cancelBuble = true;
			});
		}


		//Fill type inputs
		function onCategoryChange () {
			var type_val = parseIntRight(this.className);
			var td = dojo.dom.getAncestorsByTag(this, 'TD', true);
			var table = dojo.dom.getAncestorsByTag(this, 'TABLE', true);
			var tds = table.getElementsByTagName('TD');

			for(var i=0, j=tds.length; i<j; i++) {
				dojo.removeClass(tds[i], 'active');
			}

			dojo.byId('category_' + id).value = type_val;
			dojo.addClass(td, 'active');

			var p = getParentByClass(this, 'opened');
			var elm1 = dojo.query('div.period', p)[0];

			if (type_val == 4) {
				dojo.addClass(elm1, 'hidden');

				//Reset time options to defaults
				dojo.byId('start_date_' + id).value = '';
				dojo.byId('start_time_' + id).value = '';
				dojo.byId('end_date_' + id).value = '';
				dojo.byId('end_time_' + id).value = '';
				dojo.byId('reminder_enabled_' + id).checked = false;
				dojo.byId('recurring_' + id).options[0].selected = true;
			} else {
				dojo.removeClass(elm1, 'hidden');
			}
		}



		var els = dojo.query('table.type_container a', n);
		var i, imax = els.length;
		var sel_v = Project.get(id, 'category');

		for(i = 0; i < imax; i++) {
			var prop = els[i].name;
			var type_val = parseIntRight(els[i].className);

			if (type_val == sel_v) {
				var td = dojo.dom.getAncestorsByTag(els[i], 'TD', true);
				dojo.addClass(td, 'active');
			}

			els[i].onclick = onCategoryChange;
		}

		//Fill type labels
		var els = n.getElementsByTagName('LABEL');
		var i, imax = els.length;
		for(i = 0; i < imax; i++) {
			var str_for = String(els[i].htmlFor);
			if (str_for != '' && str_for.indexOf('category_') != -1) {
				els[i].htmlFor = str_for + '_' + id;
			}
		}

		fillAssignList.selected[id] = Project.get(id, 'assign');
		fillAssignList(id);

		var e;
		var text = Project.get(id, 'title');
		var l = dojo.query('.heading', el)[0];
		var closed = dojo.query('.closed', n)[0];
		var footer = dojo.query('.footer', n)[0];

		if (options.editMode) {
			hideElement(closed);
			hideElement(l);
			hideElement(footer);
			showElement(opEl);
			dojo.addClass(el, 'highlight-edit');
		} else {
			showElement(closed);
			showElement(footer);
			hideElement(opEl);
			dojo.removeClass(el, 'highlight-edit');
		}

		inp = dojo.byId('title_' + id);
		//inp.disabled = className == 'assigned_to';
		inp.value = text;


		Project.propOpenProc = true;
		var op1 = dojo.query('.period', n)[0];
		var op2 = dojo.query('.close', closed)[0];
		if (op1) op1.style.visibility = 'hidden';
		if (op2) op2.style.visibility = 'hidden';

		var showEditFields = function() {
			return;
			hideElement(closed);
			hideElement(footer);
			hideElement(l);
			showElement(opEl);
			Project.openEdit = true;
			dojo.addClass(el, 'highlight-edit');
		}

		var setStar = function (id) {
			try {

				var el = dojo.byId('element_properties_' + id);
				if (el) {
					var star = dojo.query('.star', el)[0];
					var inp = star.getElementsByTagName('INPUT')[0];

					var v = Project.get(id, 'starred') || 0;
					if (parseInt(v)) {
						dojo.addClass(star, 'star-selected');
						inp.value = 1;
					} else {
						dojo.removeClass(star, 'star-selected');
						inp.value = 0;
					}
				}
			} catch (e) {}
		};

		var setColor = function (id) {
			try {

				var el = dojo.byId('element_properties_' + id);
				if (el) {
					var color_picker = dojo.query('.color_picker', el);
					if (color_picker.length) {
						color_picker = color_picker[0];
						var col = Project.get(id, 'color') || 0;
						var divs = color_picker.getElementsByTagName('DIV');
						if (col >=0 && col < divs.length) {
							dojo.removeClass(divs[0], 'selected');
							dojo.addClass(divs[col], 'selected');
						}
					}
				}

			} catch (e) {}
		}

		var focusTitle = function(id, name) {
			try {
				var inp = dojo.byId(name + '_' + id);
				if (!inp) {
					var inp = dojo.byId('title_' + id);
				}
				if (inp.tagName == 'INPUT' && inp.type == 'text') {
					dojo.dom.selectInputText(inp);
				}

				inp.focus();

				if (inp.tagName == 'TEXTAREA') {
					dojo.dom.setCaretPosition(inp, inp.value.length);
				}
			} catch (e) {}
		}

		Hi_Tag.fillPropertyTags();

		if (options.editMode) {
			showEditFields();
		}

		//Show/hide type selection
		if (typeof options.deleteOnCancel == 'undefined' || !options.deleteOnCancel) {
			var type_container = dojo.query('table.type_container', n)[0];
			if (type_container) {
				dojo.addClass(type_container, 'hidden');
			}
		} else {
			if (HiTaskCalendar.getCurrentDateStr() != HiTaskCalendar.getTodayDateStr()) {
				dojo.byId('start_date_' + id).value = time2str(HiTaskCalendar.currentDate, Hi_Calendar.format);
			}
		}

		function handleDD () {
			var el = dojo.byId('element_properties_' + id);
			var def_h = Hi_Preferences.get('description_input_height', 60);

			if (el) {
				var resize_handle = dojo.query('.textarea-resize', el)[0],
					is_drag = false,
					start_y = 0,
					delta_y = 0;

				var textarea = prevElement(resize_handle);
					textarea.style.height = def_h + 'px';

				dojo_on(resize_handle, 'mousedown', function (evt) {
					try {
					is_drag = true;
					start_y = evt.clientY;
					dojo.stopEvent(evt);
					} catch (e) {}
				}, true);
				dojo_on(document.body, 'mousemove', function (evt) {
					if (!is_drag) return;
					dojo.stopEvent(evt);
					delta_y = evt.clientY - start_y;
					start_y = evt.clientY;

					if (delta_y) {
						try {
						textarea.style.height = (parseInt(textarea.style.height) || def_h) + delta_y + 'px';
						} catch (e) {}
					}
				});
				dojo_on(document.body, 'mouseup', function (evt) {
					try {
					if (!is_drag) return;
					Hi_Preferences.set('description_input_height', parseInt(textarea.style.height));
					Hi_Preferences.send();
					is_drag = false;
					} catch (e) {}
				});
			}
		}

		handleDD();

		var afterWipeIn = function() {

			Project.propOpenProc = false;
			n.style.height = 'auto';
			el.title = '';
			if (options.editMode) {
				focusTitle(id, 'title');
			} else {
				var highlights = dojo.query('.highlight, .ignore-click', el);
				var i, imax = highlights.length;
				for (i = 0; i < imax; i++) {
					var name = highlights[i].id;
					highlights[i].onclick = function(event) {


						dojo.stopEvent(event || window.event);
					}
					//highlights[i].title = 'Click to edit';
				}
			}
			if (op1) op1.style.visibility = 'visible';
			if (op2) op2.style.visibility = 'visible';
		}

		n.parentNode.style.height = '1px';

		if (!options.faster) {
			var eff = dojo_fx.wipeIn({node: n, duration: CONSTANTS.duration});
			dojo.connect(eff, 'onEnd', afterWipeIn);
			eff.play();
			dojo.addClass(el, 'highlight');
		} else {
			n.style.height = 'auto';

			showElement(n);
			showElement(n.parentNode);

			dojo.addClass(el, 'highlight');
			afterWipeIn();
		}

		n.parentNode.style.height = 'auto';
	}

	dojo.global.fillAssignList = function(taskId) {

		if (!('friends' in Project)) return;

		taskId = taskId || 0;
		var prefix = '', suffix = '_' + taskId;
		if (!taskId) {
			prefix = 'newItem_';
			suffix = '';
		}
		var el = dojo.byId(prefix + 'assign' + suffix);
		if (!el) return;
		var shared = null;
		var sharedEl = dojo.byId(prefix + 'shared' + suffix)
		if (sharedEl) {
			shared = sharedEl.checked;
		}
		for (var i = el.options.length - 1; i > 0; i--) {
			if (el.options[i].selected) {
				fillAssignList.selected[taskId] = el.options[i].value;
			}
			el.options[i] = null;
		}
		var opt, id, imax = Project.friends.length;
		for (var i = 0; i < imax; i++) {
			id = Project.friends[i].id;
			if (Project.canAssign(taskId, id, shared)) {
				if (Project.friends[i].activeStatus) {
					opt = new Option(Project.getFriendName(id, 2, true), id);
					if (fillAssignList.selected[taskId] == id) {
						opt.selected = 'selected';
					}
					el.options[el.options.length] = opt;
				}
			}
		}
	}

	dojo.global.fillAssignList.selected = {};

	dojo.global.updateTaskHtml = function(id, data) {
		var el = dojo.byId('task_' + id);
		if (!el) return;

		if (data.start_date || data.end_date) {
			var pdata = Project.getData(true);
			var date_el = dojo.query('.date_title', el)[0];
			date_el.innerHTML = getTaskDateTitle(id, new Date(), pdata[id]);
		}
	}


	dojo.global.hideProperties = function(id, options) {
		//Tooltip hide
		var tmpnew = (Project.opened == -1 ? true : false);

		Project.opened = false;
		Project.openedInstance = false;
		Project.openEdit = false;
		var old = dojo.byId('element_properties_' + id);

		var funcAfter = function() {
			Project.propOpenProc = false;
			processProperties();
			options.afterHide();
			Tooltip.updatePositions();
		}

		if (!old) {
			funcAfter();
			return;
		}

		Project.propOpenProc = true;

		document.body.focus();

		var closed = dojo.query('.closed', n)[0];
		var footer = dojo.query('.footer', n)[0];

		var op = dojo.query('.close', closed)[0];
		if (op) op.style.visibility = 'hidden';
		var op = dojo.query('.period', old)[0];
		if (op) op.style.visibility = 'hidden';

		var el = dojo.byId('task_' + id);
		var n = dojo.byId('element_properties_' + id);



		var opEl = dojo.query('.opened', n)[0];
		if (elementVisible(opEl)) {
			fillViewProperties(id, n, {allowEmptyMessage: true});
			showElement(closed);
			showElement(footer);
			showElement(dojo.query('.heading', el)[0]);
			hideElement(opEl);
			dojo.removeClass(el, 'highlight-edit');
		}

		if (Project.opening && Project.opening != id) {

			/* Another item will be opened, hide without animation */
			n.parentNode.style.height = 'auto';
			hideElement(n);
			funcAfter();
		} else {
			var eff = dojo_fx.wipeOut({node: n, duration: CONSTANTS.duration});
			dojo.connect(eff, 'onEnd', funcAfter);
			eff.play();

		}

		dojo.removeClass(el, 'highlight');
		el.title = 'Click to view';
		var highlights = dojo.query('.highlight', el);
		var i, imax = highlights.length;
		for (i = 0; i < imax; i++) {
			highlights[i].onclick = function() {}
			highlights[i].title = '';
		}



	}

	dojo.global.feedback_submit = function(e) {
		var tooltip = dojo.byId('fdb_tooltip');

		//Show thank you message
		dojo.addClass('fdb_tooltip_form', 'hidden');
		dojo.removeClass('fdb_tooltip_message', 'hidden');

		//Close tooltip after 1 second
		setTimeout(function () {
			Tooltip.close('feedback');
		}, 1000);

		//Restore form after 2 seconds to compensate fadeOut
		setTimeout(function () {
			dojo.removeClass('fdb_tooltip_form', 'hidden');
			dojo.addClass('fdb_tooltip_message', 'hidden');
		}, 4000);
	}
	dojo.global.feedback_validate = function(e) {
		var text = getValue('feedback_text');

		if (text == '') {
			return false;
		}
		return true;
	}

	dojo.global.fillUsers = function(data, force) {
		if (data !== false) Team.slowDown++;
		if (typeof data == 'undefined') {
			data = false;
		}
		if (typeof data == 'object') {
			showElement('grouping_5');
		} else if (data !== false) {
			var status = parseInt(data);
			if (status == 1) {
				openTab('team_container', 1);
			} else if (status == 2) {
				openTab('team_container', 2);
			}
			Project.pingWait = Math.round(CONSTANTS.refreshIntervalEmptyTeam / CONSTANTS.refreshInterval);
			return;
		}
		var active_user = getValue('chat_user_id');
		if (data) {
			var firstCall = Project.friends ? false : true;
			if (data[4] && data[4].domain_name) {
				Project.alertElement.showItem(3, data[4]);
			}

			if (Project.businessId && (!data[5] || !data[5].id || data[5].id != Project.businessId)) {
				Project.alertElement.showItem(4);
			}

			if (data[1] !== 0) {
				var newFrlist = data[1];
				var listTime = data[0];
				Project.friends = newFrlist;
				Project.friendsTime = listTime;
				if (!Project.getFriend(active_user)) {
					Hi_Team.closeChat();
				}

			}
			if (data[2] != 0) {
				var all_msg = data[2];
				Project.chatTime = Project.chatTime || 0;
				var latest = Project.chatTime;
				var taskAssignChange = false;
				var playSound = false;
				for (var i in all_msg) {
					var hasNewMessages = false;
					dojo.forEach(all_msg[i], function(el) {
						if (el.sender != Project.user_id && el['new']) {
							hasNewMessages = true;
						}
						if (Project.chatTime < el.time) {
							if (parseInt(el.type) in {1:1,2:2,10:10}) {
								if (el.sender != Project.user_id) {
									taskAssignChange = true;
								}
								var text = '';
								if (el.type == 1) {
									text = '<span class="assigned_task" onclick="processProperties(' + el.text[0] + ')"><img src="' + Project.icon(el.text[1]) + '" /> ' + htmlspecialchars(el.text[2]) + '</span> <span class="assigned">';
									if (el.text[3] == Project.user_id) {
										text += 'received from <span fill="html: sender_name"></span></span>';
									} else {
										text += 'assigned to <span fill="html: receiver_name"></span></span>';
									}
								}
								if (el.type == 2) {
									text = '<span class="unassigned_task" onclick="processProperties(' + el.text[0] + ')"><img src="' + Project.icon(el.text[1]) + '" /> ' + htmlspecialchars(el.text[2]) + '</span> <span class="unassigned">';
									if (el.text[3] == el.sender) {
										text += hi.i18n.getLocalization('main.returned_to') + ' <span fill="html: receiver_name"></span></span>';
									} else {
										text += hi.i18n.getLocalization('main.returned_to') + ' <span fill="html: sender_name"></span></span>';
									}
								}
								if (el.type == 10) {
									text = '<span class="deleted_task"><img src="' + Project.icon(el.text[1]) + '" /> ' + htmlspecialchars(el.text[2]) + '</span> <span class="deleted">';
									text += hi.i18n.getLocalization('main.deleted_by') + ' <span fill="html: sender_name"></span></span>';
								}
								el.text = text;
							} else {
								el.text = htmlspecialchars(el.text);
							}

							if (!(i in Hi_Team.chatMessages)) {
								Hi_Team.chatMessages[i] = [];
							}
							Hi_Team.chatMessages[i].push(el);
							if (active_user == i) {
								Hi_Team.addMessage(i, el);
							}
							if (el.time > latest) {
								latest = el.time;
							}
						}
					});
					if (hasNewMessages) {
						if (active_user == i) {
							Project.ajax('chread', {user_id: i, time: latest});
						}
						Hi_Team.refreshMessageCount(i);
						playSound = true;
					}
				}

				if (taskAssignChange) {
					if (Project.loadTimeout) {
						dojo.global.clearTimeout(Project.loadTimeout);
						Project.loadTimeout = false;
					}
					Project.load();
				}
				Project.chatTime = latest;

			}
			if (playSound && !firstCall) {
				Hi_Team.playSound();
			}
			var online_status_changed = false;
			var i, imax = Project.friends.length;
			var fr_id;
			for (i = 0; i < imax; i++) {
				fr_id = Project.friends[i].id;
				if ((fr_id in data[3]) != Project.friends[i]['online'] || (Project.friends[i]['online'] !== true && Project.friends[i]['online'] !== false)) {
					Project.friends[i]['online'] = (fr_id in data[3]);
					online_status_changed = true;
				}
			}
			if (('length' in Project.friends) && Project.friends.length == 0) {
				Project.pingWait = Math.round(CONSTANTS.refreshIntervalEmptyTeam / CONSTANTS.refreshInterval);
			}
			if (!Hi_Team.countChanged && data[1] === 0 && data[2] == 0 && !online_status_changed) {
				return;
			}
			Project.friends.sort(function(a, b) {

				var getOrder = function(u) {
					if (u.id == Project.user_id) {
						return 10;
					}
					if (u.status == 10 || u.business_status > 0) {
						return 30;
					}
					if (u.status == 2) {
						return 20;
					}
					return 40;
				}

				a.statusOrder = getOrder(a);
				b.statusOrder = getOrder(b);

				if (a.statusOrder != b.statusOrder) {
					return a.statusOrder - b.statusOrder;
				} else {
					if (a.online != b.online) {
						return a.online ? -1 : 1;
					}
					var a_ = a.name.toLowerCase();
					var b_ = b.name.toLowerCase();
					if (a_ == b_) {
						return a.name > b.name ? 1 : -1;
					} else {
						return a_ > b_ ? 1 : -1;
					}
				}
			});

			openTab('team_container', 0);
			Hi_Team.countChanged = false;
		}

		if (!('friends' in Project) || force) {
			Project.load();
			//Project.ajax('frlist', {time: Project.friendsTime || 0, chat_time: Project.chatTime || 0}, function(data){fillUsers(data)}, function() {Project.pingWait = Project.pingTimeout / CONSTANTS.refreshInterval;});
			return;
		}

		Team.slowDown = 0;
		Project.pingWait = 0;
		HiDragSourceCached.user = false;
		var container = Project.getContainer('fillUsers');
		container = dojo.byId(container);
		var parent = childNode(dojo.byId('user_container_template'), 0);
		parent = parent.cloneNode(true);
		var template = childNode(dojo.byId('user_template'), 0);
		var users = Project.friends;
		var i, j;
		var on = 0;
		var approved = 0;
		l = users.length;
		var msg_cnt = 0;
		var userDef = Hi_Team.emptyUser;
		var atLeastOneFriend = false;
		for (i = 0; i < l; i++) {

			if (Hi_Team.refreshMessageCount(users[i]['id']) > 0) {
				msg_cnt++;
			}
			Hi_Team.countChanged = false;

			var u = {};
			for (j in users[i]) {
				u[j] = users[i][j];
			}
			u.status = parseInt(u.status);

			u = dojo.mixin(u, userDef);

			u['image'] = 'url(/avatar/' + u['picture'] + '.16.gif)';
			u['image32'] = 'url(/avatar/' + u['picture'] + '.32.gif)';
			u['image64'] = 'url(/avatar/' + u['picture'] + '.64.gif)';
			if (!u['online']) {
				u['offline'] = 'offline';
			} else {
				u['offline'] = 'online';
				on++;
			}
			u['menuStatus'] = true;
			if (u['messages']) {
				u['msgsStatus'] = true;
			}
			if (u.status == 1) {
				u['waitStatus'] = true;
			} else if (u.status == 2) {
				u['reqStatus'] = true;
				u['menuStatus'] = false;
			} else if (u.status == 0) {
				u['declStatus'] = true;
			} else if (u.status == 10) {
				u['activeStatus'] = true;
				u['premiumStatus'] = u['level'];
				approved = 1;
			}

			if (u.business_status !== '') {
				u['premiumStatus'] = false;
				u['menuStatus'] = true;
				u['businessStatus'] = true;
				if (u.business_status == -1) {
					u['businessStatusDeclined'] = true;
				} else if (u.business_status == 0) {
					u['businessStatusInactive'] = true;
				} else {
					if (u.business_status == 100) {
						u['businessStatusAdmin'] = true;
					}
					u['activeStatus'] = true;
					u['businessStatusActive'] = true;
					u['waitStatus'] = false;
					u['reqStatus'] = false;
					approved = 1;
				}
			}
			for (j in u) {
				Project.friends[i][j] = u[j];
			}
			if (users[i].id == Project.user_id) continue;

			tmp = template.cloneNode(true);
			fillTemplate(tmp, u);
			tmp.id = 'friend_item_' + u['id'];
			if (active_user == u['id']) {
				tmp.style.fontWeight = 'bold';
			}
			parent.appendChild(tmp);
			atLeastOneFriend = true;
		}
		if (msg_cnt > 0) {
			dojo.removeClass('new_messages', 'invisible');
			dojo.removeClass('new_messages_collapsed', 'invisible');
			setValue(dojo.query('.message_count', dojo.byId('new_messages'))[0], msg_cnt);
		} else {
			dojo.addClass('new_messages', 'invisible');
			dojo.addClass('new_messages_collapsed', 'invisible');
		}

		var help = dojo.byId('team_help');
		if (approved == 0 || Hi_Preferences.get('hide_team_help') == 1) {
			hideElement(help);
		} else {
			showElement(help);
		}

		//removeNode(container, false);
		container.innerHTML = '';	//faster than removing each node sepparate

		if (atLeastOneFriend) {
			showElement(container);
			container.appendChild(parent);
		} else {
			hideElement(container);
		}
		if (Project.firstUserLoad) {
			fillTasks();
			Project.firstUserLoad = false;
		}
		Tooltip.updatePositions();
	}

	dojo.global.completeList = [];
	dojo.global.completeTimer = null;

	dojo.global.completeTask = function(el, noFill) {
		var task = getParentByClass(el, 'task_li');
		var id = parseIntRight(task.id);
		var completeContainer = dojo.byId('tasks_completed');

		var _completeTask = function (ch, id, task) {
			ch.checked = el.checked;
			ch.disabled = true;
			if (el.checked) {
				dojo.addClass(task, 'completed');
			} else {
				dojo.removeClass(task, 'completed');
			}
		}
		Project.iterateTaskElements(id, _completeTask, 'check');
		var after = function() {
			Project.iterateTaskElements(id, function(ch) {ch.disabled=false}, 'check');
		}

		var options = {redraw: false, afterRequest: after};
		var ch = el.checked;

		if (parseInt(Project.get(id, 'recurring'))) {
			var instance_date = el.getAttribute('eventdate');

			if (instance_date && Project.isEventComplete(id, instance_date)) {
				ch = 0;
			} else {
				ch = 1;
			}

			if (instance_date) {
				if (typeof Project.data[id].recurring_instances == 'undefined') Project.data[id].recurring_instances = {};
				if (typeof Project.data[id].recurring_instances[instance_date] == 'undefined') Project.data[id].recurring_instances[instance_date] = {status: 0};
				Project.data[id].recurring_instances[instance_date] = {status: ch};
				options.instance_date = instance_date;
			}
		}

		Project.set(id, 'completed', ch ? '1' : '0', options, 'itemcomplete');
		//Project.changeCompletedCount(ch ? 1 : -1);

		var isContainerTooltip = getParentByClass(el, 'task_ul');
			isContainerTooltip = (isContainerTooltip && isContainerTooltip.id == 'tooltip_tasks' ? true : false);

		var group = Hi_Preferences.get('currentTab', 'my', 'string');

		if (group != 'my' && group != 'project' && el.checked && completeContainer && task && !isContainerTooltip) {
			completeContainer.appendChild(task);

			//Remove sorting
			dojo.removeClass(task, 'dojoDndItem');

		} else {
			if (!noFill) fillTasks();
		}
	}

	dojo.global.colorPickerClick = function(el, col, is_new) {
		var els = el.parentNode.getElementsByTagName('DIV');

		for(var i=0,j=els.length; i<j; i++) {
			dojo.removeClass(els[i], 'selected');
		}

		dojo.addClass(el, 'selected');

		if (!is_new) {
			chooseColor(col ,Project.opened);
		} else {
			newItemChooseColor(col);
		}
	}

	dojo.global.chooseColor = function(n, op, li) {
		op = op || false;
		if (!op) {
			var op = Project.opened;
			if (!op) {
				return;
			}
		}

		var li = dojo.byId('task_' + op);
		var check = dojo.query('.check', li)[0];

		if (!check) return;
		dojo.removeClass(check, 'color1');
		dojo.removeClass(check, 'color2');
		dojo.removeClass(check, 'color3');
		dojo.removeClass(check, 'color4');
		dojo.removeClass(check, 'color5');
		dojo.removeClass(check, 'color6');
		dojo.removeClass(check, 'color7');

		if (n) {
			dojo.addClass(check, 'color' + n);
		}
		if (dojo.byId('color_' + op)) {
			dojo.byId('color_' + op).value = n;
		}
	}

	dojo.global.newItemChooseColor = function(n) {
		var off = false;
		if (typeof n == 'undefined') {
			off = true;
		}
		n = n || 0;
		setValue('newItem_color', n);

		var cont = dojo.byId('newItem_color_container');
		var color_cont = dojo.query('.color_picker', cont)[0];
		var colors = color_cont.getElementsByTagName('DIV');

		for(var i=0; i<colors.length; i++) {
			if (i != n) {
				dojo.removeClass(colors[i], 'selected');
			} else {
				dojo.addClass(colors[i], 'selected');
			}
		}
	}

	dojo.global.Team = {
		slowDown: 0,
		teamInterval: false,
		friendCount: false,
		pingForce: false,
		ping: function(force) {
			if (Team.pingForce && !force) {
				Team.pingForce = false;
				return;
			}
			fillUsers(false, true);
			if (force) {
				Team.pingForce = true;
			}
		},
		reset: function (t, e) {
			openTab('frfind', 0, 0, {});
		},
		frfind: function (data) {
			if (data && data.id) {
				if (data.status) {
					openTab('frfind', 5, 0, data);
				} else {
					openTab('frfind', 1, 0, data);
				}
			} else {
				if (data.return_code == '2') {
					openTab('frfind', 9, 0, data);
				} else if (data.return_code == '3') {
					openTab('frfind', 10, 0, {});
				} else {
					if ('email' in  data) {
						openTab('frfind', 2, 0, data);
					} else {
						openTab('frfind', 6, 0, data);
					}
				}
			}
		},
		fradd: function (data) {
			if (data && data.status == '1') {
				openTab('frfind', 3);
				Team.ping(true);
			} else {
				var err = Team.fradd_error(data.error, 1);
				if (!err)
					openTab('frfind', 4);
			}
		},
		frapprove: function (data) {
			Team.ping(true);
		},
		frdelete: function (data) {
			Team.ping(true);
			Project.load();
		},
		frinvite: function (data) {
			if (data && data.status == '1') {
				openTab('frfind', 7);
				Team.ping(true);
			} else {
				var err = Team.fradd_error(data.error, 1);
				if (!err)
					openTab('frfind', 8);
			}
		},
		fradd_error: function(status, tooltip) {
			tooltip = tooltip || false;
			switch (status) {
				case 'NO_LICENSES':
					Project.alertElement.showItem(5);
					return true;
				case 'IN_BUSINESS':
					if (tooltip) {
						openTab('frfind', 11)
					} else {
						alert(hi.i18n.getLocalization('friendfind.in_business'));
					}
					return true;
			}
			return false;
		},
		fradd_business: function (data) {
			if (data && data.status == '1') {
				Team.ping(true);
			} else {
				var err = Team.fradd_error(data.error, 0);
				if (!err) {
					alert('Oops! We can\'t execute this operation at the moment. Please try again a bit later.');
				}
			}
		},
		mchange: function (data) {
			if (data) {
				if (data.status == '1') {
					setValue('confirm_email_email', htmlspecialchars(data.email));
					openTab('team_container', 1);
				} else {
					if (data.error == '2') {
						alert('This email is already used by other person!');
					} else if (data.error == 3) {
						// email confirmed alreay. Refresh team sidebar
						Team.ping(true);
					} else if (data.error == '10') {
						alert('Incorrect email address!');
					} else {
						alert('Couldn\'t change email for unknown reason!');
					}
				}
			}
			setValue('confirm_email_email_input', getValue('confirm_email_email'));
		},
		binv_decline: function() {
			Project.alertElement.hide();
		},
		binv_decline_error: function() {
			alert('Oops! We can\'t execute this operation at the moment. Please try again a bit later.');
			Project.alertElement.hide();
		},
		binv_accept: function(id) {
			location.href = '/business-invitation?yes&' + Project.session_name + '=' + Project.session_id + '&invitation_id=' + id;
		}
	}
	dojo.global.advFocus = function(obj, val) {
		if(obj.value == val) {
			obj.value = "";
			dojo.removeClass(obj, 'empty')
		}
	}
	dojo.global.advBlur = function(obj, val, func) {
		func = func || false;
		if(obj.value == "") {
			obj.value = val;
			dojo.addClass(obj, 'empty');
			if (func) {
				func();
			}
		}
	}

	dojo.global.toggleColumn = function(btn, column, oppositeColumn){
		var column = dojo.byId(column);
		if(!column) return false;

		var oppositeColumn = dojo.byId(oppositeColumn);
		var columnText = dojo.byId(btn + '_text');

		var main = dojo.byId('main');
		if(!main) return false;

		if(!dojo.hasClass(column, 'invisible')) {
			dojo.addClass(column, 'invisible');
			dojo.addClass(main, 'no_' + btn);
			if(oppositeColumn) {
				dojo.addClass(oppositeColumn, 'no_' + btn);
			}
			if(columnText) {
				dojo.removeClass(columnText, 'hidden');
			}
			Hi_Preferences.set('sidebar_' + column.id, '0');
		} else {
			if(columnText) {
				dojo.addClass(columnText, 'hidden');
			}
			if(oppositeColumn) {
				dojo.removeClass(oppositeColumn, 'no_' + btn)
			}
			dojo.removeClass(main, 'no_' + btn);
			dojo.removeClass(column, 'invisible');
			Hi_Preferences.set('sidebar_' + column.id, '1');
		}
		var btnEl = dojo.query('.btns', dojo.byId('collapse'))[0];
		btnEl = dojo.query(btn, btnEl)[0];
		if(!dojo.hasClass(btnEl, 'left')){
			dojo.removeClass(btnEl, 'right');
			dojo.addClass(btnEl, 'left');
		}else{
			dojo.removeClass(btnEl, 'left');
			dojo.addClass(btnEl, 'right');
		}
		HiDragSourceCached.project = false;
	}

	dojo.global.toggleElementAndCloseOther = function(id1, id2) {
		var focusBlurFirstInput = function (el, mode) {
			var inputs = el.getElementsByTagName('INPUT');
			if(mode==1)
				dojo.global.setTimeout(function(){inputs[0].focus();}, 100);
			else {
				var i;
				for (i = 0; i < inputs.length; i++) {
					try {
						inputs[i].value = inputs[i].defaultValue;
						inputs[i].checked = inputs[i].defaultChecked;
						inputs[i].blur();
					} catch (err) {};
				}
			}
		}
		var element = dojo.byId(id1);
		if(!element) return false;

		var other = dojo.byId(id2);

		if(!dojo.hasClass(element, 'hidden')) {
			focusBlurFirstInput(element, 0);
			dojo.addClass(element, 'hidden');
		} else {
			if(other) {
				focusBlurFirstInput(other, 0);
				dojo.addClass(other, 'hidden');
			}
			dojo.removeClass(element, 'hidden');
			focusBlurFirstInput(element, 1);
		}

	}

	dojo.global.hoverTimeout = null;
	dojo.global.curHoverElement = null;

	dojo.global.hoverElement = function(element)
	{
		if (!dojo.isIE || dojo.isIE >= 7) return;
		if (Project.dragging) {
			return false;
		}
		if(curHoverElement != null) {
			clearTimeout(hoverTimeout);
			if(element != curHoverElement) {
				houtCurHoverElement();
			}
		}
		dojo.addClass(element, 'hover');
	}
	dojo.global.houtElement = function(element) {
		if (!dojo.isIE || dojo.isIE >= 7) return;
		curHoverElement = element;
		hoverTimeout = setTimeout(houtCurHoverElement, 10);
	}
	dojo.global.houtCurHoverElement = function() {
		curHoverElement = curHoverElement || null;
		if (!curHoverElement) return;
		dojo.removeClass(curHoverElement, 'hover');
		curHoverElement = null;
	}
	dojo.global.houtAllElements = function(el) {
		el = dojo.byId(el);
		var els = dojo.query('.hover', el);
		var i, imax = els.length;
		for (i = 0; i < imax; i++) {
			if (dojo.hasClass(els[i], 'hover'))
				dojo.removeClass(els[i], 'hover');
		}
	}

	dojo.global.objectLength  = function(obj) {
		var cnt = 0;
		for(var i in obj) if (obj.hasOwnProperty(i)) cnt++;
		return cnt;
	}

	dojo.global.searchData = function(str) {
		var i;
		if (!dojo.isString(str)) {
			str = '';
		}
		str = str.toLowerCase().trim();
		var el;
		for (i in Project.data) {
			if (el = dojo.byId('task_' + i)) {
				el = dojo.query('.task_title', el)[0];
				if (str == '' || Project.data[i].title.toLowerCase().indexOf(str) == -1) {
					el.style.backgroundColor = '';
				} else {
					el.style.backgroundColor = 'yellow';
				}
			}
		}
	}

	dojo.global.unassignTask = function(id) {
		Project.unassign(id, {
			after:function() {
				processProperties(null, {save: false});
			}
		}, false);
	}

	/**
	 * Format message,
	 * Converts textile into HTML
	 */
	dojo.global.formatMessage = function(message, unescape) {
		var r = message;

		//if (unescape) r = unhtmlspecialchars(r);
		if (!unescape) r = htmlspecialchars(r);

		//Unescape quoteblock
		r = r.replace(/(^|\n)&gt;/g, '$1>');

		// quick tags first
		qtags = [['\\*', 'strong'],
				 ['\\?\\?', 'cite'],
				 ['\\+', 'ins'],  //fixed
				 ['~', 'sub'],
				 ['\\^', 'sup'], // me
				 ['@', 'code']];

		for (var i=0;i<qtags.length;i++) {
			ttag = qtags[i][0]; htag = qtags[i][1];
			re = new RegExp(ttag+'\\b(.+?)\\b'+ttag,'g');
			r = r.replace(re,'<'+htag+'>'+'$1'+'</'+htag+'>');
		}

		// preformatted code
		var lines = r.split('\n'), out = [], prev = null;

		for(var i=0,ii=lines.length; i<ii; i++) {
			var line = lines[i];
			var cur = null;

			if (line.indexOf('    ') == 0) cur = 'pre';
			if (line.indexOf('> ') == 0) cur = 'blockquote';

			if (prev && (!cur || cur != prev)) {
				out[out.length-1] += '</' + prev + '>';
			}

			if (cur && (!prev || prev != cur)) {
				out[out.length] = '<' + cur + '>' + line.substr((cur == 'pre' ? 4 : 2));
			} else if (cur && cur == prev) {
				if (cur == 'pre') {
					out[out.length-1] += "__PRE_BREAK__" + line.substr(4);
				} else if (cur == 'blockquote') {
					out[out.length] = line.substr(2);
				}
			} else {
				out[out.length] = line;
			}

			prev = cur;
		}

		r = out.join('\n');

		// h1-h6
		r = r.replace(/(^|\n)h([1-6])\.(.*)?/g, '$1<h$2>$3</h$2>');

		// fix </block><br />
		r = r.replace(/(<\/(h1|h2|h3|h4|h5|h6|pre|blockquote)>)\n/g, '$1');

		// underscores count as part of a word, so do them separately
		re = new RegExp('\\b_(.+?)_\\b','g');
		r = r.replace(re,'<em>$1</em>');

		//jeff: so do dashes
		re = new RegExp('[\s\n]-(.+?)-[\s\n]','g');
		r = r.replace(re,'<del>$1</del>');

		// links
		r = r.replace(/(^|[\s\(])([a-z]+\:\/\/([\.,;\-!\?]?[^\s\.,;\-!\?\)\(])+)(([\.,;\-!\?]*[\s\(\)])|([\.,;\-!\?\)\(]*$))/ig, '$1<a target="_blank" onclick="javascript:event.cancelBubble=true" title="Go to $2" href="$2">$2</a>$4');
		r = r.replace(/(^|[\s\(])(www\.([\.,;\-!\?]?[^\s\.,;\-!\?\)\(])+)(([\.,;\-!\?]*[\s\(\)])|([\.,;\-!\?\)\(]*$))/ig, '$1<a target="_blank" onclick="javascript:event.cancelBubble=true" title="Go to $2" href="http://$2">$2</a>$4');
		r = r.replace(/(^|^[^<>]*|>[^<]*)(hi)(task)/ig, '$1<span class="hi">$2</span><span class="task">$3</span>');

		re = new RegExp('"\\b(.+?)\\(\\b(.+?)\\b\\)":([^\\s]+)','g');
		r = r.replace(re,'<a target="_blank" onclick="javascript:event.cancelBubble=true" href="$3" title="$2">$1</a>');
		re = new RegExp('"\\b(.+?)\\b":([^\\s]+)','g');
		r = r.replace(re,'<a target="_blank" onclick="javascript:event.cancelBubble=true" href="$2">$1</a>');

		// images
		re = new RegExp('!\\b(.+?)\\(\\b(.+?)\\b\\)!','g');
		r = r.replace(re,'<img src="$1" alt="$2">');
		re = new RegExp('!\\b(.+?)\\b!','g');
		r = r.replace(re,'<img src="$1">');

		// lists
		var lines = r.split('\n');
		var out = '';
		nr = '';
		for (var i=0;i<lines.length;i++) {
			line = lines[i].replace(/\s*$/,'');

			if (line.search(/^\s*\*\s+/) != -1) { line = line.replace(/^\s*\*\s+/,'\t<liu>') + '</liu>'; } // * for bullet list; make up an liu tag to be fixed later
			if (line.search(/^\s*#\s+/) != -1) { line = line.replace(/^\s*#\s+/,'\t<lio>') + '</lio>'; } // # for numeric list; make up an lio tag to be fixed later
			lines[i] = line;
		}

		// Second pass to do lists
		inlist = 0;
		listtype = '';
		for (var i=0;i<lines.length;i++) {
			line = lines[i];
			if (inlist && listtype == 'ul' && !line.match(/^\t<liu/)) { line = '</ul>' + line; inlist = 0; }
			if (inlist && listtype == 'ol' && !line.match(/^\t<lio/)) { line = '</ol>' + line; inlist = 0; }
			if (!inlist && line.match(/\t<liu/)) { line = line.replace(/\t<liu/, '<ul><liu'); inlist = 1; listtype = 'ul'; }
			if (!inlist && line.match(/\t<lio/)) { line = line.replace(/\t<lio/, '<ol><lio'); inlist = 1; listtype = 'ol'; }
			lines[i] = line;
		}

		if (inlist) {
			if (listtype == 'ul') lines.push('</ul>');
			if (listtype == 'ol') lines.push('</ol>');
		}

		r = lines.join('\n');
		r = r.replace(/li[o|u]>/g,'li>');
		r = r.replace(/(<\/(li|ul|ol)>)\n/g, '$1');

		//Line breaks
		re = new RegExp('\n','g');
		r = r.replace(re,"<br />");

		//Pre new lines shouldn't be converted to <br />
		r = r.replace(/__PRE_BREAK__/g, "\n");

		return r;
	}

	//TODO
	return {};

});
