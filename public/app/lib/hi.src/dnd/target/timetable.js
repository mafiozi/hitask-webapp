/**
 * Specifies tooltips on time table drop targets
 */
//dojo.provide("hi.dnd.target.timetable");
define(function () {
	
	dojo.declare("dojo.dnd.HiDragTargetTimetable", [dojo.dnd.Source], {
		isSource: false,
		selfAccept: false,
		accept: ["dragged_task"],
		dropTarget: null,
		permissionsAllowed: [Hi_Permissions.MODIFY, Hi_Permissions.EVERYTHING],

		action: 'modify',
		
		onMouseOut: function (e) {
			dojo.dnd.HiDragTargetTimetable.superclass.onMouseMove.call(this, e);
			
			//JDT var m = dojo.dnd.Manager();
			var m = dojo.dnd.Manager.manager();

			if (m.source instanceof dojo.dnd.HiDragSource) {
				m.avatar.updateText('');
				this.dropValue = '';
			}
		},

		onMouseMove: function (e) {
			dojo.dnd.HiDragTargetTimetable.superclass.onMouseMove.call(this, e);
			
			if (this.isDragging) {
				
				//JDT var m = dojo.dnd.Manager();
				var m = dojo.dnd.Manager.manager();
				if (m.source instanceof dojo.dnd.HiDragSource) {
	
					if (parseInt(Hi_Preferences.get('sidebar_calendar')) == 0) return;
					
					var type = hi.items.manager.get(m.source.dragNode.dragId, 'category');
					var id = m.source.dragNode.dragId;
					var item = him.get(id);

					/* #3059 Note can be d&d to the calendar and set the time*/
					if (type == hi.widget.TaskItem.CATEGORY_FILE || type == hi.widget.TaskItem.CATEGORY_NOTE) return;
					
					var c = dojo.byId('timetable');
					var b = dojo.position(c, true);
					var y = e.pageY - b.y - 7; // #2760: minus 7px.
					
					var time = HiTaskCalendar.getTaskTime(y);
					var txt = '';
					this.dropValue = time;

					if (item.start_date) {
						txt = hi.i18n.getLocalization('center.start_on');
					} else {
						txt = hi.i18n.getLocalization('center.due_on');
					}
					txt += ' ';
					txt += HiTaskCalendar.formatTime(time);

					// if (type === hi.widget.TaskItem.CATEGORY_TASK) {
					// 	txt = hi.i18n.getLocalization('center.due_on') + ' ' + time2str(HiTaskCalendar.currentDate, 'd-m-y');
					// }

					m.avatar.updateText(txt);
				}
			}
		}
	});
	
	return dojo.dnd.HiDragTargetTimetable;
});