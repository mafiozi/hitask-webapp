package oauth;

import java.util.Map;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import utils.HttpResponseWrapper;

public class OAuth2Error {
	private final OAuth2Error.Type type;
	private final String error;
	private final String description;

	public enum Type {
		COMMUNICATION, OAUTH, UNKNOWN
	}

	private OAuth2Error(OAuth2Error.Type type, String error, String description) {
		this.type = type;
		this.error = error;
		this.description = description;
	}

	static OAuth2Error communication() {
		return new OAuth2Error(Type.COMMUNICATION, null, null);
	}

	static OAuth2Error oauth2(HttpResponseWrapper response) {
		if (response.getContentType().startsWith("application/json")) {
			JsonObject jsonResponse = response.getJson().getAsJsonObject();
			JsonElement error = jsonResponse.get("error");
			if (error.isJsonObject()) {
				return new OAuth2Error(Type.OAUTH, error.getAsJsonObject().getAsJsonPrimitive("type").getAsString(), error.getAsJsonObject().getAsJsonPrimitive("message").getAsString());
			} else if (error.isJsonPrimitive()) {
				return new OAuth2Error(Type.OAUTH, null, error.getAsString());
			} else {
				return new OAuth2Error(Type.UNKNOWN, null, null);
			}
		} else {
			Map<String, String> qs = response.getQueryString();
			if (qs.containsKey("error")) {
				return new OAuth2Error(Type.OAUTH, qs.get("error"), qs.get("error_description"));
			} else {
				return new OAuth2Error(Type.UNKNOWN, null, null);
			}
		}
	}

	@Override
	public String toString() {
		return "OAuth2 Error: " + type + " - " + error + " (" + description + ")";
	}

	public OAuth2Error.Type getType() {
		return type;
	}

	public String getError() {
		return error;
	}

	public String getDescription() {
		return description;
	}
}