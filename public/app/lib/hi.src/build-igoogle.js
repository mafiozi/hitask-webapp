/*
dojo.provide("hi.build-igoogle");

dojo.require("hi.api");
dojo.require("hi.igoogle");
dojo.require("hi.calendar");
dojo.require("hi.taskcalendar");
*/

define([
	"hi/api",
	"hi/igoogle",
	"hi/calendar",
	"hi/taskcalendar"
], function () {
	return {};
});