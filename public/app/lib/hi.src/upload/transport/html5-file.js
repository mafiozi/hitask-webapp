define(function () {
	
	var FileUploadHTML5File = dojo.global.FileUploadHTML5File = function (guid, file, task) {
		this.guid = guid;
		this.file = file;
		this.task = task;
		
		this.state = 1;
		
		this.ready();
	};
	
	FileUploadHTML5File.prototype = {
		/**
		 * Unique ID for file
		 * @type {Number}
		 */
		guid: null,
		
		/**
		 * Task ID
		 * @type {String}
		 */
		task: null,
		
		/**
		 * File
		 * @type {Object}
		 */
		file: null,
		
		/**
		 * Current state
		 * 0 - initial, 1 - preparing, 2 - ready, 3 - uploading, 4 - completed
		 * @type {Number}
		 */
		state: 0,
		
		/**
		 * Upload params
		 * @type {Object}
		 */
		params: {},
		
		/**
		 * Upload URL
		 * @type {String}
		 */
		url: '',
		
		/**
		 * XHR request object
		 * @type {Object}
		 */
		xhr: null,
		
		/**
		 * File upload was canceled
		 * @type {Boolean}
		 */
		canceled: false,
		
		/**
		 * Ready file upload
		 */
		ready: function () {
			var _this = this;
			
			FileUpload.loadStorageInfo(function (data) {
				/* #4480 forbid folder drop */
				if (!_this.file.type || _this.file.type == "") return;
				var filename = _this.file.fileName || _this.file.name,
					filesize = _this.file.fileSize || _this.file.size;
				var fileSizeMb = filesize / 1024 / 1024;
				
				if((data.quota - data.used - fileSizeMb) < 0){
					Project.alertElement.showItem(13);
					return;
				}
					
				if (FileDropArea.ready(_this.guid, _this.task, filename, filesize)) {
					_this.upload();
				}
			});
		},
		
		/**
		 * Cancel file upload
		 */
		cancel: function () {
			this.canceled = true;
			if (this.xhr) {
				this.xhr.abort();
			}
		},
		
		/**
		 * Upload file
		 */
		upload: function () {
			if (this.state >= 3) return;
			
			var filename = this.file.fileName || this.file.name,
				self = this;
			
			this.params = {
				'api_key': Project.api2_key,
				'api_ver': Project.api_version,
				'parent_id': this.task > 0 ? this.task : 0
			};
			
			this.params[Project.api2_session_name] = Project.api2_session_id;
			
			this.url = Project.api2_base_url + '/' + Project.api2_url_index + '/file/upload';
			this.state = 3;
			
			if (!this.url) {
				this.updateError();
			} else if (typeof FormData != 'undefined') {
				this._uploadFormData();
			} else {
				this._uploadPlain();
			}
		},
		
		_uploadPlain: function () {
			//Read file
			var reader = new FileReader(),
				filename = this.file.fileName || this.file.name,
				filetype = this.file.type || 'application/octet-stream',
				url = this.url,
				xhr = new XMLHttpRequest(),
				self = this,
				binary = window.XMLHttpRequest.prototype.sendAsBinary ? true : false;
			
			if (!Project.isDevMode) {
				url = this.url.replace(/^http(s)?:\/\/[^\/]+/i, '');
			}
			
			this.xhr = xhr;
			
			reader.onload = function (evt) {
				var data = evt.target.result,
					boundary = 'xhrupload-' + parseInt(Math.random()*(2 << 16)),
					completed = false;
				
				xhr.onreadystatechange = function (evt) {
					if(xhr.readyState === 4) {
						//Upload completed successfully
						completed = true;
						self.updateComplete(evt);
					}
				};
				
				xhr.upload.addEventListener("progress", function(evt) {
					self.updateProgress(evt);
				}, false);
				
				xhr.upload.addEventListener("error", function(evt) {
					//Upload failed
					if (!completed) {
						self.updateError();
					}
				}, false);
				
				if (binary) {
					if (/[^\x20-\x7E]/.test(filename)) {
						filename = unescape(encodeURIComponent(filename));
					}
				} else {
					data = data.substring(data.indexOf(',')+1, data.length);
					filename = encodeURIComponent(filename);
				}
				
				xhr.open("POST", url);
				xhr.setRequestHeader('Content-Type', 'multipart/form-data; boundary=' + boundary);
				
				var body = ''; //'Content-Type: multipart/form-data; boundary=' + boundary + '\r\n\r\n';
				for(var i in self.params) {
					body += '--' + boundary + '\r\n' +
							'Content-Disposition: form-data; name="' + encodeURIComponent(i) + '"\r\n\r\n' + 
							self.params[i] + '\r\n';
				}
				
				body += '--' + boundary + '\r\n' +// RFC 1867 Format, simulate form file upload
						'Content-Disposition: form-data; name="file"; filename="' + filename + (binary ? '' : '.base64') + '"\r\n' +
						(binary ? '' : 'Content-Transfer-Encoding: base64\r\n') +// Vaild MIME header, but won't work with PHP file upload handling.
						'Content-Type: ' + filetype + '\r\n\r\n' +
						data + '\r\n' + '--' + boundary + '--\r\n';
				
				if (binary) {
					xhr.sendAsBinary(body);
				} else {
					xhr.send(body);
				}
				
				//Clean up
				//It's meaningless to delete variable like this!
				// delete(body);
				// delete(data);
				delete(evt.target.result);
			};
			
			if (binary) {
				reader.readAsBinaryString(this.file);
			} else {
				reader.readAsDataURL(this.file);
			}
		},
		
		/**
		 * FormData is not implemented in all browsers yet
		 * Advatage of using FormData is less memory consumtion, because all file
		 * doesn't have to be read into memory
		 */
		_uploadFormData: function () {
			var data = new FormData(),
				self = this,
				url = this.url,
				xhr = new XMLHttpRequest(),
				completed = false;
			
			this.xhr = xhr;
			if (!Project.isDevMode) {
				// Remove domain from url in PROD mode only.
				url = this.url.replace(/^http(s)?:\/\/[^\/]+/i, '');
			}
			
			xhr.onreadystatechange = function (evt) {
				if(xhr.readyState === 4) {
					//Upload completed successfully
					completed = true;
					self.updateComplete(evt);
				}
			};
			
			xhr.upload.addEventListener("progress", function(evt) {
				self.updateProgress(evt);
			}, false);
			
			xhr.upload.addEventListener("error", function(evt) {
				//Upload failed
				if (!completed) {
					self.updateError();
				}
			}, false);
			
			
			for(var i in this.params) {
				data.append(i, this.params[i]);
			}
			
			//File should be last field
			data.append('file', this.file);
			
			xhr.open("POST", url);
			xhr.send(data);
		},
		
		/**
		 * Error occurred during upload
		 */
		updateError: function () {
			var file = FileUpload.getFile(this.guid);
								
			if (file) {
				file.error();
			}
		},
		
		/**
		 * Send upload progress to front-end
		 * 
		 * @param {Event} evt
		 */
		updateProgress: function (evt) {
			if (evt.lengthComputable || evt.total) {
				var percentage = Math.round((evt.loaded * 100) / evt.total),
					file = FileUpload.getFile(this.guid);
				
				if (file) {
					file.progress(percentage);
				}
			}
		},
		
		/**
		 * Send complete progress
		 * @param {Object} evt
		 */
		updateComplete: function (evt) {
			this.state = 4;
			
			if (!this.canceled) {
				var file = FileUpload.getFile(this.guid);
				if (file && !file.canceled) {
					/* Bug #1960- myResponse holds the new item ID */
					var apiResponse = JSON.parse(evt.target.response);
					if (apiResponse.error_message) {
						if (apiResponse.response_status == 20) {
							Project.alertElement.showItem(13);
						}
						file.error(apiResponse);
					} else {
						var myResponse = apiResponse.id;
						file.complete(myResponse);
					}
					
					/*
					FileUpload.notifyComplete(this.params.key, function (response) {
						if (file) {
							if (response == 'OK') {
								file.complete();
							} else {
								file.error(response);
							}
						}
					});
					*/
				}
			}
		}
	};
	
	
	/**
	 * Handle files
	 */
	var FileUploadHTML5FileList = dojo.global.FileUploadHTML5FileList = {
		/**
		 * File list
		 * @type {Object}
		 */
		files: {},
		
		addFile: function (guid, file, task) {
			this.files[guid] = new FileUploadHTML5File(guid, file, task);
		},
		
		upload: function (guid) {
			this.files[guid].upload();
		},
		
		cancel: function (guid) {
			this.files[guid].cancel();
		}
	};
	
});