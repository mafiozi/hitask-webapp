define([
	"dojo/_base/array",
	"dojo/_base/declare",
	"dojo/_base/fx",
	"dojo/cldr/supplemental",
	"dojo/date",
	"dojo/date/locale",
	"dojo/html",
	"dojo/on",
	"dojo/dom",
	"dojo/dom-class",
	"dojo/request/xhr",
	"dojo/_base/event",
	"dojo/_base/lang",
	"dojo/_base/sniff",
	"dojo/string",
	'hi/store',
	"dojo/_base/window",
	"dijit/_Widget",
	"dijit/_Templated",
	"dojo/Evented",
	'dojo/cookie',
	'dojo/query',
	'dojo/dom-attr',
	'dojo/dom-construct'
], function(array, declare, fx, cldrSupplemental, date, local, dojoHtml, dojoOn, dom, domClass, xhr, event, lang, has, string, hi_store, win,
	_WidgetBase, _TemplatedMixin, _Evented, dojoCookie, dojoQuery, dojoAttr, domConstruct) {

	// module:
	//		hi/widget/ActivityFeedContainer
	// summary:
	//		Activity feed

	var ActivityFeedContainer = declare("hi.widget.ActivityFeedContainer", [_WidgetBase, _TemplatedMixin, _Evented], {
		RECENT_NOTIFICATION_COOKIE_NAME: 'desktop_notification_recent_event_time',
		MINIMUM_DOM_ELEMENTS_BEFORE_CLEANUP: 32,
		AVATAR_SIZE: 64,

		templateString: '\
			<div id="activityFeedContainer" class="hidden" data-dojo-attach-point="ActivityFeedContainer">\
				<div class="feed_empty hidden" data-dojo-attach-point="noItemsNode">\
					' + hi.i18n.getLocalization('center.no_items') + '\
					<div class="error" data-dojo-attach-point="websocketBehindProxyError"></div>\
				</div>\
				<div id="feedLoadingBar" data-dojo-attach-point="activityFeedLoadingBar" class="loading-bar hidden loader-inner ball-clip-rotate">\
					<div></div>\
				</div>\
			</div>',
		postTemplate:
			'<div id="activityPost_{id}" style="opacity:0;display: none;" class="post" data-guid="{guid}">\
				<img src="{userImageUrl}" class="userAvatar" data-user-id="{userAvatarId}">\
				<div class="postData">\
					<div class="actionType">{actionType}</div>\
					<div class="actionDescription">{description}</div>\
					<div class="postDateTime">{postDateTime}</div>\
				</div>\
				<div class="clear"></div>\
			</div>',
		taskLinkTemplate:
			'<span class="{taskClass}" onClick="{onClickHandler}">\
				{targetObjectTitle}\
			</span>',
		posts: [],
		totalLoadedCounter: 0,

		idIterator: 0,

		animationInPostDuration: 500,

		EventType: {
			USER_CREATE: 0,
			USER_LOGIN: 1,
			USER_LOGOUT: 2,
			USER_INVITE: 3,
			USER_UPDATE: 4,
			USER_CHANGE_PASSWORD: 5,
			BUSINESS_ADD_USER: 6,
			BUSINESS_REMOVE_USER: 7,
			BUSINESS_UPDATE: 8,
			ADD_TO_GROUP: 9,
			REMOVE_FROM_GROUP: 10,
			ITEM_DELETE: 11,
			ITEM_MODIFY: 12,
			ITEM_COMPLETE: 13,
			ITEM_ASSIGN: 14,
			ITEM_CREATE: 15,
			ITEM_CREATE_FROM_EMAIL: 16,
			ITEM_ARCHIVE: 17,
			ITEM_RESTORE: 18,
			ITEM_RESTORE_COPY: 19,
			ACCOUNT_SUBSCRIPTION_BUSINESS: 20,
			ACCOUNT_SUBSCRIPTION_PREMIUM: 21,
			EMAIL_NOTIFICATION_SENT: 22,
			PUSH_NOTIFICATION_SENT: 23,
			DESKTOP_NOTIFICATION: 24,
			FILE_ATTACHED: 25,
			BUSINESS_USER_LEAVE: 26,
			ITEM_COMMENT: 27,
			FILE_DETACHED: 28,
			ITEM_UNCOMPLETE: 29,
			ITEM_STARRED: 30,
			ITEM_UNSTARRED: 31
		},
		DesktopEventType: {
			ITEM_ASSIGNED: "ITEM_ASSIGNED",
			ITEM_UNASSIGNED: "ITEM_UNASSIGNED",
			ITEM_MODIFIED: "ITEM_MODIFIED",
			ITEM_DELETED: "ITEM_DELETED",
			COMMENT_ADDED: "COMMENT_ADDED",
			ITEM_COMPLETED: "ITEM_COMPLETED",
			ITEM_RESTORED: "ITEM_RESTORED",
			FILE_ATTACHED: "FILE_ATTACHED",
			PARTICIPANT_ADDED: "PARTICIPANT_ADDED",
			PARTICIPANT_REMOVED: "PARTICIPANT_REMOVED"
		},
		notifyViewSettings: {
			notify_web: true,
			notify_web_item_assigned: true,
			notify_web_item_participant: true,
			notify_web_item_modify: true,
			notify_web_item_priority: true,
			notify_web_item_tags: true,
			notify_web_item_completed: true,
			notify_web_item_deleted: true,
			notify_web_item_comment: true,
			notify_web_item_attachment: true
		},
		targetObjectType: {
			ITEM: 0,
			USER: 1,
			BUSINESS: 2
		},
		userAvatarsMap: {},

		getUserNotifySettings: function() {
			var self = this;
			xhr(Project.api2_full_url + '/user/preferences?session_id=' + Project.api2_session_id, {
				handleAs: "json"
			}).then(function(data) {
				self.notifyViewSettings.notify_web = (data.notify_web == "true" ) ? true : false;
				self.notifyViewSettings.notify_web_item_assigned = (data.notify_web_item_assigned == "true" ) ? true : false;
				self.notifyViewSettings.notify_web_item_participant = (data.notify_web_item_participant == "true" ) ? true : false;
				self.notifyViewSettings.notify_web_item_modify = (data.notify_web_item_modify == "true" ) ? true : false;
				self.notifyViewSettings.notify_web_item_priority = (data.notify_web_item_priority == "true" ) ? true : false;
				self.notifyViewSettings.notify_web_item_tags = (data.notify_web_item_tags == "true" ) ? true : false;
				self.notifyViewSettings.notify_web_item_completed = (data.notify_web_item_completed == "true" ) ? true : false;
				self.notifyViewSettings.notify_web_item_deleted = (data.notify_web_item_deleted == "true" ) ? true : false;
				self.notifyViewSettings.notify_web_item_comment = (data.notify_web_item_comment == "true" ) ? true : false;
				self.notifyViewSettings.notify_web_item_attachment = (data.notify_web_item_attachment == "true" ) ? true : false;
			});
		},

		buildRendering: function() {
			var self = this;
			this.inherited(arguments);
			this.getUserNotifySettings();
			setInterval(lang.hitch(this, this.refreshAvatars), 16000);
		},

		refreshAvatars: function() {
			var newAvatar;

			for (id in this.userAvatarsMap) {
				newAvatar = this.getUserAvatar(id);
				if (newAvatar != this.userAvatarsMap[id]) {
					dojoQuery('#activityFeedContainer .userAvatar[data-user-id="' + id + '"]').forEach(function(x, i) {
						dojoAttr.set(x, 'src', newAvatar);
					});
				}
			}
		},

		getUserAvatar: function(id) {
			return dojo.global.getAvatar(this.AVATAR_SIZE, id, true) || ('/avatar/header.' + this.AVATAR_SIZE + '.png');
		},

		addPost: function(postData, isInitialData) {
			isInitialData = isInitialData || false;
			var inArray = this.posts.filter(function (element) {
				return element.guid == postData.guid;
			});
			if (inArray.length != 0) {
				return;
			}

			if (this.posts.length > 0) {
				dojo.addClass(this.noItemsNode, "hidden");
			}
			var user_picture = this.getUserAvatar(postData.actorUserId),
				description,
				itemLink = '',
				typeLink = '',
				userName,
				newDateTime = new Date(postData.eventTime);

			// Find actor in friends.
			var actorFriend = Project.getFriend(postData.actorUserId);

			if (postData.actorUserId && user_picture) {
				this.userAvatarsMap[postData.actorUserId] = user_picture;
			}

			// Determine if post is new or old.
			var isnewPost = date.compare(newDateTime, this.getOldestPostDateTime()) >=0 ?  true : false;

			// Build datetime format pattern
			var dateFormat = Hi_Preferences.get('date_format'),
				timeFormat = Hi_Preferences.get('time_format'),
				datepattern = '';

			switch (dateFormat) {
				case "d-m-y": datepattern += "dd-MM-yyyy "; break;
				case "m-d-y": datepattern += "MM-dd-yyyy "; break;
				case "y-m-d": datepattern += "yyyy-MM-dd "; break;
				case "d/m/y": datepattern += "dd/MM/yyyy "; break;
				case "m/d/y": datepattern += "MM/dd/yyyy "; break;
			}
			switch (timeFormat) {
				case 12: datepattern += "hh:mm a"; break;
				case 24: datepattern += "k:mm"; break;
			}

			newDateTime = dojo.date.locale.format(newDateTime, {
				formatLength: "short",
				datePattern: datepattern,
				selector: 'date'
			});

			userName = postData.actorName || 'hiTask';
			var targetObjectTypeTitle = '';
			if (postData.targetObjectType == this.targetObjectType.ITEM && postData.eventType == this.EventType.FILE_ATTACHED) {
				targetObjectTypeTitle = 'a file';
			} else if (postData.targetObjectType == this.targetObjectType.ITEM) {
				targetObjectTypeTitle = 'a ' + postData.targetObjectCategoryTitle;
			} else if (postData.targetObjectType == this.targetObjectType.USER) {
				targetObjectTypeTitle = 'an user';
			}

			// Build task link
			var onClickEventHandler = postData.targetObjectCategoryTitle != 'project' ?
				'dojo.global.openTaskPopup(' + postData.targetObjectId +  ',this);' : '';
			itemLink += this.taskLinkTemplate.replace(/\{onClickHandler\}/g, onClickEventHandler)
				.replace(/\{targetObjectTitle\}/g, postData.targetObjectTitle)
				.replace(/\{taskClass\}/g, 'taskLink');
			typeLink += this.taskLinkTemplate.replace(/\{onClickHandler\}/g, onClickEventHandler)
				.replace(/\{targetObjectTitle\}/g, targetObjectTypeTitle)
				.replace(/\{taskClass\}/g, 'typeLink');

			// Build post description.
			description =
				['<span class="dateTime">',newDateTime,'</span>',
				'<span class="userName">',userName, '</span>',
					'<span class="eventTypeTitle">', postData.eventTypeTitle || 'performed action on', '</span>',
					postData.targetObjectType == this.targetObjectType.ITEM ? typeLink : '',
					postData.targetObjectType == this.targetObjectType.ITEM ? itemLink : ''
				].join(' ');

			if (isnewPost) {
				this.posts.push(postData);
			} else {
				this.posts.splice(0, 0, postData);
			}
			this.totalLoadedCounter++;

			// If post is not a Desktop Notification.
			if (postData.eventType != this.EventType.DESKTOP_NOTIFICATION) {

				var innerHtml = '';
				innerHtml += this.postTemplate.replace(/\{id\}/g, this.idIterator++)
					.replace(/\{userAvatarId\}/g, postData.actorUserId)
					.replace(/\{userImageUrl\}/g, user_picture)
					.replace(/\{actionType\}/g, '')
					.replace(/\{description\}/g, description)
					.replace(/\{postDateTime\}/g, '')
					.replace(/\{guid\}/g, postData.guid);
				dojo.place(innerHtml,this.ActivityFeedContainer, (isnewPost) ? 'first' : 'last');

				var feedBlock = dojo.byId('activityPost_' + (this.idIterator-1)),
					self = this;

				dojo.fx.wipeIn({
					node: feedBlock,
					duration: self.animationInPostDuration,
					onEnd: function () {
						dojo.fadeIn({
							node: feedBlock,
							duration: self.animationInPostDuration
						}).play();
					}
				}).play();

				// #4598: clean up a little bit.
				if (isnewPost && !isInitialData && this.countDomElements() > this.MINIMUM_DOM_ELEMENTS_BEFORE_CLEANUP) {
					this.cleanupDom();
				}

			} else if (postData.eventType == this.EventType.DESKTOP_NOTIFICATION && Project.user_id == postData.actorUserId) {
				// Check if we already displayed this notification.
				if (this.isNotificationRecent(postData)) {
					this.rememberRecentNotificationTime(postData);

					// If post is a desktop notification and actor is me.

					var desktopNotifyData = JSON.parse(postData.description),
						executor = Project.getFriend(desktopNotifyData.executorId),
						executorPicture = (executor) ? executor.image24src.replace(/\.24\./g, '.64.') : '';

					userName = Project.getFriendName(desktopNotifyData.executorId) || '';
					description = desktopNotifyData.subject;

					//Show notify on new post received
					if (!this.notifyViewSettings.notify_web && isnewPost === true) {
						if (desktopNotifyData.type == this.DesktopEventType.ITEM_ASSIGNED && this.notifyViewSettings.notify_web_item_assigned) {
							dojo.global.showNotify(userName, executorPicture, description);
						} else if (desktopNotifyData.type == this.DesktopEventType.ITEM_UNASSIGNED && this.notifyViewSettings.notify_web_item_assigned) {
							dojo.global.showNotify(userName, executorPicture, description);
						} else if (desktopNotifyData.type == this.DesktopEventType.PARTICIPANT_ADDED && this.notifyViewSettings.notify_web_item_participant) {
							dojo.global.showNotify(userName, executorPicture, description);
						} else if (desktopNotifyData.type == this.DesktopEventType.PARTICIPANT_REMOVED && this.notifyViewSettings.notify_web_item_participant) {
							dojo.global.showNotify(userName, executorPicture, description);
						} else if (desktopNotifyData.type == this.DesktopEventType.ITEM_MODIFIED && this.notifyViewSettings.notify_web_item_modify) {
							dojo.global.showNotify(userName, executorPicture, description);
						} else if (desktopNotifyData.type == this.DesktopEventType.ITEM_MODIFIED && this.notifyViewSettings.notify_web_item_priority) {
							dojo.global.showNotify(userName, executorPicture, description);
						} else if (desktopNotifyData.type == this.DesktopEventType.ITEM_MODIFIED && this.notifyViewSettings.notify_web_item_tags) {
							dojo.global.showNotify(userName, executorPicture, description);
						} else if (desktopNotifyData.type == this.DesktopEventType.ITEM_COMPLETED && this.notifyViewSettings.notify_web_item_completed) {
							dojo.global.showNotify(userName, executorPicture, description);
						} else if (desktopNotifyData.type == this.DesktopEventType.ITEM_RESTORED && this.notifyViewSettings.notify_web_item_completed) {
							dojo.global.showNotify(userName, executorPicture, description);
						} else if (desktopNotifyData.type == this.DesktopEventType.ITEM_DELETED && this.notifyViewSettings.notify_web_item_deleted) {
							dojo.global.showNotify(userName, executorPicture, description);
						} else if (desktopNotifyData.type == this.DesktopEventType.COMMENT_ADDED && this.notifyViewSettings.notify_web_item_comment) {
							dojo.global.showNotify(userName, executorPicture, description);
						} else if (desktopNotifyData.type == this.DesktopEventType.FILE_ATTACHED && this.notifyViewSettings.notify_web_item_attachment) {
							dojo.global.showNotify(userName, executorPicture, description);
						}
					} else {
						if (isnewPost === true) {
							dojo.global.showNotify(userName, executorPicture, description);
						}
					}
				}
			}

			if (this.countDomElements() > 0) {
				dojo.addClass(this.noItemsNode, "hidden");
			}
		},

		countDomElements: function() {
			return dojoQuery('#activityFeedContainer .post').length;
		},

		cleanupDom: function(amount) {
			amount = amount || 1;
			for (var x = 0; x < amount; x++) {
				var last = dojoQuery('#activityFeedContainer .post').last();
				last = last && last.length == 1 ? last[0] : null;
				if (last) {
					var guid = dojoAttr.get(last, 'data-guid');
					for (var i = this.posts.length - 1; i--; ) {
						if (this.posts[i].guid === guid) {
							this.posts.splice(i, 1);
						}
					}
					domConstruct.destroy(last);
				}
			}
			this.emit('domCleanup');
		},

		rememberRecentNotificationTime: function(event) {
			if (this.isNotificationRecent(event)) {
				dojoCookie(this.RECENT_NOTIFICATION_COOKIE_NAME, event.eventTime, {expires: 365});
			}
		},

		isNotificationRecent: function(event) {
			var cookie = dojoCookie(this.RECENT_NOTIFICATION_COOKIE_NAME);
			var time = cookie ? new Date(cookie) : null;
			var newTime = event && event.eventTime ? new Date(event.eventTime) : null;
			if (newTime && (!time || newTime > time)) {
				return true;
			} else {
				return false;
			}
		},

		getOldestPostDateTime: function() {
			if (this.posts.length == 0) {
				return false;
			} else {
				return new Date(this.posts[this.posts.length - 1].eventTime);
			}
		},

		getPostsCount: function() {
			return this.totalLoadedCounter;
		}
});

	return ActivityFeedContainer;
});
