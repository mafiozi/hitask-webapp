define(['dojo/aspect'], function (aspect) {
	
	return dojo.global.Sound = {
		audio: null,
		
		fn: 'require',
		obj: dojo,

		loaded: false,

		play: function (index) {
			if (!this.loaded) {
				try {
					this.load();
				} catch (e) {
					//Problems playing sound, possibly no audio device!
				}
			}
			var audio = document.getElementById('hiAudio_' + index);
			
			if (!audio) return false;
			
			audio.play();
		},

		_createAudio: function(index, src) {
			var audio = document.createElement('audio');
			audio.id = 'hiAudio_' + index;
			var source = document.createElement('source');
			source.type = 'audio/mpeg';
			source.src = Project.api2_base_url + '/public/app/audio/' + index + '.mp3';
			audio.appendChild(source);

			return audio;
		},

		load: function (snd) {
			if (this.loaded) return;
			
			var div = document.createElement('div'), audio;
			div.id = 'audioContainer';

			for (var i = 1; i < 7; i++) {
				audio = this._createAudio(i);
				div.appendChild(audio);
				// self.audio.load({url: Project.api2_base_url + '/public/app/audio/' + i + '.mp3', id:'snd'+i});
			}

			document.body.appendChild(div);
			this.loaded = true;
			return;
		}
	};

});