define([
	"dojo/_base/array", // array.forEach array.map
	"dojo/_base/declare", // declare
	"dojo/cldr/supplemental", // cldrSupplemental.getFirstDayOfWeek
	"dojo/date", // date
	"dojo/date/locale",
	"dojo/dom", // dom.setSelectable
	"dojo/dom-class", // domClass.contains
	"dojo/_base/event", // event.stop
	"dojo/_base/lang", // lang.getObject, lang.hitch
	"dojo/_base/sniff", // has("ie") has("webkit")
	"dojo/string", // string.substitute
	"dojo/_base/window", // win.doc.createTextNode
	"dijit/_Widget",
	"dijit/_Templated",
	"dojo/text!./itemnew.html",
	"dojo/NodeList-traverse",
	"hi/widget/task/item",
	"hi/widget/task/itemnewedit",
	/* #2757 */
	"dijit/_WidgetsInTemplateMixin"
], function(array, declare, cldrSupplemental, date, local, dom, domClass, event, lang, has, string, win,
			_WidgetBase, _TemplatedMixin, template, NodeList, TaskItem, TaskItemNewEdit, _WidgetsInTemplateMixin){
	
	// module:
	//		hi/widget/task/itemnew
	// summary:
	//		Task item which is used for new item, but only in top bar
	
	var TaskItemNew = declare("hi.widget.TaskItemNew", [/* #2757 */_WidgetsInTemplateMixin, TaskItem], {
		// summary:
		//		Task item which updates itself based on attributes
		
		// Template for the list
		templateString: template,
		
		/* #2757 */
		widgetsInTemplate: true,
		
		// Localization
		localeNewItemText: hi.i18n.getLocalization('project.enter_item_name'),
		localeAdd: hi.i18n.getLocalization('common.add'),
		
		// Remove task when collapsed
		removeOnCollapse: false,
		
		
		/* ----------------------------- ATTRIBUTES ------------------------------ */
		
		
		/* Task ID, widgets reserve id attribute */
		taskId: null,
		_setTaskIdAttr: function (taskId) {
			//If no taskId given, then add some fake one
			taskId = taskId || --TaskItem.ID_COUNTER;
			
			if (this.taskId) {
				hi.items.manager.removeItem(this);
			}
			if (taskId) {
				hi.items.manager.addItem(taskId || this.id, this, this.listName || (this.recurring ? this.get("eventdate") : null));
			}
			
			this.childrenNode.setAttribute("data-task-id", taskId || "");
			this.itemNode.setAttribute("data-task-id", taskId || "");
			this.itemNode.taskId = taskId;
			this._set("taskId", taskId);
		},
		
		/* Parent */
		parent: 0,
		_setParentAttr: function (value) {
			this._set("parent", parent);
			this.parentHiddenInput.value = parent;
		},
		
		/* Type*/
		category: 1,
		_setCategoryAttr: function (category) {
			this._set("category", category);
		},
		
		
		/* Priority */
		priority: 20000,
		_setPriorityAttr: function (value) {
			this._set("priority", value);
		},
		
		
		/* Title */
		title: "",
		_setTitleAttr: function (title) {
			this._set("title", title);
		},
		
		
		/* Starred */
		starred: false,
		_setStarredAttr: function (value) {
			this._set("starred", value);
		},
		
		
		/* Event date */
		eventdate: "",
		_getEventdateAttr: function () {
			return "";
		},
		_setEventdateAttr: function (eventdate) {
			this._set("eventdate", "");
		},
		
		
		/* Completed */
		completed: false,
		_setCompletedAttr: function (value) {
			this._set("completed", false);
		},
		
		/* Color */
		color: 0,
		_setColorAttr: function (color) {
			this._set("color", color);
		},
		
		/* Children */
		childrenRendered: true,
		itemWidgets: null,
		
		children: 0,
		_setChildrenAttr: function (children) {
			this._set("children", 0);
		},
		
		
		/* Dragable */
		dragVisibleClass: "hidden",
		_setDragVisibleClassAttr: function (dragVisibleClass) {
			this._set("dragVisibleClass", false);
		},
		
		
		/* Date */
		time_track: false,
		time_est: 0,
		
		due_date: "",
		end_date: "",
		end_time: "",
		start_date: "",
		start_time: "",
		
		_setEnd_dateAttr: function (end_date) {
			if (end_date && !end_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[\d\+:]+$/)) {
				var time = this.get("end_time");
				if (this.is_all_day || !time) {
					end_date = time2strAPI(HiTaskCalendar.getDateFromStr(end_date));
				} else {
					end_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(end_date, time));
				}
			}
			
			this._set("end_date", end_date);
			this.set("dateTitle", Math.random());
		},
		_setEnd_timeAttr: function (end_time) {
			var end_date = this.get("end_date");
			if (end_date) {
				end_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(end_date, end_time));
			} else {
				end_date = time2strAPI(HiTaskCalendar.getTimeFromStr(end_time));
			}
			
			this.set("end_date", end_date);
			this._set("end_time", end_time);
		},
		_setStart_dateAttr: function (start_date) {
			if (start_date && !start_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[\d\+:]+$/)) {
				var time = this.get("start_time");
				if (this.is_all_day || !time) {
					start_date = time2strAPI(HiTaskCalendar.getDateFromStr(start_date));
				} else {
					start_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(start_date, time));
				}
			}
			
			this._set("start_date", start_date);
			this.set("dateTitle", Math.random());
		},
		_setStart_timeAttr: function (start_time) {
			var start_date = this.get("start_date");
			if (start_date) {
				start_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(start_date, start_time));
			} else {
				start_date = time2strAPI(HiTaskCalendar.getTimeFromStr(start_time));
			}
			
			this.set("start_date", start_date);
			this._set("start_time", start_time);
		},
		
		_getEnd_dateAttr: function () {
			var value = this.end_date;
			if (value) value = time2str(str2time(value, "y-m-d"), Hi_Calendar.format);
			return value || "";
		},
		_getStart_dateAttr: function () {
			var value = this.start_date;
			if (value) value = time2str(str2time(value, "y-m-d"), Hi_Calendar.format);
			return value || "";
		},
		_getEnd_timeAttr: function () {
			var value = this.end_date;
			
			if (this.is_all_day) return "";
			
			if (value) value = str2timeAPI(this.end_date);
			if (value) value = time2str(value, "h:i");
			return value || "";
		},
		_getStart_timeAttr: function () {
			var value = this.start_date;
			
			if (this.is_all_day) return "";
			
			if (value) value = str2timeAPI(this.start_date);
			if (value) value = time2str(value, "h:i");
			return value || "";
		},
				
		
		dateTitle: "",
		_setDateTitleAttr: function () {
			this._set("dateTitle", "");
		},
		
		/* Assignee */
		assignee: 0,
		_setAssigneeAttr: function (value) {
			this._set("assignee", value);
		},
		
		/* Children expanded */
		expanded: false,
		_setExpandedAttr: function (expanded) {
			this._set("expanded", false);
		},
		
		/* Opened, task expanded view */
		opened: false,
		_setOpenedAttr: function (opened) {
			//Don't open if some other item is being edited
			if (opened && hi.items.manager.editing()) return;
			
			this._set("opened", opened);
			hi.items.manager.opened(this.taskId || this.id, opened ? true : false);
			
			this.set("editing", opened);
			/* #3088 */
			dojo.removeClass(this.propertiesViewNode,'hidden');
		},
		
		/* Editing */
		editing: false,
		_setEditingAttr: function (editing) {
			this._set("editing", editing);
			
			var grouping = Hi_Preferences.get('currentTab', 'my', 'string');
			if (grouping === 'activity' || grouping === 'calendar') {return;}
			if (!this.opened && editing) this.set("opened", true);
			
			if (editing) {
				dojo.addClass(this.domNode, "new-highlight");
				dojo.addClass(this.domNode, "highlight-edit");
				dojo.addClass(this.headingNode, "hidden");
				
				this.renderEditingView();
				
				hi.items.manager.editing(this.taskId || this.id, true); // TaskId will be empty for new item
				this.editingView.expand();
			} else {
				this.editingView.collapse().then(dojo.hitch(this, function () {
					//Unstyle when animation is done
					dojo.removeClass(this.domNode, "new-highlight");
					dojo.removeClass(this.domNode, "highlight-edit");
					dojo.removeClass(this.headingNode, "hidden");
					dojo.removeClass(dojo.byId('main'), 'itemNewEdit');
				}));
				hi.items.manager.editing(this.taskId || this.id, false); // TaskId will be empty for new item
			}
		},
		
		/* Recurring event instance dates */
		recurring: 0,
		instances: null,
		_setInstancesAttr: function (instances) {
			this._set("instances", instances);
		},
		
		checkOverdue: function () {
			// summary:
			//		Check if task is overdue and adjust style
			
			this.set("styleOverdue", false);
			this.set("styleCurrent", false);
		},
		
		
		/* ----------------------------- CONSTRUCTOR ------------------------------ */
		
		
		constructor: function (/*Object*/args) {
			this.itemWidgets = {};
		},
		
		uninitialize: function () {
			if (this.editing) {
				this.editingView.save();
			}
			
			if (this.taskId) {
				hi.items.manager.removeItem(this);
			}
		},
		
		buildRendering: function () {
			this.inherited(arguments);
			
			//Will need for file upload
			this.renderEditingView();
		},
		
		buildDragAndDrop: function () {
			// summary:
			//		Attach drag and drop
			
			// Can't drag or drop new item
		},
		
		renderChildren: function () {
			// summary:
			//		Render all children
			
			// Can't have children
		},
		
		renderChild: function (item) {
			// summary:
			//		Render childre task
			// item:
			//		Children item data
			
			return null;
		},
		
		renderExpandedView: function () {
			// summary:
			//		Render expanded view widget
			
			// Can't have properties view, only editing view
		},
		
		renderEditingView: function () {
			// summary:
			//		Render editing view widget
			
			if (this.editingView) return;
			
			var view = this.editingView = new hi.widget.TaskItemNewEdit({
				"task": this
			});
			
			view.placeAt(this.propertiesViewNode);
			view.startup();
			
			this._supportingWidgets.push(view);
		},
		
		
		/* ----------------------------- COLLAPSE/EXPAND API ------------------------------ */
		
		
		openEdit: function (name) {
			// summary:
			//		Open edit view
			
			if (Project.checkLimits("TRIAL_TASKS") && !this.editing) {
				this.set("editing", true);
				this.editingView.focusInput(name);
			}
		},
		
		
		/* ----------------------------- DATA API ------------------------------ */
		
		
		save: function (properties) {
			// summary:
			//		Save item properties, write to store
			// properties: {Object}
			//		Properties which will be written
			
			// Can't save, because this is new item and saving is handled
			// by TaskItemEditNew
		},
		
		showEditForm: function (event) {
			// summary:
			//		Show edit form
			// Bug #5183
			if (has('ie') || has('trident')) event.target && event.target.blur();
			/* Bug #2001 - block if there is a new task opened */
			/* #2589 */
			if (Hi_Search.isFormVisible()) Hi_Search.closeTooltipDialog('search');
			if ((hi.items.manager.openedId() && hi.items.manager.openedId() < 0) || hi.items.manager.editing()) {
				return false;
			}
			
			// #2649
			if (hi.items.manager.hasEditingItemCurrently()) {
				Project.alertElement.showItem(14,{task_alert:'Please finish editing opened item'});
				return false;
			}
			
			dojo.addClass(dojo.byId('main'), 'itemNewEdit');
			Project.hideSortingAndFilterPopups(); // #2176
			Project.hideProjectDialog(); // #2115
			
			this.set("editing", Project.checkLimits("TRIAL_TASKS"));
		},
		
		/**
		 * #3361
		 */
		handleTitleNewKeydown: function(event) {
			if (hi.items.manager.editing()) {
				if (event) {
					dojo.stopEvent(event);
				}
				return false;
			}
		}
	});
	
	return TaskItemNew;

});