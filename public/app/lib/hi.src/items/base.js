/**
 * hi.items.manager
 * 
 * Specifies the local datastore of items
 * 
 * Items are store in the hi.items.manager._items hash, where keys are item IDs, and the values are hashes of various
 * available instances of items.  For example, if the item is not recurring and just appears on one of the lists, the
 * key to the second hash is 'list'.  For each recurring item, the key will be an instance date:
 * 
 * {
 *    NON_RECUR_ID1:{
 *       list:{...}
 *    },
 *    RECUR_ID2:{
 *       instance_date1:{...},
 *       instance_date2:{...},
 *       ...
 *       instance_dateN:{...}
 *    }
 * }
 * 
 * Note that individual list widgets (e.g., TaskListToday, TaskListUngrouped, etc.), maintain a list of their own items that
 * basically reference the items in the ._items hash.
 * 
 * This object also specifies how items are saved, both locally and to the server, with methods like remove, removeSave, set, 
 * and setSave.
 * 
 * This object also mantains a list of opened (expanded) items and items in edit mode
 */
define([
	'hi/store',
	'dojo/json',
	'dojo/_base/array',
	'hi/widget/task/item_DOM'
], function (hi_store, JSON, array, item_DOM) {
	
	// hi.items is namespace for Project lists, Task lists, groups, etc.
	var hi_items = hi.items = {};
	
	// Object to access any TaskItem and manage opened items
	hi_items.manager = {
		_items: {},
		
		_opened: [],
		
		_editing: [],
		
		_lastPushedId: null,
		
		
		/* ------------------------------- ITEM API ------------------------------- */
		addItem: function (id, item, name) {
			// summary:
			//		Add task item to the list
			// id:
			//		Task ID
			// item:
			//		hi.widget.TaskItem instance
			// name:
			//		Specific name, for example: "list" or "tooltip"
			
			if (!this._items[id]) this._items[id] = {};
			this._items[id][name || "list"] = item;
		},
		
		removeItem: function (id, item) {
			// summary:
			//		Remove item to the list
			// id:
			//		Task ID
			// item:
			//		Task item or specific name, for example: "list" or "tooltip"
			var items = this._items,
				key,
				name,
				taskId = null;

			if (typeof id === "object") {
				if (items[id.taskId]) {
					name = null;
					for (name in items[id.taskId]) {
						if (items[id.taskId][name] === id) {
							taskId = id.taskId;
							/*
							 * #3333 - tear down drag source properly; this should remove references to DOM nodes and event listeners
							 */
							this._tearDownItem(items[id][name]);

							delete(items[id.taskId][name]);
							
							//Remove if this was last item
							for (key in items[id]) return;
							delete(items[id]);
						}
					}
				}
				/* Bug #2092,#2095,#2132 - sometimes just need to inspect the object that was passed in to find the taskId 
				 * until we figure out a better way to manage these widgets */
				if (!taskId) taskId = id.taskId;
			} else if (items[id]) {
				if (typeof item === "string" || typeof item === "undefined") {
					//name was 
					name = item || "list";
					if (items[id][name]) {
						taskId = id;
						/*
						 * #3333
						 */
						this._tearDownItem(items[id][name]);
						delete(items[id][name]);
					
						//Remove if this was last item
						for (key in items[id]) return;
						delete(items[id]);
					}
				} else if (typeof item === "object") {
					key = null;
					for (key in items[id]) {
						if (items[id][key] === item) {
							taskId = id;
							/*
							 * #3333
							 */
							this._tearDownItem(item);
							
							delete(items[id][key]);
							
							//Remove if this was last item
							for (key in items[id]) return;
							delete(items[id]);
						}
					}
				}
			}
			
			//Remove from opened and editing lists
			if (dojo.indexOf(this._editing, taskId) != -1) {
				this.editing(taskId, false);
			}
			if (dojo.indexOf(this._opened, taskId) != -1) {
				this.opened(taskId, false);
			}
		},
		
		/*
		 * #3333
		 * tear down widgets that might possibly be associated with an item that is about to be removed
		 */
		_tearDownItem:function(item)
		{
			if (!item) return;
			if (item.dragSource && item.dragSource.destroy) item.dragSource.destroy();
			if (item.propertiesView) 
			{
				item.propertiesView.destroyRecursive();
				delete item.propertiesView;
			}
			if (item.editingView) 
			{
				item.editingView.destroyRecursive();
				delete item.editingView;
			}
		},
		
		item: function (id, name) {
			// summary:
			//		Returns item widget from the list, hi.widget.TaskItem instance
			//		If name is specified searches by that name
			// id:
			//		Task ID
			
			var items = this._items,
				item = items[id],
				taskItem = hi.items.manager.get(id),
				key;

			if (item) {
				if (name) {
					return item[name] || null;
				} else {
					if(taskItem){
						for (key in item){
							if(taskItem.recurring){
								if(key !== 'list' && key !== 'tooltip'){ 
									return item[key];
								}
							}else{
								if(key === 'list' || key === 'tooltip'){
									return item[key];
								}
							}
						}
					}else{
						for(key in item){
							return item[key];
						}
					}
				}
			}
			return null;
		},
		
		/**
		 * Get item from specified node
		 */
		getItemByNode: function(node,taskId) {
			if (!node) return null;
			var checkNode = dojo.byId(node.id.replace('itemNode','completeNode'));
			var eventdate = dojo.dom.getAllAttributes(checkNode).eventdate || null;
			if (eventdate && eventdate.match(/\d\d\d\d-\d\d-\d\d/)) {
				return hi.items.manager._items[taskId][eventdate].taskItem;
			} else {
				return hi.items.manager._items[taskId].list.taskItem;
			}

		},
		
		
		allItems: function (id) {
			// summary:
			//		Returns all items widgets from the list matching ID, hi.widgetTaskItem instance
			// id:
			//		Task ID
			
			return this._items[id] || {};
		},
		
		itemAttr: function (id, key) {
			// summary: 
			//		Returns item attribute
			// id:
			//		Task ID
			// key:
			//		Attribute name
			
			var item = this.item(id);
			if (item) return item.get(key);
			return null;
		},
		
		
		/* ------------------------------- DATA API ------------------------------- */
		
		
		get: function (id, key) {
			// summary:
			//		Returns item data or property from item data if key argument is specified
			//		If id is object, then assumes id is query filter and return array of matches for query
			//		If id and key is not specified, then return all data
			// id:
			//		Task ID
			// key:
			//		Attribute name

			//ProjectCache is created async,
			//there may be possibility that ProjectCache is not ready when this function is called
			if(!hi.store || !hi.store.ProjectCache){ return null; }
			
			if ((typeof id === "object" || id === undefined || id === null || typeof(id) == 'function') && !key) {
				//Return query
				return hi.store.ProjectCache.query(id, {
					sort: typeof(hi.store.Project.getOrderConfiguration) != 'undefined' ? hi.store.Project.getOrderConfiguration() : hi.store.Project.getOrder()
				});
			}
			
			var data = hi.store.ProjectCache.get(id);
			
			if (key && data) {
				return key in data ? data[key] : undefined; 
			} else {
				return data;
			}
		},
		
		set: function (id, data, silent) {
			// summary:
			//		Update data without saving it to server
			// id: Number
			//		Task ID, optional
			// data: Object
			//		Data to set. If id attribute is ommited then must contain "id" property
			// silent: Boolean
			//		If true will not push all update to listeners

			if (data && typeof data === "object" && id) {
				data = dojo.mixin({"id": id}, data);
			} else if (typeof id === "object" && !data) {
				data = id;
			}
			
			if (data.id) {
				var item = hi.store.ProjectCache.get(data.id);
				if ("completed" in data) {
					if (data.completed) {
						this._setCompleted(data);
					}
					if (item && item.recurring && item.instances && item.instances.length) {
						//Update instance data
						data.instances = [].concat(item.instances);
						
						if (data.instance_date) {
							//if instance_date specified then update that instance
							for (var i=0, ii=data.instances.length; i<ii; i++) {
								if (data.instances[i].start_date == data.instance_date) {
									data.instances[i].status = data.completed ? 1 : 0;
									break;
								}
							}
							delete(data.instance_date);
						} else {
							//otherwise update first instance
							data.instances[0].status = data.completed ? 1 : 0;
						}
						
						data.completed = false;
					}
				}
				data = dojo.mixin({}, item || {}, data);
				hi.store.ProjectCache.put(data);
				// we must call "notifyComplete" to push all updates to listeners, for ...Cache only
				if (!silent) {
					hi.store.ProjectCache.notifyComplete();
				}
				return true;
			} else {
				return false;
			}
		},
		
		remove: function (id, formData) {
			// summary:
			//		Remove item from data without saving to server
			// id:
			//		Task ID
			// instance_date:
			//		Optional, instance date
			
			if (formData && String(formData.cascade) === "0") {
				//Don't remove, move out of project
				dojo.forEach(hi.items.manager.get({"parent": id}), dojo.hitch(this, function (item) {
					/* Bug #2112 - we only move items that exist in the store;
					 * so negative IDs are excluded here and are just collapsed, 
					 * which should be a graceful way of utlimately eliminating them from the DOM
					 */
					if (item.id >0) this.set(item.id, {"parent": 0}, true);
					else
					{
						this.item(item.id).collapse();
					}
				}));
			} else {
				//Remove children
				this.removeRecursive(id);
			}
			
			hi.store.ProjectCache.notifyComplete();
		},
		
		removeRecursive: function (id) {
			//Remove item
			hi.store.ProjectCache.remove(id);
			
			this.opened(id, false);
			this.editing(id, false);
			
			//and children
			var children = hi.store.ProjectCache.query({"parent": id}),
				ii = children.length,
				i = 0;
			
			for (; i<ii; i++) {
				this.removeRecursive(children[i].id);
			}
			
		},
		
		refresh: function (clean) {
			// summary:
			//		Sync data with server
			// clean: Bollean
			//		Reload all data
			
			return hi.store.Project.refresh(clean);
		},
		
		postTimeout: 15 * 1000,
		
		setSave: function (id, data) {
			// summary:
			//		Save data
			// id:
			//		Task ID, optional
			// data:
			//		Data to save. If id attribute is ommited then must contain "id" property
			var doAdd = false,
				manytasks = false,
				isSubItem = false, isRecurring;

			if (data && typeof data === "object" && id) {
				data = dojo.mixin({"id": id}, data);
			} else if (typeof id === "object" && !data) {
				data = id;
			}
			isRecurring = data ? data.recurring : false;
			/* #3088 - this tells setSave() which Saving... throbber to show and which form to hide */
			if (data.isSubItem) isSubItem = true;
			if (data.manytasks) manytasks = true;
			delete data.isSubItem;

			if ("completed" in data) {
				hi.items.manager._setSaveCompleted(data);
				
				if (!data.instance_date) {
					var date = hi.items.date.getEventDate(data.id);
					if (date) {
						/* Bug #1203 - use instance_date_time instead of instance_date */
						data.instance_date_time = date;
					}
				}
				
				// #2543
				if (data.eventDateAttr /* #2776 */&& data.recurring) {
					data.instance_date_time = data.eventDateAttr;
					delete data.eventDateAttr;
					delete data.recurring;
				}
			}
			if (data.hasOwnProperty('permissions')) {
				data.cascadeAcl = 1;
			}
			
			var deferred = null, callback;
			
			if (data.id) {
				deferred = hi.store.Project.put(data);
			} else {
				doAdd = true;
				if (isSubItem) {
					var loadingEl = dojo.byId('itemEditSaveThrobber');
					if (loadingEl) {
						dojo.removeClass(loadingEl, 'hidden');
					}
				} else if (data.category != 0 && !manytasks) {
					dojo.removeClass(dojo.query('.itemNewThrobberMsg')[0],'hidden');
					dojo.addClass(dojo.query('#itemPropertiesViewNode')[0],'hidden');
				}
				data.permission = Hi_Permissions.EVERYTHING;
				deferred = hi.store.Project.add(dojo.mixin(data,{timeout:hi.items.manager.postTimeout,doAdd:doAdd,isSubItem:isSubItem,manytasks:manytasks}), {}, hi.items.manager._handleRequestError);
			}

			if ('assignee' in data && data.assignee !== 0) {
				callback = function(id) {
					var assignee = data.assignee,
						item, permissions, pm;

					item = id && hi.items.manager.get(id);
					permissions = item && item.permissions;
					permissions = typeof permissions === 'string' ? JSON.parse(permissions) : dojo.clone(permissions);
					if (permissions && assignee !== Project.user_id) {
						pm = Hi_Actions.getPermissionByUserId(permissions, assignee);

						if (!pm) {
							permissions.push({principal: assignee, level: Hi_Permissions.COMPLETE_ASSIGN});
							hi.items.manager.set(id, {'permissions': permissions});
						}
						if (pm && pm < Hi_Permissions.COMPLETE_ASSIGN) {
							// permission object
							var po = Hi_Actions.getPermissionObjByUserId(permissions, assignee);

							if (po) {
								po.level = Hi_Permissions.COMPLETE_ASSIGN;
							}
							hi.items.manager.set(id, {'permissions': permissions});
							// item_DOM.set(hi.items.manager.item(id), 'permissions', permissions);
						}
						console.log('assignee set permission');
					}
				};
				if (data.id) {
					callback(data.id);
				} else {
					deferred.then(callback);
				}
			}

			if ('participants' in data && data.participants.length) {
				callback = function(id) {
					var parts = data.participants,
						item, permissions;

					array.forEach(parts, function(part) {
						item = id && hi.items.manager.get(id);
						permissions = item && item.permissions;
						permissions = typeof permissions === 'string' ? JSON.parse(permissions) : dojo.clone(permissions);
						if (permissions && parseInt(part, 10) !== Project.user_id) {
							pm = Hi_Actions.getPermissionByUserId(permissions, part);

							if (!pm) {
								permissions.push({principal: part, level: Hi_Permissions.COMPLETE_ASSIGN});
								hi.items.manager.set(id, {'permissions': permissions});
							}
							if (pm && pm < Hi_Permissions.COMPLETE_ASSIGN) {
								// permission object
								var po = Hi_Actions.getPermissionObjByUserId(permissions, part);

								if (po) {
									po.level = Hi_Permissions.COMPLETE_ASSIGN;
								}
								hi.items.manager.set(id, {'permissions': permissions});
								// item_DOM.set(hi.items.manager.item(id), 'permissions', permissions);
							}
							console.log('participants set permission');
						}
					});
				};
				if (data.id) {
					callback(data.id);
				}else{
					deferred.then(callback);
				}
			}
			// Recurring item completed states
			if ("completed" in data) {
				var item = hi.store.ProjectCache.get(data.id);
				if (item && item.recurring && item.instances && item.instances.length) {
					var props = {
						"completed": data.completed
					};
					// Bug #1203 - use instance_date_time instead of instance_date
					if (data.instance_date) {
						props.instance_date = data.instance_date;
					} else if (data.instance_date_time) {
						props.instance_date = time2str(new Date(data.instance_date_time),'y-m-d');
					}
					this.set(id, props);
				}
			}
			
			// If recurring event changed then update instance date
			var df = new dojo.Deferred();
			var errHandler = dojo.hitch(hi.items.manager, function (error) {
				// summary:
				//		Handle requets error
				// error: Object
				//		Error object
				
				hi.items.manager._handleRequestError(error, dojo.mixin(data,{doAdd:doAdd}));
			});
			
			if ("recurring" in data || "start_date" in data || "end_date" in data || isRecurring) {
				if (data.id) {
					hi.items.manager._setSaveRecurring(data.id, data);
					
					if (hi.items.manager._items[id]) {
						var instances = hi.items.manager.get(id).instances;
						for (var i in hi.items.manager._items[id]) {
							if (hi.items.manager._items[id][i].taskItem) {
								hi.items.manager._items[id][i].taskItem.instances = instances;
							}
						}
					}
					deferred.then(function(id) {
						df.resolve(id);
					}, errHandler);
				} else {
					deferred.then(dojo.hitch(hi.items.manager, function (id) {
						/* #2766 */
						if (id) {
							hi.items.manager._setSaveRecurring(id, data);
							if (hi.items.manager._items[id]) {
								var instances = hi.items.manager.get(id).instances;
								for (var i in hi.items.manager._items[id]) {
									if (hi.items.manager._items[id][i].taskItem) {
										hi.items.manager._items[id][i].taskItem.instances = instances;
									}
								}
							}
						}
						df.resolve(id);
					}), errHandler);
				}
			} else {
				deferred.then(function(id) {
					df.resolve(id);
				}, errHandler);
			}
			
			// After save highlight item
			df.then(function(id) {
				// summary:
				//		After save highlight item if change wasn't priority
				/* #2592 - check for error_response */
				/* #2766 */ 
				if (!id) {
					console.log('error');
					if (isSubItem) {
						if (dojo.byId('itemEditSaveThrobber')) {
							dojo.addClass(dojo.byId('itemEditSaveThrobber'),'hidden');
						}
						if (dojo.byId('itemEditForm')) {
							dojo.removeClass(dojo.byId('itemEditForm'), 'hidden');
						}
						hi.items.manager.removeLastNegative();
					} else if (!manytasks) {
						dojo.addClass(dojo.query('.itemNewThrobberMsg')[0],'hidden');
						dojo.removeClass(dojo.query('#itemPropertiesViewNode')[0],'hidden');
					}
					return;
				}
				
				hi.items.manager._lastPushedId = id;
				
				if (!manytasks) {
					var loadingEl = dojo.byId('itemEditSaveThrobber');
					if (isSubItem && loadingEl) dojo.addClass(loadingEl,'hidden');
					var throbberMessage = dojo.query('.itemNewThrobberMsg');
					if (throbberMessage.length > 0) {
						dojo.addClass(throbberMessage[0], 'hidden');
					}
				}
				
				//dojo.removeClass(dojo.query('#itemPropertiesViewNode')[0],'hidden');

				if (id && id.response_status) {
					var msg = id.error_message || 'Unable to create a task at this time';
					Project.alertElement.showItem(14, {task_alert: msg});
					hi.items.manager.get().forEach(function(item) {if (item.id<0) hi.items.manager.remove(item.id);});
					return;
				}
				var hasSignificantChange = false;
				for (var key in data) {
					if (key !== "id" && key !== "priority") {
						hasSignificantChange = true; break;
					}
				}
				
				if (hasSignificantChange) {
					if (data.id) {
						//Existing item was updated
						var item = hi.items.manager.item(data.id);
						if (item) item_DOM.highlight(item,data.instance_date_time?time2str(str2timeAPI(data.instance_date_time),'y-m-d'):null);
					} else if (id) {
						//New item, wait till it's created
						setTimeout(function () {
							var item = hi.items.manager.item(id);
							if (item) item_DOM.highlight(item);
						}, 100);
					}
				}
				
				if (typeof(Hi_Reminder) != 'undefined') {
					Hi_Reminder.setReminder(id);
				}
				
				if (data.instance_date_time) {
					HiTaskCalendar.update();
				}
			}, errHandler);
			
			return deferred;
		},
		
		getLastNegative: function() {
			var items = this.get();
			var min = 0;
			for (var i = 0, len = items.length; i < len; i++) {
				min = Math.min(min, parseInt(items[i].id));
			}
			return min < 0 ? min : null;
		},
		
		removeLastNegative: function() {
			hi.items.manager.remove(hi.items.manager.getLastNegative());
		},
		
		removeSave: function (id, formData) {
			// summary:
			//		Remove item, sent to server
			// id:
			//		Task ID
			// formData:
			//		Optional, formData
			/* Bug #1940 - return the defer from the DELETE call to server so we can add callbacks to it via .then() */
			var deferred;
			if (id > 0) {
				//Can't remove new item
				var directives = {};
				if (formData) {
					directives.formData = formData;
				}
				
				deferred = hi.store.Project.remove(id, directives);
			}
			
			this.remove(id, formData);
			return deferred;
		},
		
		getIsCompleted: function (id, instance_date, compareDateOnly) {
			// summary:
			//		Returns true if item is completed or false if it isn't
			//		For recurring items returns if instance is completed
			// id: Number
			//		Task ID or task data
			
			var data = typeof id === "object" ? id : this.get(id);
			
			if (data) {
				if (data.recurring != 0 && data.instances && data.instances.length) {
					/* #2791 */
					if (!instance_date) instance_date = getCurrentEventDateAttr(data.id,data.start_date);
					//if (instance_date) {
						for (var i=0, ii=data.instances.length; i<ii; i++) {
							var match = false;
							if (compareDateOnly) {
								var d1 = data.instances[i].start_date && data.instances[i].start_date.indexOf('T') != -1 ? data.instances[i].start_date.split('T')[0] : data.instances[i].start_date;
								var d2 = instance_date && instance_date.indexOf('T') != -1 ? instance_date.split('T')[0] : instance_date;
								match = d1 == d2;
							} else if (data.instances[i].start_date == instance_date) {
								match = true;
							}
							
							if (match) {
								return data.instances[i].status == 1;
							}
						}
						return false;
						/*
						} else {
							//Search for first not-deleted item and check if it is completed
							for (var i=0, ii=data.instances.length; i<ii; i++) {
								if (data.instances[i].status != 2) { // 2 == deleted
									return data.instances[0].status == 1; // 1 == completed
								}
							}
						}
					}*/
				}
				/* Bug #2040 - the item may not be completed, but if its parent is, it needs to be placed in the completed list*/
				/* Actually this is a hack if (data.parent && this.get(data.parent).completed) return true; */
				return data.completed;
			}
			
			return false;
		},
		
		/* 
		 * See getIsRemoved()
		 * This is a temporary modification that also checks completion status
		 */
		_getIsRemoved: function (id, instance_date) {
			// summary:
			//		For recurring items returns true if item is removed or false if it isn't
			// id: Number
			//		Task ID or task data
			
			var data = typeof id === "object" ? id : this.get(id);
			
			if (data && data.recurring != 0 && data.instances && data.instances.length) {
				if (instance_date) {
					for (var i=0, ii=data.instances.length; i<ii; i++) {
						if (data.instances[i].start_date == instance_date) {
							return data.instances[i].status == 2;// || data.instances[i].status == 1;
						}
					}
					return false;
				} else {
					return data.instances[0].status == 2;//||data.instances[0].status==1; // 2 == deleted
				}
			}
			
			return false;
		},
		
		/* Bug #1203 - may need to remove/not use */
		getIsRemoved: function (id, instance_date) {
			// summary:
			//		For recurring items returns true if item is removed or false if it isn't
			// id: Number
			//		Task ID or task data
			
			var data = typeof id === "object" ? id : this.get(id);
			
			if (data && data.recurring != 0 && data.instances && data.instances.length) {
				if (instance_date) {
					for (var i=0, ii=data.instances.length; i<ii; i++) {
						if (data.instances[i].start_date == instance_date) {
							return data.instances[i].status == 2;
						}
					}
					return false;
				} else {
					return data.instances[0].status == 2; // 2 == deleted
				}
			}
			
			return false;
		},
		
		/* ------------------------------- PRIVATE ------------------------------- */
		
		
		_handleRequestError: function (error, data) {
			// summary:
			//		Handle request error
			// error: Object
			//		Error object
			// data: Object
			//		Request data
			if (Project.isDevMode) {
				console.log('handle request error');
			}
			var responseObject = null;
			
			if (error && error.responseText) {
				//Get response as JSON object
				try {
					responseObject = JSON.parse(error.responseText);
				} catch (e) {}
				
				if (responseObject) {
					if (Tooltip.opened('message')) Tooltip.close('message', true);
					switch (responseObject.response_status) {
						case 20: // Limits
							if (responseObject.error_message === "Projects limit exceeded") {
								//Project limit exceeded
								Project.alertElement.showItem(Project.account_type == 'TEAM_BASIC' ? 18 : 2);
							} else if (responseObject.erorr_message === "Items limit exceeded") {
								//Items limit exceeded
								Project.alertElement.showItem(1);
								hi.items.manager.removeLastNegative();
							} else if (responseObject.error_message) {
								//Show server sent error message
								Project.alertElement.showItem(14, {task_alert: responseObject.error_message});
							}
							break;
						case 19:
							// Email must be confirmed.
							Project.alertElement.showItem(24);
							break;
						default: // Show error message
							if (responseObject.error_message) {
								Project.alertElement.showItem(14, {task_alert: responseObject.error_message});
							}
							break;
					}
				} 
			} else
			{
				if (Project.isDevMode) {
					console.log('error',[error,data]);
				}
				if ('doAdd' in data && data.doAdd) Project.alertElement.showItem(22);
			}
		},
		
		
		_setCompleted: function (data) {
			// summary:
			//		When setting "completed" property to true update all children also as completed
			//		Server-side automatically does this so no need for setSave
			// data: Object
			//		Changed properties
			
			var items = hi.store.ProjectCache.query({"parent": data.id}),
				ii = items.length,
				i = 0;
			
			for (; i<ii; i++) {
				if (!items[i].completed && items[i].category != hi.widget.TaskItem.CATEGORY_FILE) {
					this.set(items[i].id, {"completed": true}, true);
				}
			}
		},
		
		_setSaveCompleted: function (data) {
			// summary:
			//		When saving "completed" property update ancestors or children accordingly
			// data: Object
			//		Changed properties
			var item, parent, task, 
				subAffectParent = Hi_Preferences.get('sub_task_complete_affect_parent', false, 'bool');

			if (data.completed) {
				//Set all children as completed
				var items = hi.store.ProjectCache.query({"parent": data.id}),
					ii = items.length,
					i = 0;
				
				for (i; i<ii; i++) {
					if (!items[i].completed && items[i].category != hi.widget.TaskItem.CATEGORY_FILE) {
						this.set(items[i].id, {"completed": true});
					}
				}

				//If child item set parent as completed if all children are completed.
				item = hi.store.ProjectCache.get(data.id);
				var children = hi.store.ProjectCache.query({"parent": item.parent});
				
				var allChildrenCompleted = true;
				
				array.forEach(children, function(child) {
					if (item.id != child.id && // Dont check the current item;
						!child.completed && 
						child.category != hi.widget.TaskItem.CATEGORY_FILE) {
						allChildrenCompleted = false;
					}
				}, this);
				
				if (allChildrenCompleted && subAffectParent === true) {
					parent = hi.store.ProjectCache.get(item.parent);
					if (parent) {
						task = this.item(parent.id);
						if (task && !item_DOM.isChild(task.taskItem)) {
							item_DOM.animate(task,'fadedown');
						}
						hi.items.manager.setSave(parent.id, {"completed": true});
					}
				}
			} else {
				//Set ancestors as not completed
				item = hi.store.ProjectCache.get(data.id);
				if (item && item.parent) {
					parent = hi.store.ProjectCache.get(item.parent);
					while (parent && parent.completed && parent.category != hi.widget.TaskItem.CATEGORY_PROJECT) { 
						hi.items.manager.setSave(parent.id, {"completed": false});
						
						//Animate because child made this item uncompleted
						//and this that case item was not clicked and was not animated 
						task = this.item(parent.id);
						if (task && /* #3166 !task.isChild()*/ !item_DOM.isChild(task.taskItem)) {
							/* #3166 task.animate("fadeup");*/
							item_DOM.animate(task,'fadeup');
							parent = null;
						} else {
							parent = hi.store.ProjectCache.get(parent.parent);
						}
					}
				}
			}
			
			//Cascade
			data.cascade = 1;
		},
		
		_setSaveRecurring: function (id, changed) {
			// summary:
			//		If recurring event start_date, end_date or recurring changed then update tasks
			//		instances object
			// id: Number
			//		Task ID
			// changed: Object
			//		Changed data
			
			var data = this.get(id);
			var recurring = "recurring" in changed ? changed.recurring : data.recurring;
			
			if (recurring) {
				/* #2791 click complete provides this information, and it will be in changed object */
				
				/* #1203 - Passing in incorrect parameters to getNextEventDate
				var instance_date = hi.items.date.getNextEventDate(start_date, recurring);
				instance_date = (instance_date && instance_date[1] ? time2str(instance_date[1], 'y-m-d') : null);
				*/
				var instance_date,
					start_date,
					status;
				/* #2803 */
				if (!changed.instance_date_time)
				{
					instance_date = hi.items.date._getEventInstanceDate(id, start_date);
					start_date = time2str(instance_date,'y-m-d');
					instance_date = time2strAPI(instance_date);
				} else
				{
					/* #2910, #2791 instance_date = hi.items.date.getNextEventDate(id,start_date);*/
					instance_date = changed.instance_date_time;
					start_date = changed.start_date || time2str(new Date(instance_date),'y-m-d');
				}
				
				/* #2776 */
				if ('completed' in changed) status = changed.completed;
				else status = hi.items.manager.getIsCompleted(data.id,instance_date,true)?1:0;
				
				/* #2791 this obliterates the instances array and we lose the state of the recurring item until page reload 
				 * So if I create a daily task that starts on June 1, then on June 23 complete the June 1 instance, then go to 
				 * June 7 and complete that instance, I lose the information that the June 1 instance was completed				

				/* #2791 this obliterates the instances array and we lose the state of the recurring item until page reload 
				 * So if I create a daily task that starts on June 1, then on June 23 complete the June 1 instance, then go to 
				 * June 7 and complete that instance, I lose the information that the June 1 instance was completed				

				this.set({
					"id": data.id,
					"instances": [{
						"start_date": start_date,
						'start_date_time':instance_date,
						"status": status,
						"task_id": data.id
					}]
				});
				*/
				
				var instances = hi.items.manager.get(id).instances||[];
				var updateInstance = hi.items.date.getInstance(id,start_date);
				if (updateInstance)
				{
					updateInstance.status = status;
				} else
				{
					instances.push({
						start_date:start_date,
						start_date_time:instance_date,
						status:status,
						task_id:data.id
					});
				}
				
				this.set({id:data.id,instances:instances});
			}
		},
		
		
		/* ------------------------------- ACTIVE ITEM API ------------------------------- */
		
		
		opened: function (id, state) {
			// summary:
			//		If id is not specified then returns opened hi.widget.TaskItem instance or null if there isn't one
			//		If id is specified checks if item is opened
			//		If id and state is specified then sets that item as opened if state is true or unsets if state is false
			
			return this._listSetGet(this._opened, id, state, "opened");
		},
		
		openedId: function (id) {
			// summary:
			//		Returns item ID which is opened or null if there isn't one if id attribute is not specified
			//		If id is specified checks if item is opened
			
			var item = this.opened(id);
			return item ? item.taskId : null;
		},
		
		editing: function (id, state) {
			// summary:
			//		If id is not specified then returns item which is edited or null if there isn't one
			//		If id is specified checks if item is being edited
			//		If id and state is specified then sets that item as edited if state is true or unsets if state is false
			
			return this._listSetGet(this._editing, id, state, "editing");
		},
		
		editingId: function (id) {
			// summary:
			//		Returns item ID which is edited or null if there isn't one if id attribute is not specified
			//		If id is specified checks if item is being edited
			
			var item = this.editing(id);
			return item ? item.taskId : null;
		},
		
		_listSetGet: function (list, id, state, prop) {
			// summary:
			//		If id is not specified then returns item from the list or null if there isn't one
			//		If id is specified checks if item is in the list
			//		If id and state is specified then adds that item to the list if state is true or unsets if state is false
			
			var items = null,
				key = null,
				i = 0;
			
			if (id) {
				if (state === true || state === false) {
					i = dojo.indexOf(list, id);
					if (state === true) {
						if (i === -1) list.push(id);
						items = this._items[id];
					} else {
						if (i !== -1) list.splice(i, 1);
					}
				} else {
					i = dojo.indexOf(list, id);
					if (i === -1) return null;
					items = this._items[id];
				}
			} else {
				if (list.length) {
					items = this._items[list[0]];
				}
			}
			
			for (key in items) {
				if (items[key][prop]) return items[key];
			}
			
			return null;
		},
		
		collapseOpenedItems: function (force, save) {
			// summary:
			//		Collapse all expanded tasks
			//		Tasks which are being edited will not be collapsed
			// force:
			//		Collapse even if editing
			// save:
			//		Save if editing
			
			var opened = this._opened,
				i = opened.length - 1,
				item = null;
			
			for (; i >= 0; i--) {
				if (opened[i] < 0) continue;
				item = this.opened(opened[i]);

				if(item && item.opened){
					if(item.editing){
						if(force){
							if(save){
								item.editingView.save();
							}else{
								item_DOM.collapse(item);
							}
						}
					}else{
						item_DOM.collapse(item);
						//  #2965
						//  * Item may not be found if it was expanded and then a tab switch occurred;
						//  * it nevertheless needs to be marked as 'not opened'
					}
				}else{

				}
			}
		},
		
		hasEditingItemCurrently: function() {
			var editing = this._editing,
				i = editing.length - 1,
				item = null;
			
			for (; i>=0; i--) {
				item = this.editing(editing[i]);
				if (item && item.editing) {
					return true;
				}
			}
			
			return false;
		},
		
		openItem: function (id) {
			// summary:
			//		Expand task
			// id: Number
			//		Task ID
			
			var item = this.item(id);
			if (item) {
				item_DOM.expand(item);
			}
		},
		
		
		/* ------------------------------- GROUPING API ------------------------------- */
		
		
		redraw: function (argument) {
			// summary:
			//		Handle redraw call
			//		This function should be used only if absolutely neccessary to avoid
			//		performance hit
			
			//NOTE: this function doesn't do anything here, but instead is attached from
			//view-*.js by dojo.connect and it is handled there
		},
		
		groupingChange: function () {
			// summary:
			//		Handle grouping change
			
			//NOTE: this function doesn't do anything here, but instead is attached from
			//view-*.js by dojo.connect and it is handled there
		},
		
		tagChange: function () {
			// summary:
			//		Handle tag filter change
			
			this.redraw();
		},
		
		filterChange: function () {
			// summary:
			//		Handle filter change
			
			//Display Loading in the center column like when page is first loaded
			var container = dojo.byId(Project.getContainer('tasksList'));
			dojo.addClass(container, "loading");
			
			//Reload list
			hi.items.manager.refresh(true).then(function () {
				dojo.removeClass(container, "loading");
				try {hi.items.manager.redraw(true);} catch(e) {console.log('e=',e);}
			});
		},
		
		searchChange: function () {
			// summary:
			//		Handle search change
			
			this.redraw();
		},
		
		getTaskLists: function () {
			// summary:
			//		Returns task lists for current group
			
			var group = Hi_Preferences.get('currentTab', 'my', 'string');
			if (this.getTaskLists[group]) {
				return this.getTaskLists[group]();
			} else {
				return [];
			}
		},

		isTask: function(item) {
			item = typeof item === 'object'? item : hi.items.manager.get(item);

			if (!item) return false;

			return item.category === hi.widget.TaskItem.CATEGORY_TASK;
		},

		isProject: function(item) {
			item = typeof item === 'object'? item : hi.items.manager.get(item);

			if (!item) return false;

			return item.category === hi.widget.TaskItem.CATEGORY_PROJECT;
		},

		isFile: function(item) {
			item = typeof item === 'object'? item : hi.items.manager.get(item);

			if (!item) return false;

			return item.category === hi.widget.TaskItem.CATEGORY_FILE
		},

		isEvent: function(item) {
			item = typeof item === 'object'? item : hi.items.manager.get(item);

			if (!item) return false;

			return item.category === hi.widget.TaskItem.CATEGORY_EVENT;
		},

		isNote: function(item) {
			item = typeof item === 'object'? item : hi.items.manager.get(item);

			if (!item) return false;

			return item.category === hi.widget.TaskItem.CATEGORY_Note;
		}
	};
	
	// For compatibility with Project.setContainer, but we wont be doing anything here
	dojo.global.tasksList = function () {};
	dojo.global.him = hi_items.manager;
	
	return hi_items;
	
});
