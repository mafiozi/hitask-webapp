package beans;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.joda.time.Days;
import org.joda.time.LocalDate;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import utils.Constants;

/**
 * Dummy java bean. Update and replace it as soon as Login and User Session start working.
 * @author Vadim Manikhin
 */
public class UserSession extends BaseBean {
	public enum AccountType {
		TEAM_BASIC, TEAM_BUSINESS, PERSONAL_FREE, PERSONAL_PREMIUM
	}
	
	/**
	 * Time when expire warning starts to be shown.
	 */
	private static final int EXPIRE_WARNING_INTERVAL_DAYS = 7;
	
	public static final int ACCOUNT_LEVEL_FREE = 0;
	public static final int ACCOUNT_LEVEL_PREMIUM_EXPIRED = 50;
	public static final int ACCOUNT_LEVEL_PREMIUM = 100;
	public static final int ACCOUNT_LEVEL_BUSINESS_EXPIRED = 150;
	public static final int ACCOUNT_LEVEL_BUSINESS = 200;
	
	public static final int BUSINESS_LEVEL_OWNER = 100;
	public static final int BUSINESS_LEVEL_ADMIN = 10;
	public static final int BUSINESS_LEVEL_MANAGER = 2;
	public static final int BUSINESS_LEVEL_USER = 1;
	
	// Following properties are going from GET /user:
	private String email;
	private String emailConfirmed;
	private Integer emailErrorCode;
	private String emailErrorText;


	private String firstName;
	private String lastName;
	private Long id;
	private boolean isOnline;
	private boolean isPremium;
	private Integer level = ACCOUNT_LEVEL_FREE;
	private String login;
	private String accountType;
	private String apiKey;
	private UserApiKey apiKeyObj;
	
	// Post init following:
	private int expireIn = 0;
	private boolean expireWarning = false;
	private String api2SessionId = null;
	private UserBusiness business;
	
	// Some other properties that can be updated later if required:
	private boolean isIeUser = false;
	private int businessLevel = 0;
	
	@SerializedName("expirationDate")
	private String levelExpireDate = null;
	private String pictureHash = "";
	private int sessionId = 1;
	private String sessionName = "dummy";
	@SerializedName("time_zone")
	private String timezone = "Europe/London";
	private boolean attachmentsEnabled = true;
	private boolean deleted = false;
	private boolean noSubscribe = false;
	private boolean expired = false;
	private String accountCreateTime;
	private Date accountCreateTimeDate;
	private long numberOfContacts = 0;
	
	private long numberOfProjects = 0;
	private long numberOfItems = 0;
	private long numberOfArchived = 0;
	
	private boolean hasGoogleAccount = false;
	private boolean hasGoogleSync = false;
	private boolean hasPassword = false;
	private boolean googleSyncDisabled = false;

	private Transaction lastTransaction;
	
	private UserAuthSession userSession;
	
	private Set<UserExternalAccount> externalAccounts = new HashSet<UserExternalAccount>();
	private Set<UserExternalSync> externalSyncs = new HashSet<UserExternalSync>();
	
	private boolean hasCustomer = false;
	private String customerError = null;
	private String customerDescription = null;
	private Integer paymentFailsCount = null;
	private ProductPlan productPlan;
	private String emailInbox;
	
	private Double monthlyRate = 0.0;
	
	public void postInit(String api2SessionId, UserBusiness business) {
		isPremium = level == ACCOUNT_LEVEL_PREMIUM;
		this.api2SessionId = api2SessionId;
		Date levelExpireDate = parseApiDate(this.levelExpireDate);
		
		if (business != null && business.getLevel() != null) {
			this.business = business;
			this.business.postInit();
			businessLevel = this.business.getLevel();
			levelExpireDate = parseApiDate(business.getExpirationDate());
		}
		
		LocalDate now = new LocalDate();
		LocalDate expireDay = levelExpireDate != null ? new LocalDate(levelExpireDate) : null;
		expireIn = expireDay != null ? Days.daysBetween(now, expireDay).getDays() : 0;
		if (expireIn < 0) {
			expireIn = 0;
		}
		
		if (expireDay != null && now.isBefore(expireDay) && expireIn <= EXPIRE_WARNING_INTERVAL_DAYS) {
			// #2212: Only business owners and premium users should see expiration warning.
			expireWarning = isBusinessOwner() || isPremium ? true : false;
			
			// Do not display warning for users with valid customer details.
			if (hasCustomer && customerError == null) {
				expireWarning = false;
			}
		}
		
		if (expireDay != null && (now.isEqual(expireDay) || now.isAfter(expireDay))) {
			expired = true;
		}
		
		accountCreateTimeDate = parseApiDate(accountCreateTime);
	}
	
	public boolean isShowAd() {
		return Arrays.asList(new Integer[] {ACCOUNT_LEVEL_FREE, ACCOUNT_LEVEL_BUSINESS_EXPIRED, ACCOUNT_LEVEL_PREMIUM_EXPIRED}).contains(level);
	}

	public String getLevelExpireDate() {
		return levelExpireDate;
	}

	public void setLevelExpireDate(String levelExpireDate) {
		this.levelExpireDate = levelExpireDate;
	}

	public boolean isExpireWarning() {
		return expireWarning;
	}

	public void setExpireWarning(boolean expireWarning) {
		this.expireWarning = expireWarning;
	}

	public int getExpireIn() {
		return expireIn;
	}

	public void setExpireIn(int expireIn) {
		this.expireIn = expireIn;
	}

	public int getAccountLevel() {
		return expired ? ACCOUNT_LEVEL_FREE : level;
	}

	public int getBusinessLevel() {
		return businessLevel;
	}

	public void setBusinessLevel(int businessLevel) {
		this.businessLevel = businessLevel;
	}

	public String getPictureHash() {
		return pictureHash;
	}

	public void setPictureHash(String pictureHash) {
		this.pictureHash = pictureHash;
	}
	
	public String getAvatar() {
		String avatar = null;
		if (StringUtils.isNotBlank(pictureHash)) {
			avatar = new StringBuilder().append("https://").append(Constants.DEFAULT_DOMAIN).append("/avatar/").append(pictureHash).append(".64.gif").toString();
		}
		return avatar;
	}

	public String getFullName() {
		if (StringUtils.isNotBlank(firstName) || StringUtils.isNotBlank(lastName)) {
            return new StringBuilder().append(firstName != null ? firstName : "").append(" ").append(lastName != null ? lastName : "").toString().trim();
        }
        if (StringUtils.isNotBlank(emailConfirmed)) {
            return emailConfirmed;
        }
        if (StringUtils.isNotBlank(email)) {
            return email;
        }
        return login;
	}

	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	public String getSessionName() {
		return sessionName;
	}

	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}

	public Long getUserId() {
		return id;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String isEmailConfirmed() {
		return emailConfirmed;
	}

	public void setEmailConfirmed(String emailConfirmed) {
		this.emailConfirmed = emailConfirmed;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailConfirmed() {
		return emailConfirmed;
	}
	
	public String getAnyEmail() {
		if (emailConfirmed != null) {
			return emailConfirmed;
		}
		return email;
	}
	
	public String getApi2SessionId() {
		return api2SessionId;
	}

	public void setApi2SessionId(String api2SessionId) {
		this.api2SessionId = api2SessionId;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public boolean isAttachmentsEnabled() {
		return attachmentsEnabled;
	}

	public void setAttachmentsEnabled(boolean attachmentsEnabled) {
		this.attachmentsEnabled = attachmentsEnabled;
	}

	public UserBusiness getBusiness() {
		return business;
	}

	public void setBusiness(UserBusiness business) {
		this.business = business;
	}
	
	public String getBusinessAsJson() {
		return new Gson().toJson(business);
	}

	public boolean isIeUser() {
		return isIeUser;
	}

	public void setIeUser(boolean isIeUser) {
		this.isIeUser = isIeUser;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isNoSubscribe() {
		return noSubscribe;
	}

	public void setNoSubscribe(boolean noSubscribe) {
		this.noSubscribe = noSubscribe;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isOnline() {
		return isOnline;
	}

	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}

	public boolean isPremium() {
		return isPremium && getBusiness() == null;
	}

	public void setPremium(boolean isPremium) {
		this.isPremium = isPremium;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	public boolean isExpired() {
		return expired;
	}
	
	public boolean isBusinessOwner() {
		return business != null && businessLevel == BUSINESS_LEVEL_OWNER;
	}

	public String getAccountCreateTime() {
		return accountCreateTime;
	}

	public void setAccountCreateTime(String accountCreateTime) {
		this.accountCreateTime = accountCreateTime;
	}

	public Date getAccountCreateTimeDate() {
		return accountCreateTimeDate;
	}

	public long getNumberOfContacts() {
		return numberOfContacts;
	}

	public void setNumberOfContacts(long numberOfContacts) {
		this.numberOfContacts = numberOfContacts;
	}

	public long getNumberOfProjects() {
		return numberOfProjects;
	}

	public void setNumberOfProjects(long numberOfProjects) {
		this.numberOfProjects = numberOfProjects;
	}

	public long getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(long numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public long getNumberOfArchived() {
		return numberOfArchived;
	}

	public void setNumberOfArchived(long numberOfArchived) {
		this.numberOfArchived = numberOfArchived;
	}

	public Transaction getLastTransaction() {
		return lastTransaction;
	}

	public void setLastTransaction(Transaction lastTransaction) {
		this.lastTransaction = lastTransaction;
	}
	
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	private AccountType getAccountTypeValue() {
    	try {
    		return AccountType.valueOf(accountType);
    	} catch (NullPointerException e) {
        	return null;
        } catch (IllegalArgumentException e) {
        	return null;
        }
    }
	
	public boolean isFree() {
		return getAccountTypeValue() != null && getAccountTypeValue().equals(AccountType.PERSONAL_FREE);
	}
	
	public boolean isTeamBusiness() {
		return getAccountTypeValue() != null && getAccountTypeValue().equals(AccountType.TEAM_BUSINESS);
	}
	
	public boolean isTeamBasic() {
		return getAccountTypeValue() != null && getAccountTypeValue().equals(AccountType.TEAM_BASIC);
	}

	public Integer getEmailErrorCode() {
		return emailErrorCode;
	}

	public void setEmailErrorCode(Integer emailErrorCode) {
		this.emailErrorCode = emailErrorCode;
	}

	public String getEmailErrorText() {
		return emailErrorText;
	}

	public void setEmailErrorText(String emailErrorText) {
		this.emailErrorText = emailErrorText;
	}

	public UserAuthSession getUserSession() {
		return userSession;
	}

	public void setUserSession(UserAuthSession userSession) {
		this.userSession = userSession;
	}
	
	public boolean isHasGoogleAccount() {
		return hasGoogleAccount;
	}

	public void setHasGoogleAccount(boolean hasGoogleAccount) {
		this.hasGoogleAccount = hasGoogleAccount;
	}

	public boolean isHasGoogleSync() {
		return hasGoogleSync;
	}

	public void setHasGoogleSync(boolean hasGoogleSync) {
		this.hasGoogleSync = hasGoogleSync;
	}
	
	public UserExternalSync getGoogleSync() {
		if (hasGoogleAccount && hasGoogleSync && !googleSyncDisabled && externalSyncs != null && !externalSyncs.isEmpty()) {
			for (UserExternalSync sync : externalSyncs) {
				if (sync.isGoogle()) {
					return sync;
				}
			}
		}
		
		return null;
	}
	
	public boolean isGoogleSyncForbiddenError() {
		if (hasGoogleAccount && hasGoogleSync && externalSyncs != null && !externalSyncs.isEmpty()) {
			for (UserExternalSync sync : externalSyncs) {
				if (sync.isGoogle() && sync.isForbiddenError()) {
					return true;
				}
			}
		}
		
		return false;
	}

	public boolean isHasPassword() {
		return hasPassword;
	}

	public void setHasPassword(boolean hasPassword) {
		this.hasPassword = hasPassword;
	}

	public Set<UserExternalAccount> getExternalAccounts() {
		return externalAccounts;
	}
	
	public String getExternalGoogleEmail() {
		if (externalAccounts != null) {
			for (UserExternalAccount x : externalAccounts) {
				if (x.getType() != null && x.getType().equalsIgnoreCase("google")) {
					return x.getExternalUserHumanReadableId();
				}
			}
		}
		
		return null;
	}

	public void setExternalAccounts(Set<UserExternalAccount> externalAccounts) {
		this.externalAccounts = externalAccounts;
	}
	
	public Set<UserExternalSync> getExternalSyncs() {
		return externalSyncs;
	}

	public void setExternalSyncs(Set<UserExternalSync> externalSyncs) {
		this.externalSyncs = externalSyncs;
	}

	public boolean isGoogleSyncDisabled() {
		return googleSyncDisabled;
	}

	public void setGoogleSyncDisabled(boolean googleSyncDisabled) {
		this.googleSyncDisabled = googleSyncDisabled;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public boolean hasApiKey() {
		return StringUtils.isNotBlank(getApiKey()) && getApiKeyObj() != null;
	}

	public UserApiKey getApiKeyObj() {
		return apiKeyObj;
	}

	public void setApiKeyObj(UserApiKey apiKeyObj) {
		this.apiKeyObj = apiKeyObj;
	}

	public boolean isHasCustomer() {
		return hasCustomer;
	}

	public void setHasCustomer(boolean hasCustomer) {
		this.hasCustomer = hasCustomer;
	}

	public ProductPlan getProductPlan() {
		return productPlan;
	}

	public void setProductPlan(ProductPlan productPlan) {
		this.productPlan = productPlan;
	}

	public String getCustomerError() {
		return customerError;
	}

	public void setCustomerError(String customerError) {
		this.customerError = customerError;
	}

	public Double getMonthlyRate() {
		return monthlyRate;
	}

	public void setMonthlyRate(Double monthlyRate) {
		this.monthlyRate = monthlyRate;
	}

	public String getCustomerDescription() {
		return customerDescription;
	}

	public void setCustomerDescription(String customerDescription) {
		this.customerDescription = customerDescription;
	}

	public Integer getPaymentFailsCount() {
		return paymentFailsCount;
	}

	public void setPaymentFailsCount(Integer paymentFailsCount) {
		this.paymentFailsCount = paymentFailsCount;
	}

	public String getEmailInbox() {
		return emailInbox;
	}

	public void setEmailInbox(String emailInbox) {
		this.emailInbox = emailInbox;
	}
}
