//dojo.provide("hi.upload.transport.html5");

/*
 * If file drag & drop is supported
 */
define(function () {
	
	if (typeof(FileReader) != 'undefined') {
	
		dojo.global.FileUploadTaskHTML5  = function(parent, guid, fileName, fileSize) {
			if (!parent) return;
			
			this.id = guid;
			this.parent = parent;
			
			if (this.parent.taskId <= 0) {
				this.parentId = hi.items.manager.get(this.parent.taskId, "parent") || 0;
			} else {
				this.parentId = this.parent.taskId;
			}
			
			this._create(fileName, fileSize);
		};

		dojo.global.FileUploadTaskHTML5.prototype = dojo.mixin(new FileUploadFile(), {
			/**
			 * Don't retry uploading
			 */
			retries: 0,
			
			_create: function (fileName, fileSize, restore) {
				var label_failed = hi.i18n.getLocalization('properties.file_upload_failed'),
					label_cancel = hi.i18n.getLocalization('properties.cancel_upload');
				
				this.filename = fileName || '';
				this.filesize = fileSize || '';
				fileSize = FileList.formatFileSize(fileSize);
				
				var nodeContainer = this.nodeContainer = document.createElement('LI');
				var html = '<div class="txt-failure txt">' + label_failed + ' <span fill="html: filename">' + this.filename + '</span></div>' + 
							'<div class="txt-success txt"><span class="uploaded_filename" fill="html: filename">' + this.filename + '</span> <b></b></div>' +
							'<div class="txt-uploading txt-uploading-html5 txt"><a class="cancel">' + label_cancel + '</a><span class="progress-bar"><em></em><span>0%</span></span><span class="uploaded_filename" fill="html: filename">' + this.filename + '</span></div>' +
							'<div class="txt-ready txt"><a class="cancel">' + label_cancel + '</a><span class="uploaded_filename" fill="html: filename">' + this.filename + '</span></div>';
				
				nodeContainer.innerHTML = html;
				
				var targetNode = dojo.query('li', this.parent.nodeList);
				targetNode = targetNode.length ? targetNode[targetNode.length - 1] : null; 
				
				if (targetNode) {
					targetNode.parentNode.insertBefore(nodeContainer, targetNode);
				} else {
					return;
				}
				
				this.nodeProgress = dojo.query('span.progress-bar span', nodeContainer)[0];
				this.nodeProgressLine = previousElement(this.nodeProgress);
				
				dojo.forEach(dojo.query('a.cancel', nodeContainer), dojo.hitch(this, function (node) {
					dojo.connect(node, 'click', this, 'cancel');
				}));
				
				//Files attached to task should start uploading automatically
				/*
				if (this.parent.taskId && this.parent.taskId != -1) {
					dojo.addClass(this.nodeContainer, 'file-uploading');
				} else {
					dojo.addClass(this.nodeContainer, 'file-ready');
				}
				*/
				
				dojo.addClass(this.nodeContainer, 'file-ready');
			},
			
			/**
			 * Move node to tooltip
			 */
			moveToTooltip: function (parentNode) {
				parentNode.appendChild(this.nodeContainer);
			},
			
			/**
			 * Destroy file instance
			 */
			destroy: function () {
				if (!this.uploading) {
					this.cancel();
				}
				
				delete(this.nodeProgress);
				delete(this.nodeProgressLine);

				removeNode(this.nodeContainer);
				delete(this.nodeContainer);
				
				this.destroyed = true;
			},
			
			/**
			 * Cancel upload
			 */
			cancel: function () {
				if (this.canceled) return;
				this.canceled = true;
				
				if (this.uploading) {
					FileUploadHTML5FileList.cancel(this.id);
					
					if (this.key) {
						FileUpload.notifyCancel(this.key);
					}
					
					this.uploading = false;
					this.progress(0);
					if (this.parent) this.parent.uploadNext();
				}
				
				FileUploadContainer.destroy(this.id);
				if (this.parent) {
					this.parent.removeFile(this.id);
				}
			},
			
			/**
			 * Start file upload
			 */
			upload: function () {
				/* 
				 * Only files which are 'ready' can be uploaded, if file isn't ready, then it is already
				 * uploading or uploaded
				 */
				if (!this.isready || this.completed || this.uploading) return;
				this.isready = false;
				this.uploading = true;
				
				dojo.removeClass(this.nodeContainer, 'file-ready');
				dojo.addClass(this.nodeContainer, 'file-uploading');
				
				FileUploadHTML5FileList.upload(this.id);
				
				// show tooltip only for uploading to Project
				/*
				 * #3323
				 * taskId might be less than -1, e.g., when uploading file as item in project
				 */
				if (!this.parent.taskId || this.parent.taskId <= -1) {
					FileUploadTooltip.show();
				}
				
				//Hide "Start upload" buttons
				if (this.parent) {
					this.parent.updateReadyView();
				}
			},
			
			/**
			 * Upload success handler
			 * 
			 * @param {Object} params Params has following structure {'bucket': '...', 'key': 'files/file_i_uploaded.jpg', 'etag': '...'}
			 */
			_uploadSuccess: function (params) {
				if (this.canceled) return;
				
				dojo.removeClass(this.nodeContainer, 'file-uploading');
				dojo.addClass(this.nodeContainer, 'file-success');
				
				//Update parent task
				var taskId = this.parent ? this.parent.taskId : 0;
				var self = this;
				if (this.parent.taskId<=0 && hi.items.manager.editing() && hi.items.manager.editing().editingView)
				{
					var properties = hi.items.manager.editing().editingView.getFormValues();
					properties = dojo.mixin(properties,{id:params,title:this.filename});
					hi.items.manager.refresh().then(function() {
						hi.items.manager.setSave(properties).then(function() {self.onUploadSuccess();});
						
					});
				} else {
					/*
					this.uploading = false;
					this.completed = true;
					
					this.progress(100);
					
					FileUploadContainer.destroy(this.id);
					
					//Update
					if (this.parent && !this.destroyed) {
						this.parent.update();
						this.parent.uploadNext();
					}	
					*/
					self.onUploadSuccess();
				}

			},
			
			onUploadSuccess: function() {
				this.uploading = false;
				this.completed = true;
				
				this.progress(100);
				
				FileUploadContainer.destroy(this.id);
				
				//Update
				if (this.parent && !this.destroyed) {
					this.parent.update();
					this.parent.uploadNext();
				}
			},
			
			error: function (response) {
				if (!this.destroyed && !this.canceled) {
					this._uploadFailure(response);
				}
			},
			
			complete: function (response) {
				if (!this.destroyed && !this.canceled) {
					this._uploadSuccess(response);
				}
			},
			
			progress: function (progress) {
				if (!this.destroyed) {
					progress = Math.min(100, Math.max(0, parseFloat(progress)));
					
					this.uploadProgress = progress;
					this.nodeProgress.innerHTML = ~~progress + '%';
					this.nodeProgressLine.style.width = progress + '%';
				}
			}
		});
		
		return dojo.global.FileUploadTaskHTML5;
	}
	
	return {};
});