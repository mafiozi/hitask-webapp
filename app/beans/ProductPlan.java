package beans;

import java.io.Serializable;

public class ProductPlan implements Serializable {
	private Double price;
	private Long period;
	private Long licenses;
	private Double save = 0.0;
	private Long id;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Long getPeriod() {
		return period;
	}

	public void setPeriod(Long period) {
		this.period = period;
	}

	public Long getLicenses() {
		return licenses;
	}

	public void setLicenses(Long licenses) {
		this.licenses = licenses;
	}

	public Double getSave() {
		return save;
	}

	public void setSave(Double save) {
		this.save = save;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
