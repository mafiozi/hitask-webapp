//dojo.provide("hi.form");
define(function () {
	
	dojo.form = {};
	dojo.form.formFilter = function (node) {
		var type = (node.type || "").toLowerCase();
		return !node.disabled && node.name && !in_array(type, ["file", "submit", "image", "reset", "button"]);
	};
	dojo.form.getValue = function (elm) {
		elm = dojo.byId(elm);
		if ((!elm) || (!elm.tagName)) {
			throw("Attempted to get value from a non-input element.");
		}
		var value = false;
		var type = (elm.type || "").toLowerCase();
		if (type == "select-multiple") {
			for (var j = 0; j < elm.options.length; j++) {
				if (elm.options[j].selected) {
					value = elm.options[j].value;
				}
			}
		} else {
			if (in_array(type, ["radio", "checkbox"])) {
				if (elm.checked) {
					value = elm.value;
				}
			} else {
				value = elm.value;
				var widget = dijit.getEnclosingWidget(elm);
				if(widget && widget instanceof dijit.form._FormWidget){ value = widget.get('value'); }
			}
		}
		value = (typeof value === 'string' && value.trim)? value.trim(): value; 
		return value;
	};
	dojo.form.getFormValues = function (formNode, formFilter) {
		formNode = dojo.byId(formNode);
		if ((!formNode) || (!formNode.tagName) || !(formNode.tagName.toLowerCase() == "form")) {
			throw("Attempted to get form values from a non-form element.");
		}
		if (!formFilter) {
			formFilter = dojo.form.formFilter;
		}
		var values = {};
		var formNodeElements = dojo.query('input,textarea,select', formNode);
		for (var i = 0; i < formNodeElements.length; i++) {
			var elm = formNodeElements[i];
			if (!elm || elm.tagName.toLowerCase() == "fieldset" || !formFilter(elm)) {
				continue;
			}
			
			var name = elm.name;
			
			if (elm.tagName.toLowerCase() == 'input' && elm.getAttribute('type').toLowerCase() == 'radio') {
				if (elm.checked) {
					values[name] = dojo.form.getValue(elm);
				}
			} else {
				values[name] = dojo.form.getValue(elm);
			}
		}
		var inputs = formNode.getElementsByTagName("input");
		for (var i = 0; i < inputs.length; i++) {
			var elm = inputs[i];
			if(elm.type.toLowerCase() == "image" && elm.form == formNode && formFilter(elm)) {
				values[name] = elm.value;
				values[name + ".x"] = 0;
				values[name + ".y"] = 0;
			}
		}
		return values;
	};
	dojo.form.encodeData = function(data) {
		var s = [];
		for (i in data) {
			s.push(encodeURIComponent(i) + '=' + encodeURIComponent(data[i]));
		}
		return s.join('&');
	};
	dojo.form.encodeForm = function (formNode, formFilter) {
		var values = dojo.form.getFormValues(formNode, formFilter);
		return dojo.form.encodeData(values);
	};
	dojo.form.disable = function (formNode) {
		formNode = dojo.byId(formNode);
		if ((!formNode) || (!formNode.tagName) || (!formNode.tagName.toLowerCase() == "form")) {
			throw("Attempted to get form values from a non-form element.");
		}
		var formNodeElements = dojo.query('input,textarea,select', formNode);
	    for (i = 0; i < formNodeElements.length; i++) {
			dojo.form.element.disable(formNodeElements[i]);
		}
	    return;
	};
	dojo.form.enable = function (formNode) {
		formNode = dojo.byId(formNode);
		if ((!formNode) || (!formNode.tagName) || (!formNode.tagName.toLowerCase() == "form")) {
			throw("Attempted to get form values from a non-form element.");
		}
		var formNodeElements = dojo.query('input,textarea,select', formNode);
	    for (i = 0; i < formNodeElements.length; i++) {
			dojo.form.element.enable(formNodeElements[i]);
		}
	    return;
	};
	dojo.form.element = function() {};
	dojo.form.element.disable = function(element) {
		element = dojo.byId(element);
	    element.blur();
	    element.disabled = true;
	    return;
	};
	dojo.form.element.enable = function(element) {
		element = dojo.byId(element);
	    element.disabled = false;
	    return;
	};
	
	return dojo.form;

});