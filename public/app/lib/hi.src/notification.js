//dojo.provide("hi.notification");
define(function () {
	
	return dojo.global.Hi_Notification = {
		default_time: 1,
		timer: null,
		animation: null,
		messageQueue: [],
		notificationMessage: {
			'itemsave': hi.i18n.getLocalization('notification.task_modified'), 
			'itemnew': hi.i18n.getLocalization('notification.task_created'),
			'tmpsave': hi.i18n.getLocalization('notification.task_created'),
			'itemassign': hi.i18n.getLocalization('notification.task_assigned'),
			'itemdelete' : hi.i18n.getLocalization('notification.task_deleted'),
			'itemcomplete' : hi.i18n.getLocalization('notification.task_completed'),
			'tuncomplete' : hi.i18n.getLocalization('notification.task_uncompleted'),
			'fradd' : hi.i18n.getLocalization('notification.team_member_invited'),
			'fradd_business' : hi.i18n.getLocalization('notification.team_member_invited'),
			'frinvite' : hi.i18n.getLocalization('notification.team_member_invited'),
			'frinvite_business' : hi.i18n.getLocalization('notification.team_member_invited'),
			'frdelete' : hi.i18n.getLocalization('notification.team_member_deleted'),
			'frdelete_business' : hi.i18n.getLocalization('notification.team_member_business_deleted'),
			'projectnew' : hi.i18n.getLocalization('notification.project_was_created'),
			'ptitle' : hi.i18n.getLocalization('notification.project_was_updated'),
			'ptdelete' : hi.i18n.getLocalization('notification.project_was_deleted'),
			'pdelete' : hi.i18n.getLocalization('notification.project_was_deleted'),
			'itemsave_starred': hi.i18n.getLocalization('notification.task_modified_starred'),
			'itemsave_start_date': hi.i18n.getLocalization('notification.task_modified_date'),
			'itemsave_end_date': hi.i18n.getLocalization('notification.task_modified_date'),
			'itemsave_color': hi.i18n.getLocalization('notification.task_modified_color'),
			'itemsave_project': hi.i18n.getLocalization('notification.task_modified_project'),
			'task_archive': hi.i18n.getLocalization('notification.task_archive'),
			'task_restore': hi.i18n.getLocalization('notification.task_restore'),
			'task_copy_and_restore': hi.i18n.getLocalization('notification.task_copy_and_restore'),
			'project_copy_restore': hi.i18n.getLocalization('notification.project_copy_restore'),
			'project_restore': hi.i18n.getLocalization('notification.project_restore'),
			'task_duplicate': hi.i18n.getLocalization('notification.task_duplicate')
		},
		notificationDivId: 'notification',
		borderClassName : 'border',
		
		putMessageInQue: function (action) {
			if (!Hi_Notification.isValidAction(action)) {
				return;
			}
			
			var msg = Hi_Notification.notificationMessage[action], emptyQueue;
			
			if (Hi_Notification.messageQueue.length === 0) {
				emptyQueue = true;
				Hi_Notification.messageQueue.push(msg);
			} else {
				//Same message doesn't need to be displayed more than once in a row
				if (Hi_Notification.messageQueue[Hi_Notification.messageQueue.length - 1] != msg) {
					Hi_Notification.messageQueue.push(msg);
				}
			}
			
			if (emptyQueue) {
				Hi_Notification.checkQueue();
			}
			
		},
		
		checkQueue: function () {
			var queue = Hi_Notification.messageQueue;
			if (queue.length && queue.length > 0) {
				msg = queue[0];
				Hi_Notification.create(msg);
				return;
			}
		},
		
		create : function (msg) {
			notificationDiv = dojo.byId(Hi_Notification.notificationDivId);
			notificationDivChilds = notificationDiv.getElementsByTagName('DIV');
			if (notificationDivChilds.length) {
				notificationDiv.removeChild(notificationDivChilds[0]);
			}
			
			elDiv = Hi_Notification.createElement();
			notificationDiv.style.display = '';
			notificationDiv.style.opacity = '';
			
			if (notificationDiv.runtimeStyle) {
				notificationDiv.runtimeStyle.filter = 'Alpha(Opacity=100)';
			}
			
			Hi_Notification.setMessage(elDiv, msg);
			Hi_Notification.timer = setTimeout(Hi_Notification.hide, Hi_Notification.default_time * 1000);
					
		},
		
		createElement: function () {
			notificationDiv = dojo.byId(Hi_Notification.notificationDivId);
			elDiv = document.createElement('div');
			elDiv.className = Hi_Notification.borderClassName;
			notificationDiv.appendChild(elDiv);
			return elDiv;
		},
		
		show: function (el) {
			
		},
		
		hide: function () {
			var notificationDiv = dojo.byId(Hi_Notification.notificationDivId);
			
			notificationDiv.style.display = 'none';
			notificationDiv.style.opacity = '0';
			
			if (notificationDiv.runtimeStyle) {
				notificationDiv.runtimeStyle.filter = 'Alpha(Opacity=0)';
			}
		
			Hi_Notification.messageQueue.shift();
			
			setTimeout(Hi_Notification.checkQueue, 500);
		},
		
		setMessage: function (elDiv, msg) {
			elDiv.innerHTML = msg;
		},
		
		isValidAction: function (action) {
			var msgArray;
			msgArray = Hi_Notification.notificationMessage;
			for (var i in msgArray) {
				if (action == i) {
					return true;
				}
			}
			return false;
		}
	};

});