/**
 * Copyright (C) 2005-2009 Vide Infra Grupa SIA, Riga, Latvia. All rights reserved.
 * http://videinfra.com
 * Version 1.0
 */
define([
	"dojo/on",
	'hi/widget/task/item_DOM'
], function (dojo_on, item_DOM) {
	
	dojo.global.Hi_Shortcuts = {
		
		//Char codes
		KEY_A: 65,
		KEY_B: 66,
		KEY_C: 67,
		KEY_D: 68,
		KEY_E: 69,
		KEY_F: 70,
		KEY_G: 71,
		KEY_H: 72,
		KEY_I: 73,
		KEY_J: 74,
		KEY_K: 75,
		KEY_L: 76,
		KEY_M: 77,
		KEY_N: 78,
		KEY_O: 79,
		KEY_P: 80,
		KEY_R: 82,
		KEY_Q: 81,
		KEY_S: 83,
		KEY_T: 84,
		KEY_U: 85,
		KEY_V: 86,
		KEY_W: 87,
		KEY_X: 88,
		KEY_Y: 89,
		KEY_Z: 90,
		
		//Key codes
		KEY_ENTER: 13,
		KEY_ESCAPE: 27,
		KEY_DELETE: 46,
		
		KEY_LEFT_ARROW: 37,
		KEY_RIGHT_ARROW: 39,
		shortcutData: {},
		
		addKeyListener: function (key, callback) {
			var keyCode = key.keyCode;
			if (!Hi_Shortcuts.shortcutData[key.keyCode]) Hi_Shortcuts.shortcutData[key.keyCode] = [];
			
			Hi_Shortcuts.shortcutData[key.keyCode].push({
				'keyCode': key.keyCode,
				'ctrl': (key.ctrl ? true : false),
				'alt': (key.alt ? true : false),
				'shift': (key.shift ? true : false),
				'meta': (key.meta ? true: false),
				'callback': callback
			});
		},
		
		onKeyPress: function (evt) {
			var keyCode;

			if (evt.keyCode) {
				keyCode = evt.keyCode;
			} else {
				keyCode = evt.charCode;
			}
			
			if (Hi_Shortcuts.shortcutData[keyCode]) {
				for(var i=0,j=Hi_Shortcuts.shortcutData[keyCode].length; i<j; i++) {
					var k = Hi_Shortcuts.shortcutData[keyCode][i];
					
					if ((k.ctrl && !evt.ctrlKey) || (!k.ctrl && evt.ctrlKey)) continue;
					if ((k.alt && !evt.altKey) || (!k.alt && evt.altKey)) continue;
					if ((k.shift && !evt.shiftKey) || (!k.shift && evt.shiftKey)) continue;
					if ((k.meta && !evt.metaKey) || (!k.meta && evt.metaKey)) continue;

					if (k.callback) {
						var r = k.callback(k, keyCode, evt);
	
						if (r === false) {
							dojo.stopEvent(evt);
							return false;
						}
					}
				}
			}
			
			return true;
		},
		
		targetNotInput: function (evt) {
			evt = (evt ? evt : window.event);
			var targ = evt.target,
				tagName = (targ ? targ.tagName.toLowerCase() : '');
				
			if (tagName != 'input' && tagName != 'textarea' && tagName != 'select') {
				return true;
			} else {
				return false;
			}
		},
		
		init: function () {
			var self = this;

			//Listen for key press event
			dojo_on(document, 'keydown', this.onKeyPress);
			
			//Overwrite M -> switch to My view grouping
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_M}, function (k ,keyCode, evt) {
				var tab = dojo.global.tabs.main;
				
				if (self.targetNotInput(evt) && tab) {
					tab.open('my');
					return false;
				}
			});
			
			//Overwrite P -> switch to Project grouping
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_P}, function (k ,keyCode, evt) {
				var tab = dojo.global.tabs.main;

				if (self.targetNotInput(evt) && tab) {
					tab.open('project');
					return false;
				}
			});
			
			//Overwrite T -> switch to Team grouping
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_E}, function (k ,keyCode, evt) {
				var tab = dojo.global.tabs.main;

				if (self.targetNotInput(evt) && tab) {
					tab.open('team');
					return false;
				}
			});
			
			//Overwrite D -> switch to Date grouping
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_D}, function (k ,keyCode, evt) {
				var tab = dojo.global.tabs.main;

				if (self.targetNotInput(evt) && tab) {
					tab.open('date');
					return false;
				}
			});

			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_RIGHT_ARROW, ctrl: true}, function (k ,keyCode, evt) {
				var tab = dojo.global.tabs.main;

				if (self.targetNotInput(evt) && tab) {
					tab.openNext();
					return false;
				}
			});

			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_LEFT_ARROW, ctrl: true}, function (k ,keyCode, evt) {
				var tab = dojo.global.tabs.main;

				if (self.targetNotInput(evt) && tab) {
					tab.openPrevious();
					return false;
				}
			});
			
			//Overwrite A or N -> Add task (focus on input)
			var func_focus_title =  function (k ,keyCode, evt) {
				if (self.targetNotInput(evt)) {
					var item = hi.items.manager.item(-1);
					if (item) item.showEditForm();
					return false;
				}
			};
			var func_add_project = function (k, keyCode, evt) {
				if (self.targetNotInput(evt)) {
					if(Project.checkLimits('TRIAL_PROJECTS')) {
						Project.showProjectDialog();
						return false;
					}
				}
			};
			
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_N}, func_focus_title);
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_A}, function (k, keyCode, evt) {
				if (Hi_Preferences.get('currentTab', 'my', 'string') === 'project') {
					return func_add_project(k, keyCode, evt);
				}
			});
			
			//Overwrite H -> show task history
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_H}, function (k ,keyCode, evt) {
				if (hi.items.manager.opened() && self.targetNotInput(evt)) {
					Hi_Comments.loadComments();
					return false;
				}
			});
			
			//Overwrite C -> mark task completed
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_C}, function (k, keyCode, evt) {
				var opened = hi.items.manager.opened();
				if (!item_DOM.transitioning && opened && opened.taskId > 0 && self.targetNotInput(evt)) {
					item_DOM.handleClickComplete(opened.taskId, opened.taskItem.eventdate || null, evt);
					return false;
				}
			});
					
			//Overwrite Delete -> delete task
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_DELETE}, function (k ,keyCode, evt) {
				var opened = hi.items.manager.opened();
				if (opened && self.targetNotInput(evt) && opened.propertiesView) {
					opened.propertiesView.handleDeleteClick(evt);
					return false;
				}
			});
			
			//Overwrite S, F -> show search
			var func_show_search = function (k ,keyCode, evt) {
				if (self.targetNotInput(evt)) {
					Hi_Search.searchWidget.focus();
					return false;
				}
			};
			
			/*
			 * you can lose your task entry accidentally:
				1. click to add new item
				2. enter title
				3. press TAB, TAB
				4. press s
				item lost
			 */
			
			// As requested in #3221:
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_S}, func_show_search);
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_F, ctrl: true}, func_show_search);
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_F, shift: true}, func_show_search);
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_F, alt: true}, func_show_search);
			
			//Overwrite CTRL + S -> save item
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_S, ctrl: true}, function (k ,keyCode, evt) {
				var editing = hi.items.manager.editing();
				if (editing) {
					//Collapse and save
					hi.items.manager.collapseOpenedItems(true, true);
					return false;
				}
				return true;
			});
			
			//Overwrite O and Enter -> open task for editing
			var func_open_task_form = function (k, keyCode, evt) {
				if (self.targetNotInput(evt)) {
					//Temporary project (taskId < 0) is always in edit mode
					var opened = hi.items.manager.opened();
					if (opened && !opened.editing && opened.taskId > 0) {
						item_DOM.openEdit(opened);
						return false;
					}
				}
			};
			
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_O}, func_open_task_form);
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_ENTER}, func_open_task_form);
			
			//Overwrite CTRL+ESCAPE -> Cancel add task (if task form is opened)
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_ESCAPE}, function (k, keyCode, evt) {
				var e = (evt ? evt : window.event);
				var editing = hi.items.manager.editing(),
					opened  = hi.items.manager.opened();
				
				
				// #4308
				if (item_DOM.lightboxDialog) {
					item_DOM.lightboxDialog.hide();
				}
	
				if (editing) {
					//editing.closeEdit();
					setTimeout(function() {
						if (editing.editingView && editing.editingView.isNew) {
							hi.items.manager.remove(editing.editingView.taskId);
						} else {
							item_DOM.closeEdit(editing.taskId,editing.taskItem.eventdate||null);
						}
					},500);
					return false;
				} else if (opened) {
					setTimeout(function() {hi.items.manager.collapseOpenedItems(true, false);},500);
					return false;
				} else if (e.target && e.target.id == 'newProjectName') {
					Project.hideProjectDialog();
					return false;
				}
			});
			
			//ALT + Return submits comment
			function addComment (k, keyCode, evt) {
				if (!self.targetNotInput(evt)) {
					Hi_Comments.handleAddComment();
				}
				return false;
			} 
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_ENTER, ctrl: true}, addComment);
			this.addKeyListener({keyCode: Hi_Shortcuts.KEY_ENTER, alt: true}, addComment);
		}
	};
	
	Hi_Shortcuts.init();
	
	return dojo.global.Hi_Shortcuts;

});
