/*
dojo.provide("hi.comments");

dojo.require("hi.comments.base");
dojo.require("hi.comments.output");
dojo.require("hi.comments.date");
*/

define([
	
	"hi/comments/base",
	"hi/comments/output",
	"hi/comments/date"
	
], function () {
	return {};
});
