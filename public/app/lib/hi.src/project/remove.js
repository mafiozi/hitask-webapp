//dojo.provide("hi.project.remove");
define([
	'dojo/on',
	'hi/widget/task/item_DOM',
	"hi/project/base"
], function (dojo_on,item_DOM) {
	
	var Project = dojo.global.Project;
	
	dojo.mixin(Project, {
		
		/*Delete item*/
		removeItem: {},
		
		deleteRecurringOneBtnHandle:null,
		deleteRecurringAllBtnHandle:null,
		
		removeTask: function (id, options) {
			if (!Project.canDeleteItem(id)) {
				return false;
			}
			
			var recurring = parseInt(hi.items.manager.get(id, "recurring") || 0);

			/*recurring task*/
			if (options && options.instance_date && recurring && options.tarchive == undefined) {
				var instance_date = str2time(options.instance_date, 'y-m-d');
				
				if (options.target) {
					var btn = options.target;
				} else {
					var btn = dojo.byId('prop_act_delete_' + id);
				}
				
				//Tooltip.open('deleterecurring', "", btn);
				Tooltip.open('deleterecurring', "", btn, null, null, null, {orient:false});
				//Initialize tooltip if needed
				var tooltip = dojo.byId('tooltip_deleterecurring');
				if (!Project.removeTaskTooltipInitialized) {
					dojo.connect(tooltip, 'click', function (ev) {
					    dojo.stopEvent(ev);
					});
					Project.removeTaskTooltipInitialized = true;
				}
				
				var closeTooltip = function () {
					Tooltip.close('deleterecurring');
					hi.items.manager.collapseOpenedItems();
				};

				var todaySignal,allSignals;
				dijit.byId('deleteRecurringOneBtn').set('label',hi.i18n.getLocalization('task.delete_button')+' ' + instance_date.getDate() + ' ' + Hi_Calendar.months_short[instance_date.getMonth()])
				
				if (Project.deleteRecurringOneBtnHandle && Project.deleteRecurringOneBtnHandle.remove) {
					Project.deleteRecurringOneBtnHandle.remove();
				}
				
				Project.deleteRecurringOneBtnHandle = todaySignal = dojo_on.once(dijit.byId('deleteRecurringOneBtn'),'Click',function() {
					closeTooltip();
					/* Bug #2106 Project.remove(id, options); */
					// Cannot do this until we solve bug #1203 hi.items.manager.removeSave(id,options);
					Project.remove(id, options);
					todaySignal.remove();
					if (allSignals) allSignals.remove();
					Tooltip.close('taskedit');
				});
				allSignals = dojo_on.once(dijit.byId('deleteRecurringAllBtn'),'Click',function(e) {
					delete options.instance_date;
					options.cascade = true;
					closeTooltip();
					
					var item = hi.items.manager.item(id);
					if (item) {
						item_DOM.animate(item,"puff", false, function () {
							/* Bug #2106 Project.remove(id, options); */
							hi.items.manager.removeSave(id,options);
						});
					} else {
						/* Bug #2106 Project.remove(id, options); */
						hi.items.manager.removeSave(id,options);
					}
					allSignals.remove();
					if (todaySignal) todaySignal.remove();
					Tooltip.close('taskedit');
				});
				
			} else {
				
				var category = hi.items.manager.get(id, 'category');
				var message = hi.i18n.getLocalization('tooltip.delete_message_task_' + category);
				
				if (options.target) {
					var btn = options.target;
				} else {
					var btn = dojo.byId('prop_act_delete_' + id);
				}
				
				Project.showDeleteConfirmation(message, btn, function () {
					delete options.instance_date;
					options.cascade = true;
					var item = hi.items.manager.item(id);
					if (item) {
						item_DOM.animate(item,"puff", false, function () {
							/* Bug #2106 Project.remove(id, options);*/
							hi.items.manager.removeSave(id, options);
							Tooltip.close('taskedit');
						});
					} else {
						/* Bug #2016 Project.remove(id, options); */
						hi.items.manager.removeSave(id, options);
						Tooltip.close('taskedit');
					}
				});
			}
		},
		
		_test3333_refresh:function() {
			hi.store.Project.refreshStopTimer();
			hi.items.manager.refresh(true);
		},
				
		remove: function(id, options) {
			var formData = {};
			if (options && options.instance_date) {
				//Convert client-side instance_date to server-side, timezone difference
				var start_date = options.instance_date,                    // 31-12-2012
					start_time = Project.get(id, 'start_time'), // 12:34
					timezone_time = time2strAPI(new Date()).substr(-6);   // +12:00
					
				if(!start_time) start_time = '00:00';
				
				formData.instance_date_time = start_date + 'T' + start_time + ':00.000' + timezone_time;
			} else if (options && options.cascade) {
				formData.cascade = options.cascade;
			} else {
				formData = null;
			}
			/* Bug #1940 - on delete callback, refresh UI */
			hi.items.manager.removeSave(id, formData).then(function() {
				var def = hi.items.manager.refresh(true);
				item_DOM.globalHighlight=false;
				def.then(function() {item_DOM.globalHighlight=true;});
			});
			
		},
		
		removeCompleted: function (link, form) {
			var message = hi.i18n.getLocalization('tooltip.delete_completed_items');
			
			var node = dojo.byId('deleteconfirmation_message');
				node.innerHTML = message;
				
			var btn = link;
			
			Tooltip.open('deleteconfirmation', "", btn);
			
			//Initialize tooltip if needed
			var tooltip = dojo.byId('tooltip_deleteconfirmation');
			if (!Project.removeTaskTooltip2Initialized) {
				dojo.connect(tooltip, 'click', function (ev) {
				    dojo.stopEvent(ev);
				});
				Project.removeTaskTooltip2Initialized = true;
			}
			
			function closeTooltip () {
				Tooltip.close('deleteconfirmation');
			}
			/* #2292
			var btns = tooltip.getElementsByTagName('BUTTON');
				
				//Delete completed tasks
				btns[0].onclick = function () {
				    closeTooltip();
					form.onsubmit();
				};
				//Cancel 
				btns[1].onclick = function (event) {
				   dojo.stopEvent(event);
				   closeTooltip();
				};
				*/
			var okSignal,cancelSignal;
			okSignal = dojo_on.once(dijit.byId('deleteConfirmationOKBtn'),'Click',function() {
				closeTooltip();
				form.onsubmit();
				okSignal.remove();
				if (cancelSignal) cancelSignal.remove();
			});
			cancelSignal = dojo_on.once(dijit.byId('deleteConfirmationCancelBtn'),'Click',function(e) {
				dojo.stopEvent(e);
				closeTooltip();
				if (okSignal) okSignal.remove();
				cancelSignal.remove();
			});
			
		},
		
		removeCompletedItems: function(btn, callback) {
			var message = hi.i18n.getLocalization('tooltip.delete_completed_items');
			var action = function() {
				Project.ajax("tpurge");
				if (typeof(callback) == 'function') {
					callback();
				}
			};
			
			if (btn === false) {
				if (confirm(message)) {
					action();
				}
			} else {
				Project.showDeleteConfirmation(message, btn, function() {
					action();
				});
			}
		},
		removeArchivedItems: function(btn, callback) {
			var message = hi.i18n.getLocalization('tooltip.archive_completed_items');
			var action = function() {
				Project.ajax("tarchivepurge");
				if (typeof(callback) == 'function') {
					callback();
				}
			};
			
			if (btn === false) {
				if (confirm(message)) {
					action();
				}
			} else {
				Project.showDeleteConfirmation(message, btn, function() {
					action();
				});
			}
		}
		
	});

	return Project;

});