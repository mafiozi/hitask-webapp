package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.annotations.SerializedName;

import utils.HTMLUtils;

public class PublishedItem extends BaseBean implements Serializable {
	private static final DateTimeFormatter DUE_DATE_SHORT_FORMATTER = DateTimeFormat.forPattern("dd/MM");
	public static final int ITEM_TYPE_PROJECT = 0;
	public static final int ITEM_TYPE_TASK = 1;
	public static final int ITEM_TYPE_EVENT = 2;
	public static final int ITEM_TYPE_NOTE = 4;
	public static final int ITEM_TYPE_FILE = 5;
	public static final int ITEM_TYPE_PROJECT_ARCHIVE_MIRROR = -1;
	
	private String title;
	private Long id;
	@SerializedName("color_value")
	private String colorValue;
	private Integer category;
	private List<PublishedItem> items = new ArrayList<PublishedItem>();
	private Boolean starred = Boolean.FALSE;
	private Boolean completed = Boolean.FALSE;
	@SerializedName("message")
	private String description;
	@SerializedName("due_date")
	private String dueDateStr;
	
	public String getDueDateShort() {
		String d = null;
		DateTime dt = parseApiDateTime(dueDateStr, false);
		if (dt != null) {
			d = dt.toString(DUE_DATE_SHORT_FORMATTER);
		}
		
		return d;
	}

	public String getDueDateStr() {
		return dueDateStr;
	}

	public void setDueDateStr(String dueDateStr) {
		this.dueDateStr = dueDateStr;
	}

	public String getDescription() {
		return description;
	}
	
	public String getDescriptionTextiled() {
		return HTMLUtils.textileToHtml(description);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getStarred() {
		return starred;
	}

	public void setStarred(Boolean starred) {
		this.starred = starred;
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getColorValue() {
		return colorValue;
	}

	public void setColorValue(String colorValue) {
		this.colorValue = colorValue;
	}

	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	public List<PublishedItem> getItems() {
		return items;
	}

	public void setItems(List<PublishedItem> items) {
		this.items = items;
	}
	
	public String getCategoryIcon() {
		if (category != null && ArrayUtils.contains(new int[] {ITEM_TYPE_NOTE, ITEM_TYPE_EVENT}, category)) {
			switch (category) {
				case ITEM_TYPE_EVENT:
					return "&#xe01a;";
				case ITEM_TYPE_NOTE:
					return "&#xe035;";
				default:
					return null;
			}
		} else {
			return null;
		}
	}
	
	public boolean isCategoryFile() {
		return category != null && category == ITEM_TYPE_FILE;
	}
}
