//dojo.provide("hi.project.base");

define(function () {
	
	return dojo.global.Project = {
		
		ACCOUNT_LEVEL_FREE: 0,
		ACCOUNT_LEVEL_PREMIUM_EXPIRED: 50,
		ACCOUNT_LEVEL_PREMIUM: 100,
		ACCOUNT_LEVEL_BUSINESS_EXPIRED: 150,
		ACCOUNT_LEVEL_BUSINESS: 200,
		
		BUSINESS_ADMINISTRATOR_LEVEL: 100,
		
		ORDER_SUBJECT: 1,
		ORDER_START_DATE: 2,
		ORDER_LAST_MODIFIED: 3,
		ORDER_PRIORITY: 4,
		ORDER_DUE_DATE: 5,
		
		faster: true,
		session_id: false,
		session_name: false,
		firstUserLoad: true,
		dropProject: {},
		api_key: '',
		api_version: '',
		last_team_time: null,
		last_chat_time: null,
		
		expandedTasks: {},
		
		hasProjects: false,			//Only valid right after fillTasks when group == 4
		notInProjectCount: 0,		//Item count, which are in none of the projects, only if hasProjects == true
		hiddenTasksDrawn: false,
		hiddenTasksVisible: false,
		
		newItemDefaultTab: '1',
		inv_sent:[] // invitations
	};

});