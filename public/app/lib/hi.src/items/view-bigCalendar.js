/**
 * Instantiation of big calendar on Calendar tab.  
 */
define([
	'hi/store',
	'hi/items/base',
	'hi/store/Memory',
	'hi/taskcalendar',
	'hi/widget/task/item_DOM',
	'dojo/aspect',
	'dojo/store/Observable',
	'dojox/calendar/Calendar',
	'hi/widget/hiVerticalScrollBarBase',
	'dojo/_base/lang',
	'dojo/sniff',
	'dojo/on',
	'dojo/keys',
	'dojo/mouse', 
	'dojo/window',
	'dojo/Deferred',
	'dojo/_base/event',
	'hi/items/date',
	'dojo/json'
], function (hi_store, hi_items, hi_memory, hi_taskCalendar, item_DOM,
			dojo_aspect, observable, Calendar, _VerticalScrollBarBase,
			lang, sniff, on, keys, mouse, _window, Deferred, event, hiDate, dojoJson) {
	
	var util = supra.common;
	var afterLoadDone;

	dojo.global.bigCalendar = {
		id: 0,						//current task id

		store: null,				//store to hold the taks events

		widget: null,				//big calendar widget

		initialized: false,

		timeFormat: '24',

		_deferredUpdate: false,		//flag to show if an deferred update operation is needed

		_domWidth: 0,

		_editEventSt: null,

		_editEventEt: null,

		_canUpdate: true,

		_editMode: '',				//move, resizeStart, resizeEnd

		closeOpenedTooltip: function() {
			if (this.id) {
				//Remove item
				this.tooltipTaskList.remove({
					"id": this.id
				});
				
				this.id = null;

				if(this.widget){
					this.widget.set('scrollable', true);
				}

				Project.openedInstance = null;
				var openedIntance = hi.items.manager.opened();
				if (openedIntance && openedIntance._setOpenedAttr) {
					openedIntance._setOpenedAttr(openedIntance, false);
				}
				Tooltip.close('taskedit');
			}
		},

		setStore: function() {
			var i, item,
				w = this.widget,
				data = [], 
				cache = hi.items.manager.get(),
				len = cache.length,
				oldEndDate;

			for (i = 0; i < len; i++) {
				item = cache[i];
				if (item.category === hi.widget.TaskItem.CATEGORY_TASK || item.category === hi.widget.TaskItem.CATEGORY_EVENT) {
					data = data.concat(HiTaskCalendar.eventArray(item, true));
				}
			}
			len = data.length;
			for (i = 0; i < len; i++) {
				item = data[i];
				item.color = item.color ? item.color : 0;

				//Change #4594 - Do not show project name in calendar
				// var parentTitle = hi.items.manager.get(item.id, 'parentTitle');
				// if(parentTitle)
				// 	item.title = '&nbsp;<span class="taskParentTitle">' + htmlspecialchars(parentTitle) + '&nbsp;<span class="taskParentTitleArrow" data-icon="" aria-hidden="true"></span></span>' + item.title;

				// Feature #5854 - task with start_date and due_start shows multi-day
				// if (him.isTask(item)) {
				// 	if (item.due_date) {
				// 		// #5735: task item with due date is all day.
				// 		item.isAllDay = true;
				// 		item.startDate = new Date(item.due_date);
				// 		item.startDate.setHours(0);
				// 		item.startDate.setMinutes(0);
				// 		item.startDate.setSeconds(0);

				// 		item.endDate = new Date(item.due_date);
				// 	}
				// }
				if (item.isAllDay) {
					if (item.endDate && item.endDate.getHours() === 23 && item.endDate.getMinutes() === 59) {
						var _endDate = item.endDate.getTime();
						item.endDate = new Date(item.endDate.getFullYear(), item.endDate.getMonth(), item.endDate.getDate() + 1);
						// item.endDate.setSeconds(59);
						item.error = item.endDate.getTime() - _endDate;
					} else if (item.endDate && item.endDate.getHours() === 0 && item.endDate.getMinutes() === 0 && item.endDate.getSeconds() === 0) {
						item.endDate = new Date(item.endDate.getTime() + 86400000);
						item.error = 86400000;
					}
				}

				if (!item.endDate) {
					if (item.isAllDay) {
						item.endDate = new Date(item.startDate.getTime() + 24 * 60 * 60 * 1000 - 1000);
					} else {
						item.endDate = new Date(item.startDate.getTime() + 15 * 60 * 1000);
					}
				} else if (item.endDate.getTime() - item.startDate.getTime() <= 900000) {
					oldEndDate = item.endDate.getTime();
					item.endDate = new Date(item.startDate.getTime() + 900000);
					item.error = item.endDate.getTime() - oldEndDate;
				}
			}

			// dummy data test test dojox/calendar widget
			// var data = [
			// 	{
			// 		id: 0,
			// 		title: "Event 1",
			// 		startDate: new Date(2014, 4, 16),
			// 		endDate: new Date(2014, 4, 17, 1, 0),
			// 		isAllDay: true
			// 	},
			// 	{
			// 		id: 1,
			// 		title: "Event 2",
			// 		startDate: new Date(2014, 10, 1, 10, 0),
			// 		endDate: new Date(2014, 10, 3, 12, 0),
			// 		isAllDay: true
			// 	},
			// 	{
			// 		id: 2,
			// 		title: "Event 3",
			// 		startDate: new Date(2015, 9, 14, 0, 0),
			// 		endDate: new Date(2015, 9, 14, 23, 59, 0),
			// 		isAllDay: true,
			// 	}
			// ];
			// this.store = new observable(hi_memory({data: data, idProperty: 'uuid'}));
			this.store = new observable(hi_memory({data: data}));
			if (w) w.set('store', this.store);
		},

		update: function(){
			this.setStore();
		},

		adjustWidth: function(){
			var c = dojo.byId('calendar'),
				m = dojo.byId('main'),
				w = this.widget,
				messenger = dojo.byId('messenger');
			
			// modify width for bigCalender here
			dojo.addClass(m, 'bigCalendarContainer');
			if(w){
				this.resize();
			}
		},

		refresh: function() {
			if (this.widget && this._canUpdate === true) this.widget.refreshRendering(true);
		},

		resize: function() {
			if (!this._canUpdate) return;
			//calculate the height of the bigCalendar here
			var container = dojo.byId('bigCalendar'),
				centerPane = dojo.byId('centerPane');

			if(!container || !centerPane){ return; }

			// var headerH = dojo.marginBox(dojo.query('.header-inner', dojo.byId('header'))[0]).h,		//height of .header-inner
			var headerH = dojo.marginBox(dojo.byId('headerPane')).h,		//height of .header-inner
				mainMenuHeight = dojo.marginBox(dojo.query('#main .menu')[0]).h,
				// mainMt = dojo.style(dojo.query('.content-wrapper')[0], 'marginTop'),
				containerPt = dojo.style(container, 'paddingTop'),
				containerPb = dojo.style(container, 'paddingBottom'),
				containerBw = dojo.style(container, 'border-bottom-width'),
				centerPaneHeight, centerPanePaddingTop,
				h = 0;

			centerPaneHeight = centerPane.clientHeight;
			centerPanePaddingTop = dojo.style(centerPane, 'paddingTop');
			h = 0;

			h = centerPaneHeight - centerPanePaddingTop - containerPb - containerPt - containerBw - mainMenuHeight;
			// console.log(centerPaneHeight, centerPanePaddingTop, containerPb, containerPt, mainMenuHeight);

			if(this.widget){
				this.widget.resize({h: h});
				this._domWidth = this.widget.domNode.clientWidth;
			}
		},

		renderTab: function() {
			var cv,		//columnView
				sb,		//scrollBar 
				w,		//dojox/calendar
				c = dojo.byId('calendarPane'),
				m = dojo.byId('main'),
				t = this,
				messenger = dojo.byId('messenger'),
				container = dojo.byId('bigCalendar');

			if (!m || !c || !messenger) return;

			//hide element on the page for spaces for bigCalendar
			if (Hi_Preferences.get('sidebar_calendar', 1, 'int')) {
				util.hideElement(c);
			}
			util.hideElement(dojo.byId('mainContent'));
			util.hideElement(dojo.byId('calendarHideButton'));
			util.hideElement(dojo.byId('column1_text'));

			dojo.addClass(m, 'bigCalendarContainer');
			dojo.addClass(dojo.byId('centerPane'), 'bigCalendarContainer');

			dijit.byId('appContainer').resize();

			if(!this.initialized){
				dojo.parser.parse(container).then(function (instance) {
					w = t.widget = dijit.byId('bigCalendarWidget');
					t.timeFormat = Hi_Calendar.time_format;

					setTimeout(function(){
						util.hideElement(dojo.byId('column1_text'));
					}, 100);

					if(t.widget){
						if(w.columnView){
							cv = w.columnView;
							cv.set('rowHeaderTimePattern', t.timeFormat === '24' ? 'H:mm' : 'h:mm a');

							// Bug #4407
							// Hack calendar scrollBar to prevent scroll bigCalendar columnView when opening a task in popup.
							cv._viewHandles.shift().remove();
							cv._viewHandles.unshift(on(cv.scrollContainer, mouse.wheel, dojo.hitch(cv, function(e){
								if(Project.openedInstance){
									return false;
								}
								this._mouseWheelScrollHander(e);
							})));

							if(cv.scrollBar){
								cv.scrollBar.destroy(true);
								var sb = cv.scrollBar = new _VerticalScrollBarBase({
									content: cv.vScrollBarContent,
									scrollable: true
									// scrollCallback: function(e){
									// 	var st = this.domNode.scrollTop;
									// 	if(Project.openedInstance){
									// 		event.stop(e);
									// 		this.domNode.scrollTop = this._lastValue;
									// 		return false;
									// 	}
									// 	this._lastValue = this.value = this._getDomScrollerValue();
									// 	this.onChange(this.value);
									// 	this.onScroll(this.value);
									// }
								}, cv.vScrollBar);
								// sb.own(on(sb.domNode, "scroll", lang.hitch(sb, function(e) {
								// 	if(Project.openedInstance){
								// 		event.stop(e);
								// 		return false;
								// 	}

								// 	this.value = this._getDomScrollerValue();
								// 	this.onChange(this.value);
								// 	this.onScroll(this.value);
								// })));

								sb.on("scroll", lang.hitch(cv, function(e){
									if (!t.inPrint) sb._curScrollTop = sb.domNode.scrollTop;
									if (Project.openedInstance) return false;
									this._scrollBar_onScroll(e);
								}));
							}
						}
						if (w.matrixView) {
							w.matrixView.set('cellPaddingTop', 20);
						}
						w.on('itemClick', lang.hitch(t, t.taskClick));
						w.on('itemEditEnd', lang.hitch(t, t.taskEdit));
						w.on('gridDoubleClick', lang.hitch(t, t.gridDoubleClick));
						w.on('itemEditBegin', function(e) {
							t._editEventSt = w.newDate(e.item.startTime);
							t._editEventEt = w.newDate(e.item.endTime);
						});
						w.on('itemEditBeginGesture', function(e) {
							t._editEventSt = w.newDate(e.item.startTime);
							t._editEventEt = w.newDate(e.item.endTime);
							t._editMode = e.editKind;
							hi.items.manager.collapseOpenedItems(true);
						});

						//add empty event listeners here for later use
						w.on('itemEditMoveGesture', function(e) {});
						w.on('itemEditResizeGesture', function() {});
						w.on('itemEditEndGesture', function() {});

						t.setStore();
						t.initialized = true;

						if (afterLoadDone) {
							afterLoadDone.then(function() {
								setTimeout(function() {
									if (w.get('dateInterval') !== 'month') {
										cv.set("startTimeOfDay", {hours: 8, duration: 0});
									}
								}, 10);
							});
						}
					}
				});
			}

			util.showElement(dojo.byId('bigCalendar'));
			if(t.widget){
				dojo.global.preLoadDoneDeferred.then(function () {
					t.resize();
				});
			}
			if(this._deferredUpdate){
				this.update();
			}
			/* Bug #4327 */
			if (dojo.byId('tooltip_taskedit')) {
				hi.items.manager.collapseOpenedItems(true);
			}
		},

		unrenderTab: function(){
			var c = dojo.byId('calendarPane'),
				m = dojo.byId('main'),
				messenger = dojo.byId('messenger');

			if(!c || !m || !messenger){ return; }

			// util.showElement(c);
			if (Hi_Preferences.get('sidebar_calendar', '1', 'int')) {
				util.showElement(c);
			}
			util.showElement(dojo.byId('mainContent'));

			if (Hi_Preferences.get('sidebar_calendar', 0, 'int')) {
				util.showElement(dojo.byId('calendarHideButton'));
			} else {
				util.showElement(dojo.byId('column1_text'));
			}

			//modify width for bigCalender here
			dojo.removeClass(m, 'bigCalendarContainer');
			if(dojo.hasClass(messenger, 'no_column1')){
				dojo.addClass(m, 'no_column1');
			}else{
				dojo.removeClass(m, 'no_column1');
			}

			if(dojo.hasClass(c, 'invisible')){
				util.showElement(dojo.byId('column1_text'));
			}else{
				dojo.removeClass(m, 'no_column1');
			}

			dojo.removeClass(m, 'bigCalendarContainer');
			dojo.removeClass(dojo.byId('centerPane'), 'bigCalendarContainer');
			util.hideElement(dojo.byId('bigCalendar'));
			dijit.byId('appContainer').resize();
		},

		gridDoubleClick: function(e) {
			if (!e || !e.date) return;

			var t = this,
				w = t.widget,

				okBtn,
				cancelBtn,
				
				dateInterval = w.get('dateInterval'),
				createEventTitleInput,
				
				okSignal,
				cancelSignal,
				
				inSubmit = false,
				eventDate = e.date,
				gridTable = e.source.gridTable,
				isAllDay = dojo.hasClass(gridTable.parentNode.parentNode, 'dojoxCalendarMatrixView') || dateInterval === 'month';
				target = t._getEventCalendarGridCell(e, dateInterval);

			openTooltip(target);

			function submit(){
				if (inSubmit) return;
				inSubmit = true;

				var title = createEventTitleInput ? createEventTitleInput.get('value') : '',
					// isAllDay = dateInterval === 'month' ? true : false,
					alertElementCloseBtn, startDate, startTime, endDate,
					endTime, alertCloseSignal;

				// startDate = eventDate ? time2str(eventDate, "y-m-d") : "";
				startDate = dojo.clone(eventDate);
				endDate = dojo.clone(eventDate);

				if (!isAllDay) {
					var minutes = eventDate.getMinutes();
					minutes = Math.floor(minutes / 15) * 15;

					startDate.setMinutes(minutes);
					startDate.setSeconds(0);

					endDate.setMinutes(minutes + 15);
					endDate.setSeconds(0);

					startDate = time2strAPI(HiTaskCalendar.getDateTimeFromStr(time2str(startDate, "y-m-d"), time2str(startDate, "h:i:s")));
					endDate = time2strAPI(HiTaskCalendar.getDateTimeFromStr(time2str(endDate, "y-m-d"), time2str(endDate, "h:i:s")));
					// endDate = time2strAPI(HiTaskCalendar.getDateTimeFromStr(endDate, endTime));
				} else {
					/* #4383 */
					startDate = time2strAPI(HiTaskCalendar.getDateFromStr(time2str(startDate, "y-m-d"), true));
					endDate.setHours(23);
					endDate.setMinutes(59);
					endDate.setSeconds(59);
					endDate.setMilliseconds(999);
					endDate = time2strAPI(endDate);
					//startDate = time2strAPI(HiTaskCalendar.getDateFromStr(startDate, true));
				}
				
				if (!title || title.trim() === '') {
					Tooltip.close('bigCalendarCreateEvent');
					Project.alertElement.showItem(14, {task_alert:'Please specify a title'});

					alertElementCloseBtn = dojo.byId('alertElement_close');
					if(alertElementCloseBtn){
						alertCloseSignal = on.once(alertElementCloseBtn, 'click', function(){
							alertCloseSignal.remove();
							openTooltip(target);
							createEventTitleInput.focus();
						});
					}
					inSubmit = false;
					return false;
				}

				t._createEvent(title, startDate, endDate, isAllDay).then(function(id) {
					closeTooltip();
					inSubmit = false;
				}, function(){
					//handle error here
				});
			}

			function openTooltip(target){
				if(!target) {
					return;
				}

				Tooltip.open('bigCalendarCreateEvent', '', target);

				createEventTitleInput = dijit.byId('bigCalendarCreateEventTitle');
				okBtn = dijit.byId('bigCalendarCreateEventOKBtn');
				cancelBtn = dijit.byId('bigCalendarCreateEventCancelBtn');

				createEventTitleInput.set('value', '');
				if(createEventTitleInput){
					createEventTitleInput.focus();
				}

				if(createEventTitleInput && createEventTitleInput._keyHandler){
					createEventTitleInput._keyHandler.remove();
				}
				createEventTitleInput._keyHandler = on(createEventTitleInput, 'keypress', function(e){
					var code = e.charCode || e.keyCode;

					if(code === keys.ENTER){
						submit();
					}else if(code === keys.ESCAPE){
						closeTooltip();
					}
				});

				if(okBtn && okBtn._clickHandler){
					okBtn._clickHandler.remove();
				}
				okSignal = okBtn._clickHandler = on(okBtn, 'click', function(e) {
					/* Bug #4383 */
					submit();
				});

				if(cancelBtn && cancelBtn._clickHandler){
					cancelBtn._clickHandler.remove();
				}
				cancelSignal = cancelBtn._clickHandler = on(cancelBtn, 'click', function(e){
					var createEventTitleInput = dijit.byId('bigCalendarCreateEventTitle');

					event.stop(e);
					if(createEventTitleInput){
						createEventTitleInput.set('value', '');
					}
					closeTooltip();
				});
			}

			function closeTooltip(){
				var createEventTitleInput = dijit.byId('bigCalendarCreateEventTitle');

				Tooltip.close('bigCalendarCreateEvent');
				if(createEventTitleInput){
					createEventTitleInput.set('value', '');
				}
			}
		},

		taskClick: function(e) {
			// return false;
			if (!e || !e.item || !e.item.uuid || hi.items.manager.editing()){ return; }

			this._inTaskClick = true;
			if (Project.checkLimits("TRIAL_TASKS")) {
				var taskId = e.item.uuid,
					target = e.triggerEvent.target,
					opened = hi.items.manager.openedId();
				/* Bug #2064 - id < 0 indicates a sub item is opened, so block */
				if(opened < 0) {
					this._inTaskClick = false;
					return;
				}/* #2973 - close tooltip if it is already opened for this item */
				/* Also check if tooltip is opened, since opened might refer to an expanded item
				 * in a task list
				 */
				if(opened == taskId && Tooltip.opened('taskedit')){
					this.closeOpenedTooltip();
					this._inTaskClick = false;
					return;
				}
				
				//If there is an item remove it
				if(this.id){
					//Remove item
					this.tooltipTaskList.remove({
						"id": this.id
					});
					
					this.id = null;
					Project.openedInstance = null;
					Tooltip.close('taskedit');
				}
				
				//Make sure all other opened tasks are closed
				hi.items.manager.collapseOpenedItems(true, true);
				
				/*Bug #4130 Calendar pop-up is not opened for all-day tasks*/
				if(taskId){
					Project.openedInstance = time2str(e.item.startDate, 'y-m-d');
					this.id = taskId;
					this.showTaskEditTooltip(taskId, target);
				}
			}
		},

		taskEdit: function(e){
			if (!e || !e.item || !e.item._item) return;

			var obj,
				hiItem,
				t = this,
				em = t._editMode,
				startTime = e.item.startTime,
				endTime = e.item.endTime,
				taskId = e.storeItem.uuid,
				item = e.storeItem,
				hiItemStartDate = '',
				hiItemEndDate = '',
				recurringEndTime;

			function cancelEvent(){
				e.preventDefault();
				e.item.startTime = t._editEventSt;
				e.item.endTime = t._editEventEt;
				return false;
			}

			if (!taskId) {
				return;
			}
			hiItem = hi.items.manager.get(taskId);
			hiItemStartDate = hiItem.start_date && new Date(hiItem.start_date);
			hiItemEndDate = hiItem.end_date && new Date(hiItem.end_date);
			
			if (!Hi_Actions.validateAction('modify', hiItem)) {
				return cancelEvent();
			}
			// Bug #4364
			// If move recurring items to invalid dates(like date that exceeds its recurring end date), 
			// cancel the current edit event.
			if (hiItem.recurring) {
				if(startTime.toLocaleDateString() !== item.startDate.toLocaleDateString()){
					console.warn('cancel edit recurring event!');
					return cancelEvent();
				}

				recurringEndTime = typeof hiItem.recurring_end_date === 'string' ? new Date(hiItem.recurring_end_date) : hiItem.recurring_end_date;
				if(startTime.getTime() - recurringEndTime.getTime() > 0 || endTime.getTime() - recurringEndTime.getTime() > 24 * 60 * 60 * 1000){
					console.warn('can not set time for recurring item');
					return cancelEvent();
				}
				// recurring event can only change time not date
				if (hiItem.start_date) {
					hiItemStartDate = new Date(hiItem.start_date);
					// Bug #5607 
					// only first recurring item could be edited
					if (hiItemStartDate.getTime() !== item.startDate.getTime()) {
						return cancelEvent();
					}
					startTime.setFullYear(hiItemStartDate.getFullYear());
					startTime.setMonth(hiItemStartDate.getMonth());
					startTime.setDate(hiItemStartDate.getDate());
				}
				if (hiItem.end_date) {
					hiItemEndDate = new Date(hiItem.end_date);
					endTime.setFullYear(hiItemEndDate.getFullYear());
					endTime.setMonth(hiItemEndDate.getMonth());
					endTime.setDate(hiItemEndDate.getDate());
				}
				if(!e.item._item.end_date && (endTime - startTime === 15 * 60 * 1000) && mode !== 'resizeEnd'){
					endTime = '';
				}
			} else {
				/* Bug #4353
				 * if edit a task without an end date, bigCalendar should not set 
				 * the end date in the event
				 */
				if (!e.item._item.end_date && (endTime - startTime === 15 * 60 * 1000)) {
					endTime = '';
				} else {
					endTime = new Date(endTime.getTime() - (e.item && e.item._item && e.item._item.error? e.item._item.error : 0));
				}
				if (hiItem.is_all_day) {
					if (!item.end_date && em === 'move') {
						endTime = '';
					} else {
						// Bug #5735
						// google_allday_event = {start_date: 2015-01-01, end_date: 2015-01-01}
						if (endTime.getMinutes() === 0 && endTime.getHours() === 0 && endTime.getSeconds() === 0) {
						} else {
							// endTime = new Date(endTime.getTime() - 1);
						}
					}
				}
			}
			obj = {id: item.uuid};
			obj.start_date = time2strAPI(startTime);
			obj.end_date = endTime? time2strAPI(endTime) : '';
			
			if (hiItem.category === hi.widget.TaskItem.CATEGORY_TASK) {
				if (hiItem.due_date) {
					if (!hiItem.start_date) {
						delete obj.start_date;
						obj.due_date = obj.end_date;
					} else {
						// if (hiItemEndDate && hiItemStartDate) {
						// 	var initDiff = hiItemEndDate.getTime() - hiItemStartDate.getTime();
						// 	obj.start_date = new Date(endTime.getTime() - initDiff);
						// 	obj.start_date = time2strAPI(obj.start_date);
						// }
						
					}
				} else {
					delete obj.end_date;
				}
			// 	// if (endTime && hiItemStartDate && endTime.getTime() < hiItemStartDate.getTime()) {
			// 	// 	// obj.start_date = time2strAPI(startTime);
			// 	// }
			}
			hi.items.manager.setSave(obj);
			item_DOM._save(taskId, obj);
		},
		
		showTaskEditTooltip: function (id, el) {
			this.initTaskEditTooltip();
			
			var t = this,
				ul = dojo.byId('taskedit_tooltip').getElementsByTagName('UL');

			if (ul.length) {
				ul = ul[0];
				dojo.removeClass(dojo.byId('tooltip_taskedit'), 'expanded');
				
				var is_completed = hi.items.manager.getIsCompleted(id, Project.openedInstance);
				/* #2791 - convert openedInstance time to unambiguous time */
				var next_event_date = Project.getNextEventDate(id, new Date(str2timeAPI(Project.openedInstance)));
				var def = this.tooltipTaskList.add(hi.items.manager.get(id), is_completed, next_event_date,/* #2556 - tell TaskList this goes on tooltip, so ignore parent attribute */true);
				def.then(function(widget) {
					if(t.widget){
						t.widget.set('scrollable', false);
					}

					if (widget) {
						/*
						widget.watch("editing", function (name, prevVal, newVal) {
							if (newVal) {
								//Feature #1721 HiTaskCalendar.taskTooltipAdjust(el);
								HiTaskCalendar._taskTooltipAdjust(el);
							}
						});
						*/
						dojo.connect(widget, '_setEditingAttr', widget, function(w, newVal) {
							if (newVal) this._taskTooltipAdjust(el);
						});
						
						widget.openedConnect = dojo.connect(widget, '_setOpenedAttr', widget, function(w, newVal) {
							if (!newVal && t.taskEditTooltipVisible() && this.taskId == t.id){
								Tooltip.close('taskedit');
								dojo.disconnect(widget.openedConnect);
							}
						});
						/*
						widget.watch("opened", function (name, prevVal, newVal) {
							//if (id == HiTaskCalendar.id ) console.log('widget watch',[name,prevVal,newVal]);
							if (newVal != prevVal && !newVal && HiTaskCalendar.taskEditTooltipVisible() && id == HiTaskCalendar.id) {
								//Close tooltip if item was opened
								//console.log('CLOSE TASKEDIT',[widget,name,newVal,prevVal]);
								Tooltip.close('taskedit');
							}
						});
						*/
						//Feature #1721 HiTaskCalendar.taskTooltipAdjust(el);
						t._taskTooltipAdjust(el);
					}
				});
			}
		},

		initTaskEditTooltip: function(){
			hi_taskCalendar.initTaskEditTooltip.apply(this, []);
		},

		_taskTooltipAdjust: function(el){
			if (!el || !el.parentNode) return;

			var ep = dojo.position(el),		//element position
				pos_l = Math.floor((ep.x + ep.w / 2)),
				tooltip_el = dojo.byId('tooltip_taskedit'), t = this,
				_checkPosition = function(node) {
					console.log(node);
					var np = dojo.position(node),
						windowBox = _window.getBox(),
						offset = {x: 0, y: 0};

					if (np.x + np.w > windowBox.w) {
						offset = np.x + np.w - windowBox.w;
					}

					left = dojo.style(node, 'left');

					console.log('offset is', offset);
					left -= offset;
					node.style.left = left + 'px';
				};
			
			//Use the onEnd callback on the fade in animation to remove class that hides the close icon
			//We need to do this because the taskedit tooltip content is from itemProperties, which is
			//also used by tasks in the team, project, etc. views
			Tooltip.createTooltipDialog('taskedit', "", el, null, 'after-centered', null, {
				onEnd:function() {
					var tooltipDialog = dojo.byId('tooltip_taskedit_TooltipDialog');
					// _checkPosition(tooltipDialog.parentNode);
					dojo.style(tooltipDialog, '-webkit-user-select', 'none');

					if(!tooltipDialog.signal){
						tooltipDialog.signal = on(tooltipDialog, 'click', function(e){
							Click.setClickFunction(dojo.hitch(hi.items.manager, hi.items.manager.collapseOpenedItems, false/*force to collapse*/), true, "taskItemManager");
						});
					}

					setTimeout(function() {
						var closeNode = dojo.query('#tooltip_taskedit_TooltipDialog .hiTaskTooltipDialogTaskClose');
						if (closeNode && closeNode.length > 0) {
							dojo.removeClass(closeNode[0],'taskEdit_Close');
						}
					}, 500);
				},
				// orient: ['after-centered']
				// orient: ['after-centered', 'before-centered', 'below']
				orient: ['after-centered', 'before-centered']
			});
			// dojo.style(dijit.byId('tooltip_taskedit_TooltipDialog').domNode.parentNode, 'left', pos_l + 'px');
			// t._taskTooltipAdjustVerticalPos(el);
			// dojo.style(dijit.byId('tooltip_taskedit_TooltipDialog').domNode.parentNode, 'top', 10000 + 'px');

		},

		_getEventCalendarGridCell: function(e, mode){
			var colIndex,		//undefined
				rowIndex,		//undefined
				cell,
				date = e.date,
				w = this.widget,
				target = e.triggerEvent.target;
				targetParent = target && dojo.query(target).closest('tr')[0],
				gridTable = e.source.gridTable,
				firstDayOfWeek = Hi_Calendar.startDay,
				dayMapping = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

			if(!w || !date || !target || !targetParent || !gridTable){
				return false;
			}
			if(mode === 'month'){
				if(date && typeof date === 'object'){
					colIndex = date.getDay() - firstDayOfWeek;
					colIndex = colIndex < 0 ? colIndex + 7 : colIndex;
				}

				rowIndex = dojo.query('.dojoxCalendarMatrixView.view .dojoxCalendarContainer .dojoxCalendarContainerTable tr', w.domNode).indexOf(targetParent);
				if(rowIndex !== undefined && colIndex !== undefined){
					if(gridTable){
						cell = gridTable.rows[rowIndex].cells[colIndex];
					}
				}
			}else if(mode === 'week' || mode === 'day'){
				var isAllday = dojo.hasClass(gridTable.parentNode.parentNode, 'dojoxCalendarMatrixView');
				rowIndex = date.getHours() * 4 + Math.floor(date.getMinutes() / 15);
				var weekDay = dayMapping[date.getDay()];
				var eventTr = isAllday? dojo.query('tr', gridTable)[0] : dojo.query('tr', gridTable)[rowIndex];

				if(eventTr){
					cell = dojo.query('td.' + weekDay, eventTr)[0];
				}
			}
			return cell;
		},

		_createEvent: function(/*string*/title, /*string*/startDate, /*string*/endDate, /*bool*/isAllDay){
			var evt = this._getEmptyEvent();
			evt.title = title;
			evt.start_date = startDate;
			if(endDate){
				evt.end_date = endDate;
			}
			evt.is_all_day = isAllDay;
			evt.shared = Project.businessId && Hi_Preferences.get('default_shared', false, 'bool') ? true : false;
			if (Project.businessId) {
				evt.permissions = [];
				evt.permissions.push({principal: Project.user_id + '', level: dojo.global.Hi_Permissions.EVERYTHING});
				evt.permissions = dojoJson.stringify(evt.permissions);
			}

			var firePostResponse = firePOST({
				post: hi.items.manager.setSave,
				postArgs: [evt]
			});

			return firePostResponse.deferred;
		},

		_getEmptyEvent: function(){
			var evt = {
				_participant_select: "0",
				assignee: 0,
				category: 2,
				color: 0,
				completed: 0,
				end_date: "",
				is_all_day: false,
				message: "",
				parent: "",
				participants: [],
				priority: 21801,
				prop_act_upload: "",
				recurring: 0,
				recurring_interval: "1",
				reminder_enabled: false,
				reminder_time: 0,
				reminder_time_type: "m",
				shared: 1,
				starred: false,
				start_date: "",
				tags: [],
				time_est: 0,
				time_track: false,
				title: "",
				user_id: Project.user_id
			};

			return evt;
		},
		
		/**
		 * Feature #1721
		 */
		_taskTooltipAdjustVerticalPos: hi_taskCalendar._taskTooltipAdjustVerticalPos,
		
		taskTooltipAdjustVerticalPos: hi_taskCalendar.taskTooltipAdjustVerticalPos,

		taskEditTooltipVisible: hi_taskCalendar.taskEditTooltipVisible
	};

	//After hi.store.init create color groups
	dojo.global.viewBigCalendarTab = function(q){
	//dojo.connect(hi_store, "init", hi_store, function () {
		// Create task groups (each color + default list) and set filters
		

		var readStore = hi_store.ProjectCache,
			writeStore = hi_store.Project,
			query = q,//readStore.query(),
			calendarStore = null,
			
			groupSelf = 'calendar',
			groupCurrent = Hi_Preferences.get('currentTab', 'my', 'string'),
			// container = dojo.
			
			i = 1,
			count = 7,
			group = null,
			taskList = null,
			initialized = false,
			util = supra.common,
			calendarWidget = null,
			cnnt = dojo.connect;

		afterLoadDone = new Deferred();

		var myC = dojo.connect(hi_store, 'afterLoad', function() {
			var container = dojo.byId('bigCalendar');

			container && dojo.removeClass(container, 'loading');
			bigCalendar.setStore();
			dojo.disconnect(myC);
			afterLoadDone.resolve();
		});
		dojo.connect(window, 'onresize', function(){
			bigCalendar.resize();
		});

		/*Bug #4341*/
		cnnt(Tooltip, 'position', function(){
			var w = bigCalendar.widget;

			if(Hi_Preferences.get('currentTab', 'my', 'string') === groupSelf && w && w.domNode.clientHeight > 0 && w._domWidth !== w.domNode.clientWidth){
				w._domWidth = w.domNode.clientWidth;
				bigCalendar.widget.currentView._refreshItemsRendering();
			}
		});

		cnnt(Tooltip, 'close', function(name){
			var w = bigCalendar.widget;

			if(!w){
				return false;
			}

			if(name === 'taskedit'){
				w.set('scrollable', true);
			}
			if(Hi_Preferences.get('currentTab', 'my', 'string') === groupSelf && w && w.domNode.clientHeight > 0 && w._domWidth !== w.domNode.clientWidth){
				w._domWidth = w.domNode.clientWidth;
				if(w.currentView){
					w.currentView._refreshItemsRendering();
				}
			}
		});
		// };

		Click.setClickFunction(dojo.hitch(hi.items.manager, hi.items.manager.collapseOpenedItems, false/*force to collapse*/), true, "taskItemManager");

		// var _itemClick = function(e){
		// 	var id = e.item && e.item.id;
		// 	var opened = hi.items.manager.openedId();
			
		// 	/* #2973 - close tooltip if it is already opened for this item */
		// 	/* Also check if tooltip is opened, since opened might refer to an expanded item
		// 	 * in a task list
		// 	 */
		// 	if(opened == id && Tooltip.opened('taskedit')){
		// 		HiTaskCalendar.closeOpenedTooltip();
		// 		this._inTaskClick = false;
		// 		return;
		// 	}
			
		// 	//If there is an item remove it
		// 	if(HiTaskCalendar.id){
		// 		//Remove item
		// 		HiTaskCalendar.tooltipTaskList.remove({
		// 			"id": HiTaskCalendar.id
		// 		});
				
		// 		HiTaskCalendar.id = null;
		// 		Project.openedInstance = null;
		// 		Tooltip.close('taskedit');
		// 	}
			
		// 	//Make sure all other opened tasks are closed
		// 	hi.items.manager.collapseOpenedItems(true, true);
			
		// 	/*Bug #4130 Calendar pop-up is not opened for all-day tasks*/
		// 	if(taskId){
		// 		Project.openedInstance = time2str(HiTaskCalendar.currentDate, 'y-m-d');
		// 		HiTaskCalendar.id = taskId;
		// 		HiTaskCalendar.showTaskEditTooltip(taskId, this);
		// 	}
		// }


		if (groupCurrent == groupSelf) {
			bigCalendar.renderTab();
		} else {
			bigCalendar.unrenderTab();
		}
		
		hi_items.GroupColor = [];
		hi_items.TaskListColor = [];

		dojo.connect(HiTaskCalendar, 'update', function() {
			var container = dojo.byId('bigCalendar');

			if(Hi_Preferences.get('currentTab', 'my', 'string') === groupSelf && dojo.style(container, 'display') !== 'none'){
				bigCalendar.update();
			}else{
				bigCalendar._deferredUpdate = true;
			}
		});

		dojo.connect(HiTaskCalendar, 'rolloverNextDay', function(init){
			if(init !== true){
				bigCalendar.refresh();
			}
		});
		
		dojo.connect(hi.items.manager, "groupingChange", hi.items.manager, function (group, groupPrev) {
			// summary:
			//		On group change enable/disable lists
			var util = supra.common;

			if (group == groupSelf && groupPrev != groupSelf) {
				bigCalendar.renderTab();
			} else if (group != groupSelf && groupPrev == groupSelf) {
				bigCalendar.unrenderTab();
				HiTaskCalendar._scrollToFirstItem();
				HiTaskCalendar.scrollToHour(8);

				// dojo.removeClass(dojo.byId('centerPane'), 'bigCalendarContainer');

				for (i = 0; i<count; i++) {
					hi_items.GroupColor[i].set("disabled", true);
				}
				hi_items.TaskListColorUngrouped.set("disabled", true);
			}else {

			}
			
			groupCurrent = group;
		});

		dojo_aspect.after(dojo.global, "toggleColumn", function (deferred) {
			if(deferred && deferred.then){
				//return deferred here to make aspect.after to be able to chained
				return deferred.then(function(){
					var groupCurrent = Hi_Preferences.get('currentTab', 'my', 'string'),
						w = bigCalendar.widget;

					if(groupSelf === groupCurrent){
						bigCalendar.resize();

						//hack chrome to force the browser to re-render
						if(sniff('chrome')){
							w.domNode.style.height = w.domNode.clientHeight + 1 + 'px';
							w.domNode.style.height = w.domNode.clientHeight - 1 + 'px';
						}
					}
				});
			}
		});
	};
});