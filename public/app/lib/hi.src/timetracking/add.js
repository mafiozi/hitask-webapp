//dojo.provide("hi.timetracking.add");

/**
 * Add entry form
 */
define(function () {
	
	Hi_TimeTracking.addForm = {
		node: null,		//Form container node
		node_spent: null,
		node_date: null,
		calendar_widget: null,
		
		getDate: function () {
			var value = this.node_date.value; //dojo.byId('addEntryCalendar').value;
			var m;
			
			//IE reports incorrect date format?
			if (m = value.match(/(\d{4}).(\d{2}).(\d{2})/)) {
				value = m[2] + '/' + m[3] + '/' + m[1];
			}
			
			return value;
		},
		
		_onDateChange: function () {
			var date = str2time(Hi_TimeTracking.addForm.getDate(), 'm/d/y');
			if (!date) return;
			
			var dateFormatted = time2LocalizedStr(date);
			this.node_spent.innerHTML = dateFormatted;
		},
		
		/**
		 * Save form data and hide form
		 */
		save: function () {
			if (!this.node) return;
			
			var inputs = Hi_TimeTracking.getInputs(this.node);
			var minutes = parseTimeString(inputs.minutes.value);
			if (minutes === null) return;
			
			var date = str2time(Hi_TimeTracking.addForm.getDate(), 'm/d/y');
			
			if (!date) {
				date = HiTaskCalendar.currentDate;
			} else {
				var calCurYear = HiTaskCalendar.currentDate.getFullYear();
				var calCurMonth = HiTaskCalendar.currentDate.getMonth();
				var calCurDay = HiTaskCalendar.currentDate.getDate();
				
				if (date.getFullYear() > calCurYear
					|| (date.getFullYear() == calCurYear && date.getMonth() > calCurMonth)
					|| (date.getFullYear() == calCurYear && date.getMonth() == calCurMonth && date.getDate() > calCurDay)) {
					
					Project.alertElement.showItem(14,{task_alert:'Future date is not acceptable'});
					return;
				}
			}
			
			date = time2str(date, "y-m-d");
			
			var max_allowed = Hi_TimeTracking.list.getMaxAllowedTime(inputs.user.value, date);
			if (minutes > max_allowed) minutes = max_allowed;
			if (!minutes) this.reset();
			
			var postData = {
				'user_id': inputs.user.value,
				'time_spent': minutes,
				'date': date,
				'description': ''
			};
			
			Hi_TimeTracking.list.addItem(postData);
			
			Hi_TimeTracking.addForm.reset();
			
			var node_report = this.node.parentNode.parentNode;
			dojo.removeClass(node_report, 'time-track-notimespent');
		},
		
		/**
		 * Reset form data and hide form
		 */
		reset: function () {
			if (!this.node) return;

			var link_user = dojo.query('a.user', this.node)[0];
			if(!link_user) { return; }

			var friends = [];
			// add "myself" to top of the list
			friends.push({
				'name': Project.user_name, 
				'id': Project.user_id, 
				'image': 'url(' + dojo.global.getAvatar(16) + ')'
			});
			if(dojo.isArray(Project.friends)){
				for(var i in Project.friends){
					friends.push(Project.friends[i]);
				}
			}
			
			var default_friend = friends[0];
			var inputs = Hi_TimeTracking.getInputs(this.node);
			
			if (default_friend && default_friend.id) {
				default_friend.image = 'url(' + dojo.global.getAvatar(16, default_friend.id) + ')';
			}
			Hi_PersonSelect.fillPersonEntry(link_user, default_friend);
			
			if (this.calendar_widget) {
				// #2594
				// fails here on PROD only (if minified)
				/*
				 * Uncaught TypeError: Cannot call method 'setAttribute' of undefined
					_708._setValueAttrdojo.js:9260
					_d19dojo.js:17143
					_d75._setValueAttrdojo.js:17776
					_85e.setdojo.js:10810
					require.cache.hi/timetracking/add.Hi_TimeTracking.addForm.reset/public/app/lib/dojo.1.x.hitask.10.0.673/hi/build-hitask.js:7567
					require.cache.hi/timetracking/add.Hi_TimeTracking.addForm.show/public/app/lib/dojo.1.x.hitask.10.0.673/hi/build-hitask.js:7584
					require.cache.hi/widget/task/itemproperties._815.handleTimeTrackFormShow/public/app/lib/dojo.1.x.hitask.10.0.673/hi/build-global.js:14046
					lang.hitch
				 */
				// for some reason this.calendar_widget.focusNode becomes undefined.
				//this.calendar_widget.set("value", null);
			}
			
			if (this.node_spent) {
				this.node_spent.innerHTML = time2LocalizedStr(new Date());
			}
			
			if (inputs.user) {
				inputs.user.value = default_friend.id;
			}
			
			if (inputs.minutes) {
				inputs.minutes.value = hi.i18n.getLocalization('properties.time_tracking_hours');
			}
			
			this.node.style.display = 'none';
			Hi_PersonSelect.hide();
		},
		
		/**
		 * Open form
		 */
		show: function () {
			var item = hi.items.manager.opened();
			if (!item) return;
			
			var node = dojo.query('div.time-track-add-entry',dojo.byId('propertiesViewNode_'+item.widgetId))[0];//item.query('div.time-track-add-entry', true);
			if (node) {
				this.node = node;
				
				//Reset form data
				Hi_TimeTracking.addForm.reset();
				
				//References
				this.calendar_widget = item.propertiesView.timeTrackAddDateWidget;
				this.calendar_widget.reset();
				this.node_spent = dojo.query('a.spent', node)[0];
				this.node_date = this.calendar_widget.valueNode;
				
				//Show form	
				node.style.display = 'block';
				
				if (!node.initialized) {
					node.initialized = true;
					
					//On focus/blur remove/set label inside input
					var self = this;
					var inp = dojo.query('input[name="minutes"]', node)[0];
					
					inp.onfocus = function($e) {
						if (this.value == hi.i18n.getLocalization('properties.time_tracking_hours')) {
							this.value = '';
						}
					};
					inp.onblur = function($e) {
						if (this.value == '') {
							this.value = hi.i18n.getLocalization('properties.time_tracking_hours');
						}
					};
					
					inp.focus();
					
					var fn = function ($e) {
						var key = $e.which || $e.keyCode || false;
						switch(key) {
							case 13:
								dojo.stopEvent($e);
								self.save();
								return false;
							case 27:
								dojo.stopEvent($e);
								self.reset();
								return false;
						}
					};
					
					dojo.connect(inp, 'keypress', fn);
					dojo.connect(inp, 'keydown', fn);
					dojo.connect(inp, 'keyup', fn);
					
					this.node_spent.onclick = dojo.hitch(this, function (event) {
						dojo.stopEvent(event);
						
						this.calendar_widget.openDropDown();
						this.calendar_widget.dropDown.focus();
						
						//Prevent clicking on calendar from closing task
						dojo.connect(this.calendar_widget.dropDown.domNode, 'click', function (event) {
							dojo.stopEvent(event);
						});
						
						//Prevent clicking on month dropdown from closing task
						dojo.connect(this.calendar_widget.dropDown.monthWidget.dropDown.domNode, 'click', function (event) {
							dojo.stopEvent(event);
						});
					});
				}
			}
			
			return false;
		}
		
	};
	
	return Hi_TimeTracking.addForm;
	
});
