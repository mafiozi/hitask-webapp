//dojo.provide("hi.comments.date");

/**
 * Date formating functions
 */
define(function () {
	
	dojo.global.Hi_Comments.Date = {
		/*
		 * Localized interval names
		 * @type {Object}
		 */
		interval_names: {
			'i': 	hi.i18n.getLocalization('history.minute'),
			'I': 	hi.i18n.getLocalization('history.minutes'),
			'h': 	hi.i18n.getLocalization('history.hour'),
			'H': 	hi.i18n.getLocalization('history.hours'),
			'd': 	hi.i18n.getLocalization('history.day'),
			'D': 	hi.i18n.getLocalization('history.days'),
			'w': 	hi.i18n.getLocalization('history.week'),
			'W': 	hi.i18n.getLocalization('history.weeks'),
			'm': 	hi.i18n.getLocalization('history.month'),
			'M':	hi.i18n.getLocalization('history.months'),
			'y':	hi.i18n.getLocalization('history.year'),
			'Y':	hi.i18n.getLocalization('history.years')
		},
		
		/**
		 * Convert server datetime string into Date object
		 * 
		 * @param {String} str
		 * @return Date object
		 * @type {Date}
		 */
		stringToDate: function (datetime, utc) {
			if (!datetime) {
				return false;
			} else if (datetime.getTime) {
				return datetime;
			}
			
			//Remove milliseconds
			datetime = datetime.replace(/(:\d+)\..*$/, '$1');
			
			var dateObj = str2time(datetime, 'y-m-d h:i:s', (utc === false ? false : true));
			if (!dateObj) {
				dateObj = str2time(datetime, 'y-m-d h:i', (utc === false ? false : true));
				if (!dateObj) {
					dateObj = str2time(datetime, 'y-m-d', (utc === false ? false : true));
					if (!dateObj) {
						return false;
					}
				}
			}
			
			return dateObj;
		},
		
		/**
		 * Format Date object into string
		 * 
		 * @param {Date} date
		 * @return Formated date
		 * @type {String}
		 */
		formatDate: function (date) {
			var shortFormat = (!date.getHours() && !date.getMinutes() && !date.getSeconds());
			var str = time2str(date, Hi_Calendar.format);
			if (!shortFormat) {
				str += ' ' + HiTaskCalendar.formatTime(time2str(date, 'h:i'));
			}
			return str;
		},
		
		/**
		 * Format 
		 * @param {Object} date
		 */
		timeDifString: function (date, utc) {
			//Convert to date
			if (typeof date == 'string') {
				//date = this.stringToDate(date, utc);
				date = str2timeAPI(date);
			}
			
			//Time difference in minutes
			var dif = ((+new Date()) - (+date)) / 60000;
				dif = Math.max(1, ~~dif);
			
			var s = '';
			var intervals = Hi_Comments.Date.interval_names;
			var get = function (n) {
				return (dif > 1 ? dif + ' ' : '') + intervals[n];
			};
			
			if (dif <= 1) 	return get('i');
			if (dif <= 59)	return get('I');
			dif = ~~(dif / 60);	//in hours
			
			if (dif <= 1)	return get('h');
			if (dif <= 23)	return get('H');
			dif = ~~(dif / 24); //in days
			
			if (dif <= 1)	return get('d');
			if (dif <= 6)	return get('D');
			var tmp_dif = dif;  //save to reuse for months and years
			dif = ~~(dif / 7); //in weeks
			
			if (dif <= 1)	return get('w');
			if (dif <= 3)	return get('W');
			dif = ~~(tmp_dif / 30); //in months
			
			if (dif <= 1)	return get('m');
			if (dif <= 11)	return get('M');
			dif = ~~(tmp_dif / 365); //in years
			
			if (dif <= 1)	return get('y');
			return get('Y');
		}
	};
	
	return dojo.global.Hi_Comments.Date;

});