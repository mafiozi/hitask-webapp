//dojo.provide("hi.filter");
define(function () {
	
	return dojo.global.Hi_TaskFilter = {
		
		FILTER_ALL: 0,
		FILTER_PRIVATE: 1,
		FILTER_MY_SHARED: 2,
		FITLER_MY_UNASSIGNED: 3,
		
		filter: 0,
		
		/**
		 * Set filter
		 * @param {Object} val
		 */
		setFilter: function (val) {
			if (this.filter == val) return;
			
			var old = this.filter;
			this.filter = val;
			
			Hi_Preferences.set('taskfilter', val);
			Hi_Preferences.send();
			
			hi.items.manager.filterChange();
			
			//Change popup class
			var popup = dojo.byId('taskFilterPopup');
			dojo.removeClass(popup, 'selected-' + old);
			dojo.addClass(popup, 'selected-' + val);
			dojo.removeClass(popup, 'popup-opened');
			
			//Change toolbar class
			var tb = dojo.byId('taskFilter');
			dojo.removeClass(tb, 'selected-' + old);
			dojo.addClass(tb, 'selected-' + val);
			
			//Change message
			this.updateStatusMessage();
		},
		
		/**
		 * Returns current filter value
		 * 
		 * @return Filter value
		 * @type {Number}
		 */
		getFilter: function () {
			return this.filter;
		},
		
		/**
		 * Returns true if filter is active, otherwise false
		 * 
		 * @return True if filter is active
		 * @type {Boolean}
		 */
		hasFilter: function () {
			return this.filter != this.FILTER_ALL;
		},
		
		togglePopup: function (node) {
			/* Bug #2001 - block if there is a new task opened */
			if (hi.items.manager.openedId() && hi.items.manager.openedId() < 0) return false;
			var popup = dojo.byId('taskFilterPopup'),
				sortingNode = dojo.query('#toolbar td.sorting')[0],
				wrapper = dojo.query('div.wrapper', 'centerPaneContent')[0];

			// if (!sortingNode) return;
		
			if (dojo.hasClass(popup, 'popup-opened')) {
				dojo.removeClass(popup, 'popup-opened');
				dojo.disconnect(Hi_TaskFilter.togglePopup.handle);
			} else {
				dojo.addClass(popup, 'popup-opened');
				if (wrapper) wrapper.style.overflow = 'visible';
				
				//Position
				var popup_inner = popup.getElementsByTagName('UL')[0];
				if (node && dojo.hasClass(node.parentNode, 'taskfilter-status')) {
					popup_inner.style.left = node.offsetLeft + 'px';
					// popup_inner.style.top = sortingNode.offsetHeight + 5 + 'px';
				} else {
					// popup_inner.style.left = sortingNode.clientWidth + 95 +'px';
					popup_inner.style.left = node.offsetLeft + 'px';
					popup_inner.style.top = '3px';
				}
				
				//Bind to document click to hide popup
				Hi_TaskFilter.togglePopup.handle = dojo.connect(document, 'click', function () {
					Project.hideSortingAndFilterPopups();
				});
			}
		},
		
		init: function () {
			var filter = Hi_Preferences.get('taskfilter', 0);
			if (!filter || filter == 'null') filter = 0;
			this.filter = parseInt(filter);
			
			var filter = this.getFilter();
			var popup = dojo.byId('taskFilterPopup');
			var node = dojo.byId('taskFilter');
			var fs = dijit.byId('filterSelect');
			
			if (popup && node) {
				if (filter) {
					fs._noChange = true;
					fs.set('value', filter);
				}
				dojo.addClass(popup, 'selected-' + filter);

				dojo.removeClass(node, 'selected-0');
				dojo.addClass(node, 'selected-' + filter);

				this.updateStatusMessage();
			}
		},
		
		updateStatusMessage: function () {
			var container = dojo.byId('taskFilterStatus');
			
			if (this.hasFilter()) {
				container.style.display = 'block';
				var span = container.getElementsByTagName('SPAN')[0];
				var text = '';
				
				switch(this.getFilter()) {
					case 1: text = hi.i18n.getLocalization('taskfilter.status_my'); break;
					case 2: text = hi.i18n.getLocalization('taskfilter.status_my_created'); break;
					case 3: text = hi.i18n.getLocalization('taskfilter.status_my_unassigned'); break;
				}
				
				span.innerHTML = text;
				container.style.display = 'block';
			} else {
				container.style.display = 'none';
			}
		}
		
	};

});