//dojo.provide("hi.overdue");

define(['hi/widget/task/item_DOM'],function (item_DOM) {
	
	return dojo.global.Hi_Overdue = {
		TYPE_OVER: 1,
		TYPE_ACTIVE: 2,
		_interval: false,
		_refresh: 86400000,
		_timeouts: {},
		getNow: function() {
			var now = new Date();
			now.setSeconds(0);
			now.setMilliseconds(0);
			return now;
		},
		reload: function() {
			if (!Hi_Overdue._interval) {
				Hi_Overdue._interval = dojo.global.setInterval(Hi_Overdue.reload, Hi_Overdue._refresh);
			}
			var task_id;
			for (i in Hi_Overdue._timeouts) {
				dojo.global.clearTimeout(Hi_Overdue._timeouts[task_id]);
				Hi_Overdue._timeouts[task_id] = null;
			}
			
			var items = hi.items.manager.get();
			for (var i=0, ii=items.length; i<ii; i++) {
				Hi_Overdue.reloadItem(items[i].id);
			}
		},
		reloadItem: function(task_id) {
			if (task_id in Hi_Overdue._timeouts) {
				dojo.global.clearTimeout(Hi_Overdue._timeouts[task_id]);
				Hi_Overdue._timeouts[task_id] = null;
			}
			
			var active = Hi_Overdue.isActive(task_id);
			if (active) {
				Hi_Overdue.color(task_id, Hi_Overdue.TYPE_ACTIVE);
			} else {
				Hi_Overdue.color(task_id);
			}
			
			var timeout = Hi_Overdue.tillActive(task_id);
			if (timeout <= Hi_Overdue._refresh && timeout > 0) {
				Hi_Overdue._timeouts[task_id] = dojo.global.setTimeout(function() {Hi_Overdue.color(task_id, Hi_Overdue.TYPE_ACTIVE, true)}, timeout);
			} else {
				timeout = Hi_Overdue.overdueTime(task_id);
				if (timeout > Hi_Overdue._refresh) {
					
				} else if (timeout > 0) {
					Hi_Overdue._timeouts[task_id] = dojo.global.setTimeout(function() {Hi_Overdue.color(task_id, Hi_Overdue.TYPE_OVER)}, timeout);
				} else if (timeout < 0) {
					Hi_Overdue.color(task_id, Hi_Overdue.TYPE_OVER);
				}
			}
			
			//Update style for all items with given ID
			//since task can be in more than one list
			var items = hi.items.manager.allItems(task_id),
				key = null;
			
			for (key in items) {
				//items[key].checkOverdue();
				/* It could be the case that the widget DOM no longer exists */
				if (dojo.byId('itemNode_'+items[key].widgetId)) item_DOM.checkOverdue(items[key]);
			}
		},
		overdueTime: function(task_id) {
			if (parseInt(hi.items.manager.get(task_id, 'completed')) || parseInt(hi.items.manager.get(task_id, 'recurring')) || hi.items.manager.get(task_id, 'category') == 5) {
				return false;
			}
			var t = hi.items.date.getEventEndTime(task_id, '23:59');
			if (!t) {
				var t = hi.items.date.getEventStartTime(task_id, '23:59');
			}
			if (!t) {
				return false;
			}
			return t - new Date();
		},
		isOverdue: function(task_id) {
			return Hi_Overdue.overdueTime(task_id) < 0;
		},
		
		isActiveWidget: function(start_date, end_date, start_time, end_time, recurring) {
			var tStart = Project.getEventStartTimeWidget(start_date, start_time);
			var tEnd = Project.getEventEndTimeWidget(end_date, end_time);
			var tRecurring = recurring;
			
			if (!tEnd || !tStart) {
				return false;
			}
			var now = Hi_Overdue.getNow();
			if (!tRecurring) {
				if (tStart <= now && tEnd >= now) {
					return true;
				} else {
					return false;
				}
			}
			var tEndNew = HiTaskCalendar.nextEvent(tEnd, tRecurring, now);
			tEndNew = tEndNew[1];
			var diff = tEndNew - tEnd;
			var tStartNew = new Date(tStart.valueOf() + diff.valueOf());
			if (tStartNew <= now && tEndNew > now) {
				return true;
			} else {
				return false;
			}
		},
		
		isActive: function(task_id) {
			if (parseInt(hi.items.manager.get(task_id, 'completed'))) {
				return false;
			}
			var tStart = hi.items.date.getEventStartTime(task_id, '00:00');
			var tEnd = hi.items.date.getEventEndTime(task_id, '23:59');
			var tRecurring = parseInt(hi.items.manager.get(task_id, 'recurring'));
			if (!tEnd || !tStart) {
				return false;
			}
			var now = Hi_Overdue.getNow();
			if (!tRecurring) {
				if (tStart <= now && tEnd >= now) {
					return true;
				} else {
					return false;
				}
			}
			var tEndNew = HiTaskCalendar.nextEvent(tEnd, tRecurring, now);
			tEndNew = tEndNew[1];
			var diff = tEndNew - tEnd;
			var tStartNew = new Date(tStart.valueOf() + diff.valueOf());
			if (tStartNew <= now && tEndNew > now) {
				return true;
			} else {
				return false;
			}
		},
		tillActive: function(task_id) {
			var ret = this.nextEvent(task_id);
			return ret ? ret[0] : null;
		},
		nextEvent: function(task_id) {
			var time = hi.items.date.getEventStartTime(task_id);
			if (!time) return;
			var recurring;
			if (hi.items.manager.get(task_id, 'category') == 5) {
				recurring = 4;
			} else {
				recurring = hi.items.manager.get(task_id, 'recurring');
			}
			return HiTaskCalendar.nextEvent(time, recurring);
		},
		_uncolor: function(el) {
			if (!el) return;
			dojo.removeClass(el, 'overdue');
			dojo.removeClass(el, 'current');
		},
		_colorOverdue: function(el) {
			if (!el) return;
			dojo.removeClass(el, 'current');
			dojo.addClass(el, 'overdue');
		},
		_colorActive: function(el) {
			if (!el) return;
			dojo.removeClass(el, 'overdue');
			dojo.addClass(el, 'current');
		},
		color: function(task_id, type, timeouted) {
			var day = dojo.byId('day_task' + task_id);
				tt = dojo.byId('timetable_task' + task_id),
				func = null
			
			if (type == Hi_Overdue.TYPE_OVER) {
				func = Hi_Overdue._colorOverdue;
			} else if (type == Hi_Overdue.TYPE_ACTIVE) {
				func = Hi_Overdue._colorActive;
			} else {
				func = Hi_Overdue._uncolor;
			}
			
			func(day);
			func(tt);
			
			if (type == Hi_Overdue.TYPE_ACTIVE && timeouted) {
				dojo.global.setTimeout(function() {Hi_Overdue.reloadItem(task_id)}, 1000);
			}
		}
	};

});