define([
	"dojo/_base/declare",
	'dojo/sniff',
	'dojo/on',
	'dojo/aspect',
	'dojo/_base/lang',
	'dojo/_base/event',
	"dojo/_base/fx",
	'dojo/string',
	'dojo/keys',
	'dojo/dom-geometry',

	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dijit/_HasDropDown' ,
	'dojo/text!./template/hiSharingDropDownItemBaseDropDown.html'
], function(declare, sniff, on, aspect, lang, event, fx, string, keys, domGeo,
			_WidgetBase, _TemplatedMixin, _HasDropDown, template){

	var ItemBaseDropDown = declare([_WidgetBase, _TemplatedMixin], {

		baseClass: 'hiSharingItemBaseActionList dijitMenu',

		templateString: template,

		value: 0,
		_setValueAttr: function(value){
			this._set('value', value);
			this.emit('valuechange', {value: value});
		},

		// onChange: function(value){},

		postCreate: function(){
			this.inherited(arguments);

			var permissions = dojo.global.Hi_Permissions.getPermissions(),
				len = permissions.length, i, pm, li, nls;

			for(i = 0; i < len; i++){
				pm = permissions[i];
				nls = hi.i18n.getLocalization(pm[0]);
				li = document.createElement('li');
				li.innerHTML = "<span class='icon-checkmark2 checkmark'></span><span class='action'>" + nls + "</span><span class='icon icon-hitask'>" + pm[2] + "</span>";
				dojo.addClass(li, 'hiSharingDropDownItemBaseDropDownList');
				li.setAttribute('data-permission', pm[1]);
				this.listContainer.appendChild(li);
			}

			this.own(
				on(this.domNode, 'click', lang.hitch(this, this._onListClick))
			);
		},

		_onListClick: function(e){
			event.stop(e);
			var li = dojo.query(e.target).closest('.hiSharingDropDownItemBaseDropDownList')[0], pm,
				item = this.taskId && hi.items.manager.get(this.taskId);

			if(item && item.user_id === this._uuid){
				return;
			}
			
			if(li){
				dojo.query('li', li.parentNode).removeClass('selected');
				dojo.addClass(li, 'selected');
				pm = parseInt(li.getAttribute('data-permission'), 10);
				this.set('value', pm);
			}
		}
	});

	return declare('hi.widget.SharingDropDownItemBase', [_WidgetBase, _TemplatedMixin, _HasDropDown], {

		image: '',

		name: '',

		data: {},

		permission: 0,
		_setPermissionAttr: function(value){
			this._set('permission', value);

			var permissions = dojo.global.Hi_Permissions.getPermissions(),
				len = permissions.length, i, temp;

			for(i = 0; i < len; i++){
				temp = permissions[i];
				if(temp[1] === value){
					this.iconNode.innerHTML = temp[2];
					this.emit('permissionchange', {_uuid: this._uuid, value: value});
				}
			}
		},

		_uuid: '',

		_noanim: false,

		postCreate: function(){
			this.inherited(arguments);
			this.dropDown = new ItemBaseDropDown({taskId: this.taskId, _uuid: this._uuid});
			this.dropDown.startup();
			this.own(
				on(this.dropDown.removeButton, 'click', lang.hitch(this, this._remove)),
				on(this.dropDown, 'valuechange', lang.hitch(this, this.onChange)),
				on(window, 'keydown', lang.hitch(this, this._keypress)),
				// aspect.after(this.dropDown, 'onChange', lang.hitch(this, this.onChange), true),
				aspect.after(this, 'closeDropDown', lang.hitch(this, this._afterCloseDropDown))
			);
		},

		onRemove: function(){},

		onChange: function(e){
			this.set('permission', e.value);
			this.closeDropDown();
		},

		_remove: function(){
			var t = this;
			var item = t.taskId && hi.items.manager.get(t.taskId);
			if(item && item.user_id === t._uuid){
				return;
			}
			t._noanim = true;
			t.closeDropDown();

			fx.fadeOut({
				node: this.domNode,
				onEnd: function(){
					t._noanim = false;
					// t.destroy();
					t.emit('remove', {_uuid: t._uuid});
					t.onRemove(t._uuid);
				}
			}).play();
		},

		_keypress: function(e){
			var code = e.charCode || e.keyCode;

			if(code === keys.BACKSPACE || code === keys.DELETE){
				if(this._opened === true){
					this._remove();
					event.stop(e);
				}
			}
		},

		_afterCloseDropDown: function() {
			var itemNode = this.domNode,
				t = this;

			if(t._noanim === true || t.inanim === true) return;

			dojo.removeClass(t.domNode, 'overlap');
			t.inanim = true;
			fx.animateProperty({
				node: itemNode,
				properties: {
					width: 110
				},
				onEnd: function() {
					if (!itemNode || !itemNode.parentNode || !itemNode._parentNode) return;
					
					itemNode.parentNode.removeChild(itemNode);
					itemNode._parentNode.appendChild(itemNode);
					itemNode.style.top = itemNode._top;
					itemNode.style.left = itemNode._left;
					itemNode.style.zIndex = itemNode._z;
					t._expanded = false;
					t.inanim = false;
				}
			}).play();
		},

		_onDropDownMouseDown: function(e){
			if(this.inanim === true || this._expanded === true){return;}
			var target = e.target,
				itemNode = dojo.query(target).closest('.hiSharingDropDownItem')[0],
				t = this,
				offset = 0,
				arg = arguments,
				geo = domGeo.position(itemNode);

			dojo.addClass(itemNode.parentNode, 'inanim');

			itemNode._top = itemNode.style.top;
			itemNode._left = itemNode.style.left;
			itemNode._z = itemNode.style.zIndex;
			itemNode._parentNode = itemNode.parentNode;
			itemNode.parentNode.removeChild(itemNode);

			itemNode.style.position = 'absolute';

			offset = sniff('chrome') ? document.body.scrollTop : document.documentElement.scrollTop;
			itemNode.style.top = Math.round(geo.y) - 1 + offset + 'px';
			itemNode.style.left = Math.round(geo.x) - 1 + document.body.scrollLeft + 'px';
			itemNode.style.zIndex = 999999;

			document.body.appendChild(itemNode);
			t.inanim = true;
			fx.animateProperty({
				node: itemNode,
				properties: {
					width: 180
				},
				onEnd: function(){
					dojo.addClass(itemNode, 'overlap');
					t.inherited(arg);
					t.domNode.focus();
					t.inanim = false;
					t._expanded = true;

					var lis = dojo.query('li', t.dropDown.domNode);
					lis.removeClass('selected');

					for(var i = 0; i < lis.length; i++){
						if(lis[i].getAttribute('data-permission') == t.permission){
							dojo.addClass(lis[i], 'selected');
						}
					}
					if(t.everyone === true){
						dojo.addClass(t.dropDown.domNode, 'everyone');
					}
				}
			}).play();

			return false;
		}
	});
});