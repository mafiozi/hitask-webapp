define([
	"hi/items/view-my",
	"hi/items/view-today",
	"hi/items/view-activityFeed",
	"hi/items/view-date",
	"hi/items/view-overdue",
	"hi/items/view-color",
	"hi/items/view-team",
	"hi/items/view-projects",
	"hi/items/view-all",
	"hi/items/view-bigCalendar",
	'hi/items/view-main'
], function (hi_items, my_view) {
	return hi_items;
});
