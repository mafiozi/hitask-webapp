//dojo.provide("hi.upload.tooltip");

/**
 * Header file upload tooltip
 */
define(['hi/widget/task/item_DOM'],function (item_DOM) {
	
	dojo.global.FileUploadTooltip = {
		/**
		 * List node
		 * @type {HTMLElement}
		 */
		nodeList: null,
		
		/**
		 * Header button which opens tooltip
		 * @type {HTMLElement}
		 */
		nodeButton: null,
		
		/**
		 * Node containing space used
		 * @type {HTMLElement}
		 */
		nodeStatusUsed: null,
		
		/**
		 * Node container space quota
		 * @type {HTMLElement}
		 */
		nodeStatusTotal: null,
		
		/**
		 * Tooltip initialzation state
		 * @type {Boolean}
		 */
		initialized: false,
		
		/**
		 * File list
		 * @type {Object}
		 */
		files: {},
		
		/**
		 * Uploader object list
		 * @type {Array}
		 */
		uploaders: [],
		
		/**
		 * All files were uploaded
		 * @param {Boolean} uploader
		 */
		completed: false,
		
		/**
		 * Last known total progress
		 * @param {Object} uploader
		 */
		totalProgress: false,
		
		/**
		 * Tooltip is visible
		 * @type {Boolean}
		 */
		visible: false,
		
		/**
		 * Popup should be shown when ready
		 * @type {Boolean}
		 */
		showWhenReady: false,
		
		/**
		 * Add uploader
		 * 
		 * @param {Object} uploader Uploader instance
		 */
		addUploader: function (uploader) {
			this.uploaders.push(uploader);
			FileUpload.remove(0);
			
			var files = uploader.files,
				newFiles = [],
				oldFiles = [],
				hasFiles = false, i, ii;
			
			for(i = 0, ii = files.length; i < ii; i++) {
				if (files[i].isready) {
					//Move file item to tooltip
					this.addFile(files[i]);
					hasFiles = true;
					newFiles.push(files[i]);
				} else {
					oldFiles.push(files[i]);
				}
			}
			
			if (hasFiles) {
				for(i = 0, ii = oldFiles.length; i < ii; i++) {
					oldFiles[i].destroy();
				}
				uploader.files = newFiles;
				uploader.updateReadyView(0);
				uploader.inTooltip = true;
				
				//Show 'Uploading files' tooltip on first opportunity 
				this.showWhenReady = true;
			}
		},
		
		/**
		 * Move file to upload tooltip
		 * @param {Object} file
		 */
		addFile: function (file) {
			this.init();
			
			file.parent.nodeList = this.nodeList;
			file.moveToTooltip(this.nodeList);
			
			this.completed = false;
			this.files[file.id] = file;
			
			if (file.progress) {
				dojo.connect(file, 'progress', this, 'updateProgress');
			}
			
			//Start file upload
			this.uploadNext();
		},
		
		/**
		 * Upload next pending file
		 */
		uploadNext: function () {
			var files = this.files,
				next = null;
				
			for(var id in files) {
				if (files[id].uploading) {
					//If some file is already uploading, then wait till it ends
					return;
				} else if (files[id].isready && !files[id].canceled && !next) {
					next = files[id];
				}
			}
			
			if (next) next.upload();
		},
		
		/**
		 * Show tooltip
		 */
		show: function () {
			this.init();
			
			if (!this.visible) {
				dojo.removeClass(this.nodeButton, 'hidden');
				this.nodeButton.onclick();
				this.visible = true;
				this.showWhenReady = false;
				
				var percent_used = FileUpload.serverQuota.percent_used || 0;
				var quota_formatted = FileUpload.serverQuota.quota_formatted || 0;
				
				FileUploadTooltip.setQuotaLabel(percent_used + '%', quota_formatted);
			}
		},
		
		/**
		 * Hide tooltip
		 */
		hide: function () {
			if (this.initialized) {
				if (!Tooltip.close('upload')) {
					//If tooltip is already closed, have to calog('call after hide');
					this.afterHide();
				}
			}
		},
		
		/**
		 * Called after tooltip is hidden:
		 * Removes files, hides button, updates button label
		 */
		afterHide: function () {
			this.visible = false;
			if (this.completed) {
				this.removeAllFiles();
				dojo.addClass(this.nodeButton, 'hidden');
				this.setProgressLabel(0);
				/* Bug #2024 Must reset uploader now to show on UI */
				if (hi.items.manager.openedId()<0)
				{
					if (hi.items.manager.opened().collapse)
					{
						FileUpload.reset();
						FileUpload.get(hi.items.manager.openedId()).resetAll();
						 hi.items.manager.opened().collapse();						
					}
					else 
					{
						var openedId = hi.items.manager.openedId();
						hi.items.manager.remove(openedId);
					}
				} else
				{
					//Force collapse
					hi.items.manager._items[-1].newitem.collapse();
				}
			}
		},
		
		/**
		 * Remove all files and uploders 
		 */
		removeAllFiles: function () {
			var uploaders = this.uploaders;

			/* Bug #2029 Rescue nodeList before it is clobbered by below destroy() method? */
			dojo.place(this.nodeList,dojo.query('div.usage',dojo.byId('tooltip_upload'))[0],'before');
			var nodeList = this.nodeList;
			while(nodeList.firstChild) {
				removeNode(nodeList.firstChild);
			}
			
			for(var i=0,ii=uploaders.length; i<ii; i++) {
				uploaders[i].destroy();
			}
			dojo.place(this.nodeList,dojo.query('div.usage',dojo.byId('tooltip_upload'))[0],'before');			
			this.completed = true;
			this.files = {};
			this.uploaders = [];
		},
		
		/**
		 * Update button progress
		 */
		updateProgress: function () {
			var totalProgress = 0,
				totalCount = 0,
				files = this.files,
				hasFiles = false,
				hasError = false,
				allCompleted = true;
			
			for(var i in files) {
				totalCount++;
				if (files[i].uploadError) {
					hasError = true;
					hasFiles = true;
				}
				
				if (files[i].uploading) {
					if ('uploadProgress' in files[i]) {
						totalProgress += files[i].uploadProgress;
						allCompleted = false;
					}
					hasFiles = true;
				} else if (files[i].completed) {
					totalProgress += 100;
					hasFiles = true;
				} else if (files[i].canceled) {
					totalProgress += 100;
				}
			}
			
			totalProgress = totalProgress / totalCount;
			
			if (totalProgress == 100) {
				if (allCompleted) {
					this.completed = true;
					
					if (!hasFiles) {
						//If all files were canceled hide popup
						FileUploadTooltip.hide();
					} else if (!hasError || !Tooltip.opened('upload')) {
						//If no errors or tooltip not opened: wait 3 seconds and then hide
						setTimeout(function () {
							FileUploadTooltip.hide();
						}, 3000);
					}
				}
				
				this.setProgressLabel(true);
			} else {
				this.setProgressLabel(totalProgress);
			}
		},
		
		/**
		 * Update buttol label with total progress
		 * 
		 * @param {Number} totalProgress
		 */
		setProgressLabel: function (totalProgress) {
			var nodeLabel = this.nodeButton.getElementsByTagName('span')[0],
				title = '';
			
			//If progress didn't changed then skip to avoid redraw
			totalProgress = (totalProgress !== true ? Math.round(totalProgress) : totalProgress);
			if (this.totalProgress === totalProgress) return;
			this.totalProgress = totalProgress;
			
			if (totalProgress === true) {
				title = this.nodeButton.getAttribute('titleDone');
			} else {
				title = this.nodeButton.getAttribute('title') + ' ' + totalProgress + '%';
			}
			
			nodeLabel.innerHTML = title;
		},
		
		/**
		 * Update quota labels
		 * 
		 * 
		 */
		setQuotaLabel: function (used, quota) {
			this.nodeStatusUsed.innerHTML = used;
			this.nodeStatusTotal.innerHTML = quota;
		},
		
		/**
		 * Initialize upload tooltip
		 */
		init: function () {
			if (this.initialized) return;
			this.initialized = true;
			
			this.nodeButton = dojo.byId('uploadLink');
			this.nodeList = dojo.byId('upload_tooltip_list');
			this.nodeStatusUsed = dojo.byId('upload_space_used');
			this.nodeStatusTotal = dojo.byId('upload_space_total');
			
			dojo.subscribe('TooltipCloseAfter', function (name) {
				if (name == 'upload') {
					FileUploadTooltip.afterHide();
				}
			});
		}
	};

	return dojo.global.FileUploadTooltip;
	
});