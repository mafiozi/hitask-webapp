package beans;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import play.Play;
import play.i18n.Lang;
import play.i18n.Messages;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Dummy java bean. Update and replace it as soon as Login and User Session start working.
 * @author Vadim Manikhin
 */
public class UserPreferences {
	public static final int TIME_FORMAT_24 = 24;
	public static final int TIME_FORMAT_12 = 12;
	public static final String TIME_FORMAT_PROPERTY = "time_format";
	
	private final GregorianCalendar gregorianCalendar = new GregorianCalendar();
	
	@SerializedName("show_news")
	private boolean showNews = true;
	@SerializedName("reminder_enabled")
	private boolean reminderEnabled = true;
	@SerializedName("reminder_time")
	private int reminderTime = 5;
	@SerializedName("reminder_time_type")
	private String reminderTimeType = "m";
	@SerializedName("reminder_sound")
	private String reminderSound = "1";
	@SerializedName("chat_sound")
	private String chatSound = "url";
	@SerializedName("grouping")
	private int grouping = 0;
	@SerializedName("taskfilter")
	private int filter = 0;
	@SerializedName("sidebar_calendar")
	private int sidebarCalendar = 1;
	@SerializedName("sidebar_messenger")
	private int sidebarMessenger = 1;
	@SerializedName("closed_news")
	private int closedNews = 0;
	@SerializedName("feeds")
	private int feeds = 1;
	@SerializedName("date_format")
	private String dateFormat = "d-m-y";
	@SerializedName("first_day_of_week")
	private int firstDayOfWeek = gregorianCalendar.getFirstDayOfWeek();
	@SerializedName(TIME_FORMAT_PROPERTY)
	private int timeFormat = TIME_FORMAT_24;
	@SerializedName("time_max_length")
	private int timeMaxLength = TIME_FORMAT_24;
	@SerializedName("reminder_time_types")
	private List<ReminderTimeType> reminderTimeTypes = new ArrayList<ReminderTimeType>();
	private String language = "en";
	@SerializedName("time_zone")
	private String timeZone;
	private HashMap<String, String> rawPreferences;

	public UserPreferences() {
		timeMaxLength = timeFormat == TIME_FORMAT_24 ? 5 : 8;
		initReminderTimeTypes();
	}
	
	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	
	public boolean isShowNews() {
		return showNews;
	}
	
	public void setShowNews(boolean showNews) {
		this.showNews = showNews;
	}
	
	public boolean isReminderEnabled() {
		return reminderEnabled;
	}
	
	public void setReminderEnabled(boolean reminderEnabled) {
		this.reminderEnabled = reminderEnabled;
	}
	
	public int getReminderTime() {
		return reminderTime;
	}
	
	public void setReminderTime(int reminderTime) {
		this.reminderTime = reminderTime;
	}
	
	public String getReminderTimeType() {
		return reminderTimeType;
	}
	
	public void setReminderTimeType(String reminderTimeType) {
		this.reminderTimeType = reminderTimeType;
	}
	
	public String getReminderSound() {
		return reminderSound;
	}
	
	public void setReminderSound(String reminderSound) {
		this.reminderSound = reminderSound;
	}
	
	public String getChatSound() {
		return chatSound;
	}
	
	public void setChatSound(String chatSound) {
		this.chatSound = chatSound;
	}
	
	public int getGrouping() {
		return grouping;
	}
	
	public void setGrouping(int grouping) {
		this.grouping = grouping;
	}
	
	public int getFilter() {
		return filter;
	}
	
	public void setFilter(int filter) {
		this.filter = filter;
	}
	
	public int getSidebarCalendar() {
		return sidebarCalendar;
	}
	
	public void setSidebarCalendar(int sidebarCalendar) {
		this.sidebarCalendar = sidebarCalendar;
	}
	
	public int getSidebarMessenger() {
		return sidebarMessenger;
	}
	
	public void setSidebarMessenger(int sidebarMessenger) {
		this.sidebarMessenger = sidebarMessenger;
	}
	
	public int getClosedNews() {
		return closedNews;
	}
	
	public void setClosedNews(int closedNews) {
		this.closedNews = closedNews;
	}
	
	public int getFeeds() {
		return feeds;
	}
	
	public void setFeeds(int feeds) {
		this.feeds = feeds;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public int getFirstDayOfWeek() {
		return firstDayOfWeek;
	}

	public void setFirstDayOfWeek(int firstDayOfWeek) {
		this.firstDayOfWeek = firstDayOfWeek;
	}

	public int getTimeFormat() {
		return timeFormat;
	}

	public void setTimeFormat(int timeFormat) {
		this.timeFormat = timeFormat;
	}

	public int getTimeMaxLength() {
		return timeMaxLength;
	}

	public void setTimeMaxLength(int timeMaxLength) {
		this.timeMaxLength = timeMaxLength;
	}
	
	private void initReminderTimeTypes() {
		forceSetPlayLanguage();
		reminderTimeTypes.add(new ReminderTimeType("m", StringUtils.capitalize(Messages.get("js.history.minutes"))));
		reminderTimeTypes.add(new ReminderTimeType("h", StringUtils.capitalize(Messages.get("js.history.hours"))));
		reminderTimeTypes.add(new ReminderTimeType("d", StringUtils.capitalize(Messages.get("js.history.days"))));
	}
	
	/**
	 * Changes current Play language to user preferred language.
	 * Related to #3214.
	 */
	public void forceSetPlayLanguage() {
		if (StringUtils.isNotBlank(getLanguage()) && Play.langs.contains(getLanguage())) {
			Lang.change(getLanguage());
		}
	}
	
	public List<ReminderTimeType> getReminderTimeTypes() {
		return reminderTimeTypes;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public HashMap<String, String> getRawPreferences() {
		return rawPreferences;
	}

	public void setRawPreferences(HashMap<String, String> rawPreferences) {
		this.rawPreferences = rawPreferences;
	}
	
	public String toJson() {
		String json = new Gson().toJson(this);
		
		if (rawPreferences != null) {
			HashMap<String, String> map = new Gson().fromJson(json, HashMap.class);
			rawPreferences.putAll(map);
			return new Gson().toJson(rawPreferences);
		}
		
		return json;
	}
}