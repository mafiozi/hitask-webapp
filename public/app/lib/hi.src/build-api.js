define([
	"dojo/on",
	"dojo/cldr/nls/gregorian",
	"dojo/cldr/supplemental",
	
	"hi/api",
	"hi/calendar",
	"hi/taskcalendar",
	"hi/i18n"
], function () {
	return {};
});