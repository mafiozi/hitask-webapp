/**
 * Instantiation of task lists (and the groups that contain them) on My View tab:
 * hi.items.TaskListStarred
 * hi.items.TaskListUngrouped
 * hi.items.TaskListCompleted
 */
define([
	'hi/store',
	'hi/items/base',
	'dojo/aspect',
	'hi/items/date'
], function (hi_store, hi_items,dojo_aspect) {
	

	//After hi.store.init create MyView groups
	dojo.global.viewMyTab = function(q) {
	//dojo.connect(hi_store, "afterInit", hi_store, function () {
		// Create task groups (Starred, Completed and default list) and set filters
		//console.log('afterinit',q.length);
		
		var readStore = hi_store.ProjectCache,
			writeStore = hi_store.Project,
			query = q,//readStore.query(),
			
			groupSelf = 'my',
			groupCurrent = Hi_Preferences.get('currentTab', 'my', 'string'),
			getDayDiff = dojo.global.viewDate.getDayDiff;

		
		//My view
		if (groupCurrent == groupSelf){
			var myV = dojo.connect(hi_store, 'afterLoad', function() {
				// Add to container
				if (groupSelf == groupCurrent){
					finalizeInitialization([hi_items.GroupStarred, hi_items.GroupCompleted],[hi_items.TaskListUngrouped]);
				}
				dojo.disconnect(myV);
			});
		}

		
		hi_items.GroupStarred = new hi.widget.Group({
			"projectId": 1,
			"expandable": false, // not expandable (always expanded)
			
			"title": hi.i18n.getLocalization("center.starred"),
			"moveTitle": hi.i18n.getLocalization("center.starred"),
			"groupClassName": "color3 starred",
			"disabled":groupSelf != groupCurrent,
			"preferencesGroup": groupSelf
		});
		
		hi_items.TaskListStarred = new hi.widget.TaskList({
			"readStore": readStore,
			"writeStore": writeStore,
			"query": q,//groupSelf == groupCurrent?query:null,
			"disabled":groupSelf != groupCurrent,
			showing:groupSelf == groupCurrent?true:false,
			mainList:true,
			"filterType": /*#2403 "any"*/'top', // if item or any of its parents is starred it should be in the list
			
			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				// strict:
				//		If true then don't check ancestors
				/* 3229 */
				/*Change #4771*/
				if(item.category === hi.widget.TaskItem.CATEGORY_EVENT && getDayDiff(item) < 0){
					return false;
				}
				if (item.recurring){
					if (!this.calculateInstanceDate(item)) return false;
				}
				
				if (item.starred) return true;
				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			}
		});
		
		try{
		hi_items.TaskListUngrouped = new hi.widget.TaskList({
			'title': 'ungrouped',
			"readStore": readStore,
			"writeStore": writeStore,
			"query": q,//groupSelf == groupCurrent?query:null,
			showing:groupSelf == groupCurrent?true:false,
			"disabled":groupSelf != groupCurrent,
			"preferencesGroup": groupSelf,
			
			"completedSubList": false, // All completed items are in different group, no need for sublist
			
			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				// strict:
				//		If true then don't check ancestors	

				/* #3229 - check there are any instances left */
				var instance_date;
				if (item.recurring) {
					instance_date = this.calculateInstanceDate(item);
					if (!instance_date) return false;
				}
				/*Change #4771*/
				if(item.category === hi.widget.TaskItem.CATEGORY_EVENT && getDayDiff(item) < 0){
					return false;
				}
				// Item and all parents must pass filterStarred
				if (!strict && !this.filterMatches(item, /*#2403 'all'*/"top", "filterStarred")) return false;
				// Item or any parent must pass filterCompleted
				if (!strict && this.filterMatches(/* #1203 proposed tmpItem*/item, "any", "filterCompleted")) return true;

				if (item.recurring) {
					return !item.starred && !hi.items.manager.getIsCompleted(item,instance_date);
				}
				else return !item.starred && !hi.items.manager.getIsCompleted(item);
			},
			"filterStarred": function (item, strict) {
				// summary:
				//		Filter starred items, if all parents are not starred, then test passed
				// item:
				//		Item data
				// strict:
				//		If true then don't check ancestors
				
				//If item is starred, then failed
				if (item.starred) return false;
				
				//If can check parent, do so
				if (!strict) {
					if (item.parent && item.category) {
						return this.filterMatches(item.parent, /*#2403 "all"*/'top', "filterStarred");
					}
				}
				return true;
			},
			"filterCompleted": function (item, strict) {
				// summary:
				//		Filter completed items, if any parent is not completed, then test passed
				// item:
				//		Item data
				// strict:
				//		If true then don't check ancestors
				
				//return !item.completed && (strict || !item.parent || !item.category || this.filterMatches(item.parent, "any", "filterCompleted"));
				
				//If item is starred, then failed

				/* #2493 - filter by instances */
				if (item.recurring)
				{
					if (!this.calculateInstanceDate(item)) return false;
					if (hi.items.manager.getIsCompleted(item,this.calculateInstanceDate(item))) return false;
					else return true;
					//if (!recurringItemMatchesDate(item,HiTaskCalendar.getStrFromDate(HiTaskCalendar.today))) return false;
				}
				if (hi.items.manager.getIsCompleted(item/*#1203 proposed,time2str(new Date(),'y-m-d')*/)) return false;
				
				//If can check parent, do so
				if (!strict) {
					if (item.parent && item.category) {
						return this.filterMatches(item.parent, "any", "filterCompleted");
					}
				}
				return true;
			}
		});
		} catch(e) {console.log('ERROR',e);}
		
		hi_items.GroupCompleted = new hi.widget.Group({
			"projectId": "completed",
			"emptyVisible": false, // don't show when empty
			"expanded": false,     // by default not expanded
			'groupSubmenusVisible': true,
			'animationEnabled': true,
			"title": hi.i18n.getLocalization("completed.title"),
			"moveTitle": "",
			"groupClassName": "day project-completed group-completed",
			
			"disabled": groupSelf != groupCurrent
		});
		
		hi_items.TaskListCompleted = new hi.widget.TaskList({
			"readStore": readStore,
			"writeStore": writeStore,
			"query": q,//groupSelf == groupCurrent?query:null,
			showing: groupSelf == groupCurrent? true : false,
			initialyCollapsed:true,
			"localeItemCollapsed": hi.i18n.getLocalization("center.completed_item_count").replace(/%s\s+/,' '),
			"localeItemsCollapsed": hi.i18n.getLocalization("center.completed_items_count").replace(/%s\s+/,' '),
			"disabled":groupSelf != groupCurrent,
			"completedSubList": false, // All items already are completed, no need for sublist
			/* Bug #2040 - change filter type from 'all' to 'top' - if top ancestor is completed, so are all of its desendents */
			"filterType": "top", // If item and all parents are completed and not starred then show in list
			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				/*Change #4771*/
				if(item.category === hi.widget.TaskItem.CATEGORY_EVENT && getDayDiff(item) < 0){
					return false;
				}
				if (item.recurring)
				{
					var instance_date = this.calculateInstanceDate(item);
					if (!instance_date) return false;
					if (!item.starred && hi.items.manager.getIsCompleted(item,instance_date)) return true;
					else return false;
				}
				if (!item.starred && hi.items.manager.getIsCompleted(item)) return true;
				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			},
			
			/*
			 * This tell GroupCompleted to set the collapsed attribute to false, so that completed items are only added
			 * to the DOM on a user-on-demand basis
			 */
			afterExpand: function() {
				this.set('collapsed',false);
			}
		});
		
		// Add to container
		var container = dojo.byId(Project.getContainer('tasksList'));
		
		hi_items.GroupStarred.placeAt(container);
		hi_items.TaskListStarred.placeAt(hi_items.GroupStarred);
		
		hi_items.TaskListUngrouped.placeAt(container);
		
		hi_items.GroupCompleted.placeAt(container);
		hi_items.TaskListCompleted.placeAt(hi_items.GroupCompleted);
		
		dojo.connect(hi.items.manager, "groupingChange", hi.items.manager, function (group, groupPrev) {
			// summary:
			//		On group change enable/disable lists
			
			if (group == groupSelf && groupPrev != groupSelf) {
				refreshTab([hi_items.GroupStarred ,hi_items.GroupCompleted], [hi_items.TaskListUngrouped], true);
			} else if (group != groupSelf && groupPrev == groupSelf) {
				hi_items.GroupStarred.set("disabled", true);
				hi_items.TaskListUngrouped.set("disabled", true);
				hi_items.GroupCompleted.set("disabled", true);
			}
			
			groupCurrent = group;
		});
		
		dojo.connect(Project, "changeSorting", Project, function () {
			// summary:
			//		On sorting change empty lists
			if (groupCurrent == groupSelf) {
				/* #2509
				hi_items.TaskListStarred.reset({"fill": false});
				hi_items.TaskListUngrouped.reset({"fill": false});
				hi_items.TaskListCompleted.reset({"fill": false});
				*/
				hi_items.GroupStarred.set("disabled", true);
				hi_items.TaskListUngrouped.set("disabled", true);
				hi_items.GroupCompleted.set("disabled", true);
			}
		});
		
		dojo.connect(hi.items.manager, "filterChange", hi.items.manager, function () {
			// summary:
			//		On sorting change empty lists
			/* #2509
			if (groupCurrent == groupSelf) {
				hi_items.TaskListStarred.reset({"fill": false});
				hi_items.TaskListUngrouped.reset({"fill": false});
				hi_items.TaskListCompleted.reset({"fill": false});
			}
			*/
			if (groupCurrent == groupSelf) {
				hi_items.TaskListStarred.set('disabled',true);
				hi_items.TaskListUngrouped.set('disabled',true);
				hi_items.TaskListCompleted.set('disabled',true);
			}
			
		});
		
		/* This is invoked after refresh data store on filter selection change 
		 * This should just initiate the query observers on the lists and enable the lists
		 */
		dojo.connect(hi.items.manager, "redraw", hi.items.manager, function () {
			// summary:
			//		Some major change happened, redraw lists
			if (groupCurrent == groupSelf)
			{
				/* #2331 - rereshTab on searchChange */
				//if (hi.items.TaskListStarred.get('disabled')) initiateTab([hi_items.GroupStarred,hi_items.GroupCompleted],[hi_items.TaskListUngrouped]);
				/*else*/ refreshTab([hi_items.GroupStarred,hi_items.GroupCompleted],[hi_items.TaskListUngrouped]);
			}
		});
		
		hi.items.manager.getTaskLists[groupSelf] = function () {
			// summary:
			//		Return all task lists for this group
		
			return [
				hi_items.TaskListUngrouped,
				hi_items.TaskListStarred,
				hi_items.TaskListCompleted
			];
		};
	};
});