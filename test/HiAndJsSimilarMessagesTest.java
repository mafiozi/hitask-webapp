import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import play.Logger;
import play.i18n.Messages;


public class HiAndJsSimilarMessagesTest extends GroovyViewsBaseTest {
	@Test
	public void searchForSimilarMessages() {
		final String engLocale = "en";
		Map<String, Set<String>> duplicates = new HashMap<String, Set<String>>();
		
		for (Entry<String, Properties> messages : Messages.locales.entrySet()) {
			if (messages.getKey().equals(engLocale)) {
				Properties props = messages.getValue();
				
				for (Object key : props.keySet()) {
					if (key instanceof String) {
						String messageKey = (String)key;
						
						String message = props.getProperty(messageKey);
						
						if (!duplicates.containsKey(message)) {
							duplicates.put(message, new HashSet<String>());
						}
						
						String keyPrefix = extractMessagePrefix(messageKey);
						
						if (StringUtils.isNotBlank(keyPrefix)) {
							duplicates.get(message).add(keyPrefix);
						}
					}
				}
			}
		}
		
		for (Iterator<Entry<String, Set<String>>> it = duplicates.entrySet().iterator(); it.hasNext(); ) {
			Entry<String, Set<String>> next = it.next();
			
			if (next.getValue().size() < 2) {
				it.remove();
			}
		}
		
		Logger.info("List of messages defined in more than one lang properties (total %d messages):", duplicates.entrySet().size());
		for (Entry<String, Set<String>> entry : duplicates.entrySet()) {
			Logger.info("Message: [%s], defined in %s", entry.getKey(), entry.getValue().toString());
		}
	}
	
	private String extractMessagePrefix(String messageKey) {
		String[] split = StringUtils.split(messageKey, '.');
		
		return split.length > 1 ? split[0] : null;
	}
}
