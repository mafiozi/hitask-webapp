# README #

This README would normally document whatever steps are necessary to get your application up and running.

Please let us know if you have any suggestions or comments about our process and tools(software) used.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Servers

http://hitask.com is a production instance. 
iOS and Android apps work with this server. (This means that to test mobile apps you need to use real accounts registered on hitask.com.)

http://qatask.com is a test (staging) server for next release 
projects hitask-play, hitask-webapp,hitask-website deployed here
Any commits made to develop.[branch] branches are automatically deployed to this server within 2-5 minutes.

http://testask.com is a test (staging) server for previous release
projects hitask-play, hitask-webapp,hitask-website deployed here
Any commits made to develop branches are automatically deployed to this server within 2-5 minutes.
For testing, to register a paid account on testask.com use Stripe Sandbox


### 1. How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## 2. Git guidelines ##

### One commit per change or per bugfix

Create one commit per feature, per change or per fix. It's good to have more commits than less, makes easier to review and find changes.

If you have multiple changes to commit with multiple fixes,features, don't commit all files at once. Separate into logical commits per change.



### Commit title

Commit title should provide some description. it's not enough to only have ticket number. few words from ticket description is ok.


### Commit only tested and working changes

Any code committed to develop branch should be tested and working. If you commit something that breaks the project, making it unable to build or use product, time wasted because of that will be subtracted from your hours.


### No overwriting of history

If you made mistake and want to reverse the change set you already pushed to repo, create new commit with reversed changes. Do not rewrite history in repo.


### Review

Your commits are reviewed using bitBucket commit browser and comments are added. You will receive email notification about any comments on your commits.
If you notice someone else's error or have comment, please add it.

## 3. Code Style

* Use tabs for indentation.
* Do NOT force wrap. Don't wrap at 80/120/whatever characters. this is not 1990s. screens today are wide enough.
* Enclose conditions in {} even if it's one line.

## 4. No modifications of 3rd party libraries

Do not modify sources of any 3rd party libraries: Dojo, JQuery, etc. These libraries may be upgraded and overwritten at any time.


## 5. Working with tickets in Redmine 
Issue tracking system is located at https://red.hitask.com

Use tickets instead of email co communicate about tasks.
If you have question about particular ticket, add comment and assign to person whom you need to ask. The assignee will receive email notification.

### Email notifications

Make sure you receive email notifications about tasks assigned to you. Check that messages sent from red.hitask.com do not go into Spam/Junk folder.
It is very important as we rely on notifications, counting that once you receive notification you ail check the ticket and plan your work.

### Reporting issues

Keep Title simple and to the point.
Provide exact steps to reproduce the issue
Select version of the product corresponding to the version you're testing. If there's a build number, specify that too.
Assign the issue to responsible person

### Issue status workflow 

Developer:
Ticket Status : **Tested error**
This means that when ticket was tested the error was found  in implementation.
You need to review and fix.
After fixing, Assign back to the person who was testing it with status Testing.

Ticket Status: **Feedback**
Feedback means answer is needed. Provide an answer and Assign back with status **Feedback** or **Assigned** 

For Tester:
Ticket Status: **Testing**
When ticket is assigned to you with Testing, test it and Assign it back with status Tested Ok, or Tested Error, depending on result. When returning with Tested Error you will probably add comment.
When you don't understand how to test or have other question, add comment and assign back with status Feedback.



### Who do I talk to? ###

* Architecture, Organizational issues: Roman
* Programming questions: Vadim