package tags;

import groovy.lang.Closure;

import java.io.PrintWriter;
import java.util.Map;

import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;
import play.templates.JavaExtensions;

@FastTags.Namespace("app.hi")
public class JsContent extends BaseTag {
	public static void _jsContent(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String clazz = getArgAsString("class", args, "");
		String tag = getArgAsString("tag", args, DEFAULT_TAG);
		String function = getArgAsString("function", args);
		String content = body != null ? JavaExtensions.addSlashes(JavaExtensions.toString(body)) : "";
		content = content.replace("\r\n", "\n").replace("\n", "\\n").replace("/", "\\/");
		
		out.println("<script type=\"text/javascript\"><!--");
		out.println("require(['hi/project'], function () {");
		out.println("var name = Project.setContainer(null, " + toJson(function) + ");");
		out.println(String.format("document.write('<%s id=\"' + name + '\" class=\"%s\">%s<\\/%s>');", tag, clazz, content, tag));
		out.println("});");
		out.println("--></script>");
	}
}