//dojo.provide("hi.upload.droparea");

/**
 * Handle HTML5 file drag and drop
 */
define(function () {
	
	var FileDropArea = dojo.global.FileDropArea = {
		
		/**
		 * Drag enter was triggered recently
		 * @type {Boolean}
		 */
		enterEventTriggered: false,
		
		/**
		 * Highlight timer
		 */
		highlightTimer: null,
		
		/**
		 * Hide file drop area highlights
		 */
		highlightOff: function (event) {
			if (event && dojo.isFF) {
				//In firefox highlighting is a little wrong
				if (FileDropArea.highlightTimer) {
					clearTimeout(FileDropArea.highlightTimer);
				}
				
				//Get node under cursor and check if actually are outside drop proxy
				var node = document.elementFromPoint(event.pageX - document.documentElement.scrollLeft, event.pageY - document.documentElement.scrollTop);
				
				if (node && node.tagName != 'DIV') {
					node = dojo.dom.getAncestorsByTag(node, 'DIV', true);
				}
				if (node && dojo.hasClass(node, 'file-drop-proxy')) {
					//Mouse is over drop proxy, cancel
					return;
				}
				
				FileDropArea.highlightTimer = setTimeout(FileDropArea.highlightTrueOff, 120);
			} else {
				dojo.removeClass(document.body, 'file-drop-highlight');
			}
		},

		highlightTrueOff: function () {
			dojo.removeClass(document.body, 'file-drop-highlight');
		},
		
		/**
		 * Show file drop area highlights
		 */
		highlightOn: function () {
			if (dojo.isFF) {
				//In firefox highlighting is a little wrong
				if (FileDropArea.highlightTimer) {
					clearTimeout(FileDropArea.highlightTimer);
					FileDropArea.highlightTimer = null;
				}
			}
			
			dojo.addClass(document.body, 'file-drop-highlight');
		},
		
		/**
		 * Stop event and mark as invalid drop target
		 * 
		 * @param {Event} evt
		 */
		stopEventGlobal: function (evt) {
			if (evt && evt.dataTransfer && !dojo.isMac && !dojo.isIE) {
				evt.dataTransfer.dropEffect = 'none';
				evt.dataTransfer.effectAllowed = 'none';
			}
			FileDropArea.stopEvent(evt);
		},
		
		/**
		 * When file drop leaves document remove drop section highlighting
		 * 
		 * @param {Event} evt
		 */
		onDragLeaveGlobal: function (evt) {
			if (dojo.isFF && dojo.isFF < 4) {
				/*
				 * In FF 3.6 relatedTarget can be used to detect when drag leave document
				 */
				if (evt.relatedTarget !== null) return;
			} else {
				/*
				 * If 'enter' event was triggered recently, then this is related event and
				 * only specific element was left, but not all document
				 */
				if (FileDropArea.enterEventTriggered) {
					FileDropArea.enterEventTriggered = false;
					return;
				} 
			}
			
			/*
			 * If drag left element, but didn't entered any other, then it left page
			 */
			FileDropArea.highlightOff(evt);
			FileDropArea.stopEventGlobal(evt);
		},
		
		/**
		 * When file is droped remove drop section highlighting
		 * 
		 * @param {Event} evt
		 */
		onDragDropGlobal: function (evt) {
			FileDropArea.highlightOff();
			FileDropArea.stopEventGlobal(evt);
		},
		
		/**
		 * When file drop enter document highlight drop target sections
		 * 
		 * @param {Event} evt
		 */
		onDragEnterGlobal: function (evt) {
			if (FileDropArea.ifDndFiles(evt)) {
				FileDropArea.enterEventTriggered = true;
				setTimeout(FileDropArea.resetEnterEvent, 10);
				
				FileDropArea.highlightOn();
				FileDropArea.stopEventGlobal(evt);
				
				var opened = hi.items.manager.openedId();
				var uploader = FileUpload.get(opened);
				if (uploader) {
					uploader.initDnD();
				} else if (opened && FileUpload.get(-1)) {
					/* #2934 - collapse opened item if its 'attach file' section is not showing */
					hi.items.manager.collapseOpenedItems();
					//FileUpload.reset(true);
				} else {
					if (!opened) {
						FileUpload.reset(true);
					}
				}
			} else {
				FileDropArea.stopEvent(evt);
			}
		},
		
		resetEnterEvent: function () {
			FileDropArea.enterEventTriggered = false;
		},
		
		/**
		 * Stop event
		 * @param {Object} evt
		 */
		stopEvent: function (evt) {
			evt.preventDefault();
			evt.stopPropagation();
		},
		
		/**
		 * When mouse leaves item stop event
		 * 
		 * #3322 - turn off highlighting after leave, since sometimes we cannot detect when
		 * the mouse has left the document.
		 * This timer is reset in the drag over event, since that will indicate that the user
		 * still has a file and is moving around on the document
		 */
		onDragLeaveItem: function (evt) {
			dojo.global.fileDraggingTimer = setTimeout(function() {
				FileDropArea.highlightOff(evt);
				FileDropArea.stopEventGlobal(evt);
			}, 2 * 1000);

			FileDropArea.stopEvent(evt);
		},
		
		/**
		 * When mouse enters item allow drop
		 */
		onDragEnterItem: function (evt) {
			if (!dojo.isIE) {
				evt.dataTransfer.dropEffect = 'copy';
				evt.dataTransfer.effectAllowed = 'all';
			}
			
			FileDropArea.stopEvent(evt);
		},
		
		onDragOverItem: function (evt) {
			if (FileDropArea.ifDndFiles(evt)) {
				if(!dojo.isIE) {
					evt.dataTransfer.dropEffect = 'copy';
					evt.dataTransfer.effectAllowed = 'all';
				}
			} else {
				FileDropArea.highlightOff();
			}
			if (dojo.global.fileDraggingTimer) {
				clearTimeout(dojo.global.fileDraggingTimer);
				dojo.global.fileDraggingTimer = null;
			}
			FileDropArea.stopEvent(evt);
		},
		
		onDropItem: function (evt) {
			try {
				var files = evt.dataTransfer.files,
					node = evt.target,
					upload = function(file) {
						var fileSizeMb = file.size / 1024 / 1024;
						
						if (fileSizeMb > CONSTANTS.maxUploadFileSizeMb) {
							Project.alertElement.showItem(20, {filename: file.name});
						} else if (file.size == 0) {
							Project.alertElement.showItem(14, {task_alert: 'Failed to upload "' + file.name + '" because file size is 0 byte.'});
						} else {
							FileUploadHTML5FileList.addFile(FileUpload.guid(), file, node.taskId);
						}
					};
				
				while (node && node.tagName != 'DIV') {
					node = node.parentNode;
				}

				for(var i = 0,ii = files.length; i < ii; i++) {
					upload(files[i]);
				}
			} catch (e) {}
			
			FileDropArea.highlightOff();
			
			FileDropArea.stopEvent(evt);
			FileDropArea.onDragDropGlobal(evt);
		},
		
		/**
		 * Use this function in order to determine if file items are being dragged.
		 */
		ifDndFiles: function(event) {
			// Preventing possible issues with stupid old browsers like IE series.
			if (!event || !event.dataTransfer /*|| !event.dataTransfer.files || !event.dataTransfer.items */|| !event.dataTransfer.types) {
				return true;
			}
			
			if (/*event.dataTransfer.items.length == 0 ||*/ event.dataTransfer.types.lenght == 0) {
				return false;
			}
			
			var i = 0;
			/*for (i = 0, len = event.dataTransfer.items.length; i < len; i++) {
				var kind = event.dataTransfer.items[i].kind;
				if (typeof(kind) == 'string' && kind.toLowerCase() == 'file') {
					return true;
				}
			}*/
			
			for (i = 0, len = event.dataTransfer.types.length; i < len; i++) {
				var type = event.dataTransfer.types[i];
				if (typeof(type) == 'string' && (type.toLowerCase() == 'file' || type.toLowerCase() == 'files')) {
					return true;
				}
			}
			
			return false;
		},
		
		ready: function (guid, taskId, filename, filesize) {
			var uploader = FileUpload.get(taskId);
			taskId = taskId || -1;
			/*
			 * #3323
			 * Only need to do this when the item is new but not a sub item or inside a project
			 */
			if (/*taskId <= 0*/taskId==-1) { // New item
				var item = hi.items.manager.item(taskId);
				if (item) {
					/*
					 * #3323
					 * If a form is already opened that does not correspond to a new, detached file item, close it
					 */
					if (hi.items.manager.openedId() && hi.items.manager.openedId()!=-1) 
					{
						var opened = hi.items.manager.openedId();
						hi.items.manager.opened().propertiesView.task.closeEdit();
						hi.items.manager.remove(opened);
					}
					item.openEdit();
					item.editingView.set("type", 6);
				}
			}
			
			if (uploader) {
				var obj = uploader.addFile(guid, filename, filesize);
				obj.isready = true;
				
				if (!taskId || taskId < 0) {
					uploader.updateReadyView();
				}
			}
			
			if (taskId && taskId > 0 && uploader) {
				//Using timeout, because this maybe be called from file constructor
				//so this file is not in the FileUploadHTML5FileList list yet. Need to wait till it is
				setTimeout(function () {
					//Start uploading immediately (one at a time)
					uploader.uploadNext();
				}, 1);
				
				return true;
			} else {
				return false;
			}
		},
		
		prepare: function (guid, taskId, filename, callback) {
			var prepareData = null;
			var file = FileUpload.getFile(guid);
			if (file && 'prepareData' in file && file.prepareData) {
				prepareData = file.prepareData;
			} else {
				prepareData = {'parent': taskId || 0};
			}
			
			FileUpload.prepare(filename, prepareData, function (url, key, ret) {
				var taskId = taskId || 0;
				var uploader = FileUpload.get(taskId);
				
				if (!uploader) {
					var file = FileUpload.getFile(guid);
					file.key = key;
					uploader = file.parent;
					
					if (uploader && (taskId < 0)) {
						uploader.updateReadyView();
					}
				} else {
					var files = uploader.files;
					for(var i=0,ii=files.length; i<ii; i++) {
						if (files[i].id == guid) {
							files[i].key = key;
						}
					}
					
					if (taskId < 0) {
						uploader.updateReadyView();
					}
				}
				
				if (url) {
					if (FileUploadTooltip.showWhenReady){
						FileUpload.loadStorageInfo(function (data) {
							FileUploadTooltip.show();
						});
					}
				}
				
				callback({
					'guid': data.guid,
					'uploadUrl': url,
					'data': ret,
					'postpone': true
				});
				
			}, window, false);
		},
		
		/**
		 * Initialize task drop area
		 */
		initTask: function (node) {
			if (node && node.addEventListener) {
				node.addEventListener('dragleave', this.onDragLeaveItem, false);
				node.addEventListener('dragenter', this.onDragEnterItem, false);
				node.addEventListener('dragover', this.onDragOverItem, false);
				node.addEventListener('drop', this.onDropItem, false);
			}
		},
		
		/**
		 * Initialize
		 * @param {Object} node
		 */
		init: function (node) {
			if (node && node.addEventListener) {
				node.addEventListener('dragleave', this.onDragLeaveGlobal, false);
				node.addEventListener('dragenter', this.onDragEnterGlobal, false);
				node.addEventListener('dragover', this.stopEventGlobal, false);
				node.addEventListener('drop', this.onDragDropGlobal, false);
				node.addEventListener('dragend', this.onDragDropGlobal, false);
			}
		}
	};
	
	return dojo.global.FileDropArea;
	
});
