//var throbberPosition;
/**
 * Get data from service via xhr
 * @param xhr {dojo.request.xhr}
 * @param parms {JSON} Contains action and sessionID
 * @param callback {function} callback on success
 * @param callbackErr {function) callback on failure
 */
function getUserX(xhr,parms,callback,callbackErr) {
	throbberPosition = dojo.clone(dojo.position('loadingPageThrobber',true));
	xhr(parms.url + '/'+parms.action+'?session_id='+parms.session_id,{
		handleAs: "json",
		method: 'GET'
	}).then(function(data) {
		if (callback) callback(data,parms);
	}, function(err) {
		if (callbackErr) callbackErr(err);
	}, function(evt) {
		// Handle a progress event from the request if the
		// browser supports XHR2
	});
}

/**
 * feature #4245
 * Set style height of a container with settings data
 */
function setSettingsBlockStyle() {
	require(["dojo/window", "dojo/dom-style", "dojo/dom", "dojo/query"], function(win, style, dom, query){
		var vs = win.getBox();
		var height = vs.h - dojo.byId('header').clientHeight - 15;
		style.set(dom.byId("settingsPageContent"), "height", height + "px");
		height = height - 60;
		query(".wrap-settings-fieldset").forEach(function(elem) {
			style.set(elem, "height", height + "px");
		});
		height = height + 55;
		query(".wrap-business-fieldset").forEach(function(elem) {
			style.set(elem, "height", height + "px");
		});
	});
}

function saveSettings(api,formData,successMsg,errorMsg,expectedObjectName,successCallback,errorCallback)
{
	require(['dojo/request/xhr'],function(xhr) {
		dojo.global.showThrobber();
		xhr(api.url + '/'+api.action,{
			handleAs: "json",
			method: api.method,
			data:formData
		}).then(function(data) {
			if (data.response_status)
			{
				if (data.response_status == 0)
				{
					showResult(successMsg);
					if (successCallback) successCallback(data);
				} else
				{
					showResult(errorMsg);
					if (errorCallback) errorCallback(data);
				}

			} else if (data[expectedObjectName]) 
			{
				showResult(successMsg);
				if (successCallback) successCallback(data);
			} else if (typeof data == 'object') 
			{
				showResult(successMsg);
				if (successCallback) successCallback(data);
				
			} else {
				showResult(errorMsg);
				if (errorCallback) errorCallback(data);
			}

			
		}, function(err) {
			//console.log('err',err);
			if (err && err.response && err.response.data) {
				if (err.response.data.response_status || err.response.data.error_message) {
					showResult(err.response.data.error_message||errorMsg);
				}
			} else {
				showResult(errorMsg);
			}
			
			if (errorCallback) errorCallback(err);
		}, function(evt) {
			// Handle a progress event from the request if the
			// browser supports XHR2
		});		
	});
}

/**
 * Set the profile form in the My Profile tab on settings page
 * @param userData {JSON} JSON data from /user/info service
 */
function setProfileForm(userData, parms) {
	if (userData.email && dojo.byId('changeEmail')) dojo.byId('changeEmail').value = userData.email;
	if (userData.email && dojo.byId('changeEmailSent') && (!userData.emailConfirmed || (userData.email.toLowerCase() != userData.emailConfirmed.toLowerCase())) && !dojo.byId('emailMessageCouldNotBeSentErrorNode')) {
		dojo.removeClass(dojo.byId('changeEmailSent'), 'hidden');
	}
	if (userData.login && dojo.byId('settingsUsername')){
		// dijit.byId('settingsUsername').set('value', userData.login);
		dojo.byId('settingsUsername').value = userData.login;
	}
	if (userData.lastName && dojo.byId('changeLastname')) dojo.byId('changeLastname').value = userData.lastName;
	if (userData.firstName && dojo.byId('changeFirstname')) dojo.byId('changeFirstname').value = userData.firstName;
	if (parms.http && parms.host && userData.pictureHash && dojo.byId('current_photo')) {
		dojo.byId('current_photo').src = parms.http+'://'+parms.host+'/avatar/'+userData.pictureHash+'.96.gif';
		dojo.style('personal_delete_btnWrapper', 'display', userData.pictureGenerated ? 'none' : '');
		dojo.removeClass('avatarContainer', 'hidden');
		dojo.addClass('no_current_photo', 'hidden');
		var uploadButton = dijit.byId('personal_picture');
		if (userData.pictureGenerated) {
			if (uploadButton) uploadButton.set('label', i18n('settings.add'));
		} else {
			if (uploadButton) uploadButton.set('label', i18n('settings.personal.change'));
		}
	} else {
		dojo.removeClass('no_current_photo', 'hidden');
		dojo.addClass('avatarContainer', 'hidden');
		var uploadButton = dijit.byId('personal_picture');
		if (uploadButton) uploadButton.set('label', i18n('settings.add'));
	}
}

/**
 * Set the form in the Display tab on settings page
 * @param prefData {JSON} JSON data from /user/preferences service
 */
function setDisplayForm(prefData) {
	if (prefData.language && dijit.byId('locales')) {
		dijit.byId('locales').set('value', prefData.language);
		dojo.setAttr(dijit.byId('locales').domNode, 'prevValue', prefData.language);
	}
	if (prefData.comment_order && dojo.byId('comment_order_1') && dojo.byId('comment_order_0'))
	{
		if (prefData.comment_order > 0)
		{
			dojo.byId('comment_order_1').checked=true;
		} else
		{
			dojo.byId('comment_order_0').checked=true;
		}
	}
	
	if (prefData.first_day_of_week && dijit.byId('first_day_of_week') && prefData.first_day_of_week >= 0 && prefData.first_day_of_week < 7)
	{
		dijit.byId('first_day_of_week').set('value', prefData.first_day_of_week);
	}
	
	if (prefData.date_format && dijit.byId('date_format'))
	{
		dijit.byId('date_format').set('value', prefData.date_format);
	}
	
	if (prefData.time_format && dijit.byId('time_format'))
	{
		dijit.byId('time_format').set('value', prefData.time_format);
	}
	
	if (prefData.time_zone && dijit.byId('timezones'))
	{
		dijit.byId('timezones').set('value', prefData.time_zone);
	}
	
}

function setNotificationsForm(prefData) {
	//settings_notifications_group is globally defined in notifications.html
	//settings_notifications_preIDs globally defined in notifications.html
	var allId, preId, allName, i;

	for (var z = 0, len = settings_notifications_preIDs.length; z < len; z++) {
		allId = settings_notifications_preIDs[z].allId;
		preId = settings_notifications_preIDs[z].preId;
		allName = settings_notifications_preIDs[z].allName;
		
		if (dojo.byId(allId)) {
			dojo.byId(allId).checked = false;
		}
		
		// Check/uncheck specific notification options.
		for (i in settings_notifications_group) {
			if (prefData[preId + i] && prefData[preId + i].toUpperCase() == 'TRUE') {
				dojo.byId(preId + i).checked = true;
			} else {
				dojo.byId(preId + i).checked = false;
			}
		}
		// If all options checked then check the main one.
		if (ifAllSubNotificationsChecked(preId)) {
			dojo.byId(allId).checked = true;
		}
		
		// If main option is checked then check also all sub options.
		if (prefData[allName] && prefData[allName].toUpperCase() == 'TRUE') {
			dojo.byId(allId).checked = true;
			for (i in settings_notifications_group) {
				dojo.byId(preId + i).checked = true;
			}
		} else {
			dojo.byId(allId).checked = false;
		}
	}
	
	// Receive news.
	if (prefData['receive_email'] && prefData['receive_email'].toUpperCase() == 'TRUE') {
		dojo.byId('receive_email').checked = true;
	} else {
		dojo.byId('receive_email').checked = false;
	}
	
	for (i = 1; i < 8; i++) {
		var k = 'daily_summary_email_' + i;
		if (typeof(prefData[k]) != 'undefined' && prefData[k].toUpperCase() == 'TRUE' && dojo.byId(k)) {
			dojo.byId(k).checked = true;
		}
	}
}

function ifAllSubNotificationsChecked(preId) {
	var count = 0,
		changed = 0;
	
	for (var i in settings_notifications_group) {
		count++;
		if (dojo.byId(preId + i).checked) {
			changed++;
		}
	}
	
	return count == changed;
}

function setTaskPreferencesForm(prefData) {
	var node, value;

	if (prefData.show_confirmation_complete && prefData.show_confirmation_complete.toUpperCase() === 'TRUE' && dojo.byId('show_confirmation_complete')) {
		dojo.byId('show_confirmation_complete').checked = true;
	} else {
		dojo.byId('show_confirmation_complete').checked = false;
	}
	
	if (prefData.duplicate_with_files && prefData.duplicate_with_files.toUpperCase() === 'TRUE' && dojo.byId('duplicate_with_files')) {
		dojo.byId('duplicate_with_files').checked = true;
	} else {
		dojo.byId('duplicate_with_files').checked = false;
	}
	
	if (typeof(prefData.default_assign_user) == 'undefined') {
		prefData.default_assign_user = 0;
	}
	
	if ((prefData.default_assign_user || prefData.default_assign_user === 0) && assigneeWidget) {
		var defaultUserId = parseInt(prefData.default_assign_user),
			defaultUserName = '',
			defaultUser = assigneeWidget.assigneeStore.query({id: defaultUserId});

		if (isNaN(defaultUserId) || !defaultUser.length) {
			defaultUserId = 0;
			defaultUser = assigneeWidget.assigneeStore.query({id: defaultUserId});
		}
		defaultUserName = defaultUser.length? defaultUser[0].name : 'None';

		assigneeWidget.assigneeComboBox.set('value', defaultUserName);
	}

	//enable time tracking
	var time_tracking_checkbox = dojo.byId('enable_time_tracking');

	if(time_tracking_checkbox){
		if(prefData.enable_time_tracking === 'true'){
			time_tracking_checkbox.checked = true;
		}else{
			time_tracking_checkbox.checked = false;
		}
	}
	
	//sub-task complete config
	node = dojo.byId('sub_task_complete_affect_parent');
	if(node){
		if(prefData.sub_task_complete_affect_parent === 'true'){
			node.checked = true;
		}else{
			node.checked = false;
		}
	}
	
	//default_sharing_permission
	var sharingPermissionSettingWidget = dijit.byId('sharing_permission_setting');
	if (Project.businessId && typeof(sharingPermissionSettingWidget) != 'undefined') {
		if(prefData.default_sharing_permission){
			sharingPermissionSettingWidget.set('value', prefData.default_sharing_permission);
		}else{
			sharingPermissionSettingWidget.set('value', 50);
		}
	}
	
	//default sharing permission for automatically created items
	if(Project.businessId && typeof(sharingWidget) != 'undefined') {
		value = prefData.default_sharing_permission_auto;
		if (value) {
			value = typeof value === 'string' ? JSON.parse(value) : value;
			sharingWidget.fillSharingList(value);
		}
	}

	//default sharing permission for automatically created items
	if(Project.businessId && typeof(defaultSharingWidget) != 'undefined') {
		value = prefData.default_new_item_sharing_permission;
		if (value) {
			value = typeof value === 'string' ? JSON.parse(value) : value;
			defaultSharingWidget.fillSharingList(value);
		}
	}

	//default permission value type for new item(use last permission or self-defined permission)
	var lastRadio = dojo.byId('lastPermissionRadio'),
		myRadio = dojo.byId('myPermissionRadio');		//user user's own permission configuration

	if (Project.businessId && lastRadio && myRadio) {
		value = prefData.default_new_item_use_last || 'true';		//default value is true(use last value)
		if (value === 'true') {
			lastRadio.checked = true;
		} else {
			myRadio.checked = true;
		}
	}
}

function setBusinessAccountTab(bData,parms) {
	var fmt = 'd MMMM yyyy',
		myDate,
		expDate = businessAccountTemplates['expirationDate'],
		licensesTemplate = businessAccountTemplates['licensesUsed'],
		storageTemplate = businessAccountTemplates['storageUsed'],
		licensesTitle = '',
		storageTitle = '';
	if (bData.title && dojo.byId('business_title')) dojo.byId('business_title').value=bData.title;
	if (parms.http && parms.host && bData.pictureHash && dojo.byId('business_current_photo')) {
		dojo.byId('business_current_photo').src = parms.http+'://'+parms.host+'/avatar/business/'+bData.pictureHash+'.96.gif';
		dojo.style('business_delete_btnWrapper','display','');
		dojo.removeClass('business_current_photo', 'hidden');
		dojo.addClass('business_no_current_photo', 'hidden');
	} else {
		dojo.removeClass('business_no_current_photo', 'hidden');
		dojo.addClass('business_current_photo', 'hidden');
	}
	
	if (bData.expirationDate && dojo.byId('expiration_date')) {
		myDate = new Date(bData.expirationDate);
		dojo.byId('expiration_date').innerHTML = expDate.replace('%s',dojo.date.locale.format(myDate,{selector:'date',datePattern:fmt}));
	} else if (dojo.byId('expiration_date')) {
		dojo.byId('expiration_date').innerHTML = expDate.replace('%s','unknown');
	}
	
	if (bData.licensesNumber && bData.licensesUsed && dijit.byId('licenseProgressBar')) {
		licensesTitle = licensesTemplate;
		licensesTitle = licensesTitle.replace('%d1',bData.licensesUsed);
		licensesTitle = licensesTitle.replace('%d2',bData.licensesNumber);
		dijit.byId('licenseProgressBar').set({
			maximum:bData.licensesNumber,
			value:bData.licensesUsed,
			title:licensesTitle
		});
		dijit.byId('licenseProgressBar').update();
	}
	
	if (bData.storageQuota && bData.storageUsed >= 0 && bData.storageQuotaFormatted && bData.storageUsedFormatted && dijit.byId('storageProgressBar')) {
		storageTitle = storageTemplate;
		storageTitle = storageTitle.replace('%d1',bData.storageUsedFormatted);
		storageTitle = storageTitle.replace('%d2',bData.storageQuotaFormatted);
		dijit.byId('storageProgressBar').set({
			maximum:bData.storageQuota,
			value:bData.storageUsed,
			title:storageTitle
		});
		dijit.byId('storageProgressBar').update();
	} 
}

function setBusinessMemberTab(bData,parms) {
	var fmt = 'd MMMM yyyy',
		myDate,
		expDate = businessAccountTemplates['expirationDate'],
		licensesTemplate = businessAccountTemplates['licensesUsed'],
		storageTemplate = businessAccountTemplates['storageUsed'],
		licensesTitle = '',
		storageTitle = '';
	
	if (bData.expirationDate && dojo.byId('member_expiration_date')) {
		myDate = new Date(bData.expirationDate);
		dojo.byId('member_expiration_date').innerHTML = expDate.replace('%s',dojo.date.locale.format(myDate,{selector:'date',datePattern:fmt}));
	} else if (dojo.byId('member_expiration_date')) {
		dojo.byId('member_expiration_date').innerHTML = expDate.replace('%s','unknown');
	}
	
	if (bData.storageQuota && bData.storageUsed >= 0 && bData.storageQuotaFormatted && bData.storageUsedFormatted && dijit.byId('memberStorageProgressBar'))
	{
		storageTitle = storageTemplate;
		storageTitle = storageTitle.replace('%d1',bData.storageUsedFormatted);
		storageTitle = storageTitle.replace('%d2',bData.storageQuotaFormatted);
		dijit.byId('memberStorageProgressBar').set({
			maximum:bData.storageQuota,
			value:bData.storageUsed,
			title:storageTitle,
			label:storageTitle
		});
		dijit.byId('memberStorageProgressBar').update();
	} 
}

function initializePersonalTabs(data,xhr,parms) {
	var storageTitle,
		fmt = 'd MMMM yyyy',
		formattedExpirationDate,
		currentTime = new Date().getTime(),
		expDate,
		progressBar;
	getUserX(xhr,{url:parms.url,action:'user/fileStorageInfo',session_id:parms.session_id},function(info) {
		if (data.isPremium) {
			hideFreeAccountTab();
			progressBar = 'premiumStorageProgressBar';
			if (data.expirationDate) {
				expDate = new Date(data.expirationDate);
				/* TEST expDate = new Date('2012-04-30T05:00:00.000+00:00');*/
				formattedExpirationDate = dojo.date.locale.format(expDate,{selector:'date',datePattern:fmt});
				if (expDate.getTime() > currentTime) {
					dojo.addClass('premium_expired','hidden');
					dojo.removeClass('premium_not_expired','hidden');
					dojo.byId('not_expired_date').innerHTML = formattedExpirationDate;
				} else {
					dojo.removeClass('premium_expired','hidden');
					dojo.addClass('premium_not_expired','hidden');
					dojo.byId('expired_date').innerHTML = formattedExpirationDate;
					
				}
			} else
			{
				dojo.addClass('premium_not_expired','hidden');
				dojo.addClass('premium_expired','hidden');
			}
		} else
		{
			progressBar = 'freeAccountProgressBar';
			hidePremiumAccountTab();
		}		
		
		//Storage info:
		if (info.quota && info.used >= 0 && info.quota_formatted && info.used_formatted)
		{
			storageTitle = premiumStorageTemplate;
			storageTitle = storageTitle.replace('%d1',info.used_formatted);
			storageTitle = storageTitle.replace('%d2',info.quota_formatted);
			dijit.byId(progressBar).set({
				maximum:info.quota,
				value:info.used,
				title:storageTitle,
				label:storageTitle
			});
			dijit.byId(progressBar).update();
		}
	},function(error) {
		console.log('error loading quota information');
	});
}

function hideBusinessAccountTab() {
	var tab;
	(tab = dijit.byId('businessAccountTab')) && dijit.byId('settingTabs').removeChild(tab);
	(tab = dijit.byId('businessMemberTab')) && dijit.byId('settingTabs').removeChild(tab);
}

function hideBusinessMemberTab() {
	var tab;
	(tab = dijit.byId('businessMemberTab')) && dijit.byId('settingTabs').removeChild(tab);
}

function hideBusinessAdminTab() {
	var tab;
	(tab = dijit.byId('businessAccountTab')) && dijit.byId('settingTabs').removeChild(tab);
}

function hideFreeAccountTab() {
	var tab;
	(tab = dijit.byId('freeAccountTab')) && dijit.byId('settingTabs').removeChild(tab);
}

function hidePremiumAccountTab() {
	var tab;
	(tab = dijit.byId('premiumAccountTab')) && dijit.byId('settingTabs').removeChild(tab);
}

/**
 * Save settings on form in Display tab on settings page
 */
function saveDisplaySettings(api)
{
	//Post values based on specifications in Feature #1746
	var data = {
		'time_zone': dijit.byId('timezones') && dijit.byId('timezones').get('value'),
		'time_format': dijit.byId('time_format') && dijit.byId('time_format').get('value'),
		'first_day_of_week': dijit.byId('first_day_of_week') && dijit.byId('first_day_of_week').get('value'),
		'date_format': dijit.byId('date_format') && dijit.byId('date_format').get('value'),
		'comment_order':dojo.byId('comment_order_0').checked?0:1,
		'language': dijit.byId('locales') && dijit.byId('locales').get('value'),
		'session_id':api.session_id
	};
	
	if (dojo.getAttr(dijit.byId('locales').domNode, 'prevValue') != dijit.byId('locales').get('value')) {
		saveSettings(api,data,'Successfully saved display settings','Error saving display settings', null, function() {
			window.location.reload();
		});
	} else {
		saveSettings(api,data,'Successfully saved display settings','Error saving display settings');
	}
}

function uploadPersonalPhoto(api)
{
	var data = {
		'session_id':api.session_id,
		'MAX_FILE_SIZE':dojo.query('input[name=MAX_FILE_SIZE]')[0].value
	};
}

/**
 * Create uploader button overlay for Flash and IE - Proposed fix for Bug #1843 - Not implemented yet
 * @param {JSON} obj 
 */
function createUploaderBtnOverlay(obj){}

function positionOverlay(prefix,container,placeholder)
{
	var positions = {
		personal:{left:0,top:128},
		business:{left:37,top:776}
	};
	var pos = dojo.marginBox(placeholder);
	container.style.position='relative';
	container.style.left=positions[prefix].left+'px';
	container.style.top=~~(pos.t-positions[prefix].top) + 'px';
	/*
	container.style.left = * #2476 ~~(pos.l - 8) +* '0px';
	container.style.top = ~~(pos.t - * #2476 6 *128) + 'px';
	*/
	container.style.zIndex = 9000;
}

function hideOverlay(container){
	dojo.style(container,{top:'-1000px',left:'-1000px'});
}

function createUploader(prefix, IEBtnTemplate, uploadURL, isOver, btnLabel, completeCallback){
	var uploader,
		overlay;
	
	dojo.create('div', {id: prefix + '_photo_placeholder'}, prefix + '_conditional_photo_button','after');
	var uploaderOptions = {
		'class': 'picture',
		uploadOnSelect: true,
		style: 'cursor:pointer;',
		id: prefix + '_picture',
		name: 'picture',
		label: btnLabel,
		url: uploadURL,//'${_apiConfig.apiUrl}/user/picture?session_id=${_userBean.api2SessionId}',
		serverTimeout: 5 * 1000
	};
	overlay = dojo.byId(prefix + '_photo_placeholder');
	
	uploader = new dojox.form.Uploader(uploaderOptions,overlay);
	uploader._getButtonStyle = function (node) {
		uploader.btnSize = {w:180, h:45};
	};
	uploader.startup();
	/* Bug #1843 - this should point a mouse pointer on uploader button in non IE browsers */
	if (!dojo.isIE) {
		dojo.style(dojo.query('input[name=picture]',dojo.byId(prefix+'_picture_select'))[0],'cursor','pointer');
		dojo.connect(uploader,'_createInput',function() {
			dojo.style(dojo.query('input[name=picture]',dojo.byId(prefix+'_picture_select'))[0],'cursor','pointer');
		});
	}
	
	//Upload picture			
	dojo.connect(dijit.byId(prefix+'_picture'),'onComplete',function(data) {
		hideThrobber();
		testFlashData = dojo.clone(data);
		
		if (data.hash && completeCallback) completeCallback(data);
		else showResult(i18n('settings.picture.validation.errorcode.' + data.response_status));
	});
	
	dojo.connect(dijit.byId(prefix+'_picture'),'onBegin',function(data) {
		showThrobber(i18n('settings.picture.avatar_uploading'));
	});
	
	dojo.connect(dijit.byId(prefix+'_picture'),'onError',function(evtObj) {
		//dijit.byId('picture').set('disabled',false);
		hideThrobber();
		console.log("error",evtObj);
		showResult(i18n('settings.picture.error_upload'));
	});
	return uploader;
}

function showResult(msg)
{
	dojo.byId('tooltip_settingschangedresultMSG').innerHTML = msg;
	dojo.global.hideThrobber();
	if (msg) {
		Tooltip.open('settingschangedresult', "", dojo.byId('loadingPageThrobber'),null,null,null,{
			orient:false,
			x:throbberPosition.x,
			y:throbberPosition.y
		}, true/*underLayWrapper*/);
	}
}
