//dojo.provide("hi.sortable.project");
define(function () {
	
	dojo.declare("dojo.dnd.HiSortDragProject", [dojo.dnd.Avatar], {
		type: 'project',
		dontMove: true,
		
		constructor: function (manager) {
			var div = dojo.create('div', {'class': 'projectDropIndicator', 'style': 'position: absolute; z-index: 999;'});
			dojo.body().appendChild(div);
			this.node = div;
		},
		
		update: function () {}
	});
	
	dojo.declare("dojo.dnd.HiProjectDragSource",[dojo.dnd.Source],{withHandles:true, delay: 5, accept: ["project"], type: "project",
		avatarClass: dojo.dnd.HiSortDragProject,	//Force not to show avatar
		dragBefore: false,
		/* #2535 via https://support.sitepen.com/issues/22098 */
		onSelectStart:function(e) {return;},
		onDndCancel: function () {
			// if (Project.getSorting(2) !== Project.ORDER_PRIORITY) return;
			dojo.dnd.HiProjectDragSource.superclass.onDndCancel.call(this);
			//JDT if (dojo.dnd.Manager().avatar) dojo.dnd.Manager().avatar.destroy();
			if (dojo.dnd.Manager.manager().avatar) dojo.dnd.Manager.manager().avatar.destroy();
		},
		
		onDndDrop: function () {
			// if (Project.getSorting(2) !== Project.ORDER_PRIORITY) return;
			dojo.dnd.HiProjectDragSource.superclass.onDndDrop.call(this);
		},
		
		onDndStart: function(source, nodes, _x) {
			// if (Project.getSorting(2) !== Project.ORDER_PRIORITY) return;
			dojo.dnd.HiProjectDragSource.superclass.onDndStart.call(this, source, nodes, _x);
			this.dragNode = nodes[0];
		},
		
		onMouseOver: function (e) {
			dojo.dnd.HiProjectDragSource.superclass.onMouseOver.call(this, e);
			this.moveAvatar(e);
		},
		
		moveAvatar: function (e) {
			if (this.isDragging && this.dragNode) {
				//JDT var m = dojo.dnd.Manager();
				var m = dojo.dnd.Manager.manager();
				//Make sure we are not droping on same element and drag source is the same as drag target (projects group)
				if (this.current && this.dragNode != this.current && m.source == this) {
					var projects = Project.draggingProjectList.domNode;
					var pos_x = dojo.position(projects, true).x;
					
					dojo.style(m.avatar.node, {
										'width': projects.offsetWidth + 'px',
										'left': Math.floor(pos_x) + 'px'
									});
					
					this.moveAvatarVertical(e);
				}
			}
		},
		
		moveAvatarVertical: function (e, before) {
			if (this.isDragging && this.dragNode) {
				//JDT var m = dojo.dnd.Manager();
				var m = dojo.dnd.Manager.manager();
				//Make sure we are not droping on same element and drag source is the same as drag target (projects group)
				if (this.current && this.dragNode != this.current && m.source == this) {
					var pos = dojo.position(this.current, true);
					before = (typeof before == 'boolean' ? before : this.before);
						this.dragBefore = before;
					
					var t = pos.y;
					if (before) {
						t -= 6;
					} else {
						t += this.current.offsetHeight + 4;
					}
					
					dojo.style(m.avatar.node, {'top': Math.floor(t) + 'px'});
				} else {
					dojo.style(m.avatar.node, {'top': '-100px'});
				}
			}
		},
		
		_markTargetAnchor: function (before) {
			this.moveAvatarVertical(null, before);
			dojo.dnd.HiProjectDragSource.superclass._markTargetAnchor.call(this, before);
		},
		
		_unmarkTargetAnchor: function () {
			this.moveAvatarVertical();
			dojo.dnd.HiProjectDragSource.superclass._unmarkTargetAnchor.call(this);
		}
	});
	
	return dojo.dnd.HiSortDragProject;
});