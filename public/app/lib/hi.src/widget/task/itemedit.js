 /**
 * The widget corresponding to the form for editing a task item (by double clicking on a collapsed item or by expanding
 * and clicking 'Modify') and the form corresponding to creating a sub-task (by clicking the sub-task button on an expanded
 * item)
 * 
 * It is a dojo-templated widget, so should be more or less automatically rendered and torn down in the usual way 
 * 
 * This object is accessible via:
 * hi.items.manager.opened().editingView
 * hi.items.manager._items[ID][name].editingView
 * 
 */
define([
	"dojo/_base/array", // array.forEach array.map
	"dijit/form/TimeTextBox",
	"dojo/_base/declare", // declare
	"dojo/cldr/supplemental", // cldrSupplemental.getFirstDayOfWeek
	"dojo/date", // date
	"dojo/date/locale",
	"dojo/dom", // dom.setSelectable
	"dojo/dom-class", // domClass.contains
	"dojo/_base/event", // event.stop
	"dojo/_base/lang", // lang.getObject, lang.hitch
	"dojo/_base/sniff", // has("ie") has("webkit")
	"dojo/string", // string.substitute
	"dojo/_base/window", // win.doc.createTextNode
	"dijit/_Widget",
	"dijit/_Templated",
	"dijit/_WidgetsInTemplateMixin",
	"dojo/text!./itemnewedit.html",
	"dojo/text!./template_include/itemedit_title.html",
	"dojo/fx",
	"dijit/form/Button",
	'dojo/sniff',
	"dijit/form/ComboBox",
	"dijit/form/Select",
	"hi/widget/form/hiDateTextBox",
	'dojo/aspect',
	'hi/widget/task/item_DOM',
	"hi/widget/task/config/fieldsController",
	'dojox/css3/transition',
	'dojo/query',
	'dojo/NodeList-manipulate',
	'dojo/store/Memory'
], function(array, TimeTextBox, declare, cldrSupplemental, date, local, dom, domClass, event, lang, has, string, win,
			_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template, template_title, fx,
			Button, sniff, ComboBox, dijitSelect, hiDateTextBox, aspect, item_DOM, fieldsController, css3fx, dojoQuery) {

	//Replace localization string in template instead of using attributes
	template = template.replace('{include_title}', template_title).replace(/\{#([^#]+)#\}/g, function (all, str) {
		var localized = hi.i18n.getLocalization(str.trim()) || '';
		if (!localized) console.warn('Missing localization for "' + str + '"');
		return localized;
	});
	
	/* Feature #4323 Time picker open at 12:00 noon by default */
	TimeTextBox.prototype._onDropDownClick = function() {
		if (this.get("value") === null) {
			var containers = document.querySelectorAll('.dijitTimePickerPopup');
			for (var i = 0; i < containers.length; i++) {
				var rowToScrollTo = containers[i].querySelectorAll('.dijitTimePickerMarker')[12];
				containers[i].scrollTop = rowToScrollTo.offsetTop;
			}
		}
	};
	/* -->> end Feature #4323 */
	
	// module:
	//		hi/widget/TaskItemEdit
	// summary:
	//		Task item edit view
	
	var TaskItemEdit = declare("hi.widget.TaskItemEdit", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
		// summary:
		//		Task item edit view
		
		// Template for the view
		templateString: template,
		
		// TaskItem widget
		task: null,
		
		// Other widgets are in template, parse them
		widgetsInTemplate: true,

		doRememberPermission: false,		//#5256
		
		// Properties which will be copied from task
		_copyProperties: {
			"taskId": true,
			"starred": true,
			"title": true,
			"message": true,
			"participants": true,
			"tags": true,
			"category": true,
			"color": true,
			"reminder_time": true, "reminder_time_type": true, "reminder_enabled": true,
			"time_track": true, "time_est": true,
			"recurring": true,
			'recurring_interval':true,
			'recurring_end_date':true,
			"parent": true,
			"priority": true,
			"is_all_day": true,
			"assignee": true, "shared": true, "my": true,
			"permissions": [], "permission": 0
		},

		// Properties which will be copied to task after save
		// NOTE: this is used to make sure user sees then change immediatelly
		_reverseCopyProperties: {
			"title": true,
			"category": true,
			"color": true,
			"recurring": true,
			"start_date": true, "end_date": true, "is_all_day": true,
			"priority": true,
			"assignee": true, "shared": true, "my": true
		},
		
		_onPermissionsChange: function() {
			var useLast = Hi_Preferences.get('default_new_item_use_last', true, 'bool');

			if (this._isPermissionsChanged(this.get('permissions'), this.sharingWidget.get('value'))) {
				if (!useLast) {
					this.rememberPermission.style.display = 'inline-block';
				}
			} else {
				this.rememberPermission.style.display = 'none';
				this.rememberPermissionCheckbox.checked = false;
				this.doRememberPermission = false;
			}
		},

		postCreate: function() {
			this.inherited(arguments);
			var t = this;

			this.own(
				aspect.after(this.sharingWidget, 'onChange', lang.hitch(this, this._onPermissionsChange))
			);
		},

		destroy: function() {
			this.inherited(arguments);
			console.log('in itemedit destroy');
		},
		/* ----------------------------- ATTRIBUTES ------------------------------ */

		/* Task ID */
		taskId: null,
		_setTaskIdAttr: function (taskId) {
			this._set("taskId", taskId);
			this.set("isNew", !taskId || taskId < 0);
		},
		
		/* Color */
		color: "0",
		_setColorAttr: function (color) {
			this._set("color", color);
			
			this.colorInput.value = color;
			
			//Color picker
			var items = dojo.query("div", this.colorPickerNode),
				i = 0,
				ii = items.length;
			
			for (; i<ii; i++) {
				dojo.toggleClass(items[i], "selected", color == dojo.getAttr(items[i], "data-color"));
			}
		},
		
		/* Is this a new item */
		isNew: false,
		_setIsNewAttr: function (is_new) {
			is_new = !!is_new;
			this._set("isNew", is_new);

			
			dojo.toggleClass(this.domNode, "is_new_item", is_new);
			dojo.toggleClass(this.categorySelectionNode, "hidden", !is_new);

			dojo.toggleClass(this.parentContainerNode, "hidden", is_new);
			
			if (is_new) {
				this.task.set("removeOnCollapse", true);
			}
		},
		
		/* Category */
		category: hi.widget.TaskItem.CATEGORY_TASK,
		_setCategoryAttr: function (category) {
			this._set("category", category);
			this.categoryInput.value = category;
			
			if (category == hi.widget.TaskItem.CATEGORY_TASK || category == hi.widget.TaskItem.CATEGORY_EVENT || category == hi.widget.TaskItem.CATEGORY_NOTE) {
				dojo.removeClass(this.saveButton.attr('domNode'),'hidden');
				dojo.removeClass(this.titleContainerNode, "hidden");
			}
			if (category == hi.widget.TaskItem.CATEGORY_TASK || category == hi.widget.TaskItem.CATEGORY_EVENT) {
				dojo.removeClass(this.reminderContainerNode, "hidden");
				dojo.removeClass(this.timeContainerNode, "hidden");
			} else if (category == hi.widget.TaskItem.CATEGORY_NOTE || category == hi.widget.TaskItem.CATEGORY_FILE) {
				dojo.addClass(this.reminderContainerNode, "hidden");
				dojo.addClass(this.timeContainerNode, "hidden");
			}
			
			if (category == hi.widget.TaskItem.CATEGORY_FILE) {
				// #2785: while editing File Item it's title must be simple label not input text box.
				if (this.titleTextNode) {
					dojo.removeClass(this.titleTextNode, "hidden");
					this.titleTextNode.innerHTML = htmlspecialchars(this.get('title'));
				}
				dojo.addClass(this.titleInputNode.domNode, "hidden");
				dojo.addClass(this.domNode, "tab-file");
				if (this.taskId < 0) dojo.addClass(this.saveButton.attr('domNode'),'hidden');
				this.titleContainerNode.style.display = 'block';
				this.titleContainerNode.style.marginTop = '-12px';
				
				if (FileUpload.enabled) {
					FileUpload.reset(this.taskId);
				}
			} else {
				dojo.addClass(this.titleTextNode, "hidden");
				dojo.removeClass(this.titleInputNode.domNode, "hidden");
				dojo.removeClass(this.domNode, "tab-file");
				this.titleContainerNode.style.marginTop = '';
			}
			fieldsController.showFields(this, category);
			this.fillReminderText();
		},
		
		/* Type */
		type: 1,
		_setTypeAttr: function (type) {
			this._set("type", type);
			
			if (type == 1 || type == 2 || type == 4) {
				//Task, Event or Note
				this.set("category", type);
			} else if (type == 6) {
				//File
				this.set("category", hi.widget.TaskItem.CATEGORY_FILE);
				dojo.addClass(this.titleContainerNode,'hidden');
			}
			
			if (type == 5) {
				//Multiple items
				this.set("category", hi.widget.TaskItem.CATEGORY_TASK);
				
				dojo.addClass(this.reminderContainerNode, "hidden");
				dojo.addClass(this.timeContainerNode, "hidden");
				dojo.addClass(this.titleContainerNode, "hidden");
				dojo.addClass(this.messageContainerNode, "hidden");
				dojo.addClass(this.domNode, "tab-many-tasks");
			} else {
				dojo.removeClass(this.messageContainerNode, "hidden");
				dojo.removeClass(this.domNode, "tab-many-tasks");
				
				NewItemManyTasks.goStepBack();
				this.manyTasksInput.value = "";
				this.handleManyTasksBlur();
			}
			
			if (this.isNew) {
				var nodes = dojo.query('td', this.categorySelectionNode),
					i = 0,
					ii = nodes.length;
				
				for (; i<ii; i++) {
					dojo.toggleClass(nodes[i], "active", type == parseInt(dojo.getAttr(nodes[i], "data-type"), 10));
				}
			}
		},
		
		/* Starred */
		starred: false,
		_setStarredAttr: function (starred) {
			starred = !!starred;
			this._set("starred", starred);
			
			dojo.toggleClass(this.starredNode, "star-selected", starred);
			dojo.query("input", this.starredNode, true)[0].value = starred ? 1 : 0;
		},
		
		/* Title */
		title: "",
		_setTitleAttr: function (title) {
			var inputDijit = this.titleInputNode;
			this._set("title", title);
			inputDijit.set('placeHolder', this._getDefaultTitle());
		
			// #3773
			//title = htmlspecialchars(title);
			inputDijit.set('value', title);
		},

		_getDefaultTitle: function () {
			return hi.i18n.getLocalization("project.enter_item_name");
		},
		
		/* Message */
		message: "",
		_setMessageAttr: function (message) {
			/* #2764 */
			message = dojo.trim(message || "");
			
			this._set("message", message);
			this.messageInput.set('value', message);
			//dojo.toggleClass(this.messageLabelNode, "hidden", message);
		},
		
		/* Start date */
		hasStartDateTime: false,
		_setHasStartDateTimeAttr: function (has) {
			this._set("hasStartDateTime", has);
			//dojo.setAttr(this.recurringInput, "disabled", !has); reccurent needed only startDate
			
			// if (this.recurringInput) this.recurringInput.set("disabled", !has);
			if (this.recurringIntervalNode) this.recurringIntervalNode.set("disabled", !has);
			if (this.recurrenceEndDateWidget) this.recurrenceEndDateWidget.set("disabled", !has);
			
			if (!has) {
				this.set('recurring', 0);
			}
			
			if (this.get('recurring') == 0) {
				if (this.recurringIntervalNode) this.recurringIntervalNode.set("disabled", true);
				this.set('recurring_interval', 1);
			}
		},
		
		start_date: null,
		_setStart_dateAttr: function (start_date) {
			this._set("start_date", start_date);
			this.startDateWidget.set("value", start_date ? str2timeAPI(start_date) : null);
		},

		end_date: null,
		_setEnd_dateAttr: function (end_date) {
			this._set("end_date", end_date);
			var val = end_date ? str2timeAPI(end_date) : null;
			this.endDateWidget.set('value', val);
		},

		start_time: null,
		_setStart_timeAttr: function (start_time) {
			this._set("start_time", start_time);
			this.startTimeWidget.set("value", start_time && !this.is_all_day ? str2timeAPI(start_time) : null);
		},
		
		end_time: null,
		_setEnd_timeAttr: function (end_time) {
			this._set("end_time", end_time);
			this.endTimeWidget.set("value", end_time && !this.is_all_day  ? str2timeAPI(end_time) : null);
		},

		due_date: null,
		_setDue_dateAttr: function (due_date) {
			this._set("due_date", due_date);
			var val = due_date ? str2timeAPI(due_date) : null;
			this.dueDateWidget.set("value", val);
		},
		
		/* Is all day */
		is_all_day: false,
		_setIs_all_dayAttr: function (is_all_day) {
			is_all_day = !!is_all_day;
			this._set("is_all_day", is_all_day);
			
			dojo.setAttr(this.isAllDayInput, "checked", is_all_day);
			
			this.startTimeWidget.set("disabled", is_all_day);
			this.endTimeWidget.set("disabled", is_all_day);
			
			if (is_all_day) {
				this.startTimeWidget.set("value", null);
				this.endTimeWidget.set("value", null);
			}
		},
		
		/* Sharing */
		hasSharing: true,
		_setHasSharingAttr: function (has) {
			this._set("hasSharing", has);
			dojo.toggleClass(this.sharingContainerNode, "hidden", !has);
		},
		
		/* Business */
		hasBusiness: true,
		_setHasBusinessAttr: function (has) {
			this._set("hasBusiness", has);
			dojo.toggleClass(this.sharingSharingNode, "hidden", !has);
			dojo.toggleClass(this.sharingParticipantsNode, "hidden", !has);
		},
		
		/* Recurring */
		recurring: 0,
		_setRecurringAttr: function (recurring) {
			this._set("recurring", recurring);
			this.recurringInput.attr('value', recurring, false);
			this.recurringInput.value = recurring;
			
			if (recurring > 0) {
				this.setRecurrence(true);
			} else {
				this.setRecurrence();
			}
		},
		
		recurring_interval:1,
		_setRecurring_intervalAttr: function(d) {
			this._set("recurring_interval", d);
			this.recurringIntervalHiddenInput.value = d;
		},
		
		_getRecurring_intervalAttr: function(){
			return this.recurringIntervalHiddenInput.value || 1;
		},
		
		recurring_end_date:null,
		_setRecurring_end_dateAttr: function(d){
			this._set('recurring_end_date',d);
			this.recurrenceEndDateWidget.set("value", d ? str2timeAPI(d) : null);
		},
		
		/* Reminder time type */
		reminder_time_type: "m",
		_setReminder_time_typeAttr: function (reminder_time_type){
			this._set("reminder_time_type", reminder_time_type);
			// this.reminderTimeNode.set('value', reminder_time_type);
		},
		
		reminder_time: 0,
		_setReminder_timeAttr: function (reminder_time){
			this._set("reminder_time", reminder_time);
			// this.reminderTimeNode.value = reminder_tirme;
		},

		reminder_enabled: false,

		_setReminder_enabledAttr: function (enabled) {
			if(!enabled){
				this.reminderTimeNode.set('value', 'none');
			}
			this._set("reminder_enabled", enabled);
		},
		
		/* Time tracking */
		time_track: false,
		_setTime_trackAttr: function (enabled) {
			this._set("time_track", enabled);
			
			dojo.setAttr(this.timeTrackingInput, "checked", enabled);
			this.timeTrackingEstInput.set('disabled', !enabled);
			dojo.toggleClass(this.timeTrackingContainerNode, "time-tracking-disabled", !enabled);
		},
		
		time_est: 0,
		_setTime_estAttr: function (time_est) {
			this._set("time_est", time_est || 0);
			
			var time_est_output = (time_est ? parseTimeString(time_est, "text") : '');
			this.timeTrackingEstInput.set('value', time_est_output);
		},
		
		/* Parent */
		parent: 0,
		_setParentAttr: function (parent) {
			this._set("parent", parent);

			var parentItem = hi.items.manager.item(parent);
			if(this.parent && this.parent >= 0 && parentItem && parentItem.taskItem.category !== hi.widget.TaskItem.CATEGORY_PROJECT){
				dojo.toggleClass(this.parentContainerNode, "hidden", true);
			}			
			// Set parentInput value

			var item = this.parentInput.store.query({id: parent});
			if (item.length > 0) {
				this.parentInput.set("displayedValue", item[0].name);	
			}
			
		},
		
		/* Priority */
		priority: 0,
		_setPriorityAttr: function (priority) {
			priority = priority || 20000;
			this._set("priority", priority);
			
			if (priority < 20000) {
				dojo.setAttr(this.priorityInputLow, "checked", true);
			} else if (priority < 30000) {
				dojo.setAttr(this.priorityInputMedium, "checked", true);
			} else {
				dojo.setAttr(this.priorityInputHigh, "checked", true);
			}
		},
		
		/* Shared */
		assignee: 0,
		_setAssigneeAttr: function(value) {
			this._set("assignee", value);
		},
		
		my: true,
		_setMyAttr: function (my) {
			//in case if "my" changes after "shared" attribute
			if (this.my != my) {
				this._set("my", my);
				this.set("shared", this.shared);
			}
		},
		
		shared: 0,
		_setSharedAttr: function(shared) {
			// #2205
			return;
			if (typeof(shared) != 'boolean') {
				shared = parseInt(shared);
				shared = !isNaN(shared) && shared > 0 ? true : false;
			}
			
			this._set("shared", shared);
			
			if (!this.get("hasBusiness") || (!shared && !this.my)) {
				dojo.addClass(this.sharingSharingNode, "hidden");
				// this.sharedInputPrivate.checked = true;
				this.sharingWidget.sharingDropDown.set('status', 1);
			} else {
				dojo.removeClass(this.sharingSharingNode, "hidden");
				// this.sharedInputShared.checked = shared ? true : false;
				// this.sharedInputPrivate.checked = shared ? false : true;
				this.sharingWidget.sharingDropDown.set('status', 0);
				// this.fillAssigneeList();
			}
		},

		permissions: null,
		_setPermissionsAttr: function(pms) {
			if(typeof pms === 'string'){
				pms = JSON.parse(pms);
				hi.items.manager.set(this.taskId, {permissions: pms}, true/*silent*/);
			}
			this._set('permissions', pms);

			if (!this.get("hasBusiness") || (!pms && !this.my)) {
				dojo.addClass(this.sharingSharingNode, "hidden");
				// this.sharedInputPrivate.checked = true;
				this.sharingWidget.sharingDropDown.set('status', 1);
			} else {
				dojo.removeClass(this.sharingSharingNode, "hidden");
				// this.sharedInputShared.checked = shared ? true : false;
				// this.sharedInputPrivate.checked = shared ? false : true;
				this.sharingWidget.sharingDropDown.set('status', 0);
				this.fillAssigneeList();
			}
			this.sharingWidget.fillSharingList(pms);
		},

		permission: 0,
		_setPermissionAttr: function(value){
			this._set('permission', value);

			if(this.task){
				this.task.permission = value;
			}
			
			if(value === Hi_Permissions.EVERYTHING && this.get('hasBusiness')){
				dojo.global.showElement(this.sharingSharingNode);
			}else{
				dojo.global.hideElement(this.sharingSharingNode);
			}
		},
		/* ----------------------------- CONSTRUCTOR ------------------------------ */
		constructor: function (/*Object*/args) {
			
		},
		
		buildRendering: function () {
			this.inherited(arguments);

			this.fillReminderTimeTypes();
			this.fillReminderText();
			
			this.set("hasBusiness", Project.businessId);
			this.set("hasSharing", CONSTANTS.accountLevel || CONSTANTS.emailConfirmed);
			this.set("reminder_time_type", Hi_Preferences.get("reminder_time_type"));
			
			//Message input max length
			dojo.setAttr(this.messageInput, "maxlength", CONSTANTS.taskMessageMaxlength);
			this.messageInput.set('placeHolder', hi.i18n.getLocalization('task.message_empty'));

			//Set prefered date format
			var datePattern = Hi_Preferences.get('date_format').replace('m', 'MM').replace('d', 'dd').replace('y', 'yyyy'),
				constraints = this.startDateWidget.get("constraints");
				
			constraints.datePattern = datePattern;
			this.startDateWidget.set("constraints", constraints);
			this.endDateWidget.set("constraints", constraints);
			this.dueDateWidget.set("constraints", constraints);
			this.recurrenceEndDateWidget.set('constraints',constraints);

			//Set prefered time format
			var timePattern = Hi_Preferences.get('time_format') == 12 ? "h:mm a" : "HH:mm";
			
			constraints = this.startTimeWidget.get("constraints");
			constraints.timePattern = timePattern;
			this.startTimeWidget.set("constraints", constraints);
			this.endTimeWidget.set("constraints", constraints);
			
			var newInput = this.participantDijit;
			
			this.assigneeDijit.set('labelFunc',dojo.hitch(this, this.renderLabel));
			dojo.connect(this.assigneeDijit,'format',this,'formatDisplayedAssigneeValue');
			
			this.participantDijit.set('labelFunc',this.renderLabel);
			dojo.connect(this.participantDijit,'format', this, 'formatDisplayedParticipantValue');
			
			// dojo.connect(Hi_Participant,'removePropertyParticipant',this,function(elem) {
			var cnt = dojo.connect(Hi_Participant,'removePropertyParticipant', lang.hitch(this, function(elem) {
				var noneElement = newInput.store.data.shift(),				// remove the first "None" element from the array of combobox to exclude it from sorting
					addNewPersonElement = newInput.store.data.pop(),		// #4403 remove the last "Add new person" option
					myselfElement,
					container = Hi_Participant._container,
					sd = newInput.store.data;		// store data

				if (!this.domNode) {
					return dojo.disconnect(cnt);
				}

				if (!this.domNode.contains(container)) {
					return false;
				}

				var participantId = elem.parentNode.id;
				if (participantId == Project.user_id) {
					sd.sort(function(a, b) {
						return a['name'].localeCompare(b['name']);
					});
					sd.unshift({
						name: Project.getMyself(),
						id: Project.user_id,
						image24src: dojo.global.getAvatar(48, null, true)
					});
				} else {
					/* Bug #4403 */
					if (sd.length > 0 && sd[0]['id'] == Project.user_id) {
						myselfElement = sd.shift();
					}
					var tmp = dojo.clone(Project.getFriend(participantId));
					if (!tmp.name || tmp.name.length <= 0) {
						tmp.name = tmp.firstName + ' ' + tmp.lastName;
					}
					newInput.store.data.push(tmp);

					sd.sort(function(a, b) {
						return a['name'].localeCompare(b['name']);
					});
					/* Bug #4403 */
					if (typeof myselfElement != 'undefined') {
						sd.unshift(myselfElement);
					}
				}
				sd.sort(function(a, b) {
					return a.name.localeCompare(b.name);
				});
				// put back "None" element to the array of combobox
				sd.unshift(noneElement);
				// #4403 add "New person" option to list
				sd.push(addNewPersonElement);
				
				if (Hi_Participant.getCurrItemParticipants().length <= 0) {
					dojo.query('.participantLabel').forEach(function(item) {
						dojo.removeClass(item,'participantLabelWithParticipants');
					});
				}
			}));


			aspect.after(this.reminderTimeNode, 'loadDropDown', lang.hitch(this, function(){
				var noneOption = this.reminderTimeNode.dropDown && this.reminderTimeNode.dropDown.getChildren();
				noneOption = noneOption.length && noneOption[0];

				if(noneOption){
					dojo.addClass(noneOption.domNode, 'dijitNoneOption');
				}
			}));

			this.parentInput.set('labelFunc', dojo.hitch(this, this.renderParentLabel));
			this.participantDijit.set('labelFunc', dojo.hitch(this, this.renderNewPersonLabel));
			// #4403
			this.assigneeDijit.set('labelFunc', dojo.hitch(this, this.renderNewPersonLabel));
			dojo.connect(this.parentInput,'format',this,'formatDisplayedParentValue');
			
			this.topLevelWrapper.id = 'itemEditForm';
			
			var datePlaceholder = (datePattern || 'date').toUpperCase();
			var timePlaceholder = (timePattern || 'time').toUpperCase();
			var placeholderAttr = this.hasNativePlaceholderSupport() ? 'placeholder' : 'placeHolder';
			
			this.startDateWidget.set(placeholderAttr, datePlaceholder);
			this.endDateWidget.set(placeholderAttr, datePlaceholder);
			this.dueDateWidget.set(placeholderAttr, datePlaceholder);
			this.startTimeWidget.set(placeholderAttr, timePlaceholder);
			this.endTimeWidget.set(placeholderAttr, timePlaceholder);
			
			if (this.recurringIntervalNode) {
				this.recurringIntervalNode.set('queryExpr', '*${0}*');
			}
			
			/* #3149 - this will cause dijit.form.Select styling to mimic some of the same actions
			 * on dijit.form.ComboBox
			 */
			if (this.recurringInput && this.recurringInput.domNode) {
				var buttonNode = dojo.query('td.dijitArrowButton',this.recurringInput.domNode)[0];
				if (buttonNode) {
					buttonNode.onmouseover = function() {
						dojo.addClass(this,'dijitSelectToComboBoxTheme');
					};
					buttonNode.onmouseout = function() {
						dojo.removeClass(this,'dijitSelectToComboBoxTheme');
					};
				}
			}

			/* Bug #4470 - New item form is collapsed after Enter key was pressed on Assign to field
			 *
			 * hack assignee widget here to make sure Enter key press event won't 
			 * submit the item edit form when the dropDown of the widget is opened.
			 *
			 */
			if(this.assigneeDijit){
				var __onKey = this.assigneeDijit._onKey;
				this.assigneeDijit._onKey = function(evt){
					var keys= dojo.keys;

					if(this._opened && evt.keyCode === keys.KEY_ENTER){
						event.stop(evt);
					}
					__onKey.apply(this, [evt]);
				};
			}
		},

		/* ----------------------------- Fill lists ------------------------------ */
		fillReminderTimeValue: function(){
			var enabled = this.get('reminder_enabled'),
				time = parseInt(this.get('reminder_time'), 10) || 0,
				type = this.get('reminder_time_type'), timesList;

			switch(type){
				case 'm':
					timesList = TaskItemEdit.ALERTTIMEMATRIX[0];
					break;
				case 'h':
					timesList = TaskItemEdit.ALERTTIMEMATRIX[1];
					break;
				case 'd':
					timesList = TaskItemEdit.ALERTTIMEMATRIX[2];
					break;
			}
			if(timesList.indexOf(time) < 0 && time > 0){
				time = timesList[0];
			}
			if(!enabled){
				this.reminderTimeNode.set('value', 'none');
			}else{
				this.reminderTimeNode.set('value', time + ',' + type);
			}
		},
		
		fillReminderText: function() {
			if (this.reminderHelperTextNode) {
				dojoQuery(this.reminderHelperTextNode).text(hi.i18n.getLocalization(this.category == hi.widget.TaskItem.CATEGORY_TASK ? 'task.before_due_start' : 'task.before_start'));
			}
		},

		fillReminderTimeTypes: function () {
			var input = this.reminderTimeNode,
				types = CONSTANTS.reminderTimeTypes,
				matrix = TaskItemEdit.ALERTTIMEMATRIX,
				i = 0,
				ii = types.length;

			if (input.getOptions().length === 0) {
				input.addOption({label: hi.i18n.getLocalization('task.none'), value: 'none'});
				input.addOption({label: 'At time of event', value: '0,m'});

				var valuelen = 0,
					values, j, label, value, unit;

				for (; i < ii; i++) {
					values = TaskItemEdit.ALERTTIMEMATRIX[i];
					valuelen = values.length;

					for(j = 0; j < valuelen; j++){
						unit =  types[i].title;
						if(!unit) continue;
						unit = parseInt(values[j], 10) === 1 ? unit.substr(0, unit.length - 1) : unit;
						label = values[j] + ' ' + unit;
						value = values[j] + ',' + types[i].id;
						input.addOption({label: label, value: value});
					}
				}
			}
		},

		/*
		 * Generate label (unit) for recurring interval, e.g., 'day', 'days', 'week', 'weeks', etc.
		 */
		generateRecurringIntervalLabel: function(interval) {
			var recurring = parseInt(this.recurringInput.value),
				label='',
				ret = [hi.i18n.getLocalization('task.recurring_every')];
			
			if (interval > 1) {
				ret.push(interval);
			}
			
			switch (recurring)
			{
				case 1:
					ret.push(interval > 1 ? hi.i18n.getLocalization('history.days') : hi.i18n.getLocalization('history.day'));
					break;
				case 2:
					ret.push(interval > 1 ? hi.i18n.getLocalization('history.weeks') : hi.i18n.getLocalization('history.week'));
					break;
				case 3:
					ret.push(interval > 1 ? hi.i18n.getLocalization('history.months') : hi.i18n.getLocalization('history.month'));
					break;
				case 4:
					ret.push(interval > 1 ? hi.i18n.getLocalization('history.years') : hi.i18n.getLocalization('history.year'));
					break;
			}
			return ret.join(' ');
		},
		
		fillRecurringIntervalList: function() {
			var input = this.recurringIntervalNode,
				intervalValue = parseInt(this.get('recurring_interval')) || 1,
				recurringValue = parseInt(this.get('recurring')) || 0;
			
			//Remove old options, add new ones
			input.store = new hi.store.Memory();
			
			for(var i = 1; i < 366; i++) {
				input.store.put({name: this.generateRecurringIntervalLabel(i), id: i});
			}
			
			//Restore previous value if possible
			var prev = input.store.get(intervalValue);
			if (prev && prev.name) {
				input.set('value', prev.name);
			}
		},
		
		fillProjectList: function () {
			// summary:
			//		Fill project drop down
			
			var projects = [].concat(hi.items.manager.get(function(x) {
					return x.category == 0 && x.permission >= Hi_Permissions.CHILDREN;
				})),	// only projects
				input = this.parentInput,
				option = null,
				value = this.parent || '';

			var sortOption = hi.store.Project.getProjectOrderConfiguration(true),
				attr, descending = false, tmp = [],
				sorting = Project.getSorting(2);

			//sync the sort in the project list with global sorting
			if (sorting === 2 || sorting === 5) {
				doOrder = true;
				projects.sort(function(a,b) {
					if (a.title < b.title)
						return -1;
					else if (a.title == b.title)
						return 0;
					else
						return 1;
				});
			} else {
				if (sortOption && sortOption.length > 0) {
					attr = sortOption[0].attribute;
					descending = sortOption[0].descending;
					projects.sort(function(a, b) {
						a = a[attr];
						b = b[attr];
						
						if (sortOption.hasOwnProperty('caseInsensitive') && !sortOption.caseInsensitive) {
							a = a[attr].toUpperCase();
							b = b[attr].toUpperCase();
						}
						if (a > b) {
							return descending? -1 : 1;
						} else if (a == b) {
							return 0;
						} else {
							return descending? 1 : -1;
						}
					});
				}
			}
			//Remove old options, add new ones
			this.parentInput.store = new hi.store.Memory();
			this.parentInput.store.put({name: hi.i18n.getLocalization("task.none"), id: ''});
			
			for(var i=0,ii=projects.length; i<ii; i++) {
				this.parentInput.store.put({name: projects[i].title, id: projects[i].id});
			}
			
			//New project item, see "handleParentChange" function
			this.parentInput.store.put({name: hi.i18n.getLocalization("project.create_new_project"), id: 'new-item'});
			
			//Restore previous value if possible
			var prev = input.store.get(value ? value : '');
			//If prev, then this item is in a project already; but now we need to make sure
			//We are not doing a new item in a project (i.e., the user has hit the 'add new item' button
			//on a project
			//In that case, we do not fool with this input dijit
			if (prev && prev.name && this.taskId>0) {
				input.set('value', prev.name);
			}
		},
		/* Not used if assignee select is a combobox
		fillAssigneeListOption: function (id, name, selected) {
			// summary:
			//		Add option to assignee list
			// id:Number
			//		Option value
			// name:String
			//		Option title
			// selected:Boolean
			//		Option is selected
			
			var option = new Option(name, id);
			this.assigneeInput.options[this.assigneeInput.options.length] = option;
		},
		*/
		/* Modified for assignee combobox
		fillAssigneeList: function () {
			// summary:
			//		Fill assignee list
			
			if (!('friends' in Project)) return;
			
			var taskId = this.taskId || 0,
				input = this.assigneeInput,
				default_value = Hi_Preferences.get('default_assign_user', 0, 'int') || 0,
				value = (this.isNew ? default_value : this.assignee) || 0,
				shared = this.shared;
			
			if (!Project.businessId || (!shared && !this.my)) shared = null; // ignore if user doesn't have option to select it
			
			for (var i = input.options.length - 1; i > 0; i--) {
				input.removeChild(input.options[i]);
			}
			// add myself to list
			input.options[input.options.length] = new Option(Project.getMyself(), Project.user_id);
			newInput.store.add({name:Project.getMyself(),id:Project.user_id});
			
			// add friends
			var friends = Project.friends,
				i = 0,
				imax = friends.length;
			
			for (; i < imax; i++) {
				id = friends[i].id;
				if (true||Project.canAssign(taskId, id, shared)) {
					if (true || friends[i].activeStatus || friends[i].waitStatus || friends[i].businessStatusInactive === true) {
						input.options[input.options.length] = new Option(Project.getFriendName(id, 2, true), id);
						tmp = dojo.clone(Project.getFriend(id));
						if (!tmp.name || tmp.name.length<=0)
						{
							tmp.name = tmp.firstName + ' ' + tmp.lastName;
						}
						newInput.store.add(tmp);
					}
				}
			}
			
			//New person item
			var separator = new Option('------------------------------', '-');
			separator.disabled = true;
			input.options[input.options.length] = separator;
			input.options[input.options.length] = new Option(hi.i18n.getLocalization('team_list.add_person'), 'new-item');
			this.assignee = input.value = value;
			this.assigneeHiddenInput.value = value;
			this.assigneeDijit.set('value',this.assigneeDijit.store.get(value).name);
		},
		*/
		/* For an assignee selection that is a combobox */
		fillAssigneeList: function () {
			// summary:
			//		Fill assignee list
			
			if (!('friends' in Project) && this.get('assignee') != Project.user_id) return;
			var taskId = this.taskId || 0,
				newInput = this.assigneeDijit,
				default_value = Hi_Preferences.get('default_assign_user', 0, 'int') || 0,
				value = (this.isNew && !this.assignee ? default_value : this.assignee) || 0,
				shared = taskId && hi.items.manager.get(taskId, 'shared');
			
			if (!Project.businessId || (!shared && !this.my)) shared = null; // ignore if user doesn't have option to select it
			
			/*
			for (var i = input.options.length - 1; i > 0; i--) {
				input.removeChild(input.options[i]);
			}
			*/
			
			// add None to the list
			/* #2162 newInput.store.put({name:'None',id:this.assignee});*/
			newInput.store.put({name:'None',id:0});
			
			var tmp;
			// add myself to list
			newInput.store.put({
				name: Project.getMyself(),
				id: Project.user_id, 
				image24src: dojo.global.getAvatar(48, null, true)
			});
			
			// add friends
			if ('friends' in Project) {
				var friends = Project.friends,
					i = 0,
					imax = friends.length;
				for (; i < imax; i++) {
					id = friends[i].id;
					if (Project.canAssign(taskId, id, shared)) {
						if (friends[i].activeStatus || !friends[i].waitStatus || friends[i].businessStatusInactive === true) {
							//input.options[input.options.length] = new Option(Project.getFriendName(id, 2, true), id);
							tmp = dojo.clone(Project.getFriend(id));
							//Must have a name attribute in order for store to return something the combobox can render
							if (!tmp.name || tmp.name.length <= 0) {
								tmp.name = tmp.firstName + ' ' + tmp.lastName;
							}
							tmp.image24src = dojo.global.getAvatar(48, id, true);
							newInput.store.put(tmp);
						}
					} else if (value == id) {
						// #1804: it is possible that currently task assigned to non-biz user (when Private checked).
						// and if Shared checked now non-biz user can not be assignee of the task.
						this.assignee = 0;
						value = 0;
					}
				}
			}
			
			// #4403 New person item
			newInput.store.put({name: hi.i18n.getLocalization("project.add_new_person"), id: "new-person"});
			/* Handled via a button that will appear when the combobox's store query return no results
			var separator = new Option('------------------------------', '-');
			separator.disabled = true;
			input.options[input.options.length] = separator;
			input.options[input.options.length] = new Option(hi.i18n.getLocalization('team_list.add_person'), 'new-item');
			this.assignee = input.value = value;
			*/
			this.assigneeHiddenInput.value = value;
			/* #2958 - the assignee might not be visible to the user; if, for example, the assignee has not yet
			 * accepted the team invitation
			 */
			if (this.assigneeDijit.store.get(value) && this.assigneeDijit.store.get(value).name)
				this.assigneeDijit.set('value', this.assigneeDijit.store.get(value).name);
		},

		/* Not used with participant combobox
		_fillParticipantList: function () {
			// summary:
			//		Fill participant list
			if (!this.hasBusiness) return;
			if (!('friends' in Project)) return;
			
			var taskId = this.taskId || 0,
				input = this.participantInput;
			
			for (var i = input.options.length - 1; i > 0; i--) {
				input.removeChild(input.options[i]);
			}
			
			var fillParticipantSelectOptions = function(id, name){
				opt = new Option(name, id);
				el.options[el.options.length] = opt;
			}
			// add none
			newInput.store.add({name:"None",id:0});
			
			// add myself to list
			input.options[input.options.length] = new Option(Project.getMyself(), Project.user_id);
			// add friends
			var friends = Project.friends,
				i = 0,
				imax = friends.length;
			
			for (; i < imax; i++) {
				id = friends[i].id;
				if (friends[i].activeStatus || friends[i].waitStatus || friends[i].businessStatusInactive === true) {
					input.options[input.options.length] = new Option(Project.getFriendName(id, 2, true), id);
				}
			}
			
			//New person item
			var separator = new Option('------------------------------', '-');
			separator.disabled = true;
			input.options[input.options.length] = separator;
			input.options[input.options.length] = new Option(hi.i18n.getLocalization('team_list.add_person'), 'new-item');
			
			input.value = "";
		},
		*/
		/* For a participant selection that is a combobox */
		fillParticipantList: function () {
			// summary:
			//		Fill participant list
			if (!this.hasBusiness) return;
			if (!('friends' in Project)) return;
			
			var taskId = this.taskId || 0,
				newInput = this.participantDijit;

			// add None to list
			newInput.store.put({name:"None",id:0});
			
			// add myself to list
			// #1422 get participants added to item in order to exclude them from the combobox
			var friendsAddedToItem = Hi_Participant.getItemParticipants(taskId);
			
			if (array.indexOf(friendsAddedToItem, Project.user_id) === -1) {
				newInput.store.put({
					name: Project.getMyself(),
					id: Project.user_id,
					image24src: dojo.global.getAvatar(48, null, true)
				});
			}
			// add friends
			var friends = Project.friends,
				tmp = null,
				i = 0,
				imax = friends.length;
				
			for (; i < imax; i++) {
				id = friends[i].id;
				if (friends[i].businessId && array.indexOf(friendsAddedToItem, id) === -1) {
					//input.options[input.options.length] = new Option(Project.getFriendName(id, 2, true), id);
					tmp = dojo.clone(Project.getFriend(id));
					//Must have a name attribute in order for store to return something the combobox can render
					if (!tmp.name || tmp.name.length <= 0) {
						tmp.name = tmp.firstName + ' ' + tmp.lastName;
					}
					tmp.image24src = dojo.global.getAvatar(48, id, true);
					newInput.store.put(tmp);
				}
			}
			// add "New person" option to list
			newInput.store.put({name: hi.i18n.getLocalization("project.add_new_person"), id: "new-person"});
			newInput.set('value','None');
		},
		
		parseTaskTitle: function () {
			// summary:
			//		Parse input title
			
			return parseTaskTitle.call(this);
		},
		
		
		/* ----------------------------- API ------------------------------ */
		isParentItemSharedPrivate: function(checkShared, checkParentCategory, exceptParentCategories) {
			var _checkParentCategory = typeof(checkParentCategory) == 'number';
			
			if (this.get('parent') && parseInt(this.get('parent')) > 0) {
				var parentItem = hi.items.manager.get(parseInt(this.get('parent')));
				if (parentItem && dojo.isArray(exceptParentCategories) && dojo.indexOf(exceptParentCategories, parentItem.category) != -1) {
					return false;
				}
				if (parentItem && (typeof(parentItem.shared) == 'boolean' ? (checkShared ? parentItem.shared : !parentItem.shared) : (checkShared ? parseInt(parentItem.shared) > 0 : parseInt(parentItem.shared) < 1)) &&
					(_checkParentCategory ? parentItem.category == checkParentCategory : true)) {
					
					return true;
				}
			}
			
			return false;
		},
		
		isParentItemShared: function(checkParentCategory, exceptParentCategories) {
			return this.isParentItemSharedPrivate(true, checkParentCategory, exceptParentCategories);
		},
		
		isParentItemPrivate: function(checkParentCategory, exceptParentCategories) {
			return this.isParentItemSharedPrivate(false, checkParentCategory, exceptParentCategories);
		},
		
		_getParentItem: function() {
			return this.get('parent') && parseInt(this.get('parent')) > 0 ? hi.items.manager.get(parseInt(this.get('parent'))) : null;
		},
		
		showHideReminderWidget: function() {
			var show = false;
			if (this.category === hi.widget.TaskItem.CATEGORY_TASK) {
				show = this.startDateWidget.get('value') || this.dueDateWidget.get('value') ? true : false;
			} else {
				show = this.startDateWidget.get('value') ? true : false;
			}
			this.reminderTimeNode.set("disabled", !show);
			if (!show && this.reminder_enabled) {
				// Disable reminder.
				this.set("reminder_enabled", false);
			}
		},
		
		expand: function () {
			// summary:
			//		Expand task and populate form with latest data
			//		Form is not updated with model (store) change because that will affect performance.
			
			dojo.removeClass(this.domNode, "hidden");
			this.task.domNode.title = "";
			
			// Copy attribute values we need from TaskItem
			var properties = this._copyProperties,
				key = null,
				attrs = {},
				task = this.task;
			var itemObj = null;
			if (this.task && this.task.id) {
				itemObj = hi.items.manager.get(this.task.id);
			}
			
			for (key in properties) {
				attrs[key] = task.get(key);
			}
			
			attrs.start_date = task.start_date; // date + time in API format
			attrs.end_date = task.end_date;   // date + time in API format
			attrs.start_time = task.start_date; // date + time in API format, time widget needs full date time
			attrs.end_time = task.end_date;   // date + time in API format, time widget needs full date time
			attrs.due_date = task.due_date;
			//New item start and end date
			if (attrs.taskId < 0 && !attrs.start_date) {
				var date = str2time(HiTaskCalendar.getCurrentDateStr(), "y-m-d"),
					now = hi.items.date.truncateTime(new Date());
				
				if (hi.items.date.compareDateTime(date, now) != 0) {
					attrs.start_date = date;
					attrs.is_all_day = true;
				}
			}
			
			attrs.hasStartDateTime = attrs.start_date;
			if (itemObj) {
				// #4143
				attrs.assignee = itemObj.assignee;
			}
			
			this.set(attrs);
			
			// Fill lists
			this.fillProjectList();
			this.fillAssigneeList();
			this.fillParticipantList();
			this.fillRecurringIntervalList();
			this.fillReminderTimeValue();
			
			Hi_Participant.fillPropertyParticipants();
			
			// Tags
			Hi_Tag.fillPropertyTags(this.taskId);
			
			//File upload
			if (this.taskId < 0) {
				FileUpload.reset(this.taskId);
			}
			
			// #2444
			// Preset expanded item 'shared' property.
			// Do this only if parent is not Project.
			if (this.isParentItemShared(null, [hi.widget.TaskItem.CATEGORY_PROJECT])) {
				this.set('shared', true);
			} else if (this.isParentItemPrivate(null, [hi.widget.TaskItem.CATEGORY_PROJECT])) {
				this.set('shared', false);
			}
			this._onPermissionsChange();

			// #3687
			// Make sure shared attr match model.
			this.set("shared", attrs.shared);
		},
		
		/* Feature #1806 */
		closeDateTimeDijits:function() {
			//if (this.startDateWidget) console.log('closeDateTimeDijits2',[this.startDateWidget]);
			if (this.startDateWidget) this.startDateWidget.closeDropDown();
			if (this.startTimeWidget) this.startTimeWidget.closeDropDown();
			if (this.endDateWidget) this.endDateWidget.closeDropDown();
			if (this.endTimeWidget) this.endTimeWidget.closeDropDown();
			if (this.dueDateWidget) this.dueDateWidget.closeDropDown();
		},
		
		resetRecurrenceIntervalForm:function() {
			this.set('recurring_interval', 1);
			this.set('recurring_end_date', null);
			this.set('recurring', 0);
			this.recurringIntervalNode.closeDropDown();
			this.recurringIntervalNode.set('disabled', true);
		},
		
		collapse: function () {
			/* Feature #1806 */
			this.closeDateTimeDijits();
			this.resetRecurrenceIntervalForm();
			dojo.addClass(this.domNode, "hidden");
			this.task.domNode.title = hi.i18n.getLocalization("project.click_to_view");
		},
		
		cancel: function () {
			// summary:
			//		Cancel editing
			/* Feature #1806 */
			this.closeDateTimeDijits();
			Project.hideProjectDialog(); // #2115
			Tooltip.close('frfind');
			/*
			if (this.isNew) {
				this.task.closeEdit();
			} else {
				this.task.closeEdit();
			}
			*/
			/* #2726 */
			if (this.isNew) hi.items.manager.remove(this.taskId);
			else item_DOM.closeEdit(this.taskId,this.task.eventdate||null);
		},
		
		/*#2766 */
		setSaveIntervalTimer: null,
		setSaveDeferred: null,
		setSaveChanged: null,
		save: function() {
			// summary:
			//		Save
			/*Feature #1806 */
			this.closeDateTimeDijits();
			var values = this.getFormValues(),
				changed = null,
				key = 0;
			
			// #2351 and #2444
			// Do not auto-cascade 'shared' attr if parent is Project.
			if (this.isParentItemShared(null, [hi.widget.TaskItem.CATEGORY_PROJECT])) {
				this.set('shared', 1);
				values.shared = 1;
			}
			
			if (this.category == hi.widget.TaskItem.CATEGORY_FILE && this.isNew) {
				//File upload
				FileUpload.get(this.taskId).upload();
			} else {
				if (this.validate(values)) {
					//Save data
					if (this.isNew) {
						values.priority =  Project.getMaxPriority(Math.max(0, Math.floor( values.priority/ 10000) - 1), 10)/* Feature #2152 */;
						/*
						 * #3362
						 * If this new item is a sub-item; get its parent widget and thus its eventdate if it is 
						 * a recurring parent item.  Then we can figure out whether or not this instance of the 
						 * parent item is completed or not
						 */
						var parentItem = hi.items.manager.item(values.parent),
							parent = values.parent||null,
							eventdate = null;

						if (parentItem && parentItem.category != hi.widget.TaskItem.CATEGORY_PROJECT && parent) {
							var parentId = this.task.domNode.parentNode.id||null;
							if (parentId && parentId.match(/^children_/)) {
								parentId = parentId.replace(/children_/,'itemNode_');
								parentWidget = hi.items.manager.getItemByNode(dojo.byId(parentId),parent);
								if (parentWidget && parentWidget.eventdate) eventdate = parentWidget.eventdate;
							}
						}
						
						this.task.set("removeOnCollapse", false);
						// #5133: we no longer use "shared" property.
						delete(values.shared);
						
						//Update cache manually + immediatelly
						//because there is no ID for new item which store could use to identify this item
						hi.items.manager.set(this.taskId, values);
						// For the saving throbber
						values.isSubItem = true;
						trackEvent(trackEventNames.ITEM_CREATE);
						
						//Save
						hi.items.manager.setSave(values).then(dojo.hitch(this, function (id) {
							/* #2766 */
							if (id) {
								var _id = id;
								/* #2315 */
								if (this.task && this.task.domNode) this.task.closeEdit();

								//When request is complete new item will be added, we can remove this one
								hi.items.manager.remove(this.taskId);
								/* Bug #2104 */
								var deferred = hi.items.manager.refresh();
								deferred.then(function() {
									var newItem = hi.items.manager.get(_id);
									
									if (newItem && newItem.parent) {
										if (parentItem) {
											var nChildren = parentItem.taskItem.children;

											if(nChildren < 0){
												nChildren = 0;
											}else{
												nChildren++;
											}
											item_DOM._setChildrenAttr(parentItem,nChildren);
											/*
											 * #3305
											 */
											if (values.parent && hi.items.manager.getIsCompleted(values.parent,eventdate)) 
											{
												hi.items.manager.setSave(_id,{completed:1});
											}
										}
									}
								});
							}

						}), dojo.hitch(this, function () {
							//On error show form, it's possible that item is already destroyed
							if (this.task) {
								this.task.expand();
								this.task.openEdit();
							}
						}));
					} else {
						changed = this.getChangedValues(values);
						
						//Check if there is something besides id
						for (key in changed) if (key != "id") break;
						if (key != "id") {
							// #2252: we can safely remove 'completed' property because this method does not even supposed
							// to change completeness of item.
							delete changed.completed;
							// #5133: we no longer use "shared" property.
							delete(changed.shared);
							var self = this;
							
							/* #2726 */
							//this.task.set(key,changed[key]);
							item_DOM._save(this.task,changed);
							
							trackEvent(trackEventNames.ITEM_MODIFY);
							/* #2766 */
							//hi.items.manager.setSave(changed);
							if (this.setSaveIntervalTimer) {
								clearInterval(this.setSaveIntervalTimer);
								if (this.setSaveDeferred) this.setSaveDeferred.cancel();
								this.setSaveChanged = dojo.mixin(this.setSaveChanged || {}, changed);
								//TODO: mixin change objects
							} else {
								this.setSaveChanged = changed;
							}
							var firePostResponse = firePOST({
								post: hi.items.manager.setSave,
								postArgs: [this.setSaveChanged]
							});
							//this.setSaveIntervalTimer = firePostResponse.intervaTimer;
							this.setSaveDeferred = firePostResponse.deferred;
							this.setSaveDeferred.then(function() {
								self._rememberPermissions();
							});
							this.setSaveIntervalTimer = firePostResponse.intervalTimer;
							// #2374
							if (this.task && /*this.task.domNode*/dojo.byId('itemNode_' + this.task.widgetId)) {
								item_DOM.closeEdit(this.task.id,this.task.eventdate);
							} else {
								if (this.task) this.task.closeEdit();
							}
							
						}
					}
					/* #2315 - apply only to new items; edited items widgets are destroyed and recreated again,
					 * to this.task.domNode is null, causing an error 
					this.task.closeEdit();
					*/
				}
			}
		},
		
		focusInput: function (name) {
			// summary:
			//		Focus input
			// name:
			//		Input name, by default will focus title
			
			name = name || "title";
			
			if (name) {
				switch (name) {
					case "start_date":
						this.startDateWidget.focus();
						this.startDateWidget.openDropDown();
						break;
					case "end_date":
						this.endDateWidget.focus();
						this.endDateWidget.openDropDown();
						break;
					case "due_date":
						this.dueDateWidget.focus();
						this.dueDateWidget.openDropDown();
						break;
					case "start_time":
						this.startTimeWidget.focus();
						this.startTimeWidget.openDropDown();
						break;
					case "end_time":
						this.endTimeWidget.focus();
						this.endTimeWidget.openDropDown();
						break;
					default:
						var input = dojo.query('textarea[name="' + name + '"], input[name="' + name + '"]', this.domNode);
						if (input.length) {
							input = input[0];
							// if (input && input.clientHeight > 0 && input.clientWidth > 0 && input.style.dislay !== 'none') {
								input.focus();
								input.setSelectionRange(input.value.length, input.value.length);
							// }
						}
						break;
				}
			}
		},
		
		getFormValues: function () {
			// summary:
			//		Returns all form values
			
			if (document.activeElement && document.activeElement.tagName == "INPUT" && document.activeElement.name == "title") {
				//Parse title
				this.set("title", this.parseTaskTitle());
			}
			
			var inputs = dojo.query('textarea[name], input[name], select[name]', this.domNode),
				i = 0,
				ii = inputs.length,
				values = {},
				name = null,
				type = null,
				tag = null,
				test_number = /^[0-9]+$/,
				widget = null;
			
			for (; i < ii; i++) {
				name = inputs[i].name;
				type = inputs[i].type;
				tag  = inputs[i].tagName;
				
				if (tag == "TEXTAREA" || tag == "SELECT" || type == "text"|| tag == "INPUT") {
					if (type != 'radio') {
						widget = dijit.byNode(inputs[i]);
						if(widget){
							values[name] = widget.get('value');
						}else{
							values[name] = inputs[i].value;
						}
					} else {
						if (inputs[i].checked) {
							values[name] = inputs[i].value;
						}
					}
					
					//Convert integers into numbers. Don't convert floats, because precision
					//could be lost (floating point after all)
					if (test_number.test(values[name]) && !(typeof(values[name]) == 'string' && values[name].replace(/0/g, '').length == 0)) {
						if (name == 'title' || name == 'message') {
							values[name] = values[name] + '';
						} else {
							values[name] = parseInt(values[name]);
						}
					}
					/* #2190? - recurring value needs to be int, so it is tested properly in TaskList.watchChanges */
					if (name == 'recurring') values[name] = parseInt(values[name]);
				} else if (type == "checkbox") {
					values[name] = inputs[i].checked ? 1 : 0;
				}
			}
			/* #2764 */
			// if (values.message ==  hi.i18n.getLocalization('task.message_empty')) values.message='';
			values.color = this.color;
			values.category = this.category;
			values.is_all_day = this.is_all_day;
			values.starred = this.starred;
			values.reminder_enabled = this.reminder_enabled;
			values.time_track = this.time_track;
			values.time_est = this.time_est;
			values.parent = this.parent;
			values.priority = this.priority;
			values.tags = Hi_Tag.getCurrItemTags(this.taskId).sort();
			values.recurring_interval = this.get('recurring_interval');
			/* #2104 - add user_id */
			values.user_id = Project.user_id;
			/* #2154, #2114 add completed, even though it's false, so that TaskList.order works properly
			 * for lists that have a completed sublist
			 */
			values.completed = 0;
			if (this.hasBusiness) {
				if (this.get('isNew') === true || this.get('permission') >= Hi_Permissions.SHARE) {
					var permsObj = this.sharingWidget.get('value') || [];
					// #5018: do not send myself in "permissions" object if i am a task owner.
					if ((this.get('isNew') === true || (this.taskId > 0 && hi.items.manager.get(this.taskId, 'user_id') == Project.user_id))) {
						// delete(permsObj[Project.user_id]);
						Hi_Actions.deletePermissionByUserId(permsObj, Project.user_id);
					}
					values.permissions = JSON.stringify(permsObj); // Mandatory!! Send even if empty.
				}
				values.participants = Hi_Participant.getCurrItemParticipants(this.taskId).sort();
			} else {
				delete(values.participants);
				delete(values.permissions);
			}
			
			//Reminder time
			// values.reminder_time = (parseInt(values.reminder_time, 10) || 0);
			// if (values.reminder_time_type == "h" && values.reminder_time % 24 == 0) {
			// 	values.reminder_time = values.reminder_time % 24;
			// }
			values.reminder_time = parseInt(this.reminder_time, 10) || 0;
			values.reminder_time_type = this.reminder_time_type;
			
			//Priority
			/* #2188 - Handle this in handlePriorityChange() if (this.isNew) { */
				//var level = Math.max(0, Math.floor(values.priority / 10000) - 1);
				//values.priority = Project.getMaxPriority(level)/* Feature #2152 */+10;
				//console.log('Now priority is',values.priority);
			/*}*/
			
			//ID
			if (this.isNew) {
				delete(values.id);
			} else {
				values.id = this.taskId;
				delete(values.user_id); // #4448
			}
			
			//Assign
			values.assignee = parseInt(values.assignee, 10) || 0;
			
			delete(values.participant_select);
			
			//Date and time
			delete(values.start_date);
			delete(values.end_date);
			delete(values.start_time);
			delete(values.end_time);
			delete(values.due_date);

			if (values.category == hi.widget.TaskItem.CATEGORY_FILE || values.category == hi.widget.TaskItem.CATEGORY_NOTE) {
				// Note and file don't have time properties
				delete(values.recurring);
				delete(values.is_all_day);
				delete(values.time_est);
				delete(values.time_track);
				delete(values.reminder_enabled);
				delete(values.reminder_time);
				delete(values.reminder_time_type);
			} else {
				var start_date = this.startDateWidget.get("value"),
					end_date = this.endDateWidget.get("value"),
					due_date = this.dueDateWidget.get("value"),
					start_time = this.startTimeWidget.get("value"),
					end_time = this.endTimeWidget.get("value"),
					recurring_end_date = parseInt(values.recurring)>0?this.recurrenceEndDateWidget.attr('value'):null,
					tmp = 0;
				
				start_date = start_date ? time2str(start_date, "y-m-d") : "";
				end_date   = end_date   ? time2str(end_date, "y-m-d") : "";
				due_date   = due_date   ? time2str(due_date, "y-m-d") : "";
				start_time = start_time ? time2str(start_time, "h:i:s") : "";
				end_time   = end_time   ? time2str(end_time, "h:i:s") : "";

				if (recurring_end_date) {
					values.recurring_end_date = time2strAPI(recurring_end_date);
				} else {
					delete values.recurring_end_date;
				}
				
				if (start_date && (start_date || end_date) && !start_time && !end_time) {
					values.is_all_day = true;
				}
				if (values.category === hi.widget.TaskItem.CATEGORY_TASK) {
					if (!start_time && !end_time && (start_date || end_date)) {
						values.is_all_day = true;
					}
				}
				
				if (start_date && start_time && !values.is_all_day) {
					values.start_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(start_date, start_time));
				} else if (start_date) {
					values.start_date = time2strAPI(HiTaskCalendar.getDateFromStr(start_date, true));
				} else if (start_time) {
					values.start_date = time2strAPI(HiTaskCalendar.getTimeFromStr(start_time));
				} else {
					values.start_date = "";
				}
				
				if (end_date && end_time && !values.is_all_day) {
					values.end_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(end_date, end_time));
				} else if (end_date) {
					values.end_date = time2strAPI(HiTaskCalendar.getDateFromStr(end_date));
				} else if (end_time) {
					values.end_date = time2strAPI(HiTaskCalendar.getTimeFromStr(end_time));
				} else {
					values.end_date = "";
				}

				// if (values.category === hi.widget.TaskItem.CATEGORY_TASK) {
				// 	if (due_date) {
				// 		values.end_date = values.due_date = time2strAPI(HiTaskCalendar.getDateFromStr(due_date));
				// 	} else {
				// 		values.end_date = values.due_date = '';
				// 	}
				// 	// revert #5208
				// 	// values.is_all_day = true;
				// 	// delete(values.start_time);
				// 	// delete(values.end_date);
				// 	// delete(values.end_time);
				// }
			}
			// #5133: we no longer use "shared" property.
			delete(values.shared);
			
			return values;
		},
		
		_isPermissionsChanged: function(oldP, newP) {
			var originps = typeof oldP === 'object'? oldP : JSON.parse(oldP || '[]'),
				currentps = typeof newP === 'object'? newP : JSON.parse(newP || '[]'),
				changed = false, pskey;

			// if (originps.length !== currentps.length) return true;

			array.forEach(originps, function(item) {
				var id = item.principal,
					ol = item.level,	// old level
					nl = Hi_Actions.getPermissionByUserId(currentps, id);	// new level

				if ('' + id === '' + Project.user_id) return;
				if (id.indexOf && id.indexOf('-') >= 0) return;

				if (ol !== nl) changed = true;
			});

			array.forEach(currentps, function(item) {
				var id = item.principal,
					nl = item.level,	// new level
					ol = Hi_Actions.getPermissionByUserId(originps, id);	// old level

				if ('' + id === '' + Project.user_id) return;
				if (id.indexOf && id.indexOf('-') >= 0) return;

				if (ol !== nl) changed = true;
			});

			return changed;
		},

		getChangedValues: function (values) {
			// summary:
			//		Returns only those values which has changed
			// values:Object
			//		All values object, optional
			
			values = values || this.getFormValues();
			
			var original = this.task.getRawData(),
				changed = {},
				key = '';

			for (key in values) {
				if (values[key] != original[key]) {
					if ((original[key] === null || original[key] === undefined) && !values[key]) continue;
					
					if (key === "end_date" || key === "start_date" || key === 'due_date') {
						//Check by converting into Date, because of timezone added to the time
						if (original[key] && values[key] && hi.items.date.compareDateTime(str2timeAPI(original[key]), str2timeAPI(values[key])) === 0) {
							continue;
						}
					} else if (key === 'reminder_time_type') {
						// if (values.reminder_enabled == (original.reminder_enabled || false)) continue;
						if (!values.reminder_enabled) continue;
					} else if(key === 'permissions'){
						var pschanged = this._isPermissionsChanged(original.permissions, values.permissions);
						if(!pschanged) continue;
					} else if (dojo.isArray(values[key]) && dojo.isArray(original[key])) {
						if (values[key].length == original[key].length) {
							//Quickly compare arrays by converting them into strings
							//since we have only tags, assign and participants as arrays
							//and they contain only strings or numbers
							var a = [].concat(values[key]).sort().join(),
								b = [].concat(original[key]).sort().join();
							
							if (a == b) continue;
						}
					} else if ((dojo.isArray(values[key]) && !values[key].length || dojo.isArray(original[key]) && !original[key].length)) {
						//Was or is empty array, which actually is the same as now
						continue;
					}
					
					changed[key] = values[key];
				}
			}
			
			if (!this.isNew) {
				//Always add id for existing tasks
				changed.id = this.taskId;
			}
			
			return changed;
		},
		
		/* Bug #2151 controls when to popup dialog if title not correctly specified */
		validate: function (values, silent) {
			// summary:
			//		Validate entered values

			/*Bug #3684 - title with value 'Enter item name' should also be valid title, no need to compare with defaultTitle*/
			if (!values.title || values.title == 'undefined' /* #2726 */ || values.title.trim().length === 0) {
				this.focusInput("title");
				if (!silent) Project.alertElement.showItem(14,{task_alert:'Please specify a title'});
				return false;
			}

			if (values.title.length > CONSTANTS.taskTitleMaxLength && this.category !== hi.widget.TaskItem.CATEGORY_FILE) {
				this.focusInput("title");
				if (!silent) Project.alertElement.showItem(14,{task_alert:hi.i18n.getLocalization("task.title_max_size").replace("%d", CONSTANTS.taskTitleMaxLength)});
				return false;
			}
			
			if (values.message && values.message.length > CONSTANTS.taskMessageMaxLength) {
				this.focusInput("message");
				if (!silent) Project.alertElement.showItem(14,{task_alert:hi.i18n.getLocalization("task.message_max_size").replace("%d", CONSTANTS.taskMessageMaxLength)});
				this.set('message', values.message.substring(0, CONSTANTS.taskMessageMaxLength));
				return false;
			}
		
			if (this.category === hi.widget.TaskItem.CATEGORY_EVENT) {
				if (!values.start_date || !values.end_date) {
					if (!silent) {
						Project.alertElement.showItem(14, {
							task_alert: 'Please specify start date & end date'
						});

						return false;
					}
				}
			}
			/* Bug #2076 - implement what is commented out below; plus validate start and end date*/
			//validate start date
			if (!this.startDateWidget.validate()) {
				Project.alertElement.showItem(14,{task_alert:'Starting date format is invalid.'});
				return false;
			}
			
			//validate end date
			if (!this.endDateWidget.validate()) {
				Project.alertElement.showItem(14,{task_alert:'Ending date format is invalid.'});
				return false;
			}
			
			//validate start time
			if (!this.startTimeWidget.validate()) {
				Project.alertElement.showItem(14,{task_alert:'Starting time format is invalid.'});
				return false;
			}
			
			//validate end time
			if (!this.endTimeWidget.validate()) {
				Project.alertElement.showItem(14,{task_alert:'Ending time format is invalid.'});
				return false;
			}
			
			var recurring = values.recurring;
			if (recurring > 0) {
				if (!this.recurrenceEndDateWidget.validate()) {
					Project.alertElement.showItem(14,{task_alert:'End of recurrence date is not valid'});
					return false;
				}
				
				var start_date = values.start_date;
				var end_date = values.end_date;
				var recurring_end_date = values.recurring_end_date;
				var dateEndCalculated;

				//The widgets above will validate to true if they are left blank; so now we need to see if they have values in them;
				//If they do, they're valid
				if (!start_date){
					Project.alertElement.showItem(14,{task_alert:'Start date is mandatory field for repeating tasks'});
					return false;
				}

				if (end_date) {
					//start_date and end_date have the time component added to them if startTimeWidget or endTimeWidget have
					//valid values specified
					var errorMessageDuration = 'Task duration must be shorter than repeating period';
					// recurring daily
					if (recurring == 1) {
						dateEndCalculated = dojo.date.add(new Date(start_date),'day',1);
						if(new Date(end_date) > dateEndCalculated){
							Project.alertElement.showItem(14,{task_alert:errorMessageDuration});
							return false;
						}
					}

					// recurring WEEKLY
					if(recurring == 2){
						dateEndCalculated = dojo.date.add(new Date(start_date),'week',1);
						if(new Date(end_date) > dateEndCalculated){
							Project.alertElement.showItem(14,{task_alert:errorMessageDuration});
							return false;
						}
					}
					
					// recurring MONTHLY
					if(recurring == 3){
						dateEndCalculated = dojo.date.add(new Date(start_date),'month',1);
						if(new Date(end_date) > dateEndCalculated){
							Project.alertElement.showItem(14,{task_alert:errorMessageDuration});
							return false;
						}
					}
					
					// recurring YEARLY
					if(recurring == 4){
						dateEndCalculated = dojo.date.add(new Date(start_date),'year',1);
						if(new Date(end_date) > dateEndCalculated){
							Project.alertElement.showItem(14,{task_alert:errorMessageDuration});
							return false;
						}
					}
					
				}
				
				//Validate end of recurrence date
				if (new Date(recurring_end_date) < new Date(start_date))
				{
					Project.alertElement.showItem(14,{task_alert:'End of recurrence must be occur after the item\'s start date'});
					return false;
				}
				
				//Validate recurrence interval
				if (!values.recurring_interval.toString().match(/\d+/))
				{
					Project.alertElement.showItem(14,{task_alert:'Recurrence interval is not a number'});
					return false;
				}
				if (parseInt(values.recurring_interval) < 1 || parseInt(values.recurring_interval) > 365 )
				{
					Project.alertElement.showItem(14,{task_alert:'Minimum recurring interval value is 1. Maximum recurring interval value is 365'});
					return false;
				}

			}
			
			return true;
		},
		
		
		/* ----------------------------- DATE TIME API ------------------------------ */
		adjustRecurrenceEndDate: function(recurrence_end_date, start_date, recurring) {
			/*
			 * Summary:
			 * Shift recurring end date if it happens to be the case
			 * that the start time is shifted before its current position
			 * 
			 * Input:
			 * recurrence_end_date Date object corresponding to items's current recurring end date time || null
			 * start_date Date object corresponding to an items's current start date time
			 * recurring integer corresponding to type of recurrence
			 */
			if (!start_date) return;
			if (!start_date) start_date = new Date();

			if (!recurrence_end_date || recurrence_end_date.getTime() < start_date.getTime()) {
				recurrence_end_date = start_date;
				if (recurring != 4) {
					recurrence_end_date.setYear(start_date.getFullYear() + 1);
				} else {
					recurrence_end_date.setYear(start_date.getFullYear() + 10);
				}
				this.recurrenceEndDateWidget.attr('value',recurrence_end_date);
			}
		},
		
		setRecurrenceEndDate: function (start_date, recurring) {
			/*
			 * Summary method connected to the recurring end date drop down on the item edit form
			 */
			if (!start_date) start_date = new Date();
			this.adjustRecurrenceEndDate(null, start_date, recurring);
		},
		
		/**
		 * #5037 and #5120
		 */
		_rememberPermissions: function(id) {
			if (Project.businessId) {
				id = id || this.taskId;
				var item = hi.items.manager.get(id),
					checkbox = this.rememberPermissionCheckbox, t = this;
				// console.log
				if (item) {
					var pms = item.permissions || [];
					pms = typeof pms === 'object' ? JSON.stringify(pms) : pms;
					Hi_Preferences.set('default_sharing_permission_last', pms);
					Hi_Preferences.send(function() {
						// if (t.rememberPermission.style.display !== 'none' && t.rememberPermissionCheckbox.checked) {
						if (t.doRememberPermission) {
							Hi_Preferences.set('default_new_item_use_last', 'true');
							Hi_Preferences.send();
						}
					});
				}
			}
		},
		
		validateDateTimeInput: function(name) {
			// summary:
			//		Validate input value and adjust other input values accordingly
			// name:
			//		Input/attribute name
			
			var start_date = hi.items.date.truncateTime(this.startDateWidget.get("value")),
				end_date = hi.items.date.truncateTime(this.endDateWidget.get("value")),
				due_date = hi.items.date.truncateTime(this.dueDateWidget.get("value")),
				recurrence_end_date = hi.items.date.truncateTime(this.recurrenceEndDateWidget.attr('value')),
				start_time = this.startTimeWidget.get("value"),
				end_time = this.endTimeWidget.get("value"),
				o_start_date = start_date,
				o_end_date = end_date,
				o_due_date = due_date,
				o_start_time = start_time,
				o_end_time = end_time,
				is_all_day = this.is_all_day,
				recurring = parseInt(this.recurringInput.value);
			
			switch (name) {
				case "start_date":
					if (!start_date || typeof(start_date.getMonth) == 'undefined' || isNaN(start_date.getMonth()) || start_date.getFullYear() < 1900) {
						end_date = null;
						start_time = null;
						end_time = null;
						this.startDateWidget.set('value', null);
					} else if (end_date) {
						if (start_date.getTime() > end_date.getTime()) {
							end_date = new Date(start_date);
						}
						if (start_date.getTime() == end_date.getTime() && start_time) {
							if (!end_time || hi.items.date.compareTime(start_time, end_time) == 1) {
								end_time = hi.items.date.addHours(start_time, 1);
							}
						}
					}
					if (due_date) {
						if (start_date && start_date.getTime() > due_date.getTime()) {
							due_date = new Date(start_date);
						}
					}
					
					if (recurring > 0) this.adjustRecurrenceEndDate(recurrence_end_date, start_date,recurring);
					break;
				case "end_date":
					if (!end_date || typeof(end_date.getMonth) == 'undefined' || isNaN(end_date.getMonth()) || end_date.getFullYear() < 1900) {
						end_time = null;
						this.endDateWidget.set('value', null);
					} else {
						if (this.category !== hi.widget.TaskItem.CATEGORY_TASK) {	// Bug #5845
							if (!start_date || start_date.getTime() > end_date.getTime()) {
								start_date = new Date(end_date);
							}
							if (start_date.getTime() == end_date.getTime()) {
								if (start_time && !end_time) {
									end_time = hi.items.date.addHours(start_time, 1);
								} else if (!start_time && end_time && this.category !== hi.widget.TaskItem.CATEGORY_TASK) {
									start_time = hi.items.date.addHours(end_time, -1);
								} else if (start_time && end_time) {
									if (hi.items.date.compareTime(start_time, end_time) == 1) {
										start_time = hi.items.date.addHours(end_time, -1);
									}
								}
							}
						}
					}
					if (recurring > 0) this.adjustRecurrenceEndDate(recurrence_end_date, start_date, recurring);
					break;
				case "due_date":
					if (!due_date || typeof(due_date.getMonth) == 'undefined' || isNaN(due_date.getMonth()) || due_date.getFullYear() < 1900) {
						// end_time = null;
						this.dueDateWidget.set('value', null);
					} else {
						if (start_date && (start_date.getTime() > due_date.getTime())) {
							start_date = new Date(due_date);
						}
					}
					if (recurring > 0) this.adjustRecurrenceEndDate(recurrence_end_date, start_date,recurring);
					break;
				case 'recurrence_end_date':
					this.adjustRecurrenceEndDate(recurrence_end_date, start_date,recurring);
					return;
					break;
				case "start_time":
					if (!start_time) {
						end_time = null;
					} else {
						if (!start_date) {
							start_date = hi.items.date.truncateTime(new Date());
						} else if (end_date) {
							if (start_date.getTime() == end_date.getTime()) {
								if (!end_time || hi.items.date.compareTime(start_time, end_time) == 1) {
									end_time = hi.items.date.addHours(start_time, 1);
								}
							}
						}
					}
					break;
				case "end_time":
					if (!end_time) {
						if (start_date && end_date && start_date.getTime() == end_date.getTime()) {
							start_time = null;
						}
					} else {
						if (!end_date) {
							if (start_date) {
								end_date = hi.items.date.truncateTime(start_date);
							} else {
								end_date = hi.items.date.truncateTime(new Date());
							}
						}
						if (this.category !== hi.widget.TaskItem.CATEGORY_TASK) {
							if (!start_date) {
								start_date = hi.items.date.truncateTime(new Date());
							}
							if (start_date.getTime() == end_date.getTime()) {
								if (!start_time || hi.items.date.compareTime(start_time, end_time) == 1) {
									start_time = hi.items.date.addHours(end_time, -1);
								}
							} else if (!start_time) {
								start_time = new Date(end_time);
							}
						} else {
							// Bug #5845
							// setting end date/time should not trigger start date/time change
							// in task, but it should sometimes
							if (start_date) {
								if (start_date.getTime() == end_date.getTime()) {
									if (!start_time || hi.items.date.compareTime(start_time, end_time) == 1) {
										start_time = hi.items.date.addHours(end_time, -1);
									}
								} else if (!start_time) {
									start_time = new Date(end_time);
								}
							}
						}
					}
					break;
			}
			
			// Update widget values
			if ((!start_date && o_start_date) || (start_date && !o_start_date /* #5736 *//*this.category != hi.widget.TaskItem.CATEGORY_TASK*/) || (start_date && o_start_date && hi.items.date.compareDateTime(start_date, o_start_date) !== 0)) {
				this.startDateWidget.set("value", start_date);
			}
			if ((!end_date && o_end_date) || (end_date && !o_end_date) || (end_date && o_end_date && hi.items.date.compareDateTime(end_date, o_end_date) !== 0)) {
				this.endDateWidget.set("value", end_date);
			}
			if ((!due_date && o_due_date) || (due_date && !o_due_date) || (due_date && o_due_date && hi.items.date.compareDateTime(due_date, o_due_date) !== 0)) {
				this.dueDateWidget.set("value", due_date);
			}
			if ((!start_time && o_start_time) || (start_time && !o_start_time) || (hi.items.date.compareTime(start_time, o_start_time) != 0)) {
				this.startTimeWidget.set("value", is_all_day ? null : start_time);
			}
			if ((!end_time && o_end_time) || (end_time && !o_end_time) || (hi.items.date.compareTime(end_time, o_end_time) != 0)) {
				this.endTimeWidget.set("value", is_all_day ? null : end_time);
			}
		},
		
		/* ----------------------------- EVENT BINDINGS ------------------------------ */
		
		handleStarredClick: function (event) {
			this.set("starred", !this.starred);
		},

		handleTitleChange: function (event) {
			this.set("title", this.parseTaskTitle());
		},

		handleTimeEstimateFocus: function() {

		},
		// reminder events
		handleAddReminderClick: function(evt) {
			console.log('in handle add event reminder');
		},

		handleReminderChange: function(value){
			if(value === 'none'){
				this.set('reminder_enabled', false);
				// this.set('reminder_time', 0);
				// this.set('reminder_time_type', 0);
			}else{
				if(!/,/.test(value)) return;
				
				var time, type,
					frags = value.split(',');

				time = parseInt(frags[0], 10);
				type = frags[1];

				// console.log(time, type);
				this.set('reminder_enabled', true);
				this.set('reminder_time', time);
				this.set('reminder_time_type', type);
			}

			if(this.reminderTimeNode.textDirNode) this.formatDisplayedLabel(value, this.reminderTimeNode.textDirNode);
		},
		
		/* Message / description */
		handleMessageFocus: function () {
			this.handleMessageKeyUp();
			/* #2764 dojo.addClass(this.messageLabelNode, "hidden");*/
			if (this.messageInput.value == hi.i18n.getLocalization('task.message_empty')){
				this.messageInput.value = '';
			}
			dojo.removeClass(this.messageInput,'placeHolder');
			dojo.addClass(this.messageInput,'inputText');
		},
		handleMessageBlur: function () {
			var message = this.messageInput.value.trim();
			
			if (!message || message=="" || message.length<=0 || message == hi.i18n.getLocalization('task.message_empty')) {
				/*#2764 dojo.removeClass(this.messageLabelNode, "hidden");*/
				this.messageInput.value = hi.i18n.getLocalization('task.message_empty');
				
				dojo.addClass(this.messageInput,'placeHolder');
				dojo.removeClass(this.messageInput,'inputText');
			}
		},
		handleMessageKeyUp: function (event) {
			var max = CONSTANTS.taskMessageMaxlength,
				message = hi.i18n.getLocalization("task.message_max_size").replace("%d", max);
			
			if (!ismaxlength(this.messageInput, false, false, false, message)) {
				if (event) {
					dojo.stopEvent(event);
				}
			}
			// this.messageAutoSize();
		},
		
		messageAutoSize: function() {
			this.messageInput.style.height = 'auto';
			this.messageInput.style.height = this.messageInput.scrollHeight.toString() + 'px';
		},
		
		handleReminderEnabledClick: function (event) {
			this.set("reminder_enabled", value);
		},
		
		handleCategoryClick: function (event) {
			var target = event.target,
				type = parseInt(dojo.getAttr(target, "data-type"), 10);
			
			if (type) {
				this.set("type", type);
			}
			
			dojo.stopEvent(event);
		},
		
		handleColorSelection: function (event) {
			var target = event.target.tagName == "SPAN" ? event.target.parentNode : event.target,
				color = parseInt(dojo.getAttr(target, "data-color"), 10);
			
			this.set("color", color);
		},
		
		handleTimeTrackingInputClick: function (event) {
			this.set("time_track", !this.time_track);
		},
		
		handleTimeTrackingEstimateChange: function (event) {
			var time_est = parseTimeString(this.timeTrackingEstInput.get('value'));
			this.set("time_est", time_est);
		},
		
		//newProjectDialogConnect:null,
		handleParentChange: function (event) {

			var input = this.parentInput;

			var id = this.parent;

			var item = this.parentInput.get("item");

			if (item) {
				id = item.id;
			}

			// Test for new project
			var items = input.store.query({id: id});

			if (items.length && items[0].id == 'new-item') {
				//New project popup
				if (this.parent == 0 || !this.parent) {
					input.set('value', input.store.get('').name);
				} else {
					var pinp = input.store.get(this.parent);
					input.set('value', pinp ? pinp.name : input.store.get('').name);
				}
				
				if (Project.checkLimits('TRIAL_PROJECTS')) {
					Project.showProjectDialog(/*Bug #1990 */null, input.domNode, {orient: ['after-centered']});
				}
			} 
			else {
				if (items.length) {
					var newId = parseInt(items[0].id);
					if (!isNaN(newId) && newId > 0) {
						this.set("parent", newId);
						this.formatDisplayedLabel(newId, this.parentInput.id);
					} else {
						this.set("parent", '');
						this.formatDisplayedLabel('', this.parentInput.id);
					}
				} else {
					this.set("parent", '');
					this.formatDisplayedLabel('', this.parentInput.id);
				}
			}
		},
		
		/**
		 * #2811: Clears the combobox if parent is not set yet.
		 */
		handleParentClick: function(event) {
			if (!this.get('parent')) {
				var input = dojo.query('input.dijitInputInner', this.parentInput.domNode);
				if (input.length) {
					input[0].value = '';
				}
			}
		},
		
		/**
		 * #2811: Sets 'none' value to combobox if parent was not chosen and combobox closed.
		 */
		handleParentBlur: function() {
			if (dojo.isFF) {
				this.parentInput.textbox.setSelectionRange(0, 0);
			}
			if (!this.get('parent')) {
				var noneItem = this.parentInput.store.get('') || this.parentInput.store.get(0);
				var input = dojo.query('input.dijitInputInner', this.parentInput.domNode);
				if (input.length && noneItem && noneItem.name) {
					input[0].value = noneItem.name;
				}
			}
		},
		
		handleSharedChange: function (event) {
			var shared = this.sharedInputShared.checked;
			this.set("shared", shared);
		},
		
		/*
		 * #3149 - Try to remove focus after an item for dijit.form.Select has been clicked on
		 */
		changeRecurrence:function() {
			if (this.recurringInput && this.recurringInput.domNode) {
				dojo.removeClass(this.recurringInput.domNode,'dijitSelectFocused');
			}
			this.setRecurrence();
		},
		
		/*
		 * Set up recurring interval and recurring end date widgets on form based on changed value of recurring option
		 */
		setRecurrence: function(edit) {
			if (this.recurringInput) {
				var recurring = parseInt(this.recurringInput.attr('value'), 10);//parseInt(this.recurringInput.value);
				if (recurring > 0) {
					this.recurringIntervalNode.set('disabled', false);
					this.recurringInput.set('disabled', false);
					if (dojo.hasClass(this.recurrence_details_wrapper,'hidden')) this.showRecurrenceDetails(recurring,edit);
					this._set("recurring", recurring);
				} else {
					this.recurringIntervalNode.set('disabled', true);
					//this.hideRecurrenceEndDateWidget();
					this.hideRecurrenceDetailsWidget();
					this.set('recurring_interval', 1);
					this._set("recurring", 0);
				}
				
				this.fillRecurringIntervalList();
			}
		},

		
		/*
		 * Try to animate the appearence of the recurring end date widget
		 */
		showRecurrenceDetails:function(recurring, edit) {
			var nodes = [this.recurrence_details_wrapper, this.recurrence_end_date_field_wrapper];
			var self = this,
				i = 0,
				afterEndCallback = function() {
					if (!self.get('recurring_end_date') || recurring == 4) {
						var start_date = self.startDateWidget.get("value") ? hi.items.date.truncateTime(self.startDateWidget.get("value")) : hi.items.date.truncateTime(new Date());
						self.setRecurrenceEndDate(start_date, recurring);
					} else if (self.recurrenceEndDateWidget) {
						self.recurrenceEndDateWidget.attr('value', self.get('recurring_end_date'));
					}
				};

			for (i = 0; i < nodes.length; i++) {
				dojo.removeClass(nodes[i], "hidden");
			}
			
			this.recurrenceEndDateWidget.set('disabled', false);
			if (supportsCSS3Transitions()) {
				for (i = 0; i < nodes.length; i++) {
					css3fx.fade(nodes[i], {
						'in': true,
						duration: CONSTANTS.animationDuration.groupFadeMs,
						direction: 1,
						onAfterEnd: afterEndCallback
					}).play();
				}
			} else
			{
				if(!this.get('recurring_end_date') || recurring == 4){
					var start_date = self.startDateWidget.get("value")?hi.items.date.truncateTime(self.startDateWidget.get("value")):hi.items.date.truncateTime(new Date());
					self.setRecurrenceEndDate(start_date, recurring);
				}else{
					this.recurrenceEndDateWidget.attr('value',this.get('recurring_end_date'));
				}
			}
		},
		
		/*
		 * Hide recurring end date widget
		 */
		hideRecurrenceEndDateWidget: function(){
			var node = this.recurrence_end_date_field_wrapper;
			dojo.addClass(node, "hidden");
			//this.recurrenceEndDateWidget.attr('value',null);
			this.recurrenceEndDateWidget.set('disabled',true);
		},
		
		hideRecurrenceDetailsWidget: function(){
			dojo.addClass(this.recurrence_details_wrapper, "hidden");
			dojo.addClass(this.recurrence_end_date_field_wrapper, "hidden");
			//this.recurrenceEndDateWidget.attr('value',null);
			this.recurrenceEndDateWidget.set('disabled',true);
		},
		
		/*
		 * Callback to changing recurring interval value
		 */
		handleRecurringIntervalChange: function(event) {
			var input = this.recurringIntervalNode,
				value = input.get('value'),
				recurring = this.recurringInput.get('value');
			
			var items = input.store.query({name: value});
			if (items.length) {
				var newId = parseInt(items[0].id);
				if (!isNaN(newId) && newId > 0 && newId < 366) {
					this.set("recurring_interval", newId);
				} else {
					this.set("recurring_interval", 1);
				}
			} else {
				this.set("recurring_interval", 1);
			}
			
			if (!this.get('recurring_end_date') || (recurring && recurring == 4)) {
				var start_date = this.startDateWidget.get("value") ? hi.items.date.truncateTime(this.startDateWidget.get("value")) : hi.items.date.truncateTime(new Date());
				this.setRecurrenceEndDate(start_date, this.recurringInput.value);
			} else {
				this.recurrenceEndDateWidget.attr('value', this.get('recurring_end_date'));
			}
		},
		
		handleRecurringIntervalBlur: function() {
			var node = dojo.query('.dijitHasDropDownOpen',this.recurringIntervalNode.domNode)[0];
			if (node) dojo.removeClass(node,'dijitHasDropDownOpen');
		},
		
		/* Callback from friendsStore to indicate whether or not any results were
		 * found.  If no results found, display 'New person...' button
		 */
		_checkAssigneeSearchResults: function(results) {
			if (!results||results.length <= 0){
				dojo.style(this.newAssigneeBtn,'display','');
			} else {
				dojo.style(this.newAssigneeBtn,'display','none');
			}
		},
		
		/* Callback from friendsStore to indicate whether or not any results were
		 * found.  If no results found, display 'New person...' button
		 */
		checkParticipantSearchResults:function(results) {
			if (!results || results.length <= 0) {
				dojo.style(this.addNewParticipantBtn,'display','');
			} else {
				dojo.style(this.addNewParticipantBtn,'display','none');
			}
		},
		
		//callback from 'New person..' button click
		handleAddNewAssignee:function() {
			var self = this;
			var newInput = this.assigneeDijit.domNode;
			var person_fullname = dijit.byId('person_fullname');
			var person_email = dijit.byId('person_email');

			if (person_fullname) person_fullname.set('value', '');
			if (person_email) person_email.set('value', '');
			Tooltip.open('frfind', 'search: ;', newInput, null, null, 'frfind_0');
			/* Feature #1721 */
			openTab('frfind', 0, 0, {});
			//Delay resetting widget, otherwise opening the tooltip may not fire
			setTimeout(function() {self.assigneeDijit.set('value', 'None', false);},1000);
			
		},
		/* Not used for assignee combobox
		_handleAssigneeChange: function (event) {
			var input = this.assigneeInput,
				value = input.value;
			var newInput = dijit.byId('assign_1_'+this.id);
			var newValue = newInput.store.query({name:newInput.value})[0].id;
			if (value == 'new-item'||newValue == 'new-item') {
				//New project popup
				input.value = this.assignee;
				Tooltip.open('frfind', 'search: ;', input, null, 'left', 'frfind_0');
				Team.reset(input);
			} else {
				this.set("assignee", value);
			}
		},
		*/
		handleAssigneeChange: function (event) {
			var newInput = this.assigneeDijit;
			var items = newInput.store.query({name:newInput.value}),newValue=-1;

			//combobox brings back 'searchAttr', as opposed to unique ID,
			//so query may bring down more than one result,
			//however, we know that the selected value must exactly match the name attribute
			//to one of the query result items
			for (var i = 0;i<items.length;i++)
			{
				if (newInput.value == items[i].name)
				{
					newValue = items[i].id;
					// #4403 "New person" option
					if (newValue === "new-person") {
						this.handleAddNewAssignee();
						newInput.reset();
						return;
					}
					break;
				}
			}
			
			if (newValue > 0) 
			{
				this.assigneeHiddenInput.value = newValue;
				this.set('assignee',newValue);
				//Format the label to display in combobox according to its value
				this.formatDisplayedLabel(newValue,newInput.id);
			} else 
			{
				this.assigneeHiddenInput.value = 0;
				this.set('assignee',0);
				//Format the label to display in combobox according to its value
				this.formatDisplayedLabel(0,newInput.id);
			}
		},

		//Connect to assignee combobox onClick event - will open the drop down
		openAssigneeDropDown:function()
		{
			this.assigneeDijit.loadAndOpenDropDown();
			// #2811
			if (!this.get('assignee')) {
				var input = dojo.query('input.dijitInputInner', this.assigneeDijit.domNode);
				if (input.length) {
					input[0].value = '';
				}
			}
		},
		
		/**
		 * #2811: Set label of combobox to 'None' if assignee is not set.
		 */
		blurAssigneeDropDown:function()
		{
			if (!this.get('assignee')) {
				var noneItem = this.assigneeDijit.store.get(0);
				var input = dojo.query('input.dijitInputInner', this.assigneeDijit.domNode);
				if (input.length && noneItem && noneItem.name) {
					input[0].value = noneItem.name;
				}
			}
		},
		
		//Style the label in the assignee/participant combobox drop down when its value is 'None'
		renderLabel:function(item,store) 
		{
			if (!item.id) {
				return '<div class="comboboxNoneSelection">' + item.name + '</div>';
			} else {
				return '<div class="' + (item.id == this.assignee ? 'selected' : '') + '">' +
						(item.image24src ? ('<img class="comboboxAvatar" src="' + item.image24src + '" width="24" height="24" />') : '') +
						item.name +
						'</div>';
			}
		},
		
		renderParentLabel: function(item, store) {
			if (item.id == 'new-item') {
				return '<div class="comboboxNewProjectSelection">' + item.name + '</div>';
			} else if (item.id == '') {
				return '<div class="comboboxNoneSelectionAutoHeight">' + item.name + '</div>';
			} else {
				var color = hi.items.manager.get(item.id) ? hi.items.manager.get(item.id).color_value : '';
				return '<div class="' + (item.id == this.parent ? 'selected' : '') + '">' +
						'<span' + (color ? (' style="color: ' + color + ';"') : '') + '" data-icon="&#xe01f;" aria-hidden="true"></span>' +
						item.name + '</div>';
			}
		},
		
		renderNewPersonLabel: function(item, store) {
			if (item.id == 'new-person') {
				return '<div class="comboboxNewPersonSelection">' + item.name + '</div>';
			} else if(item.id == 0) {
				return '<div class="comboboxNoneSelection">' + item.name + '</div>';
			} else {
				return '<div class>' + '<img class="comboboxAvatar" src="' + item.image24src +
						'" width="24" height="24">' + item.name + '</div>';
			}
		},
		
		formatDisplayedAssigneeValue:function(value)
		{
			this.formatDisplayedLabel(this.get('assignee'), this.assigneeDijit.id);
		},
		
		formatDisplayedParticipantValue:function(value)
		{
			this.formatDisplayedLabel('', this.participantDijit.id);
		},
		
		formatDisplayedParentValue: function(value) {
			this.formatDisplayedLabel(this.get('parent'), this.parentInput.id);
		},
		
		//Style the displayed selected value of assignee/participant combobox
		formatDisplayedLabel: function(value, id) {
			if (!value || value.toString().toLowerCase() === 'none') {
				dojo.addClass(id, 'comboboxNoneSelected');
				dojo.removeClass(id, 'comboboxItemSelected');
			} else {
				dojo.addClass(id, 'comboboxItemSelected');
				dojo.removeClass(id, 'comboboxNoneSelected');
			}
		},

		handleAddNewParticipant: function() {
			var self = this;
			var newInput = this.participantDijit.domNode;
			var person_fullname = dijit.byId('person_fullname');
			var person_email = dijit.byId('person_email');

			if (person_fullname) person_fullname.set('value', '');
			if (person_email) person_email.set('value', '');
			Tooltip.open('frfind', 'search: ;', newInput, null, /*'left'*/null, 'frfind_0');
			/* Feature #1721 */
			openTab('frfind', 0, 0, {});
			setTimeout(function() {self.participantDijit.set('value','None',false);}, 1000);
		},
		/* Not used for participant combobox
		_handleParticipantChange: function (event) {
			var input = this.participantInput;
			
			if (input.value == 'new-item') {
				//New project popup
				Tooltip.open('frfind', 'search: ;', input, null, 'left', 'frfind_0');
				Team.reset(input);
				input.value = "";
			}
		},
		*/

		handleParticipantChange:function(event) {
			var input = this.participantDijit;
			var items = input.store.query({name:input.value}),newValue=-1;
			this.formatDisplayedLabel('',input.id);
			//combobox brings back 'searchAttr', as opposed to unique ID,
			//so query may bring down more than one result,
			//however, we know that the selected value must exactly match the name attribute
			//to one of the query result items
			for (var i = 0;i<items.length;i++)
			{
				if (input.value == items[i].name)
				{
					newValue = items[i].id;
						// #4403 "New person" option
						if (newValue === "new-person") {
							this.handleAddNewParticipant();
							input.reset();
							return;
						}
					if (items[i].id !== 0) {
						input.store.data.splice(input.store.data.indexOf(items[i], input.store.data), 1);
					}
					break;
				}
			}

			if (newValue > 0)
			{
				this.participantHiddenInput.value = newValue;
				this.set('_participant',newValue);
				Hi_Participant.addParticipantInput(input.domNode);
				// #2811: doing this does not allow user to input whole search query he wants.
				//this.participantDijit.set('value','None',false);
				dojo.query('.participantLabel').forEach(function(item) {
					dojo.addClass(item,'participantLabelWithParticipants');
				});
			} else 
			{
				this.participantHiddenInput.value = 0;
				this.set('_participant',0);
			}
			input.reset();
		},

		//Connect to participant combobox onClick event - will open the drop down
		openParticipantDropDown:function()
		{
			// #2811
			this.participantDijit.loadAndOpenDropDown();
			var input = dojo.query('input.dijitInputInner', this.participantDijit.domNode);
			if (input.length) {
				input[0].value = '';
			}
		},
		
		blurParticipantDropDown: function() {
			// #2811
			var noneItem = this.participantDijit.store.get(0);
			var input = dojo.query('input.dijitInputInner', this.participantDijit.domNode);
			if (input.length && noneItem && noneItem.name) {
				input[0].value = noneItem.name;
			}
		},
		
		handlePriorityChange: function (event) {
			var new_value = parseInt(event.target.value, 10),
				level = Math.max(0, Math.floor( new_value / 10000) - 1),
				old_value = this.task.get("priority"),
				new_level = Math.floor(new_value / 10000),
				old_level = Math.floor(old_value / 10000);
			/* Apply #2188 here, not in getFormValues()*/
			
			if (new_level == old_level) {
				//Don't change if priority group didn't changed
				new_value = old_value;
			} else {
				//#2188 new_value = Project.getMaxPriority(Math.max(0, new_level - 1));
				new_value = Project.getMaxPriority(Math.max(0, new_level - 1), 10)/* Feature #2152 */;
			}
			this.set("priority", new_value);
			
		},
		
		/* Tags */
		handleTagKeyPress: function (event) {
			// summary:
			//		Handle key press while inside tag input
			
			var keys = dojo.keys,
				key  = event.which || event.keyCode || false,
				el   = event.srcElement || event.target;
			
			event.cancelBubble = true;
			
			if (key == keys.KEY_ENTER && (el.tagName == "TEXTAREA" || (el.tagName == "INPUT" && el.type == "button") || el.tagName == "BUTTON")) {
				return;
			}
			
			if (key == keys.KEY_ESCAPE) {
				dojo.stopEvent(event);
				Hi_Tag.emptyTagInput(el, event);
			} else if (key == keys.KEY_ENTER) {
				dojo.stopEvent(event);
				Hi_Tag.addTagInput(el, event);
			}
		},
		handleTagAddButton: function (event) {
			// summary:
			//		Handle add tag button click
			
			Hi_Tag.addTagInput(event.target);
		},
		handleTagMoreTags: function (event) {
			// summary:
			//		Handle "More tags" click
			
			Hi_Tag.fillMoreTags(this.taskId, false);
			dojo.addClass(event.target, "hidden");
		},
		
		/* Date time */
		
		handleStartDateClick: function (event) {
			var self = this;
			this.startDateWidget.openDropDown();
			
			/* Bug #1933 */
			this.startDateWidget.focus();
		},

		handleEndDateClick: function (event) {
			var self = this;
			this.endDateWidget.openDropDown();
			/* Bug #1933 */
			this.endDateWidget.focus();
		},

		handleDueDateClick: function (event) {
			var self = this;
			this.dueDateWidget.openDropDown();
			/* Bug #1933 */
			this.dueDateWidget.focus();
		},

		handleRecurrenceEndDateClick:function(event) {
			var self = this;
			if (this.recurrenceEndDateWidget.get('disabled')) {
				dojo.stopEvent(event);
				return;
			}
			this.recurrenceEndDateWidget.openDropDown();
			/* Bug #1933 */
			this.recurrenceEndDateWidget.focus();
		},

		handleStartTimeClick: function () {
			/* Bug #2178 */
			if (this.get('is_all_day')) return;
			/*#2817: this.startTimeWidget.openDropDown();*/
			/* Bug #1933 */
			this.startTimeWidget.focus();
		},

		handleEndTimeClick: function () {
			/* Bug #2178 */
			if (this.get('is_all_day')) return;
			/*#2817: this.endTimeWidget.openDropDown();*/
			/* Bug #1933 */
			this.endTimeWidget.focus();
		},
		handleStartTimeIconClick: function () {
			this.handleStartTimeClick();
			this.startTimeWidget.openDropDown();
		},
		handleEndTimeIconClick: function () {
			this.handleEndTimeClick();
			this.endTimeWidget.openDropDown();
		},
		
		handleStartDateChange: function (value) {
			this.validateDateTimeInput("start_date");
			this.set("hasStartDateTime", value);
			this.showHideReminderWidget();
		},

		handleEndDateChange: function (event) {
			// if (this.get('category') === hi.widget.TaskItem.CATEGORY_TASK) return;
			this.validateDateTimeInput("end_date");
			this.dueDateWidget.set('value', this.endDateWidget.get('value'));
			this.showHideReminderWidget();
		},

		handleDueDateChange: function (event) {
			this.validateDateTimeInput("due_date");
			// this.endDateWidget.set('value', this.dueDateWidget.get('value'));
			this.showHideReminderWidget();
		},

		handleRecurrenceEndDateChange: function(event) {
			//Validate recurrence end date based on type of recurrence set?
			this.validateDateTimeInput("recurrence_end_date");
		},

		handleStartTimeChange: function () {
			this.validateDateTimeInput("start_time");
		},

		handleEndTimeChange: function () {
			this.validateDateTimeInput("end_time");
		},
		
		handleIsAllDayInputClick: function (event) {
			this.set("is_all_day", !this.is_all_day);
		},
		
		handleEventHalt: function (event) {
			dojo.stopEvent(event);
		},
		
		/* Multiple items */
		
		handleManyTasksFocus: function () {
			// summary:
			//		Handle many task input focus, hide label
			
			hideElement(previousElement(this.manyTasksInput));
		},
		handleManyTasksBlur: function () {
			// summary:
			//		Handle many task input blur, show label if input is empty
			
			if (!this.manyTasksInput.value) {
				showElement(previousElement(this.manyTasksInput));
			}
		},
		
		handleManyTasksKeydown: function(event) {
			
		},
		
		handleMultipleStepForward: function () {
			// summary:
			//		Show next step for multiple item form
			
			NewItemManyTasks.goStepForward();
			//TODO
		},
		
		handleMultipleCreate: function () {
			// summary:
			//		Create multiple items
			/* #3086 - this will tell NewItemManyTasks objects where on the DOM to look for many tasks specification */
			NewItemManyTasks.add(true);
			//TODO
		},
		
		handleSaveClick: function () {
			// summary:
			//		Handle save button click
			
			this.save();
		},
		
		handleCancelClick: function () {
			// summary:
			//		Handle cancel button click
			
			this.cancel();
		},

		handlerRememberPermissionCheckboxClick: function() {
			this.doRememberPermission = !this.doRememberPermission;
		},
		
		handleTooltipCallerClick: function (event) {
			// summary:
			//		Handle click on item which should show tooltip
			//this.editingView.collapse();
			function locateFn (name) {
				// summary:
				//		Finds function from string and returns it with correct "this" context
				
				if (!name) return null;
				
				var context = dojo.global,
					last = context,
					ns = name.split(/[\/\.]/);
				
				for(var i=0,ii=ns.length; i<ii; i++) {
					if (!context[ns[i]]) return null;
					last = context;
					context = context[ns[i]];
				}
				
				return function () { 
					return context.apply(last, arguments);
				};
			}
			
			var target = event.target,
				tooltip_name = dojo.getAttr(target, "data-tooltip-name"),
				tooltip_position = dojo.getAttr(target, "data-tooltip-position"),
				tooltip_data = dojo.getAttr(target, "data-tooltip-data"),
				tooltip_validate = dojo.getAttr(target, "data-tooltip-validate"),
				tooltip_start = dojo.getAttr(target, "data-tooltip-start"),
				tooltip_function = dojo.getAttr(target, "data-tooltip-function");
			
			tooltip_position = tooltip_position ? tooltip_position : false;
			tooltip_data = tooltip_data ? dojo.fromJson(tooltip_data) : null;
			tooltip_validate = locateFn(tooltip_validate);
			tooltip_start = tooltip_start ? tooltip_start : "";
			tooltip_function = locateFn(tooltip_function);
			
			if (tooltip_validate && !tooltip_validate(target, event)) {
				return;
			}
			
			var res = Tooltip.open(tooltip_name, tooltip_data, target, event, tooltip_position, tooltip_start);
			var self = this;
			/* Bug #1873 */
			require(['dojo/aspect'],function(aspect) {
				aspect.before(self,'collapse',function() {if (Tooltip.opened(tooltip_name)) Tooltip.close(tooltip_name);});
			});
			Click.setClickFunction(
				function() {
					if (tooltip_function) {
						if (tooltip_function.indexOf('(') === -1) {
							tooltip_function = locateFn(tooltip_function);
							if (tooltip_function) tooltip_function();
						} else {
							dojo.eval(tooltip_function);
						}
					}
				},
				target,
				tooltip_name || ''
			);
		},
		
		hasNativePlaceholderSupport: function() {
			var inp = document.createElement('input');
			return typeof(inp.placeholder) != 'undefined';
		}
		
	});

	TaskItemEdit.ALERTTIMEMATRIX = [
		[1, 5, 10, 15, 20, 30],		//'MINUTE' 
		[1, 2, 3, 4, 5, 8, 12],		//'HOUR'
		[1, 2, 3, 5, 7]				//'DAY' 
	];

	return TaskItemEdit;
});
