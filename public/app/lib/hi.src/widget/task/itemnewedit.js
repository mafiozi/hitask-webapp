/** * dojo widget corresponding to the form used to specify a new item (e.g., by pushing add button, clicking on the input element labeled
 * 'Enter item name')
 * 
 */
define([
	"dojo/_base/array", // array.forEach array.map
	"dojo/_base/declare", // declare
	"dojo/cldr/supplemental", // cldrSupplemental.getFirstDayOfWeek
	"dojo/date", // date
	"dojo/date/locale",
	"dojo/dom", // dom.setSelectable
	"dojo/dom-class", // domClass.contains
	"dojo/_base/event", // event.stop
	"dojo/_base/lang", // lang.getObject, lang.hitch
	"dojo/_base/sniff", // has("ie") has("webkit")
	"dojo/string", // string.substitute
	"dojo/_base/window", // win.doc.createTextNode
	"dijit/_Widget",
	"dijit/_Templated",
	"dijit/_WidgetsInTemplateMixin",
	"dojo/text!./itemnewedit.html",
	"dojo/text!./template_include/itemnewedit_title.html",
	"dojo/fx",
	"dijit/form/Button",
	"hi/widget/task/item",
	"hi/widget/task/itemedit",
	"hi/widget/task/config/fieldsController",
	'hi/widget/task/item_DOM',
	'dojo/query'
], function(array, declare, cldrSupplemental, date, local, dom, domClass, event, lang, has, string, win,
			_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template, template_title,
			fx, Button, TaskItem, TaskItemEdit, fieldsController, item_DOM, dojoQuery) {
	
	//Replace localization string in template instead of using attributes
	template = template.replace('{include_title}', template_title).replace(/\{#([^#]+)#\}/g, function (all, str) {
		var localized = hi.i18n.getLocalization(str.trim()) || '';
		if (!localized) console.warn('Missing localization for "' + str + '"');
		return localized;
	});
	
	// module:
	//		hi/widget/TaskItemEdit
	// summary:
	//		Task item edit view
	
	var TaskItemNewEdit = declare("hi.widget.TaskItemNewEdit", [TaskItemEdit], {
		// summary:
		//		Task item edit view

		// Template for the viewset
		templateString: template,
		
		// TaskItem widget
		task: null,
		
		// Other widgets are in template, parse them
		widgetsInTemplate: true,
		
		// Properties which will be copied from task
		_copyProperties: {
			"taskId": true,
			"starred": true,
			"title": true,
			"message": true,
			"participants": true,
			"tags": true,
			"color": true,
			"reminder_time": true, "reminder_time_type": true, "reminder_enabled": true,
			"time_track": true, "time_est": true,
			"recurring": true,
			"parent": true,
			"priority": true,
			"is_all_day": true,
			"assignee": true, "shared": true, "my": true,
			"permissions": []
		},
		
		// Properties which will be copied to task after save
		// Since this is new item we don't want to copy them
		_reverseCopyProperties: {},
		_lastCreatedItemTypePreferenceName: 'last_created_item_type',
		
		/* ----------------------------- ATTRIBUTES ------------------------------ */
		/* Task ID */
		taskId: null,
		_setTaskIdAttr: function (taskId) {
			this._set("taskId", taskId);
			this.set("isNew", !taskId || taskId < 0);
		},
		
		/* Color */
		color: "0",
		_setColorAttr: function (color) {
			this._set("color", color);
			
			this.colorInput.value = color;
			
			//Color picker
			var items = dojo.query("div", this.colorPickerNode),
				i = 0,
				ii = items.length;
			
			for (; i<ii; i++) {
				dojo.toggleClass(items[i], "selected", color == dojo.getAttr(items[i], "data-color"));
			}
		},
		
		/* Is this a new item */
		isNew: false,
		_setIsNewAttr: function (is_new) {
			is_new = !!is_new;
			this._set("isNew", is_new);
			
			dojo.toggleClass(this.domNode, "is_new_item", is_new);
			dojo.toggleClass(this.categorySelectionNode, "hidden", !is_new);
			//dojo.toggleClass(this.parentContainerNode, "hidden", is_new); // Commented due to #2073.
		},
		
		/* Category */
		category: hi.widget.TaskItem.CATEGORY_TASK,
		_setCategoryAttr: function (category) {
			var prevCategory = this.category;
			this._set("category", category);
			
			this.categoryInput.value = category;
			
			if (category == hi.widget.TaskItem.CATEGORY_TASK || category == hi.widget.TaskItem.CATEGORY_EVENT) {
				dojo.removeClass(this.reminderContainerNode, "hidden");
				dojo.removeClass(this.timeContainerNode, "hidden");
			} else if (category == hi.widget.TaskItem.CATEGORY_NOTE || category == hi.widget.TaskItem.CATEGORY_FILE) {
				dojo.addClass(this.reminderContainerNode, "hidden");
				dojo.addClass(this.timeContainerNode, "hidden");
			}
			
			if (category == hi.widget.TaskItem.CATEGORY_FILE) {
				dojo.removeClass(this.titleTextNode, "hidden");
				dojo.addClass(this.titleInputNode, "hidden");
				dojo.addClass(this.saveButtonContainerNode, "hidden");
				dojo.addClass(this.domNode, "tab-file");
				
				if (category != prevCategory && FileUpload.enabled) {
					FileUpload.reset(this.taskId);
				}
			} else {
				dojo.addClass(this.titleTextNode, "hidden");
				dojo.removeClass(this.titleInputNode, "hidden");
				dojo.removeClass(this.saveButtonContainerNode, "hidden");
				dojo.addClass(this.uploadButtonContainerNode, "hidden");
				dojo.removeClass(this.domNode, "tab-file");
				this._adjustStarNodePosition();

				if (FileUpload.enabled) {
					FileUpload.destroy(this.taskId);
				}
			}

			// var fc = fieldsController[category];
			// var k, vis, node;

			// for (k in fc.visible) {
			// 	vis = fc.visible[k];
			// 	node = this[fc.fields[k]];
			// 	if (vis) {
			// 		dojo.removeClass(node, 'hidden');
			// 	} else {
			// 		dojo.addClass(node, 'hidden');
			// 	}
			// }
			fieldsController.showFields(this, category);
			this.fillReminderText();
		},

		_adjustStarNodePosition: function(){
			var starNode = this.starredNode,
				addButtonNode = this.saveButtonContainerNode, x1, x2, right;

			if(!starNode || !addButtonNode) return;

			x1 = dojo.position(starNode).x;
			x2 = dojo.position(addButtonNode).x;
			right = parseInt(dojo.style(starNode, 'right'));

			if(!x1 || !x2) return;

			right -= x2 - 22 - x1;
			dojo.style(starNode, 'right', right + 'px');
		},
		
		/* Type */
		type: 1,
		_setTypeAttr: function (type) {
			this._set("type", type);
			
			if (type == 1 || type == 2 || type == 4) {
				//Task, Event or Note
				this.set("category", type);
			} else if (type == 6) {
				//File
				this.set("category", hi.widget.TaskItem.CATEGORY_FILE);
			}
			
			if (type == 5) {
				//Multiple items
				this.set("category", hi.widget.TaskItem.CATEGORY_TASK);
				
				dojo.addClass(this.reminderContainerNode, "hidden");
				dojo.addClass(this.timeContainerNode, "hidden");
				dojo.addClass(this.titleTextNode, "hidden");
				dojo.addClass(this.titleInputNode, "hidden");
				dojo.addClass(this.messageContainerNode, "hidden");
				dojo.addClass(this.domNode, "tab-many-tasks");
			} else {
				dojo.removeClass(this.messageContainerNode, "hidden");
				dojo.removeClass(this.domNode, "tab-many-tasks");
				
				NewItemManyTasks.goStepBack();
				this.manyTasksInput.set('value', '');
				this.manyTasksInput.set('placeHolder', hi.i18n.getLocalization('task.many_tasks_description'));
			}
			
			if (this.isNew) {
				var nodes = dojo.query('td', this.categorySelectionNode),
					i = 0,
					ii = nodes.length;
				
				for (; i < ii; i++) {
					dojo.toggleClass(nodes[i], "active", type == parseInt(dojo.getAttr(nodes[i], "data-type"), 10));
				}
			}
		},
		
		/* Starred */
		starred: false,
		_setStarredAttr: function (starred) {
			starred = !!starred;
			this._set("starred", starred);
			
			dojo.toggleClass(this.starredNode, "star-selected", starred);
			dojo.query("input", this.starredNode, true)[0].value = starred ? 1 : 0;
		},
		
		/* Title */
		title: "",
		_setTitleAttr: function (title) {
			var inputDijit = this.titleInputNode;
			this._set("title", title);
			inputDijit.set('placeHolder', this._getDefaultTitle());
		
			// #3773
			//title = htmlspecialchars(title);
			inputDijit.set('value', title); 
		},
		
		/* Message */
		message: "",
		_setMessageAttr: function (message) {
			/* #2764 */
			return this.inherited(arguments);
			/*
			message = dojo.trim(message || "");
			this._set("message", message);
			
			this.messageInput.value = message;
			dojo.toggleClass(this.messageLabelNode, "hidden", message);
			*/
		},
		
		/* Defined in ItemEdit widget.
		// Start date
		hasStartDateTime: false,
		_setHasStartDateTimeAttr: function (has) {
		},
		*/

		/* Is all day */
		is_all_day: false,
		_setIs_all_dayAttr: function (is_all_day) {
			is_all_day = !!is_all_day;
			this._set("is_all_day", is_all_day);
			
			dojo.setAttr(this.isAllDayInput, "checked", is_all_day);
			
			this.startTimeWidget.set("disabled", is_all_day);
			this.endTimeWidget.set("disabled", is_all_day);
			
			if (is_all_day) {
				this.startTimeWidget.set("value", null);
				this.endTimeWidget.set("value", null);
			}
		},
		
		/* Sharing */
		hasSharing: true,
		_setHasSharingAttr: function (has) {
			this._set("hasSharing", has);
			dojo.toggleClass(this.sharingContainerNode, "hidden", !has);
		},
		
		/* Business */
		hasBusiness: true,
		_setHasBusinessAttr: function (has) {
			this._set("hasBusiness", has);
			dojo.toggleClass(this.sharingSharingNode, "hidden", !has);
			dojo.toggleClass(this.sharingParticipantsNode, "hidden", !has);
		},
		
		/* Recurring */
		/*
		recurring: 0,
		_setRecurringAttr: function (recurring) {
			this._set("recurring", recurring);
			this.recurringInput.value = recurring;
		},
		*/
		
		/* Defined in ItemEdit widget. 
		// Reminder time type
		reminder_time_type: "m",
		_setReminder_time_typeAttr: function (reminder_time_type) {
		},
		*/
		
		reminder_time: 0,
		_setReminder_timeAttr: function (reminder_time) {
			this._set("reminder_time", reminder_time);
		},
		
		/* Defined in itemEdit widget.
		reminder_enabled: false,
		_setReminder_enabledAttr: function (enabled) {
		},*/
		
		/* Time tracking */
		time_track: false,
		_setTime_trackAttr: function (enabled) {
			this._set("time_track", enabled);
			
			dojo.setAttr(this.timeTrackingInput, "checked", enabled);
			this.timeTrackingEstInput.set('disabled', !enabled);
			dojo.toggleClass(this.timeTrackingContainerNode, "time-tracking-disabled", !enabled);
		},
		
		time_est: 0,
		_setTime_estAttr: function (time_est) {
			this.inherited(arguments);
		},
		
		/* Parent */
		parent: 0,
		_setParentAttr: function (parent) {
			this._set("parent", parent);
			this.parentHiddenInput.value = parent;
		},
		
		permissions: null,
		_setPermissionsAttr: function(pms) {
			if(typeof pms === 'string'){
				pms = JSON.parse(pms);
				// hi.items.manager.set(this.taskId, {permissions: pms}, true/*silent*/);
			}
			this._set('permissions', pms);

			if (!this.get("hasBusiness") || (!pms && !this.my)) {
				dojo.addClass(this.sharingSharingNode, "hidden");
				// this.sharedInputPrivate.checked = true;
				this.sharingWidget.sharingDropDown.set('status', 1);
			} else {
				dojo.removeClass(this.sharingSharingNode, "hidden");
				// this.sharedInputShared.checked = shared ? true : false;
				// this.sharedInputPrivate.checked = shared ? false : true;
				this.sharingWidget.sharingDropDown.set('status', 0);
				this.fillAssigneeList();
			}
			this.sharingWidget.fillSharingList(pms);
		},

		/* Priority */
		priority: 0,
		_setPriorityAttr: function (priority) {
			priority = priority || 20000;
			this._set("priority", priority);
			
			if (priority < 20000) {
				dojo.setAttr(this.priorityInputLow, "checked", true);
			} else if (priority < 30000) {
				dojo.setAttr(this.priorityInputMedium, "checked", true);
			} else {
				dojo.setAttr(this.priorityInputHigh, "checked", true);
			}
		},
		
		/* Shared */
		assignee: 0,
		
		my: true,
		_setMyAttr: function (my) {
			//in case if "my" changes after "shared" attribute
			if (this.my != my) {
				this._set("my", my);
				this.set("shared", this.shared);
			}
		},
		/* ----------------------------- CONSTRUCTOR ------------------------------ */
		constructor: function (/*Object*/args) {},
		
		buildRendering: function () {
			this.inherited(arguments);
			
			this.fillReminderTimeTypes();
			
			this.set("hasBusiness", Project.businessId);
			this.set("hasSharing", CONSTANTS.accountLevel || CONSTANTS.emailConfirmed);
			this.set("reminder_time_type", Hi_Preferences.get("reminder_time_type"));
			
			//Message input max length
			dojo.setAttr(this.messageInput, "maxlength", CONSTANTS.taskMessageMaxlength);
			
			//Set prefered date format
			var datePattern = Hi_Preferences.get('date_format').replace('m', 'MM').replace('d', 'dd').replace('y', 'yyyy'),
				constraints = this.startDateWidget.get("constraints");
				
			constraints.datePattern = datePattern;
			this.startDateWidget.set("constraints", constraints);
			this.endDateWidget.set("constraints", constraints);
			this.dueDateWidget.set("constraints", constraints);
			this.recurrenceEndDateWidget.set('constraints',constraints);
			
			//Set prefered time format
			var timePattern = Hi_Preferences.get('time_format') == 12 ? "h:mm a" : "HH:mm";

			constraints = this.startTimeWidget.get("constraints");
			constraints.timePattern = timePattern;
			this.startTimeWidget.set("constraints", constraints);
			this.endTimeWidget.set("constraints", constraints);
			
			this.topLevelWrapper.id = 'itemNewEditForm';
			var lastType = this._getLastCreatedItemType();
			if (lastType) {
				this.set('type', lastType);
				dojoQuery('.type_selector', this.categorySelectionNode).removeClass('active');
				dojoQuery('.type_selector[data-type="' + lastType + '"]', this.categorySelectionNode).addClass('active');
			}
		},
		
		
		/* ----------------------------- Fill lists ------------------------------ */
		
		
		/* Defined in ItemEdit widget.
		fillReminderTimeTypes: function () {
		},
		*/
		
		// Defined in ancestor widget ItemEdit.
		fillProjectList: function () {
			this.inherited(arguments);
			input = this.parentInput,
			option = null,
			value = this.parent || '';

			//Restore previous value if possible
			var prev = input.store.get(value ? value : '');

			if (prev && prev.name) {
				input.set('value', prev.name);
			}
		},
		
		//Probably can be commented out if using combobox for assignee
		fillAssigneeListOption: function (id, name, selected) {
			// summary:
			//		Add option to assignee list
			// id:Number
			//		Option value
			// name:String
			//		Option title
			// selected:Boolean
			//		Option is selected
			
			var option = new Option(name, id);
			this.assigneeInput.options[this.assigneeInput.options.length] = option;
		},
		/* defined for combobox in TaskItemEdit object, which is defined in itemedit.js
		fillAssigneeList: function () {
			// summary:
			//		Fill assignee list
			
			if (!('friends' in Project)) return;
			
			var taskId = this.taskId || 0,
				input = this.assigneeInput,
				default_value = Hi_Preferences.get('default_assign_user', 0, 'int') || 0,
				value = (this.isNew ? default_value : this.assignee) || 0,
				shared = this.shared;
			
			if (!Project.businessId || (!shared && !this.my)) shared = null; // ignore if user doesn't have option to select it
			
			for (var i = input.options.length - 1; i > 0; i--) {
				input.removeChild(input.options[i]);
			}
			
			// add myself to list
			input.options[input.options.length] = new Option(Project.getMyself(), Project.user_id);
			
			// add friends
			var friends = Project.friends,
				i = 0,
				imax = friends.length;
			
			for (; i < imax; i++) {
				id = friends[i].id;
				if (Project.canAssign(taskId, id, shared)) {
					if (friends[i].activeStatus || friends[i].waitStatus || friends[i].businessStatusInactive === true) {
						input.options[input.options.length] = new Option(Project.getFriendName(id, 2, true), id);
					}
				}
			}
			
			//New person item
			var separator = new Option('------------------------------', '-');
			separator.disabled = true;
			input.options[input.options.length] = separator;
			input.options[input.options.length] = new Option(hi.i18n.getLocalization('team_list.add_person'), 'new-item');
			
			this.assignee = input.value = value;
		},
		*/
		/* defined for combobox in TaskItemEdit object, which is defined in itemedit.js
		fillParticipantList: function () {
			// summary:
			//		Fill participant list
			
			if (!this.hasBusiness) return;
			if (!('friends' in Project)) return;
			
			var taskId = this.taskId || 0,
				input = this.participantInput;
			
			for (var i = input.options.length - 1; i > 0; i--) {
				input.removeChild(input.options[i]);
			}
			
			var fillParticipantSelectOptions = function(id, name){
				opt = new Option(name, id);
				el.options[el.options.length] = opt;
			}
			
			// add myself to list
			input.options[input.options.length] = new Option(Project.getMyself(), Project.user_id);
			
			// add friends
			var friends = Project.friends,
				i = 0,
				imax = friends.length;
			
			for (; i < imax; i++) {
				id = friends[i].id;
				if (friends[i].activeStatus || friends[i].waitStatus || friends[i].businessStatusInactive === true) {
					input.options[input.options.length] = new Option(Project.getFriendName(id, 2, true), id);
				}
			}
			
			//New person item
			var separator = new Option('------------------------------', '-');
			separator.disabled = true;
			input.options[input.options.length] = separator;
			input.options[input.options.length] = new Option(hi.i18n.getLocalization('team_list.add_person'), 'new-item');
			
			input.value = "";
		},
		*/
		parseTaskTitle: function () {
			// summary:
			//		Parse input title
			
			return parseTaskTitle.call(this);
		},
		
		
		/* ----------------------------- API ------------------------------ */
		
		
		expand: function () {
			// summary:
			//		Expand task and populate form with latest data
			//		Form is not updated with model (store) change because that will affect performance.
			
			dojo.removeClass(this.topLevelNode, 'hidden');
			var effect = fx.wipeIn({node: this.domNode, duration: CONSTANTS.duration}),
				deferred = new dojo.Deferred();
			
			dojo.connect(effect, "onEnd", this, function () {
				this.domNode.style.height = "auto";
				this.domNode.style.overflow = "visible";
				this._adjustStarNodePosition();
				this.focusInput();
				
				deferred.resolve({success: true});
			});
			effect.play();
			
			//File upload
			if (this.taskId < 0 && this.category == 5) { // File
				FileUpload.reset(this.taskId);
				//Remov all files
				FileUpload.get(this.taskId).resetAll();
			}
			
			// Copy attribute values we need from TaskItem
			var properties = this._copyProperties,
				key = null,
				attrs = {},
				task = this.task;
			
			for (key in properties) {
				attrs[key] = task.get(key);
			}
			
			attrs.start_date = task.start_date; // date + time in API format
			attrs.end_date   = task.end_date;   // date + time in API format
			attrs.start_time = task.start_date; // date + time in API format, time widget needs full date time
			attrs.end_time   = task.end_date;   // date + time in API format, time widget needs full date time
			attrs.due_date = task.due_date;
			
			//New item start and end date
			if (attrs.taskId < 0 && !attrs.start_date) {
				var dateStr = HiTaskCalendar.getCurrentDateStr(),
					date = str2time(dateStr, "y-m-d"),
					now = hi.items.date.truncateTime(new Date());
				
				if (hi.items.date.compareDateTime(date, now) !== 0) {
					attrs.start_date = dateStr;
					attrs.is_all_day = true;
				}
			}
			
			attrs.hasStartDateTime = attrs.start_date;
			attrs.shared = Hi_Preferences.get('default_shared', false, 'bool');
			attrs.time_track = Hi_Preferences.get('enable_time_tracking', false, 'bool');
			if (Hi_Preferences.get('default_new_item_use_last', true, 'bool')) {
				attrs.permissions = Hi_Preferences.get('default_sharing_permission_last', '[{"principal": "everyone", "level": 60}]', 'string');
			} else {
				attrs.permissions = Hi_Preferences.get('default_new_item_sharing_permission', '[]', 'string');
			}

			this.set(attrs);
			this.set('reminder_time', this.reminder_time ? this.reminder_time : 0);
			
			// Fill lists
			this.fillProjectList();
			this.fillAssigneeList();
			this.fillParticipantList();
			this.fillRecurringIntervalList();
			
			Hi_Participant.fillPropertyParticipants();
			
			//Tags
			if(this.tagInput) this.tagInput.removeAll();
			Hi_Tag.fillPropertyTags(this.taskId);
			
			var tagFilter = Hi_Preferences.get('tagfilter', '');
			if (tagFilter) {
				Hi_Tag.addTag(this.tagMoreNode, tagFilter);
			}
			
			//Many tasks
			NewItemManyTasks.goStepBack();
			this.manyTasksInput.set('value', '');
			this._onPermissionsChange();
			
			return deferred;
		},
		
		collapse: function () {
			/* Feature #1806 */
			this.closeDateTimeDijits();
			this.resetRecurrenceIntervalForm();
			var effect = fx.wipeOut({node: this.domNode, duration: CONSTANTS.duration}),
				deferred = new dojo.Deferred();
			
			dojo.connect(effect, "onEnd", this, function () {
				this.domNode.style.overflow = "visible";
				deferred.resolve({success: true});
			});
			effect.play();
			
			return deferred;
		},
		
		cancel: function () {
			// summary:
			//		Cancel editing
			/* Feature #1806 */
			this.closeDateTimeDijits();
			this.resetRecurrenceIntervalForm();
			Project.hideProjectDialog(); // #2115

			this.task.closeEdit();
		},
		
		_getLastCreatedItemType: function() {
			return Hi_Preferences.get(this._lastCreatedItemTypePreferenceName, null, 'int');
		},
		
		_setLastCreatedItemType: function(type, send) {
			var t = this._getLastCreatedItemType();
			if (type != t) {
				Hi_Preferences.set(this._lastCreatedItemTypePreferenceName, type);
				if (send) {
					Hi_Preferences.send();
				}
			}
		},
		
		/* #2766 */
		setSaveIntervalTimer:null,
		setSaveChanged:null,
		setSaveDeferred:null,
		save: function () {
			/* Feature #1806 */
			this.closeDateTimeDijits();
			// summary:
			//		Save

			//getFormValue is inherited from itemedit.js
			var values = this.getFormValues(),
				changed = null,
				key = 0;

			if (this.category == hi.widget.TaskItem.CATEGORY_FILE) {
				//File upload
				FileUpload.get(this.taskId).upload();
				//this.task.closeEdit();
			} else {
				if (this.validate(values)) {
					/* #2188 - must set default medium prority value to highest + 10 so new items with medium (default) 
					 * priority show up on top of the medium priority heap of items
					 */
					values.priority =  Project.getMaxPriority(Math.max(0, Math.floor( values.priority/ 10000) - 1), 10)/* Feature #2152 */;

					//Update cache manually + immediatelly
					//because there is no ID for new item which store could use to identify this item
					var taskId = --TaskItem.ID_COUNTER;
					values.user_id = Project.user_id;
					hi.items.manager.set(taskId, values);
					
					trackEvent(trackEventNames.ITEM_CREATE);
					
					//Save
					/* #2766 */
					this.setSaveChanged = values;
					var firePostResponse = firePOST({
						post:hi.items.manager.setSave,
						postArgs:[this.setSaveChanged]
					});
					var self = this;
					
					//this.setSaveIntervalTimer = firePostResponse.intervaTimer;
					this.setSaveDeferred = firePostResponse.deferred;
					this.setSaveIntervalTimer = firePostResponse.intervalTimer;
					this.setSaveDeferred.then(function(id) {
						/* #2766 */
						hi.items.manager.remove(taskId);
						if (id) {
							self._rememberPermissions(id);
							
							//When request is complete new item will be added, we can remove temporary one
							// #2766 propose to move before callback for Chrome hi.items.manager.remove(taskId);
							hi.items.manager.refresh();
							//#2766 move close edit to callback until we can open the form and populate it
							// after a timeout
							self.task.closeEdit();
							self._setLastCreatedItemType(self.get('type')); // #5830
						} else {
							if (!Project.alertElement.open) {
								Project.alertElement.showItem(22);
							}
							clearInterval(self.setSaveIntervalTimer);
							//#2766 proposed self.setSaveDeferred.cancel();
						}
					});
					/*
					hi.items.manager.setSave(values).then(dojo.hitch(this, function (id) {
						if (id)
						{
							//When request is complete new item will be added, we can remove temporary one
							hi.items.manager.remove(taskId);
							hi.items.manager.refresh();	
							//#2766 move close edit to callback until we can open the form and populate it
							// after a timeout
							this.task.closeEdit();
						} 

					}));
					*/
					//this.task.closeEdit();

				}
			}
		},
		
		
		/* ----------------------------- EVENT BINDINGS ------------------------------ */
		handleStarredClick: function (event) {
			this.set("starred", !this.starred);
		},
		
		handleTitleChange: function (event) {
			this.set("title", this.parseTaskTitle());
		},
		
		handleMessageKeyUp: function (event) {
			var max = CONSTANTS.taskMessageMaxlength,
				message = hi.i18n.getLocalization("task.message_max_size").replace("%d", max);
			
			if (!ismaxlength(this.messageInput, false, false, false, message)) {
				if (event) {
					dojo.stopEvent(event);
				}
			}
			// this.messageAutoSize();
		},
		
		messageAutoSize: function() {
			this.messageInput.style.height = 'auto';
			this.messageInput.style.height = this.messageInput.scrollHeight.toString() + 'px';
		},
		
		handleReminderEnabledClick: function (event) {
			var value = this.reminderEnabledNode.checked;
			this.set("reminder_enabled", value);
		},
		/* Bug #2029 Is defined in itemedit already
		handleCategoryClick: function (event) {
			var target = event.target,
				type = parseInt(dojo.getAttr(target, "data-type"), 10);
			
			if (type) {
				this.set("type", type);
			}
			
			dojo.stopEvent(event);
		},
		*/
		handleColorSelection: function (event) {
			var target = event.target.tagName == "SPAN" ? event.target.parentNode : event.target,
				color = parseInt(dojo.getAttr(target, "data-color"), 10);
			
			this.set("color", color);
		},
		
		handleTimeTrackingInputClick: function (event) {
			this.set("time_track", !this.time_track);
		},
		
		handleTimeTrackingEstimateChange: function (event) {
			var time_est = parseTimeString(this.timeTrackingEstInput.get('value'));
			this.set("time_est", time_est);
		},
		// Defined in ancestor widget ItemEdit.
		/*handleParentChange: function (event) {
			
		},*/
		
		handleSharedChange: function (event) {
			var shared = this.sharedInputShared.checked;
			this.set("shared", shared);
		},
	
		/* Tags */
		handleTagKeyPress: function (event) {
			// summary:
			//		Handle key press while inside tag input
			
			var keys = dojo.keys,
				key  = event.which || event.keyCode || false,
				el   = event.srcElement || event.target;
			
			event.cancelBubble = true;
			
			if (key == keys.KEY_ENTER && (el.tagName == "TEXTAREA" || (el.tagName == "INPUT" && el.type == "button") || el.tagName == "BUTTON")) {
				return;
			}
			
			if (key == keys.KEY_ESCAPE) {
				dojo.stopEvent(event);
				Hi_Tag.emptyTagInput(el, event);
			} else if (key == keys.KEY_ENTER) {
				dojo.stopEvent(event);
				Hi_Tag.addTagInput(el, event);
			}
		},
		handleTagAddButton: function (event) {
			// summary:
			//		Handle add tag button click
			
			Hi_Tag.addTagInput(event.target);
		},
		handleTagMoreTags: function (event) {
			// summary:
			//		Handle "More tags" click
			
			Hi_Tag.fillMoreTags(this.taskId, false);
			dojo.addClass(event.target, "hidden");
		},

		/* #2178 - this is defined in itemedit alreay 
		handleIsAllDayInputClick: function (event) {
			this.set("is_all_day", !this.is_all_day);
		},
		*/
		handleEventHalt: function (event) {
			dojo.stopEvent(event);
		},
		
		/* Multiple items */
		
		handleManyTasksFocus: function () {
			// summary:
			//		Handle many task input focus, hide label
			
			//hideElement(previousElement(this.manyTasksInput));

			if (this.manyTasksInput.value == hi.i18n.getLocalization('task.many_tasks_description'))
			{
				this.manyTasksInput.value='';
			} 
			dojo.addClass(this.manyTasksInput,'inputText');
			dojo.removeClass(this.manyTasksInput,'placeHolder');
		},
		handleManyTasksBlur: function () {
			// summary:
			//		Handle many task input blur, show label if input is empty
			
			if (!this.manyTasksInput.value || this.manyTasksInput.value.trim().length<=0 || this.manyTasksInput.value == hi.i18n.getLocalization('task.many_tasks_description')) {
				//showElement(previousElement(this.manyTasksInput));
				this.manyTasksInput.value = hi.i18n.getLocalization('task.many_tasks_description');
				dojo.removeClass(this.manyTasksInput,'inputText');
				dojo.addClass(this.manyTasksInput,'placeHolder');

			}

		},
		handleManyTasksKeydown: function(event) {
			// #2419:
			var value = this.manyTasksInput.value || '';
			if (event && event.keyCode == Hi_Shortcuts.KEY_ENTER && value.split('\n').length >= CONSTANTS.manyTasksMaxItems) {
				dojo.stopEvent(event);
			}
		},
		
		handleMultipleStepForward: function () {
			// summary:
			//		Show next step for multiple item form
			
			NewItemManyTasks.goStepForward();
			//TODO
		},
		
		handleMultipleCreate: function () {
			// summary:
			//		Create multiple items
			
			NewItemManyTasks.add();
			//TODO
		},
		
		handleSaveClick: function () {
			// summary:
			//		Handle save button click
			
			this.save();
		},
		
		handleCancelClick: function () {
			// summary:
			//		Handle cancel button click
			
			this.cancel();
		}/*,
		 Bug #1873 - This is already defined in itemedit.js 
		handleTooltipCallerClick: function (event) {
			// summary:
			//		Handle click on item which should show tooltip
			
			function locateFn (name) {
				// summary:
				//		Finds function from string and returns it with correct "this" context
				
				if (!name) return null;
				
				var context = dojo.global,
					last = context,
					ns = name.split(/[\/\.]/);
				
				for(var i=0,ii=ns.length; i<ii; i++) {
					if (!context[ns[i]]) return null;
					last = context;
					context = context[ns[i]];
				}
				
				return function () { 
					return context.apply(last, arguments);
				};
			};
			
			var target = event.target,
				tooltip_name = dojo.getAttr(target, "data-tooltip-name"),
				tooltip_position = dojo.getAttr(target, "data-tooltip-position"),
				tooltip_data = dojo.getAttr(target, "data-tooltip-data"),
				tooltip_validate = dojo.getAttr(target, "data-tooltip-validate"),
				tooltip_start = dojo.getAttr(target, "data-tooltip-start"),
				tooltip_function = dojo.getAttr(target, "data-tooltip-function");
			
			tooltip_position = tooltip_position ? tooltip_position : false;
			tooltip_data = tooltip_data ? dojo.fromJson(tooltip_data) : null;
			tooltip_validate = locateFn(tooltip_validate);
			tooltip_start = tooltip_start ? tooltip_start : "";
			tooltip_function = locateFn(tooltip_function);
			
			if (tooltip_validate && !tooltip_validate(target, event)) {
				return;
			}
			
			var res = Tooltip.open(tooltip_name, tooltip_data, target, event, tooltip_position, tooltip_start);
			
			Click.setClickFunction(
				function() {
					if (tooltip_function) {
						if (tooltip_function.indexOf('(') === -1) {
							tooltip_function = locateFn(tooltip_function);
							if (tooltip_function) tooltip_function();
						} else {
							dojo.eval(tooltip_function);
						}
					}
				},
				target,
				tooltip_name || ''
			);
		}
		*/
		
	});
	
	return TaskItemNewEdit;

});
