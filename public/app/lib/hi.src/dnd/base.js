//dojo.provide("hi.dnd.base");
define(function () {
	
	dojo.global.HiDragSourceCached = {
		calendar: false,
		user: false,
		project: false,
		timetable: false
	};
	
	dojo.dnd.HiSortDragExtra = {
		constraints: [0, 0],
		connect: null,
		allowSort: true,
		
		initial_parent: 0,
		
		initial_priority: 0,
		initial_priority_level: 'low',
		last_priority: 0,
		last_priority_level: 'low',
		
		force_priority: false,
		
		drop_target: null,
		drop_place: null,
		drop_indicator: null,
		drop_type: null,
		actionType: undefined,				// Feature #5555, indicate current action.
		pEl: null,							// #5015: remember project DOM element.
		groupChange: null,					/* #2366 */
		waitingConfirmation: false,
		dndMarkChildTimeout: null,
		
		notParent: function (drag, targ) {
			while(targ) {
				if (targ === drag) return false;
				targ = targ.parentNode;
			}
			return true;
		},
		
		markTarget: function (el, place) {
			var self = dojo.dnd.HiSortDragExtra;
			
			if (el) {
				//If drop indicator doesn't exist, create it 
				if (!self.drop_indicator) {
					self.drop_indicator = document.createElement('DIV');
					self.drop_indicator.className = 'taskDropIndicator';
					self.drop_indicator.innerHTML = '<div class="l"></div><div class="r"></div>';
					
					document.body.appendChild(self.drop_indicator);
				}
				
				//Position
				var style = {
					'display': 'block',
					'width': 0,
					'left': 0,
					'top': 0
				};
				
				container = el;
				/*
				var container = getParentByClassQuick(el, 'tasks');
				if (!container) container = el;
				*/
				
				var pos_c = dojo.position(container, true);
					
				var pos = dojo.position(el, true);
				var pos_x = pos.x - 2;
				var pos_y = pos.y - 1;
				var drop_type = 'line';
				var width = el.offsetWidth + 4;
				var height = 0;
				
				if (place == 'after') {
					pos_y += el.offsetHeight;
				} else if (place == 'before') {
					//Do nothing...
				} else {
					//Element itself is a target
					drop_type = 'box';
					height = 34;
					pos_y -= 2;
					
					pos_x = pos_c.x - dojo.style(container, 'marginLeft') - 4;
					width = container.offsetWidth + dojo.style(container, 'marginLeft') + dojo.style(container, 'marginRight') - 3 + 7;
				}
				
				if (self.drop_type != drop_type) {
					if (drop_type == 'line') {
						self.drop_indicator.className = 'taskDropIndicator taskDropIndicatorLine';
					} else {
						self.drop_indicator.className = 'taskDropIndicator taskDropIndicatorBox';
					}
					self.drop_type = drop_type;
				}
				
				style.width = width + 'px';
				style.height = height + 'px';
				style.left = ~~pos_x + 'px';
				style.top  = ~~pos_y + 'px';
				
				dojo.style(self.drop_indicator, style);
				
				self.drop_place = place;
				self.drop_target = el;
			} else {
				//Remove drop indicator
				self.drop_target = null;
				self.drop_type = null;
				self.drop_place = null;
				
				if (self.drop_indicator) {
					self.drop_indicator.style.display = 'none';
				}
			}
		},
		
		resetAvatarText: function () {
			//JDT var m = dojo.dnd.Manager();
			var m = dojo.dnd.Manager.manager();
			m.avatar.resetOrigianlText();
		},
		
		resetDropIndicator: function () {
			dojo.dnd.HiSortDragExtra.markTarget(null);
		},
		
		onDndStart: function(source, nodes, copy) {
			if (dojo.dnd.HiSortDragExtra.allowSort) { 
				HiSortDragSource_dragNode = nodes[0];
				dojo.addClass(nodes[0], 'sortDropIndicatorClass');
				
				var id = nodes[0].taskId;
				
				var priority = parseInt(hi.items.manager.get(id, "priority"));
				dojo.dnd.HiSortDragExtra.initial_priority = priority;
				dojo.dnd.HiSortDragExtra.initial_priority_level = (priority >= 30000 ? 'high' : (priority < 20000 ? 'low' : 'medium'));
				dojo.dnd.HiSortDragExtra.force_priority = false;
				
				var parent = Project.getParentTask(id);
				dojo.dnd.HiSortDragExtra.initial_parent = parent;
			}
		},
		
		onDndCancel: function () {
			this.actionType = undefined;
			if (HiSortDragSource_dragNode) {
				dojo.removeClass(HiSortDragSource_dragNode, 'sortDropIndicatorClass');
			}
			if (dojo.dnd.HiSortDragExtra.allowSort && HiSortDragSource_dragNode) {
				dojo.dnd.HiSortDragExtra.setSortingDelay.call(this);
			}
		},
		
		onDndDrop: function (_1b, _1c, _1d, _1e) {
			this.actionType = undefined;
			if (HiSortDragSource_dragNode) {
				dojo.removeClass(HiSortDragSource_dragNode, 'sortDropIndicatorClass');
			}
			if (dojo.dnd.HiSortDragExtra.allowSort && HiSortDragSource_dragNode) {
				dojo.dnd.HiSortDragExtra.setSortingDelay.call(this);
			}
		},
		
		onMouseMove: function (e) {
			if (!dojo.dnd.HiSortDragExtra.allowSort) return;
			
			//JDT var m = dojo.dnd.Manager();
			var m = dojo.dnd.Manager.manager(),
				self = dojo.dnd.HiSortDragExtra,
				sortingByPriority = (Project.getSorting() == Project.ORDER_PRIORITY),
				avatarText;
			
			//if (m.canDropFlag && this.current && m.source && m.source.declaredClass == this.declaredClass) {
			//Only in "By priority" sorting mode it's allowed to change order. Validate source and target
			if (HiSortDragSource_dragNode && m.source && m.source.declaredClass == this.declaredClass) {
				if (this.current) {
					//var id = this.current.taskId;
					var id = this.current ? dojo.getAttr(this.current, 'data-task-id') : undefined;
					self.last_priority = self.initial_priority;
					self.last_priority_level = self.initial_priority_level;

					if (dojo.hasClass(this.current, 'task_empty')) {
						//Moving to another group, priority should stay same
						self.last_priority = self.initial_priority;
						self.last_priority_level = self.initial_priority_level;
						
						self.setPriorityText(m, self.last_priority_level, self.last_priority);
						
						//No target to mark
						self.markTarget(null);
						
					} else if (HiSortDragSource_dragNode != this.current && self.notParent(HiSortDragSource_dragNode, this.current)/* commented because of #3162 && hi.items.manager.get(id, "category") != 5*/) {
						
						//Calculate coordinates
						if (!this.targetBox || this.targetBoxCurrent != this.current) {
							this.targetBox = {
								xy: dojo.coords(this.current, true),
								w: this.current.offsetWidth,
								h: this.current.offsetHeight
							};
							this.targetBoxCurrent = this.current;
						}
						
						var new_priority = null, new_priority_level = null, force_priority = false;
						
						//Is mouse inside item or near the edge?
						var hotspot_height = 10;
						var offsetY = e.pageY - this.targetBox.xy.y;
						var max = this.targetBox.h - hotspot_height - 2;
						
						if (offsetY > hotspot_height && offsetY < max) {
							this.inside = true;
						} else {
							this.inside = false;
							this.before = (offsetY <= hotspot_height); 
						}
						
						if (this.inside) {
							// if (sortingByPriority) {	// Change #5219
							// 	return self.markTarget(null);
							// }
							var t = this;
							if (!this.dndMarkChildTimeout) {
								this.dndMarkChildTimeout = setTimeout(function() {
									if (!m || !m.avatar) return false;
									
									self.markTarget(t.current, 'inside');
									
									//Drag tooltip should change to "Make child of another task" or "Move out of task"
									if (id != self.initial_parent) { 
										//If draging file, then text should be "Attach to item"
										if (hi.items.manager.get(HiSortDragSource_dragNode.dragId, "category") == 5) {
											avatarText = hi.i18n.getLocalization('project.make_task_attach');
										} else {
											avatarText = hi.i18n.getLocalization('project.make_task_nested');
										}
										self.actionType = 'addSub';
										m.avatar && m.avatar.updateText(avatarText, true);
									}
								}, 500);
							}

						} else if (this.before) {
							/*
							 * #3227
							 * current is the item we are dragging into in a downward motion
							 * previous is either null, or the item before (above; equal to or higher in priority)
							 * previous is null when we are at the top of the list
							 */
							if (this.dndMarkChildTimeout) {
								clearTimeout(this.dndMarkChildTimeout);
								this.dndMarkChildTimeout = null;
							}
							self.markTarget(this.current, 'before');
							var item;
							if (sortingByPriority) {
								var previous_element = previousElement(this.current),
									previousTaskId = previous_element?dojo.dom.getTaskId(previous_element) : null,
									previousItem = previousTaskId?hi.items.manager.get(previousTaskId) : null,
									currentTaskId = dojo.dom.getTaskId(this.current)||null,
									currentItem = currentTaskId?hi.items.manager.get(currentTaskId) : null;
									
								item = hi.items.manager.get(id);
								if (item) {
									new_priority = parseInt(item.priority);
									
									if (previousTaskId && previousTaskId == item.id)
									{
										//Don't change because we are just bumping into the next item down
										new_priority = self.initial_priority;
									} else
									{
										//Otherwise calculate the average priority between the two items
										if (previousItem  && currentItem)
										{
											new_priority  = Math.round( (currentItem.priority + previousItem.priority)/2 );
											force_priority = true;
										} else if (currentItem)
										{
											//We are at the top, so just add 10
											new_priority = currentItem.priority+10;
											force_priority = true;
										} else if (previous_element)
										{
											//The old way; which doesn't really work
											if (new_priority < 20000) {
												new_priority = 20000;
												force_priority = true;
											} else if (new_priority < 30000) {
												new_priority = 30000;
												force_priority = true;
											}
										} else
										{
											// Catch-all
											new_priority = self.initial_priority;
										}
									} 
									/*
									//"Change priority to High" if inserting before first element
									if (!previousElement(this.current)*||previousElement(this.current) != this.current*) {
										if (new_priority < 20000) {
											new_priority = 20000;
											force_priority = true;
										} else if (new_priority < 30000) {
											new_priority = 30000;
											force_priority = true;
										}		
									}
									*/
								} else {
									//Only item in group
									new_priority = self.initial_priority;
								}
							}
						} else {
							if (this.dndMarkChildTimeout) {
								clearTimeout(this.dndMarkChildTimeout);
								this.dndMarkChildTimeout = null;
							}
							self.markTarget(this.current, 'after');
							/*
							 * #3227
							 * current is the item we are dragging into in an upward motion
							 * next is either null, or the item before (below; equal to or lower in priority than current)
							 * next is null when we are at the bottom of a list
							 */
							if (sortingByPriority) {
								var next_element = nextElement(this.current),
									nextTaskId = next_element?dojo.dom.getTaskId(next_element) : null,
									nextTaskItem = nextTaskId?hi.items.manager.get(nextTaskId) : null,
									currentTaskId = dojo.dom.getTaskId(this.current) || null,
									currentItem = currentTaskId?hi.items.manager.get(currentTaskId) : null;
								var is_next = false;
								item = null;

								if (!currentTaskId) {
									//Should never reach here
									new_priority = self.last_priority;
								} else {
									if (nextTaskItem) {
										//Just calculate the average priority between the two items
										force_priority = true;
										new_priority = Math.round( (currentItem.priority+nextTaskItem.priority)/2 );
									} else {
										//We're at the bottom of the list, so just subtract 10 from the current item
										force_priority = true;
										new_priority = currentItem.priority-10;
									}
								}
								/*
								if (next_element) {
									next_element = next_element.taskId;
									if (next_element && (item = hi.items.manager.get(next_element))) {
										new_priority = parseInt(item.priority);
										is_next = true;
									}
								}
								
								if (!is_next && (item = hi.items.manager.get(id))) {
									//"Change priority to Low" if inserting after last element
									var p = parseInt(item.priority);
									if (p > 20000) {
										new_priority = Project.getMaxPriority(1) + 1;
										force_priority = true;
									}
									else if (p > 10000) {
										new_priority = Project.getMaxPriority(0) + 1;
										force_priority = true;
									}
								}
								*/
							}
						}
						
						var text_changed = false;
						
						if (!this.inside && id && hi.items.manager.get(id)) {
							var new_parent_id = Project.getParentTask(id);
							if (self.initial_parent != new_parent_id) {
								// if (sortingByPriority) {
								// 	return self.markTarget(null);		// Change #5219
								// }

								if (new_parent_id) {
									//If file is a sub-task, then it's attached to that item
									if (hi.items.manager.get(HiSortDragSource_dragNode.dragId, "category") == 5) {
										avatarText = hi.i18n.getLocalization('project.make_task_attach');
									} else {
										avatarText = hi.i18n.getLocalization('project.make_task_nested');
									}
									m.avatar.updateText(avatarText, true);
								} else {
									m.avatar.updateText(hi.i18n.getLocalization('project.move_task_out'), true);
								}
								text_changed = true;
							}
						}
						
						if (sortingByPriority && new_priority !== null) {
							new_priority_level = (new_priority >= 30000 ? 'high' : (new_priority < 20000 ? 'low' : 'medium'));
							
							if (!text_changed && /*#2366 */!self.groupChange) {
								self.setPriorityText(m, new_priority_level, new_priority);
							}
							
							self.last_priority_level = new_priority_level;
							self.last_priority = new_priority;
							self.force_priority = force_priority;
						}
					} else {
						if (sortingByPriority &&  /*#2366 */!self.groupChange) {
							self.setPriorityText(m, self.last_priority_level, self.last_priority);
						}
					}
				}
			}
		},
		
		setPriorityText: function (m, level, priority) {
			if (m.source instanceof dojo.dnd.HiDragSource) {
				if (dojo.dnd.HiSortDragExtra.initial_priority_level != level) {
					m.avatar.updateText(hi.i18n.getLocalization('priority.change_priority') + ' ' + hi.i18n.getLocalization('priority.' + level), true);
					this.dropTarget = null;
				} else if (dojo.dnd.HiSortDragExtra.initial_priority != priority) {
					var txt = '';
					if (dojo.dnd.HiSortDragExtra.initial_priority > priority)
						txt = hi.i18n.getLocalization('priority.decrease');
					else
						txt = hi.i18n.getLocalization('priority.increase');
						
					m.avatar.updateText(txt, true);
					this.dropTarget = null;
				}
			}
		},
		
		setSortingDelay: function () {
			dojo.global.isSorting = true;
			setTimeout(function () {
				dojo.global.isSorting = false;
			}, 100);
		}
	};

	return dojo.dnd.HiSortDragExtra;
	
});