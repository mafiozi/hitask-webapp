define([
	'dojo/string',
	'dojo/_base/array'
], function(string, array) {
	dojo.global.Hi_Permissions = (function() {
		var permissions = [
			["permissions.title_20",	20,		"&#xf06e;"],	//View, Comment
			["permissions.title_50",	50,		"&#xe039;"],	//Complete, Assign
			["permissions.title_60",	60,		"&#xf044;"],	//Modify
			["permissions.title_100",	100,	"&#xe005;"]		//Everything
		];

		// Feature #4579
		return {
			VIEW_COMMENT: 20,
			COMPLETE_ASSIGN: 50,
			MODIFY: 60,
			EVERYTHING: 100,
			
			// Following not used in Permissions Widget but required.
			SHARE: 70,
			CHILDREN: 30,
			ASSIGN: 40,
			COMPLETE: 50,
			ARCHIVE: 80,
			DELETE: 100,

			getPermissions: function(){
				return permissions;
			}
		};
	})();
	
	dojo.global.Hi_Actions = {
		'delete': Hi_Permissions.DELETE,
		'duplicate': Hi_Permissions.DELETE,
		'modify': Hi_Permissions.MODIFY,
		'attach': Hi_Permissions.CHILDREN,
		'assign': Hi_Permissions.ASSIGN,
		'complete': Hi_Permissions.ASSIGN,
		'archive': Hi_Permissions.ARCHIVE,
		'addSub': Hi_Permissions.CHILDREN,
		'removeChild': Hi_Permissions.CHILDREN,
		'makeStarred': Hi_Permissions.MODIFY,
		'logTime': Hi_Permissions.COMPLETE,

		_getPermissionWarning: function(action, item) {
			var template = hi.i18n.getLocalization('common.not_permitted_warning'),
				// task = t.task,
				owner = item.user_id,
				ownerName = 'Owner',
				permissions = item.permissions,
				additionalOwners = [],
				k, ao;

			action = hi.i18n.getLocalization('common.' + action + 'Action');
			array.forEach(Project.friends, function(fr) {
				if (fr.id === owner) {
					ownerName = fr.name;
				}
			});

			for (k in permissions) {
				if (permissions[k] === 100 && k !== 'everyone' && parseInt(k, 10) !== owner) {
					ao = Hi_Team.getContactById(k);
					if(!ao) continue;

					additionalOwners.push(ao.name || ao.email);
				}
			}

			if (additionalOwners.length > 2) additionalOwners.length = 2;
			additionalOwners = additionalOwners.join(', ');
			additionalOwners = additionalOwners ? 'or ' + additionalOwners  + ' ': '';

			// console.log(template, task, owner, permissions);
			// console.log(string.substitute(template, [action, ownerName, owner, action]));

			return string.substitute(template, [action, ownerName, additionalOwners, action]);
		},

		validateAction: function(action, item/*Object|Number*/, noMessage) {
			item = typeof item === 'object' ? item : hi.items.manager.get(item);

			if (!this.hasOwnProperty(action) || item.permission < this[action]) {
				if (!noMessage) {
					Project.alertElement.showItem(28, {alert: this._getPermissionWarning(action, item)});
				}
				return false;
			}
			return true;
		},

		getPermissionByUserId: function(pms, id) {
			if (!pms || !pms.length) return null;

			var len = pms.length, temp;

			for (var i = 0; i < len; i++) {
				temp = pms[i];

				if (('' + temp.principal) === ('' + id)) return temp.level;
			}

			return null;
		},

		getPermissionObjByUserId: function(pms, id) {
			if (!pms || !pms.length) return null;

			var len = pms.length, temp;

			for (var i = 0; i < len; i++) {
				temp = pms[i];

				if (('' + temp.principal) === ('' + id)) return temp;
			}

			return null;
		},

		deletePermissionByUserId: function(pms, id) {
			if (!pms || !pms.length) return null;

			var len = pms.length, temp;

			for (var i = 0; i < len; i++) {
				temp = pms[i];

				if (('' + temp.principal) === ('' + id)) return pms.splice(i, 1);
			}

			return null;
		}
	};
});