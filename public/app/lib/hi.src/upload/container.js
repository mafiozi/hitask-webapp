//dojo.provide("hi.upload.container");

/**
 * Container for uploaders
 * Flash resets if flash object is moved (when task is closed), so need to put it in container and
 * move it to needed screen position manually to prevent it.
 */
define(function () {
	
	dojo.global.FileUploadContainer = {
		/**
		 * Container node
		 * @type {HTMLElement}
		 */
		nodeContainer: null,
		
		/**
		 * Initialization state
		 * @type {Boolean}
		 */
		initialized: false,
		
		/**
		 * File instances
		 * @type {Object}
		 */
		files: {},
		
		/**
		 * Add node to container
		 */
		addNode: function (node) {
			this.init();
			this.nodeContainer.appendChild(node);
		},
		
		/**
		 * Add file to container
		 * @param {Object} uploader
		 */
		addFile: function (file) {
			this.init();
			this.files[file.id] = file;
			
			file.fileUploadContainer = document.createElement('DIV');
			file.fileUploadContainer.style.position = 'absolute';
			file.fileUploadContainer.style.left = '-1000px';
			file.fileUploadContainer.style.top = '-1000px';
			
			var overlayOpacity = 0.01;
			//Opacity can not be 0 for Safari5 compability
			//if (dojo.isWebKit) overlayOpacity = 0;
			dojo.style(file.fileUploadContainer, 'opacity', overlayOpacity);
			
			var isOver = false;
			
			dojo.connect(file.nodeButton, 'mouseover', function () {
				if (isOver) return;
				isOver = true;
				
				var pos = dojo.position(file.nodeButton, true);
				file.fileUploadContainer.style.left = ~~(pos.x - 6) + 'px';
				file.fileUploadContainer.style.top = ~~(pos.y - 4) + 'px';
				file.fileUploadContainer.style.zIndex = 9000;
			});
			
			function outDelayed () {
				isOver = false;
				
				file.fileUploadContainer.style.left = '-1000px';
				file.fileUploadContainer.style.top = '-1000px';
			}
			dojo.connect(file.fileUploadContainer, 'mouseout', function () {
				if(dojo.isSafari){
					outDelayed();
				}else{
					setTimeout(outDelayed, 100);
				}
			});
			
			file.fileUploadContainer.appendChild(file.nodeButtonOverlay);
			this.nodeContainer.appendChild(file.fileUploadContainer);
		},
		
		/**
		 * Destroy file upload widget
		 * @param {Object} taskId
		 */
		destroy: function (fileId) {
			if (fileId in this.files) {
				var file = this.files[fileId];
				
				if (file.nodeButtonOverlay) {
					removeNode(file.nodeButtonOverlay);
					delete(file.nodeButtonOverlay);
				}
				if (file.fileUploadContainer) {
					removeNode(file.fileUploadContainer);
					delete(file.fileUploadContainer);
				}
				this.remove(fileId);
			}
		},
		
		/**
		 * Remove uploader without destroying it
		 * @param {Object} taskId
		 */
		remove: function (fileId) {
			if (fileId in this.files) {
				delete(this.files[fileId]);
			}
		},
		
		/**
		 * Returns file uploader widget
		 * @param {Object} taskId
		 */
		get: function (fileId) {
			if (fileId in this.files) {
				return this.files[fileId];
			} else {
				return null;
			}
		},
		
		/**
		 * Returns file widget
		 * @param {Object} guid Unique file ID
		 */
		getFile: function (fileId) {
			if (fileId in this.files) {
				return this.files[fileId];
			}
			
			return null;
		},
		
		init: function () {
			if (this.initialized) return;
			this.initialized = true;
			
			var nodeContainer = this.nodeContainer = document.createElement('DIV');
			nodeContainer.onclick = function (e) {
				var evt = e || window.event;
				
				if (evt.stopPropagation) evt.stopPropagation();
				evt.cancelBubble = true;
			};
			nodeContainer.className = 'file-upload-container';
			
			document.body.appendChild(nodeContainer);
			
		}
	};
	
	return dojo.global.FileUploadContainer;

});