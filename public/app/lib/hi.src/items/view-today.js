/**
 * Instantiation of task lists (and the groups that contain them) on My View tab:
 * hi.items.TaskListStarred
 * hi.items.TaskListUngrouped
 * hi.items.TaskListCompletedToday
 */
define([
	'hi/store',
	'hi/items/base',
	'dojo/aspect',
	'hi/items/date'
], function (hi_store, hi_items, dojo_aspect) {
	

	//After hi.store.init create MyView groups
	dojo.global.viewTodayTab = function(q) {
	//dojo.connect(hi_store, "afterInit", hi_store, function () {
		// Create task groups (Starred, Completed and default list) and set filters
		//console.log('afterinit',q.length);
		
		var readStore = hi_store.ProjectCache,
			writeStore = hi_store.Project,
			query = q,//readStore.query(),
			
			groupSelf = 'today',
			groupCurrent = Hi_Preferences.get('currentTab', 'my', 'string'),
			getDayDiff = dojo.global.viewDate.getDayDiff,

			myCalculateInstanceDate = function(id) {
				/*
				 * Summary:
				 * Overrides the default action of calculateInstanceDate method
				 */
				//Find first non-removed,non-completed instance date in series that occurs today
				var item = typeof id === 'object' ? id : this.readStore.get(id),
					today = new Date(),
					series;

				series = hi.items.date.generateRecurrenceSeries(item, time2str(today,'y-m-d'), time2str(today,'y-m-d'));
				if (series && series.length > 0)
					return series[0];
				else
					return null;
			},

			isToday = function(item) {
				if (item.recurring) {
					return myCalculateInstanceDate(item);
				}
				if (getDayDiff(item) === 0) return true;
				return false;
			};

		//My view
		if (groupCurrent == groupSelf){
			var myV = dojo.connect(hi_store, 'afterLoad', function() {
				// Add to container
				if (groupSelf == groupCurrent){
					finalizeInitialization([hi_items.GroupCompletedToday], [hi_items.TaskListTodayMy]);
				}
				dojo.disconnect(myV);
			});
		}

		var user_id = Project.user_id;

		hi_items.TaskListTodayMy = new hi.widget.TaskList({
			"title": 'today',
			"readStore": readStore,
			"writeStore": writeStore,
			"query": query,//groupSelf == groupCurrent?query:null,
			"disabled": groupSelf != groupCurrent,
			"showing": groupSelf == groupCurrent? true : false,
			"filterType": "any", // If item or any of the parent matches filter then this item can be shown in this list
			"completedSubList": false,

			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				var instance_date;
				if (item.recurring) {
					instance_date = this.calculateInstanceDate(item);
					if (!instance_date) return false;
				}

				if (getDayDiff(item) > 0) return false;
				if (item.assignee && item.assignee !== user_id) return false;
				// if (!strict && !this.filterMatches(item, "any", "filterCompleted")) return false;

				if (item.recurring && hi.items.manager.getIsCompleted(item, instance_date)) return false;
				if (!item.recurring && hi.items.manager.getIsCompleted(item)) return false;
				
				if (getDayDiff(item) === 0) return true;
				// if (!strict && this.filterMatches(item.parent)) return true;
				if (this.filterMatches(item, /*#2403 'all'*/"any", "filterMy")) return true;

				return false;
			},

			"filterMy": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				/* #3229 - check there are any instances left */
				if (item.recurring) {
					if (!this.calculateInstanceDate(item)) return false;
				}
				if (item.assignee && item.assignee == user_id) return true;
				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			},

			"filterCompleted": function(item, strict) {
				// summary:
				//		Filter completed items, if any parent is not completed, then test passed
				// item:
				//		Item data
				// strict:
				//		If true then don't check ancestors
				
				//return !item.completed && (strict || !item.parent || !item.category || this.filterMatches(item.parent, "any", "filterCompleted"));
				/* #2493 - filter by instances */
				if (item.recurring) {
					if (!this.calculateInstanceDate(item)) {
						return false;
					}
					if (hi.items.manager.getIsCompleted(item,this.calculateInstanceDate(item))) {
						return false;
					} else {
						return true;
					}
					//if (!recurringItemMatchesDate(item,HiTaskCalendar.getStrFromDate(HiTaskCalendar.today))) return false;
				}
				if (hi.items.manager.getIsCompleted(item/*#1203 proposed,time2str(new Date(),'y-m-d')*/)) return false;
				
				//If can check parent, do so
				if (!strict) {
					if (item.parent && item.category) {
						return this.filterMatches(item.parent, "any", "filterCompleted");
					}
				}
				return true;
			},

			myCalculateInstanceDate: function(id) {
				/*
				 * Summary:
				 * Overrides the default action of calculateInstanceDate method
				 */
				//Find first non-removed,non-completed instance date in series that occurs today
				var item = typeof id === 'object' ? id : this.readStore.get(id),
					today = new Date(),
					series;
					
				series = hi.items.date.generateRecurrenceSeries(item, time2str(today,'y-m-d'), time2str(today,'y-m-d'));
				if (series && series.length > 0)
					return series[0];
				else
					return null;
			}
		});

		hi_items.GroupCompletedToday = new hi.widget.Group({
			"projectId": "completed",
			"emptyVisible": false, // don't show when empty
			"expanded": false,     // by default not expanded
			'groupSubmenusVisible': true,
			'animationEnabled': true,
			"title": hi.i18n.getLocalization("completed.title"),
			"moveTitle": "",
			"groupClassName": "day project-completed group-completed",
			"disabled": groupSelf != groupCurrent
		});
		
		hi_items.TaskListCompletedToday = new hi.widget.TaskList({
			"readStore": readStore,
			"writeStore": writeStore,
			"query": q,		//groupSelf == groupCurrent?query:null,
			showing: groupSelf == groupCurrent? true : false,
			initialyCollapsed:true,
			"localeItemCollapsed": hi.i18n.getLocalization("center.completed_item_count").replace(/%s\s+/,' '),
			"localeItemsCollapsed": hi.i18n.getLocalization("center.completed_items_count").replace(/%s\s+/,' '),
			"disabled":groupSelf != groupCurrent,
			"completedSubList": false,	// All items already are completed, no need for sublist
			 // Bug #2040 - change filter type from 'all' to 'top' - if top ancestor is completed, so are all of its desendents 
			"filterType": "top", 		// If item and all parents are completed and not starred then show in list
			"filter": function(item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				/*Change #4771*/
				if (item.category === hi.widget.TaskItem.CATEGORY_EVENT && getDayDiff(item) < 0) {
					return false;
				}
				if (item.recurring) {
					var instance_date = this.calculateInstanceDate(item);
					if (!instance_date) return false;
					if (!item.starred && hi.items.manager.getIsCompleted(item,instance_date)) return true;
					else return false;
				}
				if (hi.items.manager.getIsCompleted(item)) return true;
				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			},

			/*
			 * This tell GroupCompletedToday to set the collapsed attribute to false, so that completed items are only added
			 * to the DOM on a user-on-demand basis
			 */
			afterExpand: function() {
				this.set('collapsed',false);
			}
		});
		
		// Add to container
		var container = dojo.byId(Project.getContainer('tasksList'));
		
		hi_items.TaskListTodayMy.placeAt(container);
		
		hi_items.GroupCompletedToday.placeAt(container);
		hi_items.TaskListCompletedToday.placeAt(hi_items.GroupCompletedToday);
		
		dojo.connect(hi.items.manager, "groupingChange", hi.items.manager, function (group, groupPrev) {
			// summary:
			//		On group change enable/disable lists
			
			if (group == groupSelf && groupPrev != groupSelf) {
				refreshTab([/*hi_items.GroupTodayMy,*/hi_items.GroupCompletedToday], [hi_items.TaskListTodayMy], true);
			} else if (group != groupSelf && groupPrev == groupSelf) {
				// hi_items.GroupTodayMy.set("disabled", true);
				hi_items.TaskListTodayMy.set("disabled", true);
				hi_items.GroupCompletedToday.set("disabled", true);
			}
			
			groupCurrent = group;
		});
		
		dojo.connect(Project, "changeSorting", Project, function () {
			// summary:
			//		On sorting change empty lists
			if (groupCurrent == groupSelf) {
				/* #2509
				hi_items.TaskListStarred.reset({"fill": false});
				hi_items.TaskListUngrouped.reset({"fill": false});
				hi_items.TaskListCompletedToday.reset({"fill": false});
				*/
				// hi_items.GroupTodayMy.set("disabled", true);
				hi_items.TaskListTodayMy.set("disabled", true);
				hi_items.GroupCompletedToday.set("disabled", true);
			}
		});
		
		dojo.connect(hi.items.manager, "filterChange", hi.items.manager, function () {
			// summary:
			//		On sorting change empty lists
			/* #2509
			if (groupCurrent == groupSelf) {
				hi_items.TaskListStarred.reset({"fill": false});
				hi_items.TaskListUngrouped.reset({"fill": false});
				hi_items.TaskListCompletedToday.reset({"fill": false});
			}
			*/
			if (groupCurrent == groupSelf) {
				hi_items.TaskListTodayMy.set('disabled',true);
				// hi_items.TaskListUngrouped.set('disabled',true);
				hi_items.TaskListCompletedToday.set('disabled',true);
			}
			
		});
		
		/* This is invoked after refresh data store on filter selection change 
		 * This should just initiate the query observers on the lists and enable the lists
		 */
		dojo.connect(hi.items.manager, "redraw", hi.items.manager, function () {
			// summary:
			//		Some major change happened, redraw lists
			if (groupCurrent == groupSelf) {
				/* #2331 - rereshTab on searchChange */
				//if (hi.items.TaskListStarred.get('disabled')) initiateTab([hi_items.GroupStarred,hi_items.GroupCompletedToday],[hi_items.TaskListUngrouped]);
				/*else*/ 
				refreshTab([hi_items.GroupCompletedToday], [hi_items.TaskListTodayMy]);
			}
		});
		
		hi.items.manager.getTaskLists[groupSelf] = function () {
			// summary:
			//		Return all task lists for this group
		
			return [
				hi_items.TaskListTodayMy,
				hi_items.TaskListCompletedToday
			];
		};
	};
});