define(function () {
	
	return dojo.global.hi.i18n = {
		
		/**
		 * Returns localization for hi module
		 * 
		 * @param {String} key Dot separated path to value
		 * @return Localized string
		 * @type {String}
		 */
		'getLocalization': function (key) {
			key = dojo.trim(key).split('.');
			var data = dojo.i18n.getLocalization('hi', 'hi');
			
			if (key.length > 1) {
				return (typeof data[key[0]] != 'undefined' ? data[key[0]][key[1]] : '');
			} else {
				return data[key[0]];
			}
		}
		
	};
	
});
