import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.UnknownFormatConversionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import play.Logger;
import play.i18n.Messages;


public class LocalizationTest extends GroovyViewsBaseTest {
	public static final String MESSAGE_USAGE_PATTERN_IMPLICIT = "&\\{'([a-zA-Z._]+)'(,.*)?\\}";
	public static final String MESSAGE_USAGE_PATTERN_EXPLICIT = "\\$\\{messages\\.get\\('([a-zA-Z._]+)'\\).*\\}";
	public static final String MESSAGE_USAGE_PATTERN_JS = "i18n\\('([a-zA-Z._]+)'(,.*)?\\)";
	public static final String OLD_MESSAGE_USAGE_PATTERN = "(\\{#[a-zA-Z_]+#[\\w\\$\\|:'\"%]*\\})";
	
	/**
	 * Test searches for old messages in views using regular expression.
	 */
	@Test
	public void searchForOldLocalizationMessages() {
		Iterator<File> fileIterator = getAllViews();
		Pattern pattern = Pattern.compile(OLD_MESSAGE_USAGE_PATTERN, Pattern.MULTILINE);
		long counter = 0;
		Set<String> fileNames = new HashSet<String>();
		
		while (fileIterator.hasNext()) {
			File view = fileIterator.next();
			String content = getFileAsString(view, true);
			Matcher matcher = pattern.matcher(content);
			
			while (matcher.find()) {
				String match = matcher.group(1);
				if (match != null) {
					counter++;
					fileNames.add(view.getAbsolutePath());
				}
			}
		}
		
		assertEquals(String.format("Found %d old messages in following files: %s", counter, StringUtils.join(fileNames, ", ")), 0, counter);
	}
	
	@Test
	public void searchForUnusedLocalizationMessages() {
		Logger.info("Starting searchForUnusedLocalizationMessages test");
		
		for (Entry<String, Properties> messages : Messages.locales.entrySet()) {
			String locale = messages.getKey();
			Logger.info("Found locale: %s", locale);
			
			// Only process en.
			if (locale.equalsIgnoreCase("en")){
				UnusedMessagesResult result = new UnusedMessagesResult(locale);
				
				for (Object key : messages.getValue().keySet()) {
					if (key instanceof String) {
						String messageKey = (String)key;
						
						boolean found = searchForUnusedLocalizationMessages(getAllViews(), messageKey)
							|| searchForUnusedLocalizationMessages(getAllJavascripts(), messageKey)
							|| searchForUnusedLocalizationMessages(getAllJsViews(), messageKey)
							|| searchForUnusedLocalizationMessages(getAllJava(), messageKey);
						
						if (!found) {
							result.addUnusedMessage(messageKey);
						}
					}
				}
				
				Logger.info("locale contains %d messages", messages.getValue().keySet().size());
				Logger.info("locale contains %d unused messages", result.getUnusedMessages().length);
				if (result.hasUnused()) {
					Logger.warn("%s", result);
				}
			}
		}
		
		Logger.info("Finished searchForUnusedLocalizationMessages test");
	}
	
	/**
	 * Inspired by #4006. This test tries to find all messages with "%" placeholders and test if these placeholders valid.
	 */
	@Test
	public void checkFormatterPlaceholders() {
		Object[] dummyParams = {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null};
		
		for (Entry<String, Properties> messages : Messages.locales.entrySet()) {
			for (Object obj : messages.getValue().keySet()) {
				if (obj != null && obj instanceof String) {
					String key = (String)obj;
					String message = messages.getValue().getProperty(key);
					// Only with placeholder and not JS message (they may contain various custom placeholders).
					if (message != null && message.contains("%") && !key.startsWith("js.")) {
						try {
							String.format(message, dummyParams);
						} catch (UnknownFormatConversionException e) {
							Logger.error(e, e.getMessage());
							fail("locale " + messages.getKey() + " contains invalid formatter placeholder; see logs for more details; key [" + key + "]; message [" + message + "]");
						}
					}
				}
			}
		}
	}
	
	private boolean searchForUnusedLocalizationMessages(Iterator<File> fileIterator, String messageKey) {
		boolean foundAnyUsage = false;
		List<String> searchPatterns = new ArrayList<String>();
		searchPatterns.add("&{'" + messageKey);
		searchPatterns.add("${messages.get('" + messageKey);
		searchPatterns.add("i18n('" + messageKey);
		searchPatterns.add("\"" + messageKey + "\"");
		
		// Special case for Dojo JS localization.
		final String jsPrefix = "js.";
		if (messageKey.startsWith(jsPrefix)) {
			searchPatterns.add("hi.i18n.getLocalization('" + StringUtils.replace(messageKey, jsPrefix, "", 1));
			searchPatterns.add("hi.i18n.getLocalization(\"" + StringUtils.replace(messageKey, jsPrefix, "", 1));
			searchPatterns.add("{#" + StringUtils.replace(messageKey, jsPrefix, "", 1) + "#}");
		}
		
		while (fileIterator.hasNext() && !foundAnyUsage) {
			File f = fileIterator.next();
			String content = getFileAsString(f, false);
			for (String search : searchPatterns) {
				if (!foundAnyUsage) {
					foundAnyUsage = foundAnyUsage || StringUtils.contains(content, search);
				}
			}
		}
		
		return foundAnyUsage;
	}
	
	public static class UnusedMessagesResult {
		private String locale;
		private Set<String> messages = new HashSet<String>();
		
		public UnusedMessagesResult(String locale) {
			this.locale = locale;
		}
		
		public void addUnusedMessage(String message) {
			messages.add(message);
		}
		
		public String getLocale() {
			return locale;
		}
		
		public String[] getUnusedMessages() {
			return messages.toArray(new String[messages.size()]);
		}
		
		public boolean hasUnused() {
			return messages.size() != 0;
		}
		
		public boolean hasUnused(String key) {
			return messages.contains(key);
		}
		
		@Override
		public String toString() {
			Comparator<String> comparator = new Comparator<String>() {
				@Override
				public int compare(String arg0, String arg1) {
					return arg0.compareToIgnoreCase(arg1);
				}
			};
			Set<String> treeSet = new TreeSet<String>(comparator);
			treeSet.addAll(messages);
			return new StringBuilder().append("Unused messages in locale ").append(locale).append(": \n").append(StringUtils.join(treeSet, "\n ")).toString();
		}
	}
}