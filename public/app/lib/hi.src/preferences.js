/**
 * Copyright (C) 2005-2006 Vide Infra Grupa SIA, Riga, Latvia. All rights reserved.
 * http://videinfra.com
 * Version 1.1
 */

define(['dojox/storage/manager'], function (storageManager) {
	
	dojo.global.Hi_Preferences = {
		interval: 60,
		started: false,
		changed: false,
		preferences: {
			grouping: 0,
			calendar: true,
			team: true
		},
		projectPreg: /^p_4_(\d+)$/,
		preferencesStoragedKeys: [
			'time_zone_offset',
			'tagfilter',
			'filter',
			'sorting',
			'projectSorting',
			'grouping',
			'currentTab',
			'sidebar_messenger',
			'team_size',
			'conversation_size',
			'hide_team_help',
			'sidebar_calendar',
			'closed_news',
			'p_4_'	/* projects expand in GroupByProject*/
		],
		set: function(n, i, silent) {
			if (n.indexOf('ts_') == 0) Hi_Preferences.lastActiveTask = null;
			if (n == '_') return;
			if (n in Hi_Preferences.preferences && Hi_Preferences.preferences[n] == i) return;
			Hi_Preferences.preferences[n] = i;
			 
			var storegedKey = n;
			if(Hi_Preferences.projectPreg.test(n)){
				storegedKey = 'p_4_';
			}
			
			if(in_array(Hi_Preferences.preferencesStoragedKeys, storegedKey)){
				// save to storage preferences listed in "preferencesStoraged"
				if (storageManager.isAvailable()) {
					dojox.storage.put(n, i, null, preferences_ns);
					if (typeof(Hi_Address) != 'undefined') {
						Hi_Address.remove(n);
					}
				}else{
					// if storage isn't available, then set preference to url 
					if (typeof(Hi_Address) != 'undefined') {
						Hi_Address.set(n, i, silent);
					}
				}
			}else{
				if (!Hi_Preferences.changed){
					if (!Hi_Preferences.started) {
						Hi_Preferences.started = dojo.global.setInterval(Hi_Preferences.send, Hi_Preferences.interval * 1000);
					}
					Hi_Preferences.changed = {};
					Hi_Preferences.changed[n] = i;
					if (typeof(Hi_Address) != 'undefined') {
						Hi_Address.set(n, i, silent);
					}
				}
			}
		},
		get: function (s, d, type) {
			type = type || false;
			d = ((d !== undefined) && (d !== null)) ? d : false;
			if (s in Hi_Preferences.preferences) {
				switch (type) {
					case 'int':
						return parseInt(Hi_Preferences.preferences[s]);
					case 'bool':
						var val = Hi_Preferences.preferences[s];
						if (typeof(val) == 'string' && (val.toLowerCase() == 'false' || val.toLowerCase() == 'true')) {
							return val.toLowerCase() == 'true';
						} else {
							return parseInt(val) ? true : false;
						}
					case 'string':
						return String(Hi_Preferences.preferences[s]);
					default:
						return Hi_Preferences.preferences[s];
				}
			} else {
				return d;
			}
		},
		remove: function (k) {
			Hi_Preferences.set(k, 'null');
		},
		clear: function() {
			var keysToClear = [];
			var keysAll = dojox.storage.getKeys(preferences_ns);
			for(var i in keysAll){
				
				// claer project
				var projectPregResult = Hi_Preferences.projectPreg.exec(keysAll[i]);
				if(projectPregResult) {
					if (!hi.items.manager.get(projectPregResult[1])) {
						keysToClear.push(keysAll[i]);
					}
				}
			}
			if(keysToClear.length > 0){
				dojox.storage.removeMultiple(keysToClear, preferences_ns);
			}
		},
		send: function(callback) {
			callback = (typeof callback == 'function' ? callback : function (){});
			
			if (Hi_Preferences.changed) {
				Project.ajax('prefchange', Hi_Preferences.changed, function (data) {
					Hi_Preferences.receive(data);
					callback(data);
				}, function() {
					callback();
					ALERT('Prefs not saved! Will try later!');
				});
				
				return;
			}
			
			callback();
		},
		receive: function (data){
			if (data && data.isStatusOk()) {
				Hi_Preferences.changed = false;
				dojo.global.clearInterval(Hi_Preferences.started);
				Hi_Preferences.started = false;
				Hi_Address.removeAll();
			}
		},
		hideNews: function(id) {
			if (hideElement('latest_news_' + id)) {
				Hi_Preferences.set('closed_news', id);
			}
		},
		toggleFeeds: function() {
			toggleClass('feeds', 'opened', 'closed');
			if (dojo.hasClass('feeds', 'opened')) {
				Hi_Preferences.set('feeds', 1);
			} else {
				Hi_Preferences.set('feeds', 0);
			}
		},
		lastActiveTask: null,
		getLastActiveTask: function() {
			if (Hi_Preferences.lastActiveTask !== null) {
				return Hi_Preferences.lastActiveTask;
			}
			var i, imax;
			var activeSort, activeId = false, sort;
			var task;
			var items = hi.items.manager.get();
			for (i=0, imax=items.length; i<imax; i++) {
				task = items[i];
				if (task['category'] != 0 && task['completed'] != 1) {
					sort = Hi_Preferences.get('ts_' + task['id']);
					if (!activeId || activeSort < sort) {
						activeId = task['id'];
						activeSort = parseInt(Hi_Preferences.get('ts_' + activeId));
					}
				}
			}
			Hi_Preferences.lastActiveTask = activeId;
			return activeId;
		},
		
		/**
		 * Restore from storage
		 */
		restoreFromStorage: function () {
			var keys = dojox.storage.getKeys(preferences_ns);
			for(var i=0,ii=keys.length; i<ii; i++) {
				Hi_Preferences.preferences[keys[i]] = dojox.storage.get(keys[i], preferences_ns);
			}
		}
	};
	
	dojo.require("dojox.storage.LocalStorageProvider");
	dojo.require("dojox.storage.CookieStorageProvider");
	dojo.require("dojox.storage.FlashStorageProvider");
	storageManager.initialize();
	if (storageManager.isAvailable()) {
		Hi_Preferences.restoreFromStorage();
	}
	
	return dojo.global.Hi_Preferences;
});