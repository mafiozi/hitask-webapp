define(['dojo/on', 'hi/widget/task/item_DOM', 'dojo/_base/array', 'dojo/string'],function (dojoOn, itemDOM, dojoArray, dojoString) {

	var Team = dojo.global.Team = {
		slowDown: 0,
		friendCount: false,
		pingForce: false,

		friend: true,
		fullname: null,
		email: null,
		business_licenses_remaining: 0,

		intervalBusiness: false,
		intervalMessages: false,
		intervalContacts: false,

		/* Bug #1823 */
		deferredRequest:null,
		requestStartTime:0,
		deferredBusinessRequest:null,
		businessRequestStartTime:0,
		deferredContactsRequest:null,
		contactsRequestStartTime:0,
		errorMessage: 'Oops! We can\'t execute this operation at the moment. Please try again a bit later.',

		isFriend: function(id) {
			var result = false;
			id = parseInt(id);
			for(var i in Project.friends){
				var friend = Project.friends[i];
				if(friend.id && friend.id == id){
					result = true;
				}
			}
			return result;
		},
		ping: function(force) {
			if (Team.pingForce && !force) {
				Team.pingForce = false;
				return;
			}
			fillUsers(false, true);
			if (force) {
				Team.pingForce = true;
			}
		},
		isEmail: function(email){
			if(email.constructor != String){
				return false;
			}
			email = email.trim();
			if(!email.match(/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,63}|[0-9]{1,6})(\]?)$/)){
				return false;
			}else{
				return true;
			}
		},
		checkEmail: function () {
			var str = dojo.byId('person_email').value;
			var err = dojo.byId('person_email_error');
			var errBlank = dojo.byId('person_email_blank_error');
			if (str && str.trim) {
				str = str.trim();
			}
			dojo.addClass(err, 'hidden');
			dojo.addClass(errBlank, 'hidden');
			
			if (!str) {
				dojo.removeClass(errBlank, 'hidden');
				return false;
			} else if (Team.isEmail(str)) {
				return true;
			} else {
				dojo.removeClass(err, 'hidden');
				return false;
			}
		},
		checkDuplicate: function() {
			var dup = false,
				input = dojo.byId('person_email').value,
				friends = Project.friends,
				fl = friends.length, i, temp;

			input = input && dojoString.trim(input);
			for (i = 0; i < fl; i++) {
				temp = friends[i];

				if (temp.email === input) {
					openTab('frfind', 5, 0, {});
					return true;
				}
			}

			return false;
		},
		reset: function (t, e) {
			openTab('frfind', 0, 0, {});
			/* #2764 */
			dojo.removeClass(dojo.byId('person_fullname'),'inputText');
			dojo.removeClass(dojo.byId('person_email'),'inputText');
		},
		clearInput: function() {
			var person_email = dijit.byId('person_email');
			if (person_email) person_email.set('value', '');
			var person_fullname = dijit.byId('person_fullname');
			if (person_fullname) person_fullname.set('value', '');
		},
		frfind: function (dataResponse) {
			hideThrobber('frfind');
			Team.fullname = dojo.byId('person_fullname').value;
			Team.email = (data && 'email' in data ? data.search : dojo.byId('person_email').value);
			Team.friend = true;

			var notFound = false;
			var manyFound = false;
			var data = {};

			if(!dataResponse.isStatusOk()){
				// if user not found, show open tab 2
				if(dataResponse.getStatus() == 1){	// ErrorCode.NOT_FOUND
					notFound = true;
				}else{
					return;
				}
			}else{
				var allData = dataResponse.getData();
				if(!dojo.isArray(allData)){
					allData = {};
				}
				if(allData.length == 0){
					notFound = true;
				}else if(allData.length > 1){
					manyData = true;
				}else{
					data = allData[0];
				}
			}

			if(notFound){
				if(Team.isEmail(Team.email)){
					//Open tab 2 (invite) and submit it or open tab 12 (invite type selection "Business" "Friend")
					Team.businessLoad().then(function() {
						// #5800: always show this tab.
						if (Project.businessId && Project.business_level >= 2 /* manager */ && (Project.business_can_invite || true)) {
							data = dojo.mixin(data, {fullname: Team.fullname, email: Team.email});
							openTab('frfind', 14, 0, data);
						} else {
							var tab = dojo.byId('frfind_2');
							var form = tab.getElementsByTagName('FORM')[0];
							data = dojo.mixin(data, {email: Team.email});

							Team.friend = true;
							form.action = '/fradd';

							fillTemplate(tab,data);
							form.onsubmit();
						}
					}, function() {
						alert(Team.errorMessage);
					});
				}else{
					openTab('frfind', 6, 0);
				}
			}else if(manyFound){
				data = dojo.mixin(data, {search: (Team.fullname + ' ' + Team.email).trim()});
				openTab('frfind', 9, 0, data);
			}else{
				if(data.id == Project.user_id){
					openTab('frfind', 10, 0, {search: ''});
				}else if(Team.isFriend(data.id)){
					openTab('frfind', 5, 0, data);
				}else{
					Team.businessLoad().then(function() {
						if (Project.businessId && Project.business_level >= 2 /* manager */ && (Project.business_can_invite || true)) {
							openTab('frfind', 12, 0, dojo.mixin(data, {fullname: Team.fullname, email: Team.email}));
						} else {
							var tab = dojo.byId('frfind_1');
							var form = tab.getElementsByTagName('FORM')[0];
								form.action = 'fradd';

							fillTemplate(tab,data);
							form.onsubmit();
						}
					}, function() {
						alert(Team.errorMessage);
					});
				}
			}
		},
		frfind_error: function() {
			hideThrobber('frfind');
			alert(Team.errorMessage);
		},
		fradd: function (dataResponse, ajaxOptions, ajaxEvt) {

			if(!dataResponse){
				return;
			}

			if (dataResponse.isStatusOk()) {
				openTab('frfind', 3, 0, {
					search: Team.fullname || Team.email
				});

				if (!Team.friend) {
					//User was added to business
					Team.business_licenses_remaining--;
				}

				Team.updateAssignList();

				Team.ping(true);
			} else {
				var message = dataResponse.response? dataResponse.response.error_message: null;
				var err = Team.fradd_error(dataResponse.getStatus(), 1, ajaxEvt && ajaxEvt.xhr ? ajaxEvt.xhr.status : null, dataResponse.getErrorMessage());
				if (!err)
					openTab('frfind', 4);
			}
		},
		frapprove_frdecline: function (data) {
			Team.ping(true);
		},
		forceRefreshWIthoutHighlight: function() {
			itemDOM.globalHighlight = false;
			hi.items.manager.refresh(true).promise.then(function() {
				setTimeout(function() {
					itemDOM.globalHighlight = true;
				}, 3000);
			});
		},
		frdelete: function (data) {
			hideThrobber('frdelete');
			Team.ping(true);
			var callback = function() {
				hi.items.manager.redraw();
				hi.items.ProjectListTeam.disableNonexistGroups();
				Team.forceRefreshWIthoutHighlight();
			};
			Team.contactsLoad().then(callback, callback);
		},
		frdelete_error: function (data) {
			hideThrobber('frdelete');
			alert(Team.errorMessage);
		},
		frdelete_business: function (data) {
			hideThrobber('frdelete_business');
			var callback = function() {
				hi.items.ProjectListTeam.disableNonexistGroups();
				Team.forceRefreshWIthoutHighlight();
			};
			Team.contactsLoad().then(callback, callback);
			Team.business_licenses_remaining++;
			Team.businessLoad();
		},
		frdelete_business_error: function (data) {
			hideThrobber('frdelete_business');
			alert(Team.errorMessage);
		},
		frinvite: function (dataResponse, ajaxOptions, ajaxEvt) {
			hideThrobber('frinvite');

			if (dataResponse.isStatusOk()) {
				openTab('frfind', 7, 0, {search: ''});
				Team.ping(true);
			} else {
				var err = Team.fradd_error(dataResponse.getStatus(), 1, ajaxEvt && ajaxEvt.xhr ? ajaxEvt.xhr.status : null, dataResponse.getErrorMessage());
				if (!err) {
					openTab('frfind', 8, {search: ''});
				}
			}
		},
		frinvite_error: function(data) {
			hideThrobber('frinvite');
			alert(Team.errorMessage);
		},
		fradd_error: function(status, tooltip, httpStatus, rawMessage) {
			tooltip = tooltip || false;
			switch (status) {
				case 2: // Invitation exists.
					openTab('frfind', 13);
					return true;
				case 3: //'NO_PERMISSION' or 'invalid email':
					if (httpStatus == 200 && rawMessage) {
						Project.alertElement.showItem(14, {task_alert: rawMessage});
					} else {
						Project.alertElement.showItem(16);
					}
					return true;
				case 12: //'NO_LICENSES':
					if (Project.account_type == 'TEAM_BASIC') {
						Project.alertElement.showItem(19);
					} else {
						Project.alertElement.showItem(5);
					}

					Tooltip.close('frfind');
					return true;
				case 16: //'IN_BUSINESS':
					if (tooltip) {
						openTab('frfind', 11);
					} else {
						alert(hi.i18n.getLocalization('friendfind.in_business'));
					}
					return true;
				case 19: //'UNCONFIRMED_EMAIL':
					alert(hi.i18n.getLocalization('friendfind.unconfirmed_email'));
					return true;
				case 44: // Team Basic account contacts limit reached.
					if (Project.account_type == 'TEAM_BASIC') {
						Project.alertElement.showItem(19);
						Tooltip.close('frfind');
						return true;
					} else {
						return false;
					}
				default:
					if (rawMessage) {
						Project.alertElement.showItem(14, {task_alert: rawMessage});
						return true;
					}
			}
			return false;
		},
		fradd_business: function (dataResponse) {

			if(!dataResponse){
				return;
			}

			if (dataResponse.isStatusOk()) {
				Team.ping(true);
				Team.business_licenses_remaining--;
				Team.businessLoad();
			} else {
				var err = Team.fradd_error(dataResponse.getStatus(), 0);
				if (!err) {
					alert(Team.errorMessage);
				}
			}
		},

		frresend: function (data) {
			//TODO
		},

		mchange: function (data) {
			var email = getValue('confirm_email_email_input');

			if (data) {
				if (data.isStatusOk()) {
					alert('Email sent');
					setValue('confirm_email_email', htmlspecialchars(email));
					openTab('team_container', 1);
					//Bug #5180
					dojo.global.showElement(dojo.byId('team_container_0'));
				} else {
					if (data.getStatus() == 22) {
						alert('This email is already used by other person!');
					} else if (data.getStatus() == 3) {
						// email confirmed alreay. Refresh team sidebar
						Team.ping(true);
					} else if (data.getStatus() == 5) {
						alert('Incorrect email address!');
					} else {
						alert('Couldn\'t change email for unknown reason!');
					}
				}
			}
			setValue('confirm_email_email_input', email);
		},
		binv_decline: function() {
			hideThrobber('binvDecline');
			Project.alertElement.hide();
		},
		binv_decline_error: function() {
			hideThrobber('binvDecline');
			alert(Team.errorMessage);
			Project.alertElement.hide();
		},
		binv_accept: function(id,title) {
			Project.alertElement.hide();
			showThrobber(null, 'binvAccept');

			Project.ajax('binv_accept', {id: id}, function() {
				window.location.reload();
			}, function(a, b, apiResponse) {
				hideThrobber('binvAccept');
				if (apiResponse && apiResponse.getStatus() == 16) {
					alert('You are already a team member. Please leave current team before accepting another invitation.');
				} else if (apiResponse && apiResponse.getErrorMessage()) {
					alert(apiResponse.getErrorMessage());
				} else {
					alert('Sorry, error occured.');
				}
			});
		},

		/**
		 * Wait till friend list is updated and then select newly added user
		 */
		updateAssignList: function () {
			var email = Team.email;

			var item = hi.items.manager.editing();
			if (item) {
				item.editingView.fillAssigneeList();
			}
		},

		businessLoad: function(){
			return Project.ajax('listbusiness', {}, Team.businessRecieve);
		},
		businessRecieve: function(dataResponse){
			/* Bug #1823 */
			Team.deferredBusinessRequest = null;
			if(!(dataResponse instanceof hiApiResponse && dataResponse.isStatusOk())){
				return;
			}
			fillBusiness(dataResponse.getDataBusiness());
		},

		messagesLoad: function(){
			if(!Project.last_chat_time){
				Project.last_chat_time = time2strAPI(new Date(0));
			}
			/* Bug #1823 */
			return Project.ajax('listmessages', {time:Project.last_chat_time}, Team.messagesRecieve);
		},

		/* Bug #1823 try a bogus API call */
		bogusLoad:function() {
			return Project.ajax('nowhere',{},Team.messagesReceive);
		},
		messagesRecieve: function(dataResponse){
			/* Bug #1823 */
			Team.deferredRequest = null;
			if(!(dataResponse instanceof hiApiResponse && dataResponse.isStatusOk())){
				return;
			}
			fillMessages(dataResponse.getDataMessages());
		},

		contactsLoad: function(){
			return Project.ajax('listcontacts', {forceSendContacts: true}, Team.contactsReceive, Team.contactsReceive);
		},

		contactsReceive: function(dataResponse){
			Team.deferredContactsRequest = null;
			if (dataResponse instanceof hiApiResponse) {
				if (dataResponse.isStatusOk()) {
					if (!Project.email_confirmed) Project.email_confirmed = true;
					openTab('team_container', 0);
					
					var friends = dataResponse.getDataContacts();
					var activityTabId = 'activity';
					var tabsId = 'grouping';
					var tab = dojo.global.tabs.main;
					var activityTab = dojo.byId(tabsId + '_' + activityTabId);
					
					fillContacts(friends);
					// try {
					// 	if (activityTab) {
					// 		if (dojoArray.filter(friends, function(x) { return x.activeStatus; }).length > 0) {
					// 			dojo.removeClass(activityTab, 'hidden');
					// 		} else {
					// 			dojo.addClass(activityTab, 'hidden');
					// 			if (tab.selected() == activityTabId) {
					// 				tab.open('my');
					// 			}
					// 		}
					// 	}
					// } catch (e) {
					// 	console.error(e);
					// }
				} else if (dataResponse.getStatus() == 19) {
					openTab('team_container', 1);
					// Change #5064
					if (!Project.email_confirmed) {
						var teamContainer0 = dojo.byId('team_container_0');
						dojo.global.showElement(teamContainer0);
						var loadingIcon = dojo.query('.loading-icon', teamContainer0)[0];
						if (loadingIcon) {
							dojo.global.hideElement(loadingIcon);
						}
					}
					if (dataResponse.response && dataResponse.response.contacts) {
						// #5611
						Project.shadowFriends = {};
						for (var i = 0, len = dataResponse.response.contacts.length; i < len; i++) {
							Project.shadowFriends[dataResponse.response.contacts[i].id] = dataResponse.response.contacts[i];
						}
					}
				} else if (dataResponse.getStatus() == 18) {
					openTab('team_container', 2);
				}
			}
		}
	};

	var fillBusiness = dojo.global.fillBusiness = function(data) {

		if (dojo.isArray(data.inv_get) && data.inv_get.length > 0) {
			var _data = data.inv_get[0];
			_data.inviter_name = ('' + (_data.firstName || '') + ' ' + (_data.lastName || '')).trim();
			if (!_data.inviter_name) {
				_data.inviter_name = _data.inviterName || ('user' + _data.businessUserId);
			}
			_data.title = _data.domainTitle;
			Project.alertElement.showItem(3, _data);
		}

		if (Project.businessId && (!data.domainId || data.domainId != Project.businessId)) {
			Project.alertElement.showItem(4);
		}

		// #2758: account has been upgraded to business.
		if ((!Project.businessId && data.domainId) || (!Project.isPremium && data.isPremium)) {
			Project.alertElement.showItem(21);
		}

		if(dojo.isArray(data.inv_sent)){
			var new_inv_sent = [];
			// fill new invitation data
			for(var i in data.inv_sent){
				new_inv_sent[data.inv_sent[i].userId] = data.inv_sent[i];
			}
			// clear invitation data
			Project.inv_sent = new_inv_sent;
		}

		Team.business_licenses = data.license_count || 0;
		Team.business_licenses_remaining = Team.business_licenses - (data.license_usage || 0);
		Project.business_can_invite = data.canInvite;
		// #5800
		if (Project.account_type == 'TEAM_BASIC' && Team.business_licenses_remaining == 0) {
			Team.business_licenses_remaining = 1;
			Project.business_can_invite = true;
		}
	};

	var fillMessages = dojo.global.fillMessages = function(data) {

		var active_user = getValue('chat_user_id');
		var playSound = false;
		var firstCall = Project.friends ? false : true;

		if (data && typeof data == 'object') {
			Project.chatTime = Project.chatTime || "";
			var latest = Project.chatTime;
			var taskAssignChange = false;
			for (var i in data) {
				var hasNewMessages = false;
				dojo.forEach(data[i].messages, function(el) {
					var contact_id = data[i].user;
					if (el.userFrom != Project.user_id && el['messageNew']) {
						hasNewMessages = true;
					}
					if (Project.chatTime < el.messageTime) {
						if (parseInt(el.messageType) in {1:1,2:2,10:10}) {
							if (el.userFrom != Project.user_id) {
								if (Project.chatTime) {
									//If not first request only then task assign can change
									taskAssignChange = true;
								}
							}
							var text = '';
							if (el.messageType == 1) {
								text = '<span class="assigned_task" onclick="hi.items.manager.openItem(' + el.taskId + ')"><img src="' + Project.icon(1) + '" /> ' + htmlspecialchars(el.messageBody) + '</span> <span class="assigned">';
								if (el.userTo == Project.user_id) {
									text += 'received from <span fill="html: sender_name"></span></span>';
								} else {
									text += 'assigned to <span fill="html: receiver_name"></span></span>';
								}
							}
							if (el.messageType == 2) {
								text = '<span class="unassigned_task" onclick="hi.items.manager.openItem(' + el.taskId + ')"><img src="' + Project.icon(1) + '" /> ' + htmlspecialchars(el.messageBody) + '</span> <span class="unassigned">';
	//							if (el.userFrom != Project.user_id) {
	//								text += hi.i18n.getLocalization('main.returned_to') + ' <span fill="html: receiver_name"></span></span>';
	//							} else {
									text += hi.i18n.getLocalization('main.returned_to') + ' <span fill="html: sender_name"></span></span>';
	//							}
							}
							if (el.messageType == 10) {
								text = '<span class="deleted_task"><img src="' + Project.icon(1) + '" /> ' + htmlspecialchars(el.messageBody) + '</span> <span class="deleted">';
								text += hi.i18n.getLocalization('main.deleted_by') + ' <span fill="html: sender_name"></span></span>';
							}
							el.messageBody = text;
						} else {
							el.messageBody = htmlspecialchars(el.messageBody);
						}



						if (!(contact_id in Hi_Team.chatMessages)) {
							Hi_Team.chatMessages[contact_id] = [];
						}
						Hi_Team.chatMessages[contact_id].push(el);
						if (contact_id && active_user == contact_id) {
							Hi_Team.addMessage(contact_id, el);
						}
						if (el.messageTime > latest) {
							latest = el.messageTime;
						}
					}
				});
				if (hasNewMessages) {
					if (active_user == data[i].user) {
						Project.ajax('chread', {userid: data[i].user, time: latest});
					}
					Hi_Team.refreshMessageCount(data[i].user, true);
					playSound = true;
				}
			}

			if (taskAssignChange) {
				hi.items.manager.refresh();
			}
			Project.chatTime = latest;

			Project.last_chat_time = latest;

		}
		Hi_Team.updateAllMessagesEnvelope();
		if (playSound && !firstCall) {
			Hi_Team.playSound();
		}
	};

	var fillContacts = dojo.global.fillContacts = function(data, force) {
		if(Tooltip.opened('contact_info')){
			// if team menu is opened, then skip render contact list
			return;
		}

		if (data !== false) Team.slowDown++;
		if (!data) {
			data = false;
		}

		var active_user = getValue('chat_user_id'), i;

		if (data && typeof data == 'object') {
			showElement('grouping_5');
			var firstCall = Project.friends ? false : true;
			Project.friends = data;
			Project.shadowFriends = null;
			if (!Project.getFriend(active_user)) {
				Hi_Team.closeChat();
			}

			//Update remaining license count
			if (!firstCall && Project.businessId) {
				var licenses = Team.business_licenses;

				// one license for myself
				licenses -= 1;

				for (i = 0, ii = data.length; i < ii; i++) {
					//null or '' <- not in business
					//0 - in business, but inactive
					if ((data[i].subscription == 'BIS' || data[i].subscription == 'BIS_WAIT') && data[i].id != Project.user_id) {
						licenses--;
					}
				}

				Team.business_licenses_remaining = licenses;
			}

			// Sort friends list
			Project.friends.sort(function(a, b) {

				var getOrder = function(u) {
					if (u.id == Project.user_id) {
						return 10;
					}
					if (u.subscription == 'BOTH' || u.subscription == 'BIS' || u.level > 0) {
						return 30;
					}
					if (u.subscription == 'TO') {
						return 20;
					}
					return 40;
				};

				a.statusOrder = getOrder(a);
				b.statusOrder = getOrder(b);

				// #2638 requires sorting by name only.
				// So, commenting this.
				//if (a.statusOrder != b.statusOrder) {
				//	return a.statusOrder - b.statusOrder;
				//} else {
					//if (a.isOnline != b.isOnline) {
					//	return a.isOnline ? -1 : 1;
					//}
					var a_ = a.name.toLowerCase();
					var b_ = b.name.toLowerCase();
					if (a_ == b_) {
						return a.name > b.name ? 1 : -1;
					} else {
						return a_ > b_ ? 1 : -1;
					}
				//}
			});
		}

		Team.slowDown = 0;
		Project.pingWait = 0;
		HiDragSourceCached.user = false;
		var container = Project.getContainer('fillUsers');
		container = dojo.byId(container);
		var parent = childNode(dojo.byId('user_container_template'), 0);
		parent = parent.cloneNode(true);
		var template = childNode(dojo.byId('user_template'), 0);
		var users = Project.friends;
		var j;
		var on = 0;
		var approved = 0;
		l = users.length;
		var msg_cnt = 0;
		var userDef = Hi_Team.emptyUser;
		var atLeastOneFriend = false;
		for (i = 0; i < l; i++) {

			if (Hi_Team.refreshMessageCount(users[i]['id']) > 0) {
				msg_cnt++;
			}

			var u = {};
			for (j in users[i]) {
				u[j] = users[i][j];
			}

			u = dojo.mixin(u, userDef);

			u['login'] = u['login'] || '';
			u['subscription'] = u['subscription'] || '';
			u['pictureHash'] = u['pictureHash'] || '';
			u['image16src'] = '/avatar/' + (u['pictureHash'] || '') + '.16.gif';
			u['image20src'] = '/avatar/' + (u['pictureHash'] || '') + '.20.gif';
			u['image24src'] = '/avatar/' + (u['pictureHash'] || '') + '.24.gif';
			u['image'] = 'url(' + u['image16src'] + ')';
			u['image32'] = 'url(/avatar/' + (u['pictureHash'] || '') + '.32.gif)';
			u['image40'] = 'url(/avatar/' + (u['pictureHash'] || '') + '.40.gif)';
			u['image64'] = 'url(/avatar/' + (u['pictureHash'] || '') + '.64.gif)';
			if (!u['isOnline']) {
				u['offline'] = 'offline';
			} else {
				u['offline'] = 'online';
				on++;
			}
			u['menuStatus'] = true;
			u['registerStatus'] = (u.emailConfirmed)?true:false;
			if (u['messages']) {
				u['msgsStatus'] = true;
			}
			if (u.subscription == 'FROM' || u.subscription == 'BIS_WAIT') {
				u['waitStatus'] = true;
				u['waitBisStatus'] = u.subscription == 'BIS_WAIT';
			} else if (u.subscription == 'TO') {
				u['reqStatus'] = true;
				u['menuStatus'] = false;
			} else if (u.subscription == 'FR_D') {
				u['declStatus'] = true;
			} else if (u.subscription == 'BIS_DEC') {
				u['bisDeclStatus'] = true;
			} else if (u.subscription == 'BOTH' || u.subscription.indexOf('BIS') == 0) {

				u['activeStatus'] = true;
				approved = 1;

				if(u.subscription.indexOf('BIS') == 0){
					u['businessStatus'] = true;

					if(u.level > 0){
						approved = 1;
					}
					if(u.businessLevel){
						if(u.businessLevel == 0){
							u['businessStatusInactive'] = true;
						}else{
							if (u.businessLevel >= Hi_TeamLevels.ADMIN){
								u['businessStatusAdmin'] = true;
							} else if (u.businessLevel == Hi_TeamLevels.MANAGER){
								u['businessStatusManager'] = true;
							}
							u['businessStatusActive'] = true;
						}
					}
				}else{
					u['premiumStatus'] = u['isPremium'];
				}
			}
			if (Project.inv_sent) {
				var markDeclined = function(declined) {
					if (declined) {
						u['waitStatus'] = false;
						u['businessStatusDeclined'] = true;
					} else {
						u['waitStatus'] = true;
					}
				};

				if (u.id in Project.inv_sent) {
					u['declStatus'] = false;
					markDeclined(Project.inv_sent[u.id].declined);
				} else if (u.subscription == 'BIS_DEC') {
					markDeclined(true);
				}
			}

			for (j in u) {
				Project.friends[i][j] = u[j];
			}
			if (users[i].id == Project.user_id) continue;

			tmp = template.cloneNode(true);

			fillTemplate(tmp, u);
			tmp.id = 'friend_item_' + u['id'];
			if (active_user == u['id']) {
				tmp.style.fontWeight = 'bold';
			}
			parent.appendChild(tmp);
			atLeastOneFriend = true;
		}
		/* Feature #2052 - define click, doubleclick,mouseover events in JavaScript, as opposed in the HTML template
		 * because that doesn't work
		 */
		var children = parent.childNodes;
		var timers = {};
		for (i = 0; i < children.length; i++) {
			var li = children[i];
			//Put single click on a timer; so if double click occurs, we can kill it
			dojoOn(li,'click',function(evt) {
				if (Tooltip.opened('contact_info')) return;
				var self = this;
				if (timers[self.id]) clearTimeout(timers[self.id]);
				timers[self.id] = setTimeout(function() {
					var target = dojo.query('div.button',self)[0];
					Hi_Team.hasAssignedTasks(self);
					var data = target.getAttribute('data');

					if (Tooltip.opened('contact_info')) {
						Tooltip.close('contact_info');
					} else {
						Tooltip.open('contact_info', data, self, evt, 'below-centered', 'contact_info_start',
							{
								orient: [
									'below-centered',
									'above-centered',
									'before-centered',
									'after-centered'
								]
							}
						);
					}
				},250);//250 is totally arbitrary and may need to be changed
			});

			dojoOn(li,'dblclick',function(evt) {
				var self = this;
				if (timers[self.id]) clearTimeout(timers[self.id]);
				if (Tooltip.opened('contact_info')) Tooltip.close('contact_info');
				Hi_Team.startChat.call(this);
			});
			//onmouseover="Hi_Team.avatarOver.call(this, 1)" onmouseout="Hi_Team.avatarOver.call(this, 0)"
			dojoOn(li,'mouseover',function(evt) {Hi_Team.avatarOver.call(this,1);});
			dojoOn(li,'mouseout',function(evt) {Hi_Team.avatarOver.call(this,0);});
		}
		if (msg_cnt > 0) {
			dojo.removeClass('new_messages', 'invisible');
			dojo.removeClass('new_messages_collapsed', 'invisible');
			setValue(dojo.query('.message_count', dojo.byId('new_messages'))[0], msg_cnt);
		} else {
			dojo.addClass('new_messages', 'invisible');
			dojo.addClass('new_messages_collapsed', 'invisible');
		}

		var help = dojo.byId('team_help');
		if (approved == 0 || Hi_Preferences.get('hide_team_help') == 1) {
			hideElement(help);
		} else {
			showElement(help);
		}

		//removeNode(container, false);
		container.innerHTML = '';	//faster than removing each node sepparate

		if (atLeastOneFriend) {
			showElement(container);
			container.appendChild(parent);
			dijit.byId('messengerPane').resize();
		} else {
			hideElement(container);
		}
		if (Project.firstUserLoad) {
			hi.items.ProjectListTeam.reset();
			Project.firstUserLoad = false;
		}
		//Tooltip.updatePositions();

		 if (dojo.isArray(Project.friends)) {
			// #2373
			dojo.every(Project.friends, function(friend, i) {
				if (friend.reqStatus === true && !friend.businessStatus) {
					if (typeof(Project.friendsInvShown) == 'undefined') {
						Project.friendsInvShown = [];
					}

					if (dojo.indexOf(Project.friendsInvShown, friend.id) == -1) {
						// Do not show popup for same inviter again till page refreshed.
						Project.friendsInvShown.push(friend.id);
						var _data = friend;
						_data.inviter_name = ('' + (_data.firstName || '') + ' ' + (_data.lastName || '')).trim();
						if (!_data.inviter_name) {
							_data.inviter_name = _data.name || _data.login;
						}
						Project.alertElement.showItem(15, _data);
						Project.friends[i].reqStatus = false;
						return false;
					}
				}
			});
		}
	};

	var fillUsers = dojo.global.fillUsers = function(data, force) {
		if (force) {
			//close team menu when force load
			Tooltip.close('contact_info');
			Team.businessLoad();
			Team.messagesLoad();
			Team.contactsLoad();
		}
		// functionality will be moved to another functions (fillContacts, fillMessages)
		//console.warn("*** start Function #fillUsers# breaked");
		return;
	};

	//TODO
	return {};

});
