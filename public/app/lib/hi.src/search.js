//dojo.provide("hi.search");
// Feature #1721 pull down dijit.popup
define(['dijit/popup', 'hi/widget/form/cleartextbox', "dojo/on"], function(popup, cleartextbox, djOn) {
	
	return dojo.global.Hi_Search = {
			
		//Feature #1721
		closeConnect:null,
		
		//Search results
		results: {},
		
		//Expression to escape string (strings which will be used in regular expressions) 
		escapeExpression: /(\/|\.|\-|\*|\+|\?|\||\(|\)|\[|\]|\{|\}|\\)/g,
		
		//Tasks has been filled
		resultsActive: false,
		
		//Search query string
		query: '',
		
		//Search query regex
		searchExpression: null,
		
		//Grouping
		previousGrouping: null,
		
		//Last key press time
		keyPressTimeout: null,
		keyPressTimeoutInterval: 500,
		
		searchWidget: null,
		
		init: function() {
			dojo.parser.parse(dojo.byId('headerSearchSection'));
			this.searchWidget = dijit.byId('hiSearchWidget');
			var tb = dojo.query('input[type=text]', this.searchWidget.domNode);
			var self = this;
			djOn(tb[0], 'keyup', function(event) {
				self._onKeyPress(event);
			});
			djOn(tb[0], 'keydown', function(event) {
				self._onKeyDown(event);
			});
			this.searchWidget.on('change', function(val) {
				if (!val) {
					Hi_Search._reset();
				}
			});
		},
		
		_getPreparedQuery: function() {
			return Hi_Search.searchWidget.get('value').replace(/(^\s+|\s+$)/g, '');
		},
		
		_doKeyPressCheck: function () {
			Hi_Search.keyPressEvent = null;
			
			var query = Hi_Search._getPreparedQuery();
			
			if (query.length >= 2) {
				Hi_Search.search(query);
			} else if (query.length == 0) {
				Hi_Search._reset();
			}
		},
		
		_changeView: function (resultsActive) {
			var aC = dojo.addClass;
			var rC = dojo.removeClass;
			var newItem = hi.items.manager.item(-1);
				
			if (resultsActive) {
				if (newItem) {
					aC(newItem.editingView.titleContainerNode, "hidden");
				}
				
				aC(Hi_Tag.tag_bar(), 'hidden');
				rC(dojo.byId('search_bar'), 'hidden');
				aC(dojo.byId(Project.getContainer("tasksList")), 'search_results');
				aC(dojo.byId('newProjectButton'), 'hidden');
				aC(dojo.byId('toolbar'), 'hidden');
			} else {
				if (newItem) {
					rC(newItem.editingView.titleContainerNode, "hidden");
				}
				
				rC(Hi_Tag.tag_bar(), 'hidden');
				aC(dojo.byId('search_bar'), 'hidden');
				rC(dojo.byId(Project.getContainer("tasksList")), 'search_results');
				// #2194
				if (tabs.main.active === 'project') {
					// Show back button only on Project tab.
					rC(dojo.byId('newProjectButton'), 'hidden');
				}
				rC(dojo.byId('toolbar'), 'hidden');
			}
		},
		
		/**
		 * When key is pressed
		 */
		_onKeyPress: function (event) {
			var event = event || window.event;
			
			//ESCAPE and ENTER already are handled
			if (event.keyCode == 13 || event.keyCode == 27) {
				return;
			}
			
			if (Hi_Search.keyPressTimeout) {
				clearTimeout(Hi_Search.keyPressTimeout);
				Hi_Search.keyPressTimeout = null;
			}
			
			Hi_Search.keyPressTimeout = setTimeout(Hi_Search._doKeyPressCheck, Hi_Search.keyPressTimeoutInterval);
		},
		
		_onKeyDown: function(event) {
			var event = event || window.event;
			if (event && hi.items.manager.editing()) {
				if (event.target && typeof(event.target.blur) == 'function') {
					event.target.blur();
				}
				dojo.stopEvent(event);
				Project.alertElement.showItem(14, {task_alert: 'Please finish editing open item.'});
				return false;
			}
		},
		
		/* #2331 - clear search but don't close search tooltip */
		_reset:function()
		{
			this.searchWidget.set('value', '');
			this.results = {};
			this.query = '';
			this.searchExpression = null;
			
			if (this.resultsActive) {
				this.resultsActive = false;
				this._changeView(false);
				dojo.removeClass(dojo.query('#searchItemListContainer .search_bar')[0],'withSearchResults');
				hi.items.manager.searchChange();
			}
		},
		
		/**
		 * Feature #1721
		 */
		closeTooltipDialog:function(name)
		{
			Tooltip.close(name);
			Hi_Search._reset();
		},
		
		/**
		 * Close search form and results
		 */
		close: function () {
			//Hide tooltip
			Tooltip.close('search');
			Hi_Search._reset();
		},
		
		
		/**
		 * Run search and show results
		 * @param {String} query
		 */
		search: function (query) {
			//Don't search again unless something has changed
			if (this.query == query) return this.results;
			
			//Add loading icon
			dojo.addClass('tooltip_search', 'loading');
			
			//Reset variables
			this.results = {};
			this.query = query;
			
			//Convert from string into regular expression
			this.searchExpression = this.queryToExpression(query);
			
			//Show/hide needed/unneeded elements
			if (this.resultsActive) {
				//Fill tasks; loading icon won't be seen unless fill is delayed
				setTimeout(function () {
					hi.items.manager.searchChange();
				}, 50);
			} else {
				dojo.addClass(dojo.query('#searchItemListContainer .search_bar')[0], 'withSearchResults');
				//Change view changes tab, which calls UI update
				this._changeView(true);
				hi.items.manager.searchChange();
			}
			
			//Show results
			this.resultsActive = true;
			
			//Set query
			//dojo.byId('search_bar_query').innerHTML = dojo.string.escapeXml(query);
			innerText(dojo.byId('search_bar_query'), query);
			
			//Remove loading icon
			setTimeout(function () {
				dojo.removeClass('tooltip_search', 'loading');
			}, 200);
		},
	
	
		/**
		 * Returns current search query
		 * 
		 * @return {String}
		 */
		getSearchQuery: function () {
			return this.query;
		},
		
		/**
		 * Returns search query regular expression
		 * 
		 * @return {Object}
		 */
		getSearchRegEx: function () {
			return this.searchExpression;
		},
		
		/**
		 * Returns true if search form is visible and contains query.
		 * 
		 * @return {Boolean}
		 */
		isFormVisible: function () {
			return this.searchWidget.get('value') != '';
		},
		
		/**
		 * Returns true if search results are visible
		 * 
		 * @return {Boolean}
		 */
		isResultsVisible: function () {
			return this.resultsActive;
		},
		
		/**
		 * Escapes regular expression special characters
		 *  
		 * @param {String} str
		 * @return {String}
		 */
		escapeStringRegEx: function (str) {
		    return str.replace(this.escapeExpression, '\\$1');
		},
		
		
		/**
		 * Converts string into regular expression to match words in the data
		 * 
		 * @param {Object} query
		 * @return {RegExp}
		 */
		queryToExpression: function (query) {
		    var regex = this.escapeStringRegEx(query);
			
			return new RegExp(regex, 'ig');
		},
		
		/* Dummy function */
		emptyFunction: function () {}
		
	};

});