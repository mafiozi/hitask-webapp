/*
 * File list
 */
define(['hi/widget/task/item_DOM', 'dojo/on'], function(item_DOM, dojoOn) {
	
	dojo.global.FileList = {
		
		/**
		 * Format file size title
		 * 
		 * @param {Number} fileSize
		 * @return Text representation of file size
		 * @type {String}
		 */
		formatFileSize: function (fileSize) {
			if (!fileSize) return '';
			
			fileSize = fileSize / 1024;
			if (fileSize > 1024) {
				return Math.round(fileSize / 1024) + 'MB';
			} else {
				return Math.ceil(fileSize) + 'KB';
			}
		},
		
		/**
		 * Get file list node
		 */
		_getNodeList: function () {
			var taskItem = hi.items.manager.opened();
			if (taskItem && taskItem.taskId && taskItem.propertiesView) {
				var taskId = taskItem.taskId;
				var uploader = FileUpload.get(taskId);
				var nodeContainer = taskItem.propertiesView.fileListNode;
				var getNodeListFromProperties = function() {
					var nodeList = nodeContainer.getElementsByTagName('UL');
					if (nodeList.length) {
						return nodeList[0];
					} else {
						return null;
					}
				};
				var nodeList = getNodeListFromProperties();
				
				if (uploader) {
					if (nodeList && uploader.nodeList != nodeList) {
						// #3225:
						dojo.destroy(nodeList);
					}
					return uploader.nodeList;
				} else {
					// When file uploading is disabled, but viewing files is allowed (Archive).
					if (nodeList) {
						return nodeList;
					} else {
						nodeList = document.createElement('UL');
						nodeList.onclick = FileList.handleClick;
						nodeContainer.appendChild(nodeList);
						return nodeList;
					}
				}
			}
			
			return null;
		},
		
		/**
		 * Create HTML node from data and template
		 * 
		 * @param {Object} data
		 * @return {HTMLElement}
		 */
		_create: function (data) {
			var taskItem = hi.items.manager.opened();
			var template_data = {
				'id': 'file_' + data.id,
				'filename': data.title,
				'fileSize': this.formatFileSize(data.file_size),
				'modifyClass': taskItem.taskItem.permission >= Hi_Permissions.MODIFY || data.user_id === Project.user_id ? '' : 'hidden'
			};
			
			var node = document.createElement('UL');
			node.innerHTML = fillStringTemplate(TEMPLATES.file, template_data);
			
			var previewUrl = item_DOM.getPreviewImageUrl(data.id);
			
			if (previewUrl && taskItem && taskItem.propertiesView) {
				dojo.query('.filesize, .filename', node).addClass('hidden');
				dojo.query('.file-inner', node).addClass('file-inner-no-back');
				var imgNode = dojo.query('.fileAttachedPreviewImg', node);
				if (imgNode) {
					imgNode.removeClass('hidden');
					imgNode[0].src = previewUrl;
					dojoOn(imgNode[0], 'click', function(e) {
						taskItem.propertiesView.showPreviewLightbox(data.id);
					});
				}
			}
			
			return dojo.dom.getChildElements(node)[0];
		},
		
		/**
		 * Add file
		 * 
		 * @param {Object} data
		 */
		addFile: function (data) {
			if (typeof data == 'string' || typeof data == 'number') {
				data = hi.items.manager.get(data);
			}
			if (!data) return;
			
			var node = this._create(data),
				items = null,
				container = FileList._getNodeList();
			
			//Place node in correct position
			if (node) {
				items = dojo.dom.getChildElements(container);
				
				if (items.length) {
					container = items[0];
					
					for(var i=0,ii=items.length; i<ii; i++) {
						if (!dojo.hasClass(items[i], 'file')) {
							container = items[i]; break;
						}
					}
				}
				
				if (container.tagName == 'LI') {
					dojo.place(node, container, 'before');
				} else {
					container.appendChild(node);
				}
			}
			
		},
		
		removeFile: function (id) {
			var node = dojo.byId('file_' + id);
			if (node) {
				node.parentNode.removeChild(node);
			}
		},
		
		fadeOutFile: function (id, effect) {
			var node = dojo.byId('file_' + id);
			if (!node) return;
			
			var callback = function () {
				FileList.removeFile(id);
			};
			
			if (effect == 'dropout') {
				node.style.position = 'relative';
				dojo.anim(node, {
					'top':		{end: -20},
					'opacity':	{end: 0}
				}, 650, null, callback);
			} else {
				dojo.anim(node, {
					'opacity':	{end: 0}
				}, 650, null, callback);
			}
			
		},
		
		/**
		 * Show controls under file
		 */
		showFileControls: function (evt, item, id, target) {
			dojo.addClass(item, 'file-expanded');
		},
		
		/**
		 * Hide controls under file
		 */
		hideFileControls: function (evt, item, id, target) {
			dojo.removeClass(item, 'file-expanded');
		},
		
		/**
		 * Download file from a list
		 */
		downloadFile: function (evt, item, id, target) {
			if (!dojo.hasClass(item, 'file-expanded')) {
				FileUpload.openFile(id);
			}
		},
		
		deleteFile: function (evt, item, id, target) {
			if (typeof Hi_Archive != 'undefined') {
				Hi_Archive.remove(id, {from_archive: 1, target: target});
				FileList.fadeOutFile(id, 'puff');
			} else {
				if (!Project.canDeleteItem(id)) {
					return false;
				}
				var message = hi.i18n.getLocalization('tooltip.delete_message_task_5');
				
				Project.showDeleteConfirmation(message, target, function () {
					hi.items.manager.removeSave(id);
					FileList.fadeOutFile(id, 'puff');
				});
			}
		},
		
		detachFile: function (evt, item, id, target) {
			//Reduce parent children count
			var oldParentId = hi.items.manager.get(id, "parent"),
				oldParent   = hi.items.manager.get(oldParentId),
				newParentId = oldParent.category ? oldParent.parent || 0 : 0;
			
			// #2935: file item must be detached to the top level of hierarchy.
			var props = {'id': id, 'parent': 0};
			
			//Keep file assigned
			var assignUser = hi.items.manager.get(oldParentId, "assignee");
			if (assignUser) {
				props.assignee = assignUser;
			}
			
			//Save changed file properties
			hi.items.manager.setSave(props);
			
			FileList.fadeOutFile(id, 'dropout');
		},
		
		/**
		 * Handle link click
		 */
		handleClick: function (e) {
			var evt = e || window.event,
				target = evt.target || evt.srcElement || evt.currentTarget,
				item,
				id;
			
			if (target.nodeType == 3) {
				//Safari bug
				target = target.parentNode;
			}
			
			if (target.tagName.toUpperCase() === 'A') {
				var c = target.className,
					fn = '';
				
				if (c.indexOf('modify') != -1) {
					fn = 'showFileControls';
				} else if (c.indexOf('close') != -1) {
					fn = 'hideFileControls';
				} else if (c.indexOf('filename') != -1) {
					if (typeof Hi_Archive != 'undefined') return;
					fn = 'downloadFile';
				} else if (c.indexOf('delete') != -1) {
					fn = 'deleteFile';
				} else if (c.indexOf('detach') != -1) {
					fn = 'detachFile';
				} else {
					return;
				}
				
				item = dojo.dom.getAncestorsByTag(target, 'LI')[0];
				id = parseIntRight(item.id);
				FileList[fn](evt, item, id, target);
				
				dojo.stopEvent(evt);
				return false;
			}
		},
		
		/**
		 * Draw 
		 * @param {Object} data
		 */
		fillFiles: function(data, forceRedraw) {
			var opened = hi.items.manager.opened();
			if (!opened || !data) return;
			
			//Remove all completed files
			var nodeList = FileList._getNodeList();
			
			//Get all current files
			var taskId = opened.taskId,
				oldFiles = {},
				children,
				fileId;
			
			children = dojo.dom.getChildElements(nodeList);
			
			var i = 0,
				ii = children.length;
			
			for(i=ii-1; i>=0; i--) {
				fileId = children[i].id;
				if (fileId && (fileId = parseIntRight(fileId))) {
					if (forceRedraw) {
						// #4389
						nodeList.removeChild(children[i]);
					} else {
						oldFiles[fileId] = true;
					}
				} else if (dojo.hasClass(children[i], 'file-success')) {
					//Remove completed file nodes
					nodeList.removeChild(children[i]);
				}
			}
			
			//Add new files
			i = 0;
			ii = data.length;
				
			for(; i<ii; i++) {
				if (data[i].parent == taskId && data[i].category == 5) {
					if (data[i].id in oldFiles) {
						delete(oldFiles[data[i].id]);
					} else {
						FileList.addFile(data[i]);
					}
				}
			}
			
			//Remove old files which are not in the list anymore
			for(fileId in oldFiles) {
				FileList.fadeOutFile(fileId, 'puff');
			}
			
		},
		
		/**
		 * Load tasks file list from server
		 */
		load: function (taskId, force) {
			if (force) {
				hi.items.manager.refresh();
			} else {
				var files = hi.items.manager.get({"category": 5, "parent": taskId});
				FileList.fillFiles(files);
			}
		},
		
		forceRedrawOpened: function() {
			var oid = hi.items.manager.openedId();
			if (oid) {
				FileList.fillFiles(hi.items.manager.get({"category": 5, "parent": oid}), true);
			}
		}
	};

	return dojo.global.FileList;
	
});