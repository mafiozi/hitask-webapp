//dojo.provide("hi.sortable.task");
define(function () {
	
	//There are several dojo.dnd.HiSortDragSource object (one for each project),
	//making sure we have dragNode in all of them 
	dojo.global.HiSortDragSource_dragNode = null;
	
	dojo.declare("dojo.dnd.HiSortDragSource",[dojo.dnd.Source],{
		withHandles:true,
		delay: 5,
		accept: ["task"],
		type: "task",
		avatarClass: dojo.dnd.HiSortDragNone,
		/* #2535 via https://support.sitepen.com/issues/22098 */
		onSelectStart:function(e) {return;},
		onDndStart: function(source, nodes, copy) {
			dojo.dnd.HiSortDragSource.superclass.onDndStart.call(this, source, nodes, copy);
			if (source == this) {
				HiSortDragSource_dragNode = nodes[0];
				dojo.addClass(nodes[0], 'sortDropIndicatorClass');
			}
		},
		
		onDndCancel: function () {
			dojo.dnd.HiSortDragSource.superclass.onDndCancel.call(this);
			
			if (HiSortDragSource_dragNode) {
				dojo.removeClass(HiSortDragSource_dragNode, 'sortDropIndicatorClass');
				this.setSortingDelay();
			}
		},
		
		onDndDrop: function (_1b, _1c, _1d, _1e) {
			dojo.dnd.HiSortDragSource.superclass.onDndCancel.call(this, _1b, _1c, _1d, _1e);
			
			if (HiSortDragSource_dragNode) {
				dojo.removeClass(HiSortDragSource_dragNode, 'sortDropIndicatorClass');
				this.setSortingDelay();
			}
		},
		
		onMouseDown: function(e){
			// summary: event processor for onmousedown
			// e: Event: mouse event
			
			if(!this.mouseDown && this._legalMouseDown(e) && (!this.skipForm || !dojo.dnd.isFormElement(e))){
			//if(!this.mouseDown && (!this.skipForm || !dojo.dnd.isFormElement(e))){
				//var self = this;
				//setTimeout(function () {self.mouseDown = true;}, 0);
				this.mouseDown = true;
				this._lastX = e.pageX;
				this._lastY = e.pageY;
				dojo.dnd.Source.superclass.onMouseDown.call(this, e);
			}
		},
		
		onMouseMove: function (e) {
			dojo.dnd.HiSortDragSource.superclass.onMouseMove.call(this, e); 
			
			if (this.isDragging && HiSortDragSource_dragNode && HiSortDragSource_dragNode != this.current) {
				//JDT var m = dojo.dnd.Manager();
				var m = dojo.dnd.Manager.manager();
				//Make sure item can be dropped and that it comes from 'dojo.dnd.HiSortDragSource'
				if (m.canDropFlag && this.current && m.source.declaredClass == this.declaredClass) {
					if (this.before) {
						insertBefore(HiSortDragSource_dragNode, this.current);
					} else {
						insertAfter(HiSortDragSource_dragNode, this.current);
					}
				}
			}
		},
		
		setSortingDelay: function () {
			dojo.global.isSorting = true;
			setTimeout(function () {
				dojo.global.isSorting = false;
			}, 100);
		}
	});
	
	return dojo.dnd.HiSortDragSource;
});