define([
	"dojo/_base/declare",
	'dojo/sniff',
	'dojo/on',
	'dojo/aspect',
	'dojo/_base/lang',
	'dojo/_base/event',
	"dojo/_base/fx",
	'dojo/string',
	'dojo/store/Memory',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dijit/_HasDropDown' ,
	'dijit/registry',
	'hi/project/store/friendsStore',
	'hi/widget/form/hiSharing/hiSharingDropDownItemBase',
	'dojo/text!./hiSharingDropDown.html'
], function(declare, sniff, on, aspect, lang, event, fx, string, Memory,
	_WidgetBase, _TemplatedMixin, _HasDropDown, registry, fStore, itemBase, template) {

	var avatarAttr = 'image16src';

	var PlainSharingItem = declare('hi.widget.sharingDropDownItemPlain', [_WidgetBase, _TemplatedMixin, itemBase], {

		baseClass: 'hiSharingDropDownItem item',

		templateString: [
			"<div>",
				"<img src='${image}'></img><div class='name'>${name}</div>",
				"<span class='icon icon-hitask' data-dojo-attach-point='iconNode'></span>",
			"</div>",
		].join('')
	});

	var EveryOneSharingItem = declare('hi.widget.sharingDropDownItemPlain', [_WidgetBase, _TemplatedMixin, itemBase], {

		baseClass: 'hiSharingDropDownItem item everyone',

		everyone: true,
		
		templateString: [
			"<div class='everyone'>",
				"<div class='iconContainer'><span class='icon icon-users'></span></div>",
				"<div class='name'>${name}</div><span class='icon icon-hitask' data-dojo-attach-point='iconNode'></span>",
			"</div>",
		].join(''),

		postCreate: function(){
			this.inherited(arguments);
		}
	});

	var hiSharingList = declare('hi.widget.SharingList', [_WidgetBase, _TemplatedMixin], {
		// summary:
		//		A trivial popup widget
		baseClass: 'hiSharingList dijitMenu',

		templateString: "<div><ul data-dojo-attach-point='list'></ul></div>",

		avatarAttr: 'image16src',

		data: null,

		taskId: 0,

		_setDataAttr: function(){},

		postCreate: function(){
			this.own(
				// on(this._buttonNode, touch.press, lang.hitch(this, "_onDropDownMouseDown")),
				on(this.list, "click", lang.hitch(this, "_onListClick"))
				// on(keyboardEventNode, "keydown", lang.hitch(this, "_onKey")),
				// on(keyboardEventNode, "keyup", lang.hitch(this, "_onKeyUp"))
			);
		},

		renderList: function(datas){
			var len = datas.length,
				li, avatar, name, checkmark, i;

			this.list.innerHTML = '';

			for(i = 0; i < len; i++){
				data = datas[i];

				li = document.createElement('li');
				li.id = 'hiSharingListItem_' + data.id;
				if(data.selected === true){
					dojo.addClass(li, 'selected');
				}
				dojo.addClass(li, 'hiSharingItem');

				//namewrapper
				nameWrapper = document.createElement('div');
				dojo.addClass(nameWrapper, 'nameWrapper');
				nameNode = document.createElement('div');
				dojo.addClass(nameNode, 'name');
				nameNode.innerHTML = data.name;
				nameWrapper.appendChild(nameNode);

				if(data.isEveryOne === true){
					avatarNode = document.createElement('span');
					dojo.addClass(avatarNode, 'icon-users');
					dojo.addClass(avatarNode, 'avatar');
				}else{
					avatarNode = document.createElement('img');
					avatarNode.src = data[avatarAttr];
					dojo.addClass(avatarNode, 'avatar');
				}

				// checkmark
				checkmark = document.createElement('div');
				dojo.addClass(checkmark, 'checkmark');
				checkmark.innerHTML = '<span class="icon-checkmark2"></span>';

				li.appendChild(nameWrapper);
				li.appendChild(avatarNode);
				li.appendChild(checkmark);
				this.list.appendChild(li);
			}
		},

		refreshList: function(){
			var data = this.data, key, temp;

			for(key in data){
				temp = data[key];
				node = dojo.byId('hiSharingListItem_' + temp.id);

				if(node){
					if(temp.selected === true){
						dojo.addClass(node, 'seleced');
					}else{
						dojo.removeClass(node, 'selected');
					}
				}
			}
		},

		onItemSelectedUpdate: function(id){},

		_onListClick: function(e){
			var li = dojo.query(e.target).closest('li.hiSharingItem')[0],
				id, part,
				_selected, item, taskId;

			if(li){
				part = li.id.split('_')[1];
				id = isNaN(part)? part : parseInt(part, 10);
				taskId = parseInt(this.taskId, 10);
				item = taskId && hi.items.manager.get(taskId);

				if(item && id === item.user_id){		//user can't edit task owner's permission
					return;
				}
				on.emit(this.domNode, 'onToggleItemSelect', {
					bubbles: true,
					cancelable: true,
					id: id
				});
				this.onItemSelectedUpdate(id);
			}
			event.stop(e);
		}
	});

	var hiSharingDropDown = declare('hi.widget.SharingDropDown', [_WidgetBase, _TemplatedMixin, _HasDropDown], {
		// summary:
		//		A button that shows a popup.

		forceWidth: true,

		_dummy: false,

		_selectedIds: {},

		_items: [],

		value: [],
		_getValueAttr: function(){
			var pms = this.store.query({selected: true}),
				len = pms.length, i, pm,
				value = [];
				// result = [];

			for(i = 0; i < len; i++){
				// value = {};
				pm = pms[i];
				if(pm.id !== Project.user_id){
					value.push({principal: pm.id, level: pm.permission});
					// result.push(value);
				}
			}
			
			if (hi.items.manager.editingId() && hi.items.manager.get(hi.items.manager.editingId(), 'adminPermission')) {
				// "if" added for #5519
				var permz = hi.items.manager.get(hi.items.manager.editingId(), 'permissions');
				if (typeof(permz) == 'object') {
					var found = false;
					for (var i = 0, len = permz.length; i < len; i++) {
						if (!found && permz[i].principal == Project.user_id) {
							value.push({principal: permz[i].principal, level: permz[i].level});
							found = true;
						}
					}
				}
			} else {
				value.push({principal: Project.user_id, level: Hi_Permissions.EVERYTHING});
			}
			
			return value;
			// return result;
		},

		store: null,
		_setStoreAttr: function(value) {
			var k, temp, items, i, item;
			this._set('store', value);
			
			// Bug #5549
			// items = dojo.query('.hiSharingDropDownItem', this.domNode);

			// for (i = 0; i < items.length; i++) {
			// 	registry.byNode(items[i]).destroy();
			// }
			if (this._items.some(function(item) {
				return item._opened;
			})) {
				return;
			}

			while (item = this._items.shift()) {
				item.destroy();
			}

			this._updateItemPosition();
			this.set('count', 0);

			var selectedItems = this.store.query({selected: true}),
				len = selectedItems.length;

			for (i = 0; i < len; i++) {
				this.addItem(selectedItems[i].id, true);
			}
		},
		
		count: 0,
		_setCountAttr: function(value){
			this._set('count', value);

			if(this.count > 0){
				if(this.count === 1){
					dojo.removeClass(this.domNode, 'wide');
					this.button.style.height = '26px';
					this.itemContainer.style.height = '26px';
				}else if(this.count > 1){
					dojo.addClass(this.domNode, 'wide');
					if(this.count === 2){
						this.button.style.height = '26px';
						this.itemContainer.style.height = '26px';
					}else if(this.count === 3 || this.count === 4){
						this.button.style.height = '48px';
						this.itemContainer.style.height = '48px';
					}else{
						this.button.style.height = '69px';
						this.itemContainer.style.height = '69px';
					}
					if(this.count > 6){
						dojo.addClass(this.domNode, 'scroll');
						this.button.style.height = '77px';
						this.itemContainer.style.height = '77px';
					}else{
						dojo.removeClass(this.domNode, 'scroll');
					}
				}
				dojo.global.hideElement(this.placeholder);
			}else{
				dojo.global.showElement(this.placeholder);
				this.button.style.height = '26px';
				this.itemContainer.style.height = '26px';
			}
			// this.onChange();
			this.onCountChange();
		},

		orient: ["below"],

		templateString: template,

		constructor: function(){
			this.inherited(arguments);
			this.store = new fStore();
			this.forceWidth = true;
			this._dummy = false;
			this._selectedIds = {};
			this._items = [];
		},

		postCreate: function(){
			this.inherited(arguments);
			if(this._dummy === true){
				this._setDummyData();
			}
			this.dropDown = new hiSharingList({
				id: this.id + "_dropDown",
				data: this.data,
				taskId: this.taskId
			});
			this.button.style.height = '22px';
			this.itemContainer.style.height = '22px';

			this.own(
				aspect.before(this, 'openDropDown', lang.hitch(this, function(){
					// console.log('refresh list');
					// this.dropDown.renderList();
				})),
				on(this.dropDown, 'onItemDeselected', lang.hitch(this, '_onItemDeselected')),
				on(this.dropDown, 'onItemSelected', lang.hitch(this, '_onItemSelected')),
				on(this.dropDown, 'onToggleItemSelect', lang.hitch(this, '_toggleItem'))
				// on(this, 'click', function(e){})
			);
		},

		focus: function(){},

		_setDummyData: function(){
			var count = 10,
				data = [], i, store;
			for(i = 0; i < count; i++){
				data.push({
					name: 'gee' + i,
					id: new Date().getTime() + i,
					image24src: dojo.global.getAvatar(16)
				});
			}
			data.unshift({
				name: 'Everyone',
				id: 'everyone',
				isEveryOne: true,
				image24src: '/avatar/' + Project.userPictureHash + '.24.gif'

			});
			store = new Memory({data: data});

			this.set('store', store);
		},

		onCountChange: function(){},
		onChange: function(){},

		_onDropDownMouseDown: function(e){
			var target = e.target;
		
			if(target === this.itemContainer || target === this.placeholder || dojo.query(target).closest('.button')[0] === this.button){
				this.inherited(arguments);
				this.domNode.focus();
				document.activeElement = this.domNode;
			}
			// if(dojo.query(target).closest('.hiSharingDropDownItem').length || target === this.placeholder || ){
			// 	return false;
			// }
		},

		isLoaded: function(){
			// summary:
			//		Returns true if the dropdown exists and it's data is loaded.  This can
			//		be overridden in order to force a call to loadDropDown().
			// tags:
			//		protected

			return false;
		},

		loadDropDown: function(callback){
			var result = this.store.query({name: ''});

			this.dropDown.renderList(result);
			this.inherited(arguments);
		},

		addItem: function(id, skipOnChange) {
			var data = this.store.get(id), item,
				t = this,
				defaultSharingPermission = Hi_Preferences.get('default_sharing_permission', 50, 'int');

			if(!data || data.selected === false){return;}

			if(data.isEveryOne === true){
				item = new EveryOneSharingItem({
					id: this.id + '_item_' + id,
					_uuid: id,
					permission: data.permission || defaultSharingPermission,
					data: data,
					image: '',
					name: data.name,
					taskId: this.taskId
				});
			}else{
				item = new PlainSharingItem({
					id: this.id + '_item_' + id,
					_uuid: id,
					permission: data.permission || defaultSharingPermission,
					data: data,
					image: data[avatarAttr],
					name: data.name,
					taskId: this.taskId
				});
			}
			item.placeAt(this.itemContainer);
			item.startup();
			this._items.push(item);
			this._selectedIds[id] = true;
			this.own(
				aspect.after(item, 'onRemove', function(uuid) {
					t.removeItem(uuid, true);
				}, true),
				on(item, 'permissionchange', function(e) {
					if(e.value && e._uuid){
						var item = t.store.get(e._uuid);
						item.permission = e.value;
						t.store.put(item, {overwrite: true});
						t.onChange();
					}
				})
			);
			this.set('count', ++this.count);
			this._updateItemPosition();
			if(!skipOnChange) this.onChange();
			// this.button.style.height = this.domNode.clientHeight + 'px';
		},

		removeItem: function (id, anim) {
			var widgetId = this.id + '_item_' + id;
				widget = registry.byId(widgetId),
				data = this.store.get(id),
				taskId = parseInt(this.taskId, 10),
				item = taskId && hi.items.manager.get(taskId);

			if ((item && item.user_id === id) || !data) return;
			if (!data) return;
			if (anim === true){
			}
			data.selected = false;
			widget && widget.destroy();
			var index = this._items.indexOf(widget);
			if (index >= 0) {
				this._items.splice(index, 1);
			}
			this.set('count', --this.count);
			this._selectedIds[id] = false;

			this._updateItemPosition();
			this.onChange();
		},

		_updateItemPosition: function(node){
			var items = dojo.query('.item', this.itemContainer),
				len = items.length,
				item, top, left, i;

			if(!len){ return; }

			for(i = 0; i < len; i++){
				item = items[i];
				top = Math.floor(i / 2) * 22;
				left = Math.floor(i % 2) * 112;

				item.style.left = 2 + left + 'px';
				item.style.top = 2 + top + 'px';
			}
		},

		_toggleItem: function(e) {
			var data = this.store.get(e.id);
			
			if (data) {
				data.selected = !data.selected;
				if (data.selected === true) {
					this.addItem(e.id);
				} else {
					this.removeItem(e.id);
				}
			}
			// this.onChange();
			this.closeDropDown();
		},

		_onItemSelected: function(e) {
			this.addItem(e.id);
			this.closeDropDown();
		},

		_onItemDeselected: function(e) {
			this.removeItem(e.id);
			this.closeDropDown();
		}
	});

	return hiSharingDropDown;
});