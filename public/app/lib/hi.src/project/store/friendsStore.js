/**
 * A dojo.store.Memory object with a customized query() method
 * In particular, the query searches for either first, last, or name attribute in its data array
 * 
 * TODO: Generalize what and how many attributes to inclusively search data on
 */
define([
	"dojo/_base/declare",
	"dojo/_base/lang",
	'dojo/store/Memory',
	"dojo/store/util/QueryResults"
], function (declare, lang, Memory, QueryResults) {
	
	var friendStore = declare([Memory], {

		callback: function(results){},

		query: function(query, options) {
			//Use searchAttr = 'name';
			var results = [],
				val, temp, i;
			
			//Query against first name, or last name, or name attribute
			if(query.hasOwnProperty('name')) {
				val = query['name'];
				//May need to escape special characters, for example, if the user selects 'Myself',
				//the query will be in the form: {name:'Myself (First name Last name)}
				//and the parentheses will screw up the matching if they are not escaped
				//
				// Also, if user types in combobox, the query is in the form of {name:RegExp(...)},
				// but when they use the drop down arrow, it is in the form of {name:{String}},
				// in which case toLowerCase() is defined on the query value
				if (val.toLowerCase) {
					val = val.toLowerCase();
					val = val.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
				}
				
				for (i = 0; i < this.data.length; i++) {
					temp = this.data[i];
					
					if (!temp) continue;

					if (
						(temp.firstName && temp.firstName.toLowerCase().match(val)) ||
						(temp.lastName && temp.lastName.toLowerCase().match(val)) ||
						(temp.name && temp.name.toLowerCase().match(val))
					) {
						results.push(temp); }
				}
			} else if(query.hasOwnProperty('id')) {
				val = query['id'];
				for (var i = 0; i < this.data.length; i++) {
					if (this.data[i].id !== undefined && this.data[i].id.toString() === val.toString()) {
						results.push(this.data[i]);
					}
				}
			}else{
				return this.inherited(arguments);
			}
			// The callback is general enough, but it is used specifically
			// to tell the widget whether or not the 'New person...' button should be enabled/visible
			this.callback(results);
			return QueryResults(results);
		}
	});
	
	return friendStore;
});