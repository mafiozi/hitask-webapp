//dojo.provide("hi.timetracking.report");

/**
 * Initialize report tooltip "Time report" content
 */
define(function () {
	var REPORT_FROM_DIFF_MS = 518400000;
	
	dojo.global.initTimeReport  = function() {
		var calendars;

		if (!dijit.byId('report_from') || !dijit.byId('report_to')) {
			return setTimeout(function() {
				initTimeReport();
			}, 100);
		}

		if (!initTimeReport.calendarsInitialized) {
			initTimeReport.calendarsInitialized = true;
			
			var inp_from = dojo.byId('report_from');
			var inp_to = dojo.byId('report_to');
			
			var format_options = {format: Hi_Calendar.format, format_year: 'Y', add_year: true};
			
			// var calendars = dojo.parser.instantiate([inp_from, inp_to]);
			calendars = [dijit.byId('report_from'), dijit.byId('report_to')];
			
			inp_from = dojo.byId('report_from');
			inp_to = dojo.byId('report_to');
			
			//Reformat date
			function reformatDate(date) {
				if (date) {
					initTimeReport[(this.textbox.id == 'report_from' ? 'date_from' : 'date_to')] = date;
					this.textbox.value = time2str(date, Hi_Calendar.format);
					
					if (initTimeReport['date_to'] && hi.items.date.truncateTime(initTimeReport['date_from']) >= hi.items.date.truncateTime(initTimeReport['date_to'])) {
						dijit.byId('report_from').set('value', new Date(initTimeReport['date_to'].getTime() - REPORT_FROM_DIFF_MS));
					}
				}
			}
			
			dojo.forEach(calendars, function(cal) {
				cal.onChange = reformatDate; // Show button if value changed.
			});
			
			
			initTimeReport.calendars = calendars;
		}
		
		var t_now = new Date();
		var t_last = new Date(t_now.getTime() - REPORT_FROM_DIFF_MS);
		
		calendars = initTimeReport.calendars;
		calendars[0].set('value', t_last);
		calendars[1].set('value', t_now);
		calendars[0].set('constraints', {datePattern: Hi_Calendar.format.replace('m', 'MM').replace('d', 'dd').replace('y', 'yyyy')});
		calendars[1].set('constraints', {datePattern: Hi_Calendar.format.replace('m', 'MM').replace('d', 'dd').replace('y', 'yyyy')});
		
		initTimeReport.date_from = t_last;
		initTimeReport.date_to = t_now;
	};
	
	dojo.global.initGlobalReport = function() {
		var calendars;

		if (!initGlobalReport.calendarsInitialized) {
			initGlobalReport.calendarsInitialized = true;
			
			var inp_from = dojo.byId('report_progress_from');
			var inp_to = dojo.byId('report_progress_to');
			
			var format_options = {format: Hi_Calendar.format, format_year: 'Y', add_year: true};
			
			// var calendars = dojo.parser.instantiate([inp_from, inp_to]);
			calendars = [dijit.byId('report_progress_from'), dijit.byId('report_progress_to')];

			inp_from = dojo.byId('report_progress_from');
			inp_to = dojo.byId('report_progress_to');
			
			//Reformat date
			function reformatDate(date) {
				if (date) {
					initGlobalReport[(this.textbox.id == 'report_progress_from' ? 'date_from' : 'date_to')] = date;
					this.textbox.value = time2str(date, Hi_Calendar.format);
					
					if (initGlobalReport['date_to'] && hi.items.date.truncateTime(initGlobalReport['date_from']) >= hi.items.date.truncateTime(initGlobalReport['date_to'])) {
						dijit.byId('report_progress_from').set('value', new Date(initGlobalReport['date_to'].getTime() - REPORT_FROM_DIFF_MS));
					}
				}
			}
			
			dojo.forEach(calendars, function(cal) {
				cal.onChange = reformatDate; // Show button if value changed.
			});
			
			
			initGlobalReport.calendars = calendars;
		}
		
		var t_now = new Date();
		var t_last = new Date(t_now.getTime() - REPORT_FROM_DIFF_MS);
		
		calendars = initGlobalReport.calendars;
		calendars[0].set('value', t_last);
		calendars[1].set('value', t_now);
		calendars[0].set('constraints', {datePattern: Hi_Calendar.format.replace('m', 'MM').replace('d', 'dd').replace('y', 'yyyy')});
		calendars[1].set('constraints', {datePattern: Hi_Calendar.format.replace('m', 'MM').replace('d', 'dd').replace('y', 'yyyy')});
		
		initGlobalReport.date_from = t_last;
		initGlobalReport.date_to = t_now;
	};
	
	dojo.global.initProjectGlobalReport = function() {
		var calendars;

		if (!initProjectGlobalReport.calendarsInitialized) {
			initProjectGlobalReport.calendarsInitialized = true;
			
			var inp_from = dojo.byId('project_report_progress_from');
			var inp_to = dojo.byId('project_report_progress_to');
			
			var format_options = {format: Hi_Calendar.format, format_year: 'Y', add_year: true};
			
			// calendars = dojo.parser.instantiate([inp_from, inp_to]);
			calendars = [dijit.byId('project_report_progress_from'), dijit.byId('project_report_progress_to')];

			inp_from = dojo.byId('project_report_progress_from');
			inp_to = dojo.byId('project_report_progress_to');
			
			//Reformat date
			function reformatDate(date) {
				if (date) {
					initProjectGlobalReport[(this.textbox.id == 'project_report_progress_from' ? 'date_from' : 'date_to')] = date;
					this.textbox.value = time2str(date, Hi_Calendar.format);
					
					if (initProjectGlobalReport['date_to'] && hi.items.date.truncateTime(initProjectGlobalReport['date_from']) >= hi.items.date.truncateTime(initProjectGlobalReport['date_to'])) {
						dijit.byId('project_report_progress_from').set('value', new Date(initProjectGlobalReport['date_to'].getTime() - REPORT_FROM_DIFF_MS));
					}
				}
			}
			
			dojo.forEach(calendars, function(cal) {
				cal.onChange = reformatDate; // Show button if value changed.
			});
			
			
			initProjectGlobalReport.calendars = calendars;
		}
		
		var t_now = new Date();
		var t_last = new Date(t_now.getTime() - REPORT_FROM_DIFF_MS);
		
		calendars = initProjectGlobalReport.calendars;
		calendars[0].set('value', t_last);
		calendars[1].set('value', t_now);
		calendars[0].set('constraints', {datePattern: Hi_Calendar.format.replace('m', 'MM').replace('d', 'dd').replace('y', 'yyyy')});
		calendars[1].set('constraints', {datePattern: Hi_Calendar.format.replace('m', 'MM').replace('d', 'dd').replace('y', 'yyyy')});
		
		initProjectGlobalReport.date_from = t_last;
		initProjectGlobalReport.date_to = t_now;
	};
	
	dojo.global.reportTooltipCloseHandler = function() {
		var from = dijit.byId('report_from');
		var to = dijit.byId('report_to');
		
		if ((from && from.dropDown && from.dropDown._popupWrapper && from.dropDown._popupWrapper.style.display != 'none') ||
			(to && to.dropDown && to.dropDown._popupWrapper && to.dropDown._popupWrapper.style.display != 'none')) {
			
		} else {
			Tooltip.close('report');
		}
	};
	
	dojo.global.getTimeReportDates  = function(as_string) {
		if (as_string) {
			var f = initTimeReport.date_from, t = initTimeReport.date_to;
			return 'from_date=' + (f ? time2str(f, 'y-m-d') : '') + '&to_date=' + (t ? time2str(t, 'y-m-d') : '');
		} else {
			return {
				'from_date': initTimeReport.date_from,
				'to_date': initTimeReport.date_to
			};
		}
	};
	
	dojo.global.getGlobalReportDates = function(as_string) {
		var isDateRange = dojo.byId('report_progress_between').checked;
		
		if (as_string) {
			var f = initGlobalReport.date_from, t = initGlobalReport.date_to;
			return isDateRange ? 'from_date=' + (f ? time2str(f, 'y-m-d') : '') + '&to_date=' + (t ? time2str(t, 'y-m-d') : '') : '';
		} else {
			return isDateRange ? {
				'from_date': initGlobalReport.date_from,
				'to_date': initGlobalReport.date_to
			} : {};
		}
	};
	
	dojo.global.getProjectGlobalReportDates = function(as_string) {
		var isDateRange = dojo.byId('project_report_progress_between').checked;
		
		if (as_string) {
			var f = initProjectGlobalReport.date_from, t = initProjectGlobalReport.date_to;
			return isDateRange ? 'from_date=' + (f ? time2str(f, 'y-m-d') : '') + '&to_date=' + (t ? time2str(t, 'y-m-d') : '') : '';
		} else {
			return isDateRange ? {
				'from_date': initProjectGlobalReport.date_from,
				'to_date': initProjectGlobalReport.date_to
			} : {};
		}
	};
	
	return {};
	
});