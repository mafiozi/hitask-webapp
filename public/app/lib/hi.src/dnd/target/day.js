/**
 * Specifies tooltip on calendar day drop target
 */
//dojo.provide("hi.dnd.target.day");
define(function () {
	
	dojo.declare("dojo.dnd.HiDragTargetDay", [dojo.dnd.Source], {
		isSource: false,

		selfAccept: false,

		accept: ["dragged_task"],

		dropValue: null,

		inDropArea: false,
		
		permissionsAllowed: [Hi_Permissions.MODIFY, Hi_Permissions.EVERYTHING],

		action: 'modify',

		onDndStart: function (_17, _18, _19) {
			dojo.dnd.HiDragTargetDay.superclass.onDndStart.call(this, _17, _18, _19);
			this.dropValue = time2str(HiTaskCalendar.currentDate, Hi_Calendar.format);
		},
		
		onMouseOut: function (e) {
			dojo.dnd.HiDragTargetDay.superclass.onMouseMove.call(this, e);
			
			//JDT var m = dojo.dnd.Manager();
			var m = dojo.dnd.Manager.manager();

			if (m.source instanceof dojo.dnd.HiDragSource) {
				m.avatar.updateText('');
				this.inDropArea = false;
			}
		},
		
		onMouseOver: function (e) {
			dojo.dnd.HiDragTargetDay.superclass.onMouseOver.call(this, e);
			

			if (this.isDragging) {
				//JDT var m = dojo.dnd.Manager();
				var m = dojo.dnd.Manager.manager();
				if (m.source instanceof dojo.dnd.HiDragSource) {
					if (parseInt(Hi_Preferences.get('sidebar_calendar')) == 0) return;
					
					var type = hi.items.manager.get(m.source.dragNode.dragId, 'category');
					var id = m.source.dragNode.dragId;
					var item = him.get(id);
					/* #3059 Note can be d&d to the calendar and set the time*/
					if (type == hi.widget.TaskItem.CATEGORY_FILE || type == hi.widget.TaskItem.CATEGORY_NOTE) return;
					
					this.inDropArea = true;

					var txt = this.text + this.dropValue;
					if (type === hi.widget.TaskItem.CATEGORY_TASK) {
						if (item.start_date) {
							txt = hi.i18n.getLocalization('center.start_on') + ' ' + this.dropValue;
						} else {
							txt = hi.i18n.getLocalization('center.due_on') + ' ' + this.dropValue;
						}
					}
					m.avatar.updateText(txt);
				}
			}
		}
		
	});

	return dojo.dnd.HiDragTargetDay;
});
