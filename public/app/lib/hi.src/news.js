//dojo.provide("hi.news");
define(function () {
	
	return dojo.global.Hi_News = {
		cache: {
			'_loading': {
				article: {
					permalink: '_loading',
					content: '<div class="loading"><!-- --></div>'
				}
			}
		},
		
		showArticle: function (link) {
			var permalink = link.getAttribute('href');
				permalink = permalink.match(/([^\/\?]*)(\?.*)?$/);;
				permalink = permalink[1];
				
			if (permalink) {
				if (!Hi_News.cache[permalink]) {
					//Show loading indicator
					Hi_News.updateContent(Hi_News.cache['_loading']);
					
					//Load content
					Project.ajax('news', {'permalink': permalink}, Hi_News.updateContent);
				} else {
					//Show content
					Hi_News.updateContent(Hi_News.cache[permalink]);
				}
			}
		},
		
		show: function (event, target) {
			Tooltip.open('news', "", target, event, "below-centered");
			
			Hi_News.showArticle(target);
			
			return false;
		},
		
		hide: function () {
			Tooltip.close('news');
		},
		
		updateContent: function (data) {
			if (data && data.article) {
				if (!Hi_News.cache[data.article.permalink]) {
					Hi_News.cache[data.article.permalink] = data;
				}
				
				var node_heading = dojo.byId('news_tooltip_heading');
				var node_date = dojo.byId('news_tooltip_posted');
				var node_content = dojo.byId('news_tooltip_text');
				var node_prev = dojo.byId('news_tooltip_next');
				var node_next = dojo.byId('news_tooltip_prev');
				
				if (data.article.title) {
					dojo.removeClass(node_heading, 'hidden');
					node_heading.innerHTML = data.article.title;
				} else {
					dojo.addClass(node_heading, 'hidden');
				}
				
				if (data.article.create_date) {
					dojo.removeClass(node_date.parentNode, 'hidden');
					node_date.innerHTML = data.article.create_date;
				} else {
					dojo.addClass(node_date.parentNode, 'hidden');
				}
				
				if (data.article.content) {
					dojo.removeClass(node_content, 'hidden');
					node_content.innerHTML = data.article.content;
				} else {
					dojo.addClass(node_content, 'hidden');
				}
				
				if (data.next_article_permalink) {
					dojo.removeClass(node_next.parentNode, 'hidden');
					node_next.setAttribute('href', '/blog/' + data.next_article_permalink);
				} else {
					dojo.addClass(node_next.parentNode, 'hidden');
				}
				
				if (data.previous_article_permalink) {
					dojo.removeClass(node_prev.parentNode, 'hidden');
					node_prev.setAttribute('href', '/blog/' + data.previous_article_permalink);
				} else {
					dojo.addClass(node_prev.parentNode, 'hidden');
				}
			} else {
				Hi_News.hide();
			}
		}	
	};

});