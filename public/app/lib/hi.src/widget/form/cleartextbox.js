define([
	"dojo/_base/declare",
	"dijit/form/TextBox",
	"hi/shortcuts"
], function(declare, djTextBox) {
	var ClearTextBox = declare("hi.widget.ClearTextBox", [djTextBox], {
		// The "Delete" icon
		deleteText: '<span aria-hidden="true" data-icon=""></span>',
		// Search decoration icon
		decorationContent: '<span aria-hidden="true" data-icon=""></span>',
		
		// Fire the change event for every text change
		intermediateChanges: true,
		
		// PostCreate method
		// Fires *after* nodes are created, before rendered to screen
		postCreate: function() {
			// Do what the previous does with this method
			this.inherited(arguments);

			// Add widget class to the domNode
			var domNode = this.domNode;
			dojo.addClass(domNode, "hiClearBox");
			if (this.width) {
				dojo.style(domNode, "width", this.width);
			}

			// Create the "X" link
			this.clearLink = dojo.create("a", {
				className: "hiClear",
				innerHTML: this.deleteText
			}, domNode, "first");
			
			//decoration icon
			this.decorationLink = dojo.create("span", {
				className: "hiClearBoxDecoration",
				innerHTML: this.decorationContent
			}, domNode, "first");
			
			// Fix the width
			var startWidth = dojo.style(domNode, "width"),
				pad = dojo.style(this.domNode,"paddingRight");
			dojo.style(domNode, "width", parseInt(startWidth - pad) + "px");

			// Add click event to focus node
			this.connect(this.clearLink, "onclick", function(){
				// Clear the value
				this.set("value", "");
				// Focus on the node, not the link
				this.textbox.focus();
			});
			
			// Fall back to custom placeholder if native is missing.
			if (this.placeholder && !this.hasNativePlaceholderSupport()) {
				this.set('placeHolder', this.placeholder);
			}

			// Add intermediate change for self so that "X" hides when no value
			this.connect(this, "onChange", "checkValue");
			
			// Add ESC listener.
			this.connect(this.domNode, "keydown", "onEscPressed");

			// Check value right away, hide link if necessary
			this.checkValue();
		},
		
		checkValue: function(value) {
			dojo[(value != "" && value != undefined ? "add" : "remove") + "Class"](this.decorationLink, "dijitHidden");
			dojo[(value != "" && value != undefined ? "remove" : "add") + "Class"](this.clearLink, "dijitHidden");
		},
		
		onEscPressed: function(event) {
			if (event && event.keyCode == Hi_Shortcuts.KEY_ESCAPE) {
				this.set('value', '');
				if (this.focusNode && this.focused) {
					this.focusNode.blur();
				}
			}
		},
		
		hasNativePlaceholderSupport: function() {
			var inp = document.createElement('input');
			return typeof(inp.placeholder) != 'undefined';
		}
	});
	
	return ClearTextBox;
});