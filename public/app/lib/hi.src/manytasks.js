/**
 * "Many tasks" tab in new item form
 */

define([/*#2790 */'hi/widget/task/item_DOM'], function (item_DOM) {
	dojo.global.NewItemManyTasks = {

		taskData: null,
		
		maxItems: 50,

		_inProcess: false,

		/**
		 * Go to previous step
		 */
		goStepBack: function(){
			var container = this.getContainer();

			if (container) {
				dojo.removeClass(container, 'tab-many-tasks-2');
			}
		},
		
		/**
		 * Open task preview
		 */
		goStepForward: function() {
			var container = this.getContainer();

			if (!container) return;
			
			var inner = this.getInnerContainer(1, container),
				textarea = inner.getElementsByTagName('TEXTAREA')[0],
				value = dijit.byNode(textarea).get('value');
			
			if (value && typeof(value) == 'string' && this.validateRawValue(value.trim())) {
				dojo.addClass(container, 'tab-many-tasks-2');
				this.parseContent(value);
			}
		},
		
		validateRawValue: function(value) {
			if (!value || value.split('\n').length > CONSTANTS.manyTasksMaxItems) {
				Project.alertElement.showItem(14, {task_alert: 'You can create no more than ' + CONSTANTS.manyTasksMaxItems + ' items at a time.'});
				return false;
			}
			
			return true;
		},
		
		/**
		 * Save tasks
		 */
		/* #2766 */elements:null,
		/* #3086 - pass in a hint to add() to indicate where many tasks table is in DOM */
		add: function (isSubItem) {
			/* #2766 - elements in the form are incrementally removed as each one is successfully added */
			if (this._inProcess) {
				console.warn('in process of save many tasks');
				return false;
			}

			var i, ii;

			if (!this.elements){
				this.elements = dojo.query('tr.sub-task', NewItemManyTasks.getInnerContainer(2));
			}
			for(i = 0, ii = this.elements.length; i < ii; i++) {
				if(this.taskData[i]){
					this.taskData[i].assignee = Hi_Preferences.get('default_assign_user', 0, 'int');
				}
			}
			
			var count = this.taskData.length;
			var taskCount = count;
			var values = null;
			var ids = [];
			var firePostResponse, setSaveDeferred, setSaveIntervalTimer;
			var self = this;
			var rootNode = isSubItem?dojo.byId('itemNode_' + hi.items.manager.opened().widgetId) : hi.items.manager.editing().domNode;
			var toAddNode = dojo.query(".many_tasks_step_2", rootNode)[0];
			
			i = 0;
			/* #2766 */
			var createTask = function() {
				if (self.taskData.length > 0) {
					self._inProcess = true;
					values = self.taskData[0];
					var perms = Hi_Preferences.get('default_sharing_permission_auto');
					if (!values.permissions && Project.businessId && perms && perms !== "false") {
						values.permissions = perms;
					}
					delete(values.shared);
					var firePostResponse = firePOST({
						post:hi.items.manager.setSave,
						postArgs:[dojo.mixin(values,{manytasks:true})]
					});
					setSaveDeferred = firePostResponse.deferred;
					setSaveIntervalTimer = firePostResponse.intervalTimer;
					setSaveDeferred.then(dojo.hitch(self, function (id) {
						if (id) {
							//When request is complete new item will be added, we can remove this one
							self._inProcess = false;
							self.taskData.shift();
							self.elements.shift();
							var removeNode = dojo.query('tr',toAddNode)[1];
							removeNode.parentNode.removeChild(removeNode);
							count--;
							ids.push(id);
							if (!count) {
								NewItemManyTasks.addComplete(ids);
								//Close edit form
								var editing = hi.items.manager.editing();
								if (editing) {
									if (editing && editing.editingView && editing.editingView._setLastCreatedItemType) {
										editing.editingView._setLastCreatedItemType(5);
									}
									if (editing.closeEdit) {
										editing.closeEdit();
									} else {
										/* #2790 editing.closeEdit();*/
										item_DOM.closeEdit(editing.taskId);
									}
								}
								
								if (taskCount) {
									trackEvent(trackEventNames.ITEM_MANY_CREATE);
								}
							} else {
								// for testing setTimeout(function() {createTask();},5000);
								createTask();
							}
						} else {
							//Failure
							Project.alertElement.showItem(23);
							clearInterval(setSaveIntervalTimer);
						}
					}));
				}
			};
			
			createTask();
		},
		
		addComplete: function (ids) {
			if (hi.items.manager.editing().taskId < -1) hi.items.manager.remove(hi.items.manager.editing().taskId);
			this.taskData = null;
			this.elements = null;
			//Update data
			hi.items.manager.refresh().then(function () {
				var i = 0,
					ii = ids.length,
					item = null;
				
				for (; i<ii; i++) {
					item = hi.items.manager.item(data.id);
					if (item) item.highlight();
				}
			});
		},
		
		parseContent: function (str) {
			var lines = str.split(/(\n\r|\n|\r)/g);
			var tbody = this.getInnerContainer(2).getElementsByTagName('TBODY')[0];
			
			while(tbody.firstChild) {
				tbody.removeChild(tbody.firstChild);
			}
			
			var friends_html = '';
			var friends = Project.friends || [];
			var business_id = Project.businessId;
			var defaultAssignUser = Hi_Preferences.get('default_assign_user', 0, 'int');
			var i, ii;
			var selectedUser = function(userId) {
				return defaultAssignUser == userId ? " selected='selected' " : '';
			};
			
			// add myself to list
			friends_html += '<option ' + selectedUser(Project.user_id) + ' value="' + Project.user_id + '" class="">' + htmlspecialchars(Project.getMyself()) + '</option>';
			
			for(i = 0,ii = friends.length; i < ii; i++) {
				var friend = friends[i];
				//var name = (friend.id == Project.user_id ? hi.i18n.getLocalization('properties.assigned_myself') + ' (' + friend.name + ')' : friend.name);
				var name = friend.name;
				var friend_class = (business_id && !friend.businessStatus ? 'friend' : '');
				
				friends_html += '<option ' + selectedUser(friend.id) + ' value="' + friend.id + '" class="' + friend_class + '">' + htmlspecialchars(name) + '</option>';
			}
			
			this.taskData = [];
			var cnt = 0;
			
			var onchange = 'this.parentNode.parentNode.className = (this.value == \'1\' ? \'business-only\' : \'\');';
			
			for(i = 0,ii = lines.length; i < ii; i++) {
				str = dojo.trim(lines[i]);
				if (str) {
					var data = this.parseLine(str);
					
					if (data) {
						//Set priority
						data.priority = Math.min(Project.getMaxPriority(1) + (ii - i), 29999);
						
						var tr = document.createElement('TR');
						tr.className = 'sub-task';
						tr.setAttribute('task-index', this.taskData.length);
						var td = document.createElement('TD');
						var title = data.title +
									(data.start_date ? ', ' + (time2str(str2time(data.start_date, 'y-m-d'), Hi_Calendar.format) +
									(data.start_time ? (' ' + HiTaskCalendar.formatTime(data.start_time)) : '')) : '') +
									(data.end_date ? ', ' + (time2str(str2time(data.end_date, 'y-m-d'), Hi_Calendar.format) +
									(data.end_time ? (' ' + HiTaskCalendar.formatTime(data.end_time)) : '')) : '') +
									(data.message ? ', ' + data.message : '');
						td.className = 'col-title';
						td.innerHTML = '<div>' + htmlspecialchars(title) + '</div>';
						tr.appendChild(td);

						tbody.appendChild(tr);
						
						delete data.start_time;
						delete data.end_time;
						//Save into cache
						this.taskData.push(data);
						
						cnt++;
						if (cnt > this.maxItems) 
							break;
					}
				}
			}
		},
		
		parseLine: function (str) {
			var parts = str.split(',');
			
			var title = dojo.trim(parts.shift()),
				date_start = null,
				date_end = null,
				message = null;
			
			if (!title) return null;
			
			if (parts.length) {
				if (this.isDateLike(parts[0])) {
					date_start = this.parseDateTime(dojo.trim(parts[0]));
					parts = parts.slice(1);
					
					if (parts.length && this.isDateLike(parts[0])) {
						date_end = this.parseDateTime(dojo.trim(parts[0]));
						parts = parts.slice(1);
					}
					
					date_start = date_start || date_end;
				}
				
				if (parts.length) {
					//Last part is description
					parts = parts.slice(-1);
				}
			}
			
			message = dojo.trim(parts.join(','));
			
			var parent = hi.items.manager.editing();
			
			var start_date = date_start ? date_start[0] : '';
			var end_date   = date_end ? date_end[0] : '';
			var start_time = date_start ? date_start[1] : '';
			var end_time   = date_end ? date_end[1] : '';
			var is_all_day = false;
			/*Feature #4052 Enable Time Tracking preference*/
			var time_track = Hi_Preferences.get('enable_time_tracking', false, 'bool');
			
			if (start_date && (start_date || end_date) && !start_time && !end_time) {
				is_all_day = true;
			}
			
			if (start_date && start_time && !is_all_day) {
				start_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(start_date, start_time));
			} else if (start_date) {
				start_date = time2strAPI(HiTaskCalendar.getDateFromStr(start_date, true));
			} else if (start_time) {
				start_date = time2strAPI(HiTaskCalendar.getTimeFromStr(start_time));
			} else {
				start_date = "";
			}
			
			if (end_date && end_time && !is_all_day) {
				end_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(end_date, end_time));
			} else if (end_date) {
				end_date = time2strAPI(HiTaskCalendar.getDateFromStr(end_date));
			} else if (end_time) {
				end_date = time2strAPI(HiTaskCalendar.getTimeFromStr(end_time));
			} else {
				end_date = "";
			}
			/* #2790 */
			var parentParameter = '';
			var color;
			if (parent) {
				if (parent.get) {
					parentParameter = parent.get('parent');
				} else {
					parentParameter = parent.taskItem.parent;
				}
				if (parent.taskItem) {
					color = parent.taskItem.color;
				}
			}
			return {
				'title': title,
				'start_date': start_date,
				'end_date': end_date,
				'start_time': start_time,
				'end_time': end_time,
				'message': message,
				'color': color || 0,
				'completed': 0,
				'shared': 0,
				'assignee': '',
				'parent':parentParameter,
				//'parent': (parent ? /* #2790 parent.get("parent")*/parent.taskItem.parent : ''),
				/* #3086 - add recurring attribute; if it is undefined we cannot complete task in same session it was created */
				recurring:0,
				'time_track': time_track,
				'category': 1,
				recurring_interval:1,
				'user_id': Project.user_id,
				'is_all_day': is_all_day
			};
		},
		
		/**
		 * Returns if string looks like date/datetime
		 * 
		 * @param {String} str
		 * @return True if string is like date or date/time, otherwise false
		 * @type {Boolean}
		 */
		isDateLike: function (str) {
			return str.match(/^\s*\d+[-\/]\d+[-\/]\d+(\s+\d+:\d+(:\d+)?)?\s*$/) ? true : false;
		},
		
		/**
		 * Try parsing datetime
		 * @param {Object} str
		 */
		parseDateTime: function (str) {
			var parts = str.match(/^(\d+[-\/]\d+[-\/]\d+)(\s+\d{1,2}:\d{2}(:\d{2})?)?$/);
			var datetime = null;
			
			if (parts) {
				var date = this.parseDate(parts[1]);
				if (date) {
					date = time2str(date, 'y-m-d');
					datetime = [date, parts[2] ? dojo.trim(parts[2]) : null];
				}
			}
			
			return datetime;
		},
		
		/**
		 * Try parsing date against most common formats
		 * 
		 * @param {String} str
		 * @return Parsed date or null on failure
		 * @type {Date}
		 */
		parseDate: function (str) {
			var formats = [Hi_Calendar.format, 'y-m-d', 'y/m/d', 'm-d-y', 'm/d/y', 'd-m-y', 'd/m/y'];
			var date = null;
			
			for(var i = 0,ii = formats.length; i<ii; i++) {
				if (date = str2time(str, formats[i])) {
					return date;
				}
			}
			return null;
		},
		
		/**
		 * Returns container element
		 * 
		 * @return Container element
		 * @type {HTMLElement}
		 */
		getContainer: function () {
			var item = hi.items.manager.editing();
			return item ? item.editingView && item.editingView.domNode : null;
		},
		
		/**
		 * Returns step container element
		 * 
		 * @param {Number} step
		 * @param {HTMLElement} container
		 * @return Step container element
		 * @type {HTMLElement}
		 */
		getInnerContainer: function (step, container) {
			container = container || this.getContainer();
			if (!container) return;
			
			var inner = dojo.query('div.many_tasks_step_' + (step || 1), container);
			return (inner.length ? inner[0] : null);
		}
	};

	return dojo.global.NewItemManyTasks;
});
