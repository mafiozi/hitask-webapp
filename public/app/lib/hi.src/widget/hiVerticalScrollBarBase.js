define([
	'dojo/_base/declare',
	'dojox/calendar/_ScrollBarBase',
	'dojo/on',
	'dojo/_base/lang',
	'dojo/_base/event'
], function(declare, base, on, lang, event){
	return declare('hi.widget.calendar.VerticalScrollBar', [base], {
		scrollCallback: null,

		scrollable: true,

		_lastValue: 0,

		buildRendering: function(){
			this.inherited(arguments);

			if(this.scrollCallback){
				this.own(on(this.domNode, "scroll", lang.hitch(this, this.scrollCallback)));
			}else{
				this.own(on(this.domNode, "scroll", lang.hitch(this, function(e) {
					if(!this.scrollable){
						event.stop(e);
						this.domNode.scrollTop = this._lastValue;
						return false;
					}
					this._lastValue = this.value = this._getDomScrollerValue();
					this.onChange(this.value);
					this.onScroll(this.value);
				})));
			}
		},
	});
});