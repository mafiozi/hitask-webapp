//dojo.provide("hi.timetracking.list");
define(['dojo/Deferred', 'dojo/query'], function (Deferred, query) {
	
	Hi_TimeTracking.list = {
		node_progress: null,
		node_list: null,
		data: [],
		editing: null,
		
		visible: false,
		drawn: false,
		tmpIdCounter: 1,
		
		/**
		 * Returns entry 
		 * @param {Object} user_id
		 * @param {Object} date
		 */
		getIndexByUserDate: function (user_id, date) {
			for(var i = 0, ii = this.data.length; i < ii; i++) {
				if (this.data[i].user_id == user_id && this.data[i].date == date) {
					return i;
				}
			}
			
			return null;
		},
		
		/**
		 * Returns max allowed time which can be spent on task today by given user (backend limit)
		 */
		getMaxAllowedTime: function (user_id, date) {
			var index = this.getIndexByUserDate(user_id, date);
			if (index !== null) {
				return 32767 - parseInt(this.data[index].time_spent);
			}
			return 32767;
		},
		
		/**
		 * Add item to the list
		 */
		addItem: function (data) {
			data = dojo.mixin({
				'description': '',
				'date': '',
				'user_id': '',
				'time_spent': 0,
				'_logId': this.tmpIdCounter++
			}, data);
			
			var forceReload = false;
			if (!this.drawn) forceReload = true;	//If not drawn yet, need to load data from server
			
			this.sendDataToServer(data, forceReload);
			
			this.data.push(data);
			this.combineData();
			
			if (this.drawn) {
				this.redrawList();
			}
			
			return data;
		},
		
		/**
		 * Send request to server to remove item and remove item from data
		 */
		removeItem: function (user_id, date_id, remove_from_data) {
			var index = this.getIndexByUserDate(user_id, date_id),
				def;

			if (this.data[index]) {
				def = this._doRemoveItem(this.data[index].log_id, index, remove_from_data);
				
				// #2669: force call notifiers in order to refresh Project progress.
				setTimeout(function() {
					hi.store.ProjectCache.notifyComplete();
				}, 5000);
			}
			return def;
		},
		
		/**
		 * #5102
		 */
		_completeItemIfDone: function() {
			var item = hi.items.manager.get(hi.items.manager.openedId());
			var it = hi.items.manager.opened();
			
			if (item && item.time_est > 0 && !hi.items.manager.editingId() && item.permission >= Hi_Permissions.COMPLETE) {
				if ((this.getTotalSpentTime() >= item.time_est && !item.completed) ||
					(this.getTotalSpentTime() < item.time_est && item.completed)) {
					
					if (item.recurring && it && it.taskItem && it.taskItem.eventdate) {
						// #5838
						dojo.global.handleClickComplete(item.id, it.taskItem.eventdate);
					} else {
						dojo.global.handleClickComplete(item.id);
					}
				}
			}
		},
		
		_showThrobber: function() {
			var item = hi.items.manager.opened();
			if (item) {
				dojo.query('.itemTimetrackThrobberMsg', dojo.byId('propertiesViewNode_' + item.widgetId)).removeClass('hidden');
			}
		},
		
		_hideThrobber: function() {
			var item = hi.items.manager.opened();
			if (item) {
				dojo.query('.itemTimetrackThrobberMsg', dojo.byId('propertiesViewNode_' + item.widgetId)).addClass('hidden');
			}
		},
		
		_doRemoveItem: function(logId, index, removeFromData) {
			var self = this,
				def = new Deferred();
			
			this._showThrobber();
			Project.ajax('timedelete', {id: logId}, function() {
				self._hideThrobber();
				if (removeFromData) {
					self.data = self.data.slice(0, index).concat(self.data.slice(index + 1));
					
					if (self.drawn) {
						self.redrawList();
					}
				}
				self._completeItemIfDone();
				def.resolve();
			}, function(a, b, resp) {
				self._hideThrobber();
				if (resp && resp.getStatus() == 3) {
					alert('Sorry but you do not have enough permissions to delete this time log. Administrator can delete all users time logged for date, team member can delete only own time.');
				} else {
					alert((resp ? resp.getErrorMessage() : '') || 'Sorry but probably you do not have enough permission to delete this logged time.');
				}
				def.reject();
			});

			return def;
		},
		
		/**
		 * Change user data and send request to server
		 */
		editItem: function (old_user_id, old_date, data) {
			var self = this;

			if (old_user_id != data.user_id || old_date != data.date) {
				//User changed user and/or date so we need to remove current entry
				//and add new entry 
				var removeDeferred = this.removeItem(old_user_id, old_date, true, false);

				if(removeDeferred){
					removeDeferred.then(function(){
						self.addItem(data);
					});
				}
			} else {
				//User changed only time_spent
				var index = this.getIndexByUserDate(old_user_id, old_date);
				if (index !== false) {
					var new_time_spent = data.time_spent;
					var time_spent_change = data.time_spent - this.data[index].time_spent;
					
					// The problem here is that we send the delta but if we send 0 it clears the time item.
					// So we do not have a consistent way to do this
					if (time_spent_change == 0 && old_date == data.date) {
						return;
					}
					data.time_spent = time_spent_change;	
					data._logId = this.tmpIdCounter++;
					this.sendDataToServer(data);
					this.data[index].time_spent = new_time_spent;
					this.redrawList();
				} else {
					//What was he editing?
				}
			}
		},
		
		_sendDataToServer: function(taskId, data, callback) {
			data.id = taskId;
			Project.ajax('timelog', data, callback);
		},
		
		/**
		 * Send data to server
		 */
		sendDataToServer: function(postData, forceReload) {
			var item = hi.items.manager.opened();
			var self = this;
			
			if (item) {
				postData.returnId = true;
				this._showThrobber();
				this._sendDataToServer(item.taskId, postData, function(apiResp) {
					if (forceReload === true) {
						Hi_TimeTracking.list.reloadData(function () {
							var node;
							if (Hi_TimeTracking.addForm.node) {
								node = dojo.query('a.spent', Hi_TimeTracking.addForm.node.parentNode)[0];
							}
							if (!node) {
								node = dojo.query('a.spent',dojo.byId('propertiesViewNode_'+item.widgetId))[0];//item.query('a.spent', true);
							}
							var time_spent = Hi_TimeTracking.list.getTotalSpentTime();
							node.innerHTML = formatTrackTime(time_spent);
							
							// #2788: When time is logged (first time), the logged details should be shown: progress bar and list of logged hours.
							if (item.propertiesView && item.propertiesView.timeTrackingContainer) {
								Hi_TimeTracking.list.show(item.propertiesView.timeTrackingContainer, true);
							}
							
							hi.items.manager.set({
								"id": postData.id,
								"time_spent": time_spent
							});
						});
					} else {
						var node;
						if (Hi_TimeTracking.addForm.node) {
							node = dojo.query('a.spent', Hi_TimeTracking.addForm.node.parentNode)[0];
						}
						if (!node) {
							node = dojo.query('a.spent',dojo.byId('propertiesViewNode_'+item.widgetId))[0];//item.query('a.spent', true);
						}
						var time_spent = Hi_TimeTracking.list.getTotalSpentTime();
						node.innerHTML = formatTrackTime(time_spent);
						
						hi.items.manager.set({
							"id": postData.id,
							"time_spent": time_spent
						});
						
						var logId = apiResp && apiResp.getData() ? apiResp.getData().log_id : null;
						var idUpdated = false;
						
						if (logId && postData._logId && self.data) {
							for (var i = 0, len = self.data.length; i < len; i++) {
								if (self.data[i]._logId == postData._logId) {
									self.data[i].log_id = logId;
									idUpdated = true;
								}
							}
						}
						
						if (idUpdated && self.drawn) {
							self.redrawList();
						}
					}
					
					self._hideThrobber();
					self._completeItemIfDone();
				});
			}
		},
		
		/**
		 * Returns total time spent on task
		 */
		getTotalSpentTime: function () {
			var time_spent = 0;
			for(var i=0, ii=this.data.length; i<ii; i++) {
				time_spent+= parseInt(this.data[i].time_spent);
			}
			return time_spent;
		},
		
		/**
		 * Reload data from server
		 */
		reloadData: function (callback) {
			var id = hi.items.manager.openedId();
			if (!id) return;
			
			var item = hi.items.manager.get(id);
			if (!item || !item.time_track) return;
			
			var self = this;
			var getData = {
				'id': id
			};
			
			this._showThrobber();
			Project.ajax('timelist', getData, function (dataResponse) {
				self._hideThrobber();
				if (!dataResponse.isStatusOk()) {
					return false;
				}
				
				var data = dataResponse.getData().list;
				
			    var dataArr = [], i=0;
				
				//Convert object into array
				while(i in data) {
					data[i].time_spent = parseFloat(data[i].time_spent);
					dataArr.push(data[i]);
					i++;
				}
				
				self.data = dataArr;
				self.combineData();
				
				if (self.draw_when_ready) {
					self.redrawList();
				}
				
				if (typeof callback == 'function') callback();
			});
		},
		
		fetchData: function(taskId, callback) {
			Project.ajax('timelist', {id: taskId}, function (dataResponse) {
				if (!dataResponse.isStatusOk()) {
					return false;
				} else {
					callback(dataResponse.getData().list);
				}
			});
		},
		
		/**
		 * Remove item and data
		 * @param {Object} li
		 */
		remove: function (li) {
			var data = this.data[li.dataIndex];
			return this.removeItem(data.user_id, data.date, true);
		},
		
		combineData: function () {
			var list = {};
			var dataArr = [], i, ii;
			
			for(i = 0, ii=this.data.length; i < ii; i++) {
				var id = this.data[i].date + '-' + this.data[i].user_id;
				if (parseInt(this.data[i].time_spent)) {
					if (id in list) {
						list[id].time_spent = Math.min(parseInt(list[id].time_spent) + parseInt(this.data[i].time_spent), 32767);
					} else {
						list[id] = this.data[i];
					}
				}
			}
			
			for(i in list) {
				dataArr.push(list[i]);
			}
			
			dataArr.sort(function (a,b) {
				return (a.date > b.date ? 1 : (a.date == b.date ? 0 : -1));
			});
			
			this.data = dataArr;
			return this.data;
		},
		
		save: function (li) {
			if (li) {
				var id = li.dataIndex;
				var inputs = Hi_TimeTracking.getInputs(li);
				var time_spent = parseTimeString(inputs.minutes.value);
				if (time_spent === null) return;
				
				//Make sure date format is exaclty the same as for other dates
				var date = String(Hi_TimeTracking.list.getDate()).replace(/\-/g, '/');
					date = str2time(date, 'm/d/y');
					date = time2str(date, 'y-m-d');
				
				if (!date) return;
				
				var data = dojo.mixin({}, this.data[id], {
					date: date,
					user_id: inputs.user.value,
					time_spent: time_spent
				});
				this.editItem(this.data[id].user_id, this.data[id].date, data);
			}
			
			this.editing = false;
			this.redrawList();
		},
		
		reset: function (li) {
			this.editing = null;
			if (li) {
				li.className = '';
				this.drawListItem(li);
			}
		},
		
		getDate: function () {
			var id = hi.items.manager.openedId();
			
			var node = dojo.byId('time_track_hour_target');
			if (!node) return '';
			var value = node.value;
			var m = value.match(/(\d{4}).(\d{2}).(\d{2})/);
			
			//IE reports incorrect date format?
			if (m) {
				value = m[2] + '/' + m[3] + '/' + m[1];
			}
			
			return value;
		},
		
		_onDateChange: function () {
			var date = str2time(Hi_TimeTracking.list.getDate(), 'm/d/y');
			if (!date) return;
	
			var dateFormatted = time2LocalizedStr(date);
			dojo.byId('time_track_hour_callee').innerHTML = dateFormatted;
		},
		
		showEditForm: function (li) {
			if (this.editing) return;
			var data = this.data[li.dataIndex];
			var task = hi.items.manager.get(this.data[li.dataIndex].item_id);
			if(!task || task.permission < Hi_Permissions.COMPLETE_ASSIGN){
				return;
			}
			
			Project.hideProjectDialog(); // #2115
			this.editing = li;
			
			var self = this;
	
			var date = str2time(data.date, 'y-m-d');
			var dateFormatted = time2LocalizedStr(date);
			
			var dateInp = time2str(date, 'm/d/y');
			
			li.onclick = function () {};
			
			li.className = 'editing';
			li.setAttribute('log-id', data.log_id);
			li.innerHTML = '<a id="delete" class="icon-trash" style="padding-right:4px;"></a> \
							<input type="text" class="invisible" constraints="{datePattern:\'MM/dd/yyyy\'}" dojoType="dijit.form.DateTextBox" id="time_track_hour_target" name="date" onChange="Hi_TimeTracking.list._onDateChange();" onClick="Hi_Calendar.calendar({}, \'time_track_hour_target\', false, false)" invalidMessage="" value="' + dateInp + '" /> \
							<span class="date" id="time_track_hour_callee" onclick="Hi_Calendar.calendar(event, \'time_track_hour_target\', false, false); dojo.byId(\'time_track_hour_target\').onchange=Hi_TimeTracking.list._onDateChange;dojo.connect(dojo.byId(\'widget_time_track_hour_target_dropdown\'), \'click\', function (event) { dojo.stopEvent(event); }); dojo.connect(dijit.byId(\'time_track_hour_target_popup_mddb\').domNode, \'click\', function(){dojo.connect(dojo.query(\'[dijitpopupparent=time_track_hour_target_popup_mddb]\')[0], \'click\', function(e){dojo.stopEvent(e);});})">' + dateFormatted + '</span> \
							<a class="person"></a> \
							<input type="hidden" name="user" value="' + data.user_id + '" /> \
							<input type="text" class="txt time_est" name="minutes" value="' + parseTimeString(data.time_spent, 'text') + '" /> \
							<button type="button" dojoType="dijit.form.Button" class="save-time btn-success btn-flat btn-alt">' + hi.i18n.getLocalization('properties.save_button') + '</button> \
							<button type="button" dojoType="dijit.form.Button" class="cancel-time btn-flat">' + hi.i18n.getLocalization('properties.cancel_button') + '</button>';
			
			dojo.parser.instantiate(dojo.query('button[dojoType]', li));
			
			var data_friend = Project.getFriend(data.user_id);
			var myRecord = Project.user_id == data.user_id;
			if (!data_friend) data_friend = dojo.mixin({}, Hi_PersonSelect.emptyFriend, {'name': myRecord ? Project.user_name : data.user_name, 'id': myRecord ? Project.user_id : data.user_id});
			if (data_friend && data_friend.id) {
				data_friend.image = 'url(' + dojo.global.getAvatar(16, data_friend.id) + ')';
			}
			
			var node_person = dojo.query('a.person', li)[0];
			var node_cancel = dojo.query('span.cancel-time', li)[0];
			var node_save = dojo.query('span.save-time', li)[0];
			var node_delete = dojo.query('a#delete', li)[0];
			var inputs = Hi_TimeTracking.getInputs(li);
			
			inputs.minutes.onfocus = function($e) {
				if (this.value == hi.i18n.getLocalization('properties.time_tracking_hours')) this.value = '';
			};
			inputs.minutes.onblur = function($e) {
				if (this.value == '') this.value = hi.i18n.getLocalization('properties.time_tracking_hours');
			};
			inputs.minutes.onkeydown = function ($e) {
				$e = $e || window.event || {};
				var key = ('which' in $e && $e.which ? $e.which : $e.keyCode || false); 
				switch(key) {
					case 13:
						dojo.stopEvent($e);
						self.save(this.parentNode);
						break;
					case 27:
						dojo.stopEvent($e);
						self.reset(this.parentNode);
						break;
				}
			};
			
			//IE fix for text selection
			dojo.connect(inputs.minutes, "onselectstart", function (evt) {
				if (evt.stopPropagation) evt.stopPropagation();
				evt.cancelBuble = true;
			});
			
			dojo.connect(node_delete, 'click', function (event) {
				self.remove(this.parentNode);
				dojo.stopEvent(event);
				dojo.addClass(this.parentNode, 'hidden');
			});
			
			dojo.connect(node_person, 'click', function (event) {
				Hi_PersonSelect.show(this, nextElement(this, 'INPUT'));
				dojo.stopEvent(event);
			});
			
			dojo.connect(node_save, 'click', function (event) {
				self.save(this.parentNode);
				dojo.stopEvent(event);
			});
			
			dojo.connect(node_cancel, 'click', function (event) {
				self.reset(this.parentNode);
				if (typeof(Hi_PersonSelect) == 'object' && typeof(Hi_PersonSelect.hide) == 'function') {
					Hi_PersonSelect.hide();
				}
				dojo.stopEvent(event);
			});
			
			Hi_PersonSelect.fillPersonEntry(node_person, data_friend);
		},
		
		drawListItem: function (li) {
			var data = this.data[li.dataIndex];
			var self = this;
			
			var date = str2time(data.date, 'y-m-d');
			var dateFormatted = time2LocalizedStr(date);
			
			dojo.empty(li);
			li.className = '';
			li.setAttribute('log-id', data.log_id);
			li.innerHTML = '<span class="date">' + dateFormatted + '</span>' + 
							'<a class="person"></a>' + 
							'<span class="time">' + formatTrackTime(parseInt(data.time_spent)) + '</span>';
			
			var thisItem = data.item_id ? hi.items.manager.get(data.item_id) : null;
			if (
				(thisItem && thisItem.permission >= Hi_Permissions.MODIFY) ||		//MODIFY permission or higher
				(Project.user_id == data.user_id && thisItem && thisItem.permission === Hi_Permissions.COMPLETE_ASSIGN) ||
				(Project.user_id == data.user_id && !thisItem) ||
				// Project.business_level >= Project.BUSINESS_ADMINISTRATOR_LEVEL ||
				(thisItem && thisItem.user_id == Project.user_id)
			) {
				li.onclick = function () {
					self.showEditForm(this);
				};
			} else {
				dojo.addClass(li, 'readonly');
			}
			
			var data_friend = Project.getFriend(data.user_id);
			var myRecord = Project.user_id == data.user_id;
			if (!data_friend) data_friend = dojo.mixin({}, Hi_PersonSelect.emptyFriend, {'name': myRecord ? Project.user_name : data.user_name, 'id': myRecord ? Project.user_id : data.user_id, 'image': 'url(' + dojo.global.getAvatar(16) + ')'});
			
			var node_person = dojo.query('a.person', li)[0];
			
			if (data_friend && data_friend.id) {
				data_friend.image = 'url(' + dojo.global.getAvatar(16, data_friend.id) + ')';
			}
			Hi_PersonSelect.fillPersonEntry(node_person, data_friend);
			
			this.drawn = true;
		},
		
		redrawList: function () {
			if (!this.node_list) {
				this.node_list = dojo.query('#element_properties_' + Project.opened + ' div.time-track-list')[0];
			}
			
			this.draw_when_ready = false;
			this.editing = null;
			
			dojo.removeClass(this.node_list, 'loading');
			
			var ul = dojo.query('ul', this.node_list)[0];
			var i;
			dojo.empty(ul);
			
			for(i = 0,ii = this.data.length; i < ii; i++) {
				var li = document.createElement('LI');
				
				ul.appendChild(li);
				li.dataIndex = i;
				
				this.drawListItem(li, this.data[i]);
			}
			
			if (this.data.length == 0) {
				dojo.addClass(this.node_list, 'empty-list');
			} else {
				dojo.removeClass(this.node_list, 'empty-list');
			}
			
			this.redrawProgress();
			// Bug #5466
			if (query(this.node_list).closest('.dijitTooltipDialogPopup').length) {		// in list tooltip
				dijit.popup.reposition();
			}
			this.drawn = true;
		},
		
		redrawProgress: function () {
			var item = hi.items.manager.get(hi.items.manager.openedId());
			
			var progress = ~~ (Math.min(1, this.getTotalSpentTime() / item.time_est) * 100) + '%';
			
			var bar = dojo.query('div.progress-bar div', this.node_progress)[0];
				bar.style.width = progress;
				
			var node = dojo.query('a.spent', previousElement(this.node_progress))[0];
			var time_spent = Hi_TimeTracking.list.getTotalSpentTime();
			node.innerHTML = formatTrackTime(time_spent);
			item.time_spent = time_spent;

			var percentage = dojo.query('span.progress_percentage', this.node_progress)[0];
			percentage.innerHTML = '<span>'+progress+'</span>';
		},
		
		show: function (node, redrawWithoutReload) {
			if (!this.getTotalSpentTime()) return;
			if (this.visible) return;
			this.visible = true;
			
			var item = hi.items.manager.opened();
			node = dojo.query('div.time-track-report',dojo.byId('propertiesViewNode_'+item.widgetId));//item.query('div.time-track-report');
			if (node.length && (node = node[0])) {
				this.node_progress = node;
				this.node_progress.style.display = 'block';
			}
			
			node = dojo.query('div.time-track-list',dojo.byId('propertiesViewNode_'+item.widgetId));//item.query('div.time-track-list');
			
			if (node.length && (node = node[0])) {
				this.node_list = node;
				this.node_list.style.display = 'block';
				this.draw_when_ready = true;
				
				if (redrawWithoutReload) {
					this.redrawList();
				} else {
					dojo.addClass(this.node_list, 'loading');
					this.reloadData();
				}
			}
			
			var node_details = dojo.query('a.details', previousElement(this.node_progress))[0];
			node_details.style.display = 'none';
		},
		
		preInit: function (doNotLoadData, callback) {
			// summary;
			//		Initialize time tracking for item
			
			this.visible = false;
			this.node_progress = null;
			this.node_list = null;
			this.data = [],
			this.editing = false;
			this.drawn = false;
			
			if (!doNotLoadData) this.reloadData(callback);
		},
		
		preClose: function (id) {
			// summary:
			//		Reverse dom changes back to preInit state
			
			var item = hi.items.manager.item(id);
			if (item) {
				var node = dojo.query('div.time-track-list',dojo.byId('propertiesViewNode_'+item.widgetId))[0];//item.query('div.time-track-list', true),
					list = dojo.query('div.time-track-list ul',dojo.byId('propertiesViewNode_'+item.widgetId))[0];//item.query('div.time-track-list ul', true),
					report = dojo.query('div.time-track-report',dojo.byId('propertiesViewNode_'+item.widgetId))[0];//item.query('div.time-track-report', true);
				
				if (node) {
					node.style.display = "none";
				}
				if (list) {
					list.innerHTML = "";
				}
				if (report) {
					report.style.display = "none";
				}
			}
		}
	};

	return Hi_TimeTracking.list;
});