package oauth;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import play.libs.WS;
import play.mvc.Http.Request;
import play.mvc.Scope.Params;
import play.mvc.results.Redirect;
import utils.HttpResponseWrapper;


/**
 * Library to access resources protected by OAuth 2.0. For OAuth 1.0a, see play.libs.OAuth.
 */
public class OAuth2Client {
	private static final String CLIENT_ID_NAME = "client_id";
	private static final String REDIRECT_URI = "redirect_uri";
	private static final String STATE = "state";

	private String authorizationURL;
	private String accessTokenURL;
	private String clientid;
	private String secret;
	private String providerName;
	private String icon;
	private String buttonClass;
	private boolean forceApprovalPrompt = false;

	public OAuth2Client(String authorizationURL, String accessTokenURL, String clientid, String secret, String providerName) {
		this(authorizationURL, accessTokenURL, clientid, secret, providerName, null);
	}
	
	public OAuth2Client(String authorizationURL, String accessTokenURL, String clientid, String secret, String providerName, String icon) {
		this(authorizationURL, accessTokenURL, clientid, secret, providerName, icon, null);
	}
	
	public OAuth2Client(String authorizationURL, String accessTokenURL, String clientid, String secret, String providerName, String icon, String buttonClass) {
		this.accessTokenURL = accessTokenURL;
		this.authorizationURL = authorizationURL;
		this.clientid = clientid;
		this.secret = secret;
		this.providerName = providerName;
		this.icon = icon;
		this.buttonClass = buttonClass;
	}

	public static boolean isCodeResponse() {
		return Params.current().get("code") != null;
	}

	/**
	 * First step of the OAuth2 process: redirects the user to the authorisation
	 * page
	 * 
	 * @param callbackURL
	 */
	public void retrieveVerificationCode(String callbackURL, String state) {
		retrieveVerificationCode(callbackURL, new HashMap<String, String>(), state);
	}

	/**
	 * First step of the oAuth2 process. This redirects the user to the
	 * authorisation page on the oAuth2 provider. This is a helper method that
	 * only takes one parameter name,value pair and then converts them into a
	 * map to be used by {@link #retrieveVerificationCode(String, Map)}
	 * 
	 * @param callbackURL
	 *            The URL to redirect the user to after authorisation
	 * @param parameters
	 *            Any additional parameters that weren't included in the
	 *            constructor. For example you might need to add a
	 *            response_type.
	 */
	public void retrieveVerificationCode(String callbackURL, String parameterName, String parameterValue, String state) {
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(parameterName, parameterValue);
		retrieveVerificationCode(callbackURL, parameters, state);
	}

	/**
	 * First step of the oAuth2 process. This redirects the user to the
	 * authorisation page on the oAuth2 provider.
	 * 
	 * @param callbackURL
	 *            The URL to redirect the user to after authorisation
	 * @param parameters
	 *            Any additional parameters that weren't included in the
	 *            constructor. For example you might need to add a
	 *            response_type.
	 */
	public void retrieveVerificationCode(String callbackURL, Map<String, String> parameters, String state) {
		parameters.put(CLIENT_ID_NAME, clientid);
		parameters.put(REDIRECT_URI, callbackURL);
		parameters.put(STATE, StringUtils.isNotBlank(state) ? state : providerName);
		throw new Redirect(authorizationURL, parameters);
	}

	public void retrieveVerificationCode() {
		retrieveVerificationCode(Request.current().getBase() + Request.current().url, null);
	}

	public OAuth2Response retrieveAccessToken(String callbackURL) {
		String accessCode = Params.current().get("code");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("client_id", clientid);
		params.put("client_secret", secret);
		params.put("redirect_uri", callbackURL);
		params.put("code", accessCode);
		params.put("grant_type", "authorization_code");
		return new OAuth2Response(new HttpResponseWrapper(WS.url(accessTokenURL).params(params).post()));
	}

	public OAuth2Response retrieveAccessToken() {
		return retrieveAccessToken(Request.current().getBase() + Request.current().url);
	}

	public String getProviderName() {
		return providerName;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getButtonClass() {
		return buttonClass;
	}

	public void setButtonClass(String buttonClass) {
		this.buttonClass = buttonClass;
	}

	public boolean isForceApprovalPrompt() {
		return forceApprovalPrompt;
	}

	public void setForceApprovalPrompt(boolean forceApprovalPrompt) {
		this.forceApprovalPrompt = forceApprovalPrompt;
	}
}
