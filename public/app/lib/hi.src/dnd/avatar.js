//dojo.provide("hi.dnd.avatar");
define(function () {
	
	dojo.declare("dojo.dnd.HiDragAvatar", [dojo.dnd.Avatar], {
		type: 'dragged_task',
		oldText: '',
		
		//Original text is text about project, calendar, team, but not about task (priority, children, etc.)
		originalText: '',
		
		constructor: function (manager) {
			dojo.dnd.HiDragAvatar.superclass.constructor(manager);
			
			if (this.node) {
				var ul = dojo.create('ul', {'class': 'HiDragSourceClass', style: 'position: absolute; z-index: 9999;'});
				var li = this.node.getElementsByTagName('LI')[0];
				ul.appendChild(li);
				
				// #2182: remove all web font icons.
				var icons = dojo.query('[data-icon]', ul);
				for (var i = 0; i < icons.length; i++) {
					dojo.setAttr(icons[i], 'data-icon', '');
				}
				
				var txt = dojo.create('div', {'class': 'dropIndicator'});
					prependChild(txt, li);
				
				dojo.body().appendChild(ul);
				
				if (this.node.parentNode) this.node.parentNode.removeChild(this.node);
				this.node = ul;
				this.nodeTxt = txt;
			}
		},
		
		hide: function () {
			if (this.node) {
				this.node.style.display = 'none';
			}
		},

		show: function () {
			if (this.node) {
				this.node.style.display = 'block';
			}
		},
		
		updateText: function (text, notoriginal) {
			if (text == this.oldText) return;
			
			if (text) {
				this.nodeTxt.style.visibility = 'visible';
				// innerText(this.nodeTxt, text);
				this.nodeTxt.innerHTML = text;
			} else {
				this.nodeTxt.style.visibility = 'hidden';
			}
			
			this.oldText = text;
			if (!notoriginal) {
				this.originalText = text;
			}
		},
		
		resetOrigianlText: function () {
			this.updateText(this.originalText);
		}
		
	});
	
	return dojo.dnd.HiDragAvatar;
	
});