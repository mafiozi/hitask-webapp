/*
 * This file is used only in /archive section
 */
define([
	
	"hi/store-archive",
	
	"hi/archive/main",

	"hi/dnd",
	
	"hi/global",
	"hi/i18n",
	"hi/preferences",
	"hi/address",

	"hi/project",

	"hi/connectionqueue",
	"hi/notification",

	"hi/comments",

	"hi/tag",
	"hi/filter",
	"hi/timetracking/timer",
	"hi/archive/archive",

	"hi/upload",
	"hi/widget/form/cleartextbox"
	
], function () {
	
	return {};
	
});
