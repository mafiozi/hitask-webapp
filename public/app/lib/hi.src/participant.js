define(function () {
	var nonConfirmedEmailWarning = 'Please confirm your email in order to see real names!';
	return dojo.global.Hi_Participant = {
		templates: {
			view: '<span><span id="%value_id%" class="participant">%value%</span></span>',
			viewEmailNotConfirmed: '<span title="' + nonConfirmedEmailWarning + '"><span id="%value_id%" class="participant">%value%</span></span>',
			edit: '<span><span id="%value_id%" class="participant">%value% <a title="Click to remove participant %value%" class="remove_participant" href="javascript://" onclick="Hi_Participant.removePropertyParticipant(this.previousSibling)" data-icon=""></a></span></span>',
			editEmailNotConfirmed: '<span title="' + nonConfirmedEmailWarning + '"><span id="%value_id%" class="participant">%value% <a title="Click to remove participant %value%" class="remove_participant" href="javascript://" onclick="Hi_Participant.removePropertyParticipant(this.previousSibling)" data-icon=""></a></span></span>'
		},
		getTemplate: function(name) {
			return !name && !Project.friends ? Hi_Participant.templates.viewEmailNotConfirmed : Hi_Participant.templates.view;
		},
		getTemplateEdit: function(name) {
			return !name && !Project.friends ? Hi_Participant.templates.editEmailNotConfirmed : Hi_Participant.templates.edit;
		},
		trim: function(str) {
			if (dojo.isString(str)) {
				/*Remove multiple spaces*/
				str = str.replace(/\s{2,}/g, ' ');
				str = str.trim();
				str = str.toLowerCase();
				return str;
			} else {
				return '';
			}
		},
		addParticipant: function(t, participants) {
			participants = participants.split(',');
			var i, participant, currParticipants = Hi_Participant.getCurrItemParticipants(null,t);
			var str;
			
			for (i = 0; i < participants.length; i++) {
				participant = Hi_Participant.trim(participants[i]);
				if (!participant) continue;
				if (in_array(participant, currParticipants)) continue;
				var participantEl = Hi_Participant.participantsElement(t);
				
				var name = Project.getFriendName(participant, 2, true);
				str = Hi_Participant.getTemplateEdit(name).replace(/%value_id%/g, participant).replace(/%value%/g, name ? name : ('participant' + participant)) + ' ';
				participantEl.innerHTML = participantEl.innerHTML + str;
			}
			
			return true;
		},
		openedParticipantElement: function(id) {
			var item = id ? hi.items.manager.item(id) : hi.items.manager.opened();
			if (!item) return;
			if (hi.items.manager._items[id] && hi.items.manager._items[id].tooltip) {
				item = hi.items.manager._items[id].tooltip;
			}
			//return item.query('span.participants_edit', true);
			return dojo.query('span.participants_edit',dojo.byId('propertiesViewNode_'+item.widgetId))[0];
		},
		newParticipantElement: function() {
			return this.openedParticipantElement() || dojo.byId('hi_widget_TaskItemNew_0');
		},
		getItemParticipants: function(id) {
			return hi.items.manager.get(id, 'participants') || [];
		},
		getCurrItemParticipants: function(id, t) {
			var participantEl;

			if (t) {
				participantEl = Hi_Participant.participantsElement(t);
			} else {
				var item = hi.items.manager.get(id);
				// #4324
				if (id && (id > 0 || (id < 0 && item && item.parent > 0))) {
					participantEl = Hi_Participant.openedParticipantElement(id);
				} else {
					participantEl = Hi_Participant.newParticipantElement();
				}
			}
			var participantEls = dojo.query('.participant', participantEl);
			var i;
			var participants = [];
			for (i = 0; i < participantEls.length; i++) {
				participants.push(unhtmlspecialchars(participantEls[i].id));
			}
			return participants;
		},
		participantsElement: function(t) {
			return getParentByClass(t, 'participant_cont', 'participants');
		},
		fillPropertyParticipants: function(id) {
			id = id || hi.items.manager.openedId();
			if (!id) return;
			var participants = Hi_Participant.getItemParticipants(id),
				participantEl = Hi_Participant.openedParticipantElement(id);
			
			if (participantEl) {
				var str, html = "";
				for (var i = 0, j = participants.length; i < j; i++) {
					var name = Project.getFriendName(participants[i], 2, true);
					str = Hi_Participant.getTemplateEdit(name).replace(/%value_id%/g, participants[i]).replace(/%value%/g, name ? name : ('participant' + participants[i])) + ' ';
					html += str;
				}
				participantEl.innerHTML = html;
			}
		},

		removePropertyParticipant: function(t) {
			var containerNode = dojo.query(t).closest('.participants_edit')[0];
			this._container = containerNode;

			removeNode(t.parentNode.parentNode);
		},

		addParticipantInput: function(t, e) {
			var input = getParentByClass(t, 'participant_cont', 'new_participant_select');
			var value = getValue(input);
			if (value === '') {
				if (e) {
					e.cancelBubble = false;
				}
			}
			var res = Hi_Participant.addParticipant(t, value);
			if (res) {
				setValue(input);
			}
		}
		
	};

});