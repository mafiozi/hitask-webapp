//dojo.provide("hi.api");
define(function () {
	
	dojo.global.ajaxRequestGet = function(action, params, callback, errorCallback) {
		try {
			
			var hostName = document.getElementById('widgetHost').value,
				url = '/api/' + action;
			
			params.json = true;
			
			dojo.xhrGet({
				url: url,
				handleAs: "json",
				content: params,
				load: function(data){
					callback(data);
				},
				error: function(err, ioArgs){
			        errorCallback(err);
				}
			});
		
		} catch (el) {
			console.log(el);
		}	
	};
	
	dojo.global.ajaxRequestPost = function(url, params, callback, errorCallback) {
		params.json = true;
		
		try {
			dojo.xhrPost({
				url:url,
				handleAs:"json",
				content: params,
				load: function(data){
					callback(data);
				},
				error: function(err, ioArgs){
			        errorCallback(err);
				}
			});
		}
		catch (el) {
			console.log(el);
		}
	};
	
	dojo.global.getTodayDate = function() {
		var date;
		var myDate = new Date();
		var date1 = strPadLeft(myDate.getDate(), 2, '0');
		var date2 = strPadLeft(myDate.getMonth() + 1, 2, '0');
		var date3 = myDate.getFullYear();
		date = date3 + '-' + date2+ '-' + date1;
		return date;
	};
	
	return {
		'ajaxRequestGet': ajaxRequestGet,
		'ajaxRequestPost': ajaxRequestPost,
		'getTodayDate': getTodayDate
	};
	
});