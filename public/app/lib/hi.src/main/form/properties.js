//dojo.provide("hi.main.form.properties");
define(["dojo/date/locale"], function (djDateLocale) {
	
	dojo.global.disableNew = function() {
		var item = hi.items.manager.item(-1);
		if (item) {
			var x = item.query('input, textarea, select');
			var i, imax = x.length;
			for (i = 0; i < imax; i++) {
				x[i].disabled = false;
			}
		}
	};
	
	dojo.global.enableNew = function() {
		var item = hi.items.manager.item(-1);
		if (item) {
			var x = item.query('input, textarea, select');
			var i, imax = x.length;
			for (i = 0; i < imax; i++) {
				x[i].disabled = false;
			}
		}
	};
	
	/**
	 * Auto resize textarea when return key is pressed
	 */
	dojo.global.handleTextAreaKey  = function() {
		var fontsizepx = parseInt(dojo.style(this, 'fontSize'));						//For WebKit
		var lineheightpx = dojo.style(this, 'lineHeight') || (fontsizepx + 2);
		var padding = dojo.style(this, 'paddingTop') + dojo.style(this, 'paddingBottom');
		
		var minheight = this.minheight || 0 - padding * 2;
		var maxheight = this.maxheight;
		var h = this.scrollHeight;
		
		var maxlines = ~~Math.max(maxheight ? maxheight / lineheightpx : 10, 2);
		var minlines = ~~Math.max(minheight ? minheight / lineheightpx : 2, 2);
		var lines = Math.max(~~(this.scrollHeight / lineheightpx), minlines);
		
		//Prevents jumping (FF only)
		if (lines >= maxlines) {
			this.style.overflow = 'auto';
			lines = maxlines;
		} else {
			this.style.overflow = 'hidden';
		}
		
		var new_h = lineheightpx * lines + padding * 2;
		var old_h = parseInt(this.style.height);
		if (new_h > old_h) {
			this.style.height = new_h + 'px';
		}
	};
	
	dojo.global.handleDDElement  = function(el) {
		if (el) {
			var resize_handle = dojo.query('.textarea-resize', el)[0],
				is_drag = false,
				start_y = 0,
				delta_y = 0;
			
			var textarea = prevElement(resize_handle);
			var max_h = dojo.style(textarea, 'maxHeight') || 395;
			var min_h = dojo.style(textarea, 'minHeight') || 64;
			var def_h = Math.min(max_h, Hi_Preferences.get('description_input_height', min_h));
			
			if (dojo.hasClass(textarea, 'dijitTextArea')) {
				var w = dijit.byNode(textarea);
				if (w) w.set('widgetHeight', def_h);
			}
			textarea.style.height = def_h + 'px';
			textarea.maxheight = max_h;
			textarea.minheight = min_h;
			
			dojo.connect(resize_handle, 'onmousedown', function (evt) {
				try {
					is_drag = true;
					start_y = evt.clientY;
					dojo.stopEvent(evt);
				} catch (e) {}
			}, true);
			
			function onmousemove (evt) {
				if (!is_drag) return;
				dojo.stopEvent(evt);
				
				delta_y = evt.clientY - start_y;
				start_y = evt.clientY;
				
				if (delta_y) {
					try {
						textarea.style.height = Math.min(395, (parseInt(textarea.style.height) || def_h) + delta_y) + 'px';
					} catch (e) {}
				}
			}
			
			dojo.connect(document.body, 'onmousemove', onmousemove);
			
			dojo.connect(document.body, 'onmouseup', function (evt) {
				try {
					if (!is_drag) return;
					onmousemove.call(this, evt);
					var h = parseInt(textarea.style.height, 10);
					Hi_Preferences.set('description_input_height', h);
					if(dojo.hasClass(textarea, 'dijitTextArea')){
						var w = dijit.byNode(textarea);
						if (w) w.set('widgetHeight', h);

					}
					Hi_Preferences.send();
					is_drag = false;
				} catch (e) {}
			});
		}
	};
	
	dojo.global.handleDD  = function(id) {
		var el = null;
		if (id) {
			el = dojo.byId('element_properties_' + id);
		} else {
			el = dojo.byId('newTaskBody');
		}
		
		handleDDElement(el);
	};
	
	/**
	 * Parse task title and extract project name, start date, tags
	 */
	dojo.global.parseTaskTitle = function() {
		var project = null,
			tags = [],
			start_date, i , ii, match,
			input = this.titleInputNode,
			title = input.value || input.get('value');
		
		if (!title) return;
		
		//Extract project
		if (title.indexOf('@') != -1) {
			var projects = hi.items.manager.get({"category": 0});
			i = 0;
			ii = projects.length;
			
			for (; i < ii; i++) {
				match = title.match(new RegExp('@' + escapeExpression(projects[i].title) + '\\b', 'i'));
				if (match) {
					title = title.replace(match[0], '');
					project = projects[i].id;
					break;
				}
			}
			
			//Populate form
			if (project) {
				this.set('parent', project);
				this.fillProjectList();
			} else {
				var newProjectTitle = title.match(/@[^\,\.\+\-\*\/\\\s\|\@\(\)\{\}\[\]]+/);
				if (newProjectTitle && newProjectTitle.length == 1) {
					newProjectTitle = newProjectTitle[0];
					title = title.replace(newProjectTitle, '');
					newProjectTitle = newProjectTitle.replace('@', '');
					
					var selfTask = this;
					Project._doProjectAdd(newProjectTitle, '', undefined, undefined, function(newProjectId) {
						// It may happen that editing form already collapsed and we need to set parent manually.
						setTimeout(function() {
							if (!hi.items.manager.editing()) {
								newProjectId = parseInt(newProjectId);
								if (newProjectId > 0) {
									var taskId = hi.items.manager._lastPushedId;
									var task = hi.items.manager.get(taskId);
									if (task && title.indexOf(task.title) != -1) {
										hi.items.manager.setSave(taskId, {parent: newProjectId});
									}
								}
							}
						}, 1000);
					});
				}
			}
		}
		
		//Extract tags
		if (title.indexOf('#') != -1) {
			var all_tags = [].concat(Hi_Tag.tagsArray);
			
			//Sort by longest first, shortest last
			all_tags.sort(function (a,b) { return a.length < b.length; });
			
			for(i = 0, ii = all_tags.length; i < ii; i++) {
				match = title.match(new RegExp('#' + escapeExpression(all_tags[i]) + '\\b', 'i'));
				if (match) {
					title = title.replace(match[0], '');
					tags.push(all_tags[i]);
					break;
				}
			}
			
			//Extract new tags
			title = title.replace(/#([^\s]+)\s?/g, function (item, tag) {
				tags.push(tag);
				return '';
			});
			
			//Populate form
			if (tags.length) {
				var node = dojo.query(".new_tag_input", this.domNode).pop();
				Hi_Tag.addTag(node, tags.join(','));
			}
		}
		
		function escapeList(n) {
			var ret = [];
			for(var i=0,ii=n.length; i<ii; i++) {
				ret.push(escapeExpression(n[i]));
			}
			return ret;
		}
		
		var monthsToLower = function(x) {
			return x && typeof(x) == 'string' ? x.toLowerCase() : x;
		};
		var months = dojo.map(djDateLocale.getNames('months', 'wide'), monthsToLower),
			months_short = dojo.map(djDateLocale.getNames('months', 'abbr'), monthsToLower),
			today = hi.i18n.getLocalization('calendar.today').toLowerCase(),
			tomorrow = hi.i18n.getLocalization('calendar.tomorrow').toLowerCase(),
			next_week = hi.i18n.getLocalization('calendar.next_week').toLowerCase(),
			all = escapeList([].concat(months, months_short)),
			reg_time_start = '(\\d+(:\\d+)?am\\s|\\d+(:\\d+)?pm\\s|\\d+(:\\d+)?\\s)?',
			reg_time_end = '(\\s\\d+(:\\d+)?am|\\s\\d+(:\\d+)?pm|\\s\\d+(:\\d+)?)?';
		
		match = title.match(new RegExp(reg_time_start + '(' + all.join('|') + ')(\\s\\d+)?' + reg_time_end, 'i'));
		if (!match) {
			match = title.match(new RegExp(reg_time_start + '(' + today + '|' + tomorrow + '|' + next_week + ')()' + reg_time_end, 'i'));
		}
		
		if (match) {
			//["August 10 8pm", undefined, "August", "10", " 8pm"]
			
			var time = match[1] || match[7],
				month = match[5].toLowerCase(),
				date = match[6],
				yearr = null,
				full_date = null;
			
			// Handling dates like August 2013.
			if (parseInt(date) > 31) {
				yearr = parseInt(date);
				date = 1;
			}
			
			if (month == today || month == tomorrow || month == next_week) {
				if (month == today) {
					//Today
					full_date = time2str(new Date(), Hi_Calendar.format);
				} else if (month == tomorrow) {
					//Tomorrow
					full_date = time2str(new Date(+ new Date() + 86400000), Hi_Calendar.format);
				} else {
					//Next week (next monday)
					var day = (new Date()).getDay();
					day = day ? 8 - day : 1;
					full_date = time2str(new Date(+ new Date() + day * 86400000), Hi_Calendar.format);
				}
			} else if (month && date) {
				var index = dojo.indexOf(months, month),
					year = yearr || (new Date()).getFullYear();
				
				if (index == -1) index = dojo.indexOf(months_short, month);
				date = parseInt(date, 10);
				
				full_date = time2str(new Date(year, index, date, 12, 0, 0), Hi_Calendar.format);
			} else {
				match = null;
			}
			
			if (match) {
				if (time) {
					time = dojo.trim(time);
					
					//Convert time
					if (time.indexOf('am') != -1) {
						time = time.replace('am', '');
						if (time > 11) {
							time -= 12;
							time = time + '';
						}
					} else if (time.indexOf('pm') != -1) {
						time = time.replace('pm', '');
						if (time > 11) {
							time -= 12;
							time = time + '';
						}
						time = time.replace(/^\d+/, function (v) {
							return parseInt(v, 10) + 12;
						});
					}
					
					if (time.indexOf(':') == -1) time += ':00';
					time = HiTaskCalendar.formatTime(time);
					
					// #3045:
					// "Do the homeworks tomorrow 23:00" creates item with start time 11 am
					// if user preferences is 12 hour format time then 'time' var contains pm/am format and brokes further calculations.
					if (time && (time.indexOf('pm') != -1 || time.indexOf('PM') != -1 || time.indexOf('am') != -1 || time.indexOf('AM') != -1)) {
						time = HiTaskCalendar.from12to24(time);
					}
				}
				
				//Populate form
				var datetime = time2strAPI(str2time(full_date + " " + (time ? time : "00:00"), Hi_Calendar.format + " h:i"));
				
				// #3045: set input fields only if empty.
				if (!this.get("start_date")) {
					this.set("start_date", datetime);
				}
				if (!this.get("end_date")) {
					this.set("end_date", datetime);
				}
				
				if (time && !this.get("start_time") && !this.get("end_time")) {
					this.set("start_time", datetime);
					this.set("end_time", datetime);
				}
				
				// #3045: there is no need to replace detected title with empty string.
				//title = title.replace(match[0], '');
			}
		}
		
		if (input.value != title) {
			title = dojo.trim(title);
			input.value = title;
		}
		
		return title;
	};
	
	return {};
});