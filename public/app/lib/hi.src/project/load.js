//dojo.provide("hi.project.load");
define([
	"hi/project/base"
], function () {
	
	var Project = dojo.global.Project;
		
	dojo.mixin(Project, {
		
		/* Loaded data */
		data: {},
		loaded: false,
		firstLoad: false,
		loadTimeout: false,
		latestLoadFiendCount: false,
		
		/**
		 * Find server timestamp when last chat message was sent/received
		 */
		findLastChatTime: function (chat) {
			if (chat) {
				var t_max = '';
				for(var user in chat) {
					if (chat.hasOwnProperty(user)) {
						for(var i=0,j=chat[user].messages.length; i<j; i++) {
							var t = chat[user].messages[i].messageTime;
							if(t > t_max){
								t_max = t;
							}
						}
					}
				}
				
				return t_max;
			}
			
			return 0;
		},
		
		getTimeRegex: /^([0-9]+:[0-9]+)(:.*)?$/,
	
		get: function (id, prop) {
			if (!(id in Project.data) ||!(prop in Project.data[id])) return false;
			if (Project.data[id]['category'] == 3) {
				if (prop == 'reminder_enabled') {
					return 1;
				} else if (prop == 'reminder_time') {
					return 0;
				} else if (prop == 'reminder_time_type') {
					return 'm';
				}
			}
			if (Project.data[id][prop] != null) {
				var tmp = Project.data[id][prop];
				if (tmp != '' && (prop == 'start_date' || prop == 'end_date')) {
					tmp = time2str(str2time(tmp, 'y-m-d'), Hi_Calendar.format);
				}
				if (tmp != '' && (prop == 'start_time' || prop == 'end_time')) {
					tmp = tmp.replace(Project.getTimeRegex, '$1');
				}
				if (prop == 'completed' && parseInt(Project.data[id]['recurring']) > 0) {
					return 0;
				}
				return tmp || '';
			} else {
				return '';
			}
		},
		
		/**
		 * Returns children data
		 * 
		 * @param {Number} id
		 * @param {Boolean} latest
		 * @return Object with children data
		 * @type {Object}
		 */
		filterChildrenData: function (data, parent) {
			var data = data || {};
			var ret = {};
			
			for(var i in data) {
				if (data[i].parent == parent) {
					ret[i] = data[i];
				}
			}
			
			return ret;
		}
		
	});

	return Project;

});