define(
//AUTO GENERATED FILE. DO NOT MODIFY.
//If you need to add,update,modify messages: Update /languages/[locale]/js.properties and run "ant generate-js-localization" to build this file.
//begin v1.x content
({
  "team_list": {
    "myself": "أنا",
    "add_person": "شخص جديد..."
  },
  "friendfind": {
    "unconfirmed_email": "رجاء قم تأكيد بريدك الإلكتروني",
    "in_business": "هذا الشخص ضمن الفريق بالفعل"
  },
  "color": {
    "red": "أحمر",
    "orange": "برتقالي",
    "green": "أخضر",
    "gray": "رمادي",
    "blue": "أزرق",
    "yellow": "أصفر",
    "purple": "أرجواني",
    "none": "بدون لون"
  },
  "tooltip": {
    "delete_message_task_4": "حذف الملاحظة؟",
    "delete_message_task_5": "حذف الملف؟",
    "uncomplete_confirmation": "استعادة؟",
    "delete_message_task_2": "حذف الحدث؟",
    "delete_message_task_1": "حذف المهمة؟",
    "delete_comment": "حذف التعليق؟",
    "complete_confirmation": "مكتملة؟",
    "delete_completed_items": "حذف المهام المكتملة؟",
    "archive_completed_items": "أرشفة البنود المكتملة؟"
  },
  "project": {
    "cancel": "إلغاء",
    "make_task_attach": "إرفاق الملف لهذا العنصر",
    "reports": "التقارير",
    "drag_to_sort": "اسحب وأدرج للترتيب",
    "move_out_confirmation": "نقل خارج العنصر؟",
    "none": "بدون",
    "color_orange": "برتقالي",
    "click_to_add_task": "اضغط لإضافة مهمة",
    "click_to_open_project": "اضغط على توسيع/طي المشروع",
    "click_to_complete": "اضغط لإكمال/إعادة فتح المهمة",
    "click_to_view": "اضغط للعرض",
    "color_red": "أحمر",
    "item": "البند",
    "edit": "تحرير",
    "color_yellow": "أصفر",
    "project_title": "عنوان المشروع",
    "duplicate": "تكرار...",
    "done": "تم إنجاز %d",
    "private_project": "مشروع خاص",
    "project_description": "أدخل تفاصيل المشروع هنا...",
    "delete_confirmation": "هذا العنصر يمتلكه عضو آخر في الفريق. هل أنت متأكد؟",
    "click_to_edit": "اضغط للتحرير",
    "daily": "يوميا",
    "move_task_confirmation": "جعل هذه المهمة فرعية؟",
    "monthly": "شهريا",
    "add_new_person": "إضافة شخص جديد...",
    "share_project": "مشاركة مع الفريق",
    "yearly": "سنويا",
    "create_new_project": "إنشاء مشروع جديد...",
    "items": "البنود",
    "private": "خاص",
    "save": "حفظ",
    "color_none": "بدون لون",
    "delete": "حذف، أرشفة...",
    "weekly": "أسبوعيا",
    "move_attach_confirmation": "إرفاق الملف لهذه المهمة؟",
    "share_project_only": "مشاركة المشروع فقط",
    "unshare_project": "جعل المهمة خاصة",
    "enter_item_name": "أدخل اسم العنصر",
    "share": "مشاركة...",
    "move_invalid_priority": "لإعادة ترتيب العناصر، الرجاء الفرز بناء على الأولوية",
    "color_blue": "أزرق",
    "delete_project": "حذف المشروع والمهام التابعة له",
    "delete_project_confirmation": "حذف المشروع؟",
    "color_gray": "رمادي",
    "color_purple": "أرجواني",
    "color_green": "أخضر",
    "make_task_nested": "جعل المهمة فرعية من عنصر آخر",
    "share_project_and_tasks": "مشاركة المشروع والمهام التابعة له",
    "move_task_out": "نقلها خارج العنصر",
    "click_specific_day_to_complete": "To complete recurring task, please complete it in Calendar by clicking the checkbox on that task for specific day.",
    "move_to_archive": "نقل إلى الأرشيف",
    "duplicate_project": "تكرار المشروع والمهام",
    "delete_but_keep_tasks": "حذف المشروع مع الحفاظ على المهام التابعة له",
    "create_new": "إضافة بند جديد ",
    "move_project_invalid_priority": "To reorder projects, please switch to sorting by Priority",
    "double_click_to_edit": "Double click to edit"
  },
  "main": {
    "no": "لا",
    "send_back_to": "إرسال مرة أخرى إلى",
    "assigned_to_me": "تم تعيينه لي",
    "deleted_by": "حذفت بواسطة",
    "from": "من",
    "returned_to": "يعود إلى",
    "assigned_to": "معين لـ"
  },
  "trial": {
    "warning1": "قم بالترقية الآن!",
    "warning": "متبقي للمدة التجريبية %d يوم."
  },
  "notification": {
    "project_was_created": "مشروع تم إنشاؤه",
    "notification_enable": "تفعيل إشعارات المتصفح",
    "task_modified": "البند تم تعديله",
    "task_modified_starred": "حالة البند المفضل تغيرت",
    "project_restore": "مشروع تم استعادته",
    "project_was_deleted": "مشروع تم حذفه",
    "project_was_updated": "مشروع تم تحديثه",
    "task_uncompleted": "البند تم استعادته",
    "task_modified_color": "لون البند تم تغييره",
    "team_member_business_deleted": "عضو فريق تم حذفه",
    "task_copy_and_restore": "نسخة من البند تم إنشاؤه",
    "task_modified_date": "تاريخ البند تم تغييره",
    "notification_disable": "تعطيل إشعارات المتصفح",
    "task_deleted": "البند تم حذفه",
    "task_modified_project": "البند تم نقله لمشروع",
    "task_restore": "البند تم استرجاعه",
    "project_copy_restore": "نسخة من المشروع تم إنشاؤه",
    "team_member_deleted": "عضو فريق تم حذفه",
    "task_created": "البند تم إنشاؤه",
    "logout_confirmation": "هناك عمليات لم تكتمل. هل تريد مغادرة hiTask بالفعل؟",
    "team_member_added": "عضو فريق تم إضافته",
    "team_member_invited": "عضو فريق تم دعوته",
    "task_completed": "البند تم اكتماله",
    "task_duplicate": "البند تم تكراره",
    "task_assigned": "البند تم تعيينه",
    "task_archive": "البند تم نقله للأرشيف"
  },
  "no_permissions_to_delete": {
    "not_item_owner_or_administrator": "يجب أن تكون إما صاحب هذا البند أو مدير الفريق لحذف هذا البند!",
    "not_project_owner_or_administrator": "يجب أن تكون إما صاحب المشروع أو مدير الفريق لحذف هذا البند!"
  },
  "no_permissions_to_archive": {
    "not_shared_or_assignee": "يجب أن تكون إما مالك أو معين أو مشارك في هذا البند أو أن تكون مشترك هذا البند من أجل أرشفته!"
  },
  "common": {
    "add": "إضافة",
    "logTimeAction": "log time to this item",
    "behind_proxy": "يبدو أنك تستخدم proxy، قد لا تعمل بعض المميزات عند الاتصال بالإنترنت عن طريق proxy.",
    "start_upload": "رفع ملف",
    "duplicateAction": "duplicate this item",
    "text_formatting_help": "تعليمات تنسيق النص",
    "deleteAction": "delete this item",
    "attachAction": "attach file to this item",
    "not_permitted_warning": "You have no permission to ${0}. You may ask ${1} ${2} to give you permission to ${3}.",
    "addSubAction": "add sub tasks to this item",
    "assignAction": "assign this item",
    "archiveAction": "archive this item",
    "completeAction": "complete this item",
    "modifyAction": "modify this item",
    "makeStarredAction": "make this item starred"
  },
  "permissions": {
    "title_50": "Complete, Assign",
    "title_60": "Modify",
    "title_100": "Everything",
    "title_20": "View, Comment"
  },
  "unload": {
    "warning": "Are you sure want to navigate away from this page?"
  },
  "tag": {
    "add_no_more_tags": "لا أوسمة أخرى للاختيار منها!",
    "add_title": "اضغط لوسم البند بـ %tag"
  },
  "taskfilter": {
    "status_my_created": "عرض مهامي والمهام التي تمت مشاركتها بواسطتي",
    "status_my": "عرض البنود الخاصة والمعينة لي",
    "status_my_unassigned": "عرض مهامي والمهام التي تمت مشاركتها وغير معينة"
  },
  "no_permissions": {
    "no_permissions_to_modify": "Sorry, you have no permissions to Modify this item."
  },
  "calendar": {
    "next_year": "اذهب للعام القادم",
    "change_year": "تغيير السنة",
    "tomorrow": "غدا",
    "next_month": "اذهب للشهر القادم",
    "next_week": "الأسبوع القادم",
    "previos_month": "اذهب للشهر الماضي",
    "yesterday": "بالأمس",
    "this_month": "اذهب للشهر الحالي",
    "previos_year": "اذهب للعام الماضي",
    "today": "اليوم",
    "this_yeay": "اذهب للعام الحالي",
    "this_date": "اذهب لهذا التاريخ",
    "all_day_events": "جميع أحداث اليوم"
  },
  "no_permissions_to_edit": {
    "not_shared_or_assignee": "يجب أن تكون إما مالك أو معين أو مشارك في هذا البند أو أن تكون مشترك في هذا البند من أجل تحريره!",
    "no_permission_to_set_as_private": "هذا المشروع يتبع لعضو آخر في الفريق. فقط المسؤول أو المالك يمكن جعله خاص!"
  },
  "center": {
    "move_to_project": "نقل إلى مشروع",
    "items_not_in_project": "العناصر غير الموجودة في المشروع...",
    "remove_color": "إزالة اللون",
    "assign_to": "التعيين لـ",
    "after_week": "بعد أسبوع",
    "move_out_of_project": "نقل خارج المشروع",
    "completed_items_count": "%s عناصر مكتملة...",
    "loading_items": "تحميل %s عناصر...",
    "due_on": "Due on",
    "completed_items": "العناصر المكتملة: ",
    "starred": "مميز بنجمة",
    "overdue": "مهام متأخرة",
    "loading_item": "تحميل عنصر...",
    "today": "اليوم",
    "mark_starred": "علامة",
    "from": "من",
    "make_start": "ابدأ",
    "change_color": "تغيير اللون",
    "start_on": "تبدأ في",
    "completed_item_count": "%s عناصر مكتملة...",
    "item_not_assigned": "عنصر لم يتم تعيينه لشخص...",
    "drag_items_here": "اسحب وأدرج هنا",
    "tomorrow": "غدا",
    "item_assigned_to": "تعيين العنصر لـ",
    "during_week": "الأسبوع القادم",
    "no_items": "لا توجد عناصر",
    "mark_unstarred": "مميزة بنجمة بدون علامة",
    "unassign": "إزالة التعيين",
    "item_received_from": "العنصر تم تعيينه من قبل",
    "daily": "يوميا",
    "completed_item": "العناصر المكتملة: ",
    "before_today": "قبل اليوم",
    "to": "إلى",
    "item_not_in_project": "عنصر غير موجود في مشروع...",
    "items_not_assigned": "العناصر التي لم يتم تعيينها لشخص..."
  },
  "display": {
    "weekday_0": "Sunday",
    "weekday_2": "Tuesday",
    "weekday_1": "Monday",
    "weekday_4": "Thursday",
    "weekday_3": "Wednesday",
    "weekday_6": "Saturday",
    "weekday_5": "Friday"
  },
  "completed": {
    "archiveLinkTitle": "اضغط لأرشفة العناصر المكتملة",
    "removeLinkTitle": "اضغط لحذف العناصر المكتملة",
    "submenuRemoveLink": "حذف العناصر المكتملة",
    "could_not_archive": "Some of completed tasks may not be archived because you do not have enough rights to archive these items.",
    "archiveLink": "أرشيف",
    "removeLink": "حذف",
    "title": "مكتملة",
    "submenuArchiveLink": "أرشفة العناصر المكتملة",
    "removeLinkText": "عناصر مكتملة"
  },
  "team": {
    "deleted_by": "حذف حسب",
    "are_you_sure": "هل أنت متأكد؟"
  },
  "history": {
    "week": "أسبوع",
    "weeks": "أسابيع",
    "color": "اللون",
    "year": "سنة",
    "ago": "قبل",
    "description": "الوصف",
    "title": "العنوان",
    "years": "سنوات",
    "enabled": "تم تفعيله",
    "second": "ثانية",
    "seconds": "ثواني",
    "hour": "ساعة",
    "disabled": "تم تعطيله",
    "tag": "أوسمة",
    "time_track": "تتبع الوقت",
    "day": "يوم",
    "hours": "ساعات",
    "months": "أشهر",
    "reminder": "منبه",
    "recurring": "تكرار",
    "time_est": "الوقت المقدر",
    "minutes": "دقائق",
    "end_time": "وقت الانتهاء",
    "no_value": "بدون قيمة",
    "minute": "دقيقة",
    "start_time": "وقت البدء",
    "month": "شهر",
    "days": "أيام",
    "comment": "أكتب تعليقك هنا..."
  },
  "priority": {
    "high": "عالية",
    "low": "منخفضة",
    "change_priority": "تغيير الأولوية إلى",
    "medium": "متوسطة",
    "increase": "رفع الأولوية",
    "decrease": "تخفيض الأولوية"
  },
  "sync": {
    "init_targets_retrieving": "جلب التقويمات من حساب Google..."
  },
  "task": {
    "date": "Date",
    "file_delete": "حذف",
    "ago": "قبل",
    "add_comment": "إضافة",
    "project": "المشروع",
    "or_cancel": "أو",
    "none": "بدون",
    "start_time_help": "اضغط لتعيين وقت البدء",
    "type": "النوع",
    "end_date_help": "اضغط لتعيين تاريخ الانتهاء",
    "recurring_none": "لا",
    "heading_sharing": "مشاركة",
    "create_tasks_button": "إنشاء مهام",
    "time_tracking_details": "تفاصيل...",
    "time_tracking_hours": "ساعات",
    "message_max_size": "الوصف يجب ألا يتجاوز %d حرف! تم اقتطاع النص.",
    "starts": "يبدأ",
    "last_modified": "آخر تعديل:",
    "click_to_add_project": "اضغط لإضافة مشروع",
    "remember_other_sharing": "remember for new items",
    "add_item": "مهمة فرعية",
    "many_tasks_format": "تنسيق Comma-separated: \u003cbr /\u003e العنوان، تاريخ البدء، تاريخ الانتهاء، الوصف",
    "add_tags": "إضافة أوسمة:",
    "time_tracking_today": "اليوم",
    "delete_button": "حذف",
    "time_tracking_estimated": "المقدر",
    "time_tracking_spent": "الوقت المستغرق",
    "recurring": "تكرار",
    "time_tracking_enable": "تفعيل",
    "time_tracking_timer": "بدء المؤقت",
    "archive": "الأرشيف",
    "priority": "الأولوية",
    "loading": "تحميل...",
    "sharing": "المشاركة:",
    "recurring_monthly": "شهريا",
    "tags": "أوسمة",
    "task": "المهمة",
    "duplicate_button": "نسخة طبق الأصل",
    "time_tracking_track": "سجل الوقت",
    "birthday": "ذكرى الميلاد",
    "message_empty": "أدخل وصف البند هنا",
    "note": "ملاحظة",
    "shared": "المشاركة مع الفريق",
    "private": "خاص",
    "time_tracking_estimation": "تقدير الوقت",
    "many_tasks_go_back": "حدث شيء غير صحيح؟ \u003ca href\u003d\"javascript://\" onclick\u003d\"NewItemManyTasks.goStepBack()\"\u003eاضغط هنا للرجوع.\u003c/a\u003e",
    "color": "اللون",
    "assign_to": "تعيين لـ:",
    "end_time_help": "اضغط لتعيين وقت الانتهاء",
    "description": "الوصف",
    "ungroup": "ملفي الشخصي",
    "recurring_yearly": "سنويا",
    "recurring_every": "كل",
    "time_tracking_by": "بواسطة",
    "delete_recurring_message": "حذف بنود السلسلة المتكرر؟\u003cbr /\u003eأو حذف هذا الحدث فقط؟",
    "start_date_help": "اضغط لتعيين تاريخ البدء",
    "tag_add": "إضافة",
    "before_start": "قبل البدء",
    "choose_tag": "اختيار من الأوسمة",
    "recurring_end": "نهاية التكرار:",
    "file": "ملف",
    "overdue": "متأخرة",
    "more_tags": "المزيد من الأوسمة",
    "ends": "ينتهي",
    "before_due_start": "before due date (if specified) or start",
    "event": "الحدث",
    "recurring_weekly": "أسبوعيا",
    "time_tracking": "تتبع الوقت",
    "heading_assign": "التعيين",
    "participants": "المشاركين",
    "cancel_button": "إلغاء",
    "calendar": "Calendar",
    "reminder": "التذكير",
    "assign_button": "Assign",
    "recurring_daily": "يوميا",
    "title_max_size": "العنوان يجب ألا يتجاوز %d حرف!",
    "many": "مهام عديدة",
    "priority_medium": "متوسطة",
    "priority_low": "منخفضة",
    "click_to_set_color": "اضغط لتعيين لون البند",
    "save_button": "حفظ التغييرات",
    "many_tasks_description": "ألصق ملف CSV أو أدخل مهمة واحد لكل سطر...",
    "modify": "تعديل",
    "time_tracking_save": "حفظ",
    "tag_click_help": "اضغط هنا لرؤية البنود الموسومة بـ TAG فقط",
    "open_file": "فتح الملف في نافذة جديدة",
    "due": "Due",
    "report": "تقرير",
    "progress": "التقدم",
    "proceed_button": "المواصلة",
    "activity_feed": "Activity",
    "is_all_day": "كل اليوم",
    "priority_high": "عالية",
    "user": "الفريق",
    "show_history": "عرض كل سجل التاريخ"
  },
  "properties": {
    "shared": "تم مشاركتها",
    "private": "خاص",
    "file_upload_over_quota": "لقد استخدمت كل المساحة المتاحة لتخزين الملفات. \u003ca href\u003d\"/subscribe\"\u003eالرجاء الترقية لتحصل على مساحة تخزين أكبر!\u003c/a\u003e",
    "more_actions": "More Actions",
    "assigned_none": "بدون",
    "short_datetime_daily": "Every day %time",
    "or_cancel": "أو",
    "datetime_monthly_interval": "كل %interval شهور في %date %time حتى %recurrence_end_date",
    "drag_file": "أو اسحب وأدرج الملف هنا",
    "datetime_daily": "كل يوم %time حتى %recurrence_end_date",
    "select_file": "اختيار ملف للتحميل",
    "file_upload_error": "هناك خطأ في تحميل الملف، الرجاء المحاولة مرة أخرى.",
    "file_upload_attach": "\u003cb\u003eأدرج الملفات هنا للإرفاق\u003c/b\u003e",
    "file_upload_drop": "\u003cb\u003eأدرج الملفات هنا للتحميل\u003c/b\u003e",
    "short_datetime_daily_interval": "Every %interval days %time",
    "short_datetime_weekly": "Every %day %time",
    "attach_file": "إرفاق ملف",
    "time_tracking_hours": "ساعات",
    "datetime_time": "في %time",
    "error_other": "غير قادرين على تنفيذ العملية الآن. الرجاء المحاولة لاحقا.",
    "datetime_yearly": "سنويا في %date %month %time حتى %recurrence_end_date",
    "assigned_myself": "أنا",
    "attach_another_file": "إرفاق ملف آخر",
    "error_timeout": "هناك خطأ في الاتصال بـ hitask.com. الرجاء التأكد من الاتصال بالإنترنت أو المحاولة مرة أخرى.",
    "cancel_button": "إلغاء",
    "short_datetime_weekly_interval": "Every %interval weeks on %day %time",
    "short_datetime_time": "at %time",
    "datetime_weekly_interval": "كل %interval أسبوع في %day %time حتى %recurrence_end_date",
    "short_datetime_monthly": "Every month on %date %time",
    "datetime_weekly": "كل %day %time حتى %recurrence_end_date",
    "datetime_daily_interval": "كل  %interval يوم %time حتى %recurrence_end_date",
    "file_upload_multiple": "رفع %count ملف",
    "datetime_monthly": "شهريا في %date %time حتى %recurrence_end_date",
    "file_upload_single": "رفع ملفات",
    "save_button": "حفظ",
    "short_datetime_yearly_interval": "Every %interval years on %date %month %time",
    "file_upload_failed": "خطأ تحميل",
    "datetime_yearly_interval": "كل %interval years on %date %month %time حتى %recurrence_end_date",
    "short_datetime_yearly": "Every year on %date %month %time ",
    "short_datetime_monthly_interval": "Every %interval months on %date %time",
    "cancel_upload": "إلغاء",
    "copy_link": "Copy Link"
  },
  "staticTask": {
    "reminder_time": "وقت التذكير",
    "shared": "تمت مشاركته",
    "time_create": "إضافة وقت",
    "recurring": "المتكررة",
    "time_est": "تقدير الوقت",
    "false": "خاطئ",
    "time_spent_subitems": "الوقت المستغرق للبنود الفرعية",
    "completed": "مكتملة",
    "color_value": "قيمة اللون",
    "title": "العنوان",
    "time_spent": "الوقت المستغرق",
    "reminder_time_type": "نوع وقت التذكير",
    "archived": "مؤرشف",
    "starred": "مفضلة",
    "assigneeName": "اسم المعيّن",
    "true": "صحيح",
    "last_comment": "آخر تعليق",
    "recurring_end_date": "تاريخ انتهاء التكرار",
    "reminder_enabled": "تفعيل التذكير",
    "time_est_subitems": "تقدير الوقت للبنود الفرعية",
    "time_track": "تتبع الوقت",
    "category": "الفئة",
    "recurring_interval": "مدة التكرار"
  }
})
//end v1.x content
);