/**
 * Widget correpsonding to the display rendered when an item is expanded.
 * 
 * This object - for a specific item - is accessible via hi.items.manager.opened().propertiesView or
 * hi.items.manager._items[ID][name].propertiesView
 * 
 * It is instantiated using a a dojo.Stateful-wrapped representation of an item using hi.widget.stateful_TaskItem.  We must
 * do this because the items, as rendered on the GUI, are not dojo-widgets.  So in order to make them observable and responsive
 * to changes, they must be individually wrapped in dojo.Stateful.
 */
define([
	"dojo/_base/array", // array.forEach array.map
	"dojo/_base/declare", // declare
	"dojo/cldr/supplemental", // cldrSupplemental.getFirstDayOfWeek
	"dojo/date", // date
	"dojo/date/locale",
	"dojo/dom", // dom.setSelectable
	"dojo/dom-class", // domClass.contains
	"dojo/dom-attr",
	"dojo/_base/event", // event.stop
	"dojo/_base/lang", // lang.getObject, lang.hitch
	"dojo/_base/sniff", // has("ie") has("webkit")
	"dojo/string", // string.substitute
	"dojo/_base/window", // win.doc.createTextNode
	'dojox/image',
	'dojox/image/Lightbox',
	"dijit/_Widget",
	"dijit/_Templated",
	"dijit/_WidgetsInTemplateMixin",
	"dojo/text!./itemproperties.html",
	"dojo/fx",
	"dojo/on",
	"dojo/parser",
	"dojo/aspect",
	"dijit/form/Button",
	"hi/widget/task/assignTo",
	'hi/widget/task/item_DOM',
	"hi/widget/form/hiTextarea",
	'dojo/query'
], function(array, declare, cldrSupplemental, date, local, dom, domClass, domAttr, event,
			lang, has, string, win, dojoxImage, dojoLightbox,
			_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template, fx, on, parser,
			aspect, Button, TaskAssignTo, item_DOM, hiTextarea, domQuery){
	
	//Replace localization string in template instead of using attributes
	template = template.replace(/\{#([^#]+)#\}/g, function (all, str) {
		var localized = hi.i18n.getLocalization(str.trim()) || '';
		if (!localized) console.warn('Missing localization for "' + str + '"');
		return localized;
	});
	
	// module:
	//		hi/widget/TaskItemProperties
	// summary:
	//		Task item properties view
	
	var TaskItemProperties = declare("hi.widget.TaskItemProperties", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
		// summary:
		//		Task item properties view
		
		// Template for the view
		templateString: template,
		
		// Tamplate for tag
		templateStringTag: '<span><a title="' + hi.i18n.getLocalization("task.tag_click_help").replace('TAG', '${value}') + '" href="javascript://" data-dojo-attach-event="onclick: handleTagClick" class="tag">${value}</a></span>',
		
		// Show message even if it is empty
		allowEmptyMessage: false,
		
		// TaskItem widget
		task: null,

		// _actionPermissions: {
		// 	'newAction': [{permission: Hi_Permissions.MODIFY, handler: 'handleAddItem'}],
		// 	'modifyAction': [{permission: Hi_Permissions.MODIFY, handler: 'handleModifyClick'}],
		// 	'assignAction': [{permission: Hi_Permissions.COMPLETE_ASSIGN, handler: 'handleAssignClick'}],
		// 	'attachAction': [{permission: Hi_Permissions.COMPLETE_ASSIGN, handler: 'handleAttachFile'}],
		// 	'deleteAction': [{permission: Hi_Permissions.MODIFY, handler: 'handleDeleteClick'}],
		// 	'timetrackAction': [
		// 		{permission: Hi_Permissions.COMPLETE_ASSIGN, handler: 'handleTimeTrackStart'},
		// 		{permission: Hi_Permissions.COMPLETE_ASSIGN, handler: 'handleTimeTrackFormShow'},
		// 		{permission: Hi_Permissions.COMPLETE_ASSIGN, handler: 'handleTimeTrackListShow'},
		// 		{permission: Hi_Permissions.COMPLETE_ASSIGN, handler: 'handleTimeTrackReportOpen'},
		// 		{permission: Hi_Permissions.COMPLETE_ASSIGN, handler: 'handleTimePopupShow'},
		// 	]
		// },
		
		// Properties which will be watched for changes on task and this object properties will be updated accordinly
		_watchProperties: {
			"guid": true,
			"message": true,
			"is_all_day":true,
			"start_date": true, "start_time": true, "end_date": true, "end_time": true, "due_date": true,
			"recurring": true, 
			"time_track": true, "time_spent": true, "time_est": true,'recurring_interval':true,'recurring_end_date':true,
			"participants": true,
			"tags": true,
			"completed": true,
			"category": true,
			"time_last_update": true,
			"removeOnCollapse": true,
			"assignee": true,
			"permission": true, "permissions": true
		},
		
		// Other widgets are in template, parse them
		widgetsInTemplate: true,
		
		/* ----------------------------- ATTRIBUTES ------------------------------ */
		/* Time tracking */
		time_track: false,
		_setTime_trackAttr: function (time_track) {
			time_track = (time_track === true || time_track === false ? time_track : !!parseInt(time_track, 10));
			this._set("time_track", time_track);
			
			dojo.toggleClass(this.timeTrackContainer, "hidden", !time_track);
			
			if (this.get("expanded")) {
				if (time_track) {
					Hi_TimeTracking.list.preInit();
				} else {
					Hi_TimeTracking.list.preClose();
				}
			}
			
			this.set("time_est", this.time_est);
			this.set("time_spent", this.time_spent);
		},
		
		time_spent: 0,
		_setTime_spentAttr: function (time_spent) {
			time_spent = parseInt(time_spent, 10) || 0;
			this._set("time_spent", time_spent);
			
			if (this.time_track) {
				this.timeTrackingSpentNode.innerHTML = formatTrackTime(time_spent);
			}
			if (time_spent > 0) {
				dojo.removeClass(this.timeTrackingReportLink, "hidden");
			} else {
				dojo.addClass(this.timeTrackingReportLink, "hidden");
			}
		},
		
		time_est: 0,
		_setTime_estAttr: function (time_est) {
			time_est = parseInt(time_est, 10) || 0;
			this._set("time_est", time_est);
			
			if (time_est) {
				dojo.removeClass(this.timeTrackingEstContainer, "hidden");
				dojo.addClass(this.timeTrackingReportLink, "hidden");
				dojo.removeClass(this.timeTrackReportInner, "hidden");
				
				this.timeTrackingEst.innerHTML = formatTrackTime(time_est);
			} else {
				dojo.addClass(this.timeTrackingEstContainer, "hidden");
				//dojo.removeClass(this.timeTrackingReportLink, "hidden");
				dojo.addClass(this.timeTrackReportInner, "hidden");
			}
		},
		
		/* Participants */
		participants: null,
		_setParticipantsAttr: function (participants) {
			this._set("participants", participants);
			var has = dojo.isArray(participants) && participants.length > 0;
			
			dojo.toggleClass(this.participantsContainerNode, "hidden", !has);
			
			// Draw participant list
			var html = dojo.map(participants, function (id) {
				var name = Project.getFriendName(id, 2, true);
				return Hi_Participant.getTemplate(name).replace(/%value_id%/g, id).replace(/%value%/g, name ? name : ('friend' + id));
			});
			
			this.participantsNode.innerHTML = html.join(" ");
		},
		
		/* Tags */
		tags: [],
		_setTagsAttr: function (tags) {
			this._set("tags", tags);
			var has = dojo.isArray(tags) && tags.length > 0;
			
			dojo.toggleClass(this.tagsContainerNode, "hidden", !has);
			
			if (has) {
				this.renderTags();
			}
		},
		
		/* Message */
		message: "",
		_setMessageAttr: function (message) {
			this._set("message", message);
			var hasMessage = this.allowEmptyMessage;
			if (message && message.trim()) {
				message = formatMessage(message);
				hasMessage = true;
			}
			if (hasMessage) {
				this.messageNode.innerHTML = message || "";
				dojo.removeClass(this.messageContainerNode, "hidden");
			} else {
				dojo.addClass(this.messageContainerNode, "hidden");
			}
		},
		
		/* Dates, in format for output, since date and time getters return these */
		end_date: "",
		end_time: "",
		start_date: "",
		start_time: "",
		recurring: 0,
		
		_setEnd_dateAttr: function (value) {
			this._set("end_date", value);
			this.set("time", Math.random());
		},
		_setEnd_timeAttr: function (value) {
			this._set("end_time", value);
			this.set("time", Math.random());
		},
		_setStart_dateAttr: function (value) {
			this._set("start_date", value);
			this.set("time", Math.random());
		},
		_setStart_timeAttr: function (value) {
			this._set("start_time", value);
			this.set("time", Math.random());
		},
		_setRecurringAttr: function (recurring) {
			this._set("recurring", recurring);
			this.set("time", Math.random());
		},

		_setRecurring_intervalAttr:function(d)
		{
			this._set('recurring_interval',d);
			this.set('time',Math.random());
		},
		
		_setRecurring_end_dateAttr:function(d)
		{
			this._set('recurring_end_date',d);
			this.set('time',Math.random());
		},

		assignee: 0,
		_setAssigneeAttr: function(assignee){
			if(this.assignToWidget._instantiated === true){
				this.assignToWidget.set('value', assignee);
			}
		},
		
		/* Time */
		time: "",
		_setTimeAttr: function (time) {
			time = this.getTimeString();
			
			if (time) {
				dojo.removeClass(this.timeContainerNode, "hidden");
				this.timeNode.innerHTML = "";
				this.timeNode.appendChild(win.doc.createTextNode(time + " "));
			} else {
				dojo.addClass(this.timeContainerNode, "hidden");
			}
		},
		
		/* History */
		time_last_update: "",
		_setTime_last_updateAttr: function (time_last_update) {
			if (!time_last_update) return;

			this._set("time_last_update", time_last_update);
			
			//String with time difference: 3 minutes ago
			var time_last_update_dif = Hi_Comments.Date.timeDifString(time_last_update, false),
				t = time_last_update.match(/(\d{4}.\d{2}.\d{2})\s(\d{1,2}:\d{1,2})/);
			
			if (t) {
				t[1] = str2time(t[1], "y-m-d");
				t[1] = time2str(t[1], Hi_Calendar.format);
				time_last_update = t[1] + " " + HiTaskCalendar.formatTime(t[2]);
			} else {
				time_last_update = "";
			}
			
			this.set("time_last_update_dif", time_last_update_dif);
		},
		
		time_last_update_dif: "",
		_setTime_last_update_difAttr: function (time_last_update_dif) {
			this._set("time_last_update_dif", time_last_update_dif);
			this.timeLastUpdateNode.innerHTML = "";
			this.timeLastUpdateNode.appendChild(win.doc.createTextNode(time_last_update_dif + " "));
		},
		
		user_picture_hash: "",
		_setUser_picture_hashAttr: function (user_picture_hash) {
			this._set("user_picture_hash", user_picture_hash);

			if (user_picture_hash) {
				this.userPictureNode.innerHTML = "<img src='" + dojo.global.getAvatar(32) + "' alt='' width='16px' height='16px' />";
			} else {
				this.userPictureNode.innerHTML = "";
			}
		},
		
		
		/* View is expanded */
		expandedAnimate: true,
		expanded: false,
		_setExpandedAttr: function (expanded) {
			var effect;
			this._set("expanded", expanded);
			
			if (expanded) {
				if (this.expandedAnimate) {
					effect = fx.wipeIn({node: this.domNode, duration: CONSTANTS.duration});
					dojo.connect(effect, "onEnd", this, function () {
						this.domNode && (this.domNode.style.height = "auto");
						this.afterExpand();
					});
					effect.play();
				} else {
					this.domNode.style.display = "block";
					this.afterExpand();
				}
				
				this.task.domNode.title = "";
			} else {
				if (this.expandedAnimate) {
					effect = fx.wipeOut({node: this.domNode, duration: CONSTANTS.duration});
					dojo.connect(effect, "onEnd", this, function () {
						this.afterCollapse();
					});
					effect.play();
				} else {
					this.domNode.style.display = "none";
					this.afterCollapse();
				}
				
				this.task.domNode.title = hi.i18n.getLocalization("project.click_to_view");
			}
		},

		/* Set item link */
		guid: null,
		_setGuidAttr: function(guid) {
			this._set('guid', guid);
			domAttr.set(this.itemLink, "href", "/item/" + guid);
		},
		
		/* Category */
		category: null,
		_setCategoryAttr: function (category) {
			this._set("category", category);
			
			if (category == hi.widget.TaskItem.CATEGORY_FILE) {
				var previewUrl = item_DOM.getPreviewImageUrl(this.task.taskId);
				
				if (previewUrl) {
					dojo.addClass(this.openFileWrapperNode, 'hidden');
					dojo.removeClass(this.filePreviewWrapperNode, 'hidden');
					
					this.filePreviewImageNode.src = previewUrl;
				} else {
					dojo.addClass(this.filePreviewWrapperNode, 'hidden');
					dojo.removeClass(this.openFileWrapperNode, 'hidden');
				}
				
				/* #2679 */
				dojo.addClass(this.newItemLinkNode.domNode, "hidden");
				/* #2679 */
				dojo.addClass(this.attachFileLinkNode.domNode, "hidden");
			} else {
				//dojo.addClass(this.openFileNode.domNode, "hidden");
				dojo.addClass(this.openFileWrapperNode,'hidden');
				dojo.removeClass(this.newItemLinkNode.domNode, "hidden");
			}
		},

		permission: 0,
		_setPermissionAttr: function(value){
			var hide = dojo.global.hideElement,
				show = dojo.global.showElement;

			this._set("permission", value);
			this.task.permission = value;

			var item = hi.items.manager.get(this.task.taskId),
				expanded = this.get('expanded');

			if (this.time_track && item && expanded === true) {
				var self = this;
				// #2691 fetch time log from API server only if some time is logged.
				self.resetTimeTrackingBox();
				Hi_TimeTracking.list.preInit(!item.time_spent || item.time_spent <= 0, function() {
					Hi_TimeTracking.list.show(self.timeTrackingContainer, true);
				});
			}
		},

		permissions: [],
		_setPermissionsAttr: function(value){
			// debugger;
			// hi.items.manager.item(3459868).propertiesView.task == hi.items.manager.item(3459868).editingView.task
			this._set('permissions', value);
			this.task.permissions = value;
		},
		
		_duplicateAllowed: function () {
			// summary:
			//		On category and completed attribute change show/hide duplicate link
			
			if (!this.completed && this.category != hi.widget.TaskItem.CATEGORY_FILE) {
				dojo.removeClass(this.duplicateLinkNode.domNode, "hidden");
			} else {
				dojo.addClass(this.duplicateLinkNode.domNode, "hidden");
			}
		},
		
		
		/* ----------------------------- CONSTRUCTOR ------------------------------ */
		// postCreate: function() {
		// 	var t = this,
		// 		actionPermissions = this._actionPermissions,
		// 		k,
		// 		w, 
		// 		action,
		// 		permissions,
		// 		_getPermissionWarning = function(action) {
		// 			var template = hi.i18n.getLocalization('common.not_permitted_warning'),
		// 				task = t.task,
		// 				owner = t.task.user_id,
		// 				ownerName = 'Owner',
		// 				permissions = t.get('permissions'),
		// 				additionalOwners = [],
		// 				permissions = t.get('permissions'),
		// 				action = hi.i18n.getLocalization('common.' + action),
		// 				k,
		// 				ao;

		// 			array.forEach(Project.friends, function(fr) {
		// 				if (fr.id === owner) {
		// 					ownerName = fr.name;
		// 				}
		// 			});

		// 			for (k in permissions) {
		// 				if (permissions[k] === 100 && k !== 'everyone' && parseInt(k, 10) !== owner) {
		// 					ao = Hi_Team.getContactById(k);
		// 					if(!ao) continue;

		// 					additionalOwners.push(ao.name || ao.email);
		// 				}
		// 			}

		// 			if (additionalOwners.length > 3) additionalOwners.length = 3;
		// 			additionalOwners = additionalOwners.join(', ');
		// 			additionalOwners = additionalOwners ? 'or ' + additionalOwners  + ' ': '';

		// 			// console.log(template, task, owner, permissions);
		// 			// console.log(string.substitute(template, [action, ownerName, owner, action]));

		// 			return string.substitute(template, [action, ownerName, additionalOwners, action]);
		// 		};

		// 	this.inherited(arguments);

		// 	for (k in actionPermissions) {
		// 		permissions = actionPermissions[k];
		// 		if(!permissions.length) continue;

		// 		array.forEach(permissions, function(p) {
		// 			t.own(aspect.before(t, p.handler, function(event) {
		// 				if (!event) return;

		// 				w = dijit.getEnclosingWidget(event.target);
		// 				w.action = w.action ? w.action : dojo.attr(w.domNode, 'data-action');
		// 				if (w && w.action) {
		// 					if (t.get('permission') < p.permission) {
		// 						Project.alertElement.showItem(28, {alert: _getPermissionWarning(w.action)});
		// 						return [false];
		// 					}
		// 				}
		// 				return [event];
		// 			}));
		// 		});
		// 	}
		// },
		

		buildRendering: function () {
			this.inherited(arguments);
			
			var key,
				self = this,
				watch = this._watchProperties,
				props = {};
			
			//Hide property form on creation for expand animation
			this.domNode.style.display = "none";
			
			//Apply initial attribute values
			for (key in watch) props[key] = this.task.get(key);
			this.attr(props);
			
			//Watch attribute changes	
			//#3184 - watch is_all_day attribute and adjust start/end_time accordingly
			this.task.watch(dojo.hitch(this, function (name, oldValue, newValue) {
				if (name in watch) {
					this.set(name, this.task.get(name));
					
					//Time is actually saved into date property
					if (name == "start_date" || name == 'is_all_day') this.set("start_time", this.task.get("start_time"));
					if (name == "end_date" || name == 'is_all_day') this.set("end_time", this.task.get("end_time"));
				}
			}));
			this.watchMultiple(["category", "completed"], lang.hitch(this, this._duplicateAllowed));
			this._duplicateAllowed();
			
			//Picture hash
			this.set("user_picture_hash", CONSTANTS.user_picture_hash);
			// console.log(this.buttonGroup);
			this._buildButtonGroup();
			this._buildAssignToWidget();
		},
		
		_buildAssignToWidget: function() {
			if (this.assignToWidget._instantiated !== true) {
				var instances = parser.instantiate([this.assignToWidget], {
					dojoType: 'hi.widget.TaskAssignTo',
					value: this.task.assignee,
					taskId: this.task.id
				});
				this.assignToWidget = instances[0];
				this.assignToWidget._instantiated = true;
			}
		},

		_buildButtonGroup: function(){
			var mode = Hi_Preferences.get('item.action.combobutton.last-used', TaskItemProperties.COMBO_BUTTON_MODE_DEFAULT, 'string'),
				buttonGroup = this.buttonGroup,
				label = '';

			if (!buttonGroup) return;

			dojo.addClass(buttonGroup._buttonNode, 'icon-ellipsis-h');
			this.buttonGroup.set('label', hi.i18n.getLocalization('properties.more_actions'));
		},

		renderTags: function () {
			// summary:
			//		Render tag list
			
			var tags = this.tags,
				template = this.templateStringTag,
				html = "",
				i = 0,
				ii = tags.length;
			
			for (; i<ii; i++) {
				html += template.split("${value}").join(htmlspecialchars(tags[i])) + " ";
			}
			
			this.tagsNode.innerHTML = html;
			this._attachTemplateNodes(this.tagsNode, function(n,p){ return n.getAttribute(p); });
		},
		
		initFileUploadFunctionality: function(withoutFileChildrenCheck) {
			//Add file upload functionality, a little delayed
			//to prevent UI freeze 
			var hasFileChildren = this.hasFileChildren();
			if (FileUpload.enabled && FileUpload.listEnabled && this.category != hi.widget.TaskItem.CATEGORY_FILE && this.task.taskId > 0 && (withoutFileChildrenCheck || hasFileChildren)) {
				/* #2679 */
				dojo.addClass(this.attachFileLinkNode.domNode, 'hidden');
				/*
				 * #3194 - Remove uploader for this task if it exists, since the uploader may be defined for a task on 
				 * a task list, and then later on again for a taskedit tooltip item.  When that happens, the nodeContainer
				 * for the upload widget remains with the task on the task list, so it is not visible on the tooltip
				 *
				if (this.task.taskId in FileUpload._uploaders) 
				{
					if (FileUpload._uploaders[this.task.taskId].nodeContainer) FileUpload.destroy(this.task.taskId);
					//FileUpload.reset(this.task.taskId);
				}	
				*/			
				FileUpload.reset(this.task.taskId);
				var that = this;
				
				setTimeout(dojo.hitch(this, function () {
					//Render file list
					FileList.load(this.task.taskId);
					
					// 1958: We need to disable Attach File for Private items that are owned by another, not current user.
					// When private item was assigned to me I should not be able to attach files to it.
					// Hide fileListContainerNode if there are no items attached yet and above condition is true. 
					
					// #4926: No need in this since in 16.0 permissions decide if user has access to certain functionality.
					
					//if (that.task && !that.task.shared && that.task.assignee == Project.user_id && that.task.user_id != Project.user_id && FileList._getNodeList().children.length < 2) {
					//	dojo.addClass(that.fileListContainerNode, "hidden");
					//} else {
						dojo.removeClass(this.fileListContainerNode, "hidden");
						if (!hasFileChildren) {
							dojo.animateProperty({
								node: this.fileListContainerNode,
								duration: 500,
								properties: {
									opacity: { end: 1, start: 0 }
								}
							}).play();
						}
					//}
				}), 1);
			} else if (!hasFileChildren && FileList._getNodeList() && dojo.query('.file', FileList._getNodeList()).length > 0) {
				// #2932: Need to check if item does not have any more file items really but they are still rendered in UI.
				FileList.load(this.task.taskId);
			}
		},
		
		hasFileChildren: function() {
			return hi.items.manager.get({parent: this.task.taskId, category: hi.widget.TaskItem.CATEGORY_FILE}).length > 0;
		},
		
		/* ----------------------------- API ------------------------------ */
		getTimeString: function() {
			// summary:
			//		Returns date and time as string
			
			var time = "",
				date_str = "";
			
			if (this.start_date) {
				time = this.start_date;

				if (this.recurring) {
					var interval = this.task.recurring_interval != 1 ? '_interval' : '';
					switch(this.recurring) {
						case 4:
							date_str = hi.i18n.getLocalization("properties.datetime_yearly" + interval); break;
						case 3:
							date_str = hi.i18n.getLocalization("properties.datetime_monthly" + interval); break;
						case 2:
							date_str = hi.i18n.getLocalization("properties.datetime_weekly" + interval); break;
						case 1:
							date_str = hi.i18n.getLocalization("properties.datetime_daily" + interval);
							break;
					}
					
					if (this.task.recurring_interval != 1) {
						date_str = date_str.replace(/\%interval/,this.task.recurring_interval);
					}
					
					var start_date = str2time(this.start_date, Hi_Calendar.format) || new Date(this.start_date),
						str_date = start_date.getDate(),
						str_month = HiTaskCalendar.months[start_date.getMonth()],
						str_day = HiTaskCalendar.days[start_date.getDay()],
						str_time = "";

					if (this.start_time) {
						str_time = hi.i18n.getLocalization('properties.datetime_time').replace(/\%time/, time24_to_time12(this.start_time));
					}
					
					date_str = date_str.replace(/\%time/, str_time);
					date_str = date_str.replace(/\%date/, str_date);
					date_str = date_str.replace(/\%day/, str_day);
					date_str = date_str.replace(/\%month/, str_month);
					date_str = date_str.replace(/\%recurrence_end_date/,time2str(str2timeAPI(this.task.recurring_end_date),Hi_Preferences.get('date_format')));
					
					time = date_str;
				} else {
					if (this.start_time) {
						time += " " + time24_to_time12(this.start_time);
					}
					var end_time = "";
					if (this.end_date) {
						if (this.end_date != this.start_date) {
							end_time = this.end_date;
						}
						if (this.end_time) {
							end_time += " " + time24_to_time12(this.end_time);
						}
					}
					if (end_time != "") {
						time += " - " + end_time;
					}
				}
			} else {
				var end_date = hi.items.manager.isTask(this.task.id) ? this.due_date : this.end_date;

				if (end_date) {
					time += this.end_date;
				}
			}
			
			return time;
		},
		
		expand: function (quick) {
			// summary:
			//		Expand view
			// quick:
			//		If quick then animation will not be used, by default it is used
			if (this.expanded) return;

			this._buildButtonGroup();
			if (this.logTimeThrobber) {
				dojo.addClass(this.logTimeThrobber, 'hidden');
			}

			var hasFileChildren = this.hasFileChildren();
			if(!hasFileChildren) {
				var fileUploadWidget = dojo.global.FileUpload.get(this.task.taskId);
				if(fileUploadWidget) {
					dojo.global.FileUpload.destroy(this.task.taskId);
				}
				if(!domClass.contains(this.fileListContainerNode, 'hidden')) {
					domClass.add(this.fileListContainerNode, 'hidden');
				}
				if(domClass.contains(this.attachFileLinkNode.domNode, 'hidden') && 
					this.get('category') != hi.widget.TaskItem.CATEGORY_FILE && 
					this.get('permission') >= Hi_Permissions.COMPLETE_ASSIGN ) {
					domClass.remove(this.attachFileLinkNode.domNode, 'hidden');
				}
			}

			if (quick) this.expandedAnimate = false;
			this.set("expanded", true);
			if (quick) this.expandedAnimate = true;
			
			//Comments
			setTimeout(dojo.hitch(this, function () {
				Hi_Comments.onReady(this.task.taskId);
			}), 1);
			
			//Enable time tracking
			if (this.time_track) {
				if (Hi_Timer.isTimedTask(this.task.taskId)) {
					Hi_Timer.view.updateTaskView();
					if (this.timeTrackingStartLink) {
						dojo.addClass(this.timeTrackingStartLink, "hidden");
					}
					if (this.timeTrackingShowLink) {
						this.timeTrackingShowLink.innerHTML = Hi_Timer.getTime();
						dojo.removeClass(this.timeTrackingShowLink, "hidden");
					}
				} else {
					if (this.timeTrackingStartLink) {
						dojo.removeClass(this.timeTrackingStartLink.domNode, "hidden");
					}
					if (this.timeTrackingShowLink) {
						dojo.addClass(this.timeTrackingShowLink.domNode, "hidden");
					}
				}
			}
			
			// #2718
			// We need to know if it is required to show Attach File button at all for private items assigned to another user.
			// See initFileUploadFunctionality method.

			// Remove this because in version #16.0, it's up to users' permission to decide if allowing attach file or not
			// if (this.task && !this.task.shared && this.task.assignee == Project.user_id && this.task.user_id != Project.user_id && !this.hasFileChildren()) {
			// 	// Load files and if there is no attached files yet then hide Attach button.
			// 	dojo.addClass(this.attachFileLinkNode.domNode, "hidden");
			// }
			this.initFileUploadFunctionality();
		},
		
		afterExpand: function () {
			// summary:
			//		Handle state after view has been expanded
			/* #2965 - indicate that animation has completed */
			item_DOM.transitioning = false;
			//Tooltip position
			Tooltip.updatePositions();
			/* #2535 */
			//dojo.removeClass(this.task.domNode,'dojoDndItem');
			
			var item = hi.items.manager.get(this.task.taskId);
			if (this.time_track && item) {
				var self = this;
				// #2691 fetch time log from API server only if some time is logged.
				Hi_TimeTracking.list.preInit(!item.time_spent || item.time_spent <= 0, function() {
					Hi_TimeTracking.list.show(self.timeTrackingContainer, true);
				});
			}
		},
		
		collapse: function (quick) {
			// summary:
			//		Collapse view
			// quick:
			//		If quick then animation will not be used, by default it is used
			
			if (!this.expanded) return;
			
			if (quick) this.expandedAnimate = false;
			this.set("expanded", false);
			if (quick) this.expandedAnimate = true;
			
			this.resetCommentBox();
			this.resetTimeTrackingBox();
			/* #2535 */
			//dojo.addClass(this.task.domNode,'dojoDndItem');
			//Tooltip position
			Tooltip.updatePositions();
		},
		
		afterCollapse: function () {
			// summary:
			//		Handle state after view has been collapsed
			/* #2965 - indicate that animation has completed */
			item_DOM.transitioning = false;
			if (this.removeOnCollapse) {
				hi.items.manager.remove(this.task.taskId);
			}
		},
		
		watchMultiple: function (props, callback) {
			// summary:
			//		Attach to multiple attribute changes
			// props: Array
			//		Array of attributes
			// callback: Function
			//		Callback function
			
			for (var i=0, ii=props.length; i<ii; i++) {
				this.watch(props[i], callback);
			}
		},
		
		resetCommentBox: function () {
			// summary:
			//		Reset comment box content
			
//			var nodes = dojo.query(".fulllist .dataEl, .last .dataEl", this.commentNode);

			domQuery(".fulllist", this.commentNode).forEach(function(domElem) {
				if(domClass.contains(domElem, 'dataEl')) {
					domElem.innerHTML = "";
				}
			});
			domQuery(".last", this.commentNode).forEach(function(domElem) {
				if(domClass.contains(domElem, 'dataEl')) {
					domElem.innerHTML = "";
				}
			});
//			var nodesLastLst = dojo.query(".last .dataEl", this.commentNode);
//			var nodes = nodesFullLst.concat(nodesLastLst);

//			for (var i=0, ii=nodes.length; i < ii; i++) nodes[i].innerHTML = "";

			domClass.remove(this.commentNode, "data");
//			dojo.removeClass(this.commentNode, "data");
		},
		
		resetTimeTrackingBox: function () {
			// summary:
			//		Reset time tracking box
			
			if (this.time_track) {
				//Clean up time tracking form
				Hi_TimeTracking.list.preClose(this.task.taskId);
				Hi_TimeTracking.addForm.reset();
			}
		},
		
		addNew: function () {
			// summary:
			//		Add new item form to the list
			
			var id = this.task.taskId;
			
			// If this is a new item, can't add another one under it
			if (!id || id < 0) return;
			this.task.list.addNew({"parent": id}, this);
		},
		
		showPreviewLightbox: function(taskId) {
			FileUpload.openFile(taskId, function(url) {
				item_DOM.lightboxDialog.show({
					title: '<a target="_blank" href="' + url + '">Open original file in new window</a>', 
					href: item_DOM.getPreviewImageUrl(taskId, 'max')
				});
			});
		},
		
		/* ----------------------------- EVENT BINDINGS ------------------------------ */
		
		handlePreviewImageClick: function(e) {
			e.cancelBubble = true;
			if (item_DOM.lightboxDialog && Project.checkLimits("TRIAL_TASKS")) {
				this.showPreviewLightbox(this.task.taskId);
			}
		},
		
		handleTagClick: function (event) {
			// summary:
			//		When tag is clicked set filter to that tag
			
			var target = event.target;
			if (target && target.tagName === "A") {
				Hi_Tag.filter(target);
			}
		},
		
		handleOpenFile: function (event) {
			// summary:
			//		When "Open file" link is clicked open file
			
			event.cancelBubble = true;
			
			if (Project.checkLimits("TRIAL_TASKS")) {
				FileUpload.openFile(this.task.taskId);
			}
		},
		
		handleAssignClick: function(event) {
			if (!Hi_Actions.validateAction('assign', this.task)) return false;

			this._buildAssignToWidget();
			var item = hi.items.manager.get(this.task.id);
			if (item) {
				this.assignToWidget.set('value', item.assignee);
			}
			
			dojo.global.showElement(this.assignTo);
		},

		handleAssignToOkClick: function(event){
			var comboBox = this.assignToWidget.assigneeComboBox,
				assignee = comboBox && comboBox.store.query({name: comboBox.get('value')}),
				taskId = this.task.id, postData,
				t = this;

			if(!assignee || !assignee.length || !taskId){ return; }

			postData = {
				assignee: assignee[0].id,
				id: taskId
			};
			
			// hi.items.manager.setSave(taskId, postData);
			item_DOM._save(taskId, {assignee: assignee[0].id});

			var firePostResponse = firePOST({
				post: hi.items.manager.setSave,
				postArgs: [postData]
			});

			firePostResponse.deferred.then(function() {
				if (t.assignTo) {
					dojo.global.hideElement(t.assignTo);
				}
				console.log('fire post success');
			});
		},

		handleButtonGroupClick: function(event) {
			this.buttonGroup.toggleDropDown();
		},
		
		handleArchiveClick: function (event) {
			// summary:
			//		When "Archive" link is clicked archive item
			//
			if (!Hi_Actions.validateAction('archive', this.task)) return false;

			Hi_Preferences.set('item.action.combobutton.last-used', TaskItemProperties.COMBO_BUTTON_MODE_ARCHIVE);

			if (Project.checkLimits("TRIAL_TASKS")) {
				var id = this.task.taskId,
					instance_date = hi.items.date.getNextEventDate(id);

				archiveTask(id, {'instance_date': instance_date, target: this.archiveLinkNode.domNode});
				/* Bug #2048 - kill taskedit tooltip if it is opened */
				if (Tooltip.opened('taskedit')) Tooltip.close('taskedit');
				event.cancelBubble = true;
			}
		},
		
		handleDeleteClick: function (event) {
			// summary:
			//		When "Delete" link is clicked show confirmation message and delete item
			
			if (!Hi_Actions.validateAction('delete', this.task)) return false;

			if (event) dojo.stopEvent(event);
			
			if (Project.checkLimits("TRIAL_TASKS")) {
				var id = this.task.taskId,
					eventdate = this.task.eventdate || null;
					/*
					instance_date = this.task.recurring && this.task.recurring > 0 
						? (dojo.hasClass(this.deleteLinkNode, 'timetable')) 
							? time2strAPI(HiTaskCalendar.currentDate).split('T')[0] 
							: hi.items.date.getNextEventDate(id, HiTaskCalendar.currentDate)
						: false;
					*/
				/* #2852 for version 11.0 */

				// Project.removeTask(id, {'instance_date': eventdate, target: this.deleteLinkNode.domNode});
				Project.removeTask(id, {'instance_date': eventdate, target: this.buttonGroup.domNode});
			}
		},

		handleDuplicateTreeClick: function (event) {
			// summary:
			//		When "Duplicate" link is clicked duplicate this and all children items
			
			Hi_Preferences.set('item.action.combobutton.last-used', TaskItemProperties.COMBO_BUTTON_MODE_DUPLICATE);
			dojo.stopEvent(event);

			if (!Hi_Actions.validateAction('duplicate', this.task)) return false;

			if (Project.checkLimits("TRIAL_TASKS")) {
				Project.duplicateTree(this.task.taskId, {
					"after": dojo.hitch(this, function () {
						this.task.collapse();
					})
				});
			}
		},
		
		handleCloseClick: function (event) {
			this.task.collapse();
		},
		
		/* #2535 */
		mouseMoved:false,
		/* #2535 */
		mouseDown:false,
		/* #2535 */
		handleOnMouseDown:function(event) {
			if (this._preventEvent(event)) {
				return false;
			}
			this.mouseDown=true;
		},
		_preventEvent: function(event) {
			if (event && event.target && domAttr.get(event.target, 'data-hi-stop') == 'event') {
				dojo.stopEvent(event);
				return true;
			} else {
				return false;
			}
		},
		/* #2535 */
		handleOnMouseMove:function(event)
		{
			if (!this.mouseDown) return;
			this.mouseMoved=true;
			//if (event) dojo.stopEvent(event);
		},

		handleItemLink: function(event) {
			var dialog = dijit.byId('taskLinkDialog');
			if (!dialog) return;

			if (!dialog._instantiated) {
				parser.parse(dojo.byId('taskLinkDialog'));
				dialog._instantiated  = true;

				on(dojo.byId('taskLinkDialog'), 'click', function(e) {
					e.stopPropagation();
				});
			}

			var link = dojo.byId('taskExternalLink');
			if (!link) return;

			// domAttr.set(link, "href", "/item/" + this.guid);
			dialog.show();
			link.value = document.location.origin + "/item/" + this.guid;
			if (has('ff')) link.focus();
			link.setSelectionRange(0, link.value.length);
		},
		
		rightClicked:false,
		handleOnContextMenu:function(event) {dojo.stopEvent(event);this.rightClicked=true;},
		
		handleMessageDblClick: function(e) {
			e.preventDefault();
			this.handleModifyClick(e);
		},

		handleModifyClick: function (event) {
			// summary:
			//		When "Modify" is clicked call item openEdit method
			if (!Hi_Actions.validateAction('modify', this.task)) return false;

			if (this._preventEvent(event)) {
				return false;
			}
			var target = dojo.query(event.target).closest("div"),
				name = (target.length ? dojo.getAttr(target[0], "name") : null) || "title";
			
			// #2649
			if (hi.items.manager.hasEditingItemCurrently()) {
				Project.alertElement.showItem(14,{task_alert:'Please finish editing opened item'});
				return false;
			}
			
			if (Project.checkLimits("TRIAL_TASKS")) {
				//this.task.openEdit(name);
				var item;
				item = hi.items.manager._items[this.task.taskId].tooltip || hi.items.manager._items[this.task.taskId][this.task.eventdate] ||  hi.items.manager._items[this.task.taskId].list;
				item_DOM.openEdit(item, name);
			}
		},
		
		handleAddItem: function (event) {
			// summary:
			//		When "Add item" link is clicked show new item form
			if (!Hi_Actions.validateAction('addSub', this.task)) return false;

			dojo.stopEvent(event);
			if (Project.checkLimits("TRIAL_TASKS")) {
				this.addNew();
			}
		},
		
		handleAttachFile: function(event) {
			// summary:
			//		When "Attach file" link is clicked.
			if (!Hi_Actions.validateAction('attach', this.task)) return false;

			if (Project.checkLimits("TRIAL_TASKS")) {
				this.initFileUploadFunctionality(true);
			}
		},
		
		handleCommentSubmit: function (event) {
			// summary:
			//		On comment form submit add comment
			
			dojo.stopEvent(event);
			
			if (Project.checkLimits("TRIAL_TASKS")) {
				Hi_Comments.handleAddComment();
			}
		},
		
		/* Time tracking */
		
		handleTimeTrackFormShow: function (event) {
			// summary:
			//		Show time tracking form
			if(event === false) return;
			if (!Hi_Actions.validateAction('logTime', this.task)) return false;

			Hi_TimeTracking.addForm.show();
			dojo.stopEvent(event);
		},
		handleTimeTrackStart: function (event) {
		
			// summary:
			//		Start time track timer
			if(event === false) return;
			if (!Hi_Actions.validateAction('logTime', this.task)) return false;
			
			if (Project.checkLimits("TRIAL_TASKS")) {
				/*
				 * #3367 - push task to start so timer knows about recurring items
				 */
				Hi_Timer.start(this.task.taskId,this.task);
			}
		},
		
		handleTimePopupShow: function (event) {
			// summary:
			//		Show time track popup
			if(event === false) return;

			Hi_Timer.view.show();
		},
		
		handleTimeTrackListShow: function (event) {
			// summary:
			//		Show time tracking list
			if(event === false) return;
			
			dojo.stopEvent(event);
			Hi_TimeTracking.list.show(this.timeTrackingListContainer);
		},
		
		handleTimeTrackReportOpen: function (event) {
			// summary:
			//		Open time track report
			if(event === false) return;
			
			dojo.stopEvent(event);
			window.open('/report/?code=task_time&format=html&task_id=' + this.task.taskId, '_blank', 'scrollbars=yes,resizable');
		},
		
		handleCloseTooltip: function (event) {
			// summary:
			//		On click close tooltip
			//		This element is visible only if task is opened in tooltip
			
			TaskItemProperties.closeTaskEditTooltip(event);
		},
		
		destroyRendering:function() {
			console.log('destroy rendering');this.inherited(arguments);
		}
		
	});
	
	TaskItemProperties.closeTaskEditTooltip = function(event) {
		dojo.stopEvent(event);
		//Force item in tooltip out of opened state so we can expand other items
		var opened = hi.items.manager.opened();
		if (opened && opened._setOpenedAttr) {
			opened._setOpenedAttr(hi.items.manager.opened(), false);
		}
		Tooltip.close('taskedit');
	};

	TaskItemProperties.set = function(taskItem, attr, value) {
		var id = taskItem.taskId,
			item = id && hi.items.manager.item(id),
			fun = null;

		attr = attr.substr(0,1).toUpperCase() + attr.substr(1);

		if (!item || !item.propertiesView) return;

		func = item.propertiesView['_set' + attr + 'Attr'];
		if (func) {
			func.apply(item.propertiesView, [value]);
		}
	};

	TaskItemProperties.COMBO_BUTTON_MODE_DELETE = 'DELETE';
	TaskItemProperties.COMBO_BUTTON_MODE_ARCHIVE = 'ARCHIVE';
	TaskItemProperties.COMBO_BUTTON_MODE_DUPLICATE = 'DUPLICATE';
	TaskItemProperties.COMBO_BUTTON_MODE_DEFAULT = 'MORE';
	
	return TaskItemProperties;
});
