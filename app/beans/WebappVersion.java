package beans;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import play.Play;

public class WebappVersion implements Serializable {
	private String version;
	
	public WebappVersion() {
		String version = Play.configuration.getProperty("build.version", "");
		String number = Play.configuration.getProperty("build.number", "");
		
		this.version = StringUtils.strip(new StringBuilder().append(version.trim()).append(".").append(number.trim()).toString(), ".");
	}
	
	public String getVersion() {
		return version;
	}
}
