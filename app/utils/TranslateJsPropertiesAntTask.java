package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import play.libs.IO;

import com.google.gson.GsonBuilder;

/**
 * Ant task that should be run during build process.
 * This task translates java-properties like files to Dojo i18n format. 
 * See example usage in build.xml:
 * export PLAY_PATH=/home/user/play-1.2.5/ && ant generate-js-localization
 * @author Vadim Manikhin
 */
public class TranslateJsPropertiesAntTask extends Task {
	private static final String DEFAULT_LOCALE = "en";
	private File languagesDir;
	private File outputDir;
	
	@Override
	public void execute() throws BuildException {
		if (outputDir == null || !outputDir.exists() || !outputDir.isDirectory()) {
			throw new BuildException("output dir does not exist");
		}
		
		if (languagesDir != null && languagesDir.exists() && languagesDir.isDirectory()) {
			try {
				System.out.println("");
				
				for (JsPropertiesWrapper props : getAllJsProperties()) {
					System.out.println("Generating JavaScript localization for locale "+ props.locale + " ...");
					
					// Output structure:
					// outputDir
					//   locale1
					//     hi.js
					//   locale2
					//     hi.js
					// ...
					
					// Locale directory.
					File destDir = new File(outputDir, props.locale);
					
					if (!destDir.exists()) {
						destDir.mkdir();
					}
					
					// Target hi.js file.
					File destFile = new File(destDir, "/hi.js");
					destFile.createNewFile();
					
					FileUtils.write(destFile, createDestFileContent(props), "UTF-8");
				}
			} catch (FileNotFoundException e) {
				throw new BuildException("could not read js.properties", e);
			} catch (IOException e) {
				throw new BuildException("could not create hi.js", e);
			}
		} else {
			throw new BuildException("languages dir does not exist");
		}
	}
	
	private String createDestFileContent(JsPropertiesWrapper props) {
		return new StringBuilder().append("define(\n//AUTO GENERATED FILE. DO NOT MODIFY.\n//If you need to add,update,modify messages: Update /languages/[locale]/js.properties and run \"ant generate-js-localization\" to build this file.\n//begin v1.x content\n(")
			.append(props.toJson()).append(")\n//end v1.x content\n);").toString();
	}
	
	private List<JsPropertiesWrapper> getAllJsProperties() throws FileNotFoundException {
		List<JsPropertiesWrapper> result = new ArrayList<JsPropertiesWrapper>();
		
		for (File subdir : languagesDir.listFiles()) {
			if (subdir.isDirectory()) {
				File jsPropertiesFile = new File(subdir, "js.properties");
				if (jsPropertiesFile.exists()) {
					result.add(new JsPropertiesWrapper(subdir.getName(), IO.readUtf8Properties(new FileInputStream(jsPropertiesFile))));
				}
			}
		}
		
		replaceAbsentMessagesWithDefaultLocale(result);
		return result;
	}
	
	/**
	 * Dojo i18n module does not insert messages from default locale
	 * in case if current locale lacks of some messages.
	 * So we need to manually fill localization file with messages from default locale.
	 * @param wrappers
	 */
	private void replaceAbsentMessagesWithDefaultLocale(List<JsPropertiesWrapper> wrappers) {
		// Find default locale.
		JsPropertiesWrapper defaultWrapper = null;
		for (JsPropertiesWrapper w : wrappers) {
			if (w.locale != null && w.locale.equals(DEFAULT_LOCALE)) {
				defaultWrapper = w;
			}
		}
		
		if (defaultWrapper != null) {
			// Iterate over locales.
			for (JsPropertiesWrapper w : wrappers) {
				if (w.locale != null && !w.locale.equals(DEFAULT_LOCALE)) {
					// Iterate over default locale messages.
					for (Object k : defaultWrapper.properties.keySet()) {
						// If there is no such message in current locale
						// then put message from default locale.
						if (!w.properties.containsKey(k)) {
							w.properties.put(k, defaultWrapper.properties.get(k));
						}
					}
				}
			}
		}
	}
	
	public File getLanguagesDir() {
		return languagesDir;
	}
	
	public void setLanguagesDir(File languagesDir) {
		this.languagesDir = languagesDir;
	}
	
	public File getOutputDir() {
		return outputDir;
	}
	
	public void setOutputDir(File outputDir) {
		this.outputDir = outputDir;
	}
	
	/**
	 * Wrapper class for keeping the locale and respective Java properties.
	 * @author Vadim Manikhin
	 */
	private static class JsPropertiesWrapper {
		private Properties properties;
		private String locale;
		
		public JsPropertiesWrapper(String locale, Properties properties) {
			this.locale = locale;
			this.properties = properties;
		}
		
		public String toJson() {
			Map<String, Map<String, String>> jsonObj = new HashMap<String, Map<String, String>>();
			
			// Properties has following key format:
			// basekey.messagekey = value
			// So parse it accordingly.
			for (Entry<Object, Object> propEntry : properties.entrySet()) {
				String key = (String)propEntry.getKey();
				String value = (String)propEntry.getValue();
				
				String[] split = StringUtils.split(key, '.');
				if (split.length == 2) {
					String baseKey = split[0];
					String subKey = split[1];
					
					if (!jsonObj.containsKey(baseKey)) {
						jsonObj.put(baseKey, new HashMap<String, String>());
					}
					
					jsonObj.get(baseKey).put(subKey, value);
				} else {
					System.err.println("something is wrong with " + key + " from " + locale + " locale");
				}
			}
			
			return new GsonBuilder().setPrettyPrinting().create().toJson(jsonObj);
		}
	}
}
