/**
 * hi.items.date
 * 
 * Contains various utility methods for determining date properties for items, such as next instance date for
 * recurring items
 */
define([
	'hi/items/base'
], function (hi_store) {
	
	// Helper functions for dates
	hi.items.date = {
		
		/* #2928 */
		itemSpansMultipleDays: function(id) {
			// Summary check if specified item spans multiple days
			var data = typeof id === "object" ? id : hi.items.manager.get(id);
			if (data.start_date && data.start_date.length > 0 && data.end_date && data.end_date.length > 0) {
				var checkSD = time2str(new Date(data.start_date),'y-m-d'),
					checkED = time2str(new Date(data.end_date),'y-m-d');
				if (checkSD != checkED) return true;
			}	
			return false;
		},
		
		/* #2791 - search for instance in instance array */
		getInstance: function(id,instance_start_date) {
			var data = typeof id === "object" ? id : hi.items.manager.get(id),
				instance = null;
			
			if (!data.recurring || !data.instances) return null;
			data.instances.forEach(function(item) {
				if (item.start_date == instance_start_date) instance = item;
			});
			return instance;
		},
		
		getEventStatus: function(id, instance_date) {
			// summary:
			//		Returns event status
			// id: Number
			//		Task ID
			// instance_date: String
			//		Instance date
			
			var data = typeof id === "object" ? id : hi.items.manager.get(id);
			
			if (data && instance_date && data.recurring != 0 && data.instances && data.instances.length) {
				for (var i=0, ii=data.instances.length; i<ii; i++) {
					if (data.instances[i].start_date == instance_date) {
						return data.instances[i].status;
					}
				}
			}
			
			return false;
		},
		
		getMultiDayEventStatus: function(id, instance_date) {
			/*
			 * Return event status for instance whose start and end date lies between the specified instance_date
			 */
			var data = typeof id === "object" ? id : hi.items.manager.get(id),
				start_date = new Date(data.start_date),
				end_date = new Date(data.end_date),
				instance_time = str2timeAPI(instance_date),
				timeObj = {start_time:start_date,end_time:end_date};
				
			while(timeObj.end_time < instance_time) this._incrementBy(data.recurring, timeObj);
			var search_instance_date = time2str(timeObj.start_time,'y-m-d');
			var instance = this.getInstance(data, search_instance_date);
			return instance?instance.status:0;
		},
		
		getEventDate: function(id) {
			// summary:
			//		Returns first instance date for recurring event, otherwise null
			// id:Number
			//		Task ID
			
			var data = typeof id === "object" ? id : hi.items.manager.get(id);
			
			if (data.recurring != 0) {
				if (data.instances && data.instances.length) {
					/* Bug #1203 - use instance_date_time instead of instance_date */
					return data.instances[0].start_date_time;
				} else {
					return this.getNextEventDate(data);
				}
			}
			
			return null;
		},
		
		/* #2493 - to generate recurrent instances */
		_incrementBy: function(recurrence_type,timeObj, recurrence_interval) {
			/*
			 * Summary
			 * increment attributes in timeObj based on the recurrence type and recurrence interval
			 * 
			 * Input:
			 * recurrence_type: an integer corresponding to the type of recurrence:
			 * 0 -> none
			 * 1 -> daily
			 * 2 -> weekly
			 * 3 -> monthly
			 * 4 -> yearly
			 * 
			 * recurrence_interval: the frequency of recurrence, e.g., 1 for every day, 2 for every other day, etc.
			 * 
			 * timeObj: object that contains a Date object corresponding to a start date and a Date object corresponding to
			 * an end date.  The end date is specified when the date spans multiple days
			 * 
			 * Output: 
			 * timeObj, with start and end dates incremented according to the recurrence type and interval
			 */
			var dayMS = 24 * 60 * 60 * 1000,
				weekMS = dayMS * 7;
			
			recurrence_interval = recurrence_interval || 1;
			
			switch(recurrence_type) {
				case 1:
					/*
					timeObj.start_time.setTime(timeObj.start_time.getTime()+dayMS);
					if (timeObj.end_time) timeObj.end_time.setTime(timeObj.end_time.getTime()+dayMS);
					*/
					timeObj.start_time.setDate(timeObj.start_time.getDate() + 1 * recurrence_interval);
					if (timeObj.end_time) timeObj.end_time.setDate(timeObj.end_time.getDate() + 1 * recurrence_interval);
					break;
				case 2:
					/*
					timeObj.start_time.setTime(timeObj.start_time.getTime()+weekMS);
					if (timeObj.end_time) timeObj.end_time.setTime(timeObj.end_time.getTime()+weekMS);
					*/
					timeObj.start_time.setDate(timeObj.start_time.getDate() + 7 * recurrence_interval);
					if (timeObj.end_time) timeObj.end_time.setDate(timeObj.end_time.getDate() + 7 * recurrence_interval);
					break;
				case 3:
					timeObj.start_time.setMonth(timeObj.start_time.getMonth() + 1 * recurrence_interval);
					if (timeObj.end_time) timeObj.end_time.setMonth(timeObj.end_time.getMonth() + 1 * recurrence_interval);
					break;
				case 4:
					timeObj.start_time.setFullYear(timeObj.start_time.getFullYear() + 1 * recurrence_interval);
					if (timeObj.end_time) timeObj.end_time.setFullYear(timeObj.end_time.getFullYear() + 1 * recurrence_interval);
					break;
			}
		},

		/* #2493 */
		_checkDateCriteria: function(timeObj,checkDate) {
			/*
			 * Summary
			 * Compare timeObj to check date
			 */
			if (timeObj.end_time) {
				return /*timeObj.start_time < checkDate || */timeObj.end_time < checkDate;
			} else {
				return timeObj.start_time < checkDate;
			}
		},
		
		/*
		 * See _getEventInstanceDate()
		 * This is a modification to that method to account for the recurring_interval property
		 */
		_getNextRecurrenceDate: function(id, instance_date) {
			var now = hi.items.date.truncateTime(new Date()),
				nowStr = HiTaskCalendar.getStrFromDate(now),
				data = typeof id === "object" ? id : hi.items.manager.get(id),
				dayMS = 24*60*60*1000,
				weekMS = dayMS*7,
				start_date,
				end_date,
				start_time,
				end_time,
				next_start_date,
				next_end_date,
				next_start_time,
				next_end_time,
				nextInstance = {},
				instances_length = data.instances?data.instances.length:0,
				recurring_interval,
				original_start,
				recurrence_counter = 0;

			if (data.recurring != 0) {
				recurring_interval = data.recurring_interval;
				start_date = data.start_date;
				original_start = time2str(str2timeAPI(start_date),'y-m-d');
				nextInstance.start_time = hi.items.date.truncateTime(new Date(data.start_date));
				data.end_date?nextInstance.end_time = hi.items.date.truncateTime(new Date(data.end_date)):nextInstance.end_time=null;
				
				while(hi.items.date._checkDateCriteria(nextInstance,now)) {
					hi.items.date._incrementBy(data.recurring,nextInstance);
				}
				
				var deleted = true;
				while(deleted) {
					next_start_date = HiTaskCalendar.getStrFromDate(nextInstance.start_time);
					if (recurrence_counter == 0) {
						recurrence_counter = HiTaskCalendar._getFirstRecurringIntervalInstance(recurrence_counter,data.recurring, original_start,nextInstance.start_time);
					}

					if (recurrence_counter%recurring_interval == 0) {
						deleted = false;
						for (var i = 0;i<instances_length;i++) {
							if (data.instances[i].start_date == next_start_date)
							{
								if (data.instances[i].status == 2) deleted = true;
								if (data.instances[i].status == 1) deleted = true;
							}
						}
					}
					recurrence_counter++;
					if (deleted) {
						hi.items.date._incrementBy(data.recurring,nextInstance);
					}
				}
				
		
			}

			return nextInstance.start_time;
		},
		
		/* Modification to getEventInstanceDate to manually create instances instead of relying on correct ones from server */
		_getEventInstanceDate: function(id,instance_date) {
			// summary:
			//		Returns first instance or instance matching argument date like Date object
			//		with fixed timezone
			// id: Number
			//		Task ID
			// instance_date: String
			//		Instance date
			var now = hi.items.date.truncateTime(new Date()),
				nowStr = HiTaskCalendar.getStrFromDate(now),
				data = typeof id === "object" ? id : hi.items.manager.get(id),
				dayMS = 24*60*60*1000,
				weekMS = dayMS*7,
				start_date,
				end_date,
				start_time,
				end_time,
				next_start_date,
				next_end_date,
				next_start_time,
				next_end_time,
				nextInstance = {},
				instances_length = data.instances?data.instances.length:0,
				recurrence_interval;
			
			if (data.recurring != 0) {
				start_date = data.start_date;
				nextInstance.start_time = hi.items.date.truncateTime(new Date(data.start_date));
				data.end_date?nextInstance.end_time = hi.items.date.truncateTime(new Date(data.end_date)):nextInstance.end_time=null;
				
				while(hi.items.date._checkDateCriteria(nextInstance,now))
				{
					hi.items.date._incrementBy(data.recurring,nextInstance);
				}
				
				var deleted = true;
				while(deleted)
				{
					next_start_date = HiTaskCalendar.getStrFromDate(nextInstance.start_time);
					deleted = false;
					for (var i = 0;i<instances_length;i++)
					{
						if (data.instances[i].start_date == next_start_date)
						{
							if (data.instances[i].status == 2) deleted = true;
						}
					}	
					if (deleted) {
						hi.items.date._incrementBy(data.recurring,nextInstance);
					}
				}
			}
			
			return nextInstance.start_time;
		},
		
		/*
		 * Find all instance dates for specified item that occur inclusively between
		 * the specified  start and end dates and up to and including the item's recurrence end date 
		 */
		generateRecurrenceSeries: function(id, start_date, end_date, all) {
			var data = typeof id === "object" ? id : hi.items.manager.get(id);
			if (!data.recurring) return null;
			var series = [],
				timeObj = {start_time:str2timeAPI(data.start_date)},
				counter = 0,
				recurring_end_date,// = time2str(str2timeAPI(data.recurring_end_date),'y-m-d'),
				instance_date = time2str(timeObj.start_time,'y-m-d'),
				end_date_str;// = end_date?end_date:recurring_end_date;
			
			if (data.recurring_end_date) {
				recurring_end_date = time2str(str2timeAPI(data.recurring_end_date),'y-m-d');
				end_date_str = end_date?end_date:recurring_end_date;
			} else {
				recurring_end_date = null;
				end_date_str = end_date ? end_date : null;
			}
			
			var ret = [];
			
			// #3400
			if (typeof(all) == 'boolean' && all) {
				while (this._checkLimits(instance_date, recurring_end_date) && this._checkLimits(instance_date, end_date_str)) {
					if (!hi.items.manager._getIsRemoved(data, instance_date)) {
						ret.push(instance_date);
					}
					this._incrementBy(data.recurring, timeObj, data.recurring_interval);
					instance_date = time2str(timeObj.start_time, 'y-m-d');
				}
			} else {
				while (this._checkLimits(instance_date,recurring_end_date) && this._checkLimits(instance_date,end_date_str)) {
					if (counter % data.recurring_interval == 0 && instance_date >= start_date && !hi.items.manager._getIsRemoved(data,instance_date)) {
						return [time2str(timeObj.start_time, 'y-m-d')];
					}
					this._incrementBy(data.recurring, timeObj);
					instance_date = time2str(timeObj.start_time, 'y-m-d');
					counter++;
				}
			}
			
			return ret;
		},
		
		getEventInstanceDate: function (id, instance_date) {
			// summary:
			//		Returns first instance or instance matching argument date like Date object
			//		with fixed timezone
			// id: Number
			//		Task ID
			// instance_date: String
			//		Instance date
			
			var data = typeof id === "object" ? id : hi.items.manager.get(id);
			if (data.recurring != 0) {
				var instances = data.instances,
					i = 0,
					ii = instances ? instances.length : 0;

				if (ii) {
					for (; i<ii; i++) {
						if (!instance_date || instances[i].start_date == instance_date) {
							return str2timeAPI(instances[i].start_date_time);
						}
					}
				}
			}

			return null;
		},
		
		getNextEventDate: function (id, source_date) {
			// summary:
			//		Returns next event date for recurring task
			// id:Number
			//		Task ID or task data
			
			var data = typeof id === "object" ? id : hi.items.manager.get(id);
			if (!data) return false;
			
			var start_date = hi.items.date.getEventStartTime(data, '23:59');
			if (!start_date) return false;
			
			var recurring  = parseInt(data.recurring || 0),
				date       = date || hi.items.date.truncateTime(source_date || new Date()),
				instance_date = HiTaskCalendar.nextEvent(start_date, recurring, date);
			
			instance_date = (instance_date && instance_date[1] ? time2str(instance_date[1], 'y-m-d') : null);
				
			return instance_date;
		},
		
		getEventStartTime: function(id, date_start) {
			// summary:
			//		Returns event start time as Date
			// id:Number
			//		Task ID or task data
			// date_start:String
			//		Start time
			
			var data = typeof id === "object" ? id : hi.items.manager.get(id);
			if (!data) return false;
			
			var start_date = data.start_date,
				all_day = data.is_all_day;
				date_start = date_start || "00:00";

			var pieces = date_start.split(':');
			
			var hours = pieces.length ? pieces[0] : 0;
			var minutes = pieces.length ? pieces[1] : 0;

			hours = parseInt(hours, 10);
			minutes = parseInt(minutes, 10);

			if (start_date) {
				start_date = str2timeAPI(start_date);

				if (all_day) {
					this.dateSetTime(start_date, hours, minutes);
				}

				return start_date;
			} else {
				return false;
			}
		},
		
		getEventEndTime: function(id, date_end) {
			// summary:
			//		Returns event end time as Date
			// id:Number
			//		Task ID
			// date_start:String
			//		Start time
			
			var end_date = hi.items.manager.get(id, 'end_date'),
				all_day = hi.items.manager.get(id, 'is_all_day');
			
			if (end_date) {
				end_date = str2timeAPI(end_date);
				// end_date = all_day ? time2str(end_date, Hi_Calendar.format) + ' ' + date_end : time2str(end_date, Hi_Calendar.format + ' h:i');
				if (all_day) {
					this.dateSetTime(end_date, 23, 59);
				}
				return end_date;
				// return str2time(end_date, Hi_Calendar.format + ' h:i');
			}

			return false;
		},
		
		truncateTime: function (date) {
			// summary:
			//		Remove time from date
			// date:Date
			//		Date object
			
			if (!date) return null;
			
			date.setHours(0);
			date.setMinutes(0);
			date.setSeconds(0);
			date.setMilliseconds(0);
			
			return date;
		},
		
		addHours: function (time, hours) {
			// summary:
			//		Add one hours to the time
			// time:Date
			//		Time
			// hours:Number
			//		Number of hours
			
			time = new Date(time);
			hours = time.getHours() + hours;
			
			if (hours > 23) {
				time.setHours(23);
				time.setMinutes(59);
				time.setSeconds(0);
				time.setMilliseconds(0);
			} else if (hours < 0) {
				time.setHours(0);
				time.setMinutes(0);
				time.setSeconds(0);
				time.setMilliseconds(0);
			} else {
				time.setHours(hours);
			}
			return time;
		},
		
		compareDateTime: function (a, b) {
			// summary:
			//		Compare date time and returns 0 if time matches, -1 if a is smaller than b
			//		or 1 if a is larger than b
			// a:Date
			//		First date
			// b:Date
			//		Second date
			
			if (typeof a === "string") a = str2timeAPI(a);
			if (typeof b === "string") b = str2timeAPI(b);
			
			if (!a || !b) {
				return (!a && !b ? 0 : (!a ? -1 : 1));
			}
			
			if (a.getFullYear() === b.getFullYear() && a.getMonth() === b.getMonth() && a.getDate() === b.getDate()) {
				return this.compareTime(a, b);
			} else if (a.getFullYear() < b.getFullYear()) {
				return -1;
			} else if (a.getFullYear() > b.getFullYear()) {
				return 1;
			} else if (a.getMonth() < b.getMonth()) {
				return -1;
			} else if (a.getMonth() > b.getMonth()) {
				return 1;
			} else if (a.getDate() < b.getDate()) {
				return -1;
			} else {
				return 1;
			}
		},
		
		compareTime: function (a, b) {
			// summary:
			//		Compare time and returns 0 if time matches, -1 if a is smaller than b
			//		or 1 if a is larger than b
			// a:Date
			//		First date
			// b:Date
			//		Second date
			
			if (!a || !b) {
				return (!a && !b ? 0 : (!a ? -1 : 1));
			}
			
			if (a.getHours() === b.getHours() && a.getMinutes() === b.getMinutes()) {
				return 0;
			} else if (a.getHours() < b.getHours()) {
				return -1;
			} else if (a.getHours() > b.getHours()) {
				return 1;
			} else if (a.getMinutes() < b.getMinutes()) {
				return -1;
			} else {
				return 1;
			}
		},

		dateSetDate: function(date1, date2) {
			if (!date1 || !date1.getDate) return false;
			if (!date2 || !date2.getDate) return false;

			date1.setDate(date2.getDate());
			date1.setMonth(date2.getMonth());
			date1.setFullYear(date2.getFullYear());
			return date1;
		},

		dateSetTime: function(date, h, m) {
			if (!date || typeof h === 'undefined' || typeof m === 'undefined')  return false;

			date.setMinutes(m);
			date.setHours(h);
			return date;
		},

		dateTimeSetDate: function (datetime, date) {
			// summary:
			//		Set date to datetime string
			//		Returns string in ISO format
			// datetime: String
			//		Date and time in ISO format
			// date: Date
			//		Date object or date in format y-m-d
			
			if (typeof date === "string") {
				date = str2time(date, "y-m-d");
			}
			if (!date) return;

			var t;
			if (!datetime) //datetime = '2012-10-31';
			{
				t = /*str2timeAPI(datetime);*/this.truncateTime(new Date());
			} else t= str2timeAPI(datetime);
			
			t.setFullYear(date.getFullYear());
			t.setMonth(date.getMonth());
			t.setDate(date.getDate());
			
			return time2strAPI(t);
		},
		
		dateTimeSetTime: function (datetime, time) {
			// summary:
			//		Set hours and minutes to datetime string
			//		Returns string in ISO format
			// datetime: String
			//		Date and time in ISO format
			// time: String
			//		Time in h:i format
			
			var h = parseInt(time.substr(0, 2), 10),
				i = parseInt(time.substr(3, 2), 10),
				t = str2timeAPI(datetime);
			
			t = this.truncateTime(t);
			t.setHours(h);
			t.setMinutes(i);
			
			return time2strAPI(t);
		},
		
		/*
		 * If and end date is specified, check if instance_date occurs after it;
		 * otherwise there is no limit to check
		 * 
		 * Input: 
		 * _instance_date - the instance date to check, in the form of 'y-m-d'
		 * _end_date_str - the date to compare instance_date to, in the form of 'y-m-d'
		 */
		_checkLimits: function(_instance_date, _end_date_str) {
			if (!_end_date_str) {
				return true;
			} else {
				return _instance_date <= _end_date_str;
			}
		}
	};
	
	return hi.items.date;
	
});
