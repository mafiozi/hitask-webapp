package tags;

import groovy.lang.Closure;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import play.templates.FastTags;
import play.templates.JavaExtensions;
import play.templates.GroovyTemplate.ExecutableTemplate;

@FastTags.Namespace("app.hi")
public class ComponentUrl extends BaseTag {
	private static final Map<String, String> URL_MAPPINGS = new HashMap<String, String>();
	
	static {
		URL_MAPPINGS.put("com.videinfra.arenaone.report", "/report/");
	}
	
	public static void _componentUrl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String id = getArgAsString("id", args);
		Boolean escapeHtml = getArgAsBoolean("escapeHtml", args, Boolean.TRUE);
		
		String url = id != null && URL_MAPPINGS.containsKey(id) ? URL_MAPPINGS.get(id) : "/";
		
		if (escapeHtml) {
			out.print(JavaExtensions.escapeHtml(url));
		} else {
			out.print(url);
		}
	}
}