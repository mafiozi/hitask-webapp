//dojo.provide("hi.main.header");
define(['dojox/css3/transition',
		'dojo/Deferred',
		'dojo/_base/Deferred'
], function(css3fx, Deferred, BaseDeferred) {
	dojo.global.css3fx = css3fx;

	dojo.global.getProgressReportType = function() {
		
		var types = document.progress_report.progress_report_type;
		var radioValue = '';
		
		if (types && types.nodeType) {
			radioValue = types.value;
		} else if (types) {
			for (var index=0; index < types.length; index++) {
				if (types[index].checked) {
					radioValue = types[index].value;
					break;
				}
			}
		}
		
		return radioValue;
	};
	
	dojo.global.getAssignedReportType = function() {
		
		var types = document.assigned_report.assigned_report_type;
		var radioValue = 'private';
		
		if (types && types.nodeType) {
			radioValue = types.value;
		} else if (types) {
			for (var index=0; index < types.length; index++) {
				if (types[index].checked) {
					radioValue = types[index].value;
					break;
				}
			}
		}
		
		return radioValue;
	};
	
	
	dojo.global.getReportOutputType = function(reportType) {
		var types = document.progress_report.report_progress_output_type;
		if (reportType == 2) {
			types = document.time_report.report_time_output_type;
		}
		if (reportType == 3) {
			types = document.assigned_report.report_assigned_output_type;
		}
		var radioValue = 'html';
		
		if (types && types.nodeType) {
			radioValue = types.value;
		} else if (types) {
			for (var index=0; index < types.length; index++) {
				if (types[index].checked) {
					radioValue = types[index].value;
					break;
				}
			}
		}
		return radioValue;
	};
	
	
	dojo.global.getReportOutputDescriptions = function() {
		var node = document.getElementById('report_output_descriptions');
		return node.checked ? 1 : 0;
	};
	
	dojo.global.getReportSortOption = function() {
		var select = document.getElementById('sort_report_option');
		var index = select.selectedIndex;
		return select.options[index].value;
	};
	
	
	dojo.global.getReportAssignedSortOption = function() {
		var select = document.getElementById('sort_report_option_assigned');
		var index = select.selectedIndex;
		return select.options[index].value;
	};
	
	dojo.global.getReportAssignedDescriptions = function() {
		var node = document.getElementById('report_assigned_descriptions');
		return node.checked ? 1 : 0;
	};
	
	
	/**
	 * Header button
	 */
	dojo.global.markTooltipButton = function(el, tooltip) {
		if (!el._tooltip_binded) {
			el._tooltip_binded = true;
			
			dojo.subscribe('TooltipCloseBefore', function (tt) {
				if (tt == tooltip) {
					dojo.removeClass(el, 'active');
				}
			});
		}
		
		var t = dojo.byId('tooltip_' + tooltip);
		var attr = t.getAttribute('opened');
		if (attr != '1') {
			dojo.addClass(el, 'active');
		}
	};
	
	
	//Switching tasks type in the top of main section
	dojo.global.switchType = function(n) {
		Project.filterPending = null;
		return true;
	};
	
	dojo.global.toggleColumn = function(btn, columnId, oppositeColumnId, direction, fast) {
		var columnPane = dojo.byId(columnId + 'Pane'),
			column = dojo.byId(columnId);
		if(!columnPane) return false;
		
		var oppositeColumn = dojo.byId(oppositeColumnId + 'Pane');
		var columnText = dojo.byId(btn + '_text');
		var columnHideButton = dojo.byId(columnId + 'HideButton');
		direction = direction || 1;
		
		// var main = dojo.byId('main');
		var main = dojo.byId('centerPane'),
			mainWrapper = dojo.query('div', main)[0];
			
		if (!main || !mainWrapper) return false;

		var deferred = new Deferred(), onEnd, onBefore,
			appContainer = dojo.byId('appContainer'),
			centerPane = dojo.byId('centerPane');

		if(!dojo.hasClass(columnPane, 'hidden')) {
			column.style.display = '';
			
			onEnd = function() {
				dojo.addClass(columnPane, 'hidden');
				// dojo.addClass(main, 'no_' + btn);
				if (oppositeColumn) dojo.addClass(oppositeColumn, 'no_' + btn);

				if (columnText) dojo.removeClass(columnText, 'hidden');

				columnHideButton && dojo.addClass(columnHideButton, 'hidden');
				dijit.byId('appContainer').resize();
				Hi_Preferences.set('sidebar_' + columnId, '0');
				deferred.resolve();
			};
			onBefore = function() {
				appContainer._lastOverflow = dojo.style(appContainer, 'overflow');
				appContainer.style.overflow = 'visible';
				columnPane._lastOverflow = dojo.style(columnPane, 'overflow');
				columnPane.style.overflow = 'visible';
				dojo.byId('loading_done').style.overflow = 'visible';
				document.body.style.overflow = 'visible';
			};
			
			if (fast || !supportsCSS3Transitions()) {
				onEnd();
			} else {
				onBefore();
				// dojo.addClass(centerPane, 'inAnim');

				css3fx.slide(column, {
					'in': false,
					duration: CONSTANTS.animationDuration.sidePanelSlideMs,
					direction: direction,
					onAfterEnd: function() {
						appContainer._lastOverflow && (appContainer.style.overflow = appContainer._lastOverflow);
						columnPane._lastOverflow && (columnPane.style.overflow = columnPane._lastOverflow);
						// appContainer.style.overflow = 'hidden';
						// columnPane.style.overflow = 'hidden';
						onEnd();
						// dijit.byId('appContainer').resize();
						// dojo.removeClass(centerPane, 'inAnim');
					}
				}).play();
			}
		} else {
			column.style.display = '';

			if(columnText) dojo.addClass(columnText, 'hidden');

			columnHideButton && dojo.removeClass(columnHideButton, 'hidden');
			dojo.removeClass(columnPane, 'hidden');
			mainWrapper.style.overflow = 'hidden';
			dojo.removeClass(main, 'no_' + btn);
			// dojo.global.reLayout();		//Tag bar height change unexpectedly
			if(oppositeColumn) {
				dojo.removeClass(oppositeColumn, 'no_' + btn);
			}
			// dijit.byId('appContainer').resize();

			onEnd = function() {
				Hi_Preferences.set('sidebar_' + columnId, '1');
				dojo.global.reLayout();		//Tag bar height change unexpectedly
				// deferred.resolve();
				mainWrapper.style.overflow = 'auto';
			};
			onBefore = function() {
				appContainer._lastOverflow = dojo.style(appContainer, 'overflow');
				appContainer.style.overflow = 'visible';
				columnPane._lastOverflow = dojo.style(columnPane, 'overflow');
				columnPane.style.overflow = 'visible';
				dojo.byId('loading_done').style.overflow = 'visible';
				document.body.style.overflow = 'visible';
			};

			dijit.byId('appContainer').resize();
			// setTimeout(function(){
				if (fast || !supportsCSS3Transitions()) {
					onEnd();
				} else {
					onBefore();
					centerPane = dojo.byId('centerPane');
					// appContainer.style.overflow = 'visible';
					// columnPane.style.overflow = 'visible';
					dojo.addClass(centerPane, 'inAnimShrink');

					column.style.display = 'block';
					css3fx.slide(column, {
						'in': true,
						duration: CONSTANTS.animationDuration.sidePanelSlideMs,
						// duration: 2000,
						direction: direction * -1,
						onAfterEnd: function() {
							onEnd();
							appContainer._lastOverflow && (appContainer.style.overflow = appContainer._lastOverflow);
							columnPane._lastOverflow && (columnPane.style.overflow = columnPane._lastOverflow);
							// appContainer.style.overflow = 'hidden';
							// columnPane.style.overflow = 'hidden';
							dojo.removeClass(centerPane, 'inAnimShrink');
						}
					}).play();
				}
			// }, 500);
		}
		HiDragSourceCached.project = false;
		return deferred;
	};
	
	dojo.global.showMenuSubItem = function(el) {
		var ind = 0;
		var content = el;
		
		if (el.tagName != 'LI') {
			el = dojo.dom.getAncestorsByTag(el, 'LI')[0];
		}
		
		if (dojo.hasClass(el, 'disabled')) return;
		
		//Find element index
		while(el.previousSibling) {
			el = el.previousSibling;
			if (el.nodeType == 1) ind++;
		}
		
		var lis = dojo.dom.getChildElements(el.parentNode);
		for(var i = 0, ii = lis.length; i < ii; i++) {
			if (i == ind) {
				lis[i].className = 'selected';
			} else {
				if (lis[i].className != 'disabled')
				lis[i].className = '';
			}
		}
		
		content = dojo.dom.getAncestorsByTag(content, 'UL', true);
		content = nextElement(content);
			
		var els = dojo.dom.getChildElements(content);
		for(i = 0, ii = els.length; i < ii; i++) {
			if (i == ind) {
				els[i].style.display = 'block';
				var v = els[i].getAttribute('onshow');
				if (v) {
					try { eval(v); } catch (e) {}
				}
			} else
				els[i].style.display = 'none';
		}
	};
	
	dojo.global.toggleSortingPopup = function(node) {
		/* Bug #2001 - block if there is a new task opened */
		if (hi.items.manager.openedId() && hi.items.manager.openedId() <0) return false;
		var popup = prevElement(node);
		
		if (dojo.hasClass(popup, 'popup-opened')) {
			dojo.removeClass(popup, 'popup-opened');
			dojo.disconnect(toggleSortingPopup.handle);
		} else {
			dojo.addClass(popup, 'popup-opened');
			
			//Bind to document click to hide popup
			toggleSortingPopup.node = node;
			toggleSortingPopup.handle = dojo.connect(document, 'click', function () {
				Project.hideSortingAndFilterPopups();
			});
		}
	};
	
	dojo.global.switchCategory = function(tab_id) {};
	
	//TODO
	return {};
	
});