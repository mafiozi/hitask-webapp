//dojo.provide("hi.main.logout");
define(["dojo/cookie"],function (cookie) {
	
	dojo.global.logOutConfirmation = function(callback) {
		var target = document.body;
		
		Tooltip.open('logoutconfirmation', "", target);
		
		//Initialize tooltip if needed
		var tooltip = dojo.byId('tooltip_logoutconfirmation');
		if (!Project.logoutTooltip2Initialized) {
			dojo.connect(tooltip, 'click', function (ev) { dojo.stopEvent(ev); });
			Project.logoutTooltip2Initialized = true;
		}
		
		var closeTooltip = function () {
			Tooltip.close('logoutconfirmation');
		};
		
		var btns = tooltip.getElementsByTagName('BUTTON');
		
		//Ok
		btns[0].onclick = function () {
			closeTooltip();
			callback();
		};
		
		//Cancel 
		btns[1].onclick = function (event) {
			dojo.stopEvent(event);
			closeTooltip();
		};
	};
	
	dojo.global.logOut = function(event, force) {
		var event = event || window.event;
		var afterLogout = function() {
			cookie("hiTaskTimer", "loggedOut");
			dojo.global.inLogout = true;
			document.location = '/logout';
		};
		var doLogout = function() {
			dojo.global.showThrobber('Logging out...', 'logOut');
			Project.ajax('logout', {}, function() {
				// Small delay, so browsers will remove session cookie after the Logout API.
				setTimeout(function() {
					afterLogout();
				}, 1000);
			}, function() {
				afterLogout();
			});
			
			if (event) {
				dojo.stopEvent(event);
			}
		};
		if (force) {
			doLogout();
		} else {
			//If there are items in queue show confirmation
			if (ConnectionQueue.list.length) {
				logOutConfirmation(function () {
					ConnectionQueue.list = [];
					logOut();
				});
				
				dojo.stopEvent(event);
				return;
			}
			
			if (typeof Hi_Preferences != 'undefined' && Hi_Preferences.changed) {
				Hi_Timer.stop();
				
				Hi_Preferences.send(function () {
					doLogout();
				});
			} else if (Hi_Timer.isRunning()) {
				Hi_Timer.send(function () {
					doLogout();
				});
			} else {
				doLogout();
			}
		}
	};
	
	//TODO
	return {};
});