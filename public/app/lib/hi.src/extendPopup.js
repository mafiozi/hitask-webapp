/* We must override dijit.popup because it is a singleton (i.e., one instance manages all popups
 * and when a new popup is opened, it tries to close all other opened popups.
 * 
 * Similarly, when a tooltipdialog is designated to be closed, it runs through all popups opened after that
 * one and closes them
 * 
 * The below object just overrides the open and close methods of dijit.popup
 */
define(["dojo/_base/lang",
		'dijit/popup',
		"dojo/_base/array", // array.forEach array.some
		"dojo/aspect",
		"dojo/_base/connect",	// connect._keypress
		"dojo/_base/declare", // declare
		"dojo/dom", // dom.isDescendant
		"dojo/dom-attr", // domAttr.set
		"dojo/dom-construct", // domConstruct.create domConstruct.destroy
		"dojo/dom-geometry", // domGeometry.isBodyLtr
		"dojo/dom-style", // domStyle.set
		"dojo/_base/event", // event.stop
		"dojo/keys",
		"dojo/window",/*,
		"dojo/_base/lang"*/// lang.hitch
		"dojo/on", 
		"dojo/sniff", // has("ie") has("mozilla")
		"dijit/place",
		"dijit/BackgroundIframe",
		"dijit/Viewport",
		"dijit/_TimePicker",
		"dijit/main"	// dijit (defining dijit.popup to match API doc)
],function(lang,popup,array, aspect, connect, declare, dom, domAttr, domConstruct,
			domGeometry, domStyle, event, keys/*, lang*/, _window, on, has, place, BackgroundIframe,
			Viewport, _TimePicker, dijit) {

			var stackHash = {};
			
			var _checkPosition = function(node) {
				if (!node) return false;

				var np = dojo.position(node),
					windowBox = _window.getBox(),
					offset = {x: 0, y: 0};

				if (np.x + np.w > windowBox.w) {
					offset = np.x + np.w - windowBox.w;
				}

				var left = dojo.style(node, 'left');
				left -= offset;
				node.style.left = left + 'px';
			};

			var reposition = function(name) {
				if (name) return this.open(this.stackHash[name]);
				this.open(this._args);
			};

			var nonModalOpen = function(/*__OpenArgs*/ args, isHiTooltip) {
				// summary:
				//		Popup the widget at the specified position
				//
				// example:
				//		opening at the mouse position
				//		|		popup.open({popup: menuWidget, x: evt.pageX, y: evt.pageY});
				//
				// example:
				//		opening the widget as a dropdown
				//		|		popup.open({parent: this, popup: menuWidget, around: this.domNode, onClose: function(){...}});
				//
				//		Note that whatever widget called dijit/popup.open() should also listen to its own _onBlur callback
				//		(fired from _base/focus.js) to know that focus has moved somewhere else and thus the popup should be closed.
				stackHash[args.popup.id] = args;
				this._args = args;

				popup.tooltipDialog = args.popup;
				popup._beginZIndex = 10001;
				var stack = popup._stack,
					widget = args.popup,
					node = widget.domNode,
					orient = args.orient || ["below", "below-alt", "above", "above-alt"],
					ltr = args.parent ? args.parent.isLeftToRight() : domGeometry.isBodyLtr(widget.ownerDocument),
					around = args.around,
					id = (args.around && args.around.id) ? (args.around.id+"_dropdown") : ("popup_"+popup._idGen++),
					windowBox = _window.getBox();

				// If we are opening a new popup that isn't a child of a currently opened popup, then
				// close currently opened popup(s).   This should happen automatically when the old popups
				// gets the _onBlur() event, except that the _onBlur() event isn't reliable on IE, see [22198].
				
				// while(stack.length && (!args.parent || !dom.isDescendant(args.parent.domNode, stack[stack.length-1].widget.domNode))){
				// 	this.close(stack[stack.length-1].widget);
				// }
				
				// Get pointer to popup wrapper, and create wrapper if it doesn't exist
				var wrapper = popup.moveOffScreen(widget);

				// Limit height to space available in viewport either above or below aroundNode (whichever side has more
				// room), adding scrollbar if necessary. Can't add scrollbar to widget because it may be a <table> (ex:
				// dijit/Menu), so add to wrapper, and then move popup's border to wrapper so scroll bar inside border.
				// start of Bug #5443
				var maxHeight, popupSize = domGeometry.position(node);
				if("maxHeight" in args && args.maxHeight != -1){
					maxHeight = args.maxHeight || Infinity;	// map 0 --> infinity for back-compat of _HasDropDown.maxHeight
				}

				if (isHiTooltip === true) widget.domNode.style.maxHeight = windowBox.h + 'px';
				// else{
				// 	var viewport = Viewport.getEffectiveBox(this.ownerDocument),
				// 		aroundPos = around ? domGeometry.position(around, false) : {y: args.y - (args.padding||0), h: (args.padding||0) * 2};
				// 	maxHeight = Math.floor(Math.max(aroundPos.y, viewport.h - (aroundPos.y + aroundPos.h)));
				// }
				if(popupSize.h > maxHeight){
					// Get style of popup's border.  Unfortunately domStyle.get(node, "border") doesn't work on FF or IE,
					// and domStyle.get(node, "borderColor") etc. doesn't work on FF, so need to use fully qualified names.
					var cs = domStyle.getComputedStyle(node),
						borderStyle = cs.borderLeftWidth + " " + cs.borderLeftStyle + " " + cs.borderLeftColor;
					domStyle.set(wrapper, {
						overflowY: "scroll",
						height: maxHeight + "px",
						border: borderStyle	// so scrollbar is inside border
					});
					node._originalStyle = node.style.cssText;
					node.style.border = "none";
				}
				// end of Bug #5443

				domAttr.set(wrapper, {
					id: id,
					style: {
						zIndex: popup._beginZIndex + stack.length
					},
					"class": "dijitPopup " + (widget.baseClass || widget["class"] || "").split(" ")[0] +"Popup",
					dijitPopupParent: args.parent ? args.parent.id : ""
				});
		
				if(has("ie") || has("mozilla")){
					if(!widget.bgIframe){
						// setting widget.bgIframe triggers cleanup in _Widget.destroy()
						widget.bgIframe = new BackgroundIframe(wrapper);
					}
				}
		
				// position the wrapper node and make it visible
				var best = around ?
					place.around(wrapper, around, orient, ltr, widget.orient ? lang.hitch(widget, "orient") : null) :
					place.at(wrapper, args, orient == 'R' ? ['TR','BR','TL','BL'] : ['TL','BL','TR','BR'], args.padding);
				/* Adjust position of tooltip if it is to be fixed */
				if (args.fixed){
					dojo.addClass(wrapper,'tooltipFixed');
					var aroundPos = dojo.position(around,false);
					var top = aroundPos.y+aroundPos.h+3;
					dojo.style(wrapper,'top',top+'px');
					
				}
				wrapper.style.display = "";
				wrapper.style.visibility = "visible";
				_checkPosition(wrapper);
				widget.domNode.style.visibility = "visible";	// counteract effects from _HasDropDown
		
				var handlers = [];
		
				// provide default escape and tab key handling
				// (this will work for any widget, not just menu)
				handlers.push(on(wrapper, connect._keypress, lang.hitch(this, function(evt){
					if(evt.charOrCode == keys.ESCAPE && args.onCancel){
						event.stop(evt);
						args.onCancel();
					}else if(evt.charOrCode === keys.TAB){
						event.stop(evt);
						var topPopup = popup.getTopPopup();
						if(topPopup && topPopup.onCancel){
							topPopup.onCancel();
						}
					}
				})));
		
				// watch for cancel/execute events on the popup and notify the caller
				// (for a menu, "execute" means clicking an item)
				if(widget.onCancel && args.onCancel){
					handlers.push(widget.on("cancel", args.onCancel));
				}
		
				handlers.push(widget.on(widget.onExecute ? "execute" : "change", lang.hitch(this, function(){
					var topPopup = popup.getTopPopup();
					if(topPopup && topPopup.onExecute){
						topPopup.onExecute();
					}
				})));
		
				stack.push({
					widget: widget,
					parent: args.parent,
					onExecute: args.onExecute,
					onCancel: args.onCancel,
					onClose: args.onClose,
					handlers: handlers
				});
		
				if(widget.onOpen){
					// TODO: in 2.0 standardize onShow() (used by StackContainer) and onOpen() (used here)
					widget.onOpen(best);
				}
		
				return best;
			};
		
			var nonModalClose = function(/*Widget?*/ popupWidget) {
				// summary:
				//		Close specified popup and any popups that it parented.
				//		If no popup is specified, closes all popups.
				if (popupWidget.onClose) popupWidget.onClose();
				if (popupWidget.domNode) popup.hide(popupWidget);
				// return;
				var stack = this._stack;

				// Basically work backwards from the top of the stack closing popups
				// until we hit the specified popup, but IIRC there was some issue where closing
				// a popup would cause others to close too.  Thus if we are trying to close B in [A,B,C]
				// closing C might close B indirectly and then the while() condition will run where stack==[A]...
				// so the while condition is constructed defensively.
				while(
					(popupWidget &&
					array.some(stack, function(elem){return elem.widget == popupWidget;})) ||
					(!popupWidget && stack.length)
				){
					var top = stack.pop(),
						widget = top.widget,
						onClose = top.onClose;

					if(widget.onClose){
						// TODO: in 2.0 standardize onHide() (used by StackContainer) and onClose() (used here)
						widget.onClose();
					}

					var h;
					while(h = top.handlers.pop()){ h.remove(); }

					// Hide the widget and it's wrapper unless it has already been destroyed in above onClose() etc.
					if(widget && widget.domNode){
						// this.hide(widget);
					}

					if(onClose){
						onClose();
					}
				}
			};
			//console.log("EXTEND POPUP",[lang,lang.extend,lang.mixin]);
			//for (var i in lang) {console.log('i',i);}
			_TimePicker.extend({'onOpen': function() {
					this.inherited(arguments);
					// Since _ListBase::_setSelectedAttr() calls scrollIntoView(), shouldn't call it until list is visible.
					// this.set("selected", this._selectedDiv);
					this._setSelectedAttr(this._selectedDiv, true);
				}
			});

			return lang.mixin(popup, {'open': nonModalOpen, 'close': nonModalClose, 'stackHash': stackHash, 'reposition': reposition});
});