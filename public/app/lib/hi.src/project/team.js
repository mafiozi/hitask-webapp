//dojo.provide("hi.project.team");
define([
	"hi/project/base"
], function () {
	
	var Project = dojo.global.Project;
			
	dojo.mixin(Project, {
		
		typeClass: {
			0: 'project',
			1: 'tasks',
			2: 'event', 3: 'event', 5: 'file',
			4: 'note'
		},
		typeNames: {
			0: 'project',
			1: 'task',
			2: 'event', 3: 'event', 5: 'file',
			4: 'note'
		},
		pingWait: false,
		pingTimeout: 60,
		
		icon: function(c) {
			return '/img/icon_' + Project.typeClass[c] + '.gif';
		},
		pingContacts: function() {
			if (Project.pingWait > 0) {
				Project.pingWait = parseInt(Project.pingWait);
				if (!isFinite(Project.pingWait)) {
					Project.pingWait = false;
				} else {
					Project.pingWait--;
				}
				return;
			}
			/**
			 * After 50 empty requests slow down ping 5 times
			 */
			if (Team.slowDown > 50 && Project.pingWait == 0) {
				Project.pingWait = 5;
			}
			/* Bug #1823 */
			return Team.contactsLoad();
			
		},
		pingMessages: function() {
			/* Bug #1823 */
			return Team.messagesLoad();
		},
		pingBusiness: function() {
			/* Bug #1823 */
			return Team.businessLoad();
		},
		getMyself: function() {
			var name = hi.i18n.getLocalization('team_list.myself');
			name = name + ' (' + Project.user_name + ')';
			name = name.substr(0, 1).toUpperCase() + name.substr(1);
			return name;
		},
		getFriend: function(id) {
			var fr = Project.friends;
			var s = null;
			if (fr) {
				dojo.forEach(Project.friends, function(el) {if (el.id == id) s = el;});
			}
			return s;
		},
		getFriendName: function(id, myself, ucfirst) {
			if (typeof myself == 'undefined') myself = 1;
			if (typeof ucfirst == 'undefined') ucfirst = false;
			if (myself > 0 && id == Project.user_id) {
				var name = Project.user_name;
				if (myself == 2) {
					//name = name + ' (' + Project.getFriendName(id, 0) + ')';
					name = Project.getMyself();
				} else if (myself == 3) {
					name = Project.user_name;
				}
				if (ucfirst) {
					name = name.substr(0, 1).toUpperCase() + name.substr(1);
				}
				return name;
			}
			var fr = Project.getFriend(id);
			if (fr) {
				return fr.name;
			} else if (Project.shadowFriends && id in Project.shadowFriends) {
				return Project.shadowFriends[id].fullName;
			}
			return null;
		},
		
		//Returns true if user is business administrator
		isBusinessAdministrator: function () {
			return (Project.business_level && Project.business_level >= Project.BUSINESS_ADMINISTRATOR_LEVEL ? true : false);
		},
		
		canModifyDeleteX: function(xId, msg, category) {
			var user_id = hi.items.manager.get(xId, "user_id");
			var shared = hi.items.manager.get(xId, "shared");
			// var notAllowed = user_id != Project.user_id;
			var notAllowed = hi.items.manager.get(xId, 'permission') !== Hi_Permissions.EVERYTHING;
			
			// Team Owner must be able to delete any projects too.
			//if ((shared && category == hi.widget.TaskItem.CATEGORY_PROJECT) || (category != hi.widget.TaskItem.CATEGORY_PROJECT)) {
			notAllowed = notAllowed && !Project.isBusinessAdministrator();
			//}
			
			if (notAllowed) {
				Project.alertElement.showItem(14,{task_alert:msg});
				return false;
			}
			
			return true;
		},
		
		canDeleteItem: function(itemId) {
			return Project.canModifyDeleteX(
				itemId, 
				hi.i18n.getLocalization('no_permissions_to_delete.not_item_owner_or_administrator') || 'Sorry, you do not have permissions to delete this item.'
			);
		},
		
		canModifyX: function(xId, msg) {
			var item = hi.items.manager.get(xId);
			var owner = hi.items.manager.get(xId, "user_id");
			var assignee = hi.items.manager.get(xId, "assignee");
			var shared = hi.items.manager.get(xId, "shared");
			var permission = item && item.permission;
			var participants = dojo.isArray(hi.items.manager.get(xId, "participants")) ? hi.items.manager.get(xId, "participants") : [];
			// Should be assignee or owner or participant.
			var notAllowed = owner != Project.user_id && Project.user_id != assignee && dojo.indexOf(participants, Project.user_id) == -1;
			var colleague = null;
			
			// Check if shared allowed.
			if (notAllowed && Project.businessId && permission >= Hi_Permissions.MODIFY) {
				colleague = Project.getFriend(owner);
				var sharedAllowed = colleague && colleague.businessId == Project.businessId && colleague.subscription == 'BIS';
				notAllowed = notAllowed && !sharedAllowed;
			}
			
			if (notAllowed) {
				if (msg) {
					Project.alertElement.showItem(14,{task_alert:msg});
				}
				return false;
			}
			
			return true;
		},

		canUnShareX: function(xId, msg) {
			var owner = hi.items.manager.get(xId, "user_id");

			if (owner == Project.user_id || Project.isBusinessAdministrator()) {
				return true;
			}
			else {
				Project.alertElement.showItem(14,{task_alert:msg});
				return false;
			}
		}
	});

	return Project;

});
