define([
	"dojo/store/JsonRest",
	"dojo/_base/lang",
	"hi/store/QueryResults",
	'dojo/_base/array'
], function(JsonRest, lang, QueryResults, dojoArray) {
  //  module:
  //    hi/store/JsonRest
  //  summary:
  //    The module extends dojo.store.JsonRest to allow URL parameters in target option
	var Project = dojo.global.Project;

return dojo.declare("hi.store.JsonRest", [JsonRest], {
	
	getTarget: function (id, include_params, params, removeBaseId) {
		//	summary:
		//		Returns target URL. To target string is added id and targetParams
		//	id: Number
		//		The identity to use to lookup the object
		//  include_params: Boolean
		//      Include targetParams in string or not
		//  params: Object
		//		Additional params
		//	returns: String
		//		Target URL
		
		var target = this.target, 
			params = params ? dojo.mixin({}, params, this.targetParams || {}) : this.targetParams,
			doNotAppendId = typeof(this.targetOptions) != 'undefined' && this.targetOptions.doNotAppendId;

		if (removeBaseId) {
			target = target.slice(0, -1 * (target.length - target.lastIndexOf("/")));
		}
		
		if (id && !doNotAppendId) {
			if (target.indexOf('?') == -1) {
				target += (target.substr(-1, 1) != '/' ? '/' : '') + id;
			} else {
				target = target.replace('/?', '?');
				target = target.replace('?', '/' + id + '?');
			}
		}
		if(include_params && params && typeof params == 'object') {
			var params = dojo.xhr.objectToQuery(params);
			target += (params ? (target.indexOf("?") == -1 ? "?" : "&") + params : '');
		}
		
		return target.charAt(0) != '/' ? ('/' + target) : target;
	},
	
	objectToQuery: function (obj) {
		// summary:
		//		Convert object to query and convert arrays into comma separated strings
		
		var out = {};
		for (var key in obj) {
			if (dojo.isArray(obj[key])) {
				out[key] = obj[key].join(',');
			} else {
				out[key] = obj[key];
			}
		}
		
		return dojo.xhr.objectToQuery(out);
	},
	
	get: function(id, options) {
		//	summary:
		//		Retrieves an object by its identity. This will trigger a GET request to the server using
		//		the url `this.target + id`.
		//	id: Number
		//		The identity to use to lookup the object
		//	returns: Object
		//		The object in the store that matches the given id.
		var headers = options || {};
		headers.Accept = this.accepts;
		return dojo.xhr("GET", {
			url: Project.api2_base_url + this.getTarget(id, true, options ? options.urlParams : null),
			handleAs: "json",
			headers: headers,
			failOk: !Project.isDevMode // Prevent Dojo from outputting errors to console (#1821)
		});
	},
	
	put: function(object, options) {
		// summary:
		//		Stores an object. This will trigger a PUT request to the server
		//		if the object has an id, otherwise it will trigger a POST request.
		// object: Object
		//		The object to store.
		// options: dojo.store.api.Store.PutDirectives?
		//		Additional metadata for storing the data.  Includes an "id"
		//		property if a specific id is to be used.
		//	returns: Number
		options = options || {};
		var id = ("id" in options) ? options.id : this.getIdentity(object);
		var hasId = typeof id != "undefined";
		var postData = null,
			contentType = '',
			timeout = 0;
		
		/* #2766 */
		if ('timeout' in object)
		{
			timeout = object['timeout'];
			delete object['timeout'];
		}
		
		if (this.envelope === "URL") {
			//URLEncoded
			postData = this.objectToQuery(object);
			contentType = "application/x-www-form-urlencoded";
		} else {
			//JSON encoded
			postData = JSON.stringify(object);
			contentType = "application/json";
		}

		var removeBaseId = typeof(this.targetOptions) != 'undefined' && this.targetOptions.removeBaseId;
		
		return dojo.xhr(hasId && !options.incremental ? "PUT" : "POST", {
				url: Project.api2_base_url + this.getTarget(id, true, options ? options.urlParams : null, removeBaseId),
				postData: postData,
				handleAs: "json",
				headers:{
					"Content-Type": contentType,
					Accept: this.accepts,
					"If-Match": options.overwrite === true ? "*" : null,
					"If-None-Match": options.overwrite === false ? "*" : null
				},
				failOk: !Project.isDevMode, // Prevent Dojo from outputting errors to console (#1821)
				timeout:timeout
			});
	},
	remove: function(id, options){
		// summary:
		//		Deletes an object by its identity. This will trigger a DELETE request to the server.
		// id: Number
		//		The identity to use to delete the object
		
		var formData = null,
			contentType = null;
		
		if (this.envelope === "URL") {
			//URLEncoded
			formData = options.formData ? this.objectToQuery(options.formData) : null;
			contentType = "application/x-www-form-urlencoded";
		} else {
			//JSON encoded
			formData = options.formData ? JSON.stringify(options.formData) : null;
			contentType = "application/json";
		}
		
		return dojo.xhr("DELETE", {
			url: Project.api2_base_url + this.getTarget(id, true, options ? options.urlParams : null),
			postData: formData,
			headers: {
					"Content-Type": contentType
			},
			failOk: !Project.isDevMode // Prevent Dojo from outputting errors to console (#1821)
		});
	},
	
	didLoad:0,
	query: function(query, options){
		// summary:
		//		Queries the store for objects. This will trigger a GET request to the server, with the
		//		query added as a query string.
		// query: Object
		//		The query to use for retrieving objects from the store.
		//	options: dojo.store.api.Store.QueryOptions?
		//		The optional arguments to apply to the resultset.
		//	returns: dojo.store.api.Store.QueryResults
		//		The results of the query, extended with iterative methods.
		var headers = {Accept: this.accepts},
			target = this.getTarget(null, true);
		
		options = options || {};

		if(options.start >= 0 || options.count >= 0){
			headers.Range = "items=" + (options.start || '0') + '-' +
				(("count" in options && options.count != Infinity) ?
					(options.count + (options.start || 0) - 1) : '');
		}
		if(options && options.time){
			target = (target.indexOf('?') == -1 ? target.replace(/\/?$/, '/since') : target.replace(/\/?\?/, '/since?'));
			query = query || {};
			query.time = options.time;
			if (options.forceIds) {
				query.forceIds = options.forceIds;
			}
		}
		if(query && typeof query == "object"){
			query = dojo.xhr.objectToQuery(query);
			query = query ? (target.indexOf("?") == -1 ? "?" : "&") + query: "";
		}
		if(options && options.sort){
			var sortParam = this.sortParam;
			query += (query ? "&" : (target.indexOf("?") == -1 ? "?" : "&")) + (sortParam ? sortParam + '=' : "sort(");
			for(var i = 0; i<options.sort.length; i++){
				var sort = options.sort[i];
				query += (i > 0 ? "," : "") + (sort.descending ? '-' : '+') + encodeURIComponent(sort.attribute);
			}
			if(!sortParam){
				query += ")";
			}
		}
		
		var request_str = 'http://localhost:9200/img/3500.json';
		
		if (this.didLoad) request_str='http://localhost:9200/img/3500_refresh.json';
		this.didLoad = true;
		var results = dojo.xhr("GET", {
			url:Project.api2_base_url + target + (query || ""),//request_str,'http://localhost:9200/img/3500.json',//Project.api2_base_url + target + (query || ""),//'http://localhost:9200/img/data.json',//Project.api2_base_url + target + (query || ""),
			handleAs: "json",
			headers: headers,
			failOk: !Project.isDevMode // Prevent Dojo from outputting errors to console (#1821)
		});

		results.total = results.then(function(){
			var range = results.ioArgs.xhr.getResponseHeader("Content-Range");
			return range && (range=range.match(/\/(.*)/)) && +range[1];
		});
		var qr = QueryResults(results);
		/* Remove items from store that are not found in result set */
		var storeIDs = [];
		var store = hi.items.manager.get().forEach(function(item) {if (item.id>0) storeIDs.push(item.id);});
		qr.then(function(qrData) {
			var qrIDs = [];
			dojo.forEach(qrData, function(item) {qrIDs.push(item.id)});
			dojo.forEach(storeIDs, function(id) {
				if (dojoArray.indexOf(qrIDs,id) == -1)
				{
					hi.items.manager.remove(id);
				}
			});
			
		});
		return qr;
	}
});

});