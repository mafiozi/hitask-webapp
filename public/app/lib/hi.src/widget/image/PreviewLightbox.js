define([
	"dojo/_base/declare",
	"dojox/image/Lightbox",
	'dojo/on',
	'dojo/query'
], function(declare, djLightboxDialog, dojoOn, dojoQuery) {
	return declare("hi.widget.PreviewLightbox", [dojox.image.LightboxDialog], {
		_resizedFirstTime: false,
		
		show: function(/* Object */groupData) {
			this.inherited(arguments);
			
			this._fillWindow();
			var that = this;
			// Dialog is misplaced first time.
			if (!this._resizedFirstTime) {
				var c = dojo.connect(that.imgNode, "load", that, function(e) {
					dojo.disconnect(c);
					setTimeout(function() {
						that.resize();
						that._resizedFirstTime = true;
					}, 1000);
				});
			}
			
			// Make underlay and dialog z-index to be highest.
			dojoQuery('.dijitDialogUnderlayWrapper').style({zIndex: 94900});
			dojoQuery('.dojoxLightbox').style({zIndex: 95000});
		},
		
		_fillWindow: function() {
			// Popup sized 80% of window size.
			// Some GIF images provide us with width and height = 0!
			if (this.imgNode && this.imgNode.width > 0 && this.imgNode.height > 0) {
				var box = dojo.window.getBox();
				var proportion = this.imgNode.width / this.imgNode.height;
				if (this.imgNode.width > this.imgNode.height) {
					var width80 = box.w * 0.8;
					var height80 = width80 * this.imgNode.height / this.imgNode.width;
				} else {
					var height80 = box.h * 0.8;
					var width80 = height80 * this.imgNode.width / this.imgNode.height;
				}
				if (width80 < this.imgNode.width || height80 < this.imgNode.height) {
					// #4451
					this.resizeTo({w: width80, h: height80});
				}
			}
		},
		
		_onImageClick: function(e) {
			this.inherited(arguments);
			
			// #4402: stop propagation.
			if (e) {
				e.stopPropagation();
			}
		}
	});
});