#!/bin/bash
#Run script for elastic beanstalk
#enable the use of environment variable expansion via $, as well as using the % character without escaping.
exec ./play/play run -Dprecompiled=true --%$PLAY_ID
