package beans;

import org.apache.commons.lang.StringUtils;


public class UserAuthSession {
	private String sessionId;
	private String userExternalId;
	private String externalAccount;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserExternalId() {
		return userExternalId;
	}

	public void setUserExternalId(String userExternalId) {
		this.userExternalId = userExternalId;
	}

	public String getExternalAccount() {
		return externalAccount;
	}

	public void setExternalAccount(String externalAccount) {
		this.externalAccount = externalAccount;
	}
	
	public String getExternalAccountFormatted() {
		return StringUtils.isNotBlank(externalAccount) ? StringUtils.capitalize(externalAccount.toLowerCase()) : null;
	}
}
