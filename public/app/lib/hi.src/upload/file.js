define([
	"dojo/dom-style",
	"dojox/form/Uploader"
], function (domStyle, _Uploader) {
		
	dojo.global.FileUploadFile = function (parent) {
		if (!parent) return;
		
		this.id = FileUpload.guid();
		this.parent = parent;
		
		if (this.parent.taskId <= 0) {
			this.parentId = hi.items.manager.get(this.parent.taskId, "parent") || 0;
		} else {
			this.parentId = this.parent.taskId;
		}
		
		this._create();
	};
	
	dojo.global.FileUploadFile.prototype = {
		/**
		 * Unique ID for file instance
		 */
		id: null,
		
		/**
		 * Determines what type of file uploader to use (Flash vs. HTML5) and whether or not to allow Dnd file upload
		*/
		doDnd: dojo.isFF >= 3.6 || dojo.isChrome >= 10 || dojo.isSafari >= 6,

		doHTML5FileUpload: !dojo.isIE || dojo.isIE > 10,

		/**
		 * Parent object, FileUploadWidget instance
		 * @type {Object}
		 */
		parent: null,
		
		/**
		 * Container node
		 * @type {HTMLElement}
		 */
		nodeContainer: null,
		
		/**
		 * Form node
		 * @type {HTMLElement}
		 */
		nodeForm: null,
		
		/**
		 * Original node which wasn't cloned
		 */
		nodeClone: null,
		
		/**
		 * File uploader instance
		 * @type {Object}
		 */
		uploader: null,
		
		/**
		 * Number of retries before giving up
		 * @type {Number}
		 */
		retries: 0,
		
		/**
		 * Upload was canceled
		 * @type {Boolean}
		 */
		canceled: false,
		
		/**
		 * File was destroyed or is waiting for upload to complete
		 * to be destroyed
		 * @type {Boolean}
		 */
		destroyed: false,
		
		/**
		 * Current upload progress
		 * @type {Number}
		 */
		uploadProgress: 0,
		
		/**
		 * Upload in progress
		 * @type {Boolean}
		 */
		uploading: false,
		
		/**
		 * File upload completed
		 * @type {Boolean}
		 */
		completed: false,
		
		/**
		 * Complete request is in progress
		 * @type {Boolean}
		 */
		completing: false,
		
		/**
		 * File is ready for upload
		 * @type {Boolean}
		 */
		isready: false,
		
		/**
		 * Filename
		 * @type {String}
		 */
		filename: null,
		
		/**
		 * File size
		 * @type {Number}
		 */
		filesize: null,
		
		/**
		 * File ID
		 * @type {String}
		 */
		key: null,
		
		/**
		 * Error uploading file
		 * @type {Boolean}
		 */
		uploadError: false,
		
		/**
		 * Parent task ID
		 * @type {Number}
		 */
		parentId: null,
			
		/**
		 * Create form, button and uploader
		 */
		_create: function (fileName, fileSize, restore) {
			var label_attach = hi.i18n.getLocalization('properties.attach_file'),
				label_failed = hi.i18n.getLocalization('properties.file_upload_failed'),
				label_cancel = hi.i18n.getLocalization('properties.cancel_upload'),
				class_button = 'link-attach',
				data_icon = '&#xe008;',
				html_drag = '';
			
			fileName = fileName || '';
			fileSize = fileSize ? FileList.formatFileSize(fileSize) : '';
			
			if (this.parent.taskId < 0) {
				//If file is not attached to any task show different button text
				if (this.parent.files.length) {
					label_attach = hi.i18n.getLocalization('properties.attach_another_file');
				} else {
					label_attach = hi.i18n.getLocalization('properties.select_file');
				}
				class_button = 'button-attach';
				data_icon = '&#xe007;';
			} else if (this.parent.files.length) {
				//If already has files show different button text
				label_attach = hi.i18n.getLocalization('properties.attach_another_file');
			}
			
			if (this.doHTML5FileUpload && (FileUpload.HTML5_SUPPORTED || this.doDnd)) {
				html_drag = '<em class="drag-label">' + hi.i18n.getLocalization('properties.drag_file') + '</em>';
			}
			
			var nodeContainer = this.nodeContainer = document.createElement('LI');
			var html = '<form id="file_upload_' + this.id + '">\
							<div class="' + class_button + '"><span data-icon="' + data_icon + '" aria-hidden="true"></span><div>' + label_attach + '</div></div>\
							' + html_drag + '\
						</form>\
						<div class="txt-failure txt">' + label_failed + ' <span  fill="html: filename">' + fileName + '</span></div>\
						<div class="txt-success txt"><span class="uploaded_filename" fill="html: filename">' + fileName + '</span> <b></b></div>\
						<div class="txt-uploading txt"><a class="cancel">' + label_cancel + '</a><span class="progress-bar"><em></em><span>0%</span></span><span  class="uploaded_filename" fill="html: filename">'+fileName + '</span></div>\
						<div class="txt-ready txt"><a class="cancel">' + label_cancel + '</a><span  class="uploaded_filename" fill="html: filename">' + fileName + '</span></div>';
			
			nodeContainer.innerHTML = html;
			
			if (!this._allowedToAttachFile()) {
				nodeContainer.style.display = 'none';
			}
			/*if (this.parent.taskId<0)
				dojo.place(nodeContainer,dojo.byId('file_upload_form_'),'last');
			else*/
			
			if (!this.parent.nodeList) {
				return;
			}
			this.parent.nodeList.appendChild(nodeContainer);
			
			this.nodeButton = dojo.query('div.' + class_button, nodeContainer)[0];
			this.nodeButtonOverlay = this.nodeButton.cloneNode(true);
			
//			this.nodeProgress = dojo.query('span.progress-bar span', nodeContainer)[0];
			var q1res = dojo.query('span.progress-bar', nodeContainer);
			this.nodeProgress = dojo.query('span', q1res[0])[0];
			this.nodeProgressLine = previousElement(this.nodeProgress);
			
			this.nodeForm   = nodeContainer.getElementsByTagName('form')[0];
			this.nodeCancel = dojo.query('a.cancel', nodeContainer);
			
			dojo.forEach(dojo.query('a.cancel', nodeContainer), dojo.hitch(this, function (node) {
				dojo.connect(node, 'click', this, 'cancel');
			}));
			
			//If restoring, then uploader already exists
			if (restore) {
				//Iframe doesn't support progress, other style is used
				if (!this.supportProgress()) {
					var nodeUploading = dojo.query('div.txt-uploading', this.nodeContainer)[0];
					dojo.addClass(nodeUploading, 'txt-uploading-simple');
				}
				return;
			}
			
			if (dojo.isIE) {
				var self = this;
				setTimeout(function () {
					self._createUploader();
				}, 10);	
			} else {
				this._createUploader();
			}
		},
		
		_allowedToAttachFile: function() {
			var item = hi.items.manager.get(this.parentId);
			// 1958: We need to disable Attach File for Private items that are owned by another, not current user.
			// When private item was assigned to me I should not be able to attach files to it.
			
			// No need in this since 16.0 and #4926
			/*if (item && !item.shared && item.assignee == Project.user_id && item.user_id != Project.user_id) {
				return false;
			}*/

			if (SingleItem) return false;
			
			return true;
		},
		
		/**
		 * Restore uploader
		 */
		restore: function () {
			this._create(this.filename, this.filesize, true);
			
			if (this.uploading) {
				dojo.addClass(this.nodeContainer, 'file-uploading');
			} else if (this.isready) {
				dojo.addClass(this.nodeContainer, 'file-ready');
			}
		},

		/**
		 * Create flash object for file uploading
		 * Upload URL must be set when creating object
		 */
		_createUploader: function () {
			//Add button to uploader container
			FileUploadContainer.addFile(this);
			var uploaderId = dijit.getUniqueId('fileUploader');
			var options = {
				id:uploaderId,
				isDebug: false,
				devMode: false,
				uploadUrl: Project.api2_base_url+'/' + Project.api2_url_index + '/file/upload',
				//force: 'flash',
				selectMultipleFiles: false,
				name:'file',
				//flashFieldName: 'file',
				serverTimeout: 50000,
				skipServerCheck:true
			};
			
			if (!this.doHTML5FileUpload) {
				options.force = 'flash';
				options.flashFieldName = 'file';
			} 
			//Create uploader widget
			var uploader = new dojox.form.Uploader(options, this.nodeButtonOverlay);
			this.uploader = uploader;
	
			//Iframe doesn't support progress, other style is used
			if (!this.supportProgress()) {
				var nodeUploading = dojo.query('div.txt-uploading', this.nodeContainer)[0];
				dojo.addClass(nodeUploading, 'txt-uploading-simple');
			}
			
			dojo.connect(uploader, "onChange", this, '_onFileChange');
			dojo.connect(uploader, "onProgress", this, '_uploadProgress');
			dojo.connect(uploader, "onComplete", this, '_uploadOnComplete');
			dojo.connect(uploader, "onError", this, '_uploadOnError');
			
			//Overwrite to fix size
			uploader._getButtonStyle = function (node) {
				this.btnSize = {w:180, h:45};
			};
			
			uploader.startup();
			
			/* Bug #1995 - this should put a cursor pointer on uploader link */
			if (this.doHTML5FileUpload) {
				var query = dojo.query('span[widgetid='+uploaderId+'] input[name=file]')[0];
				dojo.style(query,'cursor','pointer');
				dojo.connect(dojo.byId(uploaderId),'_createInput',function() {
					dojo.style(query,'cursor','pointer');
				});
			}
			
			//Overwrite opacity for Safari5 compability
			domStyle.set(uploader.inputNode, {
				opacity:0.01
			});
		},
		
		/**
		 * Returns true if progress is supported, otherwise false
		 * 
		 * @return True if progress is supported, otherwise false
		 * @type {Boolean}
		 */
		supportProgress: function () {
			return this.uploader && this.uploader.uploaderType == 'html' ? false : true;
		},
		
		/**
		 * Handle error
		 * 
		 * @param {Object} evt
		 */
		_uploadOnError: function (evt) {
			this._uploadFailure('upload error');
		},
		
		/**
		 * Start upload
		 */
		_onFileChange: function (dataArray) {
			if (!this.destroyed && !this.uploading && !this.isready) {
				if (this.parent) {
					this.parent.addFile();
				}
				
				this.ready();
				
				//Files attached to task should start uploading automatically
				if (this.parent && this.parent.taskId > 0) {
					var self = this;
					// HACK need delay here because we call another Flash method 
					// before return from event handler
					setTimeout(function(){ self.parent.uploadNext(); }, 1000);
				}
			}
		},
			
		/**
		 * Handle upload complete event
		 */
		_uploadOnComplete: function(fileArray) {
			// Remember item properties from editing form.
			var itemProperties = hi.items.manager.editing() && hi.items.manager.editing().editingView ?
								hi.items.manager.editing().editingView.getFormValues() : null;
			
			//If upload was canceled, nothing to be done
			if (this.canceled) return false;
			
			/*
			 * Can't detect if upload was successful, because S3 doesn't
			 * return any data, must assume it was success
			 */
			if (this.canceled || this.completed || this.completing) return;
			
			if (fileArray instanceof Object && fileArray.id)
			{
				var tmp = fileArray.id;
				fileArray = [tmp];
			}
			if (fileArray.length) {
				//process only one file came first
				var file = fileArray[0];
				if (file.error) {
					this._uploadFailure(file.error);
				} else {
					this.completing = true;
					this._uploadSuccess(file, itemProperties);
				}
			} else if (typeof(fileArray) == 'object') {
				if (fileArray.error_message) {
					this._uploadFailure(fileArray.error_message);
					
					if (fileArray.response_status == 20) {
						Project.alertElement.showItem(13);
					}
				}
			}
		},
		
		/**
		 * Handle upload progress
		 */
		_uploadProgress: function (dataArray) {
			if (this.canceled) return false;
			
			if (dataArray.percent) {
				this.progress(dataArray.percent);
			} else {
				this.progress(dataArray[0].percent);
			}
		},
		
		/**
		 * Upload success handler
		 */
		_uploadSuccess: function(file, itemProperties) {
			if (this.canceled || this.completed) return;
			
			dojo.removeClass(this.nodeContainer, 'file-uploading');
			dojo.addClass(this.nodeContainer, 'file-success');
			var self = this;
			this.key = file.id;
			var fileId = typeof(file) == 'object' && typeof(file.additionalParams) == 'object' ? file.additionalParams.id : file;
			// Set properties of new item
			/* Bug #1960
			 * File is created as item in lists, now set its properties
			 */
			var newItemEditForm = hi.items.manager.editing() && hi.items.manager.editing().editingView;

			if (this.parent.taskId <= 0 && (itemProperties || newItemEditForm)) {
				var properties = itemProperties || hi.items.manager.editing().editingView.getFormValues();
				properties = dojo.mixin(properties, {id: fileId, title: this.filename, no_audit: true});
				hi.items.manager.set(properties);
				hi.items.manager.setSave(properties).then(function() {
					self.onUploadSuccess();
					newItemEditForm._rememberPermissions(properties.id);
					newItemEditForm._setLastCreatedItemType(6);
				});
			} else {
				this.onUploadSuccess();
			}
		},
		
		onUploadSuccess: function() {
			this.uploading = false;
			this.completed = true;
			
			this.progress(100);
			
			if (this.destroyed) {
				this.destroy();
			} else {
				if (this.uploader) {
					this.uploader.destroy();
				}
				FileUploadContainer.destroy(this.id);
			}
			
			//Update and start uploading next file
			if (this.parent) {
				this.parent.update();
				this.parent.uploadNext();
			}			
		},
		
		/**
		 * Upload failure handler
		 */
		_uploadFailure: function (message) {
			if (this.canceled || this.completed) return;
			
			if (this.retries > 0) {
				this.retries--;
				this.retry();
			} else {
				var msg = hi.i18n.getLocalization('properties.file_upload_' + String(message || '').toLowerCase()) || '';
				if (msg) {
					//Change error message text
					var node = dojo.query('div.txt-failure', this.nodeContainer);
					if (node.length) {
						node[0].innerHTML = msg;
					}
				}
				
				dojo.removeClass(this.nodeContainer, 'file-uploading');
				dojo.addClass(this.nodeContainer, 'file-failed');
				
				this.uploadError = true;
				this.uploading = false;
				this.completed = true;
				if (this.destroyed) {
					this.destroy();
				} else {
					if (this.uploader) {
						this.uploader.destroy();
					}
					FileUploadContainer.destroy(this.id);
				}
				
				this.progress(100);
				
				//Update and start uploading next file
				if (this.parent) {
					this.parent.uploadNext();
				}
			}
		},
			
		/**
		 * Move node to header tooltip,
		 * If using flash then flash movie can't be moved because it will
		 * redraw and file upload will be broken
		 */
		moveToTooltip: function (parentNode) {		
			var nodeContainer = this.nodeContainer.cloneNode(true);
			parentNode.appendChild(nodeContainer);
			
			dojo.addClass(this.nodeContainer, 'file-hidden');
			
			if (dojo.isIE && dojo.isIE < 8) {
				this.nodeContainer.parentNode.style.height = '0px';
				this.nodeContainer.parentNode.style.overflow = 'hidden';
			}
			
			this.nodeClone = this.nodeContainer;
			this.nodeContainer = nodeContainer;
			
			this.nodeProgress = dojo.query('span.progress-bar span', nodeContainer)[0];
			this.nodeProgressLine = previousElement(this.nodeProgress);
			
			this.nodeForm = nodeContainer.getElementsByTagName('form')[0];
			
			dojo.forEach(dojo.query('a.cancel', nodeContainer), dojo.hitch(this, function (node) {
				dojo.connect(node, 'click', this, 'cancel');
			}));
		},
		
		/**
		 * Cancel upload
		 */
		cancel: function () {
			if (this.canceled) return;
			this.canceled = true;
			
			if (this.uploading) {
				this.uploading = false;
				this.progress(0);
				if (this.uploader) {
					if (this.uploader._cons) {
						// #4075: Dojo uploader fails with undefined var while executing the "blur" event handler.
						// Disconnect from all event connections because anyway they are useless - we are going to destroy widget.
						dojo.forEach(this.uploader._cons, dojo.disconnect);
					}
					this.uploader.destroy();
				}
				
				//Start uploading next file
				if (this.parent) {
					this.parent.uploadNext();
				}
			}
			
			FileUploadContainer.destroy(this.id);
			
			if (this.parent) {
				this.parent.removeFile(this.id);
				this.parent.updateReadyView();
			}
		},
		
		/**
		 * Destroy file instance
		 */
		destroy: function () {
			if (!this.uploading) {
				this.cancel();
				
				//Remove from upload
				FileUploadContainer.destroy(this.id);
				
				if (this.uploader) this.uploader.destroy();
			}
			
			delete(this.nodeForm);
				
			if (this.nodeClone) removeNode(this.nodeClone);
			delete(this.nodeClone);
			
			if (this.nodeContainer) removeNode(this.nodeContainer);
			delete(this.nodeContainer);
			
			this.destroyed = true;
		},
		
		/**
		 * Retry uploading
		 */
		retry: function () {
			if (this.destroyed || this.canceled) return;
			//Retry functionality disabled
		},
		
		/**
		 * Mark file as ready for upload
		 */
		ready: function () {
			if (this.destroyed || this.canceled) return;
			
			var fileList = this.uploader.getFileList(),
				filename = fileList[0].name,
				fileSize = fileList[0].size;
			
			if (!filename) return;
			
			//Change style to uploading
			dojo.addClass(this.nodeContainer, 'file-ready');
			
			//Remove path from filename
			filename = filename.replace(new RegExp("^.*\/"), '');
			
			this.isready = true;
			this.filename = filename;
			this.filesize = fileSize;
			fillTemplate(this.nodeContainer, {'filename': filename, 'fileSize': FileList.formatFileSize(fileSize)});
			
			//Update ready state
			if (this.parent) {
				this.parent.updateReadyView();
			}
		},
		
		upload: function () {
			if (this.destroyed || this.canceled) return;
			
			var fileList = this.uploader.getFileList(),
				filename = fileList[0].name,
				fileSize = fileList[0].size;
			
			if (!filename) return;
			
			//Change style to uploading
			dojo.removeClass(this.nodeContainer, 'file-ready');
			dojo.addClass(this.nodeContainer, 'file-uploading');
			
			//Remove path from filename
			filename = filename.replace(new RegExp("^.*\/"), '');
			
			this.isready = false;
			this.uploading = true;
			this.filename = filename;
			this.filesize = fileSize;
			
			fillTemplate(this.nodeContainer, {'filename': filename, 'fileSize': FileList.formatFileSize(fileSize)});
	
			// Hide "Start upload" buttons
			if (this.parent) {
				this.parent.updateReadyView();
			}
			
			var _upl = this;
			
			FileUpload.loadStorageInfo(function (data) {
				var fileSizeMb = fileSize / 1024 / 1024;
				
				if ((data.quota - data.used - fileSizeMb) < 0){
					Project.alertElement.showItem(13);
					_upl.cancel();
					return;
				} else if (fileSize == 0) {
					Project.alertElement.showItem(14, {task_alert: 'Failed to upload "' + filename + '" because file size is 0 byte.'});
					_upl.cancel();
					return;
				} else if (fileSizeMb > CONSTANTS.maxUploadFileSizeMb) {
					Project.alertElement.showItem(20, {filename: filename});
					_upl.cancel();
					return;
				}
				
				// Show tooltip only for uploading to Project and creating File Item.
				if (!_upl.parent.taskId || _upl.parent.taskId < 0 || hi.items.manager.get(_upl.parent.taskId, 'category') == 0) {
					FileUploadTooltip.show();
				}
				
				data = {};
				data.api_key = Project.api2_key;
				data.api_ver = Project.api_version;
				data[Project.api2_session_name] = Project.api2_session_id;
				data.parent_id = _upl.parentId > 0 ? _upl.parentId : 0;
				if (!_upl.doHTML5FileUpload) data.is_flash = true;
				
				_upl.uploader.upload(data);
					
			});
		},
		
		progress: function (progress) {
			if (!this.destroyed) {
				progress = Math.min(100, Math.max(0, parseFloat(progress)));
				
				this.uploadProgress = progress;
				this.nodeProgress.innerHTML = ~~progress + '%';
				this.nodeProgressLine.style.width = progress + '%';
			}
		}
	};

	return dojo.global.FileUploadFile;
});
