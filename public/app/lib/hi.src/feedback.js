//dojo.provide("hi.feedback");

define(function () {
	dojo.global.feedback_change = function(input) {
		var form = dojo.byId('fdb_tooltip_form');
		fillTemplate(form, {
			feedback: (input.value == '9' ? true : false),
			hasEmail: Project.user_has_email
		});
	};
	
	dojo.global.feedback_show = function(e) {
		var form = dojo.byId('fdb_tooltip_form');
		fillTemplate(form, {
			hasEmail: Project.user_has_email
		});
	};
	
	dojo.global.feedback_submit = function(e) {
		var tooltip = dojo.byId('fdb_tooltip');
		
		//Show thank you message
		dojo.addClass('fdb_tooltip_form', 'hidden');
		dojo.removeClass('fdb_tooltip_message', 'hidden');
		
		//Close tooltip after 1 second
		setTimeout(function () {
			Tooltip.close('feedback');
		}, 1000);
		
		//Restore form after 2 seconds to compensate fadeOut
		setTimeout(function () {
			dojo.removeClass('fdb_tooltip_form', 'hidden');
			dojo.addClass('fdb_tooltip_message', 'hidden');
		}, 4000);
	};
	dojo.global.feedback_validate = function(e) {
		var text = getValue('feedback_text');
		
		if (text == '') {
			return false;
		}
		return true;
	};
	
	return {};
});