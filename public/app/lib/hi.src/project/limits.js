//dojo.provide("hi.project.limits");
define([
	"hi/project/base"
], function () {
	
	var Project = dojo.global.Project;
	
	dojo.mixin(Project, {
		
		projectCount: 0,
		itemCount: 0,
		
		trialWarning: function(type) {
			switch (type) {
				case 'TRIAL_FRIENDS':
					var k = 0;
					break;
				case 'TRIAL_TASKS':
					var k = 1;
					break;
				case 'TRIAL_PROJECTS':
					var k = 2;
					break;
				case 'TRIAL_BUSINESS':
					var k = 12;
					break;
			}
			Project.alertElement.showItem(k);
			
			//Delete focus information to prevent element from being focused
			//once alert element is closed
			delete(Project.alertElement._savedFocus);
		},
		
		checkLimits: function(type) {
			// summary:
			//		Check if limits has not been exceeded
			
			if (!Project.account_level) {
				Project.checkLimitsCount();
				
				if (Project.businessId || Project.isPremium) {
					//Expired business account
					Project.trialWarning('TRIAL_BUSINESS');
					return false;
				}
				if (type == 'TRIAL_PROJECTS') {
					if (Project.projectCount >= parseInt(CONSTANTS.maxProjects)) {
						Project.trialWarning('TRIAL_PROJECTS');
						return false;
					}
				}
				if (type == 'TRIAL_TASKS') {
					if (Project.itemCount >= parseInt(CONSTANTS.maxItems)) {
						Project.trialWarning('TRIAL_TASKS');
						hi.items.manager.removeLastNegative();
						return false;
					}
				}
			}
			return true;
		},
		
		checkLimitsCount: function () {
			// summary:
			//		Count projects and items
			
			var projectCount = 0,
				itemCount = 0,
				items = hi.items.manager.get();
			
			for (var i=0, ii=items.length; i<ii; i++) {
				if (!items[i].user_id || items[i].user_id == Project.user_id) {
					if (items[i].category == '0') {
						projectCount++;
					} else {
						itemCount++;
					}
				}
			}
			
			Project.projectCount = projectCount;
			Project.itemCount = itemCount;
		}
	
	});

	return Project;

});