define([
	
	"dojo/dnd/common",
	"dojo/dnd/Avatar",
	"dojo/dnd/Selector",
	"dojo/dnd/Manager",
	"dojo/dnd/Source",
	
	"hi/dnd/base",
	"hi/dnd/avatar",
	"hi/dnd/source",
	"hi/dnd/target/calendar",
	"hi/dnd/target/timetable",
	"hi/dnd/target/day",
	"hi/dnd/target/team",
	"hi/dnd/target/task",
	"hi/dnd/target/project"

], function () {
	return {};
});
