package controllers;

import play.mvc.Controller;
import beans.WebappVersion;

public class API extends Controller {
	public static void version() {
		renderJSON(new WebappVersion());
	}
}
