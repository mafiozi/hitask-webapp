/**
 * Instantiation of task lists (and the groups that contain them) on Date tab:
 * hi.items.TaskListOverdue -> overdue tasks
 * hi.items.TaskListToday -> tasks for today
 * hi.items.TaskListTomorrow -> tasks for tomorrow
 * hi.items.TaskListDuringWeek -> tasks coming up within the next 7 days
 * hi.items.TaskListAfterWeek -> tasks coming up after this week
 * hi.items.TaskListDateUngrouped -> undated tasks
 */
define([
	'hi/store',
	'hi/items/base',
	'dojo/aspect',
	'hi/widget/task/item_DOM',
	'hi/items/date'
], function (hi_store, hi_items, dojo_aspect, item_DOM) {
	
	var getDayDiff = function (data) {
		// summary:
		//		Returns difference in days between task date and now
		//		If task is overdue then returns -1
		//		If task doesn't have a date then returns null
		// data: Object
		//		Task data
		/* 
		 * #2538 (after ticket was closed) To check overdue:
		 * 1) if end_date, truncate to midnight, truncate today to midnight, compare times
		 * 2) else do the same thing to start_date
		 * 3) else it is not overdue because it does not have specified dates
		 */
		if (data.end_date && data.end_date !== '') {
			/* #2797 */
			var eD;
			if (data.recurring) {
				//eD=hi.items.date.getEventDate(data.id);
				eD = time2strAPI(hi.items.date._getNextRecurrenceDate(data));
			} else {
				eD = data.end_date;
			}
			if ( hi.items.date.truncateTime(new Date(eD)).getTime() < hi.items.date.truncateTime(new Date()).getTime() ) return -1;
		} else if (data.start_date && data.start_date !== '') {
			/* #2797 */
			var sD;
			if (data.recurring) {
				//sD=hi.items.date.getEventDate(data.id);
				sD =time2strAPI(hi.items.date._getNextRecurrenceDate(data));
			} else {
				sD = data.start_date;
			}
			if ( hi.items.date.truncateTime(new Date(sD)).getTime() < hi.items.date.truncateTime(new Date()).getTime() ) return -1;
		}
		/*
		if (Hi_Overdue.isOverdue(data.id)) {
			var overdue_time = hi.items.date.getEventEndTime(data.id, '23:59');
			if (!overdue_time) {
				overdue_time = hi.items.date.getEventStartTime(data.id, '23:59');
				console.log('overdue time1',overdue_time)
			}
			if (overdue_time) {
				//var d = hi.items.date.truncateTime(new Date());
				* #2590 truncating time sets the 'current' date to today at midnight - so overdue_time -d is never < 0 *
				var d = new Date();
				if (overdue_time - d < 0) {
					return -1;
				}
			}
			
			return 0;
		}
		*/
		var date_start = false,
			date_end = false,
			recurring_date = null,
			dif_s = false,
			dif_e = false,
			dif = false,
			today = hi.items.date.truncateTime(new Date()).getTime();
			
			// #2748
			//today = new Date().getTime();/* #2742 - Don't truncate, same as #2590 hi.items.date.truncateTime(new Date()).getTime();*/
		
		if (data.end_date && data.end_date !== null && data.end_date !== '') {
			date_end = str2time(data.end_date);
			date_end = new Date(date_end.getTime() - 1);
			// date_end = str2time(data.end_date, 'y-m-d');
			date_end.setMinutes(0);
			date_end.setSeconds(0);
			date_end.setHours(0);
			date_end = date_end.valueOf();
		}
		if (data.start_date && data.start_date !== null && data.start_date !== '' && !parseInt(data.recurring)) {
			var item = HiTaskCalendar.nearestEvent(data);
			if (item.startDate) {
				date_start = item.startDate.valueOf();
			} else {
				date_start = false;
			}
			// if (item.endDate) {
			// 	date_end = item.endDate.valueOf() - 1;
			// } else {
			// 	date_end = false;
			// }
		}
		
		if (parseInt(data.recurring)) {
			date_end = hi.items.date.getEventEndTime(data.id, '23:59');
			date_start = hi.items.date.getEventStartTime(data.id, '23:59');
			
			if (date_end) date_end = date_end.getTime();
			if (date_start) date_start = date_start.getTime();
			
			//recurring_date = hi.items.date.getNextEventDate(data);
			/* #2493 recurring_date = data.instances.length ? hi.items.date.getEventInstanceDate(data) : null;*/
			recurring_date = hi.items.date._getNextRecurrenceDate/*_getEventInstanceDate*/(data);
			
			if (recurring_date) {
				hi.items.date.truncateTime(recurring_date);
				
				if (date_end) {
					date_end = recurring_date.getTime();
				} else {
					date_start = recurring_date.getTime();
				}
			}
		}
		
		if (date_start || date_end) {
			if (date_start) {
				dif_s = Math.round((date_start - today) / 86400000); // 86400000 = 24 hours = 24 * 60 * 60 * 1000
				/* #2742 - if no end date, refine difference down to the millisecond */
				// #2748
				//if ((!date_end || date_end==date_start) && dif_s == 0 && date_start < today) dif_s = -1;
				/* #2473 - ditto */
				// #2748
				//if ((!date_end || date_end==date_start) && dif_s == 0 && date_start > today) dif_s = 1;
			}
			if (date_end) {
				dif_e = Math.round((date_end - today) / 86400000);
				/* #2743 */
				//if (dif_e == 0 && date_end > today) dif_e = 1;
			}
			
			if (dif_e !== false && dif_s !== false) {
				if (dif_e < 0 && dif_s < 0) {
					dif = Math.max(dif_e, dif_s);
				} else if (dif_e > 0 && dif_s > 0) {
					dif = Math.min(dif_e, dif_s);
				} else {
					dif = dif_e;
				}
			} else if (dif_e !== false) {
				dif = dif_e;
			} else if (dif_s !== false) {
				dif = dif_s;
			}

			//Show yearly recurring events only 10 days prior
			if (data.recurring == 4) {
				if (dif !== false) {
					if (dif_s !== false && dif_s <= 10) {
					} else if (dif_e !== false  && dif_e <= 10) {
					} else {
						return null;
					}
				}
			}
			return dif;
		} else {
			return null;
		}
	};
	//After hi.store.init create date groups
	dojo.global.viewOverdue = function(q) {
	//dojo.connect(hi_store, "afterInit", hi_store, function () {
		// Create task groups (Overdue, Today, Tomorrow, etc.) and set filters
		
		var readStore = hi_store.ProjectCache,
			writeStore = hi_store.Project,
			query = q,//readStore.query(),
			
			groupSelf = 'overdue',
			groupCurrent = Hi_Preferences.get('currentTab', 'my', 'string');

		if (groupCurrent == groupSelf) {
			var myD = dojo.connect(hi_store,'afterLoad', function() {
				// Add to container
				if (groupSelf == groupCurrent) {
					finalizeInitialization([],[hi_items.TaskListOverdue]);
				}
				dojo.disconnect(myD);
			});
		}
		
		// Task groups
		// hi_items.GroupOverdue = new hi.widget.Group({
		// 	"projectId": "overdue",
		// 	"emptyVisible": true, // shown if there are no tasks in it
		// 	"expanded": true,     // by default expanded
			
		// 	"title": hi.i18n.getLocalization("center.overdue"),
		// 	"moveTitle": "",
		// 	"groupClassName": "day",
			
		// 	"disabled": groupSelf != groupCurrent,
		// 	"preferencesGroup": groupSelf
		// });
		
		// hi.items.DateGroupsList = [hi.items.GroupOverdue];
		
		// Task lists
		hi_items.TaskListOverdue = new hi.widget.TaskList({
			"readStore": readStore,
			"writeStore": writeStore,
			"query": query,			//groupSelf == groupCurrent ? query : null,
			"disabled": groupSelf != groupCurrent,
			showing:groupSelf == groupCurrent ? true : false,
			mainList:true,
			"filterType": "any", // If item or any of the parent matches filter then this item can be shown in this list
			"filter": function (item, strict) {
				// summary:
				//		Filter items which should be in the list. Returns true on success,
				//		false on failure
				// item:
				//		Item data
				
				/* #3049 */
				if ('category' in item && item.category == item_DOM.CATEGORY_EVENT) return false;

				/* #3640 */
				if (item.recurring && item.recurring == 1) return false;
				if (item.recurring){
					return this.myCalculateInstanceDate(item);
				}
				
				
				if (getDayDiff(item) === -1) return true;
				/* #2333 */if (getDayDiff(item) <0) return true;

				if (!strict && this.filterMatches(item.parent)) return true;
				return false;
			},
			myCalculateInstanceDate: function(id) {
				/*
				 * Summary:
				 * Overrides the default action of calculateInstanceDate method
				 */
				//Find first non-removed,non-completed instance date in series that occurs today
				var item = typeof id === 'object' ? id : this.readStore.get(id),
					yesterday = new Date(),
					start_time = str2timeAPI(item.start_date),
					series;
					yesterday.setDate(yesterday.getDate()-1);
					series = hi.items.date.generateRecurrenceSeries(item,time2str(start_time,'y-m-d'),time2str(yesterday,'y-m-d'), true);
					if (item.title=='2') console.log('SERIES',[item.start_date,start_time,time2str(start_time,'y-m-d'),time2str(yesterday,'y-m-d'),series]);
					if (series && series.length>0) return series[series.length-1];
					else return null;
			}
		});
		
		// Add to container
		var container = dojo.byId(Project.getContainer('tasksList'));

		hi_items.TaskListOverdue.placeAt(container);

		dojo.connect(hi.items.manager, "groupingChange", hi.items.manager, function (group, groupPrev) {
			// summary:
			//		On group change enable/disable lists
			if (group == groupSelf && groupPrev != groupSelf) {
				refreshTab([],[hi_items.TaskListOverdue]);
			} else if (group != groupSelf && groupPrev == groupSelf) {
				hi_items.TaskListOverdue.set("disabled", true);
			}
			
			groupCurrent = group;
		});
		
		dojo.connect(Project, "changeSorting", Project, function () {
			// summary:
			//		On sorting change empty lists
			
			if (groupCurrent == groupSelf) {
				/* #2509 
				hi_items.TaskListOverdue.reset({"fill": false});
				hi_items.TaskListToday.reset({"fill": false});
				hi_items.TaskListTomorrow.reset({"fill": false});
				hi_items.TaskListDuringWeek.reset({"fill": false});
				hi_items.TaskListAfterWeek.reset({"fill": false});
				hi_items.TaskListDateUngrouped.reset({"fill": false});
				*/
				hi_items.TaskListOverdue.set("disabled", true);
			}
		});
		
		dojo.connect(hi.items.manager, "filterChange", hi.items.manager, function () {
			// summary:
			//		On sorting change empty lists
			/* #2509 
			if (groupCurrent == groupSelf) {
				hi_items.TaskListOverdue.reset({"fill": false});
				hi_items.TaskListToday.reset({"fill": false});
				hi_items.TaskListTomorrow.reset({"fill": false});
				hi_items.TaskListDuringWeek.reset({"fill": false});
				hi_items.TaskListAfterWeek.reset({"fill": false});
				hi_items.TaskListDateUngrouped.reset({"fill": false});
			}
			*/
			
			if (groupCurrent == groupSelf) {
				hi_items.TaskListOverdue.set("disabled", true);
			}
		});
		
		/* This is invoked after refresh data store on filter selection change 
		 * This should just initiate the query observers on the lists and enable the lists
		 */
		dojo.connect(hi.items.manager, "redraw", hi.items.manager, function () {
			// summary:
			//		Some major change happened, redraw lists
			
			if (groupCurrent == groupSelf) {
				/* #2331 - rereshTab on searchChange */
				//if (hi_items.TaskListOverdue.get('disabled')) initiateTab([hi_items.GroupOverdue,hi_items.GroupToday,hi_items.GroupTomorrow,hi_items.GroupDuringWeek,hi_items.GroupAfterWeek],[hi_items.TaskListDateUngrouped]);
				/*else*/
				refreshTab([], [hi_items.TaskListOverdue]);
			}
		});
		
		hi.items.manager.getTaskLists[groupSelf] = function () {
			// summary:
			//		Return all task lists for this group
			
			return [hi_items.TaskListOverdue];
		};
		
	};//);
	
	dojo.global.viewDate.getDayDiff = getDayDiff;
});
