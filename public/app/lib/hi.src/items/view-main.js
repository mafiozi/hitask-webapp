/**
 * Kicks off observers for all task lists and calendar
 * 
 * The viewX methods below instantiate task lists and initiate observers for those lists.  How filtering and tab changes are 
 * handled are also defined.  These methods and definitions are located in view-X.js.  
 * For example viewMyTab is defined in view-my.js.
 */
define([
	'hi/store',
	'hi/items/base',
	'dojo/aspect',
	'hi/items/date'
], function (hi_store, hi_items, dojo_aspect) {
	dojo.connect(hi_store,'init',hi_store,function() {
		showThrobber();
		var q = hi_store.ProjectCache.query();
		//View Today
		viewTodayTab(q);
		//View My
		viewMyTab(q);
		//View Activity Feed
		viewActivityFeedTab(q);
		//View Date
		viewDate(q);
		// View Overdue
		viewOverdue(q);
		//View Project
		viewProjectTab(q);
		//View Team
		viewTeamTab(q);
		//Big Calendar
		viewBigCalendarTab(q);
		//View Color
		viewColorTab(q);
		//Calendar
		viewAllTab(q);
		//hideThrobber();
	});

});