define([
	'hi/store/JsonRest',
	'hi/store/Memory',
	'dojo/store/DataStore',
	'dojo/data/ObjectStore',
	'hi/store/Observable',
	'hi/store/Cache'
], function (JsonRest, Memory, DataStore, ObjectStore, Observable, Cache) {
	
	var ns = dojo.getObject('hi.store', true);
	
	/**
	 * Create new hi.store.Cache store (hi.store.JsonRest + hi.store.Observable(dojo.store.Memory) )
	 * Adds to the hi.store namespace:
	 *   Name (hi.store.Cache instance)
	 *   NameJson (hi.store.JsonRest instance)
	 *   NameCache (observable dojo.store.Memory instance)
	 * 
	 * @param {String} name Store name
	 * @param {Object} options JsonRest options 
	 */
	ns.create = function (name, options) {
		var params = {},
			json,
			memory,
			cache;
		
		//Add api path
		options.envelope = options.envelope || 'URL';
		options.target = (Project.api2_url_index ? Project.api2_url_index + '/' : '') + options.target;
		
		//Add params to the URL
		params.api_ver = Project.api_version;
		params.api_key = Project.api2_key;
		params[Project.api2_session_name] = Project.api2_session_id;
		
		if (options && options.targetParams) {
			dojo.mixin(params, options.targetParams);
		} else {
			options.targetParams = params;
		}
		
		//Create stores
		json = ns[name + 'Json'] = new JsonRest(options);
		
		//Memory store
		memory = ns[name + 'Cache'] = Observable(new Memory({}));
		
		//Observable cache
		cache = ns[name] = new hi.store.Cache(json, memory);
		
		return cache;
	};
	
	return ns;
	
});