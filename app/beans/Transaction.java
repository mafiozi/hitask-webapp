package beans;

import java.io.Serializable;
import java.util.Date;

public class Transaction extends BaseBean implements Serializable {
	protected String time;
	protected Double amount;
	protected String product;
	
	public String getTime() {
		return time;
	}
	
	public Date getTimeAsDate() {
		return time != null ? parseApiDate(time) : null;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}
}
