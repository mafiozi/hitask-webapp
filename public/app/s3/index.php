<?php

/*
 * THIS IS TEMPORARY FILE FOR TESTING!
 */

/*
 * Simulate S3 response for file upload.
 * S3 shouldn't redirect if file is uploaded using drag&drop; S3 upload
 * response is always empty and has status 200
 */
header('HTTP/1.1 200 OK');