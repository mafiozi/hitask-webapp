//dojo.provide("hi.project.save");
define([
	'hi/widget/task/item_DOM',
	"hi/project/base"
], function (item_DOM) {
	
	var Project = dojo.global.Project;
				
	dojo.mixin(Project, {
		
		/*Add item*/
		newItem: false,
		newItemQueue: [],
		
		addEnter: function(e) {
			// summary:
			//		Save new item
			
			e = e || window.event;
			var key = e.which || e.keyCode || false;
			if (key == 13) {
				var item = hi.items.manager.editing();
				if (item && item.editingView) {
					item.editingView.save();
				}
			}
		},
		addCancel: function() {
			// summary:
			//		Cancel new item editing
			
			var item = hi.items.manager.editing();
			if (item && item.editingView) {
				item.editingView.save();
			}
		},
		
		assign: function(id, assign, unassign) {
			// summary:
			//		Assign task
			// id: Number
			//		Task ID
			// assign: Number
			//		User to assign to, or false if task shouldn't be assigned to anyone
			// unassign: Number
			//		User to unnassign from (is this really needed or is this legacy code?)
			
			assign = assign || false;
			unassign = unassign || false;
			if (assign == unassign) return;
			if (assign == hi.items.manager.get(id, 'assignee')) assign = false;
			if (unassign != hi.items.manager.get(id, 'assignee')) unassign = false;
			
			if (unassign && dojo.isArray(unassign)) unassign = unassign[0] || false;
			if (assign && dojo.isArray(assign)) assign = assign[0] || false;
			
			if (!assign && !unassign) return;
			
			var editing = hi.items.manager.editing();
			if (editing && editing.taskId == id) {
				editing.editingView.assigneeInput.value = assign;
			}
			
			//var taskProperties = hi.items.manager._items[id].list.propertiesView?  hi.items.manager._items[id].list.propertiesView.task:null;
			//if (taskProperties) taskProperties.set("assignee", assign ? assign : 0);
			item_DOM._save(id,{assignee:assign?assign:0});

			// #2314
			hi.items.manager.setSave(id, {"assignee": assign ? assign : 0});
		},
		
		unassign: function(id, options, save) {
			save = save || false;
			options = dojo.mixin({after: function(){processProperties(id)}}, options || {});
			Project.iterateTaskElements(id, function(el){el.innerHTML = ''}, 'person');
			if (save) {
				processProperties(false,{save:true});
			}
			Project.assign(id, false, Project.get(id, 'assign_user'), options);
		},
		
		assigned: function(id) {
			// summary:
			//		Returns true if task is assigned or false if it isn't
			//		Will return false if it is assigned to current user
			
			var assigned = hi.items.manager.get(id, 'assignee');
			return assigned && assigned != Project.user_id;
		},
		
		canAssign: function(task_id, user_id, shared) {
			// summary:
			//		Returns true if task can be assigned to the user
			// task_id: Number
			//		Task ID
			// user_id: Number
			//		User ID
			// shared: Boolean
			//		If task is shared or not
			
			var fr = Project.getFriend(user_id);
			if (user_id == Project.user_id) return true;
			if (!fr) return false;
			if (typeof shared == 'undefined' || shared === null) shared = hi.items.manager.get(task_id, 'shared');
			if (shared == 1) {
				if (fr.subscription.indexOf('BIS') == 0 || fr.subscription.toUpperCase() === 'BOTH') {
					return true;
				} else {
					return false;
				}
			} else {
				var owner = Project.user_id; // default for new item
				if (task_id > 0) {
					owner = hi.items.manager.get(task_id, 'user_id');
				}
				if (owner == Project.user_id) {
					return true;
				} else if (user_id == owner) {
					return true;
				} else {
					return false;
				}
			}
		}
	
	});

	return Project;

});