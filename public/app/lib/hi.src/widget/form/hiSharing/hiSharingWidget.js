define([
	"dojo/_base/declare",
	"dojo/_base/lang",
	'dojo/sniff',
	'dojo/aspect',
	'dojo/on',

	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dijit/_WidgetsInTemplateMixin',
	'dijit/_HasDropDown' ,

	'dojo/text!./hiSharingWidget.html',
	'hi/widget/form/hiSharing/hiSharingDropDown'
], function(declare, lang, sniff, aspect, on, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin,
			_HasDropDown, template, SharingDropDown) {

	var hiSharing = declare("hi.widget.Sharing", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
		//Fire the change event for every text change
		intermediateChanges: true,

		templateString: template,

		taskId: 0,

		nls: hi.i18n.getLocalization('task'),

		postCreate: function() {
			var t = this;
			this.inherited(arguments);

			this.own(
				aspect.after(this.sharingDropDown, 'onCountChange', lang.hitch(this, '_onStatusChange')),
				aspect.after(this.sharingDropDown, 'onChange', lang.hitch(this, 'onChange'))
			);
		},

		onChange: function(){},

		//0: private
		//1: sharing
		status: 0,
		_setStatusAttr: function(value){
			this._set('status', value);

			if(value === this.get('value')){return;}

			if(value === 0){
				dojo.global.showElement(this.privateNode);
				dojo.addClass(this.sharingDropDown.domNode, 'private');
			}else{
				dojo.global.hideElement(this.privateNode);
				dojo.removeClass(this.sharingDropDown.domNode, 'private');
			}
		},

		//permissions
		value: [],
		_getValueAttr: function(){
			return this.sharingDropDown.get('value');
		},

		_onStatusChange: function(){
			if(this.sharingDropDown.get('count') > 0){
				this.set('status', 1);
			}else{
				this.set('status', 0);
			}
		},

		fillSharingList: function (pms) {
			// summary:
			//		Fill sharing list
			// if (!('friends' in Project) && this.get('assignee') != Project.user_id) return;

			var taskId = this.taskId || 0,
				dd = this.sharingDropDown,		/*widget dropdown*/
				s = null,						/*widget dropdown store*/
				default_value = Hi_Preferences.get('default_assign_user', 0, 'int') || 0,
				default_permission = Hi_Preferences.get('default_sharing_permission', 50, 'int'),
				value = (this.isNew && !this.assignee ? default_value : this.assignee) || 0,
				shared = this.shared, tmp, friends, i, flen,
				// taskId = this.get('taskId'),
				task = taskId && hi.items.manager.get(taskId),
				owner = task && task.user_id;

			if(!dd || !dd.store) return;

			if(dd._dummy === true) return;

			if (!Project.businessId || (!shared && !this.my)) shared = null; // ignore if user doesn't have option to select it

			s = dd.store;
			//clear store
			s.setData([]);

			//every one
			var pm = Hi_Actions.getPermissionByUserId(pms, 'everyone');
			s.put({
				name: 'Everyone',
				id: 'everyone',
				isEveryOne: true,
				image16src: '',
				permission: pms && pm || default_permission,
				selected: pms && (pm !== null)
			});
			//my self
			// s.put({
			// 	name: Project.getMyself(),
			// 	id: Project.user_id,
			// 	image16src: dojo.global.getAvatar(16),
			// 	permission: pms && pms[Project.user_id] || default_permission,
			// 	selected: pms && pms.hasOwnProperty(Project.user_id)
			// });
			
			// add friends
			if ('friends' in Project) {
				friends = Project.friends;
				flen = friends.length;

				for (i = 0; i < flen; i++) {
					id = friends[i].id;
					if (id !== owner &&  friends[i].subscription.indexOf('BIS') === 0 && (friends[i].activeStatus || !friends[i].waitStatus || friends[i].businessStatusInactive === true)) {
						//input.options[input.options.length] = new Option(Project.getFriendName(id, 2, true), id);
						// tmp = dojo.clone(Project.getFriend(id));
						tmp = Project.getFriend(id);
						//Must have a name attribute in order for store to return something the combobox can render
						if (!tmp.name || tmp.name.length <= 0) {
							tmp.name = tmp.firstName + ' ' + tmp.lastName;
						}
						var level = Hi_Actions.getPermissionByUserId(pms, tmp.id);
						s.put({
							name: tmp.name,
							id: tmp.id,
							image16src: dojo.global.getAvatar(32, tmp.id),
							permission: pms && level || default_permission,
							selected: pms && (level !== null)
						});
					}
					
				}
			} else if (!Project.email_confirmed && pms) {
				// #5606: since we allow unconfirmed users to edit we need to fill sharing widget with userXXX items.
				for (i = 0; i < pms.length; i++) {
					var id = pms[i].principal,
						level = pms[i].level;

					if (!isNaN(parseInt(id)) && id != owner && id != Project.user_id && dojo.global.userEmailConfirmed(id)) {
						s.put({
							name: Project.getFriendName(id) || ('member' + id),
							id: id,
							image16src: dojo.global.getAvatar(16, id),
							permission: level || default_permission,
							selected: level !== null
						});
					}
				}
			}

			dd.set('store', s);
		}
	});
	
	return hiSharing;
});