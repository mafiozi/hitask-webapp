//dojo.provide("hi.project.validate");
define(function () {
			
	return dojo.global.Hi_Validate = {
		ptitle: function(data) {
			data.id = parseInt(data.id);
			if (!isFinite(data.id) || data.id <= 0) {
				return false;
			}
			data.title += '';
			data.title = data.title.trim();
			if (data.title == '' || data.title == hi.i18n.getLocalization('project.enter_item_name')) {
				return false;
			}
			return true;
		}
	};

});