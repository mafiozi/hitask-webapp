/*
 * This file is used only in /item section
 */
define([
	
	"dijit/form/DateTextBox",
	"dijit/form/TimeTextBox",
	"dijit/form/Button",

	"hi/dnd",

	"hi/sortable",

	"hi/item/item",

	"hi/store-item",
	

	"hi/preferences",

	//"hi/search",
	"hi/comments",
	"hi/upload",
	"hi/participant",
	"hi/tag",

	"dojox/layout/ResizeHandle",
	"dojox/layout/ContentPane",
	
	//Scripts
	"hi/main/completed",
	"hi/main/tasks",
	"hi/main/projects",
	//"hi/main/archive",
	//"hi/main/team",
	"hi/timetracking",
	"hi/main/helper",
	"hi/main/textile",
	
	"hi/main/form/properties",
	"hi/main/form/color",
	
	"hi/main/header",
	"hi/main/logout",
	'hi/widget/image/PreviewLightbox',
	"hi/widget/form/hiSharing/hiSharingWidget"

], function () {

	return {};
	
});