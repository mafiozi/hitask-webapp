define([
	"dojo/_base/array", // array.forEach array.map
	"dojo/_base/declare", // declare
	"dojo/cldr/supplemental", // cldrSupplemental.getFirstDayOfWeek
	"dojo/date", // date
	"dojo/on",
	"dojo/date/locale",
	"dojo/dom", // dom.setSelectable
	"dojo/dom-class", // domClass.contains
	"dojo/_base/event", // event.stop
	"dojo/_base/lang", // lang.getObject, lang.hitch
	"dojo/_base/sniff", // has("ie") has("webkit")
	"dojo/string", // string.substitute
	"dojo/_base/window", // win.doc.createTextNode
	"dijit/_Widget",
	"dijit/_Templated",
	"dojo/text!./item.html",
	"dojo/NodeList-traverse"
], function(array, declare, cldrSupplemental, date, on, local, dom, domClass, event, lang, has, string, win,
			_WidgetBase, _TemplatedMixin, template){
	
	// module:
	//		hi/widget/Item
	// summary:
	//		Task item which updates itself based on attributes
	
	var TaskItem = declare("hi.widget.TaskItem", [_WidgetBase, _TemplatedMixin], {
		// summary:
		//		Task item which updates itself based on attributes
		
		// Template for the list
		templateString: template,
		
		// List of all data properties
		_properties: [
			"id", "guid",
			"user_id", "shared", "my",
			"title",
			"category",
			"message",
			"parent",
			"time_last_update",
			"time_create",
			"priority",
			"time_spent",
			"time_track", "time_est",
			"changed",
			"starred",
			"tags",
			"assignee",
			"start_date", "start_time", "end_date", "end_time", "is_all_day", "next_event_date",
			"children",
			"recurring",
			"reminder_enabled", "reminder_time", "reminder_time_type",
			"color",
			"completed",
			"participants"
		],
		
		_typeClassNames: {
			1: "tasks",
			2: "event",
			3: "event",
			4: "note",
			5: "file"
		},
		
		// TaskList to which this item belongs to
		list: null,
		
		// Task name sufix for hi.items.manager
		listName: null,
		
		// TaskList or TaskItem this item is child of
		container: null,
		
		// Remove task when collapsed
		removeOnCollapse: false,
		
		
		/* ----------------------------- ATTRIBUTES ------------------------------ */
		
		/* Locale */
		localeClickToView: hi.i18n.getLocalization("project.click_to_view"),
		localeClickToComplete: hi.i18n.getLocalization("project.click_to_complete"),
		
		
		/* Tag */
		tags: [],
		
		/* Task ID, widgets reserve id attribute */
		taskId: null,
		_setTaskIdAttr: function (taskId) {
			//If no taskId given, then add some fake one
			taskId = taskId || --TaskItem.ID_COUNTER;
			
			if (this.taskId) {
				hi.items.manager.removeItem(this);
				/* Bug #2092,#2095 hi.items.manager.removeItem(this.taskId);*/
			}
			if (taskId) {
				hi.items.manager.addItem(taskId || this.id, this, this.listName || (this.recurring ? this.get("eventdate") : null));
			}
			
			this.childrenNode.setAttribute("data-task-id", taskId || "");
			this.itemNode.setAttribute("data-task-id", taskId || "");
			this.itemNode.taskId = taskId;
			this._set("taskId", taskId);
		},
		
		/* Participants */
		participants: null,
		
		/* Parent */
		parent: 0,
		_setParentAttr: function (value) {
			this._set("parent", value);
			
			var group = Hi_Preferences.get('currentTab', 'my', "string"),
				isSearchActive = Hi_Search.isFormVisible(),
				tagFilter = Hi_Preferences.get("tagfilter", ""),
				parent = this.parent,
				parent_item = parent ? hi.items.manager.get(parent) : null,
				parent_is_project = (parent_item && !parseInt(parent_item.category)),
				parent_title = [],
				project_title = "";
			
			//Parent title
			if (group == 'team' || group == 'date' || (!isSearchActive && tagFilter)) {
				if (parent_item && !parent_is_project) {
					//If parent is not project
					parent_title = parent_item.title;
				
					setTextContent(this.parentTitleNode, parent_title);
					dojo.addClass(this.parentTitleNode, "parent_title");
					dojo.removeClass(this.parentTitleNode, "hidden");
				}
			}
			
			if (!parent_title.length) {
				dojo.removeClass(this.parentTitleNode, "parent_title");
				dojo.addClass(this.parentTitleNode, "hidden");
			}
			
			//Project title
			if (parent && parent_is_project && group != 'project') {
				project_title = parent_item.title;
				if (project_title) {
					setTextContent(this.projectTitleNode, " " + project_title);
					dojo.removeClass(this.projectTitleNode, "hidden");
				}
			}
			
			if (!project_title) {
				dojo.addClass(this.projectTitleNode, "hidden");
			}
		},
		
		/* Type*/
		category: 1,
		_setCategoryAttr: function (category) {
			this._set("category", category);
			this.set("typeClass", this._typeClassNames[parseInt(category)] || "");
		},
		
		typeClass: "tasks",
		_setTypeClassAttr: function (typeClass) {
			dojo.removeClass(this.headingNode, this.typeClass);
			dojo.addClass(this.headingNode, typeClass);
			this._set("typeClass", typeClass);
		},
		
		
		/* Priority */
		priority: 20000,
		_setPriorityAttr: function (value) {
			value = value || 20000;
			var hiddenClass = "hidden";
			dojo.addClass(this.priorityHighNode, hiddenClass);
			dojo.addClass(this.priorityLowNode, hiddenClass);
			
			if (value >= 30000) {
				dojo.removeClass(this.priorityHighNode, hiddenClass);
			} else if (value < 20000) {
				dojo.removeClass(this.priorityLowNode, hiddenClass);
			}
			
			this._set("priority", value);
		},
		
		/* Title */
		title: "",
		_setTitleAttr: function (title) {
			this._set("title", title);
			
			title = htmlspecialchars(title);
			this.titleNode.innerHTML = title;
		},
		
		
		/* Starred */
		starred: false,
		_setStarredAttr: function (value) {
			this._set("starred", value);
			this.set("starredClass", value ? "star-selected" : "");
		},
			
		starredClass: "",
		_setStarredClassAttr: function (starredClass) {
			dojo.removeClass(this.starredNode, this.starredClass);
			dojo.addClass(this.starredNode, starredClass);
			this._set("starredClass", starredClass);
		},
		
		
		/* Event date */
		eventdate: "",
		_getEventdateAttr: function () {
			if (this.recurring) {
				if (this.eventdate) {
					return this.eventdate;
				} else if (this.instances) {
					var instances = this.instances,
						i = 0, ii = instances.length;
					
					for (; i<ii; i++) {
						if (instances[i].status != 2) { // Not deleted
							this.set("eventdate", instances[i].start_date);
							return instances[i].start_date;
						}
					}
				}
			}
			return null;
		},
		_setEventdateAttr: function (eventdate) {
			this.completeNode.setAttribute("eventdate", eventdate);
			this._set("eventdate", eventdate);
		},
		
		
		/* Completed */
		completed: false,
		_setCompletedAttr: function (value) {
			if (parseInt(this.recurring) > 0) {
				//For recurring even check if instance is completed
				value = hi.items.manager.getIsCompleted(this.taskId, this.eventdate);
			}
			
			this._set("completed", value);
			
			dojo.toggleClass(this.itemNode, "completed", value);
			this.completeNode.checked = !!value;
			
			// Drag and drop
			if (value && (!this.container || !(this.container instanceof hi.widget.TaskItem && !this.container.completed))) {
				dojo.removeClass(this.itemNode, "dojoDndItem");
			} else {
				dojo.addClass(this.itemNode, "dojoDndItem");
			}
		},
		
		
		/* Admin classname */
		adminClass: Project.isBusinessAdministrator() ? "admin" : "",
		_setAdminClassAttr: function (adminClass) {
			dojo.removeClass(this.itemNode, this.adminClass);
			dojo.addClass(this.itemNode, adminClass);
			this._set("adminClass", adminClass);
		},
		
		
		/* My */
		my: true,
		
		/* Shared classname */
		shared: false,
		_setSharedAttr: function (shared) {
			// #2205
			if (typeof(shared) != 'boolean') {
				shared = parseInt(shared);
				shared = !isNaN(shared) && shared > 0 ? true : false;
			}
			
			/* Bug #1956 shared = parseInt(shared, 10) || 0;*/
			this._set("shared", shared);
			
			if (shared || (Project.account_level < Project.ACCOUNT_LEVEL_BUSINESS && !Project.businessId)) {
				/* Bug #1956
				dojo.addClass(this.itemNode, "shared");
				dojo.removeClass(this.itemNode, "private");
				*/
			} /*Bug #1956 else if (Project.account_level < Project.ACCOUNT_LEVEL_BUSINESS && !Project.businessId) {
				dojo.removeClass(this.itemNode, "shared");
				dojo.addClass(this.itemNode, "private");
			} */
		},
		
		sharedClass: "",
		_setSharedClassAttr: function (sharedClass) {
			dojo.removeClass(this.itemNode, this.sharedClass);
			dojo.addClass(this.itemNode, sharedClass);
			this._set("sharedClass", sharedClass);
		},
		
		
		/* User id*/
		user_id: "",
		_setUser_idAttr: function (user_id) {
			this._set("user_id", user_id);
			this.set("ownedClass", user_id == Project.user_id ? "owner" : "");
			
			if (user_id == Project.user_id) this.set("my", true);
		},
		
		ownedClass: "",
		_setOwnedClassAttr: function (ownedClass) {
			dojo.removeClass(this.itemNode, this.ownedClass);
			dojo.addClass(this.itemNode, ownedClass);
			this._set("sharedClass", ownedClass);
		},
		
		/* Business */
		businessClass: (Project.businessId ? "business" : ""),
		_setBusinessClassAttr: function (businessClass) {
			dojo.removeClass(this.itemNode, this.businessClass);
			dojo.addClass(this.itemNode, businessClass);
			this._set("businessClass", businessClass);
		},
		
		
		/* Color */
		color: "",
		_setColorAttr: function (color) {
			this._set("color", color);
			this.set("colorClass", color ? "color" + color : "");
		},
		
		colorClass: "",
		_setColorClassAttr: function (colorClass) {
			dojo.removeClass(this.completeClassNode, this.colorClass);
			dojo.addClass(this.completeClassNode, colorClass);
			this._set("colorClass", colorClass);
		},
		
		
		/* Children */
		childrenRendered: false,
		itemWidgets: null,
		
		children: 0,
		_setChildrenAttr: function (children) {
			children = parseInt(children, 10) || 0;
			this._set("children", children);
			this.set("childrenClass", children && this.hasNonFileChildren() ? "has-children" : "");
		},
		
		childrenClass: "",
		_setChildrenClassAttr: function (childrenClass) {
			dojo.removeClass(this.itemNode, this.childrenClass);
			dojo.addClass(this.itemNode, childrenClass);
			this._set("childrenClass", childrenClass);
		},
		
		
		/* Dragable */
		dragVisibleClass: "hidden",
		_setDragVisibleClassAttr: function (dragVisibleClass) {
			dojo.removeClass(this.dragHandle, this.dragVisibleClass);
			dojo.addClass(this.dragHandle, dragVisibleClass);
			this._set("dragVisibleClass", dragVisibleClass);
		},
		
		
		/* Date */
		end_date: "",
		end_time: "",
		start_date: "",
		start_time: "",
		
		_setEnd_dateAttr: function (end_date) {
			/* Bug #2132 - the UTC format is dddd-dd-ddTdd:dd:dd.ddd-dd:dd, where d is a digit
			if (end_date && !end_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[\d\+:]+$/)) {
			*/
			if (end_date && !end_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}-[0-9]{2}:[0-9]{2}$/)) {
				// #2180
				//var time = this.get("end_time");
				//if (this.is_all_day || !time) {
				//	end_date = time2strAPI(HiTaskCalendar.getDateFromStr(end_date));
				//} else {
					end_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(end_date, time2str(str2timeAPI(end_date), 'h:i')));
				//}
			}
			
			this._set("end_date", end_date);
			this.set("dateTitle", Math.random());
		},
		_setEnd_timeAttr: function (end_time) {
			var end_date = this.get("end_date");
			if (end_date) {
				end_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(end_date, end_time));
			} else {
				end_date = time2strAPI(HiTaskCalendar.getTimeFromStr(end_time));
			}
			
			this.set("end_date", end_date);
			this._set("end_time", end_time);
		},
		_setStart_dateAttr: function (start_date) {
			/* Bug #2132 - the UTC format is dddd-dd-ddTdd:dd:dd.ddd-dd:dd, where d is a digit
			 * 
			if (start_date && !start_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[\d\+:]+$/)) {
			*/
			if (start_date && !start_date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}-[0-9]{2}:[0-9]{2}$/)) {
				// #2180
				//var time = this.get("start_time");
				//if (this.is_all_day || !time) {
				//	start_date = time2strAPI(HiTaskCalendar.getDateFromStr(start_date));
				//} else {
					start_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(start_date, time2str(str2timeAPI(start_date), 'h:i')));
				//}
			}
			
			this._set("start_date", start_date);
			this.set("dateTitle", Math.random());
		},
		_setStart_timeAttr: function (start_time) {
			var start_date = this.get("start_date");
			if (start_date) {
				start_date = time2strAPI(HiTaskCalendar.getDateTimeFromStr(start_date, start_time));
			} else {
				start_date = time2strAPI(HiTaskCalendar.getTimeFromStr(start_time));
			}
			
			this.set("start_date", start_date);
			this._set("start_time", start_time);
		},
		
		_getEnd_dateAttr: function () {
			var value = this.end_date;
			if (value) value = time2str(str2time(value, "y-m-d"), Hi_Calendar.format);
			return value || "";
		},
		_getStart_dateAttr: function () {
			var value = this.start_date;
			if (value) value = time2str(str2time(value, "y-m-d"), Hi_Calendar.format);
			return value || "";
		},
		_getEnd_timeAttr: function () {
			var value = this.end_date;
			
			if (this.is_all_day) return "";
			
			if (value) value = str2timeAPI(this.end_date);
			if (value) value = time2str(value, "h:i");
			return value || "";
		},
		_getStart_timeAttr: function () {
			var value = this.start_date;
			
			if (this.is_all_day) return "";
			
			if (value) value = str2timeAPI(this.start_date);
			if (value) value = time2str(value, "h:i");
			return value || "";
		},
				
		
		dateTitle: "",
		_setDateTitleAttr: function () {
			this._set("dateTitle", "");
			
			var title = "";
			if (this.recurring && this.eventdate) {
				title = getTaskInstanceDateTitle(this.taskId, this.eventdate);
			} else {
				title = getTaskDateTitle(this.taskId, new Date(), this.getData());
			}
			
			setTextContent(this.dateTitleNode, title);
		},
		
		
		/* Reminder */
		reminder_enabled: false,
		_getRreminder_enabledAttr: function () {
			if (this.category == 3) {
				return 1;
			}
			return this.reminder_enabled;
		},
		
		reminder_time: null,
		_getReminder_timeAttr: function () {
			if (this.category == 3) {
				return 0;
			}
			return this.reminder_time;
		},
		
		
		
		reminder_time_type: "m",
		_getReminder_time_typeAttr: function () {
			if (this.category == 3) {
				return "m";
			}
			return this.reminder_time_type;
		},
		
		/* Assignee */
		assignee: 0,
		_setAssigneeAttr: function (value) {
			this._set("assignee", value);
			
			var person_name = null;
			
			if (value) {
				var isChild = Project.isTaskChild(this.taskId),
					group = Hi_Preferences.get('currentTab', 'my', "string");
					
				if (group != 'team' || isChild) {
					person_name = Project.getFriendName(value);
				}
			}
			
			if (person_name) {
				dojo.removeClass(this.personNode, "hidden");
				setTextContent(this.personNode, person_name);
			} else {
				dojo.addClass(this.personNode, "hidden");
			}
			
		},
		
		/* Children expanded */
		expanded: false,
		_setExpandedAttr: function (expanded) {
			dojo.toggleClass(this.domNode, "expanded", expanded);
			this._set("expanded", expanded);
			
			if (!expanded) {
				//Close opened sub task
				var item = this.has(hi.items.manager.opened());
				if (item) {
					if (item.editing) {
						item.stopEditing();
					} else {
						item.collapse();
					}
				}
			}
		},
		
		/* Opened, task expanded view */
		opened: false,
		_setOpenedAttr: function (opened) {
			//Don't open if some other item is being edited
			/* Bug #2126 - only block if another existing item (taskId>0) is in edit mode
			 * if (opened && hi.items.manager.editing()) return; 
			 */
			if (opened && hi.items.manager.editing() && hi.items.manager.editing().taskId > 0) return;
			dojo.toggleClass(this.domNode, "highlight", opened);
			this._set("opened", opened);
			if (opened) {
				this.renderExpandedView();
				
				//Close all other tasks
				hi.items.manager.collapseOpenedItems();
				
				hi.items.manager.opened(this.taskId || this.id, true); // TaskId will be empty for new item
				this.propertiesView.expand();
			} else {
				this.propertiesView.collapse();
				hi.items.manager.opened(this.taskId || this.id, false); // TaskId will be empty for new item
			}
		},
		
		/* Editing */
		editing: false,
		_setEditingAttr: function (editing) {
			this._set("editing", editing);
			
			dojo.toggleClass(this.domNode, "highlight-edit", editing);
			dojo.toggleClass(this.headingNode, "hidden", editing);
			
			if (editing) {
				this.renderEditingView();
				
				hi.items.manager.editing(this.taskId || this.id, true); // TaskId will be empty for new item
				
				if (this.propertiesView) {
					//Hide properties view
					this.propertiesView.collapse(true);
				}
				
				this.editingView.expand();
			} else {
				this.editingView.collapse();
				
				if (this.propertiesView) {
					this.propertiesView.expand(true);
				}
				
				hi.items.manager.editing(this.taskId || this.id, false); // TaskId will be empty for new item
			}
		},
		
		/* Recurring event instance dates */
		instances: null,
		_setInstancesAttr: function (instances) {
			this._set("instances", instances);
			
			if (instances && instances.length) {
				if (this.recurring != 0) {
					if (!this.eventdate) {
						//Find first not deleted item
						for (var i=0,ii=instances.length; i<ii; i++) {
							if (instances[i].status != 2) {
								this.set("eventdate", instances[i].start_date);
								break;
							}
						}
					}
					
					var completed = hi.items.manager.getIsCompleted(this.taskId, this.eventdate);
					
					if (this.completed && !completed) {
						this.set("completed", false);
					} else if (!this.completed && completed) {
						this.set("completed", true);
					}
				} else {
					if (this.eventdate) {
						this.set("eventdate", null);
					}
				}
			}
		},
		
		/* Overdue / current */
		styleOverdue: false,
		styleCurrent: false,
		_setStyleOverdueAttr: function (overdue) {
			this._set("styleOverdue", overdue);
			dojo.toggleClass(this.domNode, "overdue", overdue);
		},
		_setStyleCurrentAttr: function (current) {
			this._set("styleCurrent", current);
			dojo.toggleClass(this.domNode, "current", current);
		},
		
		checkOverdue: function () {
			// summary:
			//		Check if task is overdue and adjust style
			
			var id = this.taskId,
				is_overdue = false,
				is_current = Hi_Overdue.isActive(id);
			
			if (Hi_Overdue.tillActive(id) <= 0) {
				var overdueTime = Hi_Overdue.overdueTime(id);
				if (overdueTime < 0) {
					// 2538: so if date is today and start_time is 1 hour in the past it's not overdue.
					//console.log("HERE0",[id,hi.items.manager.get(id).title]);
					//console.log("HERE",[id,Hi_Overdue.nextEvent(id),Hi_Overdue.nextEvent(id).length]);
					/* #2538 - Hi_Overdue.nextEvent is ill-defined for non-recurring items? - check its validity before using */
					var nextOverDueEvent = Hi_Overdue.nextEvent(id);
					var millisFromMidnight;
					if (nextOverDueEvent && nextOverDueEvent.length && nextOverDueEvent[1])
					{
						millisFromMidnight = Hi_Overdue.nextEvent(id)[1] - HiTaskCalendar.todayMidnight;
						
						if (Math.abs(overdueTime) > millisFromMidnight) {
							is_overdue = true;
						}						
					}

				}
			}
			
			if (is_overdue) {
				this.set("styleOverdue", true);
				this.set("styleCurrent", false);
			} else if (is_current) {
				this.set("styleOverdue", false);
				this.set("styleCurrent", true);
			} else {
				this.set("styleOverdue", false);
				this.set("styleCurrent", false);
			}
		},
		
		
		/* ----------------------------- CONSTRUCTOR ------------------------------ */
		
		
		constructor: function (/*Object*/args) {
			this.itemWidgets = {};
		},
		
		uninitialize: function () {
			if (this.editing) {
				/* Bug #2151 - silently validate an item in opened state before trying to save */
				/* Bug #2315 - moved to dojo.global.initiateTab
				if (this.editingView.validate(this.editingView.getFormValues(),true))
				{
					this.editingView.save();
				}
				*/
			}
			
			if (this.taskId) {
				hi.items.manager.removeItem(this);
			}
		},
		
		buildRendering: function () {
			this.inherited(arguments);
			
			this.tags = this.tags || [];
			this.own(on(this.propertiesViewNode, 'keydown', lang.hitch(this, 'handleKeyPress')));
			//Drag visible class
			var isChild = this.isChild(),
				sorting = Project.getSorting(),
				group = Hi_Preferences.get('currentTab', 'my', "string");
			
			if ((!isChild && group == 'date') || sorting != Project.ORDER_PRIORITY) {
				//this.set("dragVisibleClass", "hidden"); // already default
			} else {
				this.set("dragVisibleClass", "");
			}
			
			//Children count
			this.set("children", this.list ? this.list.getChildrenData(this.taskId).length : 0);
			
			//Task is overdue or current depends on multiple attributes
			this.watchMultiple(["category", "completed", "recurring", "start_date", "start_time", "end_date", "end_time"], lang.hitch(this, this.checkOverdue));
			this.checkOverdue();
			
			
			this.buildDragAndDrop();
		},
		
		buildDragAndDrop: function () {
			// summary:
			//		Attach drag and drop
			
			this.dragSource = new dojo.dnd.HiDragSource(this.childrenNode, {});
		},
		
		renderChildren: function () {
			// summary:
			//		Render all children
			
			if (!this.childrenRendered) {
				this.childrenRendered = true;
				
				var items = hi.items.manager.get(),
					i = 0,
					ii = items.length,
					id = this.get("taskId"),
					list = this.list;
				
				if (list) {
					for (; i<ii; i++) {
						if (items[i].parent == id && list.filterFilters(items[i])) {
							if (items[i].category != TaskItem.CATEGORY_FILE) {
								this.renderChild(items[i]);
							} else {
								//Reload file list
								if (this.get("opened") || this.get("expanded")) {
									FileList.load(this.taskId);
								}
							}
						}
					}
				}
				
				if (this.dragSource) {
					this.dragSource.sync();
				}
			}
		},
		
		renderChild: function (item) {
			// summary:
			//		Render childre task
			// item:
			//		Children item data
			
			if (item.recurring != 0) {
				item = dojo.mixin({}, item);
				item.completed = hi.items.manager.getIsCompleted(item);
			}
			
			// Create TaskItem
			this.itemWidgets[item.id] = new TaskItem(dojo.mixin({
				"taskId": item.id, // id is reserved by dojo widget
				"list": this.list,
				"container": this
			}, item, {
				"id": null
			}));
			/* #2293 */
			//this.itemWidgets[item.id].placeAt(this.childrenNode);
			this._add(this.itemWidgets[item.id],item,this.childrenNode);
			return this.itemWidgets[item.id];
		},
		
		renderExpandedView: function () {
			// summary:
			//		Render expanded view widget
			
			if (this.propertiesView) return;
			
			var view = this.propertiesView = new hi.widget.TaskItemProperties({
				"task": this
			});
			
			view.placeAt(this.propertiesViewNode);
			view.startup();
			
			this._supportingWidgets.push(view);
		},
		
		renderEditingView: function () {
			// summary:
			//		Render editing view widget
			
			if (this.editingView) return;
			
			var view = this.editingView = new hi.widget.TaskItemEdit({
				"task": this
			});
			
			view.placeAt(this.propertiesViewNode);
			view.startup();
			
			this._supportingWidgets.push(view);
		},
		
		
		/* ----------------------------- DATA API ------------------------------ */
		
		
		getData: function () {
			// summary:
			//		Returns task data
			
			var props = this._properties,
				prop = null,
				i = 0,
				ii = props.length,
				data = {};
			
			for (i = 0,ii = props.length; i<ii; i++) {
				prop = props[i];
				if (prop == "id") {
					data[prop] = this.get("taskId");
				} else {
					data[prop] = this.get(prop);
				}
			}
			
			return data;
		},
		
		getRawData: function () {
			// summary:
			//		Returns task data
			
			var data = hi.items.manager.get(this.taskId);
			if (data) return data;

			data = {};
			// As fallback collect data from attributes
			var props = this._properties,
				prop = null,
				i = 0,
				ii = props.length;
			
			for (i = 0,ii = props.length; i < ii; i++) {
				prop = props[i];
				if (prop == "id") {
					data[prop] = thi.taskId;
				} else {
					data[prop] = this[prop];
				}
			}
			
			delete(data.start_time);
			delete(data.end_time);
			
			return data;
		},
		
		save: function (properties) {
			// summary:
			//		Save item properties, write to store
			// properties: {Object}
			//		Properties which will be written
			var self = this;
			var deferred = hi.items.manager.setSave(this.taskId, properties);
			deferred.then(function() {
				/* Bug #2042 - redraw calendar and toggle completed class on taskedit */
				HiTaskCalendar.update();
				if (!self.itemNode) return;
				if (properties.completed) {
					dojo.addClass(self.itemNode,'completed');
				} else {
					dojo.removeClass(self.itemNode,'completed');
				}
			});
			return deferred;
		},
		
		
		/* ----------------------------- CHILDREN API ------------------------------ */
		
		
		hasNonFileChildren: function () {
			// summary:
			//		Return true if task has non file children
			
			var items = hi.items.manager.get({"parent": this.taskId}),
				i = 0,
				ii = items.length;
			
			for (; i<ii; i++) {
				if (items[i].category != TaskItem.CATEGORY_FILE) {
					if (this.list && this.list.filterMatches(items[i])) {
						return true;
					}
				}
			}
			
			return false;
		},
		
		toggleChildren: function () {
			// summary:
			//		Toggle children visibility
			
			if (this.hasNonFileChildren()) {
				this.set("expanded", !this.expanded);
			}
			
			if (!this.childrenRendered) {
				this.renderChildren();
			}
		},
		
		expandChildren: function () {
			// summary:
			//		Expand children list
			
			if (this.hasNonFileChildren()) {
				this.set("expanded", true);
			}
			
			if (!this.childrenRendered) {
				this.renderChildren();
			}
		},
		
		isChild: function () {
			// summary:
			//		Returns true if this task is another tasks child (by data)
			
			var parent = this.get("parent"),
				item = null;
			
			if (parent) {
				item = hi.items.manager.get(parent);
				if (item && item.category != TaskItem.CATEGORY_PROJECT) {
					
					if (this.container && this.container instanceof hi.widget.TaskList) {
						return false;
					} else {
						return true;
					}
					
				}
			}
			
			return false;
		},
		
		isTaskItemChild: function () {
			// summary:
			//		Returns true if is TaskItem child
			
			return this.list && this.container && this.list === this.container;
		},
		
		remove: function (/*Object*/item, /*Number*/index, /*Object*/diff) {
			// summary:
			//		Remove item from the children list
			// item:
			//		Item which to remove
			// index:
			//		Index from where item was removed
			// diff:
			//		previous item values
			
			if (!this.childrenRendered) {
				this.set("children", this.list ? this.list.getChildrenData(this.taskId).length : 0);
				return false;
			}
			
			var widget = this.itemWidgets[item.id];
			if (widget) {
				widget.destroy();
				delete(this.itemWidgets[item.id]);
				
				//Update count
				this.set("children", this.children - 1);
			}
		},

		/* #2293 */
		_doCompare:function(toAddWidget,addAt,sortOptions,index,debug)
		{
			var addAtItem = this.itemWidgets[addAt.taskId],
				toAddItem = this.itemWidgets[toAddWidget.taskId],
				SOLength = sortOptions.length,
				addAtValue,
				toAddValue;
			
			if (typeof(sortOptions) == 'function') {
				return sortOptions(addAtItem, toAddItem) == -1;
			} else {
				for (var i = 0;i<SOLength;i++)
				{
					addAtValue = addAtItem[sortOptions[i].attribute];
					toAddValue = toAddItem[sortOptions[i].attribute];
					
					if (addAtValue != toAddValue)
					{
						if (!addAtValue) return false;
						if (!toAddValue) return true;
						if (sortOptions[i].descending)
						{
							if (toAddValue < addAtValue) return true;
							else return false;
						} else
						{
							if (toAddValue > addAtValue) return true;
							else return false;
						}
					}
				}
			}
			return true;
		},
		
		/* #2293 */
		_add:function(widget,item,referenceDomNode)
		{
			//var parentItem = widget.container;
			var sortAttribute = 'title',
				sortOrder = 'Ascending',
				insertAt = 0,
				sortConfiguration = hi.store.Project.getOrderConfiguration(true)[0],
				sortOptions = hi.store.Project.getOrderConfiguration(),
				childrenNodes = this.childrenNode.childNodes,
				end = childrenNodes.length,
				nChildren = childrenNodes.length;
				
			sortAttribute = sortConfiguration.attribute;

			if (sortConfiguration.descending) sortOrder = 'Descending';

			while (insertAt<end) {
				if (this._doCompare(widget,childrenNodes[insertAt],sortOptions,insertAt)) {
					insertAt++;
				}else{
					break;
				}
			}
			dojo.place(widget.domNode,referenceDomNode,insertAt);
		},

		add: function (/*Object*/item, /*Number*/index) {
			// summary:
			//		Add item to the children list
			// item:
			//		Item to add
			// index:
			//		Index where to insert item
			
			if (item.parent == this.taskId && item.category == TaskItem.CATEGORY_FILE) {
				if (this.get("opened") || this.get("expanded")) {
					FileList.load(this.taskId);
				}
			}
			
			if (!this.childrenRendered) {
				this.set("expanded", false);
				this.set("children", this.list ? this.list.getChildrenData(this.taskId).length : 0);
				return false;
			}
			
			if (!(item.id in this.itemWidgets)) {
				// Update count
				this.set("children", this.children + 1);
			}
			
			if (item.parent == this.taskId && item.category != TaskItem.CATEGORY_FILE) {
				// Create TaskItem
				var widget = this.renderChild(item);
				
				// Update drag and drop
				if (this.dragSource) {
					this.dragSource.sync();
				}
				
				return widget;
			}
		},
		
		has: function (/*Number*/id, /*Boolean*/shallow) {
			// summary:
			//		Checks if has child, return true if there is or false if there isn't
			// id:
			//		Task id
			// shallow:
			//		Check only direct children
			
			if (!this.childrenRendered) return false;
			
			if (id in this.itemWidgets) {
				return this.itemWidgets[id];
			} else if (!shallow) {
				var items = this.itemWidgets,
					item = null,
					key = null;
				
				for (key in items) {
					item = items[key].has(id, false);
					if (item) return item;
				}
			}
			return false;
		},
		
		
		/* ----------------------------- COLLAPSE/EXPAND API ------------------------------ */
		
		
		openEdit: function (name) {
			// summary:
			//		Open edit view
			
			if (Project.checkLimits("TRIAL_TASKS") && this.opened && !this.editing) {
				this.set("editing", true);
				/* #2602 */
				//this.editingView.focusInput(name);return;
				var self = this;
				var myInterval = setInterval(function() {
					if (self.editingView._started) {
						self.editingView.focusInput(name);
						clearInterval(myInterval);
					}
				}, 10);
			}
		},
		
		closeEdit: function () {
			// summary:
			//		Close edit and expanded views
			
			if (this.opened && this.editing) {
				this.set("editing", false);
				this.set("opened", false);
			}
		},
		
		collapse: function () {
			// summary:
			//		Collapse task
			
			if (this.editing) {
				this.set("editing", false);
			}
			if (this.opened) {
				this.set("opened", false);
			}
			if (this.dragHandle) {
				dojo.removeClass(this.dragHandle, 'hidden');
			}
		},
		
		expand: function () {
			// summary:
			//		Expand task
			
			if (!this.opened && Project.checkLimits("TRIAL_TASKS")) {
				dojo.addClass(this.dragHandle, 'hidden');
				this.set("opened", true);
			}
		},
		
		
		/* ----------------------------- API ------------------------------ */
		
		animate: function (/*String*/effect, /*Boolean*/reverse, /*Function*/after) {
			// summary:
			//		Animate item
			// effect:
			//		Effect name: "fadeup", "fadedown", "puff"
			// reverse:
			//		Effect direction reversed or not
			// after:
			//		Callback function when animation finishes
			
			var callback = function () {
				if (dojo.isFunction(after)) return after();
			};
			
			//Effects were disabled in IE, because of performance issues
			if (effect == "none" || (dojo.isIE && dojo.isIE < 9)) return callback();
			
			var callbackHide = function () {
				fakeNode.parentNode.removeChild(fakeNode);
				
				//If node shouldn't be visible it will be removed in callback
				node.style.visibility = "visible";
				
				callback();
			};
			
			var node = this.domNode,
				pos = dojo.position(node, true),
				fakeNode = null,
				
				opacityTo = 0,
				opacityFrom = 1,
				
				yFrom = ~~(pos.y),
				yTo = ~~(pos.y);
			
			fakeNode = dojo.create("div", {
				"class": "fake_task" + (dojo.hasClass(node, "highlight") ? " fake_task_highlight" : ""),
				"innerHTML": "<span>" + this.get("title") + "</span>",
				"style": {
					"left": ~~(pos.x) + "px",
					"top": ~~(pos.y) + "px",
					"width": node.offsetWidth - 2 + "px",
					"height": node.offsetHeight - 4 + "px"
				}
			}, dojo.body());
			
			//Effect
			var effectDir = ~~(node.offsetHeight * 1.4);
			
			if (effect == "fadeup" || effect == "copyup") {
				yTo -= effectDir;
			} else if (effect == "fadedown" || effect == "copydown") {
				yTo += effectDir;
			}
			
			if (reverse) {
				var tmp = yTo;
				yTo = yFrom;
				yFrom = tmp;
				
				opacityTo = 1;
				opacityFrom = 0;
			}
			
			if (effect == "fadeup" || effect == "fadedown" || effect == "puff") {
				node.style.visibility = "hidden";
			}
			
			if (effect == "fadeup" || effect == "fadedown" || effect == "copyup" || effect == "copydown") {
				dojo.anim(fakeNode, {
					top: {start: yFrom, end: yTo},
					height: {end: 28}
				}, 650);
			} else if (effect == "puff") {
				var scaleFactor = 20;
				dojo.anim(fakeNode, {
					top: {end: ~~(pos.y-scaleFactor)},
					left: {end: ~~(pos.x-scaleFactor)},
					width: {end: node.offsetWidth - 2 + scaleFactor * 2},
					height: {end: node.offsetHeight - 4 + scaleFactor * 2}
				}, 650);
			}
			
			dojo.anim(fakeNode, {
				opacity: {start: opacityFrom, end: opacityTo}
			}, (reverse ? 650 : 325), null, callbackHide, (reverse ? 0 : 325));
			
		},
		
		highlight: function () {
			// summary:
			//		Animate element highlight
			
			//Don't highlight if task is opened
			if (this.opened) return;
			
			var heading = this.query("div.heading", true),
				from = [255,252,159],
				to = [245,245,245],
				diff = [to[0] - from[0],to[1] - from[1],to[2] - from[2]];
		
			heading.style.background = '#FFFC9F';
			
			dojo.anim(heading, {}, 1000, function (n) { 
			var c = [~~(from[0]+diff[0]*n),~~(from[1]+diff[1]*n),~~(from[2]+diff[2]*n)];
			heading.style.background = 'rgb(' + c.join(',') + ')';
			}, function () {
			if (dojo.isIE && dojo.isIE < 9) {
					heading.removeAttribute('style');
				} else {
					heading.style.background = '';
				}
			}, 1000);
		},
		
		watchMultiple: function (props, callback) {
			// summary:
			//		Attach to multiple attribute changes
			// props: Array
			//		Array of attributes
			// callback: Function
			//		Callback function
			
			for (var i=0, ii=props.length; i<ii; i++) {
				this.watch(props[i], callback);
			}
		},
		
		query: function (selector, first) {
			// summary:
			//		Return elements matching selector or single element
			// selector:String
			//		CSS selector
			// first:Boolean
			//		Return only first match
			
			var result = dojo.query(selector, this.domNode);
			return first ? result[0] || null : result;
		},
		
		next: function () {
			// summary:
			//		Returns next item in the list
			//		Uses DOM to determine which is next
			
			var node = nextElement(this.domNode);
			if (node) {
				return dijit.byNode(node);
			}
			
			return null;
		},
		
		prev: function () {
			// summary:
			//		Returns previous item in the list
			//		Uses DOM to determine which is next
			
			var node = prevElement(this.domNode);
			if (node) {
				return dijit.byNode(node);
			}
			
			return null;
		},
		
		/* ----------------------------- EVENT BINDINGS ------------------------------ */
		
		handleExpandClick: function (e) {
			if (this.opened) {
				//this.openEdit('title'); // open edit for with title input focused
				// #2647
				this.collapse();
			} else {
				this.expand();
			}
		},
		
		handleCollapseClick: function (e) {
			if (this.opened && !this.editing) {
				dojo.stopEvent(e);
				this.collapse();
			}
		},
		
		handleItemClick: function (e) {
			//When document will be clicked collapse items
			console.log('handleItem click',e);
			Click.setClickFunction(dojo.hitch(hi.items.manager, hi.items.manager.collapseOpenedItems), this.domNode, "taskItemManager");
		},
		
		handleClickComplete: function (event) {
			/* #2064 Block click if new sub task is opened */
			var itemID = hi.items.manager.openedId(),
				msg;

			if (itemID<0) {
				var parent = hi.items.manager.get(itemID).parent;
				/* Bug #2094 - climb up the list of parent tasks 
				 * If we find find one; block until user saves the sub task that is in edit mode
				 */
				while (parent) {
					if (parent == this.taskId) {
						var editing = hi.items.manager.opened().editingView;
						if (itemID == editing.taskId) {
							Project.alertElement.showItem(14,{task_alert:'Please save this sub task first'});
							if (event) {
								dojo.stopEvent(event);
							}
							return;
						}
					}
					parent = hi.items.manager.get(parent).parent;
				}	
			}
			if (Hi_Preferences.get('show_confirmation_complete', false, 'bool')) {
				// Show confirmation.
				if (event.target.checked) {
					msg = hi.i18n.getLocalization('tooltip.complete_confirmation');
				} else {
					msg = hi.i18n.getLocalization('tooltip.uncomplete_confirmation');
				}
				
				if (!confirm(msg)) {
					event.target.checked = !event.target.checked;
					dojo.stopEvent(event);
					return;
				}
			}
			/* #1203 - use getIsCompleted to determine completed status, since this handles both cases of recurring and non-recurring items */
			var completed = !hi.items.manager.getIsCompleted(this.taskId,this.eventdate),//!hi.items.manager.get(this.taskId).completed;//!this.completed,
				animation = completed ? "fadedown" : "fadeup",
				properties = {"completed": completed ? 1 : 0},
				eventDateAttr = event && event.target ? dojo.getAttr(event.target, 'eventdate') : null;
			
			eventDateAttr = eventDateAttr && eventDateAttr != 'undefined' ? eventDateAttr : null;
			properties.eventDateAttr = eventDateAttr ? time2strAPI(str2timeAPI(eventDateAttr)) : null;
			/* #2910 */
			properties.recurring = this.get('recurring');
			// #2543
			if (properties.eventDateAttr) {
				animation = 'none';
				// Probably such instance date is not received yet, so create it manually.
				if (this.recurring != 0) {
					var instStatus = hi.items.date.getEventStatus(this.taskId, eventDateAttr);
					if (instStatus === false) {
						this.instances.push({
							start_date: eventDateAttr,
							start_date_time: properties.eventDateAttr,
							status: properties.completed,
							task_id: this.taskId
						});
					} else {
						for (var i = 0; i < this.instances.length; i++) {
							if (this.instances[i].start_date == eventDateAttr) {
								this.instances[i].status = properties.completed;
							}
						}
					}
				} 
			}

			if (Hi_Timer.isTimedTask(this.taskId)) {
				//Stop timer if it is running for this task
				Hi_Timer.stop();
			}
			
			if (this.isChild()) {
				//Childs stay where they are
				this.save(properties);
			} else {
				//Animate
				var animateCallback = lang.hitch(this, function() {
					/* Bug #2042 */
					this.completeNode.checked=properties.completed;
					this.set('complete',this.completed);
					this.save(properties);
					if (animation == 'none') {
						Tooltip.close('taskedit');
					}
				});
				
				if (animation != 'none') {
					this.animate(animation, false, animateCallback);
				} else {
					animateCallback();
				}
			}
			
			if (event) event.stopPropagation();
		},
		
		handleClickStarred: function (event) {
			/* #2064 Block click if new sub task is opened */
			var itemID = hi.items.manager.openedId();
			if (itemID<0)
			{
				var parent;
				if (!hi.items.manager.get(itemID)) parent = null;
				else parent = hi.items.manager.get(itemID).parent;
				/* Bug #2094 - climb up the list of parent tasks 
				 * If we find find one; block until user saves the sub task that is in edit mode
				 */
				while(parent)
				{
					if (parent == this.taskId) 
					{
						var editing = hi.items.manager.opened().editingView;
						if (itemID == editing.taskId)
						{
							Project.alertElement.showItem(14,{task_alert:'Please save this sub task before starring its parent'});
							dojo.stopEvent(event);
							return;
						}		
					}
					parent = hi.items.manager.get(parent).parent;
				}

			}

			
			var starred = !this.starred;
			this.set("starred", starred);
			// #2905: need to set "completed" proeprty, so "afterSave" callback will update widget properly.
			this.save({"starred": starred, completed: this.completed});
			dojo.stopEvent(event);
		},
		
		handleKeyPress: function (event) {
			var keys = dojo.keys,
				key  = event.which || event.keyCode || false,
				el   = event.srcElement || event.target;
			
			event.cancelBubble = true;
			
			if (key == keys.KEY_ENTER && (el.tagName == "TEXTAREA" || (el.tagName == "INPUT" && el.type == "button") || el.tagName == "BUTTON")) {
				return;
			}
			
			if (key == keys.KEY_ESCAPE) {
				if (this.opened) {
					if (this.editing) {
						this.editingView.cancel();
					} else {
						this.collapse();
					}
				}
			} else if (key == keys.KEY_ENTER) {
				if (this.editing && this.opened) {
					this.editingView.save();
				}
			}
		}
	});
	
	TaskItem.ID_COUNTER = -2;
	
	TaskItem.CATEGORY_PROJECT = 0;
	TaskItem.CATEGORY_TASK = 1;
	TaskItem.CATEGORY_EVENT = 2;
	TaskItem.CATEGORY_NOTE = 4;
	TaskItem.CATEGORY_FILE = 5;
	TaskItem.CATEGORY_PROJECT_ARCHIVE_MIRROR = -1;
	
	function setTextContent (node, str) {
		// summary:
		//		Helper function to escape string and set it as HTMLElement content
		// node:HTMLElement
		//		Element
		// str:String
		//		String content
		
		node.innerHTML = "";
		node.appendChild(win.doc.createTextNode(str));
	}
	
	return TaskItem;

});
