define(["dojo/query"], function (domQuery) {
	
	dojo.global.FileUpload = {
		/**
		 * Blank page URL
		 * @type {String}
		 */
		BLANK_URL: document.location.protocol + '\/\/' + document.location.host + "/lib/blank.html",
			
		/**
		 * HTML5 drag and drop api is supported
		 * @type {Boolean}
		 */
		HTML5_SUPPORTED: !!(typeof(FileReader) != 'undefined'),
		
		/**
		 * Unique ID counter
		 * @type {Number}
		 */
		_guid: 1,
		
		/**
		 * FileUploadWidget list by task id
		 * 
		 * @private
		 * @type {Object}
		 */
		_uploaders: {},
		
		/**
		 * FileUploadWidget server quota 
		 * 
		 * @type {Object}
		 */
		serverQuota: {},
		
		/**
		 * File uploading enabled
		 * @type {Boolean}
		 */
		enabled: false,
		
		/**
		 * File listing enabled
		 * @type {Boolean}
		 */
		listEnabled: false,
		
		/**
		 * Destroy file upload widget
		 * @param {Object} taskId
		 */
		destroy: function (taskId) {
			if (taskId in this._uploaders) {
				this._uploaders[taskId].destroy();
				delete(this._uploaders[taskId]);
			}
		},
		
		/**
		 * Destroys all dojo.global.FileUploadWidget type objects.
		destroyAllWidgets: function() {
			for ( var p in this._uploaders) {
				if(this._uploaders.hasOwnProperty(p) && "-1" !== p) {
					this.destroy(p);
				}
			}
		},
		 */
		
		/**
		 * Destroys all dojo.global.FileUploadWidget type objects.
		 */
		destroyAllWidgets: function() {
			for ( var p in this._uploaders) {
				if(this._uploaders.hasOwnProperty(p) && "-1" !== p) {
					this.destroy(p);
				}
			}
		},
		
		/**
		 * Remove uploader without destroying it
		 * @param {Object} taskId
		 */
		remove: function (taskId) {
			if (taskId in this._uploaders) {
				delete(this._uploaders[taskId]);
			}
		},
		
		/**
		 * Returns file uploader widget
		 * @param {Object} taskId
		 */
		get: function (taskId) {
			if (taskId === false) taskId = -1;
			
			if (taskId in this._uploaders) {
				return this._uploaders[taskId];
			} else {
				return null;
			}
		},
		
		/**
		 * Returns file widget
		 * @param {Object} guid Unique file ID
		 */
		getFile: function (guid) {
			if (guid in FileUploadTooltip.files) {
				return FileUploadTooltip.files[guid];
			} else {
				var uploaders = this._uploaders,
					file = null;
				for(var i in uploaders) {
					file = uploaders[i].getFile(guid);
					if (file) return file;
				}
			}
			
			return null;
		},
		
		/**
		 * Load storage information
		 * @param {Object} callback
		 */
		loadStorageInfo: function (callback) {
			Project.ajax('filestorageinfo', {}, function (response) {
				var data = response.getData();
				FileUpload.serverQuota = data;
				callback(data);
			});
		},
		
		/**
		 * Reset / initialize uploader
		 * 
		 * @param {Number} taskId
		 */
		reset: function (taskId, ignoreNew) {
			taskId = parseInt(taskId) || -1;
			
			if (FileUpload.enabled) {
				if (taskId in this._uploaders) {
					var uplNodeContainer = this._uploaders[taskId].nodeContainer;
					var subnodes = domQuery(">", uplNodeContainer);
					if ((uplNodeContainer && subnodes.length) || taskId == -1) { // if it's true then FileUploadWidget is already instantiated
						FileUpload.destroy(taskId);
						FileUpload.reset(taskId, ignoreNew);
					} else {
						this._uploaders[taskId].reset(ignoreNew);
					}
				} else {
					this._uploaders[taskId] = new FileUploadWidget(taskId, ignoreNew);
				}
			}
			
			if (FileUpload.listEnabled && taskId && taskId > 0) {
				FileList.load(taskId);
			}
		},
			
		/**
		 * Open popup to download file
		 * 
		 * @param {Number} id File ID
		 */
		openFile: function (id, callback) {
			if (id) {					
				Project.ajax('filedownloadurl', {id: id}, 
					function(dataResponse) {
						if (dataResponse && !dataResponse.isEmpty() && dataResponse.isStatusOk()) {
							if (typeof(callback) == 'function') {
								callback(dataResponse.getUrl());
							} else {
								//Create popup
								try {
									var popup = dojo.global.open();
									dojo.global.focus();
									popup.document.location = dataResponse.getUrl();
									popup.focus();
								} catch (e) {
									alert('Please allow popups for "' + Project.appBaseHost + '" in your browser settings and try again.');
								}
							}
						} else if (dataResponse && dataResponse.getStatus() == 13) {
							alert('File is not ready yet. Please try again later.');
						} else if (dataResponse && dataResponse.getErrorMessage()) {
							alert(dataResponse.getErrorMessage());
						} else {
							alert('Error occured. Please try again later.');
						}
					},
					function() {
						alert('File is not available. Please try again later.');
					}
				);
			}
		},
			
		/**
		 * Alert user that quota has been exceeded
		 */
		alertUserOverQuota: function () {
			var uploaderMain = FileUpload.get(-1),
				uploaderTask = FileUpload.get(hi.items.manager.openedId());
			
			setTimeout(function () {
				var opened = hi.items.manager.openedId();
				if (opened == -1) {
					uploaderMain.resetAll(opened);
				} else {
					uploaderTask.resetAll(opened);
				}
			}, 0);
			
			FileUploadTooltip.showWhenReady = false;
			FileUploadTooltip.removeAllFiles();
			FileUploadTooltip.hide();
			
			//Show "You have used up all your available file storage" message
			Project.alertElement.showItem(13);
		},
				
		/**
		 * Returns unique ID
		 */
		guid: function () {
			return this._guid++;
		}
		
	};
	
	function checkNewItemExists () {
		var item = hi.items.manager.item(-1);
		if (item && item.editingView) {
			//New item exists
			if (FileUpload.enabled) {
				if (FileUpload.HTML5_SUPPORTED) {
					FileDropArea.init(document.body);
					
					//Create file drop target for 'New Item'
					FileUpload.reset(-1, true);
					var uploader = FileUpload.get(-1);
					if (uploader) uploader.initDnD();
				}
				
				dojo.config.uploaderPath = require.toUrl("dojox/form") + "/resources/uploader.swf";
			}
		} else {
			//Not ready yet
			setTimeout(checkNewItemExists, 50);
		}
	}
	
	setTimeout(checkNewItemExists, 50);
	
	return dojo.global.FileUpload;
	
});
