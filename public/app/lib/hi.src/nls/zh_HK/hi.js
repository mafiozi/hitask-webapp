define(
//AUTO GENERATED FILE. DO NOT MODIFY.
//If you need to add,update,modify messages: Update /languages/[locale]/js.properties and run "ant generate-js-localization" to build this file.
//begin v1.x content
({
  "team_list": {
    "myself": "我自己",
    "add_person": "新人..."
  },
  "friendfind": {
    "unconfirmed_email": "请确认你的电子邮件地址",
    "in_business": "这个人已经在一个团队中"
  },
  "color": {
    "red": "红色",
    "orange": "橙色",
    "green": "绿色",
    "gray": "灰色",
    "blue": "蓝色",
    "yellow": "黄色",
    "purple": "紫色",
    "none": "没有颜色"
  },
  "tooltip": {
    "delete_message_task_4": "删除备注？",
    "delete_message_task_5": "删除文件？",
    "uncomplete_confirmation": "恢复？",
    "delete_message_task_2": "删除事件？",
    "delete_message_task_1": "删除任务？",
    "delete_comment": "要删除评价吗？",
    "complete_confirmation": "完成了吗？",
    "delete_completed_items": "要删除已完成细项？",
    "archive_completed_items": "存档已完成的细项？"
  },
  "project": {
    "cancel": "取消",
    "make_task_attach": "附加到这个细项吗？",
    "reports": "报告",
    "drag_to_sort": "拖放到排序",
    "move_out_confirmation": "从细项中移出",
    "none": "无",
    "color_orange": "橙色",
    "click_to_add_task": "点击添加一个任务",
    "click_to_open_project": "单击展开/折叠项目",
    "click_to_complete": "单击完成/重新打开任务",
    "click_to_view": "点击查看",
    "color_red": "红色",
    "item": "事项",
    "edit": "编辑",
    "color_yellow": "黄色",
    "project_title": "项目标题",
    "duplicate": "复制...",
    "done": "已完成 %d",
    "private_project": "私人项目",
    "project_description": "在这里输入项目描述...",
    "delete_confirmation": "这个细项被另一个团队成员所有。你确定吗？",
    "click_to_edit": "点击编辑",
    "daily": "每日",
    "move_task_confirmation": "将当前任务设置成子任务？",
    "monthly": "每月",
    "add_new_person": "添加新人...",
    "share_project": "与团队共享",
    "yearly": "每年",
    "create_new_project": "创建新的项目...",
    "items": "事项",
    "private": "私人",
    "save": "保存",
    "color_none": "没有颜色",
    "delete": "移除，存档...",
    "weekly": "每周",
    "move_attach_confirmation": "附加到这个细项吗？",
    "share_project_only": "仅共享项目",
    "unshare_project": "停止共享",
    "enter_item_name": "输入事项名称",
    "share": "共享...",
    "move_invalid_priority": "要对细项重新排序，请切换到按照优先级排序",
    "color_blue": "蓝色",
    "delete_project": "删除项目和任务",
    "delete_project_confirmation": "删除项目吗？",
    "color_gray": "灰色",
    "color_purple": "紫色",
    "color_green": "绿色",
    "make_task_nested": "创建其他细项的子项",
    "share_project_and_tasks": "共享项目和其任务",
    "move_task_out": "从细项中移出",
    "click_specific_day_to_complete": "To complete recurring task, please complete it in Calendar by clicking the checkbox on that task for specific day.",
    "move_to_archive": "移动到存档",
    "duplicate_project": "复制项目和任务",
    "delete_but_keep_tasks": "删除项目，保持任务",
    "create_new": "添加新事项",
    "move_project_invalid_priority": "To reorder projects, please switch to sorting by Priority",
    "double_click_to_edit": "Double click to edit"
  },
  "main": {
    "no": "无",
    "send_back_to": "送回到",
    "assigned_to_me": "分配给我",
    "deleted_by": "被   删除",
    "from": "从",
    "returned_to": "返回到",
    "assigned_to": "被分配给"
  },
  "trial": {
    "warning1": "现在升级！",
    "warning": "试用剩余 %d 天。"
  },
  "notification": {
    "project_was_created": "项目已创建",
    "notification_enable": "启用浏览器通知",
    "task_modified": "细项被修改",
    "task_modified_starred": "已更改细项星号状态",
    "project_restore": "项目已恢复",
    "project_was_deleted": "项目已删除",
    "project_was_updated": "项目已更新",
    "task_uncompleted": "细项已被恢复",
    "task_modified_color": "细项颜色被更改",
    "team_member_business_deleted": "团队成员已被移除",
    "task_copy_and_restore": "已创建复制的细项",
    "task_modified_date": "细项日期被更改",
    "notification_disable": "禁用浏览器通知",
    "task_deleted": "细项已被删除",
    "task_modified_project": "细项被移动到项目",
    "task_restore": "细项被恢复",
    "project_copy_restore": "复制的项目已创建",
    "team_member_deleted": "团队成员已被删除",
    "task_created": "细项被创建",
    "logout_confirmation": "有未完成的操作。你确定你要离开 HiTask？",
    "team_member_added": "已添加团队成员",
    "team_member_invited": "已邀请团队成员",
    "task_completed": "细项被标记为已完成",
    "task_duplicate": "细项已被复制",
    "task_assigned": "细项已被分配",
    "task_archive": "细项被移动到存档"
  },
  "no_permissions_to_delete": {
    "not_item_owner_or_administrator": "你必须是这条细项的所有者或是团队管理员才能删除这条细项",
    "not_project_owner_or_administrator": "你必须是这个项目的所有者或团队管理员才可以删除这个项目"
  },
  "no_permissions_to_archive": {
    "not_shared_or_assignee": "你必须是这条细项的所有者/被分配任务者/参与者或是细项已被分享才能存档。"
  },
  "common": {
    "add": "添加",
    "logTimeAction": "log time to this item",
    "behind_proxy": "出现你是使用代理服务器链接互联网。当你通过代理服务器链接到互联网时，一些功能或许不能使用（活动标签等）。",
    "start_upload": "上传",
    "duplicateAction": "duplicate this item",
    "text_formatting_help": "文本格式帮助",
    "deleteAction": "delete this item",
    "attachAction": "attach file to this item",
    "not_permitted_warning": "You have no permission to ${0}. You may ask ${1} ${2} to give you permission to ${3}.",
    "addSubAction": "add sub tasks to this item",
    "assignAction": "assign this item",
    "archiveAction": "archive this item",
    "completeAction": "complete this item",
    "modifyAction": "modify this item",
    "makeStarredAction": "make this item starred"
  },
  "permissions": {
    "title_50": "完成，分配",
    "title_60": "修改",
    "title_100": "每件事",
    "title_20": "查看，评价"
  },
  "unload": {
    "warning": "你确定要离开次页吗？"
  },
  "tag": {
    "add_no_more_tags": "没有更多标签可供选择！",
    "add_title": "点击使用 %标签 标记细项"
  },
  "taskfilter": {
    "status_my_created": "显示我的细项及由我创建的分享任务",
    "status_my": "显示私有细项并且分配给我",
    "status_my_unassigned": "显示我的细项及未分配的共享任务"
  },
  "no_permissions": {
    "no_permissions_to_modify": "抱歉，你没有权限须改这个条目。"
  },
  "calendar": {
    "next_year": "转到下一年",
    "change_year": "更改年",
    "tomorrow": "明天",
    "next_month": "进入下个月",
    "next_week": "下周",
    "previos_month": "转到前一个月",
    "yesterday": "昨天",
    "this_month": "进入这个月",
    "previos_year": "转到前一年",
    "today": "今天",
    "this_yeay": "进入今年",
    "this_date": "转到此日期",
    "all_day_events": "全天事件"
  },
  "no_permissions_to_edit": {
    "not_shared_or_assignee": "你必须是这条细项的所有者/被分配任务者/参加者或者是细项已被分享才可以编辑。",
    "no_permission_to_set_as_private": "这个项目属于另一个团队成员。只有管理者或所有者能私有这个项目。"
  },
  "center": {
    "move_to_project": "移动到项目",
    "items_not_in_project": "项目中没有事项...",
    "remove_color": "移除颜色",
    "assign_to": "分配给",
    "after_week": "一周之后",
    "move_out_of_project": "从项目移出",
    "completed_items_count": "%s 已完成的细项......",
    "loading_items": "正在加载 %s 细项...",
    "due_on": "Due on",
    "completed_items": "已完成的细项： ",
    "starred": "已标星号",
    "overdue": "逾期任务",
    "loading_item": "正在加载细项...",
    "today": "今天",
    "mark_starred": "标记",
    "from": "从",
    "make_start": "可以开始",
    "change_color": "更改颜色",
    "start_on": "在   启动",
    "completed_item_count": "%s 已完成的细项...",
    "item_not_assigned": "没有被分配的细项...",
    "drag_items_here": "拖放到这里",
    "tomorrow": "明天",
    "item_assigned_to": "细项被分配给",
    "during_week": "随后7天",
    "no_items": "没有事项",
    "mark_unstarred": "取消星号标记",
    "unassign": "未分配",
    "item_received_from": "从   已分配的细项",
    "daily": "每日",
    "completed_item": "已完成的细项： ",
    "before_today": "今天之前",
    "to": "到",
    "item_not_in_project": "项目中没有事项...",
    "items_not_assigned": "没有被分配的细项"
  },
  "display": {
    "weekday_0": "Sunday",
    "weekday_2": "Tuesday",
    "weekday_1": "Monday",
    "weekday_4": "Thursday",
    "weekday_3": "Wednesday",
    "weekday_6": "Saturday",
    "weekday_5": "Friday"
  },
  "completed": {
    "archiveLinkTitle": "点击存档已完成细项",
    "removeLinkTitle": "点击移除已完成细项",
    "submenuRemoveLink": "删除已完成",
    "could_not_archive": "一些已完成任务或许不能被归档，这是因为你没有足够的权利对这些条目进行归档。",
    "archiveLink": "存档",
    "removeLink": "移除",
    "title": "已完成",
    "submenuArchiveLink": "存档已完成",
    "removeLinkText": "已完成细项"
  },
  "team": {
    "deleted_by": "由  删除",
    "are_you_sure": "你确定吗？"
  },
  "history": {
    "week": "周",
    "weeks": "周",
    "color": "颜色",
    "year": "年",
    "ago": "之前",
    "description": "说明",
    "title": "标题",
    "years": "年",
    "enabled": "已启用",
    "second": "秒",
    "seconds": "秒",
    "hour": "小时",
    "disabled": "已禁用",
    "tag": "标签",
    "time_track": "时间跟踪",
    "day": "天",
    "hours": "小时",
    "months": "月",
    "reminder": "提示",
    "recurring": "再现",
    "time_est": "时间预估",
    "minutes": "分钟",
    "end_time": "结束时间",
    "no_value": "没有数值",
    "minute": "分钟",
    "start_time": "开始时间",
    "month": "月",
    "days": "天",
    "comment": "在这里输入评论..."
  },
  "priority": {
    "high": "高",
    "low": "低",
    "change_priority": "更改优先级到",
    "medium": "中",
    "increase": "提升优先级",
    "decrease": "降低优先级"
  },
  "sync": {
    "init_targets_retrieving": "正在从你的谷歌账户检索日历..."
  },
  "task": {
    "date": "Date",
    "file_delete": "删除",
    "ago": "之前",
    "add_comment": "添加",
    "project": "项目",
    "or_cancel": "或者",
    "none": "无",
    "start_time_help": "点击设置开始时间",
    "type": "输入",
    "end_date_help": "点击设置结束日期",
    "recurring_none": "无",
    "heading_sharing": "正在分享",
    "create_tasks_button": "创建任务",
    "time_tracking_details": "详细信息...",
    "time_tracking_hours": "小时",
    "message_max_size": "说明不应当超过 %d 个字符 ！文本被截断。",
    "starts": "开始",
    "last_modified": "上一次修改：",
    "click_to_add_project": "点击添加项目",
    "remember_other_sharing": "remember for new items",
    "add_item": "子任务",
    "many_tasks_format": "逗号分隔格式：\u003cbr /\u003e标题，开始日期，结束日期，说明",
    "add_tags": "添加标签：",
    "time_tracking_today": "今天",
    "delete_button": "删除",
    "time_tracking_estimated": "预估",
    "time_tracking_spent": "花费的时间",
    "recurring": "反复",
    "time_tracking_enable": "启用",
    "time_tracking_timer": "启动计时器",
    "archive": "归档",
    "priority": "优先级",
    "loading": "正在加载...",
    "sharing": "正在分享：",
    "recurring_monthly": "每月",
    "tags": "标签",
    "task": "任务",
    "duplicate_button": "复制",
    "time_tracking_track": "记录的时间",
    "birthday": "生日",
    "message_empty": "在这里输入事项描述",
    "note": "备注",
    "shared": "与团队共享",
    "private": "私有",
    "time_tracking_estimation": "时间预估",
    "many_tasks_go_back": "看起来不正确？\u003ca href\u003d\"javascript://\" onclick\u003d\"NewItemManyTasks.goStepBack()\"\u003e点击这里返回、并且更改。\u003c/a\u003e",
    "color": "颜色",
    "assign_to": "分配给：",
    "end_time_help": "点击设置结束时间",
    "description": "说明",
    "ungroup": "我的视图",
    "recurring_yearly": "每年",
    "time_tracking_by": "由",
    "delete_recurring_message": "删除重复细项系列\u003cbr /\u003e或者仅这个事件？",
    "recurring_every": "每",
    "start_date_help": "点击设置开始日期",
    "tag_add": "添加",
    "before_start": "开始之前",
    "choose_tag": "从我的标签中选择",
    "recurring_end": "结束反复时间：",
    "file": "文件",
    "overdue": "逾期",
    "more_tags": "更多标签",
    "ends": "结束",
    "before_due_start": "before due date (if specified) or start",
    "event": "事件",
    "recurring_weekly": "每周",
    "time_tracking": "时间跟踪",
    "heading_assign": "分配",
    "participants": "参与者",
    "cancel_button": "取消",
    "calendar": "Calendar",
    "reminder": "提示",
    "assign_button": "分配",
    "recurring_daily": "每日",
    "title_max_size": "标题不应当超过 %d 字符！",
    "many": "许多任务",
    "priority_medium": "中",
    "priority_low": "低",
    "click_to_set_color": "点击设置细项颜色",
    "save_button": "保存我的更改",
    "many_tasks_description": "粘贴 CSV 文件、或者在每行输入一个任务...",
    "modify": "修改",
    "time_tracking_save": "保存",
    "tag_click_help": "点击仅查看带有标签的事项",
    "open_file": "在新窗口中打开文件",
    "due": "Due",
    "report": "报告",
    "progress": "进展",
    "proceed_button": "继续进行",
    "activity_feed": "Activity",
    "is_all_day": "全天",
    "priority_high": "高",
    "user": "团队",
    "show_history": "显示所有的历史"
  },
  "properties": {
    "shared": "已分享",
    "private": "私人",
    "file_upload_over_quota": "你已经使用了所有可利用的储存空间。\u003ca href\u003d\"/subscribe\"\u003e请升级，获得更多的储存空间！\u003c/a\u003e",
    "more_actions": "More Actions",
    "assigned_none": "无",
    "short_datetime_daily": "Every day %time",
    "or_cancel": "或者",
    "datetime_monthly_interval": "在 %日期 %时间 每 %间歇月份  直到 %循环_截至_日期",
    "drag_file": "或者把文件拖放在这里",
    "datetime_daily": "每天 %时间 直到 %循环_截至_日期",
    "select_file": "选择要上传的文件",
    "file_upload_error": "上传文件出现错误，请再次重试。",
    "file_upload_attach": "\u003cb\u003e把要附加的文件放在这里\u003cb\u003e",
    "file_upload_drop": "\u003cb\u003e把要上传的文件放在这里\u003cb\u003e",
    "short_datetime_daily_interval": "Every %interval days %time",
    "short_datetime_weekly": "Every %day %time",
    "attach_file": "添加附件",
    "time_tracking_hours": "小时",
    "datetime_time": "在 %时间",
    "error_other": "哎呀！我们不能在此时执行这个操作。请稍后重试。",
    "datetime_yearly": "每年的 %日期 %月份 %时间 直到 %循环_截至_日期",
    "assigned_myself": "我自己",
    "attach_another_file": "附加另一个文件",
    "error_timeout": "连接到 hitask.com 时出现错误。请检查你的网络连接、或者稍后重试。",
    "cancel_button": "取消",
    "short_datetime_weekly_interval": "Every %interval weeks on %day %time",
    "short_datetime_time": "at %time",
    "datetime_weekly_interval": "在 %天 %时间 每 %间歇周 直到%循环_截至_日期",
    "short_datetime_monthly": "Every month on %date %time",
    "datetime_weekly": "每 %天 %时间 直到 %循环_截至_日期",
    "datetime_daily_interval": "每 %间歇天数 %时间 直到%循环_截至_日期",
    "file_upload_multiple": "上传 %count 文件",
    "datetime_monthly": "每月的 %日期 %时间 直到 %循环_结束_日期",
    "file_upload_single": "上传",
    "save_button": "保存",
    "short_datetime_yearly_interval": "Every %interval years on %date %month %time",
    "file_upload_failed": "上传出现错误",
    "datetime_yearly_interval": "在 %日期 %月份 %时间 每%间隔年份 直到 %循环_截至_日期",
    "short_datetime_yearly": "Every year on %date %month %time ",
    "short_datetime_monthly_interval": "Every %interval months on %date %time",
    "cancel_upload": "取消",
    "copy_link": "Copy Link"
  },
  "staticTask": {
    "reminder_time": "提醒时间",
    "shared": "已分享",
    "time_create": "创建时间",
    "recurring": "循环中",
    "time_est": "时间评估",
    "false": "错误",
    "time_spent_subitems": "子任务已花费时间",
    "completed": "已完成",
    "color_value": "颜色值",
    "title": "标题",
    "time_spent": "已花费时间",
    "reminder_time_type": "已分享",
    "archived": "已存档",
    "starred": "已标记",
    "assigneeName": "被分配任务者姓名",
    "true": "正确",
    "last_comment": "最后评论",
    "recurring_end_date": "循环截至日期",
    "reminder_enabled": "提醒启用",
    "time_est_subitems": "子任务时间评估",
    "time_track": "时间追踪",
    "category": "分类",
    "recurring_interval": "循环的时间间隔"
  }
})
//end v1.x content
);