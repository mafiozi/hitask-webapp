//dojo.provide("hi.taskcalendar");

/**
 * Copyright (C) 2005-2006 Vide Infra Grupa SIA, Riga, Latvia. All rights reserved.
 * http://videinfra.com
 * Version 1.1
 */
define([
	"dojo/date/locale",
	'hi/widget/task/item_DOM',
	'dojo/_base/lang',
	'dojo/on'
], function (djDateLocale, item_DOM, lang, on) {

	var _shortDays = capitalizeFirstLetter(djDateLocale.getNames('days', 'abbr'));

	dojo.global.HiTaskCalendar = {
		today: new Date(),
		todayMidnight: new Date(new Date(new Date(new Date().setMinutes(0)).setHours(0)).setSeconds(0)),
		currentDate: new Date(),

		monthData: {},
		dayData: {},
		timetableData: {},

		months: capitalizeFirstLetter(djDateLocale.getNames('months', 'wide', 'standAlone')),
		monthsFormat: capitalizeFirstLetter(djDateLocale.getNames('months', 'wide', 'format')),
		monthsAbbr: capitalizeFirstLetter(djDateLocale.getNames('months', 'abbr', 'format')),
		days: capitalizeFirstLetter(djDateLocale.getNames('days', 'wide')),
		daysAbbr: capitalizeFirstLetter(djDateLocale.getNames('days', 'abbr')),
		shortDays: _shortDays.splice(1, _shortDays.length - 1).concat(_shortDays[0]),
		startDay: 1,
		time_format: 24,
		startTime: '00:00',
		endTime: '24:00',
		lastStartTime: '21:59',
		minutesStep: 30,
		pixelsPerMinute: 38 / 60,
		minutesPrecision: 15,
		timetableTop: 0,
		timetableBottom: 911,

		taskStrechBorder: 5,
		taskDragType: 0,
		taskDragID: 0,
		taskDragged: false,
		taskOutDuringDrag: false,

		tooltipTaskList: null,

		load: function(){
			/*
			 * Drow today header
			 */
			this.drawToday();

			dojo.connect(document, "onmousemove", this, "mouseMove");
			dojo.connect(document, "onmouseup", this, "taskMouseUp");

			/*
			 * Draw time table
			 */
			this.drawTimeTable();

			/*
			 * Init next day rollover
			 */
			this.rolloverNextDay(true);

			var timeTable = dojo.byId("timetable");
			if(timeTable) {
				on(timeTable, "mouseup", lang.hitch(this, function(evt) {
					this.taskClick(evt);
				}));
			}
		},

		drawToday: function () {
			var now = dojo.byId('now');

			var currentDay = this.today.getDate();
			now.innerHTML =  this.days[this.today.getDay()] + ', ' + currentDay + ' ' + this.monthsFormat[this.today.getMonth()];
			now.style.cursor = 'pointer';
			now.onclick = function(){
				HiTaskCalendar.selectDate(HiTaskCalendar.today.getDate(), HiTaskCalendar.today.getMonth(), HiTaskCalendar.today.getFullYear());
			};
			this.update(false);
		},
		
		update: function (update) {
			// summary:
			//		Update UI
			if (hi.items.manager.get().length) {
				this.setMonthData();
				this.setDayData();
			}
			if (update === false) {
				this.draw();
			} else {
				this.draw(true);
			}
		},

		maxDate: function (date) {
			// summary:
			//		Returns last date of the month
			// date: Date
			// 		Date for which to return last date of the month

			date = new Date(date);
			date.setDate(1);
			date.setMonth(date.getMonth() + 1);
			date.setDate(0);
			return date.getDate();
		},

		eventArray: function (item, getAll) {
			// summary:
			//		Returns array of events with endDate and startDate for each
			// item: Object
			//		Item data for which to returns events

			var start = item.start_date ? str2timeAPI(item.start_date) : false;
			var end = item.end_date ? str2timeAPI(item.end_date) : false;
			var original_start = time2str(start, 'y-m-d');
			var year = this.currentDate.getFullYear();
			var month = this.currentDate.getMonth();
			var eventArray = [];
			var diffStart, diffEnd, startDate, endDate, i, startDay, endDay;
			var sdTruncated, edTruncated;
			var startIndex, endIndex;
			var counter,recurringIntervalEndCounter,recurringEndDateStr,
				recurring_interval,
				recurring_end_date,
				recurring_end_date_year,
				recurring_end_date_month,
				recurring_end_dateStr,
				diffRecurringEnd;

			if (!item) return [];

			if (hi.items.manager.isTask(item)) {
				if (!start) {
					if (item.due_date) {
						// start = moment(item.due_date).subtract(1, 'day').toDate();
						start = str2time(item.due_date, 'y-m-d');
						original_start = time2str(start, 'y-m-d');
					}
				}
			}

			if (!start) return [];

			switch (true) {
				case item.category == 5:
				case item.recurring == 4:
					recurring_interval = parseInt(item.recurring_interval, 10);
					recurring_end_date = item.recurring_end_date ? str2timeAPI(item.recurring_end_date) : null;
					recurring_end_dateStr = recurring_end_date ? time2str(recurring_end_date,'y-m-d') : null;

					if (getAll) {
						// var hasEnd = !!end;
						// end = end ? end : new Date(start.getTime() + 3600 * 24 * 1000);
						while (start.getTime() < recurring_end_date.getTime() + 3600 * 24 * 1000) {
							eventArray.push({
								id: item.id,
								startDate: new Date(start),
								endDate: end ? new Date(end) : false,
								title: item.title,
								isAllDay: item.is_all_day
							});
							start.setFullYear(start.getFullYear() + recurring_interval);
							if (end) end.setFullYear(end.getFullYear() + recurring_interval);
						}
						break;
					}
					counter = 0;
					diffStart = year - start.getFullYear();
					startDate = start.getDate();
					start.setDate(1);
					if (end) {
						diffEnd = year - end.getFullYear();
						endDate = end.getDate();
						end.setDate(1);
					} else {
						diffEnd = diffStart;
					}
					start.setYear(start.getFullYear() + diffEnd);
					if (end) {
						end.setYear(end.getFullYear() + diffEnd);
					}
					for (i = diffEnd; i <= diffStart; i++) {
						start.setDate(Math.min(startDate, this.maxDate(start)));
						if (end) {
							end.setDate(Math.min(endDate, this.maxDate(end)));
						}
						if (time2str(start, 'y-m-d') >= original_start && hi.items.date._checkLimits(time2str(start,'y-m-d'),recurring_end_dateStr))
						{
							//Figure out the position of the counter for the first instance in the series
							if (counter === 0)
							{
								counter = this._getFirstRecurringIntervalInstance(counter,item.recurring, original_start,start);
							}

							if (counter % recurring_interval === 0 && hi.items.date.getEventStatus(item.id, time2str(start, 'y-m-d')) != 2)
								eventArray.push({
									id: item.id,
									startDate: new Date(start),
									endDate: end ? new Date(end) : false,
									title: item.title,
									isAllDay: item.is_all_day
								});

							counter++;
						}
						start.setDate(1);
						if (end) {
							end.setDate(1);
						}
						start.setYear(start.getFullYear() + 1);
						if (end) {
							end.setYear(end.getFullYear() + 1);
						}
					}
					break;
				case item.recurring == 3:
					startIndex = endIndex = 0;
					recurring_interval = item.recurring_interval;
					recurring_end_date = item.recurring_end_date?str2timeAPI(item.recurring_end_date) : null;
					recurring_end_date_year = recurring_end_date.getFullYear();
					recurring_end_date_month = recurring_end_date.getMonth();
					recurring_end_dateStr = recurring_end_date?time2str(recurring_end_date,'y-m-d') : null;

					counter = 0;

					// if(getAll){
					// 	diffStart = 0;
					// }else{
					diffStart = (year - start.getFullYear()) * 12 + (month - start.getMonth());
					// }

					startDate = start.getDate();
					start.setDate(1);
					if (end) {
						diffRecurringEnd = (recurring_end_date_year - end.getFullYear()) * 12 + (recurring_end_date_month - end.getMonth());
						diffEnd = (year - end.getFullYear()) * 12 + (month - end.getMonth());
						endDate = end.getDate();
						end.setDate(1);
					} else {
						diffRecurringEnd = (recurring_end_date_year - start.getFullYear()) * 12 + (recurring_end_date_month - start.getMonth());
						diffEnd = diffStart;
					}

					if(getAll){
						startIndex = 0;
						endIndex = diffRecurringEnd;
					}else{
						startIndex = diffEnd;
						endIndex = diffStart;

						start.setMonth(start.getMonth() + diffEnd);
						if (end) {
							end.setMonth(end.getMonth() + diffEnd);
						}
					}
					for (i = startIndex; i <= endIndex; i++) {
						start.setDate(Math.min(startDate, this.maxDate(start)));
						if (end) {
							end.setDate(Math.min(endDate, this.maxDate(end)));
						}
						if (time2str(start, 'y-m-d') >= original_start && hi.items.date._checkLimits(time2str(start,'y-m-d'), recurring_end_dateStr)){
							//Figure out the position of the counter for the first instance in the series
							if (counter === 0){
								counter = this._getFirstRecurringIntervalInstance(counter,item.recurring, original_start,start);
							}

							if (counter % recurring_interval === 0 && hi.items.date.getEventStatus(item.id, time2str(start, 'y-m-d')) != 2)
								eventArray.push({
									id: item.id,
									startDate: new Date(start),
									endDate: end ? new Date(end) : false,
									title: item.title,
									isAllDay: item.is_all_day
								});

							counter++;
						}

						start.setDate(1);
						if (end) {
							end.setDate(1);
						}
						start.setMonth(start.getMonth() + 1);
						if (end) {
							end.setMonth(end.getMonth() + 1);
						}
					}
					break;
				case item.recurring == 2:
					recurring_interval = item.recurring_interval;
					recurring_end_date = item.recurring_end_date?str2timeAPI(item.recurring_end_date):null;
					recurring_end_dateStr = recurring_end_date?time2str(recurring_end_date,'y-m-d'):null;

					counter = 0;

					/* #2513
					 * Truncate time so we get y-m-dT00:00:00.000, otherwise the
					 * difference in days between the start and end date is wrong
					 */
					sdTruncated = hi.items.date.truncateTime(str2timeAPI(item.start_date));
					if (end) edTruncated = hi.items.date.truncateTime(str2timeAPI(item.end_date));

					startDay = start.getDay();
					diffStart = (year - start.getFullYear()) * 12 + (month - start.getMonth()) - 1;
					if (end) {
						diffStartEnd = this.dateDiff(sdTruncated, edTruncated);
						diffEnd = (year - end.getFullYear()) * 12 + (month - end.getMonth()) - 1;
						endDay = end.getDay();
					} else {
						diffStartEnd = false;
						diffEnd = diffStart;
					}

					if(!getAll){
						start.setMonth(start.getMonth() + diffEnd);
						if (end) {
							end.setMonth(end.getMonth() + diffEnd);
							start = new Date(end.getFullYear(), end.getMonth(), end.getDate(), start.getHours(), start.getMinutes(), start.getSeconds());
							// start = new Date(end);
							start.setDate(end.getDate() - diffStartEnd);
						}
					}

					while (start.getDay() != startDay) {
						start.setDate(start.getDate() - 1);
						if (end) {
							end.setDate(end.getDate() - 1);
						}
					}

					while ((this.getYearMonthStr(start) <= this.getYearMonthStr(this.currentDate) || getAll) && hi.items.date._checkLimits(time2str(start,'y-m-d'),recurring_end_dateStr) ) {
						if (time2str(start, 'y-m-d') >= original_start )
						{

							//Figure out the position of the counter for the first instance in the series
							if (counter === 0)
							{
								counter = this._getFirstRecurringIntervalInstance(counter,item.recurring, original_start,start);
							}
							if (counter % recurring_interval === 0 && hi.items.date.getEventStatus(item.id, time2str(start, 'y-m-d')) != 2)
								eventArray.push({
									id: item.id,
									startDate: new Date(start),
									endDate: end ? new Date(end) : false,
									title: item.title,
									isAllDay: item.is_all_day
								});

							counter++;
						}
						start.setDate(start.getDate() + 7);
						if (end) {
							end.setDate(end.getDate() + 7);
						}
					}
					break;
				case item.recurring == 1:
					recurring_interval = item.recurring_interval;
					recurring_end_date = item.recurring_end_date?str2timeAPI(item.recurring_end_date):null;
					recurring_end_dateStr = recurring_end_date?time2str(recurring_end_date,'y-m-d'):null;
					counter = 0;

					diffStart = (year - start.getFullYear()) * 12 + (month - start.getMonth()) - 1;
					if (end) {
						diffEnd = (year - end.getFullYear()) * 12 + (month - end.getMonth()) - 1;
					} else {
						diffEnd = diffStart;
					}
					if(!getAll){
						start.setMonth(start.getMonth() + diffEnd);
						if (end) {
							end.setMonth(end.getMonth() + diffEnd);
						}
					}

					while ((this.getYearMonthStr(start) <= this.getYearMonthStr(this.currentDate) || getAll) && hi.items.date._checkLimits(time2str(start,'y-m-d'),recurring_end_dateStr)  ) {
						if (time2str(start, 'y-m-d') >= original_start ) {
							//Figure out the position of the counter for the first instance in the series
							if (counter === 0)
							{
								counter = this._getFirstRecurringIntervalInstance(counter,item.recurring, original_start,start);
							}
							if (counter % recurring_interval === 0 && hi.items.date.getEventStatus(item.id, time2str(start, 'y-m-d')) != 2)
								eventArray.push({
									id: item.id,
									startDate: new Date(start),
									endDate: (end ? new Date(end) : false),
									title: item.title,
									isAllDay: item.is_all_day
								});
							counter++;
						}


						start.setDate(start.getDate() + 1);
						if (end) {
							end.setDate(end.getDate() + 1);
						}
					}
					break;
				default:
					if (start) {
						if (time2str(start, 'y-m-d') >= original_start && hi.items.date.getEventStatus(item.id, time2str(start, 'y-m-d')) != 2) {
							eventArray.push({
								id: item.id,
								startDate: new Date(start),
								endDate: end ? new Date(end) : false,
								title: item.title,
								isAllDay: item.is_all_day
							});
						}
					}
					break;
			}

			var j, temp,
				jmax = eventArray.length;

			for (j = 0; j < jmax; j++) {
				temp = eventArray[j];
				temp.id = item.id + '_' + j;
				temp.uuid = item.id;
				temp.start_date = this.getStrFromDate(temp.startDate);
				temp.end_date = temp.endDate ? this.getStrFromDate(temp.endDate) : '';

				if (item.category === hi.widget.TaskItem.CATEGORY_TASK) {
					// temp.due_date = item.due_date;
					temp.due_date = temp.endDate;
				}
				temp.recurring = item.recurring;
				temp.color = item.color;
				temp.category = item.category;
				temp.completed = item.recurring? hi.items.manager.getIsCompleted(item.id, temp.start_date) : item.completed;
			}

			return eventArray;
		},

		/*
		 * Figure out position of recurrence interval counter based on the start date of the item
		 * and the current calendar date
		 *
		 * This is necessary when we extend past the original rendering of the calendar months
		 *
		 * Input:
		 * initial_counter => current position of the counter
		 * recurring => type of recurrence
		 * original_start => string representation of start date for recurring item
		 * start_date => Date representation of starting date to calculate recurrence intervals from
		 *
		 * Output: position of the counter for the first recurrence on the recurrence interval
		 */
		_getFirstRecurringIntervalInstance:function(initial_counter, recurring, original_start, start_date) {
			var counter = initial_counter,
				timeObj = {start_time:str2timeAPI(original_start)},
				start_date_str = time2str(start_date,'y-m-d');

			while(time2str(timeObj.start_time,'y-m-d') < start_date_str) {
				counter++;
				//tmp_start.setDate(tmp_start.getDate()+1);
				hi.items.date._incrementBy(recurring,timeObj);
			}
			return counter;
		},

		dateDiff: function(d1, d2) {
			return Math.round(Math.abs(d1.valueOf() - d2.valueOf()) / 86400000);
		},

		timeDiff: function(d1, d2) {
			return d2.valueOf() - d1.valueOf();
		},

		nearestEvent: function(item, date) {
			var frac = 1 / 3;
			var nEvent;
			var start = str2time(item.start_date, 'y-m-d');
			var end = str2time(item.end_date, 'y-m-d');
			var diffStart, diffEnd, startDate, endDate;
			var arr = [];
			var i = 0, imax;
			var diffStartEnd;

			date = date || new Date();
			date = new Date(date);
			date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
			var year = date.getFullYear();
			var month = date.getMonth();

			if (time2str(start, 'y-m-d') > time2str(date, 'y-m-d')) {
				return {startDate: new Date(start), endDate: new Date(end)};
			}

			switch (true) {
				case item.recurring == 4:
				case item.category == 5:
					diffStart = year - start.getFullYear() - 1;
					startDate = start.getDate();
					start.setDate(1);
					if (end) {
						diffEnd = year - end.getFullYear() - 1;
						endDate = end.getDate();
						end.setDate(1);
					} else {
						diffEnd = diffStart;
					}
					start.setYear(start.getFullYear() + diffEnd);
					if (end) {
						end.setYear(end.getFullYear() + diffEnd);
					}
					while (i++ < 10) {
						start.setDate(Math.min(startDate, this.maxDate(start)));
						if (end) {
							end.setDate(Math.min(endDate, this.maxDate(end)));
						}
						if (end) {
							if (start < date && end < date) {
								arr.push({diff: this.dateDiff(date, end), startDate: new Date(start), endDate: new Date(end)});
							} else if (start > date && end > date) {
								arr.push({diff: this.dateDiff(date, start) * frac, startDate: new Date(start), endDate: new Date(end)});
								break;
							} else {
								arr.push({diff: 0, startDate: new Date(start), endDate: new Date(end)});
								break;
							}
						} else {
							if (start < date) {
								arr.push({diff: this.dateDiff(date, start), startDate: new Date(start), endDate: false});
							} else if (start > date) {
								arr.push({diff: this.dateDiff(date, start) * frac, startDate: new Date(start), endDate: false});
								break;
							} else {
								arr.push({diff: 0, startDate: new Date(start), endDate: false});
								break;
							}
						}
						start.setDate(1);
						if (end) {
							end.setDate(1);
						}
						start.setYear(start.getFullYear() + 1);
						if (end) {
							end.setYear(end.getFullYear() + 1);
						}
					}
					break;
				case item.recurring == 3:
					diffStart = (year - start.getFullYear()) * 12 + (month - start.getMonth()) - 1;
					startDate = start.getDate();
					start.setDate(1);
					if (end) {
						diffEnd = (year - end.getFullYear()) * 12 + (month - end.getMonth()) - 1;
						endDate = end.getDate();
						end.setDate(1);
					} else {
						diffEnd = diffStart;
					}
					start.setMonth(start.getMonth() + diffEnd);
					if (end) {
						end.setMonth(end.getMonth() + diffEnd);
					}
					while (i++ < 10) {
						start.setDate(Math.min(startDate, this.maxDate(start)));
						if (end) {
							end.setDate(Math.min(endDate, this.maxDate(end)));
						}
						if (end) {
							if (start < date && end < date) {
								arr.push({diff: this.dateDiff(date, end), startDate: new Date(start), endDate: new Date(end)});
							} else if (start > date && end > date) {
								arr.push({diff: this.dateDiff(date, start) * frac, startDate: new Date(start), endDate: new Date(end)});
								break;
							} else {
								arr.push({diff: 0, startDate: new Date(start), endDate: new Date(end)});
								break;
							}
						} else {
							if (start < date) {
								arr.push({diff: this.dateDiff(date, start), startDate: new Date(start), endDate: false});
							} else if (start > date) {
								arr.push({diff: this.dateDiff(date, start) * frac, startDate: new Date(start), endDate: false});
								break;
							} else {
								arr.push({diff: 0, startDate: new Date(start), endDate: false});
								break;
							}
						}
						start.setDate(1);
						if (end) {
							end.setDate(1);
						}
						start.setMonth(start.getMonth() + 1);
						if (end) {
							end.setMonth(end.getMonth() + 1);
						}
					}
					break;
				case item.recurring == 2:
					diffStart = (year - start.getFullYear()) * 12 + (month - start.getMonth()) - 1;
					startDate = start.getDate();
					start.setDate(1);
					if (end) {
						diffEnd = (year - end.getFullYear()) * 12 + (month - end.getMonth()) - 1;
						endDate = end.getDate();
						end.setDate(1);
					} else {
						diffEnd = diffStart;
					}
					start.setMonth(start.getMonth() + diffEnd);
					if (end) {
						end.setMonth(end.getMonth() + diffEnd);
					}
					while (i++ < 10) {
						if (end) {
							if (start < date && end < date) {
								arr.push({diff: this.dateDiff(date, end), startDate: new Date(start), endDate: new Date(end)});
							} else if (start > date && end > date) {
								arr.push({diff: this.dateDiff(date, start) * frac, startDate: new Date(start), endDate: new Date(end)});
								break;
							} else {
								arr.push({diff: 0, startDate: new Date(start), endDate: new Date(end)});
								break;
							}
						} else {
							if (start < date) {
								arr.push({diff: this.dateDiff(date, start), startDate: new Date(start), endDate: false});
							} else if (start > date) {
								arr.push({diff: this.dateDiff(date, start) * frac, startDate: new Date(start), endDate: false});
								break;
							} else {
								arr.push({diff: 0, startDate: new Date(start), endDate: false});
								break;
							}
						}
						start.setDate(start.getDate() + 7);
						if (end) {
							end.setDate(end.getDate() + 7);
						}
					}
					break;
				case item.recurring == 1:
					if (!end || time2str(end, 'y-m-d') > time2str(date, 'y-m-d')) {
						var d = new Date(start.getTime());
						start.setMonth(month);
						start.setFullYear(year);
						start.setDate(date.getDate());

						if (end) {
							end.setDate(end.getDate() + 1);
							arr.push({diff: 0, startDate: new Date(start), endDate: new Date(end)});
						} else {
							arr.push({diff: this.dateDiff(date, start), startDate: new Date(start), endDate: false});
						}
					} else {
						start.setTime(end.getTime());
						start.setDate(end.getDate() - 1);
						arr.push({diff: 0, startDate: new Date(start), endDate: new Date(end)});
					}

					break;
				default:
			}

			var currDiff = false;
			imax = arr.length;
			if (imax === 0) {
				return {startDate: str2time(item.start_date, 'y-m-d'), endDate: str2time(item.end_date, 'y-m-d')};
			}
			for (i = 0; i < imax; i++) {
				if (currDiff === false || currDiff > arr[i].diff) {
					nEvent = arr[i];
					currDiff = arr[i].diff;
				}
			}
			return nEvent;
		},

		nextEvent: function(start, recurring, date) {
			date = date || new Date();
			date = new Date(date);
			var year = date.getFullYear(),
				month = date.getMonth(),
				temp_start,
				nEvent,
				diffStart,
				startDate;

			start = new Date(start);
			if (start > date) {
				return [this.timeDiff(date, start), start];
			}
			var i = 0, imax;
			switch (true) {
				case recurring == 4:
					temp_start = new Date(start);
					temp_start.setYear(year);
					if(temp_start > date){
						temp_start.setYear(year - 1);
						if(temp_start < start){
							temp_start = new Date(start);
						}
					}
					return [this.timeDiff(date, temp_start), temp_start];
					break;
				case recurring == 3:
					temp_start = new Date(start);
					temp_start.setMonth(month);
					temp_start.setYear(year);
					if(temp_start > date){
						temp_start.setMonth(month - 1);
						if(temp_start < start){
							temp_start = new Date(start);
						}
					}
					return [this.timeDiff(date, temp_start), temp_start];
					break;
				case recurring == 2:
					temp_start = new Date(start);
					var diffWeeks = dojo.date.difference(temp_start, date, 'week');
					temp_start = dojo.date.add(temp_start, 'week', diffWeeks);
					if(temp_start > date){
						temp_start = dojo.date.add(temp_start, 'week', 1);
						if(temp_start < start){
							temp_start = new Date(start);
						}
					}
					return [this.timeDiff(date, temp_start), temp_start];
					break;
				case recurring == 1:
					// if recurrent daily, simply takes incoming date
					return [this.timeDiff(date, start), date];
					break;
				default:
			}
			return [this.timeDiff(date, start), start];
		},

		setMonthData: function() {
			this.monthData = {};

			var startDate, endDate;
			var startMonth, endMonth;
			var startDay, endDay;
			var eventArray;

			var currentMonth = this.getYearMonthStr(this.currentDate);
			var i, imax, j, jmax,
				data = hi.items.manager.get(), temp;

			for (i = 0, imax = data.length; i < imax; i++) {
				temp = data[i];
				startDate = this.getDateFromStr(temp.start_date);
				endDate = this.getDateFromStr(temp.end_date);

				if (startDate && !him.isProject(temp.id)) {
					eventArray = this.eventArray(temp);

					jmax = eventArray.length;
					for (j = 0; j < jmax; j++) {
						startDate = eventArray[j].startDate;
						endDate = eventArray[j].endDate;

						if(!endDate)
							endDate = startDate;

						startMonth = this.getYearMonthStr(startDate);
						endMonth = this.getYearMonthStr(endDate);

						if( (startMonth == currentMonth) || (endMonth == currentMonth) ||
								( (startMonth <= currentMonth) && (endMonth >= currentMonth) ) ){
							startDay = this.getDayStr(startDate, currentMonth);
							endDay = this.getDayStr(endDate, currentMonth);
							for(var d = startDay; d <= endDay; d++) {
								if(typeof this.monthData[d] == 'undefined'){
									this.monthData[d] = {categories: {0: true}, completed: {}};
								}
								this.monthData[d][temp.id] = true;
								this.monthData[d]['categories'][temp['category']] = true;
								/* #2963 this.monthData[d]['completed'][temp['id']] = temp['completed'] ? true : false; */
								this.monthData[d]['completed'][temp['id']] = hi.items.manager.getIsCompleted(temp,time2str(startDate,'y-m-d')) ? true : false;
							}
						}
					}
				}
			}
		},

		setDayData: function(){
			this.dayData = {
				multiDay: {
					wholeDay: {},
					notWholeDay: {}
				},
				thisDay: {
					wholeDay: {},
					notWholeDay: {},
					taskSort: {}
				}
			};

			var i, ij, task, time, inTimetable, isMultiDay;
			var currentDate = this.getCurrentDateStr();
			var eventArray, j, jmax, start_date, end_date, start_time, end_time;
			var item, tmp;

			if (typeof this.monthData[this.currentDate.getDate()] != 'undefined') {
				for(i in this.monthData[this.currentDate.getDate()])  {
					if(i != 'categories' && i != 'completed') {
						item = hi.items.manager.get(i);
						eventArray = this.eventArray(item);
						jmax = eventArray.length;
						for (j = 0; j < jmax; j++) {
							if (j > 0) {
								ij = j + '_' + i;
							} else {
								ij = i;
							}

							task = {};
							task['period'] = '';
							time = '';
							inTimetable = true;
							isMultiDay = false;

							start_date = eventArray[j].start_date;
							end_date = eventArray[j].end_date;
							start_time = false;
							end_time = false;

							if (end_date == '' || end_date == null) {
								if (start_date != currentDate) continue;
							} else {
								if (start_date > currentDate || end_date < currentDate) continue;
							}
							if( ( (start_date < currentDate) || (end_date > currentDate) ) ){
								isMultiDay = true;
								inTimetable = false;
							}

							if(!isMultiDay) {
								// Extract time
								if (item.start_date) {
									tmp = item.is_all_day ? false : str2timeAPI(item.start_date);
									start_time = tmp ? time2str(tmp, "h:i") : false;
								}
								if (item.end_date) {
									tmp = item.is_all_day ? false : str2timeAPI(item.end_date);
									end_time = tmp ? time2str(tmp, "h:i") : false;
								}
							}

							if(!isMultiDay && (start_time == null || start_time == ''))
								inTimetable = false;

							if(isMultiDay){
								if( (start_date < currentDate) || (start_time == '' || start_time == null) ){
									task['period'] += this.formatDate(start_date || '') + ' ';
								}
								if(start_time != '' && start_time != null){
									task['period'] += HiTaskCalendar.formatTime(start_time || '') + ' ';
								}

								task['period'] += '- ';

								if( (end_date > currentDate) || (end_time == '' || end_time == null) ){
									task['period'] += this.formatDate(end_date || '') + ' ';
								}
								if(end_time != ''){
									task['period'] += HiTaskCalendar.formatTime(end_time || '') + ' ';
								}
							}
							else if(inTimetable) {
								time = start_time;
								task['period'] = HiTaskCalendar.formatTime(time || '') + ' ';

								if(time < this.startTime)
									time = this.startTime;
								else if(time >= this.endTime)
									time = this.lastStartTime;

								task['startTime'] = time;

								time = end_time;
								if(time == '' || time == null)
									time = task['startTime'];

								if(time != task['startTime'])
									task['period'] += ' - ' + HiTaskCalendar.formatTime(time || '');
								if(time < this.startTime)
									time = this.startTime;
								else if(time >= this.endTime)
									time = this.endTime;

								task['endTime'] = time;

								task['startTime'] = this.getTimeFromStr(task['startTime']);
								task['endTime'] = this.getTimeFromStr(task['endTime']);
								this.setTaskPosition(task);
							}

							task.inTimetable = inTimetable;
							task.isMultiDay = isMultiDay;

							if(isMultiDay){
								if( (start_date < currentDate) && (end_date > currentDate) ){
									this.dayData['multiDay']['wholeDay'][ij] = task;
								} else {
									this.dayData['multiDay']['notWholeDay'][ij] = task;
								}
							}
							else
							{
								if(task['inTimetable'])
									this.dayData['thisDay']['notWholeDay'][i] = task;
								else
									this.dayData['thisDay']['wholeDay'][i] = task;
							}
						}
					}
				}
			}
			this.sortDayData();
		},
		
		draw: function(update) {
			//calendar
			var calendar = dojo.byId("hitask_calendar_2");
			if (!calendar) return;

			var currMonth = this.currentDate.getMonth();
			var current_year = this.currentDate.getFullYear();

			var html = '';
			html += '';
			html += '<div class="table">';
			html += '	<table cellspacing="0">';
			html += '		<tr>';

			var i;
			for (i = 0; i < 7; i++) {
				html += '		<th' + ((this.startDay + i + 6) % 7 > 4 ? ' class="holiday"' : '') + '>' + this.shortDays[(this.startDay + i + 6) % 7] + '</th>';
			}

			html += '		</tr>';


			var currentDate = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), this.currentDate.getDate(), 23, 59, 59, 999);
			var tmp = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
			var currentDay;
			var isActiveDay;
			var id;

			tmp.setDate(1);
			var startDate = this.startDay - ((tmp.getDay() - 1) % 7 < 0 ? (tmp.getDay() - 1) % 7 + 7 : (tmp.getDay() - 1) % 7);
			if (startDate > 1) startDate -= 7;
			tmp.setDate(startDate);

			var data = hi.items.manager.get();

			for(var j = 0; j < 6; j++) {

				html += '<tr' + (!j ? ' class="top"' : '') + '>';
				for (i = 0; i < 7; i++) {
					currentDay = tmp.getDate();

					html += '<td class="dropSpot';

					if(tmp.getMonth() != currentDate.getMonth()) {
						html += ' disabled';
					} else {
						// #2789: do not highlight day if all events are completed already at this day.
						var allCompleted = dojo.hitch(this, function() {
							var res = true;

							if (this.monthData[currentDay] && typeof(this.monthData[currentDay].completed) == 'object') {
								for (var i in this.monthData[currentDay].completed) {
									res = res && this.monthData[currentDay].completed[i];
								}
							}

							return res;
						});
						if( data.length && (currentDay in this.monthData) && !allCompleted() ) {
							html += ' b';
						}
					}
					if ((this.startDay + i + 6) % 7 > 4) {
						html += ' holiday';
					}
					if((this.today.getDate() == currentDay) && (this.today.getMonth() == tmp.getMonth()) && (this.today.getFullYear() == tmp.getFullYear())) {
						html += ' today';
					}
					if((currentDate.getDate() == currentDay) && (currentDate.getMonth() == tmp.getMonth()) && (currentDate.getFullYear() == tmp.getFullYear())) {
						html += ' active';
					}

					html += '" onmouseover="hoverElement(this)"  onmouseout="houtElement(this)">';
					html += '				<a href="javascript://" onclick="HiTaskCalendar.selectDate(' + currentDay + ', ' + tmp.getMonth() + ', ' + tmp.getFullYear() + ');" title="' + hi.i18n.getLocalization("calendar.this_date") + '">' + currentDay + '</a>';
					html += '				<input type="hidden" class="drop_hidden_text drop_hidden" value="' + time2str(tmp, Hi_Calendar.format) + '" />';
					html += '			</td>';
					// tmp.setTime(tmp.getTime() + 3600 * 24 * 1000);
					tmp.setDate(tmp.getDate() + 1);
				}
				html += '		</tr>';
			}

			html += '	</table>';
			html += '</div>';
			calendar.innerHTML = html;

			// calendar.className = calendar.className.replace(' hidden', '');
			dojo.removeClass(calendar, 'hidden');
			dijit.byId('calendarPane').resize();

			HiDragSourceCached.calendar = false;

			if(!data.length) return;

			//timetable

			var timetable = dojo.byId('timetable');
			var day = dojo.byId('day');
			var task;
			var taskData;
			var taskID;
			var timetablePadding = 0;
			var i, j;

			//removeNode(timetable, false);
			timetable.innerHTML = '';
			day.innerHTML = '';
			var day_empty = true;
			for (i in this.dayData['multiDay']['wholeDay']) { //wholeday multiday tasks
				taskID = parseIntRight(i);
				if(this.checkFilter(taskID)) {
					taskData = this.dayData['multiDay']['wholeDay'][i];
					/* #2910 - pass in true to indicate this item spans multiple days */
					taskClassName = this.getTaskClassName(taskID,true);
					task = document.createElement('a');
					task.href="javascript://";

					task.id = 'day_task' + i;
					task.className = taskClassName;

					task.innerHTML = '<span id="' + task.id + '_complete" class="complete_cb">'
						+ HiTaskCalendar.generateInlineCompleteCbHtml(taskID, taskClassName.indexOf('completed') != -1) + '</span>'
						+ '<span class="period">' + taskData['period'] + '</span>' + htmlspecialchars(hi.items.manager.get(taskID, 'title'));

					task.onclick = this.taskClick;
					day_empty = false;
					day.appendChild(task);
				}
			}

			for(i in this.dayData['multiDay']['notWholeDay']) { //multiday tasks
				taskID = parseIntRight(i);
				if(this.checkFilter(taskID)) {
					taskData = this.dayData['multiDay']['notWholeDay'][i];
					/*
					task = document.createElement('div');
					timetable.appendChild(task);

					task.id = 'timetable_task' + i;
					task.className = 'task multiday';

					task.innerHTML = '<!-- -->';

					task.style.top = taskData['top'] + 'px';
					task.style.height = taskData['height'] + 'px';
					task.style.marginLeft = timetablePadding + 'px';

					task.onclick = function() {
						var task_id = parseIntRight(this.id);
						var item = hi.items.manager.item(task_id);
						if (item) {
							item.expand();
						}
					};

					timetablePadding += task.offsetWidth + 1;
					*/
					/* #2910 - pass in true to indicate this item spans multiple days */
					taskClassName = this.getTaskClassName(taskID,true);
					task = document.createElement('a');
					task.href="javascript://";

					task.id = 'day_task' + i;
					task.className = taskClassName;
					task.innerHTML = '<span id="' + task.id + '_complete" class="complete_cb">'
						+ HiTaskCalendar.generateInlineCompleteCbHtml(taskID, taskClassName.indexOf('completed') != -1) + '</span>'
						+ '<span class="period">' + taskData['period'] + '</span> ' + htmlspecialchars(hi.items.manager.get(taskID, 'title'));

					task.onclick = this.taskClick;
					day_empty = false;
					day.appendChild(task);
				}
			}
			if(timetablePadding) { //oneday task container
				var block = document.createElement('div');
				block.style.paddingLeft = timetablePadding + 'px';
				timetable.appendChild(block);
				timetable = block;

				block = document.createElement('div');
				block.className = 'relative full';
				timetable.appendChild(block);
				timetable = block;
			}
			this.initTimetable();
			var taskClassName;
			var task_time;
			var idx;
			var idxStep = this.pixelsPerMinute * this.minutesStep;
			var width, left, right;
			var fields = {};
			var ids = {};
			var fieldsCnt, curField, tasksCnt, parentTitle;
			for(i in this.dayData['thisDay']['wholeDay']) { //oneday tasks without time
				taskID = parseIntRight(i);
				parentTitle = hi.items.manager.get(taskID, 'parentTitle');

				if(this.checkFilter(taskID)) {
					taskClassName = this.getTaskClassName(taskID);
					taskData = this.dayData['thisDay']['wholeDay'][i];
					task = document.createElement('a');
					task.href="javascript://";

					task.id = 'day_task' + i;
					task.className = taskClassName;
					task.innerHTML = '<span id="' + task.id + '_complete" class="complete_cb">'
					+ HiTaskCalendar.generateInlineCompleteCbHtml(taskID, taskClassName.indexOf('completed') != -1) + '</span>'
					/*Change #4594 Do not show project name in calendar*/
					// + (parentTitle ? ('<span class="parent_title">' + htmlspecialchars(parentTitle) + '&nbsp;&nbsp;<span data-icon="" aria-hidden="true"></span></span>') : '')
					+ htmlspecialchars(hi.items.manager.get(taskID, 'title'));

					task.onclick = this.taskClick;
					day_empty = false;
					day.appendChild(task);
				}
			}
			if (day_empty) {
				day.innerHTML = '<div class="empty">&nbsp;</div>';
			}
			for(i in this.dayData['thisDay']['taskSort']) { //oneday tasks with time
				i = this.dayData['thisDay']['taskSort'][i];
				taskID = parseIntRight(i);
				taskClassName = this.getTaskClassName(taskID);
				parentTitle = hi.items.manager.get(taskID, 'parentTitle');

				if(this.checkFilter(taskID)) {
					taskData = this.dayData['thisDay']['notWholeDay'][i];
					if (typeof(taskData['period']) == 'string' && taskData['period']) {
						taskData['period'] = taskData['period'].trim();
					}
					task = document.createElement('DIV');
					disableSelection(task);
					task.id = 'timetable_task' + taskID;
					task.className = 'task ' + taskClassName + ' hidden' + (hi.items.manager.get(taskID, 'color') ? ' color_' + hi.items.manager.get(taskID, 'color') : '');

					task_time = document.createElement('DIV');
					task_time.id = task.id + '_time';
					task_time.className = 'time' + (hi.items.manager.get(taskID, 'color') ? ' color' + hi.items.manager.get(taskID, 'color') : '');
					if (hi.items.manager.get(taskID, 'category') != '1' && hi.items.manager.get(taskID, 'category') != '2') {
						//task_time.style.display = 'none';
					}
					task_time.innerHTML = '<!-- -->';
					var task_html = '';
					var task_img = document.createElement('DIV');
					task_img.className = 'img';
					task_img.innerHTML = '<!-- -->';
					var task_period = document.createElement('P');
					task_period.className = 'task_timetable_period';
					var task_period_inner_span = document.createElement('SPAN');
					task_period_inner_span.className = 'task_timetable_period_inner';
					task_period.appendChild(task_period_inner_span);

					var task_complete_span = document.createElement('SPAN');
					task_complete_span.id = task.id + '_complete';
					task_complete_span.className = 'complete_cb';

					task_complete_span.innerHTML = HiTaskCalendar.generateInlineCompleteCbHtml(taskID, taskClassName.indexOf('completed') != -1);
					task_period_inner_span.appendChild(task_complete_span);

					var task_period_span = document.createElement('SPAN');
					task_period_span.id = task.id + '_period';
					task_period_span.className = 'period';
					task_period_span.innerHTML = taskData['period'] + '&nbsp;';
					task_period_inner_span.appendChild(task_period_span);

					task_period_span = document.createElement('SPAN');
					task_period_span.className = 'item_title';
					task_period_span.appendChild(document.createTextNode(hi.items.manager.get(taskID, 'title')));

					/* #4405 retrieve parentTitle here to show it when new subItem is dragged on the timetable */
					var parentID = hi.items.manager.get(taskID, 'parent');
					parentTitle = hi.items.manager.get(parentID, 'title');

					// Change #4594 Do not show project name in calendar
					// if (parentTitle) {
					// 	var spanParentTitleNode = document.createElement('SPAN');
					// 	spanParentTitleNode.innerHTML = '&nbsp;' + htmlspecialchars(parentTitle) + '&nbsp;&nbsp;<span data-icon="" aria-hidden="true"></span>';
					// 	spanParentTitleNode.className = 'parent_title';
					// 	task_period.appendChild(spanParentTitleNode);
					// }

					task_period_inner_span.appendChild(task_period_span);

					task.appendChild(task_time);
					// don't show icon anymore. task.appendChild(task_img)

					task.appendChild(task_period);
					task.style.top = taskData['top'] + taskData['time_top'] + 'px';
					task.style.height = Math.max(taskData['time_height'], 19) + 'px';
					timetable.appendChild(task);

					task_time.style.top = 0 + 'px';
					if(taskData['time_height'] == 0) {
						//always show vertical bar
						//task_time.className += ' hidden';
					} else {
						task_time.style.height = taskData['time_height'] + 'px';
					}
					task.onmouseover = this.taskMouseOver;
					task.onmouseout = this.taskMouseOut;
					task.onmousemove = this.taskMouseMove;
					task.onmousedown = this.taskMouseDown;

					fields = this.getEmptyFields(taskData['top']);

					curField = 0;
					for(j in fields){
						if(fields[j]['width'] > fields[curField]['width']) curField = j;
					}

					fieldsCnt = 0;
					for(j in fields){
						fieldsCnt++;
					}
					if(taskData['cnt'] <= 0) taskData['cnt'] = 1;

					tasksCnt = Math.ceil(taskData['cnt'] / fieldsCnt);

					left = fields[curField]? fields[curField]['left'] : 99;
					width = fields[curField]? fields[curField]['width'] : 0;

					width = Math.round(width / tasksCnt);

					task.style.width = width + '%';
					task.style.left = left + '%';

					ids = {};
					idx = taskData['top'];
					t = taskData['top'] + taskData['height'] + taskData['time_top'];
					right = left + width;
					while(idx <= t) {
						if (!(idx in this.timetableData)) break;
						for (var id in this.timetableData[idx]['ids']) {
							if(!(id in ids)) {
								this.dayData['thisDay']['notWholeDay'][id]['cnt']--;
								ids[id] = true;
							}
						}
						for(j = left; j < right; j++) {
							this.timetableData[idx]['fields'][j] = false;
						}
						idx += idxStep;
					}

					task.className = task.className.replace(' hidden', '');
				}

			}

			if (!HiTaskCalendar.timeTick) {
				HiTaskCalendar.timeTick = dijit.byId('TimeTick');
			}
			if (HiTaskCalendar.timeTick) {
				HiTaskCalendar.timeTick.reload();
			}

			Hi_Overdue.reload();
			// Change #5316
			if (!update) {
				this._scrollToFirstItem();
				this.scrollToHour(8);
			}
		},

		onInlineCbClick: function(taskId, eventdate,event) {
			dojo.global.handleClickComplete(taskId, eventdate, event, true);
			hi.widget.TaskItemProperties.closeTaskEditTooltip(event);
			event.stopPropagation();
		},

		generateInlineCompleteCbHtml: function(taskId, completed) {
			var item = hi.items.manager.get(taskId),
				recurring = parseInt(item.recurring),
				disabled = item.permission < Hi_Permissions.COMPLETE_ASSIGN,
				eventdate = recurring ? Project.getNextEventDate(taskId, HiTaskCalendar.currentDate) : '';
			
			var html = '<input onclick="HiTaskCalendar.onInlineCbClick(' + taskId +',\''+eventdate+ '\',event);" title="'
				+ hi.i18n.getLocalization("project.click_to_complete") + '" type="checkbox" ' +
				(disabled ? 'disabled ' : ' ') +
				(completed ? 'checked="checked"' : '') + ' eventdate="' + Project.getNextEventDate(taskId, HiTaskCalendar.currentDate) + '" />';

			return html;
		},

		changeYear: function(n,noChangeDate)
		{
			//console.log('change year',[node,n]);
			var currentDate = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), this.currentDate.getDate(), 23, 59, 59, 999);
			var month = currentDate.getMonth();

			currentDate.setFullYear(currentDate.getFullYear() + n);
			if(month != currentDate.getMonth())
				currentDate.setDate(0);

			if (!noChangeDate) this.changeDate(currentDate);

			dojo.addClass('year_select','hidden');
			var displayYear = this.currentDate.getFullYear();
			dojo.byId('displayed_year').innerHTML =displayYear;
			var years = ['minus_1','minus_2','minus_3','minus_0','plus_1','plus_2','plus_3'],
				tmp;
			years.forEach(function(item,i) {
				tmp = item.split(/_/);
				tmp[0] == 'minus'?dojo.byId('year_selection_'+item).innerHTML = displayYear-parseInt(tmp[1]):dojo.byId('year_selection_'+item).innerHTML = displayYear+parseInt(tmp[1]);
			});
		},

		changeMonth: function(n,noChangeDate)
		{
			var currentDate = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), this.currentDate.getDate(), 23, 59, 59, 999);
			var month = currentDate.getMonth(),
				index;

			currentDate.setMonth(currentDate.getMonth() + n);
			if((currentDate.getMonth() - n - month) % 12 != 0)
				currentDate.setDate(0);

			if (!noChangeDate) this.changeDate(currentDate);

			dojo.byId('displayed_month').innerHTML = HiTaskCalendar.months[HiTaskCalendar.currentDate.getMonth()];
			dojo.addClass('month_select','hidden');
			var months = ['inc_9','inc_10','inc_11','inc_0','inc_1','inc_2','inc_3'],
			tmp;
			months.forEach(function(item,i) {
				tmp = item.split(/_/);
				index = (HiTaskCalendar.currentDate.getMonth()+ parseInt(tmp[1])) % 12;
				dojo.byId('month_selection_'+item).innerHTML = HiTaskCalendar.months[index];
			});

		},


		selectDate: function(d, m, y)  {
			var currentDate = new Date(y, m, d);

			if(currentDate.getDate() == d) {
				this.changeDate(currentDate);
			}
		},

		// Change #5316
		_scrollToFirstItem: function() {
			// var firstTask = this.dayData.thisDay.taskSort[0];
			if (!this.dayData || !this.dayData.thisDay) return;

			var tasks = this.dayData.thisDay.notWholeDay;
			var firstTask, temp, st;

			for (var k in tasks) {
				temp = tasks[k];

				if (!firstTask || temp.startTime.getTime() <= tasks[firstTask].startTime.getTime()) {
					firstTask = k;
				}
			}

			if (firstTask) {
				var id = 'timetable_task' + firstTask, node;

				if (node = dojo.byId(id)) {
					// dojo.window.scrollIntoView(node);
					// return;
					st = dojo.style(node, 'top');
				}
			} else {
				st = 456;
			}

			var timetableContainer = dojo.query('.timetable')[0];
			var day = dojo.query('#day')[0];
			var sep = dojo.query('.sep')[0];

			if (!timetableContainer || !day || !sep) return;
			timetableContainer.scrollTop = (st)/*38 * 12*/ + day.clientHeight + sep.clientHeight;
		},

		scrollToHour: function(hour) {
			if (!this.dayData || !this.dayData.thisDay) return;

			var tasks = this.dayData.thisDay.notWholeDay;
			if (Object.keys(tasks).length > 0) return;

			var timetableContainer = dojo.query('.timetable')[0];
			if (!timetableContainer) return;

			var day = dojo.query('#day')[0];
			var sep = dojo.query('.sep')[0];

			hour = parseInt(hour, 10);
			timetableContainer.scrollTop = hour * 38 + day.clientHeight + sep.clientHeight;
		},

		changeDate: function(date) {
			var monthChanged = false,monthChangedBy=0;
			var yearChanged = false,yearChangedBy=0;
			var dayChanged = false;

			if((this.currentDate.getMonth() != date.getMonth()) || (this.currentDate.getFullYear() != date.getFullYear())) {
				monthChanged = true;
				monthChangedBy=date.getMonth()-this.currentDate.getMonth();
			}

			if ( this.currentDate.getFullYear() != date.getFullYear() ) {
				yearChanged = true;
				yearChangedBy = date.getFullYear() - this.currentDate.getFullYear();
			}
			if(this.currentDate.getDate() != date.getDate()) {
				dayChanged = true;
			}

			if(monthChanged || dayChanged) {
				this.currentDate = date;
				if(monthChanged)
					this.setMonthData();
				this.setDayData();
				this.draw();
			}
			/* #2859 */
			if (Tooltip.opened('taskedit') && !hi.items.manager.opened().editingView) {
				hi.items.manager.opened()._setOpenedAttr(hi.items.manager.opened(),false);
				Tooltip.close('taskedit');
			}
			if (monthChanged) this.changeMonth(monthChangedBy, true);
			if (yearChanged) this.changeYear(yearChangedBy,true);
		},

		setTaskPosition: function(task) {
			var minutesOffset = 0, top, height, tmp;

			if (task.startTime) {
				minutesOffset = (task.startTime.getHours() - this.getHoursFromStr(this.startTime)) * 60 + task.startTime.getMinutes();
				minutesOffset -= minutesOffset % this.minutesStep;
			} else {
				minutesOffset = 0;
			}

			top = Math.ceil(minutesOffset * this.pixelsPerMinute);
			task['top'] = top;

			if (task.endTime) {
				minutesOffset = Math.max((task.endTime.getHours() - task.startTime.getHours()) * 60 - (task.startTime.getMinutes() - task.startTime.getMinutes() % this.minutesStep) + task.endTime.getMinutes(), 0);
			} else {
				minutesOffset = 0;
			}

			tmp = minutesOffset % this.minutesStep;
			if(tmp) {
				minutesOffset -= tmp - this.minutesStep;
			}
			height = Math.ceil(minutesOffset * this.pixelsPerMinute) - 1;

			if(height == -1) {
				height = this.minutesStep * this.pixelsPerMinute - 1;
			}
			task['height'] = height;

			if (task.startTime) {
				task['time_top'] = Math.ceil((task.startTime.getMinutes() % this.minutesStep) * this.pixelsPerMinute);
			} else {
				task['time_top'] = 0;
			}

			if (task.endTime) {
				minutesOffset = (task.endTime.getHours() - task.startTime.getHours()) * 60 - task.startTime.getMinutes() + task.endTime.getMinutes();
			} else {
				minutesOffset = 0;
			}

			height = Math.ceil(minutesOffset * this.pixelsPerMinute) - 1;
			if(height < 0) height = 0;
			task['time_height'] = height;
		},

		initTimetable: function() {
			var taskData;
			var idxStep = this.pixelsPerMinute * this.minutesStep;
			var idx = 0;
			var id, i, j, t;

			for(i = 0; i < 48; i++) {
				this.timetableData[idx] = {
					fields: {},
					ids: {},
					cnt: 0
				};
				for(j = 0; j < 100; j++)
					this.timetableData[idx]['fields'][j] = true;
				idx += idxStep;
			}
			for (i in this.dayData['thisDay']['taskSort']) {
				i = this.dayData['thisDay']['taskSort'][i];
				if(this.checkFilter(i)) {
					taskData = this.dayData['thisDay']['notWholeDay'][i];
					taskData['cnt'] = 1;
					idx = taskData['top'];
					t = taskData['top'] + taskData['height'] + taskData['time_top'];
					while(idx <= t) {
						if (!(idx in this.timetableData)) break;
						this.timetableData[idx]['ids'][i] = true;
						this.timetableData[idx]['cnt']++;
						for(id in this.timetableData[idx]['ids']) {
							if(this.timetableData[idx]['cnt'] > this.dayData['thisDay']['notWholeDay'][id]['cnt'])
								this.dayData['thisDay']['notWholeDay'][id]['cnt'] = this.timetableData[idx]['cnt'];
						}
						idx += idxStep;
					}
				}
			}
		},

		sortDayData: function() {
			var i;
			var array = [];
			for(i in this.dayData['thisDay']['notWholeDay'])
				array.push(i);

			var tasks = HiTaskCalendar.dayData['thisDay']['notWholeDay'];

			array.sort(function(a, b) {
				if(tasks[a]['top'] < tasks[b]['top']);
					return -1;
				if(tasks[a]['top'] > tasks[b]['top']);
					return 1;
				if(tasks[a]['height'] < tasks[b]['height']);
					return 1;
				if(tasks[a]['height'] > tasks[b]['height']);
					return -1;
				if(a < b)
					return -1;

				return 1;
			});

			i = 0;
			dojo.forEach(array, function(value, index) {
				HiTaskCalendar.dayData['thisDay']['taskSort'][i++] = value;
		    });
		},

		getDateFromStr: function(str, periodBegin) {
			periodBegin = periodBegin || false;
			var d = str2time(str);
			// #5826:
			// if computer is in UTC-XX zone then following happens:
			// new Date('2016-01-14') returns date object for previous day.
			// Need to use str2time function.
			
			if (periodBegin) {
				return d;
			} else {
				d = d || new Date(str);
				d.setHours(23);
				d.setMinutes(59);
				d.setSeconds(59);
				d.setMilliseconds(999);
				return d;
			}
		},

		getYearMonthStr: function(date)  {
			var month = date.getMonth() + 1;
			return date.getFullYear() + '-' + (month < 10 ? '0' : '') + month;
		},

		getDayStr: function(date, currentMonth) {
			dateMonth = this.getYearMonthStr(date);
			var day;
			if(dateMonth == currentMonth)
				day = date.getDate();
			else if(dateMonth < currentMonth)
				day = this.startDay;
			else {
				var ym = currentMonth.split('-');
				day = 32 - new Date(ym[0], ym[1] - 1, 32).getDate();
			}
			return day;
		},

		getTimeFromStr: function(str, periodBegin) {
			periodBegin = periodBegin || false;
			var t_s = 59, t_ms = 999;
			if(periodBegin){
				t_s = 0;
				t_ms = 0;
			}
			if(!str)
				return false;

			return new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), this.currentDate.getDate(), parseInt(str.substr(0, 2), 10), parseInt(str.substr(3, 2), 10), t_s, t_ms);
		},

		getDateTimeFromStr: function(dateStr, timeStr, periodBegin) {
			periodBegin = periodBegin || false;
			var t_s = 59, t_ms = 999;
			if(periodBegin){
				t_s = 0;
				t_ms = 0;
			}
			var date = false;
			if (dateStr && timeStr) {
				if (dateStr.indexOf('T') >= 0) {
					date = new Date(dateStr);
					date.setHours(parseInt(timeStr.substr(0, 2), 10));
					date.setMinutes(parseInt(timeStr.substr(3, 2), 10));
				} else {
					date = new Date(parseInt(dateStr.substr(0, 4), 10), (parseInt(dateStr.substr(5, 2), 10) - 1), parseInt(dateStr.substr(8, 2), 10), parseInt(timeStr.substr(0, 2), 10), parseInt(timeStr.substr(3, 2), 10), t_s, t_ms);
				}
			}
			return date;
		},

		getCurrentDateStr: function() {
			return HiTaskCalendar.getStrFromDate(this.currentDate);
		},

		getTodayDateStr: function () {
			return HiTaskCalendar.getStrFromDate(this.today);
		},

		getHoursFromStr: function(str) {
			return parseInt(str.substr(0, 2), 10);
		},

		getMinutesFromStr: function(str) {
			return parseInt(str.substr(3, 2), 10);
		},

		getStrFromTime: function(time) {
			var hours = time.getHours();
			var minutes = time.getMinutes();

			return (hours < 10 ? '0' : '') + hours + ':' + (minutes < 10 ? '0' : '') + minutes;
		},

		getStrFromDate: function(date) {
			var month = date.getMonth() + 1;
			var d = date.getDate();
			return date.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (d < 10 ? '0' : '') + d;
		},

		formatDate: function(date) {
			var time = str2time(date, 'y-m-d');
			var format = Hi_Calendar.format;
			if (this.currentDate.getFullYear() == time.getFullYear()) {
				format = format.replace(/^[^md]*/, '');
				format = format.replace(/[^md]*$/, '');
			}
			return time2str(str2time(date, 'y-m-d'), format);
		},

		checkFilter: function(taskID) {
			// Deleted recurring task instances shouldn't be shown
			if (parseInt(hi.items.manager.get(taskID, 'recurring'))) {
				if (hi.items.date.getEventStatus(taskID, time2str(HiTaskCalendar.currentDate, 'y-m-d')) == 2) {
					return false;
				}
			}

			// Don't filter calendar & timeline
			return true;
		},

		getTaskClassName: function(taskID,isMultiDay) {
			taskID = taskID || false;
			if (!taskID) return false;
			var taskClassName;
			taskID = parseIntRight(taskID);
			switch(parseInt(hi.items.manager.get(taskID, 'category')))
			{
				case 1:
					taskClassName = 'tasks';
					break;
				case 2:
				case 3:
				case 5:
					taskClassName = 'event';
					break;
				case 4:
					taskClassName = 'note';
					break;
			}

			if (hi.items.manager.get(taskID, 'recurring')) {
				var todayInstanceDate = time2str(HiTaskCalendar.currentDate, 'y-m-d');
				/* #2910 - use method to get correct instance for the case when the item spans multiple days */
				var status = isMultiDay?hi.items.date.getMultiDayEventStatus(taskID,todayInstanceDate):hi.items.date.getEventStatus(taskID, todayInstanceDate);
				if (status == 1)
				{
					taskClassName += ' completed';
				}

			}else if(hi.items.manager.get(taskID, 'completed') == '1'){
				taskClassName += ' completed';
			}

			return taskClassName;
		},

		getEmptyFields: function(row) {
			var i;
			var fields = {};
			var field = 0;
			var fieldStarted = false;
			var left = 0;
			var width = 0;

			for (i = 0; i < 100; i++) {
				if(this.timetableData[row]['fields'][i]) {
					if(!fieldStarted) {
						fieldStarted = true;
						left = i;
						width = 1;
					} else {
						width++;
					}
				} else if(fieldStarted) {
					fieldStarted = false;
					fields[field++] = {left: left, width: width};
				}
			}
			if (fieldStarted) {
				fields[field] = {left: left, width: width};
			}
			return fields;
		},

		taskMouseMove: function(e) {
			if(!HiTaskCalendar.taskDragID && HiTaskCalendar.isValid !== false) {
				this.style.cursor = '';

				if(!e) e = window.event;
				var mouseY = e.pageY || event.clientY + document.documentElement.scrollTop;
				mouseY -= findPosY(this, true);

				var id = parseIntRight(this.id),
					category = hi.items.manager.get(id, 'category');

				if(category == 1 || category == 2) {
					if(mouseY < HiTaskCalendar.taskStrechBorder) {
						this.style.cursor = 'n-resize';
					} else if(this.offsetHeight - mouseY <= HiTaskCalendar.taskStrechBorder) {
						this.style.cursor = 's-resize';
					} else {
						this.style.cursor = 'move';
					}
				} else {
					this.style.cursor = 'move';
				}
			}
			return true;
		},

		taskMouseOver: function(){
			if(!HiTaskCalendar.taskDragID){
				hoverElement(this);
			}else if(parseIntRight(this.id) == HiTaskCalendar.taskDragID){
				HiTaskCalendar.taskOutDuringDrag = false;
			}
		},

		taskMouseOut: function() {
			if(!HiTaskCalendar.taskDragID) {
				houtElement(this);
			} else if(parseIntRight(this.id) == HiTaskCalendar.taskDragID) {
				HiTaskCalendar.taskOutDuringDrag = true;
			}
		},

		taskMouseDown: function(e) {
			e = e || window.event;
			var mouseY = e.pageY || event.clientY + document.documentElement.scrollTop;
			mouseY -= findPosY(this, true);

			var taskId = HiTaskCalendar.taskDragID = parseIntRight(this.id);
			var task = taskId && hi.items.manager.get(taskId);
			var permission = task && task.permission;

			if (!Hi_Actions.validateAction('modify', taskId, true)) {
				HiTaskCalendar.isValid = false;
				return;
			}
			HiTaskCalendar.taskDragType = 0;
			var category = hi.items.manager.get(HiTaskCalendar.taskDragID, 'category');

			if(category == 1 || category == 2) {
				if(mouseY < HiTaskCalendar.taskStrechBorder) {
					document.body.style.cursor = 'n-resize';
					HiTaskCalendar.taskDragType = 1;
				} else if (this.offsetHeight - mouseY <= HiTaskCalendar.taskStrechBorder) {
					document.body.style.cursor = 's-resize';
					HiTaskCalendar.taskDragType = 2;
				} else {
					document.body.style.cursor = 'move';
					HiTaskCalendar.mouseY = mouseY + HiTaskCalendar.dayData['thisDay']['notWholeDay'][HiTaskCalendar.taskDragID].top;
					HiTaskCalendar.taskDragType = 3;
				}
			} else {
				HiTaskCalendar.mouseY = mouseY + HiTaskCalendar.dayData['thisDay']['notWholeDay'][HiTaskCalendar.taskDragID].top;
				HiTaskCalendar.taskDragType = 3;
			}
			document.onselectstart = function() { return false; };
		},

		taskMouseUp: function(e) {
			document.body.style.cursor = '';
			HiTaskCalendar.isValid = true;
			if(HiTaskCalendar.taskDragged) {
				HiTaskCalendar.taskDragged = false;
			} else if(HiTaskCalendar.taskDragID || HiTaskCalendar.taskAssigned) {
				var id = HiTaskCalendar.taskDragID;
				HiTaskCalendar.taskDragID = false;
				HiTaskCalendar.taskClick(e, id || HiTaskCalendar.taskAssigned);
				//HiTaskCalendar.taskClick(HiTaskCalendar.taskDragID || HiTaskCalendar.taskAssigned);
				return;
			}

			if(HiTaskCalendar.taskDragID && HiTaskCalendar.taskOutDuringDrag) {
				var task = dojo.byId('timetable_task' + HiTaskCalendar.taskDragID);
				task.className = task.className.replace(' hover', '');
			}
			if(HiTaskCalendar.taskDragID && HiTaskCalendar.taskDragType) {
				var taskItem = hi.items.manager.opened(HiTaskCalendar.taskDragID),
					taskW = hi.items.manager._items[HiTaskCalendar.taskDragID],
					taskItemObj = taskW ? (taskW.list || taskW.tooltip) : null,
					taskProperties = taskItemObj && taskItemObj.propertiesView ? taskItemObj.propertiesView.task : null;
				if (taskItem) {
					//taskItem.closeEdit();
					item_DOM.closeEdit(HiTaskCalendar.taskDragID);
				}

				var taskData = HiTaskCalendar.dayData['thisDay']['notWholeDay'][HiTaskCalendar.taskDragID];
				var obj = {id: HiTaskCalendar.taskDragID};
				var item = hi.items.manager.get(HiTaskCalendar.taskDragID);

				if(HiTaskCalendar.taskDragType == 1) {
					/* Bug #2132 - we need to check if start time and start_date are even defined, otherwise we
					 * will pass an undefined value to observers, and cause various errors

					if (!item.end_date) obj['end_date'] = item.start_date;
					if (!item.end_time) obj['end_time'] = item.start_time;
					*/
					if (!item.end_date)
					{
						if (item.start_date) obj['end_date'] = item.start_date;
					}
					if (!item.end_time)
					{
						if (item.start_time) obj['end_time'] = item.start_time;
					}
					obj['start_date'] = item.start_date;
					obj['start_time'] = HiTaskCalendar.getTaskTime(taskData['top'] + taskData['time_top']);
				}
				if ((HiTaskCalendar.taskDragType == 2)) {
					obj['end_date'] = item.start_date;
					obj['end_time'] = HiTaskCalendar.getTaskTime(taskData['top'] + taskData['time_top'] + taskData['time_height']);
				}
				if (HiTaskCalendar.taskDragType == 3) {
					obj['start_date'] = item.start_date;
					obj['end_date'] = item.end_date;
					obj['start_time'] = HiTaskCalendar.getTaskTime(taskData['top'] + taskData['time_top']);
					obj['end_time'] = HiTaskCalendar.getTaskTime(taskData['top'] + taskData['time_top'] + taskData['time_height']);
				}

				if (obj.start_date && obj.start_time) {
					obj.start_date = hi.items.date.dateTimeSetTime(obj.start_date, obj.start_time);
					delete(obj.start_time);

					if (item.start_date && hi.items.date.compareDateTime(item.start_date, obj.start_date) == 0) {
						// Didn't changed
						delete(obj.start_date);
					}
				}
				if (obj.end_date && obj.end_time) {
					obj.end_date = hi.items.date.dateTimeSetTime(obj.end_date, obj.end_time);
					delete(obj.end_time);

					if (item.end_date && hi.items.date.compareDateTime(item.end_date, obj.end_date) == 0) {
						// Didn't changed
						delete(obj.end_date);
					}
				}

				if (obj.start_date || obj.end_date) {
					hi.items.manager.setSave(obj);
					item_DOM._save(HiTaskCalendar.taskDragID,obj);
				}
			}

			HiTaskCalendar.taskDragID = 0;
			HiTaskCalendar.taskDragType = 0;

			document.onselectstart = function() { return true; };
		},

		mouseMove: function(e) {
			/* #2973 - close calendar item tooltip if it is opened */
			var taskID = HiTaskCalendar.taskDragID;
			if (taskID) HiTaskCalendar.taskDragged = true;

			if(HiTaskCalendar.taskDragID != 0 && Hi_Actions.validateAction('modify', taskID)) {
				HiTaskCalendar.closeOpenedTooltip();

				HiTaskCalendar.taskDragged = true;

				if(!e) e = window.event;
				var mouseY = e.pageY || event.clientY + document.documentElement.scrollTop;
				mouseY -= findPosY(dojo.byId('timetable'), true);

				var dy;
				var task = taskID && hi.items.manager.get(taskID);
				var permission = task && task.permission;
				if(permission < Hi_Permissions.MODIFY){
					return;
				}
				// if (Hi_Actions.validateAction('modify', taskID)) return;

				var taskData = HiTaskCalendar.dayData['thisDay']['notWholeDay'][taskID];
				task = dojo.byId('timetable_task' + taskID);
				var task_time = dojo.byId('timetable_task' + taskID + '_time');
				var task_period = dojo.byId('timetable_task' + taskID + '_period');
				task_time.className = task_time.className.replace(' hidden', '');

				if(HiTaskCalendar.taskDragType == 1) {	//stretch task up...
					var taskTop = taskData['top'];
					dy = taskTop - mouseY;
					if(taskTop - dy < HiTaskCalendar.timetableTop)
						dy = taskTop;

					if ((taskData['height'] + dy >= 0) && (taskData['time_height'] + dy >= 0)) {
						taskData['top'] -= dy;
						taskData['height'] += dy;
						if((dy < 0) && (taskData['time_top'] > 0)) {
							taskData['time_top'] += dy;
							if(taskData['time_top'] < 0) {
								taskData['time_height'] += taskData['time_top'];
								taskData['time_top'] = 0;
							}
						} else {
							taskData['time_height'] += dy;
						}

						task.style.top = taskData['top'] + taskData['time_top'] + 'px';
						task.style.height = Math.max(taskData['time_height'], 19) + 'px';
						task_time.style.top = 0 + 'px';
						task_time.style.height = taskData['time_height'] + 'px';
					}
				}
				else if(HiTaskCalendar.taskDragType == 2) {		//stretch task down...
					var taskBottom = taskData['top'] + taskData['height'];
					dy = mouseY - taskBottom;
					if(taskBottom + dy > HiTaskCalendar.timetableBottom)
						dy = HiTaskCalendar.timetableBottom - taskBottom;

					if((taskData['height'] + dy >= 0) && (taskData['time_height'] + dy >= 0)) {
						taskData['height'] += dy;

						if((dy > 0) || (taskData['time_top'] + taskData['time_height'] > taskData['height']))
						{
							taskData['time_height'] += dy;
						}

						task.style.height = Math.max(taskData['time_height'], 19) + 'px';
						task_time.style.height = taskData['time_height'] + 'px';
					}
				} else if(HiTaskCalendar.taskDragType == 3) {		//move task...
					dy = mouseY - HiTaskCalendar.mouseY;
					HiTaskCalendar.mouseY = mouseY;
					if (mouseY < 0) {
						if (dy > 0) dy = 0;
					} else if (mouseY > HiTaskCalendar.timetableBottom) {
						if (dy < 0) dy = 0;
					}
					var border;
					if ((border = taskData['top'] + dy) < 0) {
						if (-taskData['time_top'] >= border) {
							taskData['time_top'] = 0;
						} else {
							taskData['time_top'] += border;
						}
						task_time.style.top = 0 + 'px';
						dy = 0;
					}
					if ((border = taskData['top'] + taskData['height'] + dy - HiTaskCalendar.timetableBottom) > 0) {
						var time_bottom = taskData['height'] - taskData['time_top'] - taskData['time_height'];
						if (border < time_bottom) {
							taskData['time_top'] += border;
						} else {
							taskData['time_top'] += time_bottom;
						}
						task_time.style.top = 0 + 'px';
						dy = 0;
					}

					taskData['top'] += dy;
					if (false && taskData['time_top'] != 0) {
						taskData['top'] += taskData['time_top'];
						taskData['time_top'] = 0;
						task_time.style.top = 0 + 'px';
					}

					task.style.top = taskData['top'] + taskData['time_top'] + 'px';
				}
				task_period.innerHTML = HiTaskCalendar.getTaskPeriod(taskID);
			}
		},

		taskClick: function(evt, id) {
			if (evt && evt.target && typeof(evt.target.type) == 'string' && evt.target.type.toLowerCase() == 'checkbox') {
				// Complete checkbox clicked.
				return;
			}
			if (hi.items.manager.editing() || HiTaskCalendar.taskDragID || this._inTaskClick === true) return;

			this._inTaskClick = true;
			if (Project.checkLimits("TRIAL_TASKS")) {
				var taskId;
				if (evt && id)
					taskId = parseIntRight(id);
				else
					taskId = (this.id ? parseIntRight(this.id) : '');

				var self = this;
				var opened = hi.items.manager.openedId();
				/* Bug #2064 - id < 0 indicates a sub item is opened, so block */
				if(opened < 0) {
					this._inTaskClick = false;
					return;
				}/* #2973 - close tooltip if it is already opened for this item */
				/* Also check if tooltip is opened, since opened might refer to an expanded item
				 * in a task list
				 */
				if(opened == taskId && Tooltip.opened('taskedit')){
					HiTaskCalendar.closeOpenedTooltip();
					this._inTaskClick = false;
					return;
				}

				//If there is an item remove it
				if(HiTaskCalendar.id){
					//Remove item
					HiTaskCalendar.tooltipTaskList.remove({
						"id": HiTaskCalendar.id
					});

					HiTaskCalendar.id = null;
					Project.openedInstance = null;
					Tooltip.close('taskedit');
				}

				//Make sure all other opened tasks are closed
				hi.items.manager.collapseOpenedItems(true, true);

				/*Bug #4130 Calendar pop-up is not opened for all-day tasks*/
				if(taskId){
					Project.openedInstance = time2str(HiTaskCalendar.currentDate, 'y-m-d');
					HiTaskCalendar.id = taskId;
					HiTaskCalendar.showTaskEditTooltip(taskId, this);
				}

				var t = this;
				setTimeout(function(){
					t._inTaskClick = false;
				}, 10);
			}
		},

		initTaskEditTooltip: function () {
			if (!this.initTaskEditTooltip.taskEditInitialized) {
				/**
				 * When tooltip is closed, reset Project data
				 */
				var t = this;
				var TooltipTaskEdit = {
					reset: false,
					closeBefore: function (name, target) {
						if (name == 'taskedit') {
							TooltipTaskEdit.reset = false;
							var el = dojo.byId("tooltip_" + name);
							var op = false;
							try { op = el.getAttribute("opened"); } catch (e) { op = -1; }
							if (!op) { op = -1; }
							if (op != "1") { return; }
							//We know that task edit is closing
							TooltipTaskEdit.reset = true;
						}
					},
					closeAfter: function (name) {
						if (TooltipTaskEdit.reset) {

							//Remove item
							t.tooltipTaskList.remove({
								"id": t.id
							});

							t.id = null;
							Project.openedInstance = false;
							TooltipTaskEdit.reset = false;
						}
					}
				};

				dojo.subscribe('TooltipCloseBefore', TooltipTaskEdit.closeBefore);
				dojo.subscribe('TooltipCloseAfter', TooltipTaskEdit.closeAfter);

				//Create item list
				//Feature #1721 var node = dojo.query(".border", dojo.byId("taskedit_tooltip")).pop();
				var node = dojo.query(".hiTaskToolTipDialogBorder", dojo.byId("taskedit_tooltip")).pop();
				var tooltipTaskList = t.tooltipTaskList = new hi.widget.TaskListTooltip({
					"collapsed": false,
					"completedSubList": false,
					"readStore": hi.store.ProjectCache,
					"writeStore": hi.store.Project,
					// "query": hi.store.ProjectCache.query(),
					"filter": function (item) {
						return item.id == t.id;
					}
				});
				tooltipTaskList.placeAt(node);

				this.initTaskEditTooltip.taskEditInitialized = true;
			}
		},

		taskEditTooltipVisible: function () {
			if (!dojo.hasClass(dojo.byId('tooltip_taskedit'), 'hidden')) {
				return true;
			} else {
				return false;
			}
		},

		showTaskEditTooltip: function (id, self) {
			HiTaskCalendar.initTaskEditTooltip();

			var ul = dojo.byId('taskedit_tooltip').getElementsByTagName('UL');
			var el = (self.tagName ? self : dojo.byId('timetable_task' + id));

			if (ul.length) {
				ul = ul[0];
				dojo.removeClass(dojo.byId('tooltip_taskedit'), 'expanded');

				var is_completed = hi.items.manager.getIsCompleted(id, Project.openedInstance);
				/* #2791 - convert openedInstance time to unambiguous time */
				var next_event_date = Project.getNextEventDate(id, new Date(str2timeAPI(Project.openedInstance)));
				var def = HiTaskCalendar.tooltipTaskList.add(hi.items.manager.get(id), is_completed, next_event_date,/* #2556 - tell TaskList this goes on tooltip, so ignore parent attribute */true);
				def.then(function(widget) {
					if (widget) {
						/*
						widget.watch("editing", function (name, prevVal, newVal) {
							if (newVal) {
								//Feature #1721 HiTaskCalendar.taskTooltipAdjust(el);
								HiTaskCalendar._taskTooltipAdjust(el);
							}
						});
						*/
						dojo.connect(widget,'_setEditingAttr', widget, function(w,newVal) {
							if (newVal) HiTaskCalendar._taskTooltipAdjust(el);
						});

						widget.openedConnect = dojo.connect(widget,'_setOpenedAttr',widget,function(w,newVal) {

							if (!newVal && HiTaskCalendar.taskEditTooltipVisible() && this.taskId == HiTaskCalendar.id) {
								Tooltip.close('taskedit');
								dojo.disconnect.openedConnect;
							}
						});
						/*
						widget.watch("opened", function (name, prevVal, newVal) {
							//if (id == HiTaskCalendar.id ) console.log('widget watch',[name,prevVal,newVal]);
							if (newVal != prevVal && !newVal && HiTaskCalendar.taskEditTooltipVisible() && id == HiTaskCalendar.id) {
								//Close tooltip if item was opened
								//console.log('CLOSE TASKEDIT',[widget,name,newVal,prevVal]);
								Tooltip.close('taskedit');
							}
						});
						*/
						//Feature #1721 HiTaskCalendar.taskTooltipAdjust(el);
						// setTimeout(function() {
						HiTaskCalendar._taskTooltipAdjust(el);
						// }, 400);
					}
				});

			}

		},

		taskTooltipAdjust: function (el) {
			if (!el || !el.parentNode) return;

			var pos_l = 0;
			var tooltip_el = dojo.byId('tooltip_taskedit'), max_l;

			if (el.parentNode.id == 'timetable') {
				max_l = dojo.byId('timetable').offsetWidth + 70;

				//Item in timetable
				pos_l = findTextWidth(el) + 95;		//95 - for paddingLeft
				if (pos_l > max_l) pos_l = max_l;

				pos_l = pos_l + 35;					//35 for arrow space
			} else {
				max_l = dojo.byId('day').offsetWidth - 35;

				//Item in all day tasks
				pos_l = findTextWidth(el) + 45;		//45 - for paddingLeft
				if (pos_l > max_l) pos_l = max_l;

				pos_l = pos_l + 35;					//35 for arrow space
			}

			pos_l += findPosX(el);
			if (pos_l > max_l) pos_l = max_l;

			Tooltip.open('taskedit', "", el, null, 'right');
			tooltip_el.style.left = Math.round(pos_l) + 'px';

			HiTaskCalendar.taskTooltipAdjustVerticalPos();
		},

		/**
		 * Feature #1721
		 */
		_taskTooltipAdjust:function(el)
		{
			if (!el || !el.parentNode) return;

			var pos_l = 0, max_l;
			var tooltip_el = dojo.byId('tooltip_taskedit');

			if (dojo.attr(el.parentNode, 'class') == 'actionDescription') {
				max_l = dojo.byId('activityFeedContainer').offsetWidth + 70;

				//Item in AcivityFeed
				pos_l = findTextWidth(el) + 20;		//20 - for paddingLeft
				if (pos_l > max_l) {
					pos_l = max_l;
				}
			} else {
				if (el.parentNode.id == 'timetable') {
					max_l = dojo.byId('timetable').offsetWidth + 70;

					//Item in timetable
					pos_l = findTextWidth(el) + 95;		//95 - for paddingLeft
					if (pos_l > max_l) pos_l = max_l;

					pos_l = pos_l + 35;					//35 for arrow space
				} else {
					max_l = dojo.byId('day').offsetWidth - 35;

					//Item in all day tasks
					pos_l = findTextWidth(el) + 45;		//45 - for paddingLeft
					if (pos_l > max_l) pos_l = max_l;

					pos_l = pos_l + 35;					//35 for arrow space
				}
			}

			pos_l += findPosX(el);
			if (pos_l > max_l) pos_l = max_l;
			/* Feature #1721
			Tooltip.open('taskedit', "", el, null, 'right');
			tooltip_el.style.left = Math.round(pos_l) + 'px';

			HiTaskCalendar.taskTooltipAdjustVerticalPos();
			*/
			//Use the onEnd callback on the fade in animation to remove class that hides the close icon
			//We need to do this because the taskedit tooltip content is from itemProperties, which is
			//also used by tasks in the team, project, etc. views
			Tooltip.createTooltipDialog('taskedit', "", el, null, 'after-centered',null,{onEnd:function() {
					setTimeout(function() {
						var closeNode = dojo.query('#tooltip_taskedit_TooltipDialog .hiTaskTooltipDialogTaskClose');
						if (closeNode && closeNode.length > 0) {
							dojo.removeClass(closeNode[0],'taskEdit_Close');
						}
					}, 500);
				},
				orient: ['after-centered']
			});
			// dojo.style(dijit.byId('tooltip_taskedit_TooltipDialog').domNode.parentNode,'left',pos_l+'px');
			// HiTaskCalendar._taskTooltipAdjustVerticalPos();
		},

		/**
		 * Feature #1721
		 */
		_taskTooltipAdjustVerticalPos: function(target){
			//Fix vertical position if needed
			var tooltip_el =/*Feature #1721  dojo.byId('tooltip_taskedit');*/dijit.byId('tooltip_taskedit_TooltipDialog').domNode.parentNode;
			var task = this.tooltipTaskList.has(this.id);
			var tasks_el = task ? task.domNode : null;
			var triangle = dojo.query('.dijitTooltipConnector', /* Feature #1721 tooltip_el*/dojo.byId('tooltip_taskedit_TooltipDialog'))[0];
			var tp = target && dojo.position(target);		//target position
			//Reset position
			// dojo.global.showElement(triangle);
			try { //problems with IE?
				tooltip_el.style.marginTop = '';
				triangle.style.marginTop = '0px';
			} catch (e) {}

			//Only if expanded
			if (dojo.hasClass(dojo.byId('tooltip_taskedit'), 'expanded')) {
				var tt_h = tooltip_el.offsetHeight;

				var top_pos = parseInt(tooltip_el.style.top) + tt_h / 2;
				var body_h = dojo.coords(document.body).h;

				if (top_pos > body_h) {
					//Move tooltip up
					var y_overflow = top_pos - body_h;
					tooltip_el.style.marginTop = Math.round(body_h - top_pos - tt_h / 2) + 'px';
					triangle.style.marginTop = Math.round(tt_h / 2 + y_overflow) + 'px';
				} else {
					tooltip_el.style.marginTop = - Math.round(tt_h / 2) + 'px';
					triangle.style.marginTop = Math.round(tt_h / 2) + 'px';
				}
			} else {
				setTimeout(function () {
					var tt_h = tooltip_el.offsetHeight;
					var tt_w = tooltip_el.offsetWidth;
					var offset = tt_h - 270;
					var top, left;
					var position = dojo.position(tooltip_el);
					var wb = dojo.window.getBox();		//window box

					// if (tp) {
					// 	top = tp.y + tp.h / 2 - tt_h / 2;
					// 	left = tp.x + tp.w / 2;

					// 	triangle.style.marginTop = position.h / 2 - dojo.style(triangle, 'top') - triangle.clientHeight / 2 + 'px';

					// 	if (top < 0 || top + position.h > wb.h || left < 0 || left + position.w > wb.w) {
					// 		tooltip_el.style.marginTop = '';
					// 		tooltip_el.style.top = (wb.h - position.h) / 2 + 'px';
					// 		tooltip_el.style.left = (wb.w - position.w) / 2 + 'px';
					// 		dojo.global.hideElement(triangle);
					// 		// console.warn('invalid tooltip');
					// 	} else {
					// 		tooltip_el.style.top = top + 'px';
					// 		tooltip_el.style.left = left + 'px';
					// 	}
					// 	return;
					// }
					tooltip_el.style.marginTop = -129 - Math.round(offset / 2) + 'px';
					triangle.style.marginTop = 129 + Math.round(offset / 2) + 'px';

					//dojo.style(dijit.byId('tooltip_taskedit_TooltipDialog').domNode.parentNode,'marginTop',-129 - Math.round(offset / 2) + 'px');
					//triangle.style.marginTop = Math.round(tt_h / 2 - 134) + 'px';

					if (position.y < 0) {
						// #2370
						// Huge items sometimes go over the top of document.
						var margin = dojo.position(triangle).y - 25; // Distance to the top of document.
						tooltip_el.style.marginTop = -1 * margin + 'px';
						triangle.style.marginTop = margin + 'px';
					}
				}, 1);
			}
		},

		taskTooltipAdjustVerticalPos: function () {
			//Fix vertical position if needed
			var tooltip_el = dojo.byId('tooltip_taskedit');
			var task = this.tooltipTaskList.has(HiTaskCalendar.id);
			var tasks_el = task ? task.domNode : null;
			var triangle = dojo.query('.triangle', tooltip_el)[0];

			//Reset position
			try { //problems with IE?
				tooltip_el.style.marginTop = '';
				triangle.style.marginTop = '0px';
			} catch (e) {}

			//Only if expanded
			if (dojo.hasClass(dojo.byId('tooltip_taskedit'), 'expanded')) {
				var tt_h = tooltip_el.offsetHeight;

				var top_pos = parseInt(tooltip_el.style.top) + tt_h / 2;
				var body_h = dojo.coords(document.body).h;

				if (top_pos > body_h) {
					//Move tooltip up
					var y_overflow = top_pos - body_h;
					tooltip_el.style.marginTop = Math.round(body_h - top_pos - tt_h / 2) + 'px';
					triangle.style.marginTop = Math.round(tt_h / 2 + y_overflow) + 'px';
				} else {
					tooltip_el.style.marginTop = - Math.round(tt_h / 2) + 'px';
					triangle.style.marginTop = Math.round(tt_h / 2) + 'px';
				}
			} else {
				setTimeout(function () {
					var tt_h = tooltip_el.offsetHeight;
					var offset = tt_h - 270;
					tooltip_el.style.marginTop = -129 - Math.round(offset / 2) + 'px';
					triangle.style.marginTop = Math.round(tt_h / 2 - 134) + 'px';
				}, 1);
			}
		},

		/* For #2973 */
		closeOpenedTooltip: function () {
			if (HiTaskCalendar.id) {
				//Remove item
				var item = hi.items.manager.item(HiTaskCalendar.id, 'tooltip');
				var editingId = hi.items.manager.editingId();

				// Bug #5423
				if (item && editingId && editingId == this.id) {
					item.editingView.cancel();
				}
				HiTaskCalendar.tooltipTaskList.remove({
					"id": HiTaskCalendar.id
				});

				HiTaskCalendar.id = null;
				Project.openedInstance = null;
				var openedIntance = hi.items.manager.opened();
				if (openedIntance && openedIntance._setOpenedAttr) {
					openedIntance._setOpenedAttr(openedIntance, false);
				}
				Tooltip.close('taskedit');
			}
		},

		closeTaskEditTooltipIfOpen: function() {
			if (this.taskEditTooltipVisible()) {
				this.closeOpenedTooltip();
			}
		},

		getTaskTime: function(top){
			var hours;
			var minutes;

			minutes = Math.round(top / this.pixelsPerMinute);

			hours = Math.floor(minutes / 60);
			minutes -= hours * 60;

			var startTime = this.startTime.split(':');
			hours += parseInt(startTime[0], 10);
			minutes += parseInt(startTime[1], 10);

			var tmp = minutes % this.minutesPrecision;
			if(tmp){
				minutes = minutes + this.minutesPrecision - tmp;
			}

			while(minutes >= 60){
				hours++;
				minutes -= 60;
			}

			return (hours < 10 ? '0' : '') + hours + ':' + (minutes < 10 ? '0' : '') + minutes;
		},

		getTaskPeriod: function(taskID) {
			taskID = parseIntRight(taskID);
			var taskData = this.dayData['thisDay']['notWholeDay'][taskID];
			var start_time, end_time;
			var item = hi.items.manager.get(taskID), tmp;

			if(this.taskDragType == 2) {
				tmp = item.is_all_day ? false : str2timeAPI(item.start_date);
				start_time = tmp ? time2str(tmp, "h:i") : false;
			} else {
				start_time = this.getTaskTime(taskData['top'] + taskData['time_top']);
			}

			if((this.taskDragType == 1) && (item.end_time != '' && item.end_time != null)) {
				tmp = item.is_all_day ? false : str2timeAPI(item.end_date);
				end_time = tmp ? time2str(tmp, "h:i") : false;
			} else {
				end_time = this.getTaskTime(taskData['top'] + taskData['time_top'] + taskData['time_height']);
			}

			return HiTaskCalendar.formatTime(start_time) + (end_time != start_time ? ' - ' + HiTaskCalendar.formatTime(end_time) : '');

		},

		from24to12: function(s, return_array) {
			return_array = return_array || false;
			if (s.constructor != Array) {
				if (s.constructor != String) return '';
				s = s.split(':');
			}
			if (s.length < 2) return '';
			s[0] = parseInt(s[0], 10);
			if (s[0] == 24) s[0] = 0;
			var i, imax = s.length;
			for (i = 1; i < imax; i++) {
				s[i] = strPadLeft(parseInt(s[i], 10), 2, '0');
			}
			var am_pm = dojo.i18n.getLocalization("dojo.cldr", "gregorian")['dayPeriods-format-wide-am'];
			if (s[0] >= 12) {
				s[0] -= 12;
				am_pm = dojo.i18n.getLocalization("dojo.cldr", "gregorian")['dayPeriods-format-wide-pm'];
			}
			if (s[0] == 0) {
				s[0] = 12;
			}
			if (return_array) {
				s.push(am_pm);
				return s;
			}
			return s.join(':') + ' ' + am_pm;
		},
		from12to24: function(s, return_array) {
			return_array = return_array || false;
			var am_pm;
			if (s.constructor != Array) {
				if (s.constructor != String) return '';
				s = s.split(':');
				if (s.length < 2) return '';
				am_pm = s[s.length - 1].toLowerCase().indexOf('p') != -1 ? 12 : 0;
			} else {
				if (s.length < 2) return '';
				am_pm = s.pop().toLowerCase().indexOf('p') != -1 ? 12 : 0;
			}
			var i, imax = s.length;
			for (i = 0; i < imax; i++) {
				s[i] = parseInt(s[i], 10);
			}
			if (s[0] == 12) {
				s[0] = 0;
			}
			s[0] += am_pm;
			for (i = 0; i < imax; i++) {
				s[i] = strPadLeft(s[i], 2, '0');
			}
			if (return_array) return s;
			return s.join(':');
		},
		formatTime: function(s) {
			if (HiTaskCalendar.time_format == 12) {
				return HiTaskCalendar.from24to12(s);
			}
			return s;
		},
		standartTime: function(s) {
			if (HiTaskCalendar.time_format == 12) {
				return HiTaskCalendar.from12to24(s);
			}
			return s;
		},

		drawTimeTable: function (){
			var el = Project.getContainer('HiTaskCalendar_drawTimeTable');
			el = dojo.byId(el);
			var start = parseInt(HiTaskCalendar.startTime, 10);
			var end = parseInt(HiTaskCalendar.endTime, 10);
			start = 0;
			end = 24;
			var i, html = '', time;
			for (i = start; i < end; i++) {
				time = [strPadLeft(i, 2, '0'), 0];
				if (HiTaskCalendar.time_format == 12) {
					time = HiTaskCalendar.from24to12(time, true);
				}
				html += '<div><table class="' + ((i - start) % 2 ? 'odd' : 'even') + '"><tr><td class="hour">' + time[0] + '</td><td>00&nbsp;';
				if (HiTaskCalendar.time_format == 12) {
					html += '<span class="am_pm">' + time[2] + '</span>';
				}
				html += '</td></tr></table></div>';
			}
			el.innerHTML = html;
		},

		timeRegExp: function() {
			if (HiTaskCalendar.time_format == 12) {
				return /^[0-9]{1,2}(:[0-9]{0,2}(( )?(p|a|pm|am)?))?$/i;
			}
			return /^[0-9]{1,2}(:[0-9]{0,2})?$/;
		},

		/**
		 * Moves calendar to the next day at midnight
		 */
		rolloverNextDay: function(init) {
			var n = new Date();

			if (!init) {
				this.moveToday();

				var prev = new Date(n);
				prev.setDate(prev.getDate() - 1);

				var prevStr = this.getStrFromDate(prev);
				var currStr = this.getCurrentDateStr();

				if (currStr == prevStr) {
					this.changeDate(n);
				}
				if (typeof Project != 'undefined') {
					Project.rolloverNextDay();
				}
			}

			var next = new Date(n);
			next.setHours(0);
			next.setMinutes(0);
			next.setSeconds(0);
			next.setDate(next.getDate() + 1);

			var interval = next.valueOf() - n.valueOf();
			if (interval < 1) {
				interval = 1;
			}

			dojo.global.setTimeout(function(){
				HiTaskCalendar.rolloverNextDay();
			}, interval);
		},

		moveToday: function() {
			this.today = new Date();
			this.drawToday();
		},

		getTimetableScope: function() {
			var start = new Date(this.currentDate);
			var end = new Date(this.currentDate);
			var startTime = this.startTime.split(':');
			var endTime = this.endTime.split(':');
			startTime[0] = parseInt(startTime[0], 10);
			startTime[1] = parseInt(startTime[1], 10);
			endTime[0] = parseInt(endTime[0], 10);
			endTime[1] = parseInt(endTime[1], 10);
			start.setHours(startTime[0]);
			start.setMinutes(startTime[1]);
			end.setHours(endTime[0]);
			end.setMinutes(endTime[1]);
			return [start, end];
		},

		moveTime: function(n) {
			n = parseInt(n);
			if (!isFinite(n)) {
				return;
			}
			var start = this.startTime.split(':');
			var end = this.endTime.split(':');
			start[0] = parseInt(start[0], 10);
			end[0] = parseInt(end[0], 10);
			if (start[0] + n < 0) {
				n = - start[0];
			}
			if (end[0] + n > 24) {
				n = 24 - end[0];
			}
			if (!n) return;
			start[0] += n;
			end[0] += n;
			var tmp = end[0];
			if (start[0] < 10) start[0] = '0' + start[0];
			if (end[0] < 10) end[0] = '0' + end[0];
			this.startTime = start.join(':');
			this.endTime = end.join(':');
			end[0] = tmp - 1;
			end[1] = 59;
			if (end[0] < 10) end[0] = '0' + end[0];
			this.lastStartTime = end.join(':');
			this.drawTimeTable();
			this.setDayData();
			this.draw();
			Hi_Preferences.set('ttstart', this.startTime);
			Hi_Preferences.set('ttend', this.endTime);
		}
	};

	dojo.global.HiTaskCalendar_drawTimeTable = function() {
		return HiTaskCalendar.drawTimeTable();
	};

	return dojo.global.HiTaskCalendar;

});
