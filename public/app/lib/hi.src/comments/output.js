/**
 * Output formatting
 */
define(function () {

	Hi_Comments.Output = {
		
		/*
		 * Localized field names
		 * @type {Object}
		 */
		field_names: {
			title: hi.i18n.getLocalization('history.title'),
			start: hi.i18n.getLocalization('history.start_time'),
			end: hi.i18n.getLocalization('history.end_time'),
			message: hi.i18n.getLocalization('history.description'),
			color: hi.i18n.getLocalization('history.color'),
			recurring: hi.i18n.getLocalization('history.recurring'),
			reminder: hi.i18n.getLocalization('history.reminder'),
			tag: hi.i18n.getLocalization('history.tag'),
			time_track: hi.i18n.getLocalization('history.time_track'),
			time_est: hi.i18n.getLocalization('history.time_est')
		},
		
		/**
		 * Create HTML for heading
		 * 
		 * @param {Object} data
		 * @return HTML
		 * @type {String}
		 */
		formatCaption: function (data) {
			var since = Hi_Comments.Date.timeDifString(data.time)
			var datetime = Hi_Comments.Date.formatDate(data.time);
			var name = (data.user_name || '');
			if (!name && data.user_id) {
				var friend = Project.getFriend(data.user_id);
				name = (friend ? friend.name : '');
			}
			
			var o = '<div class="caption">' +
						'<span class="datetime"><b>' + since + ' ' + hi.i18n.getLocalization('history.ago') + '</b> ' + datetime + '</span>' +
						'<span class="name">' + name + '</span>' +
					'</div>';
			
			return o;
		},
		
		/**
		 * Create HTML for comment
		 * 
		 * @param {Object} data
		 * @return HTML
		 * @type {String}
		 */
		formatComment: function (data) {
			var task_id = Hi_Comments.getTaskId();
			
			var is_owner = (data.user_id == Project.user_id);
			var can_delete = Hi_Comments.canDeleteComment(data);
			var avatar = dojo.global.getAvatar(16, data.user_id) || null;
			var comment = formatMessage(data.comment, true);
			var comment_id = typeof data.comment_id == 'string' ? "'" + data.comment_id + "'" : data.comment_id;
			
			var o = '<div class="comment' + (can_delete ? ' can-delete' : '') + (is_owner ? ' comment-owner' : '') + '" id="comment' + data.comment_id + '"><div class="comment-inner">' +
						'<span class="avatar">' + (avatar ? ('<img src="' + avatar + '" alt="" width="16px" height="16px" />') : '') + '</span>' +
						'<div></div><a class="delete" onclick="Hi_Comments.handleDeleteComment(' + comment_id + ', this)"></a><p class="forceWrap">' + comment + '</p>' + 
					'</div></div>';
					
			return o;
		},
		
		/**
		 * Create HTML for change description
		 * @param {Object} data
		 */
		formatChanges: function (data) {
			
			var tpl;
			var tplVal = {};
			var tplIndex = 'history_item_';
			var colorMap = {0:hi.i18n.getLocalization('project.color_none'),
							1:hi.i18n.getLocalization('project.color_red'),
							2:hi.i18n.getLocalization('project.color_orange'),
							3:hi.i18n.getLocalization('project.color_yellow'),
							4:hi.i18n.getLocalization('project.color_green'),
							5:hi.i18n.getLocalization('project.color_blue'),
							6:hi.i18n.getLocalization('project.color_purple'),
							7:hi.i18n.getLocalization('project.color_gray')
							};
			var recurMap = {0:hi.i18n.getLocalization('project.none'),
							1:hi.i18n.getLocalization('project.daily'),
							2:hi.i18n.getLocalization('project.weekly'),
							3:hi.i18n.getLocalization('project.monthly'),
							4:hi.i18n.getLocalization('project.yearly')
							};
	
			// property prepare
			if (data.property_name in this.field_names){
				tplVal['property'] = this.field_names[data.property_name];
			}else{
				tplVal['property'] = data.property_name;
			}
			
			// value prepare
			if(data.value != undefined){
				
				if(data.property_name == 'priority'){
					
					if(data.value >= 30000){
						tplVal['value'] = hi.i18n.getLocalization('priority.high');
					}else if(data.value < 20000){
						tplVal['value'] = hi.i18n.getLocalization('priority.low');
					}else{
						tplVal['value'] = hi.i18n.getLocalization('priority.medium');
					}
					
				}else if(data.property_name == 'color'){
					tplVal['value'] = colorMap[data.value];
				}else if(data.property_name == 'recurring'){
					tplVal['value'] = recurMap[data.value];
				}else if(data.property_name == 'reminder_time' || data.property_name == 'reminder_type'){
					data.property_name = 'reminder_changed';
				}else if(data.property_name == 'message'){
					data.property_name = 'message_changed';
				}else{
				
					if(data.property_type == 'BOOLEAN'){
						tplVal['value'] = (data.value)?hi.i18n.getLocalization('history.enabled'):hi.i18n.getLocalization('history.disabled');
					}else if(data.property_type == 'DATE'){
						try{
							tplVal['value'] = Hi_Comments.Date.formatDate(str2timeAPI(data.value));
						}catch(e){
							tplVal['value'] = data.value;
						}
					}else{
						tplVal['value'] = data.value;
					}
				
				}
			}
			
			// init teamplate
			if(data.property_type == 'BOOLEAN'){
				// use "history_item_property_boolean" teamplate
				tpl = dojo.byId(tplIndex + data.property_name + '_' + data.value);
				if(!tpl){
					tpl = dojo.byId(tplIndex + 'changed_boolean');
				}
			}else{
				// use "history_item_property" teamplate
				tpl = dojo.byId(tplIndex + data.property_name);
			}
			if(!tpl && data.property_type == 'VOID'){
				// use "history_item_void" teamplate
				tpl = dojo.byId(tplIndex + 'void');
			}
			if(!tpl){
				// use "history_item_changed" teamplate
				tpl = dojo.byId(tplIndex + 'changed');
				if(!data.value){
					tplVal['value'] = hi.i18n.getLocalization('history.no_value');
				}
			}
			if(!tpl){
				ALERT('No history element  "' + data.property_type + '"');
				return;
			}
			tplVal['user_name'] = data.user_name;
			
			tpl = tpl.cloneNode(true);
			tpl.id = null;
			fillTemplate(tpl, tplVal);
			var html = tpl.innerHTML;
			delete(tpl);
			
			return html;
			
		},
		
		/**
		 * Render comment list
		 * 
		 * @param {Object} data
		 * @return Generated HTML
		 * @type {String}
		 */
		render: function (data) {
			if (!Hi_Comments.getTaskId()) return;
			
			var o = '';
			var data = (data ? data : Hi_Comments.data);
			var lasttime = 0;
			var lastuser = '0';
			var d = '';
			
			for(var i=0,ii=data.length; i<ii; i++) {
				var newtime = data[i].time.getTime();
				
				if (Hi_Comments.isComment(data[i])) {
					var commentData = {
						'comment_id': data[i].id,
						'comment': data[i].value,
						'time': str2timeAPI(data[i].time),
						'user_id': data[i].user_id,
						'user_name': data[i].user_name
					};
					d = this.formatComment(commentData);
				} else {
					d = this.formatChanges(data[i]);
				}
				
				if (d && ((lastuser != data[i].user_id) || (Math.abs(newtime - lasttime) > 60000))) {
					o += this.formatCaption(data[i]);
				}
				if (d) {
					o += d;
					lasttime = newtime;
					lastuser = data[i].user_id;
				}
			}
			
			var node = dojo.query('div.cont.fulllist div.dataEl', Hi_Comments.container);
			
			if (node.length) {
				node[0].innerHTML = o;
				
				//Scroll into view
				setTimeout(function () {
					var order = Hi_Preferences.get('comment_order', 0, 'int');
					if (order) {
						//Latest at the top
						node[0].scrollTop = 0;
					} else {
						//Latest at the bottom
						node[0].scrollTop = 9000;
					}
				}, 16);
			}
			
			return o;
		}
	};
	
	return dojo.global.Hi_Comments.Output;
	
});