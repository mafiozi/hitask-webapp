package beans;

public class HelpTopic {
	private String id;
	private String name;
	private int index;
	
	public HelpTopic(String id, String name, int index) {
		this.id = id;
		this.name = name;
		this.index = index;
	}
	
	public HelpTopic(String id, String name) {
		this(id, name, 0);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
}