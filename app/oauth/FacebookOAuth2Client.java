package oauth;

import java.util.Map;

public class FacebookOAuth2Client extends OAuth2Client {
	public FacebookOAuth2Client(String authorizationURL, String accessTokenURL, String clientid, String secret, String providerName) {
		super(authorizationURL, accessTokenURL, clientid, secret, providerName);
	}
	
	public FacebookOAuth2Client(String authorizationURL, String accessTokenURL, String clientid, String secret, String providerName, String icon) {
		super(authorizationURL, accessTokenURL, clientid, secret, providerName, icon);
	}
	
	public FacebookOAuth2Client(String authorizationURL, String accessTokenURL, String clientid, String secret, String providerName, String icon, String buttonClass) {
		super(authorizationURL, accessTokenURL, clientid, secret, providerName, icon, buttonClass);
	}
	
	@Override
	public void retrieveVerificationCode(String callbackURL, Map<String, String> parameters, String state) {
		parameters.put("scope", "email");
		super.retrieveVerificationCode(callbackURL, parameters, state);
	}
}
