/*
dojo.provide("hi.timetracking");

dojo.require("hi.timetracking.timer");
dojo.require("hi.timetracking.base");
dojo.require("hi.timetracking.add");
dojo.require("hi.timetracking.personselect");
dojo.require("hi.timetracking.list");
dojo.require("hi.timetracking.report");
*/
define([
	"hi/timetracking/timer",
	"hi/timetracking/base",
	"hi/timetracking/add",
	"hi/timetracking/personselect",
	"hi/timetracking/list",
	"hi/timetracking/report"
], function () {
	return {};
});