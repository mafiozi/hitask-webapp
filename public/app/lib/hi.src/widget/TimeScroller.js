define([
	"dijit/_Widget",
	"dijit/_Templated"
], function (_Widget, _Templated) {
	dojo.declare("hi.widget.TimeScroller", [_Widget, _Templated], {
		keys: "", 

		templateString: "<div><div dojoAttachPoint=\"timeScrollerNode\" dojoAttachEvent=\"onclick: _onClick\"><!-- --></div></div>",

		type: 'up', 

		fillInTemplate:function () {
			if (this.type == 'up') {
				this.timeScrollerNode.className = 'icon-chevron-up '+'TimeScroller_' + this.type;
			} else {
				this.timeScrollerNode.className = 'icon-chevron-down '+'TimeScroller_' + this.type;
			}
		},
		 _onClick: function() {
			if (this.type == 'up') {
				HiTaskCalendar.moveTime(-1);
			} else {
				HiTaskCalendar.moveTime(1);
			}
		}
	});
	
	return hi.widget.TimeScroller;
});