/**
 * Copyright (C) 2005-2006 Vide Infra Grupa SIA, Riga, Latvia. All rights reserved.
 * http://videinfra.com
 * Version 1.1
 */
define([
	"dojo/cldr/nls/gregorian",
	"dojo/cldr/supplemental",
	"dojo/date/locale",
	'dojo/on',
	'dojo/query',
	'dojo/dom-construct',
	'dojo/json'
], function (djGregorian, djSupplemental, djDateLocale, dojoOn, dojoQuery, domConstruct, dojoJson) {
	dojo.getObject('dojo.cldr.nls.gregorian', true);
	
	dojo.cldr.nls.gregorian.en_gb = {
		"dateFormatItem-yM": "M/yyyy", 
		"field-dayperiod": "AM/PM", 
		"dateFormatItem-yQ": "Q yyyy", 
		"field-minute": "Minute", 
		"eraNames": [
			"Before Christ", 
			"Anno Domini"
		], 
		"dateFormatItem-MMMEd": "E, MMM d", 
		"dateFormatItem-yQQQ": "QQQ yyyy", 
		"field-weekday": "Day of the Week", 
		"days-standAlone-wide": capitalizeFirstLetter(djDateLocale.getNames('days', 'wide')), 
		"dateFormatItem-MMM": "LLL", 
		"months-standAlone-narrow": capitalizeFirstLetter(djDateLocale.getNames('months', 'abbr')), 
		"field-era": "Era", 
		"field-hour": "Hour", 
		"dateFormatItem-y": "yyyy", 
		"timeFormat-full": "h:mm:ss a v", 
		"months-standAlone-abbr": capitalizeFirstLetter(djDateLocale.getNames('months', 'abbr')), 
		"dateFormatItem-yMMM": "MMM yyyy", 
		"days-standAlone-narrow": capitalizeFirstLetter(djDateLocale.getNames('days', 'narrow')),
		"eraAbbr": [
			"BC", 
			"AD"
		], 
		"dateFormat-long": "MMMM d, yyyy", 
		"timeFormat-medium": "h:mm:ss a", 
		"field-zone": "Zone", 
		"dateFormatItem-Hm": "HH:mm", 
		"dateFormat-medium": "MMM d, yyyy", 
		"dateFormatItem-Hms": "HH:mm:ss", 
		"quarters-standAlone-wide": [
			"1st quarter", 
			"2nd quarter", 
			"3rd quarter", 
			"4th quarter"
		], 
		"dateTimeFormat": "{1} {0}", 
		"dateFormatItem-yMMMM": "MMMM yyyy", 
		"dateFormatItem-ms": "mm:ss", 
		"field-year": "Year", 
		"quarters-standAlone-narrow": [
			"1", 
			"2", 
			"3", 
			"4"
		], 
		"field-week": "Week", 
		"months-standAlone-wide": capitalizeFirstLetter(djDateLocale.getNames('months', 'wide')), 
		"dateFormatItem-MMMMEd": "E, MMMM d", 
		"dateFormatItem-MMMd": "MMM d", 
		"timeFormat-long": "h:mm:ss a z", 
		"months-format-abbr": capitalizeFirstLetter(djDateLocale.getNames('months', 'abbr')), 
		"timeFormat-short": "h:mm a", 
		"field-month": "Month", 
		"dateFormatItem-MMMMd": "MMMM d", 
		"quarters-format-abbr": [
			"Q1", 
			"Q2", 
			"Q3", 
			"Q4"
		], 
		"days-format-abbr": capitalizeFirstLetter(djDateLocale.getNames('days', 'abbr')), 
		"pm": "PM", 
		"dateFormatItem-M": "L", 
		"days-format-narrow": capitalizeFirstLetter(djDateLocale.getNames('days', 'narrow')), 
		"field-second": "Second", 
		"field-day": "Day", 
		"dateFormatItem-MEd": "E, M/d", 
		"months-format-narrow": [
			"J", 
			"F", 
			"M", 
			"A", 
			"M", 
			"J", 
			"J", 
			"A", 
			"S", 
			"O", 
			"N", 
			"D"
		], 
		"dateFormatItem-hm": "h:mm a", 
		"am": "AM", 
		"days-standAlone-abbr": capitalizeFirstLetter(djDateLocale.getNames('days', 'abbr')), 
		"dateFormat-short": "M/d/yy", 
		"dateFormatItem-yMMMEd": "EEE, MMM d, yyyy", 
		"dateFormat-full": "EEEE, MMMM d, yyyy", 
		"dateFormatItem-Md": "M/d", 
		"dateFormatItem-yMEd": "EEE, M/d/yyyy", 
		"months-format-wide": capitalizeFirstLetter(djDateLocale.getNames('months', 'wide')), 
		"dateFormatItem-d": "d", 
		"quarters-format-wide": [
			"1st quarter", 
			"2nd quarter", 
			"3rd quarter", 
			"4th quarter"
		], 
		"days-format-wide": capitalizeFirstLetter(djDateLocale.getNames('days', 'wide')), 
		"eraNarrow": [
			"B", 
			"A"
		]
	};
	
	dojo.cldr.supplemental.getFirstDayOfWeek = function(locale) {
		//Sunday (returns 0), Monday (returns 1), ...
		return parseInt(Hi_Preferences.get('first_day_of_week'));
	};
	
	
	
	ISIE = dojo.isIE && dojo.isIE < 9;
	ISOPERA = dojo.isOpera;
	if (ISOPERA) {
		ISIE = false;
	}
	
	var _daysNarrow = capitalizeFirstLetter(djDateLocale.getNames('days', 'narrow'));
	
	return dojo.global.Hi_Calendar = {
		opened: {},
		months: capitalizeFirstLetter(djDateLocale.getNames('months', 'wide')),
		months_short: capitalizeFirstLetter(djDateLocale.getNames('months', 'abbr')),
		days: _daysNarrow.splice(1, _daysNarrow.length - 1).concat(_daysNarrow[0]),
		scroll: false,
		scrollCount: 0,
		noclose: false,
		startDay: 1,
		format: 'd.m.y',
		custom_time_position_x: '',
		custom_time_position_y: '',
		sqlFormat: function(s) {
			if (s == '') {
				s = '';
			} else if (tmp = str2time(s, Hi_Calendar.format)) {
				s = time2str(tmp, 'y-m-d');
			} else if (tmp = str2time(s, 'y-m-d')) {
				s = time2str(tmp, 'y-m-d');
			} else {
				s = false;
			}
			return s;
		},
		makeDateExp: function () {
			if (Hi_Calendar.dateExp) return Hi_Calendar.dateExp;
			var dateExp = Hi_Calendar.format;
			dateExp = escapeExpression(dateExp);
			dateExp = dateExp.replace(/(\\?.)/g, '$1?');
			dateExp = dateExp.replace(/d|m/g, '[0-9]{0,2}');
			dateExp = dateExp.replace(/y/g, '[0-9]{0,4}');
			Hi_Calendar.dateExp = '^' + dateExp + '$';
			return Hi_Calendar.dateExp;
		},
		dateExp: false,
		timetable: function(e, id, startTime, endTime, customInputClass) {
			var el = dijit.byId(id);
			if (el.disabled) return;
			return Hi_Calendar._process(e, id, startTime, endTime, 'time', customInputClass);
		},
		calendar: function(e, id, startDate, endDate, customInputClass, silent) {
			return Hi_Calendar._process(e, id, startDate, endDate, false, customInputClass, silent);
		},
		
		_process: function (e, id, startDate, endDate, type, customInputClass, silent) {
		try {	
			
			
			type = type || false;
			dateType = (type != 'time');
			timeType = (type == 'time');
			
			id = id || false;
			var ID;
			
			if (!e.target) {
				if (!id) {
					var el = e.srcElement.previousSibling;
					while (el && el.tagName != 'INPUT') {
						el = el.previousSibling;
						if (!el) return;
					}
					if (el) ID = el.id;
				}
			} else {
				if (!id) {
					var el = e.target.previousSibling;
					while (el && el.tagName != 'INPUT') {
						el = el.previousSibling;
						if (!el) return;
					}
					if (el) ID = el.getAttribute('id');
				}
			}
			
			if (id) ID = id;
			el = dojo.byId(ID);
			if (el) { 
				if (!el._i) {
					
					var old_instance = dijit.byId(ID);
					if (old_instance) old_instance.destroy();
					
					var val = el.value;
					var instance = dojo.parser.instantiate([el])[0];
					el = dojo.byId(id);
					if (customInputClass) {
						el.className = customInputClass;
					}
					else {
						el.className += ' txt';
					}
					
					dojo.connect(el, 'onkeyup', function () {
						if (el._i && el._i._opened) {
							el._i.closeDropDown();
						}
					});
					
					if (instance && instance.setDisplayedValue) {
						if (val) instance.setDisplayedValue(val);
						el._i = instance;
					}
				}
				
				if (!silent) {
					if (el._i) {
						el._i.focus();
						el._i.openDropDown();
					} else {
						el.focus();
					}
				}
				
				var val = el.value;
				if (!val) {
					if (ID.indexOf('end_date') !== -1) {
						var start_el = dojo.byId(ID.replace('end_date', 'start_date'));
						var end_el = dojo.byId(ID.replace('start_date', 'end_date'));
						
						if (start_el && start_el.value) {
							var date = str2time(start_el.value, Hi_Calendar.format) || new Date();
							if (end_el._i && end_el._i._picker) end_el._i._picker.setValue(date);
						}
					}
				}
			}
			}
			catch (e) {
				log(e);
			}
		},
		__process: function(e, id, startDate, endDate, type) {
			type = type || false;
			dateType = (type != 'time');
			timeType = (type == 'time');
			
			id = id || false;
			var ID;
			if (dateType) {
				startDate = startDate || false;
				endDate = endDate || false;
				if (startDate && startDate.toLowerCase() == 'now') {
					startDate = new Date();
				} else if (startDate) {
					startDate = str2time(startDate, Hi_Calendar.format) || new Date();
				}
				if (endDate && endDate.toLowerCase() == 'now') {
					endDate = new Date();
				} else if (endDate) {
					endDate = str2time(endDate, Hi_Calendar.format) || new Date();
				}
			}
			
			if (!e.target) {
				if (!id) {
					var el = e.srcElement.previousSibling;
					while (el && el.tagName != 'INPUT') {
						el = el.previousSibling;
						if (!el) return;
					}
					if (el) ID = el.id;
				}
				var buttonNode = e.srcElement;
			} else {
				if (!id) {
					var el = e.target.previousSibling;
					while (el && el.tagName != 'INPUT') {
						el = el.previousSibling;
						if (!el) return;
					}
					if (el) ID = el.getAttribute('id');
				}
				var buttonNode = e.target;
			}
			var x = findPosX(buttonNode, true) + buttonNode.clientWidth;
			var y = findPosY(buttonNode, true);
			if (Hi_Calendar.custom_time_position_x != '' ) {
				x = Hi_Calendar.custom_time_position_x;
			}
			if (Hi_Calendar.custom_time_position_y != '' ) {
				y = Hi_Calendar.custom_time_position_y;
			}
			if (id) ID = id;
			el = dojo.byId(ID);
			if (el.disabled) {
				if (ID in Hi_Calendar.opened) {
					if (dateType) {
						Hi_Calendar.selectDate(ID);
					} else {
						Hi_Calendar.selectTime(ID);
					}
				}
			}
			
			try {
				el.focus();
			} catch (err) {}
			if (!ID) return;
			Hi_Calendar.noclose = ID;
			
			if ((ID in Hi_Calendar.opened)) {
				if (dateType) {
					Hi_Calendar.selectDate(ID);
				} else {
					Hi_Calendar.selectTime(ID);
				}
				return;
			}
			var str = false;
			if (el) str = el.value;
			if (dateType) {
				var sel_time = str2time(str, Hi_Calendar.format) || new Date();
			} else {
				var temp = new Date();
				str = validateTime(str);
				if (str) {
					var temp_h = str[0];
					var temp_m = str[1];
				} else {
					var temp_h = temp.getHours();
					var temp_m = temp.getMinutes();
				}
				temp_m = temp_m - temp_m % 15;
				var sel_time = strPadLeft(temp_h, 2, '0') + ':' + strPadLeft(temp_m, 2, '0');
			}
			
			if (dateType) {
				if (startDate !== false) {
					if (sel_time < startDate) {
						sel_time = startDate;
					}
				}
				if (endDate !== false) {
					if (sel_time > endDate) {
						sel_time = endDate;
					}
				}
			}
			
			Hi_Calendar.opened[ID] = {'date':sel_time, 'button': buttonNode};
			
			if (false && timeType) {
				if (ISIE) {
					y -= 72;
				} else if (ISOPERA) {
					y -= 71;
				} else {
					y -= 70;
				}
			}
			
			var z = dojo.byId("calendar" + ID);
			var z2 = dojo.byId("iframe" + ID);
			
			if (!z) {
				if (ISIE) {
					var z2 = document.createElement("iframe");
					var https_src = document.location.protocol + '\/\/' + document.location.host + '/lib/dojo.1.9.hitask.@build.version@.@build.number@/dojo/resources/blank.html';
					
					z2.id = "iframe" + ID;
					z2.src = https_src;
					z2.setAttribute('src', https_src);
					z2.style.visibility = "hidden";
					z2.style.display = "block";
					z2.style.border = "none";
					z2.style.position = "absolute";
					z2.style.zIndex = 9;
					dojo.body().appendChild(z2);
				}
				var z = document.createElement("div");
				Click.setClickFunction(Hi_Calendar.closeCalendars, false, 'calendar');
				z.onclick = function (ev) {Click.setClickFunction(Hi_Calendar.closeCalendars, true, 'calendar'); dojo.stopEvent(ev); }
				z.id = "calendar" + ID;
				z.style.zIndex = 10;
				z.style.position = "absolute";
				z.style.top = y + "px";
				z.style.left = x + "px";
				dojo.body().appendChild(z);
			} else {
				z.style.top = y + "px";
				z.style.left = x + "px";
				z.style.display = "";
				if (z2) z2.style.display = "";
			}
			
			if (dateType) {
				Hi_Calendar.drawCalendar(ID, startDate, endDate);
			} else {
				Hi_Calendar.drawTimetable(ID, startDate, endDate);
			}
			
			if (z2) {
				z2.style.width = z.offsetWidth;
				z2.style.height = z.offsetHeight;
				z2.style.top = z.style.top;
				z2.style.left = z.style.left;
			}
		},
		moveCalendar: function() {
			var i;
			for (i in Hi_Calendar.opened) {
				var z = dojo.byId("calendar" + i);
				var z2 = dojo.byId("iframe" + i);
				if (!z) return;
				e = Hi_Calendar.opened[i].button;
				if (!e) return;
				x = findPosX(e, true) + e.clientWidth + 5;
				y = findPosY(e, true);
				z.style.top = y + "px";
				z.style.left = x + "px";
				if (z2) {
					z2.style.top = z.style.top;
					z2.style.left = z.style.left;
				}
			}
		},
		drawCalendar: function(ID, startDate, endDate) {
			var cal = dojo.byId("calendar" + ID);
			if (!cal) return;
			var sel_time = Hi_Calendar.opened[ID]['date'];
			
			var html = '';
			html += '<div id="datetime" onclick="Click.setClickFunction(null, true, \'task\')"><div id="calendar_popup" class="calendar">\
			    <div class="top"><div class="back"><!-- --></div><div class="right"><!-- --></div></div>\
			    <table class="cont"><tr><td class="left"></td><td class="back"><div class="border">\
					<div class="navigation topNav">\
						<table><tr><td>\
			        		<a href="javascript://" onmouseout="Hi_Calendar.stopScroll()" onmousedown="Hi_Calendar.startChangeMonth(\'' + ID + '\', -1, \'' + startDate + '\', \'' + endDate + '\')" onmouseup="if(!Hi_Calendar.stopScroll()){Hi_Calendar.changeMonth(\'' + ID + '\', -1, \'' + startDate + '\', \'' + endDate + '\');}"><img src="/img/datetime_calendar_navigation_prev.gif" alt="" /></a>\
						</td><td class="text">';
			html += Hi_Calendar.months[sel_time.getMonth()];
			html += '</td><td>\
			        		<a href="javascript://" onmouseout="Hi_Calendar.stopScroll()" onmousedown="Hi_Calendar.startChangeMonth(\'' + ID + '\', 1, \'' + startDate + '\', \'' + endDate + '\')" onmouseup="if(!Hi_Calendar.stopScroll())Hi_Calendar.changeMonth(\'' + ID + '\', 1, \'' + startDate + '\', \'' + endDate + '\');"><img src="/img/datetime_calendar_navigation_next.gif" alt="" /></a>\
						</td></tr></table>\
			        </div>\
					<div class="navigation bottomNav">\
						<table><tr><td>\
			        		<a href="javascript://" onmouseout="Hi_Calendar.stopScroll()" onmousedown="Hi_Calendar.startChangeYear(\'' + ID + '\', -1, \'' + startDate + '\', \'' + endDate + '\')" onmouseup="if(!Hi_Calendar.stopScroll()){Hi_Calendar.changeYear(\'' + ID + '\', -1, \'' + startDate + '\', \'' + endDate + '\');}"><img src="/img/datetime_calendar_navigation_prev.gif" alt="" /></a>\
						</td><td class="text">';
			html += sel_time.getFullYear();
			html += '</td><td>\
			        		<a href="javascript://" onmouseout="Hi_Calendar.stopScroll()" onmousedown="Hi_Calendar.startChangeYear(\'' + ID + '\', 1, \'' + startDate + '\', \'' + endDate + '\')" onmouseup="if(!Hi_Calendar.stopScroll())Hi_Calendar.changeYear(\'' + ID + '\', 1, \'' + startDate + '\', \'' + endDate + '\');"><img src="/img/datetime_calendar_navigation_next.gif" alt="" /></a>\
						</td></tr></table>\
			        </div>\
					<div class="lt"><div class="rt">\
						<table class="table">\
							<tr>';
			var i;
			for (i = 0; i < 7; i++) {
				html += '<th>' + Hi_Calendar.days[(Hi_Calendar.startDay + i + 6) % 7] + '</th>';
			}
			html += '</tr>';
			var sel_time = new Date(sel_time.getFullYear(), sel_time.getMonth(), sel_time.getDate(), 23, 59, 59, 999);
			var tmp = new Date(sel_time.getFullYear(), sel_time.getMonth(), sel_time.getDate(), 23, 59, 59, 999);
			var tmp2 = new Date(sel_time.getFullYear(), sel_time.getMonth(), sel_time.getDate(), 0, 0, 0, 0);
		
			tmp.setDate(1);
			tmp.setDate(Hi_Calendar.startDay - ((tmp.getDay() - 1) % 7 < 0 ? (tmp.getDay() - 1) % 7 + 7 : (tmp.getDay() - 1) % 7));
			var today = new Date(startDate);
			var today2 = new Date((new Date(endDate)).valueOf() + 24*60*60*1000);
			for (j = 0; j < 6; j++) {
				html += '<tr>';
				for (i = 0; i < 7; i++) {
					html += '<td';
					html += ' class="'
					if (tmp.getMonth() != sel_time.getMonth()) {
						html += ' disabled';
					}
					if (sel_time.getDate() == tmp.getDate() && sel_time.getMonth() == tmp.getMonth() && sel_time.getFullYear() == tmp.getFullYear()) {
						html += ' selected';
					}
					if ((new Date()).getDate() == tmp.getDate() && (new Date()).getMonth() == tmp.getMonth() && (new Date()).getFullYear() == tmp.getFullYear()) {
						html += ' today';
					}
					html += '">';
					if ((!startDate || tmp >= today) && (!endDate || tmp <= today2)) {
						html += '<a href="javascript://" onclick="Hi_Calendar.selectDate(\'' + ID + '\',' + tmp.getDate() + ',' + tmp.getMonth() + ',' + tmp.getFullYear() + ')">';
						html += tmp.getDate();
						html += '</a>';
					} else {
						html += '<a href="javascript://" style="cursor: default;">';
						html += tmp.getDate();
						html += '</a>';
					}
					
					tmp.setDate(tmp.getDate() + 1);
				}
				html += '</tr>';
			}
			var now = new Date();
			html += '</table>';
			if ((!endDate || today2 >= now) && (!startDate || today <= now)) {				
				html += '<div class="today"><a href="javascript://" onclick="Hi_Calendar.selectDate(\'' + ID + '\',' + now.getDate() + ',' + now.getMonth() + ',' + now.getFullYear() + ')">' + hi.i18n.getLocalization('calendar.today') + '</a></div>';
			}
			html += '</div></div>\
			    </div></td><td class="right"></td></tr></table>\
			    <div class="bottom"><div class="back"><!-- --></div><div class="right"></div></div>\
			</div></div>';
			cal.innerHTML = html;
		},
		changeYear: function(ID, n, startDate, endDate) {
			if (startDate == 'false' || !startDate) startDate = false; else startDate = new Date(startDate);
			if (endDate == 'false' || !endDate) endDate = false; else endDate = new Date(endDate);
			if (!Hi_Calendar.opened[ID]) return;
			var d = Hi_Calendar.opened[ID].date;
			var m = d.getMonth();
			d.setFullYear(d.getFullYear() + n);
			if (m != d.getMonth()) d.setDate(0);
			if (startDate && d < startDate) d = startDate;
			if (endDate && d > endDate) d = endDate;
			Hi_Calendar.opened[ID].date = d;
			Hi_Calendar.drawCalendar(ID, startDate, endDate);
		},
		changeMonth: function(ID, n, startDate, endDate) {
			if (startDate == 'false' || !startDate) startDate = false; else startDate = new Date(startDate);
			if (endDate == 'false' || !endDate) endDate = false; else endDate = new Date(endDate);
			
			if (!Hi_Calendar.opened[ID]) return;
			var d = Hi_Calendar.opened[ID].date;
			var m = d.getMonth();
			
			d.setMonth(d.getMonth() + n);
			if ((d.getMonth() - n - m) % 12 != 0) d.setDate(0);
			
			if (startDate && d < startDate) {
				d = startDate;
			}
			if (endDate && d > endDate) {
				d = endDate;
			}
			Hi_Calendar.opened[ID].date = d;
			Hi_Calendar.drawCalendar(ID, startDate, endDate);
		},
		selectDate: function(ID,d,m,y) {
			var sel_date = new Date(y,m,d);
			var el;
			if (sel_date.getDate() == d) {
				el = dojo.byId(ID);
				el.value = time2str(sel_date, Hi_Calendar.format);
				if (el.onchange) el.onchange();
				if (el.onblur) {
					el.onblur();
				}
				try {
					el.focus();
				} catch (err) {}
			}
			delete Hi_Calendar.opened[ID];
			if (el = dojo.byId("calendar" + ID)) removeNode(el, true);
			if (el = dojo.byId("iframe" + ID)) removeNode(el, true);
		},
		closeCalendars: function() {
			var i;
			for (i in Hi_Calendar.opened) {
				if (Hi_Calendar.noclose != i) {
					delete Hi_Calendar.opened[i];
					var z = dojo.byId("calendar" + i);
					var z2 = dojo.byId("iframe" + i);
					if (!z) return;
					removeNode(z, true);
					if (z2) removeNode(z2, true);
				}
			}
			Hi_Calendar.noclose = false;
		},
		startChangeYear: function(ID, n, startDate, endDate) {
			Hi_Calendar.changeYear(ID, n, startDate, endDate);
			Hi_Calendar.scrollCount = 0;
			dojo.global.clearInterval(Hi_Calendar.scroll);
			Hi_Calendar.scroll = dojo.global.setInterval("Hi_Calendar.changeYear('" + ID + "', " + n + ", '" + startDate + "', '" + endDate + "');", 200);
		},
		startChangeMonth: function(ID, n, startDate, endDate) {
			Hi_Calendar.changeMonth(ID, n, startDate, endDate);
			Hi_Calendar.scrollCount = 0;
			dojo.global.clearInterval(Hi_Calendar.scroll);
			Hi_Calendar.scroll = dojo.global.setInterval("Hi_Calendar.changeMonth('" + ID + "', " + n + ", '" + startDate + "', '" + endDate + "');", 200);
		},
		stopScroll: function() {
			if(Hi_Calendar.scroll){
				dojo.global.clearTimeout(Hi_Calendar.scroll);
				Hi_Calendar.scroll=false;
				Hi_Calendar.scrollCount = 0;
				return true;
			} else {
				return false;
			}
		},
		check: function(t) {
			var val1, val2;
			var s, sA = ['start_date', 'start_time', 'end_date', 'end_time'];
			var i, imax = sA.length;
			var d1, t1, d2, t2, rec, rem;
			t = dojo.byId(t);
			if (!t) return;
			
			var tid = t.id;
			
			for (i = 0; i < imax; i++) {
				if (tid.indexOf(sA[i]) !== -1) {
					s = sA[i];
					break;
				}
			}
			d1 = dojo.byId(tid.replace(s, 'start_date'));
			t1 = dojo.byId(tid.replace(s, 'start_time'));
			d2 = dojo.byId(tid.replace(s, 'end_date'));
			t2 = dojo.byId(tid.replace(s, 'end_time'));
			rec = dojo.byId(tid.replace(s, 'recurring_cont'));
			rem = dojo.byId(tid.replace(s, 'reminder_cont'));
			
			switch (t) {
			case d1:
				if (d1.value == '') {
					if (t1) t1.value = '';
					if (d2) d2.value = '';
					if (t2) t2.value = '';
				} else {
					if (d2 && d2.value != '') {
						val1 = str2time(d1.value, Hi_Calendar.format).valueOf();
						val2 = str2time(d2.value, Hi_Calendar.format).valueOf();
						if (val1 > val2) {
							d2.value = d1.value;
							val2 = val1;
						}
						if (val1 == val2 && t1 && t1.value != '') {
							if (t2 && t2.value == '') {
								t2.value = Hi_Calendar.gohour(t1.value, 1);
							} else {
								val1 = validateTime(t1.value, 1);
								val2 = validateTime(t2.value, 1);
								if (val1 > val2) {
									t2.value = Hi_Calendar.gohour(t1.value, 1);
								}
							}
						}
					}
				}
				break;
			case t1:
				if (t1.value == '') {
					if (t2) t2.value = '';
				} else {
					if (d1.value == '') {
						d1.value = time2str('now', Hi_Calendar.format);
					} else {
						if (d2 && d2.value != '') {
							val1 = str2time(d1.value, Hi_Calendar.format).valueOf();
							val2 = str2time(d2.value, Hi_Calendar.format).valueOf();
							if (val1 == val2) {
								if (t2) {
									if (t2.value != '') {
										val1 = validateTime(t1.value, 1);
										val2 = validateTime(t2.value, 1);
										if (val1 > val2) {
											t2.value = Hi_Calendar.gohour(t1.value, 1);
										}
									} else {
										t2.value = Hi_Calendar.gohour(t1.value, 1);
									}
								}
							}
						}
					}
				}
				break;
			case d2:
				if (d2.value == '') {
					if (t2) t2.value = '';
				} else {
					if (d1.value == '') {
						d1.value = d2.value;
					}
					val1 = str2time(d1.value, Hi_Calendar.format).valueOf();
					val2 = str2time(d2.value, Hi_Calendar.format).valueOf();
					if (val1 > val2) {
						d1.value = d2.value;
						val1 = val2;
					}
					if (val1 == val2) {
						if (t1.value != '' && t2.value == '') {
							t2.value = Hi_Calendar.gohour(t1.value, 1);
						} else if (t1.value == '' && t2.value != '') {
							t1.value = Hi_Calendar.gohour(t2.value, -1);
						} else if (t1.value != '' && t2.value != '') {
							val1 = validateTime(t1.value, 1);
							val2 = validateTime(t2.value, 1);
							if (val1 > val2) {
								t1.value = Hi_Calendar.gohour(t2.value, -1);
							}
						}
					}
				}
				break;
			case t2:
				if (t2.value == '') {
					val1 = str2time(d1.value, Hi_Calendar.format).valueOf();
					val2 = str2time(d2.value, Hi_Calendar.format).valueOf();
					if (val1 == val2) {
						t1.value = '';
					}
				} else {
					if (d1.value == '') {
						d1.value = time2str('now', Hi_Calendar.format);
					}
					if (d2.value == '') {
						d2.value = d1.value;
					}
					val1 = str2time(d1.value, Hi_Calendar.format).valueOf();
					val2 = str2time(d2.value, Hi_Calendar.format).valueOf();
					if (val1 == val2) {
						val1 = validateTime(t1.value, 1);
						val2 = validateTime(t2.value, 1);
						if (val1 === false || val1 > val2) {
							t1.value = Hi_Calendar.gohour(t2.value, -1);
						}
					} else {
						if (t1.value == '') {
							t1.value = t2.value;
						}
					}
				}
				break;
			}
			if (rec && d1) {
				if (d1.value == '') {
					disableElements(rec);
				} else {
					enableElements(rec);
				}
			}
			if (rem && d1 && t1) {
				if (d1.value == '' || t1.value == '') {
					//hideElement(rem);
					disableElements(rem);
				} else {
					//showElement(rem);
					enableElements(rem);
				}
			}
		},
		gohour: function(time, n) {
			time = HiTaskCalendar.standartTime(time);
			time = validateTime(time);
			if (!time) return;
			time[0] += n;
			if (time[0] < 0) {
				time[0] = 0;
				time[1] = 0;
			}
			if (time[0] > 23) {
				time[0] = 23;
				time[1] = 59;
			}
			return HiTaskCalendar.formatTime(strPadLeft(time[0], 2, '0') + ':' + strPadLeft(time[1], 2, '0'));
		},
		
		checkIsAllDay: function (id) {
			var el = dojo.byId('is_all_day_' + id);
			if (!el) return;
				
			var startTime = dijit.byId('start_time_' + id);
			var endTime = dijit.byId('end_time_' + id);				
			
			if (el.checked) {
				startTime.setValue(' ');
				endTime.setValue(' ');
				startTime.setAttribute('disabled',true); 
				endTime.setAttribute('disabled',true); 

			} else {
				//startTime.setValue('12:00');
				//endTime.setValue('12:00');
				startTime.setAttribute('disabled',false);
				endTime.setAttribute('disabled',false);
			}
			
		},
		
		checkTimeTracking: function (id) {
			var el = dojo.byId('time_tracking_cont_' + id);
			if (!el) return;
			
			var check = dojo.byId('time_track_' + id);
			var inp = dojo.byId('time_est_' + id);
			var label = dojo.query('span.label-after-semi', el)[0];	
			
			if (check.checked) {
				inp.disabled = false;
				dojo.removeClass(el, 'time-tracking-disabled');
			} else {
				inp.disabled = true;
				dojo.addClass(el, 'time-tracking-disabled');
			}
		},
		cal: function() {
			var data = {};
			data.ical_address = 'webcal://' + Project.appBaseHost + '/calendar/' + encodeURIComponent(Project.user_login) + '/';
			data.ical_title = data.ical_address;
			data.ical_link = '<a href="' + data.ical_address + '">' + data.ical_title + '</a>';
			fillTemplate('exportContentCal', data);
		},
		
		googlecal: function(data) {
			data.ical_address = Project.appBaseUrl + '/calendar/' + data.feed_id;
			data.ical_title = data.ical_address;
			data.ical_link = '<a href="' + data.ical_address + '">' + data.ical_title + '</a>';
			data.gcal_link = '<a href="http://www.google.com/calendar/render?cid=' + encodeURIComponent(data.ical_address) + '" target="_blank"><img id="google_cal_feed_img" src="/img/v5/google_cal_feed.gif"></a>';
			fillTemplate('exportContentGoogleCal', data);
		},
		googlecal_reset: function() {
			Hi_Calendar.googlecal_loading();
			Project.ajax('feedid_get', {}, Hi_Calendar.googlecal_get, Hi_Calendar.googlecal_get)
		},
		
		googlecal_loading: function() {
			fillTemplate('exportContentGoogleCal', {gcal_link: '<img id="google_cal_feed_img" src="/img/v5/google_cal_feed.gif">', ical_link: 'Loading ...'});
		},
		googlecal_get: function(data) {
			if (data === false || !data || !data.isStatusOk()) {
				alert('Server error! Please try again later.');
				return;
			}
			
			Hi_Calendar.googlecal(data.getData());
		},
		
		enableGoogleSyncSignal: null,
		googleCalendarSyncEnable: function(button, onSuccess, failureUrl, accountEmail) {
			var enableInnerButton = dijit.byId('enableGoogleSyncDijitButton');
			var selectGoogleSyncTargetsInitWrapper = dojo.byId('selectGoogleSyncTargetsInitWrapper');
			var dialog = dijit.byId('selectGoogleSyncTargetsInitDialog');
			var disableButton = function(disable) {
				if (button) {
					button.set('disabled', disable);
					enableInnerButton.set('disabled', disable);
				}
			};
			var errorHandler = function(apiResponse) {
				if (apiResponse.getStatus() == 52 && failureUrl) {
					var msg = 'You will be redirected to Google Authorization Server in order to grant access to HiTask application. After that please try to enable synchronization.';
					if (accountEmail) {
						msg += '\n\nIf you have multiple Google accounts you must choose exactly this account "' + accountEmail + '".';
					}
					
					alert(msg);
					window.location.href = failureUrl;
				} else {
					alert(apiResponse.getErrorMessage());
					disableButton(false);
				}
			};
			var thatCalendar = this;
			
			dojoQuery('*', selectGoogleSyncTargetsInitWrapper).forEach(domConstruct.destroy);
			domConstruct.place(domConstruct.toDom('<p>' + htmlspecialchars(hi.i18n.getLocalization('sync.init_targets_retrieving')) + '</p>'), selectGoogleSyncTargetsInitWrapper);
			
			disableButton(true);
			Tooltip.close('sync');
			
			if (this.enableGoogleSyncSignal) {
				this.enableGoogleSyncSignal.remove();
			}
			
			Project.ajax('syncListTargets', {type: 'GOOGLE'}, function(apiResponse) {
				disableButton(false);
				var targets = apiResponse.getData();
				var html = '';
				for (var k in targets) {
					html += '<div class="initSyncTarget"><label><input type="checkbox" value="' + htmlspecialchars(k) + '" /> ' + htmlspecialchars(targets[k]) + '</label></div>';
				}
				
				dojoQuery('*', selectGoogleSyncTargetsInitWrapper).forEach(domConstruct.destroy);
				domConstruct.place(domConstruct.toDom(html), selectGoogleSyncTargetsInitWrapper);
				
				var selectedTargets = {};
				dojoQuery('.initSyncTarget input').on('click', function() {
					selectedTargets = {};
					var targetInputs = dojoQuery('..initSyncTarget input');
					
					targetInputs.forEach(function(x) {
						if (x.checked) {
							selectedTargets[x.value] = targets[x.value];
						}
					});
				});
				
				dialog.show();
				
				thatCalendar.enableGoogleSyncSignal = dojoOn(enableInnerButton, 'click', function() {
					disableButton(true);
					
					Project.ajax('syncEnable', {type: 'GOOGLE', targets: dojoJson.stringify(selectedTargets)}, function(apiResponse) {
						dialog.hide();
						if (typeof(onSuccess) == 'function') {
							onSuccess();
						}
					}, function(a, b, apiResponse) {
						errorHandler(apiResponse);
					});
				});
			}, function(a, b, apiResponse) {
				errorHandler(apiResponse);
			});
		},
		googleCalendarSyncClearError: function(onSuccess) {
			Project.ajax('syncClearError', {type: 'GOOGLE'}, function(apiResponse) {
				if (typeof(onSuccess) == 'function') {
					onSuccess();
				}
			});
		}
	};
	
});
