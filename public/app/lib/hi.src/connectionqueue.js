define(function () {
	
	dojo.global.ConnectionQueue = {
		
		/**
		 * Request timeout time
		 * @type {Number}
		 */
		REQUEST_TIMEOUT: 10,
		
		/**
		 * Request running state. Request from
		 * queue is currently running
		 */
		running: false,
		
		/**
		 * Request retry timer
		 * @type {Number}
		 */
		timer: null,
		
		/**
		 * After timeout queue requests to
		 * maintain correct execution order
		 * @type {Array}
		 */
		list: [],
		
		/**
		 * List of requests which should be queued (add, save, delete, etc.)
		 * @type {Object}
		 */
		requests: {
			'itemnew': true, 'itemsave': true, 'itemnewmulti': true, 
			'itemcomplete': true, 'itemdelete': true, 
			'timelog': true, 'savepriority': true, 'itemcomment': true, 'itemcommentdelete': true,
			'pshare': true, 'ptitle': true, 'projectnew': true, 'projectdelete': true, 'pdelete': true, 'ptdelete': true, 'tpurge': true, 'tarchivepurge': true
		},
		
		/**
		 * Update view
		 */
		update: function () {
			this.view.show();
		},
		
		/**
		 * Returns true if there are queued requests, otherwise false
		 */
		isActive: function () {
			return !!this.list.length;
		},
		
		/**
		 * Popup
		 */
		view: {
			/**
			 * Show button and popup
			 */
			show: function () {
				this.updateButton();
				this.updateView();
			},
			
			/**
			 * Hide button and popup
			 */
			hide: function () {
				//Fade out and drop down
				Tooltip.close('queue');
				dojo.anim(dojo.byId('tooltip_queue'), {top: 350}, 500);
				
				this.updateButton();
			},
			
			/**
			 * Update operation count in popup
			 */
			updateView: function () {
				var node = dojo.byId('queue_tooltip_operations'),
					classname = '';
				
				if (node) {
					node.innerHTML = ConnectionQueue.list.length;
					
					if (ConnectionQueue.list.length > 1) classname = 'multiple';
					node.parentNode.className = classname;
				}
			},
			
			/**
			 * Show/hide button and set classes
			 */
			updateButton: function () {
				var active = ConnectionQueue.isActive(),
					btn = this.getButton();
				
				if (active) {
					dojo.removeClass(btn, 'hidden');
				} else {
					dojo.addClass(btn, 'hidden');
				}
			},
			
			/**
			 * Returns header button element
			 * 
			 * @return Button
			 * @type {HTMLElement}
			 */
			getButton: function () {
				return dojo.byId('queueLink');
			}
		}
	};
	
	return dojo.global.ConnectionQueue;

});