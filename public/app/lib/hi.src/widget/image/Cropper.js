define([
	"dojo/_base/declare",
	'dijit/_Widget',
	'dojo/Evented',
	'dojo/sniff'
], function(declare, WidgetBase, Evented, sniff) {
	return declare("hi.widget.Cropper", [WidgetBase, Evented], {
		minWidth: 64,
        minHeight: 64,
        //gap between crop region border and container border
        gap: 50,
        //the initial crop region size
        cropSize: 0,
        //whether to keep crop region as a square
        keepSquare: false,
        workSize: null,
        debug: false,
        maximizeCropArea: false,
        postCreate: function() {
            this.inherited(arguments);
            this.updateUI();
            this.connect(this.cropNode, 'onmousedown', '_onMouseDown');
            this.connect(document, 'onmouseup', '_onMouseUp');
            this.connect(document, 'onmousemove', '_onMouseMove');
            document.ondragstart = function() {
                return false;
            }
            dojo.setSelectable(this.domNode, false);
            dojo.addClass(this.domNode, 'image-cropper');
            if (this.image) this.imageNode = dojo.create('img', {
                src: this.image
            }, this.domNode, 'first');
        },
        calcK: function(width) {
        	var min = this.cropSize * 2;
        	if (width > min) {
        		return this.workSize && this.workSize < width ? this.workSize / width : 1;
        	} else {
        		return min / width;
        	}
        },
        buildRendering: function() {
            this.inherited(arguments);
            this._archors = {};
            this._blockNodes = {};

            this.cropNode = dojo.create('div', {
                className: 'crop-node'
            }, this.domNode, 'last');
            dojo.addClass(this.cropNode, 'hidden');
            if (sniff('ie') || sniff('trident')) {
            	this.imgIeFix = dojo.create('img', {
                    className: 'iefix'
                });
            	this.imgIeFix.src = this._blankGif;
                this.cropNode.appendChild(this.imgIeFix);
                this.connect(this.imgIeFix, 'onmousedown', '_onMouseDown');
            }

            var arr = ['lt', 't', 'rt', 'r', 'rb', 'b', 'lb', 'l'];
            for (var i = 0; i < 8; i++) {
                var img = dojo.create('img', {
                    className: arr[i]
                });
                img.src = this._blankGif;
                this.cropNode.appendChild(img);
                this._archors[arr[i]] = img;
            }

            arr = ['l', 't', 'r', 'b'];
            for (var i = 0; i < 4; i++) {
                this._blockNodes[arr[i]] = dojo.create('div', {
                    className: 'block block-' + arr[i]
                }, this.domNode, 'last');
            }
        },
        setImage: function(url) {
        	this.emit('beforeImageLoad');
        	var self = this;
        	this.image = url;
            var img = new Image();
            img.src = url;
            
            if (!this.imageNode) {
                this.imageNode = dojo.create('img', {
                    src: this.image
                }, this.domNode, 'first');
            }
            this.imageNode.src = url;
            dojo.addClass(this.imageNode, 'hidden');
            dojo.addClass(this.cropNode, 'hidden');
            
            this.imageNode.onload = function() {
            	if (self.debug) {
            		console.debug('img onload');
            	}
            	dojo.removeClass(self.imageNode, 'hidden');
            	dojo.removeClass(self.cropNode, 'hidden');
                var k = self.calcK(self.imageNode.offsetWidth);
                var newW = self.roundToEven(k * self.imageNode.offsetWidth);
                var newH = self.roundToEven(k * self.imageNode.offsetHeight);
                self.setSize(newW, newH);
                dojo.style(self.imageNode, {width: newW + 'px', height: newH + 'px'});
                self.emit('afterImageLoad');
            }
        },
        roundToEven: function(n) {
        	n = parseInt(n);
        	if (n % 2 != 0) {
        		n += 1;
        	}
        	return n;
        },
        setSize: function(w, h) {
        	if (this.debug) {
        		console.debug('setting image cropper size: ', w, h);
        	}
            this.domNode.style.width = w + 'px';
            this.domNode.style.height = h + 'px';

            if (this.cropSize) {
                var m = Math.min(w, h, this.cropSize);
                if (this.maximizeCropArea) {
                	m = Math.min(w, h);
                }

                dojo.style(this.cropNode, {
                    width: m - 2 + 'px',
                    height: m - 2 + 'px'
                });
            } else {
                dojo.style(this.cropNode, {
                    width: w - this.gap * 2 - 2 + 'px',
                    height: h - this.gap * 2 - 2 + 'px'
                });
            }
            var l = (w - this.cropNode.offsetWidth) / 2,
                t = (h - this.cropNode.offsetHeight) / 2;
            if (l < 0) l = 0;
            if (t < 0) t = 0;
            dojo.style(this.cropNode, {
                left: l + 'px',
                top: t + 'px'
            });
            this.posArchors();
            this.posBlocks();
            this.onChange(this.getInfo());
        },
        updateUI: function() {
            this.posArchors();
            this.posBlocks();

        },
        posArchors: function() {
            var a = this._archors,
                w = this.cropNode.offsetWidth,
                h = this.cropNode.offsetHeight;

            a.t.style.left = a.b.style.left = w / 2 - 4 + 'px';
            a.l.style.top = a.r.style.top = h / 2 - 4 + 'px';
        },
        posBlocks: function() {
            var p = this.startedPos,
                b = this._blockNodes;
            var l = parseInt(this.cropNode.style.left || dojo.style(this.cropNode, 'left'));
            var t = parseInt(this.cropNode.style.top || dojo.style(this.cropNode, 'top'));
            var w = this.cropNode.offsetWidth;
            var ww = this.domNode.offsetWidth;
            var h = this.cropNode.offsetHeight;
            var hh = this.domNode.offsetHeight;

            b = this._blockNodes;
            b.t.style.height = b.l.style.top = b.r.style.top = t + 'px';

            b.l.style.height = b.r.style.height = h + 'px';
            b.l.style.width = l + 'px';

            b.r.style.width = ww - w - l + 'px';
            b.b.style.height = hh - h - t + 'px';
        },
        _onMouseDown: function(e) {
            this.dragging = e.target == this.cropNode ? 'move' : e.target.className;
            if (sniff('ie') || sniff('trident')) {
            	this.dragging = e.target == this.imgIeFix ? 'move' : e.target.className;
            }
            var pos = dojo.position(this.cropNode);
            var pos2 = dojo.position(this.domNode);
            this.startedPos = {
                x: e.pageX,
                y: e.pageY,
                h: pos.h - 2 //2 is border width
                ,
                w: pos.w - 2,
                l: pos.x - pos2.x,
                t: pos.y - pos2.y
            }
            var c = dojo.style(e.target, 'cursor');
            dojo.style(document.body, {
                cursor: c
            });
            dojo.style(this.cropNode, {
                cursor: c
            });

        },
        _onMouseUp: function(e) {
            this.dragging = false;
            dojo.style(document.body, {
                cursor: 'default'
            });
            dojo.style(this.cropNode, {
                cursor: 'move'
            });
            this.onDone(this.getInfo());
        },
        getInfo: function() {
            return {
                w: this.cropNode.offsetWidth - 2,
                h: this.cropNode.offsetHeight - 2,
                l: parseInt(this.cropNode.style.left || dojo.style(this.cropNode, 'left')),
                t: parseInt(this.cropNode.style.top || dojo.style(this.cropNode, 'top')),
                cw: this.domNode.offsetWidth, //container width,
                ch: this.domNode.offsetHeight, //container height,
                k: this.calcK(this.imageNode.naturalWidth)
            };
        },
        _onMouseMove: function(e) {
            if (!this.dragging) return;

            if (this.dragging == 'move') this.doMove(e);
            else this.doResize(e);
            this.updateUI();

            this.onChange(this.getInfo());
        },
        doMove: function(e) {
            var s = this.cropNode.style,
                p0 = this.startedPos;
            var l = p0.l + e.pageX - p0.x;
            var t = p0.t + e.pageY - p0.y;
            if (l < 0) l = 0;
            if (t < 0) t = 0;
            var maxL = this.domNode.offsetWidth - this.cropNode.offsetWidth;
            var maxT = this.domNode.offsetHeight - this.cropNode.offsetHeight;
            if (l > maxL) l = maxL;
            if (t > maxT) t = maxT;
            s.left = l + 'px';
            s.top = t + 'px'

        },
        onChange: function() {
            //Event:
            //    When the cropping size is changed.
        },
        onDone: function() {
            //Event:
            //    When mouseup.
        },
        doResize: function(e) {
            var m = this.dragging,
                s = this.cropNode.style,
                p0 = this.startedPos;
            //delta x and delta y
            var dx = e.pageX - p0.x,
                dy = e.pageY - p0.y;

            if (this.keepSquare) {
                if (m == 'l') {
                    dy = dx;
                    if (p0.l + dx < 0) dx = dy = -p0.l;
                    if (p0.t + dy < 0) dx = dy = -p0.t;
                    m = 'lt';
                } else if (m == 'r') {
                    dy = dx;
                    m = 'rb';
                } else if (m == 'b') {
                    dx = dy;
                    m = 'rb';
                } else if (m == 'lt') {
                    dx = dy = Math.abs(dx) > Math.abs(dy) ? dx : dy;
                    if (p0.l + dx < 0) dx = dy = -p0.l;
                    if (p0.t + dy < 0) dx = dy = -p0.t;
                } else if (m == 'lb') {
                    dy = -dx;
                    if (p0.l + dx < 0) {
                        dx = -p0.l;
                        dy = p0.l;
                    }
                } else if (m == 'rt' || m == 't') {
                    dx = -dy;
                    m = 'rt';
                    if (p0.t + dy < 0) {
                        dy = -p0.t;
                        dx = -dy;
                    }
                }
            }
            if (/l/.test(m)) {
                dx = Math.min(dx, p0.w - this.minWidth);
                if (p0.l + dx >= 0) {
                    s.left = p0.l + dx + 'px';
                    s.width = p0.w - dx + 'px';
                } else {
                    s.left = 0;
                    s.width = p0.l + p0.w + 'px';
                }
            }
            if (/t/.test(m)) {
                dy = Math.min(dy, p0.h - this.minHeight);
                if (p0.t + dy >= 0) {
                    s.top = p0.t + dy + 'px';
                    s.height = p0.h - dy + 'px';
                } else {
                    s.top = 0;
                    s.height = p0.t + p0.h + 'px';
                }
            }
            if (/r/.test(m)) {
                dx = Math.max(dx, this.minWidth - p0.w);
                if (p0.l + p0.w + dx <= this.domNode.offsetWidth) {
                    s.width = p0.w + dx + 'px';
                } else {
                    s.width = this.domNode.offsetWidth - p0.l - 2 + 'px';
                }
            }
            if (/b/.test(m)) {
                dy = Math.max(dy, this.minHeight - p0.h);
                if (p0.t + p0.h + dy <= this.domNode.offsetHeight) {
                    s.height = p0.h + dy + 'px';
                } else {
                    s.height = this.domNode.offsetHeight - p0.t - 2 + 'px';
                }
            }

            if (this.keepSquare) {
                var min = Math.min(parseInt(s.width), parseInt(s.height));
                s.height = s.width = min + 'px';
            }
        }
	});
});