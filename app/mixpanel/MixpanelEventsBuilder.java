package mixpanel;

import java.util.LinkedList;
import java.util.List;

import nl.bitwalker.useragentutils.UserAgent;

import com.google.gson.Gson;

/**
 * Use this class for fast and easy Mixpanel events constructing.
 * @author Vadim Manikhin
 */
public class MixpanelEventsBuilder {
	private List<MixpanelEvent> events = new LinkedList<MixpanelEvent>();
	private UserAgent userAgent;
	
	public MixpanelEventsBuilder() {
		
	}
	
	public MixpanelEventsBuilder(UserAgent userAgent) {
		this.userAgent = userAgent;
	}
	
	public MixpanelEventsBuilder add(MixpanelEvent event) {
		events.add(event);
		return this;
	}
	
	public MixpanelEventsBuilder add(String name) {
		events.add(new MixpanelEvent(name));
		return this;
	}
	
	/**
	 * Appends parameters to the last inserted event.
	 * @param name
	 * @param value
	 * @return
	 */
	public MixpanelEventsBuilder appendParam(String name, Object value) {
		if (!events.isEmpty()) {
			events.get(events.size() - 1).addParam(name, value);
		} else {
			throw new IllegalStateException("events list is empty");
		}
		return this;
	}
	
	public List<MixpanelEvent> getEvents() {
		preprocessWithUserAgent();
		return events;
	}
	
	public String toJsonString() {
		return new Gson().toJson(events);
	}
	
	public boolean isEmpty() {
		return events.isEmpty();
	}
	
	public boolean isNotEmpty() {
		return !isEmpty();
	}
	
	private void preprocessWithUserAgent() {
		if (userAgent != null) {
			for (MixpanelEvent event : events) {
				event.addBrowserParam(userAgent.getBrowser().getName());
				event.addOsParam(userAgent.getOperatingSystem().getName());
			}
		}
	}
}
