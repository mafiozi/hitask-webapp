package beans;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import play.Logger;
import utils.Constants;

import com.google.gson.annotations.SerializedName;

public class UserBusiness extends BaseBean {
	private boolean canInvite;
	private Integer domainId;
	private String domainName;
	private Integer level;
	private String title;
	private String accountCreateTime;
	private Date accountCreateTimeDate;
	private String pictureHash;
	private String invitationLink;
	private String billingAddress;
	private String emailInbox;
	
	private long numberOfProjects = 0;
	private long numberOfItems = 0;
	private long numberOfArchived = 0;
	
	@SerializedName("license_count")
	private int licenseCount = 0;
	@SerializedName("license_usage")
	private int licenseUsage = 0;
	@SerializedName("licenses_remaining")
	private int licensesRemaining;
	@SerializedName("expiration_date")
	private String expirationDate;
	private Date expirationDateDate;
	private double paymentAmountLast = 0;
	private double paymentAmountLifetime = 0;
	
	public void postInit() {
		licensesRemaining = licenseCount - licenseUsage;
		accountCreateTimeDate = parseApiDate(accountCreateTime);
		expirationDateDate = parseApiDate(expirationDate);
	}
	
	public int getId() {
		return domainId;
	}

	public void setId(int id) {
		this.domainId = id;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getLicenseCount() {
		return licenseCount;
	}
	
	public void setLicenseCount(int licenseCount) {
		this.licenseCount = licenseCount;
	}
	
	public int getLicenseUsage() {
		return licenseUsage;
	}
	
	public void setLicenseUsage(int licenseUsage) {
		this.licenseUsage = licenseUsage;
	}
	
	public Integer getLevel() {
		return level;
	}
	
	public void setLevel(Integer level) {
		this.level = level;
	}

	public int getLicensesRemaining() {
		return licensesRemaining;
	}

	public void setLicensesRemaining(int licensesRemaining) {
		this.licensesRemaining = licensesRemaining;
	}

	public boolean isCanInvite() {
		return canInvite;
	}

	public void setCanInvite(boolean canInvite) {
		this.canInvite = canInvite;
	}

	public Integer getDomainId() {
		return domainId;
	}

	public void setDomainId(Integer domainId) {
		this.domainId = domainId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	public String getHomepageAddress() {
		try {
			URI uri = new URI(Constants.APP_BASE_URL_SECURE);
			return String.format("%s://%s.%s", uri.getScheme(), domainName, uri.getHost());
		} catch (URISyntaxException e) {
			Logger.error(e, "Could not parse app base url");
		}
		
		return "";
	}

	public String getAccountCreateTime() {
		return accountCreateTime;
	}

	public void setAccountCreateTime(String accountCreateTime) {
		this.accountCreateTime = accountCreateTime;
	}

	public String getPictureHash() {
		return pictureHash;
	}

	public void setPictureHash(String pictureHash) {
		this.pictureHash = pictureHash;
	}

	public Date getAccountCreateTimeDate() {
		return accountCreateTimeDate;
	}

	public long getNumberOfProjects() {
		return numberOfProjects;
	}

	public void setNumberOfProjects(long numberOfProjects) {
		this.numberOfProjects = numberOfProjects;
	}

	public long getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(long numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public long getNumberOfArchived() {
		return numberOfArchived;
	}

	public void setNumberOfArchived(long numberOfArchived) {
		this.numberOfArchived = numberOfArchived;
	}

	public double getPaymentAmountLast() {
		return paymentAmountLast;
	}

	public void setPaymentAmountLast(double paymentAmountLast) {
		this.paymentAmountLast = paymentAmountLast;
	}

	public double getPaymentAmountLifetime() {
		return paymentAmountLifetime;
	}

	public void setPaymentAmountLifetime(double paymentAmountLifetime) {
		this.paymentAmountLifetime = paymentAmountLifetime;
	}

	public Date getExpirationDateDate() {
		return expirationDateDate;
	}
	
	public boolean isUserOwner() {
		return level == UserSession.BUSINESS_LEVEL_OWNER;
	}
	
	public boolean isUserAdmin() {
		return level == UserSession.BUSINESS_LEVEL_ADMIN;
	}
	
	public boolean isUserManager() {
		return level == UserSession.BUSINESS_LEVEL_MANAGER;
	}
	
	public boolean hasOwnerPermissions() {
		return level >= UserSession.BUSINESS_LEVEL_OWNER;
	}
	
	public boolean hasAdminPermissions() {
		return level >= UserSession.BUSINESS_LEVEL_ADMIN;
	}
	
	public boolean hasManagerPermissions() {
		return level >= UserSession.BUSINESS_LEVEL_MANAGER;
	}
	
	public boolean isUserMember() {
		return level == null || level == UserSession.BUSINESS_LEVEL_USER;
	}
	
	public String getPictureUrl(int size) {
		if (StringUtils.isNotBlank(pictureHash)) {
			return new StringBuilder().append("/avatar/business/").append(pictureHash).append(".").append(size).append(".gif").toString();
		}
		
		return null;
	}

	public String getInvitationLink() {
		return invitationLink;
	}

	public void setInvitationLink(String invitationLink) {
		this.invitationLink = invitationLink;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	
	public boolean canInviteTeamMembers() {
		return level != null && level >= UserSession.BUSINESS_LEVEL_MANAGER;
	}

	public String getEmailInbox() {
		return emailInbox;
	}

	public void setEmailInbox(String emailInbox) {
		this.emailInbox = emailInbox;
	}
}