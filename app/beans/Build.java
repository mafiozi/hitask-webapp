package beans;

import play.Play;

public class Build {
	private String version;
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	private String number;
	private boolean doNotSendWebappVersionHeader = false;
	
	public Build(String version, String number) {
		this.version = version;
		this.number = number;
	}
	public Build() {
		this.version = Play.configuration.getProperty("build.version", "?");
		this.number =  Play.configuration.getProperty("build.number", "?");
		this.doNotSendWebappVersionHeader = Boolean.parseBoolean(Play.configuration.getProperty("build.doNotSendWebappVersionHeader", "false"));
	}

	
	@Override
	public String toString() {
		return String.format("%s.%s", version, number);
	}
	
	public boolean isDoNotSendWebappVersionHeader() {
		return doNotSendWebappVersionHeader;
	}
}