package tags;

import groovy.lang.Closure;

import java.io.PrintWriter;
import java.util.Map;

import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;
import play.templates.JavaExtensions;

@FastTags.Namespace("app.hi")
public class AjaxSubmit extends BaseTag {
	public static void _ajaxSubmit(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		StringBuilder sb = new StringBuilder();
		
		String validate = getArgAsString("validate", args);
		Boolean disable = getArgAsBoolean("disable", args);
		String function = getArgAsString("function", args);
		String action = getArgAsString("action", args);
		String success = getArgAsString("success", args);
		String error = getArgAsString("error", args);
		
		sb.append("tpl_function_ajax_submit(this, ")
			.append(validate != null ? validate : "false").append(", ")
			.append(disable != null && disable ? "true" : "false").append(", ")
			.append(function).append(", ")
			.append(action != null ? "'" : "").append(action).append(action != null ? "'" : "").append(", ")
			.append(success != null ? success : "false").append(", ")
			.append(error != null ? error : "false")
			.append("); return false;");
		
		out.print(JavaExtensions.escapeHtml(sb.toString()));
	}
}