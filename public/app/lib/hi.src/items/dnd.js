/**
 * Functions for handling the various types of DnD item drop events:
 * 
 * - dropping on calendar date
 * - dropping on all day portion of time table
 * - dropping on time portion of time table
 * - dropping on project
 * - dropping on a team member in My Teams
 */
define([
	'hi/store',
	'hi/items/base',
	'hi/widget/task/item_DOM',
	'hi/items/date'
], function (hi_store, hi_items,item_DOM) {
	
	dojo.global.dragSources = {};
	dojo.global.dragSortLists = [];
	
	/*
	 * Shift start and end dates specified in saveData for the specified item.  This is necessary when,
	 * for example, a recurring, multi-day item's start date is changed
	 */
	dojo.global.shiftRecurrenceFeatures = function(item, saveData) {
		var current_recurring_end_date = str2timeAPI(item.recurring_end_date),
		new_start_date = str2timeAPI(saveData.start_date),
		new_end_date,
		oldDuration,
		new_recurring_end_date,
		recalculate_recurring_end_date = false;
	
		if (new_start_date >= current_recurring_end_date) recalculate_recurring_end_date = true;
		else if (saveData.end_date)
		{
			new_end_date = str2timeAPI(saveData.end_date);
			if (new_end_date >= current_recurring_end_date) recalculate_recurring_end_date = true;
		}
		
		if (recalculate_recurring_end_date)
		{
			oldDuration = ( current_recurring_end_date - str2timeAPI(item.start_date) );
			new_recurring_end_date = new_start_date;
			new_recurring_end_date.setTime(new_recurring_end_date.getTime() + oldDuration);
			saveData.recurring_end_date = time2strAPI(new_recurring_end_date);
		}
	};

	dojo.global.isDndValid = function (action) {
		var m = dojo.dnd.Manager.manager(),
			taskId = m.source.dragNode.taskId,
			task = taskId && hi.items.manager.get(taskId), valid;
			parent = task && task.parent && hi.items.manager.get(task.parent),
			action = action ? action : this.action;

		// valid = task && this.permissionsAllowed.indexOf(task.permission) >= 0;
		valid = task && Hi_Actions.validateAction(action, task);
		if (!valid) return false;

		var targetId = null;
		var target = null;
		
		// #5015
		var targetEl = dojo.dnd.HiSortDragExtra.drop_target || dojo.dnd.HiSortDragExtra.pEl;
		if (targetEl &&
			(targetId = dojo.dom.getTaskId(targetEl)) &&
			(target = hi.items.manager.get(targetId)) &&
			(dojo.dnd.HiSortDragExtra.drop_type !== 'line')		//dnd drop type should not be changing priority
		) {
			valid = Hi_Actions.validateAction('addSub', target);		//move into target item
			if (!valid) return false;

			if(parent && parent.category === hi.widget.TaskItem.CATEGORY_PROJECT){
				valid = Hi_Actions.validateAction('removeChild', target);		//move from parent item
			}
		}
		
		return valid;
	};
	
	dojo.global.dndHandleTeamDrop = function () {
		// summary:
		//		Handle drop on team
		//		Assign task to user
		
		//JDT var m = dojo.dnd.Manager();
		var m = dojo.dnd.Manager.manager(), valid;
		
		if (this.dropValue && m.source instanceof dojo.dnd.HiDragSource) {
			valid = dojo.global.isDndValid.apply(this, []);

			if (!valid) {
				this.dropValue = false;
				return false;
			}

			var dragId = m.source.dragNode.dragId;
			Project.assign(dragId, this.dropValue);
			trackEvent(trackEventNames.ITEM_MODIFY);
			this.dropValue = null;
		}
		
		Project.dragging = false;
	};
	
	dojo.global.dndHandleCalendarDrop = function () {
		// summary:
		//		Handle drop on calendar
		//		Update task start date, end date
		
		//JDT var m = dojo.dnd.Manager();
		var m = dojo.dnd.Manager.manager(), valid;

		if (this.dropTarget && m.source instanceof dojo.dnd.HiDragSource) {
			var dropValue = this.dropTarget.getElementsByTagName('INPUT');
			this.dropTarget = null;
			if (!dropValue.length || !dropValue[0] || !isDndValid.apply(this, [])) return (Project.dragging = false);
			
			var dragId = m.source.dragNode.taskId,
				curDate = dropValue[0].value,
				curTime = str2time(curDate, Hi_Calendar.format),
				item = hi.items.manager.get(dragId),
				start_date = item.start_date,
				end_date = item.end_date,
				due_date = item.due_date,
				category = item.category,
				
				saveData   = {
					"id": dragId,
					"category": category,
					"start_date": hi.items.date.dateTimeSetDate(start_date, curTime),
					'is_all_day': item.is_all_day || !item.start_date
				};
			
			if (category == 4) {
				/* #3059 Note can be d&d to the calendar and set the time */
				return;
			}

			var diff = 0;
			if (category === hi.widget.TaskItem.CATEGORY_TASK) {
				saveData = {
					"id": dragId,
					"category": category,
					"is_all_day": item.is_all_day || !item.start_date
				};
				if (due_date) {
					// saveData.due_date = hi.items.date.dateTimeSetDate(due_date, curTime);
					console.log('curTime is', curTime);
					if (start_date) {
						var old_start_date = str2time(start_date, 'y-m-d');
						var new_start_date = curTime;
						var due_date_time = str2time(due_date);

						diff = old_start_date.getTime() - new_start_date.getTime();
						saveData.due_date = time2strAPI(new Date(due_date_time.getTime() - diff));
						saveData.start_date = hi.items.date.dateTimeSetDate(start_date, curTime);
					} else {	// set due date when only has due date
						saveData.due_date = hi.items.date.dateTimeSetDate(due_date, curTime);
					}
				} else {
					if (start_date) {
						if (curTime.getTime() < str2time(start_date).getTime()) {
							saveData.start_date = time2strAPI(curTime);
						}
					} else {
						saveData.due_date = time2strAPI(new Date(curTime.getTime() + 86400000 - 1));
					}
				}
				saveData.end_date = saveData.due_date;
			} else {
				if (start_date && start_date != '' && end_date && end_date != '') {
					/* #2906 var delta = curTime - str2timeAPI(start_date);
					end_date = time2strAPI(new Date(str2timeAPI(end_date).valueOf() + delta));
					saveData.end_date = end_date;
					*/
					var duration = new Date(item.end_date) - new Date(item.start_date);
					var myD = new Date(saveData.start_date);
					myD.setTime(myD.getTime() + duration);
					saveData.end_date = time2strAPI(myD);
				}
			}
			

			/* 
			 * #3255 - if item is recurring and has a recurring end date set, we must reset the recurring end date
			 */
			if (item.recurring && item.recurring_end_date) {
				shiftRecurrenceFeatures(item,saveData);
			}
			//var taskProperties = hi.items.manager._items[dragId].list.propertiesView?  hi.items.manager._items[dragId].list.propertiesView.task:null;
			//if (taskProperties) taskProperties.set('start_date',saveData.start_date);
			//if (taskProperties && saveData.end_date) taskProperties.set('end_date',saveData.end_date);
			
			/*
			 * #2578
			 * 
			 */
			trackEvent(trackEventNames.ITEM_MODIFY);
			//if (taskProperties) item_DOM._save(taskProperties,saveData);
			item_DOM._save(dragId,saveData);
			hi.items.manager.setSave(saveData);
			HiTaskCalendar.update(); // update UI
			
		}
		
		Project.dragging = false;
	};
	
	dojo.global.dndHandleAllDayEventDrop = function () {
		// summary:
		//		Handle drop on all day event list
		//		Update task start date, end date
		
		//JDT var m = dojo.dnd.Manager();
		var m = dojo.dnd.Manager.manager(), valid;

		if (this.inDropArea && m.source instanceof dojo.dnd.HiDragSource) {
			this.inDropArea = false;
			valid = isDndValid.apply(this, []);

			if(!valid) return;

			var dragId  = m.source.dragNode.dragId,
				item = hi.items.manager.get(dragId),
				curTime = HiTaskCalendar.currentDate.getTime(),
				curDate = HiTaskCalendar.currentDate,
				curStr  = time2str(curDate, Hi_Calendar.format);

			if (!item) return;

			var start_date = item.start_date,
				end_date   = item.end_date,
				due_date = item.due_date,
				category   = item.category;
				
				saveData   = {
					"id": dragId,
					"category": category,
					"start_date": hi.items.date.dateTimeSetDate(start_date, curDate)
				};
			
			if (category == 4) {
				/* #3059 Note can be d&d to the calendar and set the time */
				return;
			}

			if (category === hi.widget.TaskItem.CATEGORY_TASK) {
				saveData = {
					"id": dragId,
					"category": category,
				};
				if (due_date) {
					if (start_date) {
						var old_start_date = str2time(start_date, 'y-m-d');
						var new_start_date = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate());
						var due_date_time = str2time(due_date);

						diff = old_start_date.getTime() - new_start_date.getTime();
						saveData.due_date = time2strAPI(new Date(due_date_time.getTime() - diff));
						saveData.start_date = hi.items.date.dateTimeSetDate(start_date, curDate);
					} else {
						saveData.due_date = hi.items.date.dateTimeSetDate(due_date, curDate);
					}
				} else {
					var d = hi.items.date.truncateTime(curDate);
					if (!d) return;

					if (start_date) {
						if (d.getTime() < str2time(start_date).getTime()) {
							saveData.start_date = time2strAPI(d);
						}
					} else {
						saveData.due_date = time2strAPI(new Date(d.getTime() + 86400000 - 1));
					}
					// if (start_date) {
					// 	saveData.start_date = time2strAPI(d);
					// } else {
					// 	saveData.due_date = time2strAPI(new Date(d).getTime() + 86400000 - 1);	
					// }
				}
				saveData.end_date = saveData.due_date;
			} else {
				if (start_date && start_date != '' && end_date && end_date != '') {
					/* #2928
					var delta = curTime - str2timeAPI(start_date);
					end_date = time2strAPI(new Date(str2timeAPI(end_date).valueOf() + delta));
					saveData.end_date = end_date;
					*/
					var duration = new Date(item.end_date) - new Date(item.start_date);
					var myD = new Date(saveData.start_date);
					myD.setTime(myD.getTime() + duration);
					saveData.end_date = time2strAPI(myD);
				}
			}
			
			/* #2992 */
			if (!start_date || start_date == '') {saveData.is_all_day = true;}
			/*
			 * #3255,#3355
			 */
			if (item.recurring && item.recurring_end_date) shiftRecurrenceFeatures(item,saveData);
			trackEvent(trackEventNames.ITEM_MODIFY);
			item_DOM._save(dragId,saveData);
			hi.items.manager.setSave(saveData);
			HiTaskCalendar.update(); // update UI
			
		}
		
		Project.dragging = false;
	};
	
	dojo.global.dndHandleTimeTableDrop = function () {
		// summary:
		// 		Handle drop on time table
		//		Update task start date, end date, start time, end time
		
		//JDT var m = dojo.dnd.Manager();
		var m = dojo.dnd.Manager.manager();

		if (this.dropValue && m.source instanceof dojo.dnd.HiDragSource) {
			var dragId = m.source.dragNode.dragId,
				obj = {"id": dragId},
				data = hi.items.manager.get(dragId),
				is_all_day = data.is_all_day,
				newStartDate = HiTaskCalendar.getDateTimeFromStr(HiTaskCalendar.getCurrentDateStr(), this.dropValue),
				newEndDate = null,
				dateOffset = null,
				oldStartDate = null,
				oldEndDate = null,
				valid = isDndValid.apply(this, []), taskId;

			this.dropValue = null;

			if(!valid || !data) return;

			if (data.category === hi.widget.TaskItem.CATEGORY_TASK) {
				var due_date = data.due_date;
				var curDate = newStartDate;
				var start_date = data.start_date;

				if (is_all_day) {
					obj.is_all_day = false;
				}

				if (due_date) {
					if (start_date) {
						var old_start_date = str2time(start_date);
						// var new_start_date = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate());
						var new_start_date = new Date(curDate);
						var due_date_time = str2time(due_date);

						diff = old_start_date.getTime() - new_start_date.getTime();
						obj.due_date = time2strAPI(new Date(due_date_time.getTime() - diff));
						obj.start_date = time2strAPI(curDate);
					} else {
						obj.due_date = time2strAPI(curDate);
					}
				} else {
					if (start_date) {
						obj.start_date = time2strAPI(curDate);
					} else {
						// var d = dojo.clone(HiTaskCalendar.currentDate);
						// d = hi.items.date.truncateTime(d);
						// obj.due_date = time2strAPI(new Date(d.getTime() + 86400000 - 1));
						obj.due_date = time2strAPI(curDate);
					}
				}
				obj.end_date = obj.due_date;
			} else {
				if (data.end_date) {
					oldStartDate = data.start_date ? str2timeAPI(data.start_date) : false;
					/* #2928 */
					/* #2738 dateOffset = newStartDate - newStartDate.getTimezoneOffset() * 60 * 1000 - oldStartDate + oldStartDate.getTimezoneOffset() * 60 * 1000; */
					/*
					oldEndDate = data.end_date ? str2timeAPI(data.end_date) : false;
					newEndDate = new Date();
					newEndDate.setTime(oldEndDate.getTime() * #2738 + dateOffset*);
					*/
					/* #2738 - set clock time to be same as start date clock time */
					/*
					newEndDate.setHours(newStartDate.getHours());
					newEndDate.setMinutes(newStartDate.getMinutes());
					newEndDate.setSeconds(newStartDate.getSeconds());
					newEndDate.setMilliseconds(newStartDate.getMilliseconds());
					*/
					var roughDuration = 0;
					if (time2str(new Date(data.end_date),'y-m-d') != time2str(new Date(data.start_date),'y-m-d')) roughDuration=1;
					if (!is_all_day || roughDuration != 0) {
						var duration = new Date(data.end_date) - new Date(data.start_date);
						var myD = new Date(newStartDate);
						myD.setTime(myD.getTime() + duration);
						newEndDate = time2strAPI(myD);
					} else {
						// all day item - need to set end date to start date
						newEndDate = time2strAPI(newStartDate);
					}
				}
				
				if (hi.items.manager.get(dragId, 'category') == 4) {
					/* #3059 Note can be d&d to the calendar and set the time */
					return;
				}
				
				
				//var taskProperties = hi.items.manager._items[dragId].list.propertiesView?  hi.items.manager._items[dragId].list.propertiesView.task:null;
				taskId = dragId;
				//Normalize
				if (newStartDate) {
					obj.start_date = time2strAPI(newStartDate);
					obj.is_all_day = false;	
					/*
					if (taskProperties) 
					{
						taskProperties.set('start_date',obj.start_date);
						taskProperties.set('is_all_day',false);
					}
					*/
				}
				if (newEndDate) {
					obj.end_date = newEndDate;//time2strAPI(newEndDate);
					obj.is_all_day = false;

				}
			}

			/*
			 * #3255,#3355
			 */
			if (data.recurring && data.recurring_end_date) shiftRecurrenceFeatures(data,obj);
			//Save data
			if (obj.start_date || obj.end_date || obj.due_date) {
				trackEvent(trackEventNames.ITEM_MODIFY);
				item_DOM._save(taskId, obj);
				hi.items.manager.setSave(obj);
				HiTaskCalendar.update(); // update UI
			}
		}
		
		Project.dragging = false;
	};
	
	dojo.global.dndHandleProjectDrop = function () {
		// summary:
		// 		Handle drop on project
		//console.log('handle project drop',[this.dropValue,dojo.dnd.HiSortDragExtra.waitingConfirmation]);
		if (dojo.dnd.HiSortDragExtra.waitingConfirmation) return;
		
		//JDT var m = dojo.dnd.Manager();
		var m = dojo.dnd.Manager.manager(), valid;

		if (this.dropValue !== false && m.source instanceof dojo.dnd.HiDragSource) {
			// valid = isDndValid.apply(this, []);

			// if(!valid) {
			// 	this.dropValue = false;
			// 	return false;
			// }
		
			var canceled = false;
			/* #3197
			 * m.source.dragNode.dragId is the item ID and is a string
			 * So if we allow it to propagate as a string in this branch of DnD logic, it eventually
			 * works its way to the observer, where a new item is added with a taskId in string
			 * type instead of number type
			 */
			var dragId = parseInt(m.source.dragNode.dragId);
			var isChild = Project.isTaskChild(dragId), group;
			if (isChild) {
				if (!dojo.dnd.HiSortDragExtra.drop_target) {
					//Children which was dropped on project not on task. Move out?
					//When dropped on color or date don't show dialog, because item is
					//not actually moved out
					group = Hi_Preferences.get('currentTab', 'my', 'string');
					if (group != 'color' && group != 'date') {
						//console.log('GROUP !2 && !3',group);
						var msg = hi.i18n.getLocalization('project.move_out_confirmation'),
							dragExtra = dojo.dnd.HiSortDragExtra, oldAllowSort;
						
						//Disable sorting until confirmation is closed, in Safari browser not blocked
						if (dojo.isChrome || dojo.isSafari) {
							dragExtra.waitingConfirmation = true;
							oldAllowSort = dragExtra.allowSort;
							dragExtra.allowSort = false;
						}
						
						if (!confirm(msg)) {
							canceled = true;
						}
						
						//Re-enable sorting
						if (dojo.isChrome || dojo.isSafari) {
							dragExtra.waitingConfirmation = false;
							dragExtra.allowSort = oldAllowSort;
						}
					}
					
				} else {
					//Children was dropped on other task, don't move it
					/* #2311 - may still need to adjust priority, so don't cancel yet? */
					//canceled = true;
				}
			} else if (dojo.dnd.HiSortDragExtra.drop_target) {
				//console.log('HANDLE DROP NOT CHILD');
				//Task was dropped on other task, don't move it
				/* #2311 - may still need to adjust priority, so cannot cancel action yet canceled = true; */
			}
			
			if (!canceled) {
				var taskId = dragId;
				group = Hi_Preferences.get('currentTab', 'my', 'string');
				switch (group) {
					// "all items" tab
					case 'my':
						if (!item_DOM.hasChildrenOpenForEdit(dragId)) {
							this.dropValue = parseInt(this.dropValue) ? 1 : 0;
							if (hi.items.manager.get(dragId, "starred") == this.dropValue) break;
					
							if (!isDndValid.apply(this, ['makeStarred'])) break;

							if(hi.items.manager.get(dragId, "permission") < Hi_Permissions.MODIFY){
								Project.alertElement.showItem(14, {task_alert: hi.i18n.getLocalization('no_permissions.no_permissions_to_modify') || 'Sorry, you have no permissions to Modify this item'});
								// dojo.stopEvent(event);
								break;
							}
							trackEvent(trackEventNames.ITEM_MODIFY);
							hi.items.manager.setSave(dragId, {"starred": this.dropValue});
							item_DOM._save(taskId,{"starred": this.dropValue});
						}
						break;
					case 'color':
						//By color
						if (this.dropValue == hi.items.manager.get(dragId, "color")) break;
						
						if (!isDndValid.apply(this, ['modify'])) break;

						trackEvent(trackEventNames.ITEM_MODIFY);

						hi.items.manager.setSave(dragId, {"color": this.dropValue || 0});
						item_DOM._save(taskId,{"color": this.dropValue || 0});
						break;
					case 'today':
					case 'date':
					case 'overdue':
						/* #2928 - do not allow recurring items to be Dragged and dropped throughout Date tab 
						 *       - do not allow items spanning multiple days to be dragged and dropped in Date tab
						 */
						var item = hi.items.manager.get(dragId),
							type = item.category,//hi.items.manager.get(dragId, "category");
							recurring = item.recurring;
						
						// #2928 - do not allow recurring items to be dragged and dropped in Date tab
						if (recurring) return;

						// Feature #5555
						// make sure the singleness of each operation
						// If the true action is adding sub-task, we should not trigger group change level actions.
						if (dojo.dnd.HiSortDragExtra.actionType === 'addSub') return;
						// #2928 - as per version 9, do not allow items spanning multiple days to be dragged and dropped in Date tab
						if (hi.items.date.itemSpansMultipleDays(item)) return;
						if (this.dropValue == "today") {
							this.dropValue = time2str("now", "y-m-d");
						} else if (this.dropValue == "tomorrow") {
							this.dropValue = time2str("tomorrow", "y-m-d");
						} else {
							return;
						}
						this.dropValue = str2time(this.dropValue);

						var end_date = false;
						var due_date;
						var start_date = hi.items.manager.get(dragId, "start_date");
						var save_data = {};

						// Due date feature
						if (type == 1) {		//task 
							var diff;

							due_date = hi.items.manager.get(dragId, "due_date");

							if (due_date) {
								due_date = str2time(due_date);

								if (start_date) {
									start_date = str2time(start_date);

									if (start_date) {
										diff = due_date.getTime() - start_date.getTime();
									}
								}

								hi.items.date.dateSetDate(due_date, this.dropValue);
								save_data.due_date = save_data.end_date = time2strAPI(due_date);
								
								if (typeof diff !== 'undefined') {
									save_data.start_date = new Date(due_date.getTime() - diff);
									save_data.start_date = time2strAPI(save_data.start_date);
								}
							} else {
								if (start_date) {
									start_date = str2time(start_date);
									hi.items.date.dateSetDate(start_date, this.dropValue);
									save_data.start_date = time2strAPI(start_date);
								} else {
									due_date = new Date();
									hi.items.date.dateSetDate(due_date, this.dropValue);
									hi.items.date.dateSetTime(due_date, 23,59);
									save_data.due_date = save_data.end_date = time2strAPI(due_date);
								}
							}
						} else {		// not task, keep initial logic
							if (type == 2) { // if event
								end_date = hi.items.manager.get(dragId, "end_date") ||
													hi.items.manager.get(dragId,'start_date') ||
													time2strAPI(hi.items.date.truncateTime(new Date()));
								// end_date = end_date.replace(/\d{4}-\d{2}-\d{2}/, this.dropValue);
								end_date = str2time(end_date);
								hi.items.date.dateSetDate(end_date, this.dropValue);
							}
							if (type == 4) { // if note
								save_data.category = 2; // change to task
							}
							
							if (end_date) {
								//var end_date = hi.items.manager.get(dragId, "end_date") || time2strAPI(hi.items.date.truncateTime(new Date())),
								start_date = hi.items.manager.get(dragId, "start_date") || end_date;
								start_date = start_date.getTime ? start_date : str2time(start_date);
								save_data.end_date = time2strAPI(end_date);
								// save_data.start_date = start_date.replace(/\d{4}-\d{2}-\d{2}/, this.dropValue);
								hi.items.date.dateSetDate(start_date, this.dropValue);
								save_data.start_date = time2strAPI(start_date);
							} else {
								start_date = hi.items.manager.get(dragId, "start_date") || time2strAPI(hi.items.date.truncateTime(new Date()));
								start_date = str2time(start_date);
								hi.items.date.dateSetDate(start_date, this.dropValue);
								// save_data.start_date = start_date.replace(/\d{4}-\d{2}-\d{2}/, this.dropValue);
								save_data.start_date = time2strAPI(start_date);
							}
						}
						/* #2743 - force is_all_day */
						/* #2928 - force is_all_day if no start_time is specified */
						var widget = hi.items.manager.item(dragId),
							start_time = /*widget.get('start_time')*/item_DOM._getStart_timeAttr(widget) || '',
							end_time = /*widget.get('end_time')*/item_DOM._getEnd_timeAttr(widget) || '';
						
						if (start_time.length > 0) {
							if (end_time.length <= 0) {
								save_data.end_date = save_data.start_date;//time2strAPI(HiTaskCalendar.getDateTimeFromStr(save_data.end_date, start_time));
							}
						} else {
							save_data.is_all_day = true;
						}
						
						if (!isDndValid.apply(this, ['modify'])) break;

						trackEvent(trackEventNames.ITEM_MODIFY);

						hi.items.manager.setSave(dragId, save_data);
						item_DOM._save(taskId, save_data);
						break;
					case 'project':
						//Change parent
						if (hi.items.manager.get(dragId, "parent") == this.dropValue) break;
						
						save_data = {"parent": this.dropValue || 0};
						var parent = hi.items.manager.get(this.dropValue);
						
						if (this.dropValue && hi.items.manager.get(this.dropValue, "shared") && !hi.items.manager.get(dragId, "shared")) {
							//If project was shared, then share also task
							save_data.shared = 1; 
						}

						if (!isDndValid.apply(this, ['modify'])) break;

						if (
							parent && parent.permission >= Hi_Permissions.CHILDREN ||
							(!parent && this.dropValue === 0)		//for ungrouped list
						) {
							trackEvent(trackEventNames.ITEM_MODIFY);
							hi.items.manager.setSave(dragId, save_data);
							item_DOM._save(taskId,save_data);
						}
						break;
					case 'team':
						if (!isDndValid.apply(this, ['assign'])) break;

						var prevAssignee = hi.items.manager.get(dragId, "assignee");
						
						if (this.dropValue) {
							Project.assign(dragId, this.dropValue, undefined);
						} else {
							Project.assign(dragId, false, prevAssignee);
						}

						// #3431: we need to be able to undo current action.
						m.avatar.onHiDndCancelled = function() {
							Project.assign(dragId, prevAssignee, undefined);
						};
						
						break;
				}
			}
		}
		Project.dragging = false;
		this.dropValue = false;
		this.actionType = undefined;
	};
	
	//After hi.store.init bind dnd listeners
	dojo.connect(hi_store, "init", hi_store, function () {
		
		//Bind drag and drop on team
		var cont = dojo.byId('team_container_0');
		if (cont) {
			targetTeam = new dojo.dnd.HiDragTargetTeam(cont);
			
			targetTeam.text = hi.i18n.getLocalization('center.assign_to') + ' ';
			dojo.connect(targetTeam, "onDndCancel", dndHandleTeamDrop);
			dojo.connect(targetTeam, "onDndDrop", dndHandleTeamDrop);
			
			dragSources.targetTeam = targetTeam;
		}
			
		
		//Bind drag and drop on self
		cont = dojo.byId('header_myself');

		if (cont) {
			var targetSelf = new dojo.dnd.HiDragTargetSelf(cont);
			
			targetSelf.text = hi.i18n.getLocalization('center.assign_to') + ' ';
			dojo.connect(targetSelf, "onDndCancel", dndHandleTeamDrop);
			dojo.connect(targetSelf, "onDndDrop", dndHandleTeamDrop);
			
			dragSources.targetSelf = targetSelf;
		}
			
		
		//Bind drag and drop on conversation
		cont = dojo.byId('conversation_container');

		if (cont) {
			var targetConversations = new dojo.dnd.HiDragTargetSelf(cont);
		
			targetConversations.text = hi.i18n.getLocalization('center.assign_to') + ' ';
			dojo.connect(targetConversations, "onDndCancel", dndHandleTeamDrop);
			dojo.connect(targetConversations, "onDndDrop", dndHandleTeamDrop);
			
			dragSources.targetConversations = targetConversations;
		}
		
		//Bind drag and drop on calendar
		cont = dojo.byId('hitask_calendar');

		if (cont) {
			var targetCalendar = new dojo.dnd.HiDragTargetCalendar(cont);
			
			targetCalendar.text = hi.i18n.getLocalization('center.start_on') + ' ';
			
			dojo.connect(targetCalendar, "onDndCancel", dndHandleCalendarDrop);
			dojo.connect(targetCalendar, "onDndDrop", dndHandleCalendarDrop);
		
			dragSources.targetCalendar = targetCalendar;
		}
		
		//Bind drag and drop on all day event list
		cont = dojo.byId('day');

		if (cont) {

			cont = cont.parentNode;
			var targetAllDayEvents = new dojo.dnd.HiDragTargetDay(cont);
		
			targetAllDayEvents.text = hi.i18n.getLocalization('center.start_on') + ' ';
			
			dragSources.targetAllDayEvents = targetAllDayEvents;
			dojo.connect(targetAllDayEvents, "onDndCancel", dndHandleAllDayEventDrop);
			dojo.connect(targetAllDayEvents, "onDndDrop", dndHandleAllDayEventDrop);
		}
		
		//Bind drag and drop on time table
		cont = dojo.byId('timetable');

		if (cont) {
			var targetTimeTable = new dojo.dnd.HiDragTargetTimetable(cont);
		
			targetTimeTable.text = hi.i18n.getLocalization('center.start_on') + ' ';

			dojo.global.dragSources.targetTimetable = targetTimeTable;
			dojo.connect(targetTimeTable, "onDndCancel", dndHandleTimeTableDrop);
			dojo.connect(targetTimeTable, "onDndDrop", dndHandleTimeTableDrop);
		}
		
		//Bind drag and drop on item list
		cont = dojo.byId(Project.getContainer('tasksList'));

		if (cont) {
			var targetProjects = new dojo.dnd.HiDragTargetProject(cont);
		
			dojo.global.dragSources.targetProjects = targetProjects;
			dojo.connect(targetProjects, "onDndCancel", dndHandleProjectDrop);
			dojo.connect(targetProjects, "onDndDrop", dndHandleProjectDrop);
			
			//Reset project coordinates
			dojo.connect(hi.items.manager, "groupingChange", targetProjects, targetProjects.resetProjectCoordinates); 
			dojo.connect(hi.items.manager, "redraw", targetProjects, targetProjects.resetProjectCoordinates);
		}
		
	});

});
